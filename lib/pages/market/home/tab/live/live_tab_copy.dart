// import 'package:auto_route/auto_route.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';

// class LiveTab extends StatefulWidget {
//   @override
//   _LiveTabState createState() => _LiveTabState();
// }

// class _LiveTabState extends State<LiveTab> {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       color: Color(0xff1C1A1A),
//       width: MediaQuery.of(context).size.width,
//       height: MediaQuery.of(context).size.height,
//       child: Column(
//         children: [
//           _onlineCard()
//         ],
//       ),
//     );
//   }

//   Widget _onlineCard() {
//     return Column(
//       children: [
//         Container(
//           color: Color(0xff2A2A2A),
//           child: Row(
//             children: [
//               SizedBox(width: 8),
//               ClipRRect(
//                 borderRadius: BorderRadius.circular(35),
//                 child: Image.asset(
//                   ('assets/images/users/1-boy.png'),
//                   width: 35,
//                 ),
//               ),
//               SizedBox(width: 15),
//               Text(
//                 'Namra Ebradu',
//                 style: TextStyle(color: Colors.white, fontSize: 16),
//               ),
//               Expanded(child: SizedBox()),
//               IconButton(
//                 icon: Icon(Icons.zoom_out_map),
//                 color: Colors.white70,
//                 onPressed: () => _goToRoom(),
//               )
//             ],
//           ),
//         ),
//         GestureDetector(
//           onTap: () => _goToRoom(),
//           child: Container(
//             width: MediaQuery.of(context).size.width,
//             height: MediaQuery.of(context).size.width,
//             decoration: BoxDecoration(color: Color(0xff343131), image: DecorationImage(image: AssetImage('assets/images/samples/ads.jpg'), fit: BoxFit.cover)),
//             child: Column(
//               children: [
//                 Container(
//                   width: MediaQuery.of(context).size.width,
//                   padding: EdgeInsets.all(8.0),
//                   decoration: BoxDecoration(
//                       gradient: LinearGradient(colors: [
//                     Colors.black,
//                     Colors.transparent,
//                   ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Text(
//                         'Hurry up! Limited time discount is coming',
//                         style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.w600),
//                       ),
//                       Row(
//                         children: [
//                           _live(label: 'LIVE'),
//                           _live(label: '123', isUser: true),
//                         ],
//                       )
//                     ],
//                   ),
//                 )
//               ],
//             ),
//           ),
//         )
//       ],
//     );
//   }

//   _goToRoom() {
//     ExtendedNavigator.of(context).push('/market/live');
//   }

//   Widget _live({String label, bool isUser: false}) {
//     return Container(
//       margin: EdgeInsets.symmetric(vertical: 5),
//       padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
//       decoration: BoxDecoration(color: isUser ? Color(0xff2A2A2A) : Colors.redAccent, borderRadius: isUser ? BorderRadius.only(topRight: Radius.circular(20), bottomRight: Radius.circular(20)) : BorderRadius.only(topLeft: Radius.circular(20), bottomLeft: Radius.circular(20))),
//       child: Row(
//         mainAxisSize: MainAxisSize.min,
//         children: [
//           isUser
//               ? Icon(
//                   Icons.person,
//                   color: Colors.white,
//                   size: 14,
//                 )
//               : Container(
//                   padding: EdgeInsets.all(3),
//                   decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
//                 ),
//           SizedBox(width: 5),
//           Text(
//             label,
//             style: TextStyle(color: Colors.white, fontSize: 16),
//           ),
//         ],
//       ),
//     );
//   }
// }
