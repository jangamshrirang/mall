class RestaurantMyOrderListModel {
  RestaurantMyOrderListModel({
    this.orderHashId,
    this.orderNumber,
    this.subTotal,
    this.shippingRate,
    this.total,
    this.statusInfo,
    this.restaurant,
  });

  String orderHashId;
  int orderNumber;
  String subTotal;
  String shippingRate;
  String total;
  String statusInfo;
  Restaurant restaurant;

  factory RestaurantMyOrderListModel.fromJson(Map<String, dynamic> json) => RestaurantMyOrderListModel(
        orderHashId: json["order_hash_id"],
        orderNumber: json["order_number"],
        subTotal: json["sub_total"],
        shippingRate: json["shipping_rate"],
        total: json["total"],
        statusInfo: json["status_info"],
        restaurant: Restaurant.fromJson(json["restaurant"]),
      );

  Map<String, dynamic> toJson() => {
        "order_hash_id": orderHashId,
        "order_number": orderNumber,
        "sub_total": subTotal,
        "shipping_rate": shippingRate,
        "total": total,
        "status_info": statusInfo,
        "restaurant": restaurant.toJson(),
      };
}

class Restaurant {
  Restaurant({
    this.name,
    this.street,
    this.state,
    this.city,
    this.otherAddressInfo,
    this.phone,
    this.storeEmail,
    this.latitude,
    this.longitude,
    this.hashid,
  });

  String name;
  String street;
  String state;
  String city;
  String otherAddressInfo;
  String phone;
  String storeEmail;
  String latitude;
  String longitude;
  String hashid;

  factory Restaurant.fromJson(Map<String, dynamic> json) => Restaurant(
        name: json["name"],
        street: json["street"],
        state: json["state"],
        city: json["city"],
        otherAddressInfo: json["other_address_info"],
        phone: json["phone"],
        storeEmail: json["store_email"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        hashid: json["hashid"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "street": street,
        "state": state,
        "city": city,
        "other_address_info": otherAddressInfo,
        "phone": phone,
        "store_email": storeEmail,
        "latitude": latitude,
        "longitude": longitude,
        "hashid": hashid,
      };
}
