// import 'package:auto_route/auto_route.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:google_map_location_picker/google_map_location_picker.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';

// import 'package:wblue_customer/models/user.dart';
// import 'package:wblue_customer/services/global.dart';
// import 'package:wblue_customer/widgets/w_button.dart';
// import 'package:wblue_customer/widgets/w_drawer.dart';
// import 'package:wblue_customer/widgets/w_image.dart';

// import '../env/config.example.dart';

// class LandingPage extends StatefulWidget {
//   @override
//   _LandingPageState createState() => _LandingPageState();
// }

// enum SearchBy {
//   restaurant,
//   hotel,
//   cinema,
//   market,
// }

// class _LandingPageState extends State<LandingPage> {
//   SearchBy searchBy = SearchBy.restaurant;
//   String address = Global.myAddress;
//   LatLng initialCenter = Global.myLocation;
//   final ScrollController _scrollController = ScrollController();

//   @override
//   Widget build(BuildContext context) {
//     User user = Global.user;

//     TextEditingController _controller = new TextEditingController.fromValue(
//       TextEditingValue(
//         text: address,
//         selection: new TextSelection.collapsed(
//           offset: address.length,
//         ),
//       ),
//     );

//     return Scaffold(
//       resizeToAvoidBottomInset: true,
//       backgroundColor: Config.dullColor,
//       appBar: AppBar(
//         elevation: 0.0,
//         centerTitle: true,
//         title: Text('Home'),
//         actions: <Widget>[
//           IconButton(
//             icon: Icon(Icons.account_circle),
//             onPressed: () {},
//           ),
//         ],
//       ),
//       drawer: WDrawerWidget('home'),
//       body: GestureDetector(
//         onTap: () => FocusScope.of(context).unfocus(),
//         child: ListView(
//           controller: _scrollController,
//           children: <Widget>[
//             Container(
//               height: MediaQuery.of(context).size.height * 0.30,
//               padding: EdgeInsets.symmetric(vertical: 32.0),
//               child: Center(
//                   child: WImageWidget(
//                       placeholder:
//                           AssetImage('assets/images/logos/wmall.png'))),
//             ),
//             FractionallySizedBox(
//               widthFactor: 0.8,
//               child: Container(
//                 padding: EdgeInsets.all(24.0),
//                 decoration: BoxDecoration(
//                   color: Colors.black45,
//                   borderRadius: BorderRadius.circular(30.0),
//                 ),
//                 child: Column(
//                   children: <Widget>[
//                     Text(
//                       'ENTER YOUR LOCATON',
//                       textAlign: TextAlign.center,
//                       style: TextStyle(
//                         color: searchBy == SearchBy.market
//                             ? Colors.grey
//                             : Colors.white,
//                       ),
//                     ),
//                     Container(
//                       margin: EdgeInsets.symmetric(vertical: 16.0),
//                       decoration: BoxDecoration(
//                           color: Colors.white,
//                           borderRadius: BorderRadius.circular(50.0),
//                           boxShadow: [
//                             BoxShadow(
//                               color: Colors.grey,
//                               offset: Offset(0.0, 8.0),
//                             ),
//                           ]),
//                       child: TextField(
//                         controller: _controller,
//                         decoration: InputDecoration(
//                           hintText: 'Street Address',
//                           border: InputBorder.none,
//                         ),
//                         style: TextStyle(
//                             color: searchBy == SearchBy.market
//                                 ? Colors.grey[300]
//                                 : Colors.black87),
//                         enabled: searchBy == SearchBy.market ? false : true,
//                         textAlign: TextAlign.center,
//                         onTap: () async {
//                           LocationResult result = await showLocationPicker(
//                             context,
//                             Config.googleMapAPI,
//                             automaticallyAnimateToCurrentLocation: false,
//                             initialCenter: initialCenter,
//                             myLocationButtonEnabled: true,
//                             layersButtonEnabled: true,
//                           );

//                           setState(() {
//                             address = result.address;
//                             initialCenter = result.latLng;
//                             Global.myAddress = address;
//                             Global.myLocation = initialCenter;
//                           });
//                         },
//                       ),
//                     ),
//                     SizedBox(height: 10.0),
//                     WButtonWidget(
//                       title:
//                           '${searchBy == SearchBy.market ? 'open' : 'search'} ' +
//                               (searchBy == SearchBy.restaurant
//                                   ? 'restaurant'
//                                   : searchBy == SearchBy.hotel
//                                       ? 'hotel'
//                                       : searchBy == SearchBy.cinema
//                                           ? 'cinema'
//                                           : 'market'),
//                       expanded: true,
//                       onPressed: () {
//                         if (searchBy == SearchBy.restaurant) {
//                           ExtendedNavigator.of(context)
//                               .root
//                               .push('/restaurants');
//                         } else if (searchBy == SearchBy.hotel) {
//                           ExtendedNavigator.of(context).root.push('/hotels');
//                         } else if (searchBy == SearchBy.market) {
//                           ExtendedNavigator.of(context).root.push('/market');
//                         } else {
//                           ExtendedNavigator.of(context).root.push('/cinemas');
//                         }
//                       },
//                     ),
//                     SizedBox(height: 16.0),
//                     InkWell(
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: <Widget>[
//                           Icon(
//                             Icons.pin_drop,
//                             color: searchBy == SearchBy.market
//                                 ? Colors.grey
//                                 : Colors.white,
//                           ),
//                           Text(
//                             'USE MY CURRENT LOCATION',
//                             style: TextStyle(
//                                 color: searchBy == SearchBy.market
//                                     ? Colors.grey
//                                     : Colors.white),
//                           ),
//                         ],
//                       ),
//                       onTap: () async {
//                         if (searchBy == SearchBy.market) {
//                         } else {
//                           LocationResult result = await showLocationPicker(
//                             context,
//                             Config.googleMapAPI,
//                             initialCenter: initialCenter,
//                             myLocationButtonEnabled: true,
//                             layersButtonEnabled: true,
//                           );

//                           setState(() {
//                             address = result.address;
//                             initialCenter = result.latLng;
//                             Global.myAddress = address;
//                             Global.myLocation = initialCenter;
//                           });
//                         }
//                       },
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//             SizedBox(height: 30.0),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: <Widget>[
//                 Column(
//                   children: <Widget>[
//                     FloatingActionButton(
//                       heroTag: null,
//                       child: Icon(Icons.restaurant),
//                       backgroundColor: Colors.white,
//                       foregroundColor: searchBy == SearchBy.restaurant
//                           ? Config.secondaryColor
//                           : Colors.grey,
//                       onPressed: () {
//                         setState(() {
//                           searchBy = SearchBy.restaurant;
//                         });
//                       },
//                     ),
//                     SizedBox(height: 20),
//                     Text('Restaurant',
//                         style: TextStyle(
//                           fontSize: 16.0,
//                           color: searchBy == SearchBy.restaurant
//                               ? Config.secondaryColor
//                               : Config.primaryColor,
//                         ))
//                   ],
//                 ),
//                 Column(
//                   children: <Widget>[
//                     FloatingActionButton(
//                       heroTag: null,
//                       child: Icon(Icons.hotel),
//                       backgroundColor: Colors.white,
//                       foregroundColor: searchBy == SearchBy.hotel
//                           ? Config.secondaryColor
//                           : Colors.grey,
//                       onPressed: () {
//                         setState(() {
//                           searchBy = SearchBy.hotel;
//                         });
//                       },
//                     ),
//                     SizedBox(height: 20),
//                     Text('Hotel',
//                         style: TextStyle(
//                           fontSize: 16.0,
//                           color: searchBy == SearchBy.hotel
//                               ? Config.secondaryColor
//                               : Config.primaryColor,
//                         ))
//                   ],
//                 ),
//                 Column(
//                   children: <Widget>[
//                     FloatingActionButton(
//                       heroTag: null,
//                       child: Icon(Icons.movie),
//                       backgroundColor: Colors.white,
//                       foregroundColor: searchBy == SearchBy.cinema
//                           ? Config.secondaryColor
//                           : Colors.grey,
//                       onPressed: () {
//                         setState(() {
//                           searchBy = SearchBy.cinema;
//                         });
//                       },
//                     ),
//                     SizedBox(height: 20),
//                     Text('Cinema',
//                         style: TextStyle(
//                           fontSize: 16.0,
//                           color: searchBy == SearchBy.cinema
//                               ? Config.secondaryColor
//                               : Config.primaryColor,
//                         ))
//                   ],
//                 ),
//                 Column(
//                   children: <Widget>[
//                     FloatingActionButton(
//                       heroTag: null,
//                       child: Container(
//                           height: 36,
//                           width: 36,
//                           child: WImageWidget(
//                               placeholder:
//                                   AssetImage('assets/images/logos/wmall.png'))),
//                       backgroundColor: Colors.white,
//                       foregroundColor: searchBy == SearchBy.market
//                           ? Config.secondaryColor
//                           : Colors.grey,
//                       onPressed: () {
//                         setState(() {
//                           searchBy = SearchBy.market;
//                         });
//                       },
//                     ),
//                     SizedBox(height: 20),
//                     Text('W Mall',
//                         style: TextStyle(
//                           fontSize: 16.0,
//                           color: searchBy == SearchBy.market
//                               ? Config.secondaryColor
//                               : Config.primaryColor,
//                         ))
//                   ],
//                 ),
//               ],
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
