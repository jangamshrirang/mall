import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/DriverRatings/StarRatingScreen.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

import '../../../FershLaunch.dart';
import 'AccountInfoScreen.dart';
import 'ActivityHistory.dart';
import 'AddressBook.dart';
import 'OrderHistory.dart';
import 'OrderTracking.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "Settings"),
      body: Container(
        height: size.height * 1,
        width: size.width,
        margin: EdgeInsets.only(left: 10, right: 10),
        child: ListView(
          children: ListTile.divideTiles(
            context: context,
            tiles: [
              SizedBox(
                height: 10,
              ),
              listtile(context, "Account Information", () {
                settingsScreenNavigation(context, AccountInfomrmationScreen());
              }),
              listtile(context, "Address Book", () {
                settingsScreenNavigation(context, AddressBookScreen());
              }),
              listtile(context, "Order History", () {
                settingsScreenNavigation(context, ActivityHistory());
              }),
              listtile(context, "Order Tracking", () {
                settingsScreenNavigation(context, OrderHistory(
                  appBarTitle: "My Order",
                ));
              }),
              listtile(context, "Private Transcations", () {
                settingsScreenNavigation(context, OrderHistory(
                  appBarTitle: "Private Transcations",
                ));
              }),
              listtile(context, "Dirver Rating", () {
                print("Dirver Rating");
                settingsScreenNavigation(context, DriverStarRatingScreen());
              }),
              listtile(context, "Help", () {
                print("Help");
                //  settingsScreenNavigation(context, FereshLaunch());
              }),
              listtile(context, "Logout", () {
                print("Logout");
              }),
            ],
          ).toList(),
        ),
      ),
    );
  }

  listtile(BuildContext context, String title, Function ontap) {
    return ListTile(
      // leading: Image.network(image, height: 30, color: Colors.amber),
      title: Text(
        title,
      ),
      onTap: ontap,
    );
  }

  settingsScreenNavigation(BuildContext context, screen) {
    return Navigator.push(
        context, MaterialPageRoute(builder: (context) => screen));
  }
}
