import 'package:dio/dio.dart';

import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();

class MyOrderExpantionHelper with ChangeNotifier {
  Map<String, dynamic> _myOrderExpantionHelper = new Map<String, dynamic>();
  bool _isLoading = true;

  Map get myOrderExpantion => _myOrderExpantionHelper;
  bool get isLoading => _isLoading;

  Future<void> fetchMyOrderExpantionDetails(String orderHashId) async {
    setLoading(true);
    _myOrderExpantionHelper = await fetchMyORderExpantionDetails(orderHashId);
    setLoading(false);

    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future<Map<String, dynamic>> fetchMyORderExpantionDetails(
    String orderHashID) async {
  Map<String, dynamic> _body = {
    'order_hash_id': orderHashID,
  };

  APIResponse res = await _dioClient
      .privatePost('/restaurant/order/my-order-info', data: _body);

  if (res.code >= 400) {
    return {};
  }
  print("Mu order exp data :: ${res.data}");

  return res.data;
}
