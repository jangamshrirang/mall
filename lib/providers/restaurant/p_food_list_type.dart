import 'dart:collection';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/foundation.dart';
import 'package:wblue_customer/models/Restaurant/R_ListByRatings.dart';
import 'package:wblue_customer/models/Restaurant/food_type_model.dart';
import 'package:wblue_customer/models/auth.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'package:wblue_customer/services/helper.dart';

DioClient _dioClient = DioClient();
AuthModel auth = AuthModel();

class PFoodListType with ChangeNotifier {
  List<FoodListTypeModel> _foodListType = new List<FoodListTypeModel>();
  UnmodifiableListView<FoodListTypeModel> get foodListType => UnmodifiableListView<FoodListTypeModel>(_foodListType);

  int _currentTab = 0;
  int get currentTab => _currentTab;

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  String storeHashid = '';

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }

  void setAllFoodListType(FoodListTypeModel foodListTypeModel) {
    _foodListType.add(foodListTypeModel);
  }

  Future<void> fetchFoodTypes(String hashid) async {
    try {
      storeHashid = hashid;
      _foodListType = await _fetchFoodTypes();

      setCurrentTab(0);
    } catch (e) {
      print('Error ( fetchFoodTypes ) : $e');
    }
  }

  void setCurrentTab(int index) {
    _currentTab = index;
    notifyListeners();
  }

  Future<void> isFollowInit(RestaurantListModel restaurantListModel) async {
    bool res = await auth.checkAuthentication();
    if (!res) return;
    bool isFollow = await checkFollow(restaurantListModel.hashId);
    if (isFollow)
      restaurantListModel.isFollowed = true;
    else
      restaurantListModel.isFollowed = false;
    notifyListeners();
  }

  void followApi(RestaurantListModel restaurantListModel) async {
    try {
      Map<String, dynamic> _body = {'store_hashid': storeHashid};
      APIResponse res = await _dioClient.privatePost('/vendor/store/customer-followeOrUnfollow-store', data: _body);

      if (res.code >= 400) {
        if (restaurantListModel.isFollowed)
          restaurantListModel.isFollowed = false;
        else
          restaurantListModel.isFollowed = true;
      } else {
        if (restaurantListModel.isFollowed)
          restaurantListModel.isFollowed = false;
        else
          restaurantListModel.isFollowed = true;
      }

      notifyListeners();
    } catch (e) {
      print('Method (followApi) error: $e');
      restaurantListModel.isFollowed = false;
      notifyListeners();
    }
  }

  Future<bool> checkFollow(String hashid) async {
    try {
      Map<String, dynamic> _body = {'restaurant_hash_id': hashid};

      APIResponse res = await _dioClient.privatePost('/restaurant/is-followed', data: _body);

      if (res.code >= 400) {
        return res.data == 1 ? true : false;
      }

      return res.data == 1 ? true : false;
    } catch (e) {
      print('Method (checkFollow) error: $e');
      return false;
    }
  }

  Future<List<FoodListTypeModel>> _fetchFoodTypes() async {
    try {
      Map<String, dynamic> _body = {'store_hashid': storeHashid};
      setLoading(true);
      APIResponse res = await _dioClient.publicPost('/restaurant/menu-list', data: _body);
      setLoading(false);

      if (res.code >= 400) {
        return new List<FoodListTypeModel>();
      }
      return res.data.map<FoodListTypeModel>((json) => FoodListTypeModel.fromJson(json)).toList();
    } catch (e) {
      print('Method (fetchFoodTypes) error: $e');
      return new List<FoodListTypeModel>();
    }
  }
}
