import 'package:wblue_customer/models/Restaurant/food_variation.dart';

class FoodListTypeModel {
  FoodListTypeModel({
    this.foodTypeHashid,
    this.foodTypeName,
    this.foods,
  });

  String foodTypeHashid;
  String foodTypeName;
  List<Food> foods;

  factory FoodListTypeModel.fromJson(Map<String, dynamic> json) =>
      FoodListTypeModel(
        foodTypeHashid:
            json["food_type_hashid"] == null ? null : json["food_type_hashid"],
        foodTypeName:
            json["food_type_name"] == null ? null : json["food_type_name"],
        foods: json["foods"] == null
            ? null
            : List<Food>.from(json["foods"].map((x) => Food.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "food_type_hashid": foodTypeHashid == null ? null : foodTypeHashid,
        "food_type_name": foodTypeName == null ? null : foodTypeName,
        "foods": foods == null
            ? null
            : List<dynamic>.from(foods.map((x) => x.toJson())),
      };
}

class Food {
  Food(
      {this.hashid,
      this.name,
      this.price,
      this.full,
      this.thumbnail,
      this.description,
      this.isContainsVariation,
      this.variations});

  String hashid;
  String name;
  String price;
  String description;
  bool isContainsVariation;
  String full;
  String thumbnail;
  List<RestaurantFoodVariationModel> variations;

  factory Food.fromJson(Map<String, dynamic> json) => Food(
        hashid: json["hashid"] == null ? null : json["hashid"],
        name: json["name"] == null ? null : json["name"],
        price: json["price"] == null ? null : json["price"],
        full: json["full"] == null ? null : json["full"],
        description: json["description"] == null ? null : json["description"],
        isContainsVariation: json["is_have_variation"] == 1 ? true : false,
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        variations: json["variations"] == null
            ? null
            : List<RestaurantFoodVariationModel>.from(json["variations"]
                .map((x) => RestaurantFoodVariationModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "hashid": hashid == null ? null : hashid,
        "name": name == null ? null : name,
        "description": description == null ? null : name,
        "price": price == null ? null : price,
        "full": full == null ? null : full,
        "is_have_variation":
            isContainsVariation == null ? null : isContainsVariation,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "variations": variations == null
            ? null
            : List<dynamic>.from(variations.map((x) => x.toJson())),
      };
}
