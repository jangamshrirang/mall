import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/user_office_address.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wblue_customer/providers/restaurant/checkout/p_user_address.dart';
import 'package:provider/provider.dart';

class SelectDeliveryAddressPage extends StatefulWidget {
  final UserOfficeAddressModel userOfficeAddressModel;
  SelectDeliveryAddressPage({this.userOfficeAddressModel});

  @override
  _SelectDeliveryAddressPageState createState() => _SelectDeliveryAddressPageState();
}

class _SelectDeliveryAddressPageState extends State<SelectDeliveryAddressPage> {
  CameraPosition usersLocation;
  LatLng latLng;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double mapWidth = MediaQuery.of(context).size.width;
    double mapHeight = MediaQuery.of(context).size.height - 215;
    double iconSize = 40.0;

    return Consumer<PRestaurantUserAddress>(
      builder: (context, address, child) => Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Config.primaryColor,
          title: Text('Edit Address'),
        ),
        body: Column(
          children: [
            Stack(
              alignment: Alignment(0.0, 0.0),
              children: [
                Container(
                    width: mapWidth,
                    height: mapHeight,
                    child: GoogleMap(
                      zoomControlsEnabled: false,
                      mapType: MapType.normal,
                      myLocationButtonEnabled: false,
                      buildingsEnabled: true,
                      zoomGesturesEnabled: false,
                      rotateGesturesEnabled: false,
                      tiltGesturesEnabled: false,
                      initialCameraPosition: address.usersLocation,
                      onCameraMove: (pos) {
                        latLng = pos.target;
                      },
                      onMapCreated: (GoogleMapController controller) {},
                    )),
                // ),
                //               Align(
                //                 alignment: Alignment.center,
                //                 child: Container(
                //                   child: Container(width: 0.032.sh, child: Image.asset('assets/images/pin.png')),
                //                 ),
                //               ),

                new Positioned(
                  top: (mapHeight - iconSize) / 2,
                  right: (mapWidth - iconSize) / 2,
                  child: new Icon(Icons.person_pin_circle, size: iconSize),
                ),
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 26.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                      height: 0.06.sh,
                      child: RaisedButton(
                        onPressed: () => address.getData(latLng),
                        color: Config.primaryColor,
                        textColor: Colors.white,
                        child: Text('Done', style: TextStyle(fontSize: 36.sp)),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
