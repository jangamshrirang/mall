import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WCheckingDatesWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
          ),
          padding: EdgeInsets.all(8.0),
          width: 100.0,
          height: 60.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                '29 Apr (02 PM)',
                style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold, fontSize: 10.0),
              ),
              Text(
                'Early Check-In Not Available',
                style: TextStyle(fontSize: 10.0),
                textAlign: TextAlign.start,
              ),
            ],
          ),
        ),
        Container(
          color: Colors.green[100],
          padding: EdgeInsets.all(4.0),
          child: Text(
            '1 NIGHT',
            style: TextStyle(
              fontSize: 8.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
          ),
          padding: EdgeInsets.all(8.0),
          height: 60.0,
          width: 100.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                '30 Apr (12 PM)',
                style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold, fontSize: 10.0),
              ),
              Text(
                'Late Check-Out Not Available',
                style: TextStyle(fontSize: 10.0),
                textAlign: TextAlign.start,
              ),
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
          ),
          padding: EdgeInsets.all(8.0),
          height: 60.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Room(s)'),
              Text(
                '1',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
