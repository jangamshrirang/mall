// const functions = require('firebase-functions');
// const admin = require('firebase-admin');

// admin.initializeApp(functions.config().functions);

// var newData;

// exports.myTrigger = functions.firestore.document('Messages/{messageId}').onCreate(async (snapshot, context) => {
//     if (snapshot.empty) {
//         console.log('No Devices');
//         return;
//     }
//     var tokens = [];
//     const deviceTokens = await admin
//     .firestore()
//     .collection('DeviceTokens')
//     .get();
//     newData = snapshot.data();
//     for (var token of deviceTokens.docs) {
//         tokens.push(token.data().device_token);
//     }
//     var payload = {
//         notification: { title: "W Mall", body: 'You got a notification ' }, data: { click_action: "FLUTTER_NOTIFICATION_CLICK", message: newData.message },
//     };

//     try {
//         const response = await admin.messaging().sendToDevice(tokens, payload);
//         console.log('Notification sent successfully');
//     } catch (err) {
//         console.log(err);
//     }
// });



  const functions = require('firebase-functions');
  const admin = require('firebase-admin');

  admin.initializeApp();

  const db = admin.firestore();
  const fcm = admin.messaging();

exports.sendToDevice = functions.firestore
  .document('Messages/{messageId}')
  .onCreate(async snapshot => {
    const user = snapshot.data();
    console.log('======== receriver========= '+user.receriver);

    const querySnapshot = await db
      .collection('users')
      .doc(user.receriver)
      .collection('tokens')
      .get();

    const tokens = querySnapshot.docs.map(snap => {

      console.log('======== from '+ user.sender +' ============= to ' + user.receriver + ' =============== : ' + snap.id);
      return snap.id
    });

    if(tokens.length <= 0){
      return false;
    }
 
    const payload= { 
      notification: {
        title: 'WLive',
        body: 'You have new message',
        click_action: 'FLUTTER_NOTIFICATION_CLICK',
        sound: 'default',
      },
      data: {
        click_action: "FLUTTER_NOTIFICATION_CLICK",
        id: snapshot.id,
        message: user.message,
        receriver : user.receriver,
        sender : user.sender,
      },
    };

    const options = {
      priority: 'high',
    };


    return fcm.sendToDevice(tokens, payload, options);
  });