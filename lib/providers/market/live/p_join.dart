import 'dart:async';
import 'dart:collection';
import 'dart:math';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtm/agora_rtm.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:wakelock/wakelock.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/firebaseDb/firestoreDB.dart';
import 'package:wblue_customer/models/live/message.dart';
import 'package:wblue_customer/models/user.dart';
import 'dart:math' as math;

class PJoinLive extends ChangeNotifier {
  UserModel _user;
  bool _heart = false;
  Timer _timer;
  double _height = 0.0;
  Random _random = math.Random();
  int _numConfetti = 5;
  bool isFinished = false;
  var userMap;

  RtcEngine _engine;
  List<Message> _infoStrings = <Message>[];
  List<int> _users = <int>[];

  AgoraRtmClient _client;
  AgoraRtmChannel _channel;

  AgoraRtmChannel get channel => _channel;

  final _channelMessageController = TextEditingController();

  //setup client
  int _channelId;
  String _channelName;
  String _hostId;
  String _hostImageUrl;
  String _hostName;

  int _userNo = 0;

  String get hostName => _hostName;
  String get hostImageUrl => _hostImageUrl;
  TextEditingController get channelMessageController => _channelMessageController;
  UnmodifiableListView<Message> get infoStrings => UnmodifiableListView(_infoStrings);

  UnmodifiableListView<int> get users => UnmodifiableListView(_users);
  int get userNo => _userNo;
  int get numConfetti => _numConfetti;
  bool get heart => _heart;
  Random get random => _random;
  Future<void> initialize() async {
    if (Config.agoraApi.isEmpty) {
      print('APP_ID missing, please provide your APP_ID in settings.dart');
      print('Agora Engine is not starting');
      notifyListeners();
      return;
    }

    await _initAgoraRtcEngine();
    _addAgoraEventHandlers();
    VideoEncoderConfiguration configuration = VideoEncoderConfiguration();
    configuration.dimensions = VideoDimensions(1920, 1080);
    await _engine.setVideoEncoderConfiguration(configuration);
    await _engine.joinChannel(null, _channelName, null, 0);
  }

  /// Create agora sdk instance and initialize
  Future<void> _initAgoraRtcEngine() async {
    _engine = await RtcEngine.create(Config.agoraApi);
    await _engine.enableVideo();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await _engine.setClientRole(ClientRole.Audience);
  }

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    _engine.setEventHandler(RtcEngineEventHandler(error: (code) {
      final info = 'onError: $code';
      print(info);
      notifyListeners();
    }, joinChannelSuccess: (channel, uid, elapsed) {
      final info = 'onJoinChannel: $channel, uid: $uid';
      print(info);
      notifyListeners();
    }, leaveChannel: (stats) {
      print('onLeaveChannel');
      _users.clear();
      notifyListeners();
    }, userJoined: (uid, elapsed) {
      final info = 'userJoined : $uid';
      print(info);
      _users.add(uid);
      notifyListeners();
    }, userOffline: (uid, elapsed) {
      final info = 'userOffline: $uid';
      print(info);

      if (uid == _channelId) {
        Future.delayed(const Duration(milliseconds: 2000), () async {
          await Wakelock.disable();
          isFinished = false;
          ExtendedNavigator.root.pop();
        });
        isFinished = true;
        notifyListeners();
      }

      _users.remove(uid);
      notifyListeners();
    }, firstRemoteVideoFrame: (uid, width, height, elapsed) {
      final info = 'firstRemoteVideo: $uid ${width}x $height';
      print(info);
      notifyListeners();
    }));
  }

  void createClient() async {
    print('createClient starting...');

    if (_user == null) {
      _user = await UserModel.fetchMyProfile();
    }

    _client = await AgoraRtmClient.createInstance('b42ce8d86225475c9558e478f1ed4e8e');

    _client.onMessageReceived = (AgoraRtmMessage message, String peerId) {
      print('-------------- message -----------');
      _log(user: peerId, info: message.text, type: 'message');
    };

    _client.onConnectionStateChanged = (int state, int reason) {
      if (state == 5) {
        _client.logout();
      }
    };

    await _client.login(null, _user.hashid);
    _channel = await _createChannel(_channelName);
    await _channel.join();

    _channel.getMembers().then((value) {
      _userNo = value.length - 1;
      notifyListeners();
    });

    print('joined at channel: ${_channel.channelId}');
  }

  Future<AgoraRtmChannel> _createChannel(String name) async {
    AgoraRtmChannel channel = await _client.createChannel(name);

    channel.onMemberJoined = (AgoraRtmMember member) async {
      print('-- MEMBER USER ID-- {member.userId}');

      final uprofile = await UserModel.fetchUserProfile(member.userId);
      UserModel otherUser = UserModel.fromJson(uprofile);

      userMap.putIfAbsent(member.userId, () => {'name': otherUser.name, 'img': otherUser.full});

      _channel.getMembers().then((value) {
        _userNo = value.length - 1;
        notifyListeners();
      });

      _log(info: 'Member joined: ', user: member.userId, type: 'join');
    };

    channel.onMemberLeft = (AgoraRtmMember member) {
      _channel.getMembers().then((value) {
        _userNo = value.length - 1;
        notifyListeners();
      });
    };

    channel.onMessageReceived = (AgoraRtmMessage message, AgoraRtmMember member) async {
      final uprofile = await UserModel.fetchUserProfile(member.userId);
      UserModel otherUser = UserModel.fromJson(uprofile);

      userMap.putIfAbsent(member.userId, () => {'name': otherUser.name, 'img': otherUser.full});
      _log(user: member.userId, info: message.text, type: 'message');
    };

    return channel;
  }

  void _log({String info, String type, String user}) {
    print('user ($user) sent a $type : $info');
    if (type == 'message' && info.contains('m1x2y3z4p5t6l7k8')) {
      // popUp();
    } else {
      Message m;
      var image = userMap[user];
      m = new Message(message: info, type: type, user: user, image: image);
      _infoStrings.insert(0, m);
      notifyListeners();
    }
  }

  void onSwitchCamera() {
    _engine.switchCamera();
  }

  void _logout() async {
    try {
      await _client.logout();
      // _log('Logout success.');
    } catch (errorCode) {
      print('_logout Error: $errorCode');
      //_log('Logout error: ' + errorCode.toString());
    }
  }

  void _leaveChannel() async {
    try {
      await _channel.leave();
      //_log('Leave channel success.');
      _client.releaseChannel(_channel.channelId);
      _channelMessageController.text = null;
    } catch (errorCode) {
      print('_leaveChannel Error: $errorCode');
      //_log('Leave channel error: ' + errorCode.toString());
    }
  }

  void disposeEngine() {
    // clear users
    _users.clear();
    _infoStrings.clear();

    _leaveChannel();
    _logout();

    // destroy sdk
    _engine.leaveChannel().catchError((onError) {
      print('onleaveChannelError: $onError');
    });

    _engine.destroy().catchError((onError) {
      print('ondestroyError: $onError');
    });
  }

  void setup({int channelId, String hostId, String channelName, String hostImageUrl, String hostName}) {
    _channelName = channelName;
    _channelId = channelId;
    _hostId = hostId;
    _hostImageUrl = hostImageUrl;
    _hostName = hostName;
  }

  void toggleSendChannelMessage() async {
    String text = _channelMessageController.text;
    print('toggleSendChannelMessage starting...');

    if (text.isEmpty) {
      return;
    }
    try {
      _channelMessageController.clear();
      await _channel.sendMessage(AgoraRtmMessage.fromText(text));
      _log(user: _user.name, info: text, type: 'message');
    } catch (errorCode) {
      print('errorCode (toggleSendChannelMessage): $errorCode');
      //_log('Send channel message error: ' + errorCode.toString());
    }
  }

  void setupUserMap() {
    userMap = {
      _user.hashid: {'name': _user.name, 'img': _user.full}
    };
  }

  void sendMessage(text) async {
    if (text.isEmpty) {
      return;
    }

    try {
      _channelMessageController.clear();
      await _channel.sendMessage(AgoraRtmMessage.fromText(text));
      _log(user: _user.name, info: text, type: 'message');
    } catch (errorCode) {
      //_log('Send channel message error: ' + errorCode.toString());
    }
  }
}
