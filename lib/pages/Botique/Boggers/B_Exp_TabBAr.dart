import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/Botique/Boggers/B_Expantion.dart';
import 'package:wblue_customer/pages/market/Brands/Vendor/VendorReviews.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

import 'B_productsTab.dart';

class B_Exp_TabBar extends StatefulWidget {
  int colorVal = 0xff84020e;
  @override
  _B_Exp_TabBarState createState() => _B_Exp_TabBarState();
}

class _B_Exp_TabBarState extends State<B_Exp_TabBar>
    with TickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(
      vsync: this,
      length: 3,
    );
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {
      widget.colorVal = 0xff84020e;
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(

     body: SafeArea(
        child: DefaultTabController(
          length: 2,
          child: new Scaffold(
            body: new NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    iconTheme: IconThemeData(color: Colors.black),
                    expandedHeight: size.height * 0.52,
                    floating: true,
                    pinned: true,
                    backgroundColor: Colors.white,
                    flexibleSpace: FlexibleSpaceBar(
                      titlePadding: EdgeInsetsDirectional.only(start: 0, top: 5),
                      centerTitle: false,
                      title: BloggersExpanstion(),
                    ),
                    // <--- this is required if I want the application bar to show when I scroll up
                    bottom: TabBar(
                      controller: _tabController,
                      indicatorSize: TabBarIndicatorSize.tab,
                      indicatorColor: Colors.red,
                      unselectedLabelColor: Colors.grey,
                      tabs: <Widget>[
                        Tab(child: text("PRODUCTS", 0)),
                        Tab(
                          child: text("POST", 1),
                        ),
                        Tab(
                          child: text("LIVE", 2),
                        ),
                      ],
                    ),
                  ),
                ];
              },
              body: new TabBarView(controller: _tabController, children: [
                B_ProductsTab(),
                B_ProductsTab(),
                B_ProductsTab(),
              ]),
            ),
          ),
        ),
      ),
    );
  }

  text(String title, int number) {
    return Text(
      title,
      style: TextStyle(
          color: _tabController.index == number ? Colors.black : Colors.black,
          fontWeight: FontWeight.w600),
      textAlign: TextAlign.start,
    );
  }

  payScreenContainer() {
    return Column(
      children: [
        SizedBox(
          height: 20,
        ),
        Image.asset(
          "assets/images/empty.png",
          height: 200,
          color: Config.primaryColor,
        ),
        Text(
          "There are no orders placed yet",
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.normal),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: 50,
        ),
        RaisedButton(
          onPressed: () {},
          textColor: Colors.white,
          padding: const EdgeInsets.all(0.0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(2.0)),
          child: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xFF0D47A1),
                    Color(0xFF1976D2),
                    Color(0xFF42A5F5),
                  ],
                ),
                borderRadius: BorderRadius.all(Radius.circular(2.0))),
            padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
            child:
                const Text('Continue Shopping', style: TextStyle(fontSize: 20)),
          ),
        ),
      ],
    );
  }
}
