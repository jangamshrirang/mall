import 'dart:async';
import 'dart:collection';

import 'package:flutter/foundation.dart';

class PMarketCart extends ChangeNotifier {
  List _items = new List();
  int _totalCart = 1;
  double _totalAllItems = 0.0;
  bool _showAddCartToaster = false;
  String _cartSuccessToasterMgs = 'Added to cart successfully.';
  String _cartErrorToasterMgs = 'Out of stack.';
  String _toasterMgs = '';
  int _selectedImgCartInfo = 0;
  int _selectedDelivery = 0;

  UnmodifiableListView get items => UnmodifiableListView(_items);
  int get totalCart => _totalCart;
  bool get showAddCartToaster => _showAddCartToaster;
  String get toasterMgs => _toasterMgs;
  int get selectedImgCartInfo => _selectedImgCartInfo;
  double get totalAllItems => _totalAllItems;
  int get selectedDelivery => _selectedDelivery;

  void setItem(Map item) {
    if (_showAddCartToaster) {
      return;
    }

    Map product = new Map();
    if (_items.length > 0) {
      product = _items.singleWhere((product) => product['id'] == item['id'], orElse: () => {});
    }

    if (product.length > 0) {
      if (product['availibilityCount'] <= 0) {
        setToastDuration(_cartErrorToasterMgs);
        return;
      }

      if (product['availibilityCount'] > 0) {
        product['cartCount'] += 1;
        product['availibilityCount'] -= 1;
        _totalAllItems += product['newPrice'];
      }
    } else {
      if (item['availibilityCount'] <= 0) {
        setToastDuration(_cartErrorToasterMgs);
        return;
      }

      item['cartCount'] = 1;
      item['availibilityCount'] -= 1;

      _totalAllItems += item['newPrice'];

      _items.add(item);
    }

    setToastDuration(_cartSuccessToasterMgs);
  }

  void setToastDuration(String msg) async {
    _toasterMgs = msg;
    if (_showAddCartToaster) {
      return;
    }

    _showAddCartToaster = true;
    notifyListeners();
    await Future.delayed(const Duration(seconds: 2));
    _showAddCartToaster = false;
    notifyListeners();
  }

  void setCartInfoImg(int index) {
    _selectedImgCartInfo = index;
    notifyListeners();
  }

  void setQuantity(bool isAdd, Map product) {
    if (isAdd) {
      if (product['availibilityCount'] > 0) {
        product['cartCount'] += 1;
        product['availibilityCount'] -= 1;
        _totalAllItems += product['newPrice'];
      }
    } else {
      if (product['cartCount'] > 1) {
        product['cartCount'] -= 1;
        product['availibilityCount'] += 1;
        if (product['cartCount'] > 0) {
          _totalAllItems -= product['newPrice'];
        }
      }
    }

    notifyListeners();
  }

  void setDelivery(int index) {
    _selectedDelivery = index;
    notifyListeners();
  }
}
