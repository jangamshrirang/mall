import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Reservations/reservation_model.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/providers/restaurant/p_food_list_type.dart';
import 'package:wblue_customer/providers/restaurant/p_qr_reservation.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/widgets/w_qr.dart';

class ReservationDetailsPage extends StatefulWidget {
  final RestaurentReservationModel restaurentReservationModel;

  ReservationDetailsPage({
    this.restaurentReservationModel,
  });

  @override
  _ReservationDetailsPageState createState() => _ReservationDetailsPageState();
}

class _ReservationDetailsPageState extends State<ReservationDetailsPage> {
  @override
  void initState() {
    super.initState();
    Future.microtask(() => context.read<PQRReserevation>().setReservation(widget.restaurentReservationModel));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Config.primaryColor,
        appBar: AppBar(
          backgroundColor: Config.primaryColor,
          elevation: 0,
        ),
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                child: Card(
                  margin: EdgeInsets.all(20),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      height: 100,
                                      width: 100,
                                      decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                      child: CachedNetworkImage(
                                        imageUrl: widget.restaurentReservationModel.full,
                                        imageBuilder: (context, imageProvider) => Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(5),
                                            image: DecorationImage(
                                              image: imageProvider,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        placeholder: (context, url) => CircularProgressIndicator(),
                                        errorWidget: (context, url, error) => SizedBox(),
                                      ),
                                    ),
                                    SizedBox(width: 0.02.sh),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          '${widget.restaurentReservationModel.restaurant.name}',
                                          style: TextStyle(
                                              color: Colors.black87, fontSize: 38.sp, fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(height: 0.005.sh),
                                        Text(
                                          'Phone: ${widget.restaurentReservationModel.restaurant.phone}',
                                          style: TextStyle(color: Colors.black54, fontSize: 32.sp),
                                        ),
                                        Text(
                                          '${widget.restaurentReservationModel.restaurant.storeInformation}',
                                          style: TextStyle(color: Colors.black45, fontSize: 32.sp),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(height: 0.02.sh),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Date: ${DateFormat("dd MMM yyyy").format(widget.restaurentReservationModel.date)}',
                                      style: TextStyle(color: Colors.black54, fontSize: 36.sp),
                                    ),
                                    SizedBox(height: 0.005.sh),
                                    Text(
                                      'Time: ${widget.restaurentReservationModel.timeIn}',
                                      style: TextStyle(color: Colors.black54, fontSize: 36.sp),
                                    ),
                                    SizedBox(height: 0.005.sh),
                                    Text(
                                      'No. guest: ${widget.restaurentReservationModel.numberOfGuest}',
                                      style: TextStyle(color: Colors.black54, fontSize: 36.sp),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            color: Colors.black54,
                            height: 0.01.sh,
                          ),
                          Container(
                            constraints: BoxConstraints(maxHeight: 200),
                            child: SingleChildScrollView(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: List.generate(
                                    widget.restaurentReservationModel.myPreOrder.length,
                                    (index) => Row(
                                          children: [
                                            Text(
                                              '${widget.restaurentReservationModel.myPreOrder[index].quantity}x ',
                                              style: TextStyle(fontSize: 36.sp, color: Colors.black54),
                                            ),
                                            SizedBox(width: 10),
                                            Text(
                                              '${widget.restaurentReservationModel.myPreOrder[index].name}',
                                              style: TextStyle(fontSize: 36.sp, color: Colors.black54),
                                            )
                                          ],
                                        )),
                              ),
                            ),
                          ),
                          Divider(
                            color: Colors.black54,
                            height: 0.01.sh,
                          ),
                          Selector<PQRReserevation, bool>(
                            builder: (context, value, child) => value
                                ? Center(
                                    child: Container(
                                      height: 0.5.sw,
                                      width: 0.5.sw,
                                      child: FlareActor(
                                        "assets/images/success.flr",
                                        animation: "play",
                                        callback: (data) {
                                          context
                                              .read<PFoodListType>()
                                              .fetchFoodTypes(widget.restaurentReservationModel.restaurant.hashid);

                                          context.read<PRestaurantFoodCart>().fetchRCartList();
                                          context.read<PQRReserevation>().setScanStatus(false);

                                          ExtendedNavigator.of(context).replace('/restaurants/menu',
                                              arguments: RestCatogriesArguments(
                                                isFromScan: true,
                                                restId: widget.restaurentReservationModel.restaurant.hashid,
                                                restName: widget.restaurentReservationModel.restaurant.name,
                                              ));
                                        },
                                      ),
                                    ),
                                  )
                                : Padding(
                                    padding: const EdgeInsets.all(20.0),
                                    child: JQRWidget(
                                      qrCode: widget.restaurentReservationModel.qrCode,
                                      size: 0.3.sw,
                                    ),
                                  ),
                            selector: (_, qr) => qr.isScanned,
                          ),
                          Row(
                            children: [
                              Icon(Icons.info_outline),
                              SizedBox(width: 10),
                              Expanded(child: Text('Note: When you arrive, please present the QR Code to our waiter')),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
