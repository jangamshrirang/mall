import 'package:flutter/material.dart';
import 'package:top_sheet/top_sheet.dart';
import 'package:wblue_customer/env/config.dart';
class PaymentScreen extends StatefulWidget {
  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
            appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.chevron_left),
            onPressed: () {
              setState(() {
                Navigator.pop(context, false);
              });
            }),
        elevation: 0,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        title: Text(
          "Payment",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          iconInAppbar(icon: Icons.search, onTap: () {}),
          iconInAppbar(icon: Icons.shopping_cart, onTap: () {}),
          PopupMenuButton<String>(
            onSelected: choiceAction,
            itemBuilder: (BuildContext context) {
              return Constants.choices.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          )
        ],
      ),
      body: Container(
        width: size.width,
        color: Colors.white,
        child: Column(
        children: [
          SizedBox(
            height:100,
          ),
          Image.asset(
            "assets/images/nopayment.png",
            height: 100,
          
          ),
          SizedBox(
            height:50,
          ),
          Text(
            "No Payment option",
            style: TextStyle(
                color: Colors.black, fontSize: 25, fontWeight: FontWeight.normal),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 50,
          ),
          RaisedButton(
            onPressed: () {
             
            },
            textColor: Colors.white,
            padding: const EdgeInsets.all(0.0),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(2.0)),
            child: Container(
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: <Color>[
                      Color(0xFF0D47A1),
                      Color(0xFF1976D2),
                      Color(0xFF42A5F5),
                    ],
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(2.0))),
              padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
              child:
                  const Text('Continue Shopping', style: TextStyle(fontSize: 20)),
            ),
          ),
        ],
      ),
      ),
    );
  }
   Widget iconInAppbar({IconData icon, String label, Function onTap}) {
    return IconButton(icon: Icon(icon), onPressed: onTap);
  }
  //for pop up menu in app barr
  void choiceAction(String choice) {
    if (choice == Constants.Home) {
      print('Home');
    } else if (choice == Constants.Messages) {
      print('Messages');
    } else if (choice == Constants.MyAccount) {
      print('MyAccount');
    } else if (choice == Constants.NeedHelp) {
      print('NeedHelp');
    } else if (choice == Constants.Feedback) {
      print('Feedback');
    }
  }
}