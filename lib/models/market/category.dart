import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class Categories {
  static category() {
    List _category = [
      {
        'icon': MaterialIcons.devices,
        'title': 'Electronic Devices',
        'isSelected': true,
        'subCategories': [
          {
            'name': 'Mobiles',
            'groups': []
          },
          {
            'name': 'Tablets',
            'groups': []
          },
          {
            'name': 'Laptops/',
            'groups': []
          },
          {
            'name': 'Desktops',
            'groups': []
          },
          {
            'name': 'Security Cameras',
            'groups': []
          },
          {
            'name': 'Action/Video Cameras',
            'groups': []
          },
          {
            'name': 'Digital Cameras',
            'groups': []
          },
          {
            'name': 'Gaming Consoles',
            'groups': []
          },
          {
            'name': 'Gadgets',
            'groups': []
          },
        ]
      },
      {
        'icon': FontAwesome.television,
        'title': 'Electronic Accessories',
        'isSelected': false,
        'subCategories': [
          {
            'name': 'Mobiles Accessories',
            'groups': []
          },
          {
            'name': 'Audio',
            'groups': []
          },
          {
            'name': 'Computer Accessories',
            'groups': []
          },
          {
            'name': 'Camera Accessories',
            'groups': []
          },
          {
            'name': 'Storage',
            'groups': []
          },
          {
            'name': 'Printers',
            'groups': []
          },
          {
            'name': 'Computer Components',
            'groups': []
          },
          {
            'name': 'Network Components',
            'groups': []
          },
          {
            'name': 'Network Components',
            'groups': []
          },
          {
            'name': 'Wearable Technology',
            'groups': []
          },
          {
            'name': 'Console Accessories',
            'groups': []
          },
          {
            'name': 'Tablet Accessories',
            'groups': []
          },
        ]
      },
      {
        'icon': MaterialCommunityIcons.washing_machine,
        'title': 'TV & Home Application',
        'isSelected': false,
        'subCategories': [
          {
            'name': 'TV & Video Devices',
            'groups': []
          },
          {
            'name': 'TV Accessories',
            'groups': []
          },
          {
            'name': 'Home Audio',
            'groups': []
          },
          {
            'name': 'Cooling & Air Treatment',
            'groups': []
          },
          {
            'name': 'Small kitchen Appliances',
            'groups': []
          },
          {
            'name': 'Vacuums & Floor Care',
            'groups': []
          },
          {
            'name': 'Household Appliance',
            'groups': []
          },
          {
            'name': 'Personal Care Appliances',
            'groups': []
          },
          {
            'name': 'Large Appliances',
            'groups': []
          },
          {
            'name': 'Parts & Accessories',
            'groups': []
          },
        ]
      },
      {
        'icon': MaterialCommunityIcons.brush,
        'title': 'Health & Beauty',
        'isSelected': false,
        'subCategories': [
          {
            'name': 'Make-up',
            'groups': []
          },
          {
            'name': 'Skincare',
            'groups': []
          },
          {
            'name': 'Hair Care',
            'groups': []
          },
          {
            'name': 'Bath & Body',
            'groups': []
          },
          {
            'name': 'Personal Care',
            'groups': []
          },
          {
            'name': 'Men\'s Care',
            'groups': []
          },
          {
            'name': 'Fragrances',
            'groups': []
          },
          {
            'name': 'Beauty Tools',
            'groups': []
          },
          {
            'name': 'Food Supplements',
            'groups': []
          },
          {
            'name': 'Medical Supplies',
            'groups': []
          },
          {
            'name': 'Sexual Wellness',
            'groups': []
          },
          {
            'name': 'Adult Diapers & Incontinence',
            'groups': []
          },
        ]
      },
      {
        'icon': MaterialCommunityIcons.baby_carriage,
        'title': 'Babies & Toys',
        'isSelected': false,
        'subCategories': [
          {
            'name': 'Diapering & Potty',
            'groups': []
          },
          {
            'name': 'Milk Formula & Baby Food',
            'groups': []
          },
          {
            'name': 'Baby Fashion & Accessories',
            'groups': []
          },
          {
            'name': 'Feeding Essentials',
            'groups': []
          },
          {
            'name': 'Baby Gear',
            'groups': []
          },
          {
            'name': 'Baby Personal Care',
            'groups': []
          },
          {
            'name': 'Toys & Games',
            'groups': []
          },
          {
            'name': 'Electronic & Remote Control Toys',
            'groups': []
          },
          {
            'name': 'Sports Toys & Outdoor Play',
            'groups': []
          },
          {
            'name': 'Baby & Toddler Toys',
            'groups': []
          },
          {
            'name': 'Learning & Education',
            'groups': []
          }
        ]
      },
      {
        'icon': Ionicons.ios_cart,
        'title': 'Groceries & Pets',
        'isSelected': false,
        'subCategories': [
          {
            'name': 'Drinks',
            'groups': []
          },
          {
            'name': 'Beer, Wine & Spirits',
            'groups': []
          },
          {
            'name': 'Food Staples & Cooking',
            'groups': []
          },
          {
            'name': 'Breakfast Cereals & Spreads',
            'groups': []
          },
          {
            'name': 'Chocolate, Snacks & Sweets',
            'groups': []
          },
          {
            'name': 'Bakery',
            'groups': []
          },
          {
            'name': 'Fruit & Vegetables',
            'groups': []
          },
          {
            'name': 'Household Supplies',
            'groups': []
          },
          {
            'name': 'Pet Accessories',
            'groups': []
          },
          {
            'name': 'Pet Food',
            'groups': []
          },
          {
            'name': 'Pet Health Care',
            'groups': []
          },
        ]
      },
      {
        'icon': FontAwesome.home,
        'title': 'Home & Living',
        'isSelected': false,
        'subCategories': [
          {
            'name': 'Bath',
            'groups': []
          },
          {
            'name': 'Bedding',
            'groups': []
          },
          {
            'name': 'Furniture',
            'groups': []
          },
          {
            'name': 'Storage & Organisation',
            'groups': []
          },
          {
            'name': 'Laundry & Cleaning Equipment',
            'groups': []
          },
          {
            'name': 'Kitchen & Dining',
            'groups': []
          },
          {
            'name': 'Home Decor',
            'groups': []
          },
          {
            'name': 'Outdoor & Garden',
            'groups': []
          },
          {
            'name': 'Stationery & Craft',
            'groups': []
          },
          {
            'name': 'Tools & Home Improvement',
            'groups': []
          }
        ]
      },
      {
        'icon': FontAwesome.female,
        'title': 'Women\'s Fashion',
        'isSelected': false,
        'subCategories': [
          {
            'name': 'Women\'s Clothing',
            'groups': []
          },
          {
            'name': 'Women\'s Shoes',
            'groups': []
          },
          {
            'name': 'Lingerie, Sleep & Lounge',
            'groups': []
          },
          {
            'name': 'Accessories',
            'groups': []
          },
          {
            'name': 'Swimwear & Beachwear',
            'groups': []
          },
          {
            'name': 'Girls\' Clothing',
            'groups': []
          },
          {
            'name': 'Girls\' Shoes',
            'groups': []
          },
          {
            'name': 'Bedding',
            'groups': []
          },
          {
            'name': 'Women Bags',
            'groups': []
          },
          {
            'name': 'Kids Bags',
            'groups': []
          },
          {
            'name': 'Laptop Bags',
            'groups': []
          },
          {
            'name': 'Travel',
            'groups': []
          },
        ]
      },
      {
        'icon': FontAwesome.male,
        'title': 'Men\'s Fashion',
        'isSelected': false,
        'subCategories': [
          {
            'name': 'Men\'s Clothing',
            'groups': []
          },
          {
            'name': 'Men\'s Shoes',
            'groups': []
          },
          {
            'name': 'Underwear',
            'groups': []
          },
          {
            'name': 'Accessories',
            'groups': []
          },
          {
            'name': 'Boys\' Clothing',
            'groups': []
          },
          {
            'name': 'Boys\' Shoes',
            'groups': []
          },
          {
            'name': 'Kids Bags',
            'groups': []
          },
          {
            'name': 'Men Bags',
            'groups': []
          },
          {
            'name': 'Laptop Bags',
            'groups': []
          },
          {
            'name': 'Travel',
            'groups': []
          },
        ]
      },
      {
        'icon': Icons.watch,
        'title': 'Fashion Accessories',
        'isSelected': false,
        'subCategories': [
          {
            'name': 'Women Watches',
            'groups': []
          },
          {
            'name': 'Women\'s Fashion Jewellery',
            'groups': []
          },
          {
            'name': 'Women\'s Fine Jewellery',
            'groups': []
          },
          {
            'name': 'Men Watches',
            'groups': []
          },
          {
            'name': 'Men\'s Jewellery',
            'groups': []
          },
          {
            'name': 'Kids Watches',
            'groups': []
          },
          {
            'name': 'Kids\' Jewellery',
            'groups': []
          },
          {
            'name': 'Eyeglasses',
            'groups': []
          },
          {
            'name': 'Sunglasses',
            'groups': []
          },
          {
            'name': 'Contact Lenses',
            'groups': []
          },
        ]
      },
      {
        'icon': Icons.card_giftcard,
        'title': 'Sports & Lifestyle',
        'isSelected': false,
        'subCategories': [
          {
            'name': 'Outdoor Recreation',
            'groups': []
          },
          {
            'name': 'Sports Shoes',
            'groups': []
          },
          {
            'name': 'Sports Apparel',
            'groups': []
          },
          {
            'name': 'Exercise & Fitness Equipment',
            'groups': []
          },
          {
            'name': 'Sports Equipment',
            'groups': []
          },
          {
            'name': 'Team Merchandise/Fan Shop',
            'groups': []
          },
          {
            'name': 'Musical Instruments',
            'groups': []
          },
          {
            'name': 'Books',
            'groups': []
          },
          {
            'name': 'Music',
            'groups': []
          },
          {
            'name': 'Movies',
            'groups': []
          },
          {
            'name': 'Magazines',
            'groups': []
          },
        ]
      },
      {
        'icon': MaterialIcons.motorcycle,
        'title': 'Automotive & Motorcycles',
        'isSelected': false,
        'subCategories': [
          {
            'name': 'Automotive',
            'groups': []
          },
          {
            'name': 'Motorcycles',
            'groups': []
          },
          {
            'name': 'Vehicle Care',
            'groups': []
          },
          {
            'name': 'Automobile Sales & Reservation',
            'groups': []
          },
          {
            'name': 'Motorcycle Sales & Reservation',
            'groups': []
          },
          {
            'name': 'Fuels - Gasoline/petrol, Diesel',
            'groups': []
          },
          {
            'name': 'Automotive Oils & Fluids',
            'groups': []
          },
          {
            'name': 'Motorcycle Oil & Fluids',
            'groups': []
          },
        ]
      },
      {
        'icon': Foundation.ticket,
        'title': 'Digital Goods',
        'isSelected': false,
        'subCategories': [
          {
            'name': 'Mobile Load',
            'groups': []
          },
          {
            'name': 'Bills Payment',
            'groups': []
          },
          {
            'name': 'Food & Drinks',
            'groups': []
          },
          {
            'name': 'Activities & Entertainment',
            'groups': []
          },
          {
            'name': 'Beauty & Wellness',
            'groups': []
          },
          {
            'name': 'Beauty & Wellness',
            'groups': []
          },
          {
            'name': 'Financial Services',
            'groups': []
          },
          {
            'name': 'Travel',
            'groups': []
          },
        ]
      }
    ];
    return _category;
  }
}
