import 'package:auto_route/auto_route.dart';
import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/cart.dart';

import 'package:wblue_customer/pages/restaurant/cart/cart.dart';
import 'package:wblue_customer/models/Restaurant/cart.dart';
import 'package:wblue_customer/pages/restaurant/cart/cart.dart';
import 'package:wblue_customer/models/Restaurant/cart.dart';

import 'package:wblue_customer/pages/restaurant/cart/cart.dart';

class WCartWidget extends StatefulWidget {
  String fromScan, preOrder, delivery;
  Color color;
  WCartWidget({this.fromScan, this.preOrder, this.delivery,this.color});

  @override
  _WCartWidgetState createState() => _WCartWidgetState();
}

class _WCartWidgetState extends State<WCartWidget> {
  @override
  Widget build(BuildContext context) {
    int count = Cart('all').allCount();

    return Badge(
      elevation: 0,
      badgeContent: Text(
        count > 99 ? '99+' : count.toString(),
        style: TextStyle(fontSize: 8.0, color: Colors.transparent),
      ),
      badgeColor: Colors.transparent,
      position: BadgePosition.topStart(top: 8.0, start: 4.0),
      animationType: BadgeAnimationType.fade,
      child: IconButton(
        icon: Icon(
          Icons.shopping_basket,
          color: widget.color==null? Colors.white:widget.color,
        ),
        onPressed: () {
          widget.fromScan == "yes" ||
                  widget.preOrder == "yes" ||
                  widget.delivery == "yes" ||
                  stateManagment.deliverOrTakeString == "yes"
              ? Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RestaurantCartPage(
                            delivery: widget.delivery,
                            preOrder: widget.preOrder,
                            fromScan: widget.fromScan,
                          )))
              : ExtendedNavigator.of(context).root.push('/restaurants/cart');
        },
      ),
    );
  }
}
