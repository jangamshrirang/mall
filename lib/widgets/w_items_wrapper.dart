import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class WItemList extends StatelessWidget {
  final List items;
  final DisplayType widget;
  final int crossAxisCount;
  final int crossAxisCellCount;
  final double crossAxisSpacing;
  final double mainAxisSpacing;

  WItemList({
    @required this.widget,
    this.crossAxisCount: 1,
    this.mainAxisSpacing: 0.0,
    this.crossAxisSpacing: 0.0,
    this.crossAxisCellCount: 1,
    this.items,
  });

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        maxHeight: double.infinity,
      ),
      child: Container(
        child: StaggeredGridView.countBuilder(
          padding: EdgeInsets.zero,
          crossAxisSpacing: crossAxisSpacing,
          mainAxisSpacing: mainAxisSpacing,
          physics: ClampingScrollPhysics(),
          crossAxisCount: crossAxisCount,
          itemCount: items.length,
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) => widget(items[index], index),
          staggeredTileBuilder: (int index) => new StaggeredTile.fit(crossAxisCellCount),
        ),
      ),
    );
  }
}

typedef Widget DisplayType(Map items, index);
