import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';

Widget appBarWithIcons(BuildContext context, String title) {
  return AppBar(
    leading: IconButton(
        icon: Icon(Icons.chevron_left),
        onPressed: () {
          Navigator.pop(context, false);
        }),
    elevation: 0,
    iconTheme: IconThemeData(
      color: Colors.black, //change your color here
    ),
    title: Text(
      title,
      style: TextStyle(
        color: Colors.black,
        fontFamily: 'Montserrat',
      ),
    ),
    backgroundColor: Colors.white,
    actions: <Widget>[
      // iconInAppbar(icon: Icons.search, onTap: () {}),
      iconInAppbar(icon: Icons.shopping_cart, onTap: () {}),
      PopupMenuButton<String>(
        onSelected: choiceAction,
        itemBuilder: (BuildContext context) {
          return Constants.choices.map((String choice) {
            return PopupMenuItem<String>(
              value: choice,
              child: Text(choice),
            );
          }).toList();
        },
      )
    ],
  );
  //icons for app bar
}

iconInAppbar({IconData icon, String label, Function onTap}) {
  return IconButton(icon: Icon(icon), onPressed: onTap);
}

//for pop up menu in app barr
void choiceAction(String choice) {
  if (choice == Constants.Home) {
    print('Home');
  } else if (choice == Constants.Messages) {
    print('Messages');
  } else if (choice == Constants.MyAccount) {
    print('MyAccount');
  } else if (choice == Constants.NeedHelp) {
    print('NeedHelp');
  } else if (choice == Constants.Feedback) {
    print('Feedback');
  }
}

Widget appBarWithOutIcon(BuildContext context, String title,
    {Brightness brightness, centerTitle: true, List<Widget> actions}) {
  return AppBar(
    brightness: brightness,
    leading: IconButton(
        icon: Icon(Icons.chevron_left),
        onPressed: () {
          Navigator.pop(context, false);
        }),
    elevation: 0,
    actions: actions,
    centerTitle: centerTitle,
    iconTheme: IconThemeData(
      color: Colors.black, //change your color here
    ),
    title: Text(
      title,
      style: TextStyle(
        color: Colors.black,
        fontFamily: 'Montserrat',
      ),
    ),
    backgroundColor: Colors.white,
  );
}

Widget appBarWithSearchIcons(
    BuildContext context, String title, Function cartOnTap,
    {Brightness brightnessType: Brightness.dark}) {
  return AppBar(
    brightness: brightnessType,
    leading: cartOnTap != null
        ? IconButton(
            icon: Icon(Icons.chevron_left),
            onPressed: () {
              Navigator.pop(context, false);
            })
        : null,
    elevation: 0,
    iconTheme: IconThemeData(
      color: Colors.black, //change your color here
    ),
    title: Text(
      title,
      style: TextStyle(
        color: Colors.black,
        fontFamily: 'Montserrat',
      ),
    ),
    backgroundColor: Colors.white,
    actions: <Widget>[
      iconInAppbar(icon: Icons.search, onTap: () {}),
      iconInAppbar(icon: Icons.shopping_cart, onTap: cartOnTap),
      PopupMenuButton<String>(
        onSelected: choiceAction,
        itemBuilder: (BuildContext context) {
          return Constants.choices.map((String choice) {
            return PopupMenuItem<String>(
              value: choice,
              child: Text(choice),
            );
          }).toList();
        },
      )
    ],
  );
  //icons for app bar
}
