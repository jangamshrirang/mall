import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import '../../../env/config.dart';
import 'package:wblue_customer/providers/market/p_cart.dart';
import 'package:wblue_customer/providers/market/p_home.dart';
import 'package:wblue_customer/providers/market/p_view_product.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_image_product_card.dart';
import 'package:wblue_customer/widgets/w_rating.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

class ShippingBillingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        elevation: 0.0,
        leading: IconButton(
          onPressed: () => ExtendedNavigator.of(context).pop(),
          icon: Icon(MdiIcons.arrowLeft),
          color: Colors.black,
        ),
        centerTitle: false,
        title: AutoSizeText('Shipping & Billing', style: TextStyle(color: Colors.black)),
      ),
      body: SafeArea(
        child: ListView(
          physics: ClampingScrollPhysics(),
          padding: EdgeInsets.zero,
          children: [
            userInfo(),
            SizedBox(height: 10),
            Consumer<PMarketCart>(
              builder: (__, delivery, _) => Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AutoSizeText('Choose your delivery option'),
                      SizedBox(height: 8.0),
                      Row(
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () => context.read<PMarketCart>().setDelivery(0),
                              child: Card(
                                elevation: 0.0,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        width: 1,
                                        color: delivery.selectedDelivery == 0
                                            ? Config.primaryColor
                                            : Colors.grey.withOpacity(0.2)),
                                    borderRadius: BorderRadius.circular(20)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          AutoSizeText('QAR 150'),
                                          WImageWidget(
                                            placeholder: AssetImage('assets/images/market/cart/pig.png'),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 5.0),
                                      AutoSizeText(
                                        'Economy Delivery',
                                        maxLines: 1,
                                      ),
                                      SizedBox(height: 5.0),
                                      AutoSizeText('Est. Arrival'),
                                      AutoSizeText('11-20 Aug'),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () => context.read<PMarketCart>().setDelivery(1),
                              child: Card(
                                elevation: 0.0,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        width: 1,
                                        color: delivery.selectedDelivery == 1
                                            ? Config.primaryColor
                                            : Colors.grey.withOpacity(0.2)),
                                    borderRadius: BorderRadius.circular(20)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          AutoSizeText('QAR 175'),
                                          WImageWidget(
                                            placeholder: AssetImage('assets/images/market/cart/truck.png'),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 5.0),
                                      AutoSizeText(
                                        'Standard Delivery',
                                        maxLines: 1,
                                      ),
                                      SizedBox(height: 5.0),
                                      AutoSizeText('Est. Arrival'),
                                      AutoSizeText('10-15 Aug'),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () => context.read<PMarketCart>().setDelivery(2),
                              child: Card(
                                elevation: 0.0,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        width: 1,
                                        color: delivery.selectedDelivery == 2
                                            ? Config.primaryColor
                                            : Colors.grey.withOpacity(0.2)),
                                    borderRadius: BorderRadius.circular(20)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          AutoSizeText('QAR 200'),
                                          WImageWidget(
                                            placeholder: AssetImage('assets/images/market/cart/send.png'),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 5.0),
                                      AutoSizeText(
                                        'Express Delivery',
                                        maxLines: 1,
                                      ),
                                      SizedBox(height: 5.0),
                                      AutoSizeText('Est. Arrival'),
                                      AutoSizeText('6-7 Aug'),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Consumer<PMarketHome>(builder: (__, checkout, _) {
              Map product = checkout.products[0];
              return WProductImageCard(
                onTap: () {
                  context.read<PViewProduct>().setProduct(product);
                  ExtendedNavigator.of(context).push('/view-product');
                },
                isVertical: false,
                imageUrl: '${product['images'][0]['small']}',
                widget: Container(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AutoSizeText(
                      '${product['name']}',
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      minFontSize: 16,
                    ),
                    SizedBox(height: 10.0),
                    AutoSizeText(
                      '${product['shortDescription']}',
                      style: TextStyle(fontSize: 16),
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                      minFontSize: 14,
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      child: Row(
                        children: <Widget>[
                          AutoSizeText(
                            'QR ${product['newPrice']}',
                            style: TextStyle(fontSize: 18, color: Colors.redAccent),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            minFontSize: 16,
                          ),
                          SizedBox(width: 8.0),
                          product['oldPrice'] != null
                              ? Expanded(
                                  child: AutoSizeText(
                                    'QR ${product['oldPrice']}',
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.grey, decoration: TextDecoration.lineThrough),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    minFontSize: 16,
                                  ),
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Row(
                      children: [
                        Icon(
                          MdiIcons.checkboxBlankCircle,
                          size: 16.0,
                          color: product['availibilityCount'] > 0
                              ? Colors.green.withOpacity(0.9)
                              : Colors.red.withOpacity(0.9),
                        ),
                        SizedBox(width: 8.0),
                        AutoSizeText(
                          '${product['availibilityCount'] > 0 ? 'In' : 'Out'} stock, ${product['availibilityCount'] > 1 ? '${product['availibilityCount']} units' : '${product['availibilityCount']} unit'}',
                          style: TextStyle(
                              fontSize: 16,
                              color: product['availibilityCount'] > 0
                                  ? Colors.green.withOpacity(0.9)
                                  : Colors.red.withOpacity(0.9)),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          minFontSize: 16,
                        ),
                      ],
                    ),
                    SizedBox(height: 3.0),
                    Row(
                      children: [
                        WRatingsWidget(product['ratingsValue'] / 100),
                        SizedBox(width: 2),
                        AutoSizeText(
                          '(${product['ratingsCount']})',
                          style: TextStyle(fontSize: 16, color: Colors.grey.withOpacity(0.9)),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          minFontSize: 16,
                        ),
                      ],
                    ),
                    SizedBox(height: 3.0),
                    AutoSizeText(
                      '15 Reviews',
                      style: TextStyle(fontSize: 16, color: Colors.grey),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      minFontSize: 16,
                    ),
                  ],
                )),
              );
            }),
            Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AutoSizeText('Order Instruction'),
                    SizedBox(height: 8.0),
                    TextField(
                        decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Config.primaryColor, width: 0.5),
                      ),
                      contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 0.5),
                      ),
                      hintText: 'Enter your instruction',
                    ))
                  ],
                ),
              ),
            ),
            SizedBox(height: 2),
            Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AutoSizeText('Order Summary'),
                    SizedBox(height: 8.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        AutoSizeText('Subtotal (1 items)', style: TextStyle(color: Colors.black54)),
                        AutoSizeText('QAR 175', style: TextStyle(color: Colors.black54)),
                      ],
                    ),
                    SizedBox(height: 5.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        AutoSizeText('Shipping Fee (Express)', style: TextStyle(color: Colors.black54)),
                        AutoSizeText('QAR 150', style: TextStyle(color: Colors.black54)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: TextField(
                          decoration: new InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Config.primaryColor, width: 0.5),
                        ),
                        contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
                        enabledBorder: OutlineInputBorder(
                          borderRadius:
                              BorderRadius.only(bottomLeft: Radius.circular(10.0), topLeft: Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 0.5),
                        ),
                        hintText: 'Enter Voucher Code',
                      )),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.only(topRight: Radius.circular(10.0), bottomRight: Radius.circular(10.0)),
                        color: Config.primaryColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: AutoSizeText('APPLY', style: TextStyle(color: Colors.white)),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Consumer<PMarketCart>(
              builder: (__, total, _) => Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AutoSizeText('Total Amount'),
                          AutoSizeText('QAR ${total.totalAllItems}',
                              style:
                                  TextStyle(fontWeight: FontWeight.bold, fontSize: 28.0, color: Config.primaryColor)),
                        ],
                      ),
                      WRoundedButton(
                        onCustomButtonPressed: () => ExtendedNavigator.of(context).push('/market/payment'),
                        child: AutoSizeText('Check Out'),
                        elevation: 0.0,
                        labelColor: Colors.white,
                        btnColor: Config.primaryColor,
                        borderColor: Config.primaryColor,
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget userInfo() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AutoSizeText('Shipping Method', style: TextStyle(color: Colors.black38)),
            SizedBox(height: 20),
            Row(
              children: [
                Icon(MdiIcons.packageVariantClosed, color: Colors.black54),
                SizedBox(width: 8.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AutoSizeText('Standard Delivery', style: TextStyle(color: Colors.black54)),
                    AutoSizeText('6-10 day(s)', style: TextStyle(color: Colors.black54, fontSize: 8.0)),
                  ],
                ),
              ],
            ),
            SizedBox(height: 10.0),
            Row(
              children: [
                Icon(MdiIcons.mapMarker, color: Colors.black54),
                SizedBox(width: 8.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          AutoSizeText('Juan Dela Cruz', style: TextStyle(color: Colors.black54)),
                          AutoSizeText('Edit'),
                        ],
                      ),
                      AutoSizeText('Al Khalidiya St. Najma, Doha, State of Qatar',
                          style: TextStyle(color: Colors.black54, fontSize: 8.0)),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 10.0),
            Row(
              children: [
                Icon(MdiIcons.cellphoneIphone, color: Colors.black54),
                SizedBox(width: 8.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          AutoSizeText('5094 1234', style: TextStyle(color: Colors.black54)),
                          AutoSizeText('Edit'),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 10.0),
            Row(
              children: [
                Icon(MdiIcons.email, color: Colors.black54),
                SizedBox(width: 8.0),
                Expanded(
                  child: AutoSizeText('juandelacruz@gmail.com', style: TextStyle(color: Colors.black54)),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
