import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_image.dart';

class WMovieItemWidget extends StatelessWidget {
  final bool isCompact;

  const WMovieItemWidget({
    Key key,
    this.isCompact: false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        ExtendedNavigator.of(context).root.push('/cinemas/movie-details');
      },
      child: isCompact ? _compact() : _expanded(),
    );
  }

  Widget _expanded() {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Container(
            height: 80.0,
            width: 60.0,
            child: WImageWidget(
              url: '',
              placeholder: AssetImage('assets/images/samples/movie.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(width: 8.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Movie Title',
                  style: TextStyle(fontSize: 16.0),
                ),
                SizedBox(height: 8.0),
                Text(
                  Global.money(80),
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 4.0),
                Text(
                  'Upnext 10:00 AM',
                  style: TextStyle(fontSize: 10.0),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      'Details',
                      style: TextStyle(fontSize: 10.0, color: Colors.amber),
                    ),
                    Icon(
                      Icons.arrow_right,
                      color: Colors.amber,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _compact() {
    return Card(
      clipBehavior: Clip.hardEdge,
      child: Container(
        width: 120.0,
        child: Column(
          children: <Widget>[
            Container(
              width: 120.0,
              height: 140.0,
              child: WImageWidget(
                url: '',
                placeholder: AssetImage('assets/images/samples/movie.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(height: 8.0),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                'Movie Name  Long sdadasddsadsa',
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 14.0),
              ),
            ),
            Text(
              Global.money(80),
              style: TextStyle(color: Colors.amber, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
