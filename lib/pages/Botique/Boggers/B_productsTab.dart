import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/Brands/ProductExpantion.dart';

class B_ProductsTab extends StatefulWidget {
  _B_ProductsTabState createState() => new _B_ProductsTabState();
}

class _B_ProductsTabState extends State<B_ProductsTab> {
  final ScrollController _scrollController = new ScrollController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildListDelegate(List.generate(1, (index) {
            return Column(
              children: [
                Wrap(
                    spacing: 0,
                    children: List.generate(
                        10, (index) => bProducts('assets/botique/p6.png'))),
              ],
            );
          })),
        )
      ],
    );
  }

  bProducts(String image) {
    var size = MediaQuery.of(context).size;
    return InkWell(
      onTap: () {},
      child: Container(
          width: size.width * 0.3,
          height: size.height * 0.25,
          margin: EdgeInsets.only(bottom: 10, left: 10, top: 5, right: 0),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 1,
                blurRadius: 3,
                offset: Offset(1, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.all(Radius.circular(5)),
          ),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: size.height * 0.01),
                height: size.height * 0.1,
                width: size.width * 0.28,
                decoration: BoxDecoration(
                  // color: Colors.red,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                child: Image.asset(
                  image,
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                  height: size.height * 0.1,
                  width: size.width,
                  margin: EdgeInsets.only(
                      top: size.height * 0.01, left: size.width * 0.01),
                  //  color: Colors.black,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Beats headpones",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.normal,
                            color: Colors.black54,
                            fontSize: size.height * 0.025),
                      ),
                      Spacer(),
                      Text(
                        "QAR 300.00",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.normal,
                            color: Color(0xff008DD2),
                            fontSize: size.height * 0.025),
                      ),
                      Spacer(),
                      Text(
                        "15 Reviwes",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black45,
                            fontSize: size.height * 0.02),
                      ),
                    ],
                  ))
            ],
          )),
    );
  }
}
