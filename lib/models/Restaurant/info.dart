class RestaurantInfo {
  RestaurantInfo({
    this.name,
    this.businessTypeId,
    this.storeEmail,
    this.storeInformation,
    this.phone,
    this.hashid,
    this.ratings,
    this.followers,
  });

  String name;
  int businessTypeId;
  String storeEmail;
  String storeInformation;
  String phone;
  String hashid;
  Ratings ratings;
  int followers;

  factory RestaurantInfo.fromJson(Map<String, dynamic> json) => RestaurantInfo(
        name: json["name"] == null ? null : json["name"],
        businessTypeId:
            json["business_type_id"] == null ? null : json["business_type_id"],
        storeEmail: json["store_email"] == null ? null : json["store_email"],
        storeInformation: json["store_information"] == null
            ? null
            : json["store_information"],
        phone: json["phone"] == null ? null : json["phone"],
        hashid: json["hashid"] == null ? null : json["hashid"],
        ratings:
            json["ratings"] == null ? null : Ratings.fromJson(json["ratings"]),
        followers: json["followers"] == null ? null : json["followers"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "business_type_id": businessTypeId == null ? null : businessTypeId,
        "store_email": storeEmail == null ? null : storeEmail,
        "store_information": storeInformation == null ? null : storeInformation,
        "phone": phone == null ? null : phone,
        "hashid": hashid == null ? null : hashid,
        "ratings": ratings == null ? null : ratings.toJson(),
        "followers": followers == null ? null : followers,
      };
}

class Ratings {
  Ratings({
    this.count,
    this.average,
  });

  int count;
  int average;

  factory Ratings.fromJson(Map<String, dynamic> json) => Ratings(
        count: json["count"] == null ? null : json["count"],
        average: json["average"] == null ? null : json["average"],
      );

  Map<String, dynamic> toJson() => {
        "count": count == null ? null : count,
        "average": average == null ? null : average,
      };
}

class RestaurantLocationModel {
  RestaurantLocationModel(
      {this.businessTypeId,
      this.latitude,
      this.longitude,
      this.hashid,
      this.name});

  int businessTypeId;
  String latitude;
  String longitude;
  String hashid;
  String name;

  factory RestaurantLocationModel.fromJson(Map<String, dynamic> json) =>
      RestaurantLocationModel(
        businessTypeId:
            json["business_type_id"] == null ? null : json["business_type_id"],
        latitude: json["latitude"] == null ? null : json["latitude"],
        longitude: json["longitude"] == null ? null : json["longitude"],
        hashid: json["hashid"] == null ? null : json["hashid"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "business_type_id": businessTypeId == null ? null : businessTypeId,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "hashid": hashid == null ? null : hashid,
      };
}
