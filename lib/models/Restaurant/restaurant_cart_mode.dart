class RestaurantCartModel {
  RestaurantCartModel(
      {this.customer,
      this.tableNo,
      this.reservationHashId,
      this.cartHashId,
      this.restaurant,
      this.food: const [],
      this.totalInfo,
      this.deliveryFee});

  String customer;
  dynamic tableNo;
  String reservationHashId;
  String cartHashId;
  double deliveryFee;
  Restaurant restaurant;
  List<RestaurantCartFoodModel> food;
  RestaurantCartTotalInfoModel totalInfo;

  factory RestaurantCartModel.fromJson(Map<String, dynamic> json) => RestaurantCartModel(
        customer: json["customer"],
        tableNo: json["table_no"],
        deliveryFee: json["deliveryFee"],
        reservationHashId: json["reservation_hash_id"],
        cartHashId: json["cart_hash_id"],
        restaurant: Restaurant.fromJson(json["restaurant"]),
        food: List<RestaurantCartFoodModel>.from(json["food"].map((x) => RestaurantCartFoodModel.fromJson(x))),
        totalInfo: RestaurantCartTotalInfoModel.fromJson(json["total_info"]),
      );

  Map<String, dynamic> toJson() => {
        "customer": customer,
        "table_no": tableNo,
        "deliveryFee": deliveryFee,
        "reservation_hash_id": reservationHashId,
        "cart_hash_id": cartHashId,
        "restaurant": restaurant.toJson(),
        "food": List<dynamic>.from(food.map((x) => x.toJson())),
        "total_info": totalInfo.toJson(),
      };
}

class RestaurantCartFoodModel {
  RestaurantCartFoodModel({
    this.quantity,
    this.onCheckout,
    this.isOrdered,
    this.foodStatus,
    this.foodInfo,
    this.foodvariation,
    this.totalamount,
  });

  int quantity;
  int onCheckout;
  int isOrdered;
  int foodStatus;
  FoodInfo foodInfo;
  List<FoodVariationModel> foodvariation;
  String totalamount;

  factory RestaurantCartFoodModel.fromJson(Map<String, dynamic> json) => RestaurantCartFoodModel(
        quantity: json["quantity"],
        onCheckout: json["on_checkout"],
        isOrdered: json["is_ordered"],
        foodStatus: json["food_status"],
        foodInfo: FoodInfo.fromJson(json["food_info"]),
        foodvariation: List<FoodVariationModel>.from(json["foodvariation"].map((x) => FoodVariationModel.fromJson(x))),
        totalamount: json["totalamount"],
      );

  Map<String, dynamic> toJson() => {
        "quantity": quantity,
        "on_checkout": onCheckout,
        "is_ordered": isOrdered,
        "food_status": foodStatus,
        "food_info": foodInfo.toJson(),
        "foodvariation": List<dynamic>.from(foodvariation.map((x) => x)),
        "totalamount": totalamount,
      };
}

class FoodInfo {
  FoodInfo({
    this.hashId,
    this.name,
    this.type,
    this.price,
    this.description,
    this.full,
    this.thumbnail,
  });

  String hashId;
  String name;
  dynamic type;
  String price;
  String description;
  String full;
  String thumbnail;

  factory FoodInfo.fromJson(Map<String, dynamic> json) => FoodInfo(
        hashId: json["hash_id"],
        name: json["name"],
        type: json["type"],
        price: json["price"],
        description: json["description"],
        full: json["full"],
        thumbnail: json["thumbnail"],
      );

  Map<String, dynamic> toJson() => {
        "hash_id": hashId,
        "name": name,
        "type": type,
        "price": price,
        "description": description,
        "full": full,
        "thumbnail": thumbnail,
      };
}

class Restaurant {
  Restaurant({this.name, this.hashid, this.totalFoods, this.isOpened, this.latitude, this.longitude});

  String name;
  String hashid;
  String latitude;
  String longitude;
  int totalFoods;
  IsOpened isOpened;

  factory Restaurant.fromJson(Map<String, dynamic> json) => Restaurant(
        name: json["name"],
        hashid: json["hashid"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        totalFoods: json["total_foods"],
        isOpened: json["is_opened"] == null ? null : IsOpened.fromJson(json["is_opened"]),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "hashid": hashid,
        "latitude": latitude,
        "longitude": longitude,
        "total_foods": totalFoods,
        "is_opened": isOpened.toJson(),
      };
}

class IsOpened {
  IsOpened({
    this.now,
    this.status,
    this.next,
  });

  String now;
  String status;
  String next;

  factory IsOpened.fromJson(Map<String, dynamic> json) => IsOpened(
        now: json["now"],
        status: json["status"],
        next: json["next"],
      );

  Map<String, dynamic> toJson() => {
        "now": now,
        "status": status,
        "next": next,
      };
}

class RestaurantCartTotalInfoModel {
  RestaurantCartTotalInfoModel({
    this.total: '0.00',
    this.totalCount,
  });

  String total;
  int totalCount;

  factory RestaurantCartTotalInfoModel.fromJson(Map<String, dynamic> json) => RestaurantCartTotalInfoModel(
        total: json["total"],
        totalCount: json["total_count"],
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "total_count": totalCount,
      };
}

class FoodVariationModel {
  FoodVariationModel({
    this.amount,
    this.hashId,
    this.variationTypeSelected,
  });

  int amount;
  String hashId;
  VariationTypeSelected variationTypeSelected;

  factory FoodVariationModel.fromJson(Map<String, dynamic> json) => FoodVariationModel(
        amount: json["amount"],
        hashId: json["hash_id"],
        variationTypeSelected: VariationTypeSelected.fromJson(json["variation_type_selected"]),
      );

  Map<String, dynamic> toJson() => {
        "amount": amount,
        "hash_id": hashId,
        "variation_type_selected": variationTypeSelected.toJson(),
      };
}

class VariationTypeSelected {
  VariationTypeSelected({
    this.name,
    this.price,
    this.hashid,
  });

  String name;
  int price;
  String hashid;

  factory VariationTypeSelected.fromJson(Map<String, dynamic> json) => VariationTypeSelected(
        name: json["name"],
        price: json["price"],
        hashid: json["hashid"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "price": price,
        "hashid": hashid,
      };
}
