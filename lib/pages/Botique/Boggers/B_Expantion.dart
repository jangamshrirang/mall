import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

import 'B_Exp_TabBAr.dart';

class BloggersExpanstion extends StatefulWidget {
  @override
  _BloggersExpanstionState createState() => _BloggersExpanstionState();
}

class _BloggersExpanstionState extends State<BloggersExpanstion> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin:
              EdgeInsets.only(left: size.width * 0.02, top: size.height * 0.2,),
          width: size.width * 0.95,
          height: size.height * 0.27,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: size.height * 0.12,
                        width: size.width * 0.3,
                        child: bloggerImages(
                            'assets/botique/b3.png'),
                      ),
                      SizedBox(
                        height: size.height * 0.01,
                      ),
                      text("QQQ", Colors.black, FontWeight.normal,
                          size.width * 0.05),
                      text("Blogger", Colors.black, FontWeight.normal,
                          size.width * 0.05),
                      SizedBox(
                        height: size.height * 0.005,
                      ),
                      Row(
                        children: [
                          SizedBox(width: size.width * 0.03),
                          richText(" Doha Qatar", 'assets/botique/location.png',
                              Colors.black45, size.width * 0.02),
                        ],
                      ),
                      SizedBox(
                        height: size.height * 0.005,
                      ),
                      Row(
                        children: [
                          SizedBox(width: size.width * 0.03),
                          richText(
                              " Will be deleiverd in 15 min",
                              'assets/botique/location.png',
                              Colors.black45,
                              size.width * 0.02),
                        ],
                      )
                    ],
                  ), //divider

                  Container(
          margin: EdgeInsets.only(
                        left: size.width * 0.01, ),
                    width: 1,
                    height: size.height * 0.2,
                    color: Colors.black12,
                  ), //second half
                  Container(
                    width: size.width * 0.25,
                    height: size.height * 0.20,
                    //color: Colors.red,
                    margin: EdgeInsets.only(
                        left: size.width * 0.02, top: size.height * 0.03),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        richText(" 598 ", 'assets/botique/products.png',
                            Colors.black, size.width * 0.03),
                        text("Products", Colors.black45, FontWeight.normal,
                            size.width * 0.02),
                        richText(" 446K ", 'assets/botique/follwers.png',
                            Colors.black, size.width * 0.03),
                        text("Followers", Colors.black45, FontWeight.normal,
                            size.width * 0.02),
                      
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            iocn(Icons.favorite_border),
                            iocn(Icons.bookmark_border),
                            iocn(Icons.bubble_chart),
                          ],
                        ),
                      
                        button("Follow", size.height * 0.03, size.width * 0.2,
                            () {}),
                   
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  bloggerImages(String image) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(
        left: size.width * 0.02,
        top: size.height * 0.01,
      ),
      height: size.height * 0.08,
      width: size.width * 0.18,
      decoration: BoxDecoration(
        color: Colors.transparent,
        image: DecorationImage(
            image: AssetImage(
              image,
            ),
            fit: BoxFit.fill),
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
    );
  }

  richText(
    String text1,
    image,
    Color color1,
    double imagesize,
  ) {
    var size = MediaQuery.of(context).size;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(top: size.height * 0.004),
          width: imagesize,
          child: Image.asset(
            image,
          ),
        ),
        text(text1, color1, FontWeight.bold, 0.0)
      ],
    );
  }

  text(String title, Color color, FontWeight fontWeight, double leftWidht) {
    var size = MediaQuery.of(context).size;
    return Container(
        // color: Colors.redAccent,
        margin: EdgeInsets.only(top: 3, left: leftWidht),
        width: size.width * 0.2,
        child: Text(
          title,
          textAlign: TextAlign.start,
          style: TextStyle(
              color: color,
              fontWeight: fontWeight,
              fontSize: size.height * 0.015),
        ));
  }

  button(String title, double heigt, width, Function ontap) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: ontap,
      child: Container(
        margin: EdgeInsets.only(top: size.height * 0.02),
        height: heigt,
        width: width,
        decoration: const BoxDecoration(
            color: Color(0xffFD1450),
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        child: Center(
            child: Text(title,
                style: TextStyle(
                    fontSize: size.height * 0.02, color: Colors.white))),
      ),
    );
  }

  iocn(IconData icons) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(top: size.height * 0.015),
      height: size.height * 0.03,
      width: size.width * 0.08,
      child: MaterialButton(
        onPressed: () {},
        color: Colors.grey[300],
        textColor: Colors.red,
        child: Icon(
          icons,
          size: 15,
        ),
        padding: EdgeInsets.all(0),
        shape: CircleBorder(),
      ),
    );
  }
}
