import 'dart:async';
import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart' as Path;
import 'package:wblue_customer/models/user.dart';

class FireStoreClass {
  static final FirebaseFirestore _db = FirebaseFirestore.instance;
  static final liveCollection = 'liveuser';
  static final userCollection = 'users';
  static final contentCollection = 'content';

  static void createLiveUser({id, userid, name, time, image, contentImage, contentTitle, viewer: 0}) async {
    print('createLiveUser');

    FireStoreClass.uploadThumbnail(id: userid, contentThumbanil: contentImage);

    final snapShot = await _db.collection(liveCollection).doc(userid).get();

    if (snapShot.exists) {
      await _db.collection(liveCollection).doc(userid).update({
        'channel': id,
        'name': name,
        'time': time,
        'userid': userid,
        'image': image,
        'title': contentTitle,
        'content_thumbnail': '',
        'viewer': viewer
      });
    } else {
      await _db.collection(liveCollection).doc(userid).set({
        'channel': id,
        'name': name,
        'time': time,
        'userid': userid,
        'image': image,
        'title': contentTitle,
        'content_thumbnail': '',
        'viewer': viewer
      }).catchError((onError) {
        print('onError: $onError');
      });
    }
  }

  static void updateViewer({username, viewer: 0}) async {
    final snapShot = await _db.collection(liveCollection).doc(username).get();
    if (snapShot.exists) {
      await _db.collection(liveCollection).doc(username).update({'viewer': viewer});
    }
  }

  static uploadThumbnail({String id, File contentThumbanil}) async {
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child('$id/${Path.basename(contentThumbanil.path)}');

    try {
      StorageUploadTask uploadTask = storageReference.putFile(contentThumbanil);
      await uploadTask.onComplete.then((value) async => {
            await storageReference.getDownloadURL().then((fileURL) async {
              final snapShot = await _db.collection(liveCollection).doc(id).get();
              if (snapShot.exists) {
                await _db.collection(liveCollection).doc(id).update({
                  'content_thumbnail': fileURL,
                });
              }
            })
          });
    } catch (e) {
      BotToast.showText(text: '${e.error}');
    }
  }

  static Future<String> getImage({username}) async {
    print('username - $username');

    final snapShot = await _db.collection(userCollection).doc(username).get();
    print('snapShot.data - ${snapShot.data}');
    return snapShot.get('image');
  }

  static Future<String> getName({username}) async {
    final snapShot = await _db.collection(userCollection).doc(username).get();
    return snapShot.get('name');
  }

  static Future<bool> createFirebaseUserContent({UserModel user}) async {
    try {
      final snapShot = await _db.collection(userCollection).doc(user.hashid).get();

      if (snapShot.exists) {
        await _db
            .collection(userCollection)
            .doc(user.hashid)
            .update({'id': user.hashid, 'name': user.name, 'image': user.full});
      } else {
        await _db
            .collection(userCollection)
            .doc(user.hashid)
            .set({'id': user.hashid, 'name': user.name, 'image': user.full});
      }
      return true;
    } catch (err) {
      return false;
    }
  }

  static void deleteUser({username}) async {
    final snapShot = await _db.collection(liveCollection).doc(username).get();
    if (!snapShot.exists) return;
    await _db.collection('liveuser').doc(username).delete();
  }
}
