import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/MyOrderExpantionModel.dart';
import 'package:wblue_customer/providers/restaurant/p_MyOrdersExpanion.dart';
import 'package:wblue_customer/providers/socket/driverCoordinates.dart';
import 'package:wblue_customer/providers/socket/orderStatus.dart';
import 'package:wblue_customer/widgets/w_loading.dart';
import 'package:wblue_customer/widgets/w_qr.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class MyOrderCardExp extends StatefulWidget {
  String rname,
      rImage,
      status,
      orderNo,
      date,
      time,
      orderType,
      paymentType,
      dName,
      dRatings,
      estimatedTime,
      total,
      subTotal,
      serviceCharge,
      qrCode,
      orderHashID,
      cNumber;
  List foodList;
  int noOfDeliveries;
  MyOrderCardExp({
    this.rname,
    this.rImage,
    this.status,
    this.orderNo,
    this.date,
    this.time,
    this.orderType,
    this.foodList,
    this.paymentType,
    this.dName,
    this.dRatings,
    this.noOfDeliveries,
    this.estimatedTime,
    this.total,
    this.subTotal,
    this.serviceCharge,
    this.qrCode,
    this.orderHashID,
    this.cNumber,
  });
  @override
  _MyOrderCardExpState createState() => _MyOrderCardExpState();
}

class _MyOrderCardExpState extends State<MyOrderCardExp> {
  String updatedStatus;
  @override
  void initState() {
    // TODO: implement initState
    Future.microtask(() => context
        .read<MyOrderExpantionHelper>()
        .fetchMyOrderExpantionDetails(widget.orderHashID));
    gettingUserDetails();
    super.initState();
  }

  String userHashId;
  int count = 0;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        body: Container(
      margin:
          EdgeInsets.only(left: size.width * 0.05, right: size.width * 0.05),
      child:
          Consumer<MyOrderExpantionHelper>(builder: (context, myOrder, child) {
        if (!myOrder.isLoading) {
          MyOrderExpantionModel myOrderExpantionModel =
              MyOrderExpantionModel.fromJson(myOrder.myOrderExpantion);

          return SingleChildScrollView(
            child: Column(
              children: [
                //restaurent Container
                SizedBox(height: size.height * 0.01),
                dividerWithText("   Restaurent  "),
                Consumer<DriverCoordinatesScocket>(
                    //this is for refresh the above api to get driver numbre
                    builder: (_, driverCoordinates, __) {
                  if (widget.orderHashID ==
                          driverCoordinates.driverCoordinates['order_hashid'] &&
                      userHashId ==
                          driverCoordinates.driverCoordinates['user_hashid']) {
                    context
                        .read<DriverCoordinatesScocket>()
                        .setDriverCoordinatesUpdated(false);
                    count++;
                    count == 1
                        ? context
                            .read<MyOrderExpantionHelper>()
                            .fetchMyOrderExpantionDetails(widget.orderHashID)
                        : print("shoulf not work at this count:  $count");
                    return Text("");
                  } else {
                    return Text("");
                  }
                }),
                ///////////
                SizedBox(height: size.height * 0.01),
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(color: Colors.black12)),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          imageWithColumnText(
                              widget.rname,
                              updatedStatus == null
                                  ? widget.status
                                  : updatedStatus,
                              "Order No:${myOrderExpantionModel.orderNumber}",
                              true),
                          Container(
                              height: size.height * 0.06,
                              width: size.width * 0.4,
                              child: Consumer<OrderStatusScocket>(
                                  builder: (_, orderStatus, __) {
                                if (orderStatus.isUpdated) {
                                  updatedStatus =
                                      orderStatus.updatedstatusData['status'];
                                  return FlareActor(
                                      //
                                      orderStatus.updatedstatusData['status'] ==
                                              "Pending"
                                          ? "assets/flares/restaurant/glass-hour.flr"
                                          : orderStatus.updatedstatusData[
                                                          'status'] ==
                                                      "Ready To Deliver" ||
                                                  orderStatus.updatedstatusData[
                                                          'status'] ==
                                                      "Delivering"
                                              ? "assets/flares/restaurant/delivery-man.flr"
                                              : orderStatus.updatedstatusData[
                                                          'status'] ==
                                                      "Preparing"
                                                  ? "assets/flares/landing/cooking.flr"
                                                  : "assets/flares/restaurant/glass-hour.flr",
                                      animation: "normal-loop");
                                } else {
                                  return FlareActor(
                                      //
                                      widget.status == "Pending"
                                          ? "assets/flares/restaurant/glass-hour.flr"
                                          : widget.status ==
                                                      "Ready To Deliver" ||
                                                  widget.status == "Delivering"
                                              ? "assets/flares/restaurant/delivery-man.flr"
                                              : widget.status == "Preparing"
                                                  ? "assets/flares/landing/cooking.flr"
                                                  : widget.status == "Delivered"
                                                      ? "assets/flares/restaurant/success.flr"
                                                      : "assets/flares/restaurant/glass-hour.flr", //
                                      animation: "normal-loop");
                                }
                              })),
                        ],
                      ),
                      SizedBox(height: size.height * 0.01),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            iconContainer(
                                widget.date.toString().substring(0, 10),
                                Icons.calendar_today),
                            iconContainer(widget.time, Icons.watch_later),
                            iconContainer(myOrderExpantionModel.orderTypeInfo,
                                Icons.dinner_dining),
                          ],
                        ),
                      ),
                      SizedBox(height: size.height * 0.01),
                    ],
                  ),
                ),
                SizedBox(height: size.height * 0.015),
                dividerWithText("   Please show QR code to driver  "),
                SizedBox(height: size.height * 0.01),
                Container(
                  padding: EdgeInsets.only(
                    top: 10,
                    bottom: 10,
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(color: Colors.black12)),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        JQRWidget(
                          qrCode: myOrderExpantionModel.qrcode,
                          size: size.height * 0.12,
                        ),
                        GestureDetector(
                          onTap: () {
                            launch("tel:${myOrderExpantionModel.driverNumber}");
                          },
                          child: text(" Contact Driver ", Config.primaryColor,
                              FontWeight.bold, size.height * 0.018),
                        )
                      ]),
                ),

                SizedBox(height: size.height * 0.015),
                dividerWithText("   Delivery Address  "),
                SizedBox(height: size.height * 0.015),
                Container(
                  width: size.width * 0.9,
                  padding: EdgeInsets.only(left: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(color: Colors.black12)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: size.height * 0.01),
                      text("  Jeo Bin", Colors.black, FontWeight.bold,
                          size.height * 0.025),
                      SizedBox(height: size.height * 0.005),
                      // text(
                      //     "  Mobile number : ${stateManagment.userMoileNumber}",
                      //     Colors.black,
                      //     FontWeight.normal,
                      //     size.height * 0.025),
                      SizedBox(height: size.height * 0.005),
                      text("  Payment :${myOrderExpantionModel.paymentType}",
                          Colors.black, FontWeight.normal, size.height * 0.025),
                      SizedBox(height: size.height * 0.01),
                    ],
                  ),
                ),
                SizedBox(height: size.height * 0.015),
                dividerWithText("   Item List  "),
                SizedBox(height: size.height * 0.015),
                Wrap(
                  children: List.generate(
                      myOrderExpantionModel.orderList.length, (index) {
                    return itemListCard(
                        "https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Mnx8fGVufDB8fHw%3D&w=1000&q=80",
                        // myOrderExpantionModel.orderList[index].foodInfo.full,
                        myOrderExpantionModel.orderList[index].name,
                        "${myOrderExpantionModel.orderList[index].quantity}",
                        "QAR ${myOrderExpantionModel.orderList[index].price}",
                        " Total ${myOrderExpantionModel.orderList[index].quantity * double.parse(myOrderExpantionModel.orderList[index].price)}");
                  }),
                ),

                SizedBox(height: size.height * 0.015),
                dividerWithText("   Order Summery  "),
                SizedBox(height: size.height * 0.015),
                Container(
                  width: size.width * 0.9,
                  padding: EdgeInsets.only(left: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(color: Colors.black12)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // SizedBox(height: size.height * 0.01),
                      // rowText("  Estimated Delivery time  ", "${widget.estimatedTime} min"),
                      SizedBox(height: size.height * 0.005),
                      rowText(
                        "  Sub Total   ",
                        "  QAR ${myOrderExpantionModel.subTotal}",
                      ),
                      SizedBox(height: size.height * 0.005),
                      rowText("  Service Charge  ",
                          "QAR ${myOrderExpantionModel.shippingRate}"),
                      SizedBox(height: size.height * 0.005),
                      rowText(
                        "    ",
                        "Total QAR ${myOrderExpantionModel.total}",
                      ),
                      SizedBox(height: size.height * 0.01),
                    ],
                  ),
                ),
                SizedBox(height: size.height * 0.015),
              ],
            ),
          );
        } else {
          return Center(
              child: myOrder.isLoading
                  ? WLoadingWidget()
                  : Text("something went wrong",
                      style: TextStyle(color: Colors.black, fontSize: 40)));
        }
      }),
    ));
  }

  text(String title, Color color, FontWeight fontWeight, double fontsize) {
    return Container(
      child: Text(
        title,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            color: color,
            fontSize: fontsize,
            fontWeight: fontWeight,
            fontFamily: Config.fontFamily),
      ),
    );
  }

  iconContainer(String title, IconData iconData) {
    var size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.all(12.0),
      decoration: BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Row(
        children: [
          Icon(
            iconData,
            size: size.height * 0.02,
          ),
          text(title, Colors.black, FontWeight.normal, size.height * 0.018),
        ],
      ),
    );
  }

  imageWithColumnText(String text1, text2, tex3, bool displayTextImage) {
    var size = MediaQuery.of(context).size;

    return Row(
      children: [
        // Container(
        //   margin: EdgeInsets.only(left: size.width * 0.02, top: size.height * 0.01),
        //   height: size.height * 0.1,
        //   width: size.width * 0.2,
        //   decoration: BoxDecoration(
        //       image: DecorationImage(
        //         image: NetworkImage(image),
        //         fit: BoxFit.cover,
        //       ),
        //       borderRadius: BorderRadius.circular(10.0),
        //       border: Border.all(color: Colors.white)),
        // ),
        SizedBox(width: size.width * 0.02),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: size.width * 0.45,
              child: text(
                  text1, Colors.black, FontWeight.bold, size.height * 0.025),
            ),
            SizedBox(height: size.height * 0.01),
            Row(
              children: [
                text(text2, Config.primaryColor, FontWeight.normal,
                    size.height * 0.02),
              ],
            ),
            SizedBox(height: size.height * 0.01),
            text(tex3, Colors.black54, FontWeight.normal, size.height * 0.02),
            SizedBox(height: size.height * 0.01),
          ],
        ),
      ],
    );
  }

  dividerWithText(String tittle) {
    var size = MediaQuery.of(context).size;
    return Row(children: <Widget>[
      text(tittle, Colors.black54, FontWeight.bold, 32.sp),
      Expanded(child: Divider()),
    ]);
  }

  imageWitColumnText(String image, text1, text2) {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        SizedBox(height: size.height * 0.01),
        Image.asset(image, height: size.width * 0.05),
        SizedBox(height: size.height * 0.01),
        text(text1, Colors.black, FontWeight.bold, size.height * 0.018),
        SizedBox(height: size.height * 0.01),
        text(text2, Colors.black, FontWeight.normal, size.height * 0.018),
        SizedBox(height: size.height * 0.01),
      ],
    );
  }

  itemListCard(String image, name, quantity, price, total) {
    var size = MediaQuery.of(context).size;
    return Container(
        width: size.width * 0.9,
        padding: EdgeInsets.only(left: 5, bottom: 5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(color: Colors.black12)),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(
                  left: size.width * 0.02, top: size.height * 0.01),
              height: size.height * 0.1,
              width: size.width * 0.2,
              decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(image),
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.white)),
            ),
            SizedBox(width: size.width * 0.01),
            Container(
              width: size.width * 0.37,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: size.height * 0.01),
                  text(
                      name, Colors.black, FontWeight.bold, size.height * 0.025),
                  text(" x $quantity", Colors.black, FontWeight.normal,
                      size.height * 0.02),
                  SizedBox(height: size.height * 0.035),
                ],
              ),
            ),
            SizedBox(width: size.width * 0.01),
            Container(
              width: size.width * 0.25,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(height: size.height * 0.04),
                  text(price, Colors.black, FontWeight.normal,
                      size.height * 0.02),
                  SizedBox(height: size.height * 0.01),
                  text(total, Colors.black, FontWeight.bold,
                      size.height * 0.025),
                ],
              ),
            ),
            Spacer(),
          ],
        ));
  }

  rowText(String title1, title2) {
    var size = MediaQuery.of(context).size;
    return Row(
      children: [
        SizedBox(width: size.width * 0.02),
        text(title1, Colors.black, FontWeight.normal, size.height * 0.025),
        Spacer(),
        text(
          title2,
          title2.contains('Total QAR') ? Colors.black : Colors.black,
          title2.contains('Total QAR') ? FontWeight.bold : FontWeight.normal,
          size.height * 0.025,
        ),
        SizedBox(width: size.width * 0.02),
      ],
    );
  }

  gettingUserDetails() async {
    final currentAuthUser = Hive.box('auth');
    Map user = await currentAuthUser.get('user');
    setState(() {
      userHashId = user['hashid'];
    });
  }
}
