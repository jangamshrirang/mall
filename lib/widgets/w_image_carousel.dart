import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import '../env/config.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

class WImageCarousel extends StatelessWidget {
  final List banners;
  final double height;

  WImageCarousel({
    this.banners,
    this.height: 180.0,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: Swiper(
        pagination: SwiperCustomPagination(builder: (BuildContext context, SwiperPluginConfig config) {
          return Positioned(
            bottom: 16,
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: AnimatedSmoothIndicator(
                  activeIndex: config.activeIndex,
                  count: config.itemCount,
                  axisDirection: Axis.horizontal,
                  effect: ExpandingDotsEffect(
                    dotColor: Colors.white.withOpacity(0.4),
                    activeDotColor: Colors.white,
                    dotHeight: 7,
                    dotWidth: 7,
                    expansionFactor: 5,
                  ),
                ),
              ),
            ),
          );
        }),
        itemBuilder: (BuildContext context, int index) {
          return Container(
            decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.8),
                borderRadius: BorderRadius.all(Radius.circular(10)),
                image: DecorationImage(
                  colorFilter: ColorFilter.mode(Colors.black.withOpacity(banners[index]['title'] == '' && banners[index]['description'] == '' ? 1 : 0.2), BlendMode.dstATop),
                  image: banners[index]['image'],
                  fit: BoxFit.fitWidth,
                )),
            margin: EdgeInsets.all(3.0),
            child: banners[index]['title'] != '' && banners[index]['description'] != ''
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AutoSizeText(
                        '${banners[index]['title']}',
                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 24),
                      ),
                      AutoSizeText(
                        '${banners[index]['description']}',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                      WRoundedButton(onCustomButtonPressed: () {}, btnColor: Config.primaryColor, labelColor: Colors.white, child: Text('SHOP')),
                    ],
                  )
                : SizedBox(),
          );
        },
        itemCount: banners.length,
      ),
    );
  }
}
