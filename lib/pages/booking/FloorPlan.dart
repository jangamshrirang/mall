import 'package:auto_route/auto_route.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';

class FloorPlan extends StatefulWidget {
  @override
  _FloorPlanState createState() => _FloorPlanState();
}

class _FloorPlanState extends State<FloorPlan> {
  String animation = "idle";
  double heit = 0.135;
  int value;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: InteractiveViewer(
        panEnabled: true,
        minScale: 0.5,
        maxScale: 5,
        child: Column(
          children: [
            Container(
              height: size.height * 0.5,
              width: size.width,
              child: Stack(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                        top: size.height * 0.038, left: size.height * 0.16),
                    child: Text(
                      "Table Booking",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                    ),
                  ),
                  Container(
                    constraints: BoxConstraints.expand(),
                    height: size.height * 0.8,
                    width: size.width * 0.9,
                    decoration: BoxDecoration(
                      // color: Colors.red,
                      image: DecorationImage(
                        image: AssetImage("assets/floorplan/table2.jpg"),
                        //fit: BoxFit.fitHeight,
                      ),
                    ),
                  ),
              
                  flareTableContainer(
                    size.height * heit,
                    size.width * 0.24,
                    "assets/floorplan/table2square.flr",
                    1,
                    animation,
                    0,
                    size.height * 0.06,
                  ),
                  flareTableContainer(
                    size.height * 0.195,
                    size.width * 0.24,
                    "assets/floorplan/table2square.flr",
                    2,
                    animation,
                    0,
                    size.height * 0.06,
                  ),
                  flareTableContainer(
                    size.height * 0.11,
                    size.width * 0.35,
                    "assets/floorplan/table2square.flr",
                    3,
                    animation,
                    90,
                    size.height * 0.06,
                  ),
                  flareTableContainer(
                    size.height * 0.26,
                    size.width * 0.67,
                    "assets/floorplan/table2_3chair.flr",
                    4,
                    animation,
                    0,
                    size.height * 0.06,
                  ),
                ],
              ),
            ),
            Container(
              height: size.height * 0.5,
              width: size.width,
              child: Stack(
                children: [
                  Container(
                    constraints: BoxConstraints.expand(),
                    height: size.height * 1,
                    width: size.width * 0.9,
                    decoration: BoxDecoration(
                      // color: Colors.red,
                      image: DecorationImage(
                        image:
                            AssetImage("assets/floorplan/table2_2ndfloor.jpg"),
                        //fit: BoxFit.fitHeight,
                      ),
                    ),
                  ),
                  flareTableContainer(
                    size.height * 0.18,
                    size.width * 0.22,
                    "assets/floorplan/table2square.flr",
                    6,
                    animation,
                    90,
                    size.height * 0.06,
                  ),
                  flareTableContainer(
                    size.height * 0.20,
                    size.width * 0.35,
                    "assets/floorplan/table2-rectangle.flr",
                    7,
                    animation,
                    0,
                    size.height * 0.04,
                  ),
                  flareTableContainer(
                    size.height * 0.20,
                    size.width * 0.50,
                    "assets/floorplan/table2-rectangle.flr",
                    8,
                    animation,
                    0,
                    size.height * 0.04,
                  ),
                  flareTableContainer(
                    size.height * 0.18,
                    size.width * 0.65,
                    "assets/floorplan/table2square.flr",
                    9,
                    animation,
                    90,
                    size.height * 0.06,
                  ),
                  flareTableContainer(
                    size.height * 0.26,
                    size.width * 0.20,
                    "assets/floorplan/table2-rectangle.flr",
                    10,
                    animation,
                    0,
                    size.height * 0.04,
                  ),
                  flareTableContainer(
                    size.height * 0.26,
                    size.width * 0.35,
                    "assets/floorplan/table2-rectangle.flr",
                    11,
                    animation,
                    0,
                    size.height * 0.04,
                  ),
                  flareTableContainer(
                    size.height * 0.26,
                    size.width * 0.50,
                    "assets/floorplan/table2-rectangle.flr",
                    12,
                    animation,
                    0,
                    size.height * 0.04,
                  ),
                  flareTableContainer(
                    size.height * 0.26,
                    size.width * 0.63,
                    "assets/floorplan/table2-rectangle.flr",
                    13,
                    animation,
                    0,
                    size.height * 0.04,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  flareTableContainer(double marginTop, marginLeft, String flareImage,
      int value, String animationValue, int rotation, double tableHeight) {
    var size = MediaQuery.of(context).size;
    return StatefulBuilder(builder: (context, StateSetter setState2) {
      return Container(
        margin: EdgeInsets.only(top: marginTop, left: marginLeft),
        height: tableHeight,
        width: size.width * 0.15,
        //  color: Colors.blue,
        child: GestureDetector(
          onTap: () {
            setState2(() {
              value = value;

              if (value == value) {
                animationValue == "idle" || animationValue == "deselected"
                    ? animationValue = "selected"
                    : animationValue = "deselected";
                print(value);
              }
              print(animationValue);
              animationValue == "idle" || animationValue == "selected"
                  ? _modalBottomSheetMenu(value)
                  : null;
            });
          },
          child: RotationTransition(
            turns: new AlwaysStoppedAnimation(rotation / 360),
            child: FlareActor(flareImage,
                alignment: Alignment.centerRight,
                animation: value == value ? animationValue : "idle"),
          ),
        ),
      );
    });
  } //assets/floorplan/table2square.flr

  void _modalBottomSheetMenu(int tableNumber) {
    var size = MediaQuery.of(context).size;
    Future<void> future = showModalBottomSheet<void>(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(40.0),
      ),
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: size.height * 0.25,
          color: Colors.transparent, //could change this to Color(0xFF737373),
          //so you don't have to change MaterialApp canvasColor
          child: new Container(
              decoration: new BoxDecoration(
                  color: Colors.white,
                  borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(50.0),
                      topRight: const Radius.circular(50.0))),
              child: Column(
                children: [
                  SizedBox(
                    height: size.height * 0.01,
                  ),
                  new Text(
                    "Booking",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                  ),
                  SizedBox(
                    height: size.height * 0.01,
                  ),
                  Container(
                      child: RichText(
                    text: TextSpan(
                      style: DefaultTextStyle.of(context).style,
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Selected Table Number :',
                            style: TextStyle(
                                decoration: TextDecoration.none,
                                fontWeight: FontWeight.normal,
                                color: Colors.black,
                                fontSize: 14)),
                        TextSpan(
                            text: "$tableNumber",
                            style: TextStyle(
                                decoration: TextDecoration.none,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 17)),
                      ],
                    ),
                  )),
                  Spacer(),
                  RaisedButton(
                    onPressed: () {
                      setState(() {
                        stateManagment.setTablenumber(tableNumber);
                        Navigator.of(context).pop();
                      });
                    },
                    color: Config.primaryColor,
                    textColor: Colors.white,
                    padding: const EdgeInsets.all(0.0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                    child: Container(
                      decoration: const BoxDecoration(
                          borderRadius:
                              BorderRadius.all(Radius.circular(80.0))),
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: const Text('        Conform          ',
                          style: TextStyle(fontSize: 20)),
                    ),
                  ),
                  Spacer(),
                ],
              )),
        );
      },
    );
    future.then((void value) => _closeModal(value));
  }

  void _closeModal(void value) {
    stateManagment.tableNumber == null
        ? Navigator.of(context)
        : ExtendedNavigator.of(context).root.push('/book-table');

    print(animation);
  }
}
