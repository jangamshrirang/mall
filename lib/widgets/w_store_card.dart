import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../env/config.dart';
import 'package:wblue_customer/widgets/w_image.dart';

class WStoreCard extends StatelessWidget {
  final ImageProvider storeImg;
  final String storeName;
  final String storeAddress;
  final int storeDistance;

  WStoreCard(
      {this.storeAddress: 'WBLUE Inc.',
      this.storeDistance: 0,
      this.storeImg: const AssetImage('assets/images/logos/wmall.png'),
      this.storeName: 'WBlue'});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      color: Config.dullColor,
      child: Row(children: <Widget>[
        WImageWidget(
          placeholder: storeImg,
        ),
        SizedBox(width: 30),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('$storeName',
                style: TextStyle(fontSize: 16, color: Colors.black87)),
            Text('$storeAddress',
                style: TextStyle(fontSize: 14, color: Colors.grey[800])),
            SizedBox(height: 15.0),
            Text('${storeDistance}mins Away',
                style: TextStyle(fontSize: 14, color: Colors.grey[700])),
          ],
        )
      ]),
    );
  }
}
