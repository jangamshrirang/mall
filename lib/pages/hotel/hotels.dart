import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import '../../env/config.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_drawer.dart';
import 'package:wblue_customer/widgets/w_hotel_item.dart';
import 'package:wblue_customer/widgets/w_modal_container.dart';

class HotelPage extends StatefulWidget {
  @override
  _HotelPageState createState() => _HotelPageState();
}

class _HotelPageState extends State<HotelPage> {
  String address = Global.myAddress;

  Map<String, dynamic> body = {
    'query': '',
    'duration': 1,
    'start-date': '',
    'end-date': '',
    'adults': 2,
    'children': 0,
    'room': 1,
    'travelling-for': 'business',
  };

  @override
  Widget build(BuildContext context) {
    DateTime now = new DateTime.now().toLocal();
    DateFormat formatter = new DateFormat('yyyy-MM-dd');
    DateFormat displayFormatter = new DateFormat('EEE, MMM dd');
    now = DateTime.parse(formatter.format(now));

    body['start-date'] =
        body['start-date'] == '' ? formatter.format(now) : body['start-date'];
    body['end-date'] = body['end-date'] == ''
        ? formatter.format(now.add(Duration(days: 1)))
        : body['end-date'];

    void _setPax() {
      int adults = body['adults'];
      int children = body['children'];
      int room = body['room'];

      showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState2) {
              return WModalContainerWidget(
                title: 'Guests',
                children: <Widget>[
                  ListTile(
                    title: Text('Adults'),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            if (adults > 1) {
                              setState2(() {
                                setState(() {
                                  --adults;
                                  body['adults'] = adults;
                                });
                              });
                            }
                          },
                          child: Icon(
                            Icons.remove_circle,
                            color: adults > 1 ? Colors.amber : Colors.grey,
                          ),
                        ),
                        SizedBox(width: 8.0),
                        Text(adults.toString()),
                        SizedBox(width: 8.0),
                        InkWell(
                          onTap: () {
                            setState2(() {
                              setState(() {
                                ++adults;
                                body['adults'] = adults;
                              });
                            });
                          },
                          child: Icon(
                            Icons.add_circle,
                            color: Colors.amber,
                          ),
                        ),
                      ],
                    ),
                  ),
                  ListTile(
                    title: Text('Children'),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            if (children > 0) {
                              setState2(() {
                                setState(() {
                                  --children;
                                  body['children'] = children;
                                });
                              });
                            }
                          },
                          child: Icon(
                            Icons.remove_circle,
                            color: children > 0 ? Colors.amber : Colors.grey,
                          ),
                        ),
                        SizedBox(width: 8.0),
                        Text(children.toString()),
                        SizedBox(width: 8.0),
                        InkWell(
                          onTap: () {
                            setState2(() {
                              setState(() {
                                ++children;
                                body['children'] = children;
                              });
                            });
                          },
                          child: Icon(
                            Icons.add_circle,
                            color: Colors.amber,
                          ),
                        ),
                      ],
                    ),
                  ),
                  ListTile(
                    title: Text('Room'),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            if (room > 1) {
                              setState2(() {
                                setState(() {
                                  --room;
                                  body['room'] = room;
                                });
                              });
                            }
                          },
                          child: Icon(
                            Icons.remove_circle,
                            color: room > 1 ? Colors.amber : Colors.grey,
                          ),
                        ),
                        SizedBox(width: 8.0),
                        Text(room.toString()),
                        SizedBox(width: 8.0),
                        InkWell(
                          onTap: () {
                            setState2(() {
                              setState(() {
                                ++room;
                                body['room'] = room;
                              });
                            });
                          },
                          child: Icon(
                            Icons.add_circle,
                            color: Colors.amber,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            },
          );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Config.primaryColor,
        title: Text('Browse Hotel'),
        centerTitle: true,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(30.0),
          child: Column(
            children: <Widget>[
              Text(
                '0 HOTELS FOUND',
                style: TextStyle(fontSize: 12.0, color: Config.dullColor),
              ),
              SizedBox(height: 14.0),
            ],
          ),
        ),
      ),
      drawer: WDrawerWidget('browse-hotel'),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage('assets/images/search-hotel-bg.png'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    margin: EdgeInsets.only(bottom: 12.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50.0),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.amber,
                          offset: Offset(0.0, 4.0),
                        ),
                      ],
                    ),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Search Name',
                        border: InputBorder.none,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  InkWell(
                    onTap: () async {
                      DateTime initialFirstDate = body['start-date'] == ''
                          ? now.add(Duration(seconds: 5))
                          : DateTime.parse(body['start-date'])
                              .add(Duration(seconds: 5));
                      DateTime initialLastDate = body['end-date'] == ''
                          ? now.add(Duration(seconds: 5))
                          : DateTime.parse(body['end-date'])
                              .add(Duration(seconds: 5));
                      List<DateTime> picked =
                          await DateRagePicker.showDatePicker(
                        context: context,
                        initialFirstDate: initialFirstDate,
                        initialLastDate: initialLastDate,
                        firstDate: now,
                        lastDate: now.add(Duration(days: 30)),
                      );
                      int duration = 1;
                      String dateStart = formatter.format(initialFirstDate);
                      String dateEnd = formatter.format(initialLastDate);
                      if (picked != null && picked.length == 2) {
                        duration = picked[1].difference(picked[0]).inDays;
//                        ++duration;
                        dateStart = formatter.format(picked[0]);
                        dateEnd = formatter.format(picked[1]);
                      }
                      setState(() {
                        body['duration'] = duration;
                        body['start-date'] = dateStart;
                        body['end-date'] = dateEnd;
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      margin: EdgeInsets.only(bottom: 12.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.amber,
                            offset: Offset(0.0, 4.0),
                          ),
                        ],
                      ),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.calendar_today,
                            color: Colors.red,
                          ),
                          Expanded(
                            child: Text(
                              '${displayFormatter.format(DateTime.parse(body['start-date']))} - ${displayFormatter.format(DateTime.parse(body['end-date']))}',
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        _setPax();
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      margin: EdgeInsets.only(bottom: 12.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.amber,
                            offset: Offset(0.0, 4.0),
                          ),
                        ],
                      ),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.supervisor_account,
                            color: Colors.red,
                          ),
                          Expanded(
                            child: Text(
                              '${body['adults'].toString()} adults • ${body['children'].toString()} children • ${body['room'].toString()} room',
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(12.0),
                    margin: EdgeInsets.only(bottom: 12.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.amber,
                          offset: Offset(0.0, 4.0),
                        ),
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Traveling for? ',
                          textAlign: TextAlign.right,
                        ),
                        SizedBox(width: 8.0),
                        Row(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                setState(() {
                                  body['travelling-for'] = 'business';
                                });
                              },
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    body['travelling-for'] == 'business'
                                        ? Icons.radio_button_checked
                                        : Icons.radio_button_unchecked,
                                    color: Colors.red,
                                  ),
                                  Text('Business'),
                                ],
                              ),
                            ),
                            SizedBox(width: 4.0),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  body['travelling-for'] = 'leisure';
                                });
                              },
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    body['travelling-for'] == 'leisure'
                                        ? Icons.radio_button_checked
                                        : Icons.radio_button_unchecked,
                                    color: Colors.red,
                                  ),
                                  Text('Leisure'),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  WButtonWidget(
                    title: 'search',
                    onPressed: () {},
                  ),
                ],
              ),
            ),
            Expanded(
              child: RefreshIndicator(
                onRefresh: () async {
                  setState(() {});
                },
                child: ListView(
                  shrinkWrap: true,
                  physics: const AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.all(16.0),
                  children: List.generate(10, (index) {
                    return WHotelItemWidget();
                  }),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
