import 'package:flutter/material.dart';

class WContainerBlue extends StatelessWidget {
  final Widget child;
  WContainerBlue({this.child});

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.transparent,
      elevation: 0,
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              image: DecorationImage(
                image: ExactAssetImage('assets/images/market/balance-bg.png'),
                fit: BoxFit.cover,
              )),
          child: child),
    );
  }
}
