import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

class TransactionExpantion extends StatefulWidget {
  @override
  _TransactionExpantionState createState() => _TransactionExpantionState();
}

class _TransactionExpantionState extends State<TransactionExpantion> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, " Transaction Details "),
      body: Container(
        height: size.height*1,
        child: Padding(
          padding: const EdgeInsets.all(0.0),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.check_circle,
                color: Colors.green,
                size: 64.0,
              ),
              text(
                  "     Purchase Successful ",
                  size.height * 0.023,
                  Colors.green,
                  1,
                  FontWeight.bold,
                  size.width * 0.5,
                  TextDecoration.none),
              Divider(),
              rowText("Amount", Colors.black45, "QAR 200", Colors.black,
                  size.height * 0.04),
                  rowText("Payment method", Colors.black45, "Credit/Debit card", Colors.black,
                  size.height * 0.025),
                  Divider(),
                  rowText("Order Id", Colors.black45, "33333222221111000", Colors.blue,
                  size.height * 0.02),
                  rowText("Product", Colors.black45, "GOSURF2965", Colors.black45,
                  size.height * 0.02),
                  rowText("Category", Colors.black45, "Digitila Utilities", Colors.black45,
                  size.height * 0.02),
                  rowText("Merchant", Colors.black45, "Mobile Toup PH", Colors.black45,
                  size.height * 0.02),
                  rowText("Order on", Colors.black45, "20-2-2020 22:37", Colors.black45,
                  size.height * 0.02),
            ],
          ),
        ),
      ),
    );
  }

  text(
    String text,
    double height,
    Color color,
    int lines,
    FontWeight fontWeight,
    double width,
    TextDecoration underline,
  ) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: width,
      margin: EdgeInsets.only(left: 10, top: 0),
      child: Text(
        text,
        maxLines: lines,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: fontWeight,
            color: color,
            decoration: underline,
            fontSize: height),
      ),
    );
  }

  rowText(
      String text1, Color color1, String text2, Color color, double text2size) {
    var size = MediaQuery.of(context).size;
    return Row(
      children: [
        Spacer(),
        Container(
          margin: EdgeInsets.only(
            left: size.width * 0.05,
            bottom: 10,
            top: 10,
          ),
          //color: Colors.blue,
          width: size.width * 0.4,
          child: Text(
            text1,
            style: TextStyle(
                color: color1,
                fontSize: size.height * 0.019,
                fontWeight: FontWeight.bold),
          ),
        ),
        Spacer(),
        Container(
          margin: EdgeInsets.only(right: 10),
          //color: Colors.yellow,
          width: size.width * 0.45,
          child: Text(
            text2,
            textAlign: TextAlign.end,
            style: TextStyle(color: color, fontSize: text2size),
          ),
        ),
      ],
    );
  }
}
