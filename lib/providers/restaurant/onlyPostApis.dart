import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

///restaurant/cart/total
DioClient _dioClient = DioClient();
Future fetchTotlaPrice(var cartID) async {
  Map<String, dynamic> _body = {
    'cart_id': cartID,
  };
  APIResponse res = await _dioClient.privatePost('/restaurant/cart/total', data: _body);

  ///restaurant/by-rate
  if (res.code >= 400) {
    return [];
  }
  print("total price :${res.data}");
  stateManagment.setTotalPrict(res.data);
  return res.data;
}

Future onlyTableReservatoion(var restHasId) async {
  Map<String, dynamic> _body = {
    'restaurant_id': restHasId,
    'arrive_time': "${stateManagment.time}",
    'arrive_date': "${stateManagment.date}",
    'guest': stateManagment.guestNumber == null ? "1" : "${stateManagment.guestNumber}",
  };

  APIResponse res = await _dioClient.privatePost('/restaurant/table/reservation/add', data: _body);

  print("data ${res.message}");
  print("data for only table boking api${res.data}");

  if (res.code >= 400) {
    return [];
  }
  return res.data;
}
