import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../env/config.dart';


class WLiveBottomWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: true,
      child: Container(
        child: Row(
          children: [
            _icon(icon: MdiIcons.shopping, onPressed: () {}),
            _type(),
            _icon(icon: MdiIcons.share, onPressed: () {}),
            _icon(icon: MdiIcons.heart, onPressed: () {}),
          ],
        ),
      ),
    );
  }

  Widget _icon({IconData icon, Function onPressed}) {
    return Container(
      decoration: BoxDecoration(color: Config.primaryColor, shape: BoxShape.circle),
      margin: EdgeInsets.symmetric(horizontal: 5),
      child: IconButton(
        icon: Icon(icon),
        color: Colors.white,
        iconSize: 30,
        onPressed: () => onPressed(),
      ),
    );
  }

  Widget _type() {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(color: Colors.black54, borderRadius: BorderRadius.circular(50)),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
        child: Text(
          'Share your ideas',
          style: TextStyle(color: Colors.white70, fontSize: 18),
        ),
      ),
    );
  }
}
