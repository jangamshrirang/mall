import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class WProductImageCard extends StatelessWidget {
  final Widget widget;
  final String imageUrl;
  final String imageThumbnail;
  final bool isVertical;
  final Function onTap;

  WProductImageCard(
      {this.widget,
      this.imageUrl,
      this.imageThumbnail,
      this.isVertical: false,
      @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Card(
//      margin: EdgeInsets.all(10.0),
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: GestureDetector(
        onTap: () => onTap(),
        child: isVertical
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Center(
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
                      child: CachedNetworkImage(
                        imageUrl: imageUrl ?? '',
                        imageBuilder: (context, imageProvider) => ClipRRect(
                          borderRadius: BorderRadius.circular(0.008.sh),
                          child: Container(
                            height: 0.40.sw,
                            child: Image.network(
                              imageUrl,
                              fit: BoxFit.cover,
                              height: double.infinity,
                              width: double.infinity,
                              alignment: Alignment.center,
                            ),
                          ),
                        ),
                        placeholder: (context, url) => ClipRRect(
                          borderRadius: BorderRadius.circular(0.008.sh),
                          child: Container(
                            height: 0.40.sw,
                            child: Image.network(
                              imageThumbnail,
                              fit: BoxFit.cover,
                              height: double.infinity,
                              width: double.infinity,
                              alignment: Alignment.center,
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) => Container(
                          height: 0.26.sh,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              AutoSizeText(
                                'Image Not Found',
                                style: TextStyle(
                                    fontSize: 28.sp,
                                    fontWeight: FontWeight.w300),
                              ),
                              SizedBox(
                                height: 0.02.sh,
                              ),
                              Icon(
                                MdiIcons.alertCircle,
                                color: Colors.red,
                                size: 0.08.sh,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
//                Expanded(
//                    child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: widget,
//                )),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: widget,
                  ),
                ],
              )
            : Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: Image.asset(
                          imageUrl,
                          height: 100.0,
                          width: 100.0,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: widget,
                  )),
                ],
              ),
      ),
    );
  }
}
