import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wblue_customer/env/config.dart';

class ServiceCarocel extends StatefulWidget {
  @override
  _ServiceCarocelState createState() => _ServiceCarocelState();
}

class _ServiceCarocelState extends State<ServiceCarocel> {
  //for date picker
  DateTime selectedDate = DateTime.now();
  //for time picker
  static DateTime _eventdDate = DateTime.now();
  static var now =
      TimeOfDay.fromDateTime(DateTime.parse(_eventdDate.toString()));
  String _eventTime = now.toString().substring(10, 15);
  //text editor
  TextEditingController _textFieldController = TextEditingController();

  bool selected = false;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      //  height: size.height * 0.45,
      width: size.width * 0.45,
      // margin: EdgeInsets.only(bottom: 5, top: 5),
      child: ListView(
          scrollDirection: Axis.horizontal,
          children: [servicesCard(), servicesCard(), servicesCard()]),
    );
  }

  servicesCard() {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(
        left: 10,
      ),
      //height: size.height * 0.4,
      width: size.width * 0.6,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset("assets/services/Image 5.png", height: size.height * 0.1),
          text(" Harmonise", size.height * 0.023, Colors.black, 1,
              FontWeight.bold, size.width * 0.3),
          Divider(),
          text("Harmonise", size.height * 0.023, Colors.black, 1,
              FontWeight.bold, size.width),
          text(
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisi lacus, aliquet eget tempus ut, pellentesque quis tellus. Sed posuere tincidunt urna in dignissim. Phasellus fringilla tincidunt velit, eget cursus nisl rutrum et. Ut condimentum ex et viverra dignissim. Pellentesque sem tellus, condimentum nec purus ut, auctor congue libero.",
              size.height * 0.020,
              Colors.black38,
              10,
              FontWeight.normal,
              size.width),
          button(" Book Schedule ", () {
            _selectDate(context, "Book Schedule");
          }, Color(0xff101f40)),
          button("Request a Quote", () {
            _selectDate(context, "Request a Quote");
          }, Color(0xff284E99)),
        ],
      ),
    );
  }

  text(String text, double height, Color color, int lines,
      FontWeight fontWeight, double width) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: width,
      margin: EdgeInsets.only(left: 10, top: 5),
      child: Text(
        text,
        maxLines: lines,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: fontWeight,
            color: color,
            fontSize: height),
      ),
    );
  }

//buttons
  button(String title, Function onTap, Color color) {
    return RaisedButton(
        child: Text(
          title,
          style: TextStyle(color: Colors.white),
        ),
        color: color,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        onPressed: onTap);
  }

//for date
  _selectDate(BuildContext context, String title) async {
    final DateTime picked = await showDatePicker(
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.dark().copyWith(
            colorScheme: ColorScheme.dark(
              primary: Colors.deepPurple,
              onPrimary: Colors.white,
              surface: Config.primaryColor,
              onSurface: Colors.white,
            ),
            dialogBackgroundColor: Config.primaryColor,
          ),
          child: child,
        );
      },
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        _pickTime(context, title);
        print(selectedDate);
      });
  }

//for time
  Future _pickTime(BuildContext context, String title) async {
    TimeOfDay timepick = await showTimePicker(
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: ColorScheme.dark(
                primary: Colors.white,
                onPrimary: Config.primaryColor,
                surface: Config.primaryColor,
                onSurface: Colors.white,
                onSecondary: Colors.white,
                onBackground: Colors.white,
              ),
              dialogBackgroundColor: Config.primaryColor,
            ),
            child: child,
          );
        },
        context: context,
        initialTime: new TimeOfDay.now());

    if (timepick != null) {
      setState(() {
        _eventTime = timepick.toString().substring(10, 15);
        print(_eventTime);
        title == "Book Schedule"
            ? _displayDialog(context)
            : _checkListDialog(context);
      });
    }
  }

  _displayDialog(BuildContext context) async {
    var size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    text("Do you want to add some notes", size.height * 0.023,
                        Colors.black, 1, FontWeight.bold, size.width * 0.5),
                    Divider(),
                    TextFormField(
                      maxLines: 1,
                      cursorColor: Colors.black,
                      controller: _textFieldController,
                      decoration: InputDecoration(
                          filled: true,
                          hintStyle: TextStyle(
                              color: Colors.black12,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.none),
                          fillColor: Colors.grey[200],
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: Colors.black, width: 1),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              borderSide:
                                  BorderSide(color: Colors.black38, width: 1)),
                          contentPadding: EdgeInsets.only(
                              left: 15, bottom: 10, top: 0, right: 15),
                          hintText: "Type here..... "),
                    ),
                    button(
                        _textFieldController.text != " " ||
                                _textFieldController.text != null
                            ? " Skip"
                            : " Done", () {
                      Navigator.pop(context);
                      _successDialog(context);
                    }, Color(0xff101f40)),
                  ],
                ),
              ),
            ),
          );
        });
  }

  _successDialog(BuildContext context) async {
    var size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.check_circle,
                      color: Colors.green,
                      size: 64.0,
                    ),
                    text("Shedule successefully boocked ", size.height * 0.023,
                        Colors.black, 1, FontWeight.bold, size.width * 0.5),
                    Divider(),
                    button(" Close", () {
                      Navigator.pop(context);
                    }, Color(0xff101f40)),
                  ],
                ),
              ),
            ),
          );
        });
  }

  _checkListDialog(BuildContext context) async {
    var size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 400,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: ListView(
                  children: <Widget>[
                    text("Select services you want ", size.height * 0.023,
                        Colors.black, 1, FontWeight.bold, size.width * 0.5),
                    Exercise(
                      title: "Harmonoise",
                    ),
                    Exercise(
                      title: "Wateroise",
                    ),
                    Exercise(
                      title: "Fareoise",
                    ),
                    Exercise(
                      title: "Harmonoise1",
                    ),
                    Exercise(
                      title: "Harmonoise1",
                    ),
                    button(" Done", () {
                      Navigator.pop(context);
                      _successDialog(context);
                    }, Color(0xff101f40)),
                  ],
                ),
              ),
            ),
          );
        });
  }

  checkBoxs(String title) {
    return ListTile(
      title: Text(title),
      trailing: Checkbox(
          value: selected,
          onChanged: (bool val) {
            setState(() {
              selected = val;
              print(selected);
            });
          }),
    );
  }
}

class Exercise extends StatefulWidget {
  final String title;

  Exercise({this.title});

  @override
  _ExerciseState createState() => _ExerciseState();
}

class _ExerciseState extends State<Exercise> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(widget.title),
      trailing: Checkbox(
       // checkColor: Config.primaryColor,
       activeColor: Config.primaryColor,
          value: selected,
          onChanged: (bool val) {
            setState(() {
              selected = val;
              print(selected == true ? widget.title : "false");
            });
          }),
    );
  }
}
