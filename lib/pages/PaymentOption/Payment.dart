import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/CurrencyModel.dart';
import 'package:wblue_customer/pages/PayPal/PayPalPayment.dart';
import 'package:wblue_customer/pages/PaymentOption/AllPaymentOptions.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_loading.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

class Payment extends StatefulWidget {
  String preOrder, from, fromScan, restaurentHashId, reservationHashId, rImage, rName, totalPrice;
  var orderType, lat, lang, cartId;
  Payment(
      {this.preOrder,
      this.from,
      this.lat,
      this.orderType,
      this.lang,
      this.cartId,
      this.fromScan,
      this.restaurentHashId,
      this.reservationHashId,
      this.totalPrice,
      this.rImage,
      this.rName});
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  Future<File> imageFile;
  int value;
  Future<CurrencyModel> currency;
  Future<CurrencyModel> fetchCurency() async {
    final response = await Dio().get('http://rate-exchange-1.appspot.com/currency?from=QAR&to=USD');
    print(response);

    if (response.statusCode == 200) {
      return CurrencyModel.fromJson(response.data);
    } else {
      throw Exception('Failed to load album');
    }
  }

  int pPrice = 1000;
  @override
  void initState() {
    // TODO: implement initState
    currency = fetchCurency();
    super.initState();
  }

  DioClient _dioClient = DioClient();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBarWithOutIcon(context, "Payment"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Wrap(
              runSpacing: 10,
              children: [
                paymentOption('assets/images/market/cart/visa.png', 'Credit/Debit Card', 1),
                widget.preOrder != "yes" || widget.preOrder == null
                    ? paymentOption(
                        'assets/images/market/cart/cod.png',
                        widget.from == "rest" || widget.from == "activities" ? 'Cash on Arrival' : "Cash on delivery",
                        2)
                    : Container(width: 0.01),
                paymentOption('assets/images/market/voucher-2.png', 'Wallet', 3),
                paymentOption('assets/images/market/cart/paypal.png', 'PayPal', 4),
                paymentOption('assets/images/market/cart/visa.png', 'Bank Transfer', 5),
              ],
            ),
            Divider(
              thickness: 5,
              color: Colors.grey[200],
            ),
            value == 1 || value == null
                ? creditCard(context, widget.preOrder, 1, widget.lat, widget.lang, widget.orderType, widget.cartId,
                    widget.fromScan, widget.reservationHashId, widget.restaurentHashId)
                : value == 2
                    ? cod(context, 1, widget.lat, widget.lang, widget.orderType, widget.cartId, widget.from,
                        widget.fromScan, widget.reservationHashId, widget.restaurentHashId)
                    : value == 3
                        ? wallet(context)
                        : value == 5
                            ? bankTransfer(context)
                            : value == 4
                                ? currecyConvetionData()
                                : Container() //
          ],
        ),
      ),
    );
  }

  paymentOption(String image, title, int index) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        setState(() {
          value = index;
        });
      },
      child: Container(
        width: size.width * 0.3,
        margin: EdgeInsets.only(left: size.width * 0.02),
        decoration: BoxDecoration(
          border: Border.all(color: value == index ? Config.primaryColor : Colors.black12),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          children: [
            Container(
              height: 50,
              width: 40,
              child: WImageWidget(
                placeholder: AssetImage(image),
              ),
            ),
            AutoSizeText(title),
          ],
        ),
      ),
    );
  }

  bankTransfer(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('assets/images/logos/wmall-512.png', height: size.height * 0.2),
          text(context, "This is Wmall Account Details,After transcation please upload RECEPT or SCREEN SHOT.",
              FontWeight.normal),
          SizedBox(height: 5),
          text(context, "AC.No:12345679123123 ", FontWeight.bold),
          SizedBox(height: 5),
          text(context, "Bank type : QNB", FontWeight.bold),
          SizedBox(height: 5),
          showImage(),
          RaisedButton(
            child: Text("Select Image "),
            onPressed: () {
              pickImageFromGallery(ImageSource.gallery);
            },
          ),
          // Container(color: Colors.black12, height: size.height * 0.3),
          Container(
            color: Colors.white,
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: WRoundedButton(
                onCustomButtonPressed: () {
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (context) => ActivityBookingSuccess()));
                },
                child: AutoSizeText('Upload'),
                borderColor: Config.primaryColor,
                btnColor: Config.primaryColor,
                labelColor: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  pickImageFromGallery(ImageSource source) {
    setState(() {
      imageFile = ImagePicker.pickImage(source: source);
    });
  }

  Widget showImage() {
    return FutureBuilder<File>(
      future: imageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done && snapshot.data != null) {
          return Image.file(
            snapshot.data,
            width: 300,
            height: 300,
          );
        } else if (snapshot.error != null) {
          return const Text(
            'Error Picking Image',
            textAlign: TextAlign.center,
          );
        } else {
          return const Text(
            'No Image Selected',
            textAlign: TextAlign.center,
          );
        }
      },
    );
  }

  currecyConvetionData() {
    return FutureBuilder<CurrencyModel>(
      future: fetchCurency(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          String currencry = "${snapshot.data.rate * pPrice}";
          String decimalremovedPrice = pPrice.toString().length == 5
              ? currencry.substring(0, 7)
              : pPrice.toString().length == 4
                  ? currencry.substring(0, 6)
                  : pPrice.toString().length == 3
                      ? currencry.substring(0, 5)
                      : currencry.substring(0, 4);
          print(pPrice.toString().length);
          print(currencry);
          return Column(
            children: [
              SizedBox(height: 50),
              RichText(
                text: new TextSpan(
                  // Note: Styles for TextSpans must be explicitly defined.
                  // Child text spans will inherit styles from parent
                  style: new TextStyle(
                    fontSize: 14.0,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    new TextSpan(
                      text: 'Your product Price in ',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    new TextSpan(
                        text: 'USD :',
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Config.primaryColor,
                          fontSize: 18,
                        )),
                    TextSpan(
                        text: " $decimalremovedPrice",
                        style: new TextStyle(
                          fontSize: 18,
                        )),
                  ],
                ),
              ),
              RichText(
                text: new TextSpan(
                  // Note: Styles for TextSpans must be explicitly defined.
                  // Child text spans will inherit styles from parent
                  style: new TextStyle(
                    fontSize: 14.0,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    new TextSpan(
                      text: 'Your product Price in ',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    new TextSpan(
                        text: 'QAR :',
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Config.primaryColor,
                          fontSize: 18,
                        )),
                    TextSpan(
                        text: " $pPrice",
                        style: new TextStyle(
                          fontSize: 18,
                        )),
                  ],
                ),
              ),
              Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: WRoundedButton(
                    onCustomButtonPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => PaypalPayment(
                                onFinish: (number) async {
                                  // payment done
                                  print('order id: ' + number);
                                },
                                price: "$decimalremovedPrice",
                              )));
                    },
                    child: AutoSizeText('Pay'),
                    borderColor: Config.primaryColor,
                    btnColor: Config.primaryColor,
                    labelColor: Colors.white,
                  ),
                ),
              ),
            ],
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner.
        return WLoadingWidget();
      },
    );
  }

  Future placeORder(var shippingRate, paymentMethod, lat, lang, orderType, note) async {
    Map<String, dynamic> _body = {
      'shipping_rate': 0,
      'payment_method_id': paymentMethod,
      'shipping_address': 'qatar',
      'shipping_address_coordinates': '{lat: $lat, lng: $lang}',
      'order_type': orderType,
      'note': 'any'
    };
    APIResponse res = await _dioClient.privatePost('/restaurant/order/place', data: _body);
    if (res.code >= 400) {
      return [];
    }

    return res.data;
  }
}
