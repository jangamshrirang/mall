import 'package:auto_route/auto_route.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_cache_builder.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/routes/router.gr.dart';

class BookTable extends StatefulWidget {
  String coverImage, restimage, restName, onlybooking, restSlogan, restId;
  int restind;
  BookTable(
      {this.coverImage, this.restimage, this.restName, this.onlybooking, this.restSlogan, this.restId, this.restind});
  @override
  _BookTableState createState() => _BookTableState();
}

class _BookTableState extends State<BookTable> {
  TextEditingController reqController = new TextEditingController();
  int ani = 2;
  int dynamicQty = 1;
  int max = 99;
  int min = 1;
  int stepValue = 1;

  @override
  void initState() {
    super.initState();
    Future.microtask(() => context.read<PRestaurantFoodCart>().resetSched());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(0.07.sh),
          child: AppBar(
            elevation: 0.0,
            title: Column(
              children: [
                Text("Book Your Table"),
                Text(
                  '${widget.restName}',
                  style: TextStyle(fontSize: 32.sp),
                ),
              ],
            ),
            backgroundColor: Config.primaryColor,
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          child: SizedBox(
            height: 60,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: RaisedButton(
                color: Config.primaryColor,
                elevation: 0.0,
                textColor: Colors.white,
                onPressed: () => _confirmationDialog(context),
                child: Text(
                  'Confirm',
                  style: TextStyle(fontSize: 36.sp),
                ),
              ),
            ),
          ),
        ),
        body: FlareCacheBuilder(
          [
            AssetFlare(name: 'assets/flares/restaurant/table/2.flr', bundle: rootBundle),
            AssetFlare(name: 'assets/flares/restaurant/table/3.flr', bundle: rootBundle),
            AssetFlare(name: 'assets/flares/restaurant/table/4.flr', bundle: rootBundle),
            AssetFlare(name: 'assets/flares/restaurant/table/5.flr', bundle: rootBundle),
            AssetFlare(name: 'assets/flares/restaurant/table/6.flr', bundle: rootBundle),
            AssetFlare(name: 'assets/flares/restaurant/table/7.flr', bundle: rootBundle),
            AssetFlare(name: 'assets/flares/restaurant/table/8.flr', bundle: rootBundle),
          ],
          builder: (BuildContext context, bool isWarm) {
            return !isWarm
                ? Center(
                    child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(width: 0.05.sw),
                      Text(
                        'Preparing your table...',
                        style: TextStyle(fontSize: 32.sp),
                      ),
                    ],
                  ))
                : Container(
                    color: Config.primaryColor.withAlpha(16),
                    child: Consumer<PRestaurantFoodCart>(
                      builder: (context, tableProvider, child) => ListView(
                        children: [
                          Container(
                            height: 0.5.sh,
                            child: Card(
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Card(
                                      color: Colors.grey.shade400,
                                      child: FlareActor(
                                        "assets/flares/restaurant/table/$ani.flr",
                                        alignment: Alignment.center,
                                        fit: BoxFit.contain,
                                        animation: ani.toString(),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text('Guest', style: TextStyle(fontSize: 36.sp, color: Colors.black87)),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                RawMaterialButton(
                                                  hoverElevation: 0.0,
                                                  highlightElevation: 0.0,
                                                  focusElevation: 0.0,
                                                  constraints: BoxConstraints.tightFor(width: 0.08.sw, height: 0.08.sw),
                                                  elevation: 0,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(8.0),
                                                      side: BorderSide(
                                                          color: dynamicQty > min ? Colors.black87 : Colors.black38,
                                                          width: 1.5)),
                                                  onPressed: dynamicQty == min
                                                      ? null
                                                      : () {
                                                          if (ani > 2 && dynamicQty < 9) ani--;
                                                          dynamicQty =
                                                              dynamicQty == min ? min : dynamicQty -= stepValue;
                                                          tableProvider.setTableGuest(dynamicQty);
                                                        },
                                                  fillColor: Colors.transparent,
                                                  child: Icon(
                                                    MdiIcons.minus,
                                                    color: dynamicQty > min ? Colors.black87 : Colors.black38,
                                                    size: 50.sp,
                                                  ),
                                                ),
                                                Container(
                                                  width: 0.1.sw,
                                                  child: Text(
                                                    '${tableProvider.tableGuest}',
                                                    style: TextStyle(
                                                      fontSize: 22 * 0.8,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                                RawMaterialButton(
                                                  hoverElevation: 0.0,
                                                  highlightElevation: 0.0,
                                                  focusElevation: 0.0,
                                                  constraints: BoxConstraints.tightFor(width: 0.08.sw, height: 0.08.sw),
                                                  elevation: 0,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(8.0),
                                                      side: BorderSide(
                                                          color: dynamicQty < max ? Colors.black87 : Colors.black38,
                                                          width: 1.5)),
                                                  onPressed: dynamicQty == max
                                                      ? null
                                                      : () {
                                                          if (ani < 8) ani++;
                                                          dynamicQty =
                                                              dynamicQty == max ? max : dynamicQty += stepValue;
                                                          tableProvider.setTableGuest(dynamicQty);
                                                        },
                                                  fillColor: Colors.transparent,
                                                  child: Icon(
                                                    MdiIcons.plus,
                                                    color: dynamicQty < max ? Colors.black87 : Colors.black38,
                                                    size: 50.sp,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  ListTile(
                                    tileColor: Colors.white,
                                    onTap: () {
                                      DatePicker.showDateTimePicker(context,
                                          showTitleActions: true,
                                          minTime: DateTime.now(),
                                          maxTime: DateTime(
                                              DateTime.now().year, DateTime.now().month + 1, DateTime.now().day),
                                          onChanged: (date) {
                                        print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());
                                      }, onConfirm: (date) {
                                        tableProvider.setTableSched(date);
                                      },
                                          currentTime: tableProvider.tableBookingSched ?? DateTime.now(),
                                          locale: LocaleType.en);
                                    },
                                    title: Text('Schedule', style: TextStyle(fontSize: 36.sp, color: Colors.black87)),
                                    trailing: schedule(tableProvider),
                                  ),
                                  SizedBox(height: 0.005.sh),
                                  // ListTile(
                                  //   tileColor: Colors.white,
                                  //   onTap: () {
                                  //     DatePicker.showDatePicker(context,
                                  //         showTitleActions: true,
                                  //         minTime: DateTime.now(),
                                  //         maxTime: DateTime(
                                  //             DateTime.now().year, DateTime.now().month + 1, DateTime.now().day),
                                  //         onChanged: (date) {
                                  //       print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());
                                  //     }, onConfirm: (date) {
                                  //       tableProvider.setTableDate(date.toString());
                                  //     }, currentTime: DateTime.now(), locale: LocaleType.en);
                                  //   },
                                  //   title: Text('Date', style: TextStyle(fontSize: 36.sp, color: Colors.black87)),
                                  //   trailing: tableProvider.tableBookingDate != null
                                  //       ? Text('${tableProvider.tableBookingDate}',
                                  //           style: TextStyle(
                                  //               fontSize: 32.sp, color: Colors.black87, fontWeight: FontWeight.bold))
                                  //       : Text('Choose',
                                  //           style: TextStyle(
                                  //               fontSize: 32.sp, color: Colors.black87, fontWeight: FontWeight.bold)),
                                  // ),
                                  // SizedBox(height: 0.005.sh),
                                  // ListTile(
                                  //   tileColor: Colors.white,
                                  //   onTap: () {
                                  //     DatePicker.showPicker(context,
                                  //         pickerModel: TimeHHmmPickerModel(DateTime.now(), LocaleType.en),
                                  //         onChanged: (time) {
                                  //       print('onChanged: $time');
                                  //     }, onConfirm: (time) {
                                  //       tableProvider.setTableTime(time.toString());
                                  //     });
                                  //   },
                                  //   title: Text('Time', style: TextStyle(fontSize: 36.sp, color: Colors.black87)),
                                  //   trailing: tableProvider.tableBookingTime != null
                                  //       ? Text('${tableProvider.tableBookingTime}',
                                  //           style: TextStyle(
                                  //               fontSize: 32.sp, color: Colors.black87, fontWeight: FontWeight.bold))
                                  //       : Text('Choose',
                                  //           style: TextStyle(
                                  //               fontSize: 32.sp, color: Colors.black87, fontWeight: FontWeight.bold)),
                                  // ),
                                  SizedBox(height: 0.005.sh),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                                    child: TextField(
                                      minLines: 1,
                                      maxLines: null,
                                      keyboardType: TextInputType.text,
                                      controller: reqController,
                                      onSubmitted: (value) {
                                        FocusScope.of(context).unfocus();
                                      },
                                      decoration: InputDecoration(
                                        hintText: 'Do you have any request?',
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Colors.transparent),
                                        ),
                                        fillColor: Colors.white,
                                        filled: true,
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Colors.grey),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
          },
        ));
  }

  int calculateDifference(DateTime date) {
    DateTime now = DateTime.now();
    return DateTime(date.year, date.month, date.day).difference(DateTime(now.year, now.month, now.day)).inDays;
  }

  Widget schedule(PRestaurantFoodCart tableProvider) {
    if (tableProvider.tableBookingSched != null) {
      String dt;

      final difference = calculateDifference(tableProvider.tableBookingSched);

      if (difference == 0)
        dt = 'Today, ${DateFormat('kk:mm').format(tableProvider.tableBookingSched)}';
      else if (difference == 1)
        dt = 'Tomorrow, ${DateFormat('kk:mm').format(tableProvider.tableBookingSched)}';
      else
        dt = DateFormat('EEE MMM d, kk:mm').format(tableProvider.tableBookingSched).toString();
      return Text('$dt', style: TextStyle(fontSize: 32.sp, color: Colors.black87, fontWeight: FontWeight.w500));
    } else {
      return Text('Choose', style: TextStyle(fontSize: 32.sp, color: Colors.black87, fontWeight: FontWeight.bold));
    }
  }

  _confirmationDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
            child: Container(
              height: 0.46.sw,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Would you like to: ',
                      style: TextStyle(
                        fontSize: 38.sp,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(height: 0.05.sw),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          SizedBox(
                            height: 0.12.sw,
                            child: RaisedButton(
                              elevation: 0,
                              highlightElevation: 0.0,
                              color: Config.primaryColor,
                              textColor: Colors.white,
                              onPressed: () {
                                ExtendedNavigator.of(context).push(
                                  '/restaurants/menu',
                                  arguments: RestCatogriesArguments(
                                    restId: widget.restId,
                                    restindex: widget.restind,
                                    restSlogan: widget.restSlogan,
                                    preOrder: true,
                                    coverImage: widget.coverImage,
                                    restName: widget.restName,
                                    restimage: widget.restimage,
                                  ),
                                );
                              },
                              child: Text('Place order', style: TextStyle(fontSize: 32.sp)),
                            ),
                          ),
                          SizedBox(
                            height: 0.12.sw,
                            child: RaisedButton(
                              elevation: 0,
                              highlightElevation: 0.0,
                              color: Config.primaryColor,
                              textColor: Colors.white,
                              onPressed: () {},
                              child: Text('Checkout', style: TextStyle(fontSize: 32.sp)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // button(" Place order", () {
                    //   ExtendedNavigator.of(context).push('/restaurants/menu',
                    //       arguments: RestCatogriesArguments(
                    //         restId: widget.restId,
                    //         restindex: widget.restind,
                    //         restSlogan: widget.restSlogan,
                    //         preOrder: true,
                    //         coverImage: widget.coverImage,
                    //         restName: widget.restName,
                    //         restimage: widget.restimage,
                    //       ));
                    // }, Color(0xff101f40), 150),
                    // SizedBox(height: 0.01.sh),
                    // button(" Checkout", () {
                    //   print("tbale no" + "${stateManagment.guestNumber}");
                    //   onlyTableReservatoion(
                    //     widget.restId,
                    //   );

                    //   ExtendedNavigator.of(context).replace('/restaurants/book/table/confirmation',
                    //       arguments: ConfirmationScreenArguments(
                    //         onlybooking: widget.onlybooking,
                    //         tableNumber: "${stateManagment.tableNumber}",
                    //         date: "${stateManagment.date}",
                    //         time: "${stateManagment.time}",
                    //         guest: stateManagment.guestNumber == null ? "1" : "${stateManagment.guestNumber}",
                    //       ));
                    // }, Color(0xff101f40), 100),
                    Row(
                      children: [
                        Spacer(),
                        Spacer(),
                        Spacer(),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}

class TimeHHmmPickerModel extends TimePickerModel {
  TimeHHmmPickerModel(DateTime initialTime, LocaleType localType) : super(currentTime: initialTime, locale: localType);

  @override
  String rightDivider() => "";

  @override
  List<int> layoutProportions() => [100, 100, 1];
}
