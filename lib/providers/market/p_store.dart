import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:wblue_customer/models/market/store.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'package:wblue_customer/services/helper.dart';

DioClient _dioClient = DioClient();

class PStore extends ChangeNotifier {
  PStore() {
    fetchExploreStores();
  }

  List _explore = new List();
  bool _isLoading = false;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  UnmodifiableListView get explore => UnmodifiableListView(_explore);
  RefreshController get refreshController => _refreshController;

  bool get isLoading => _isLoading;
  StoreModel storeApi = StoreModel();

  Future<void> fetchExploreStores(
      {int limit: 10, int page: 1, bool isRefresh: false}) async {
    Map<String, dynamic> body = {
      'key': '',
      'limit': limit.toString(),
      'business_type': BusinessType.marketplace,
    };

    APIResponse res =
        await _dioClient.publicPost('/store-list?page=$page', data: body);
    List data = res.data;

    if (res.code <= 400) {
      if (isRefresh) {
        _explore = new List();
      }

      _explore.addAll(data);

      if (data.length <= 0) {
        if (isRefresh) {
          _refreshController.refreshCompleted();
        }
        _refreshController.loadNoData();
      } else {
        if (isRefresh) {
          _refreshController.refreshCompleted(resetFooterState: true);
        } else {
          _refreshController.loadComplete();
        }
      }
    } else {
      if (isRefresh) {
        _refreshController.refreshFailed();
      } else {
        _refreshController.loadFailed();
      }
    }
    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }

  void setRefreshController(RefreshController value) {
    _refreshController = value;
    notifyListeners();
  }
}
