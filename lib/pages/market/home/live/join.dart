import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/providers/market/live/p_join.dart';

class JoinPage extends StatefulWidget {
  final channelId;
  final hostId;
  final hostImageUrl;
  final hostName;

  const JoinPage(
      {this.channelId, this.hostId, this.hostImageUrl, this.hostName});

  @override
  _JoinPageState createState() => _JoinPageState();
}

class _JoinPageState extends State<JoinPage> {
  @override
  void initState() {
    super.initState();

    context.read<PJoinLive>().setup(
        channelId: widget.channelId,
        hostId: widget.hostId,
        channelName: widget.hostId,
        hostImageUrl: widget.hostImageUrl,
        hostName: widget.hostName);

    context.read<PJoinLive>().initialize();
    context.read<PJoinLive>().setupUserMap();
    context.read<PJoinLive>().createClient();
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews(PJoinLive host) {
    final List<StatefulWidget> list = [];
    host.users.forEach((int uid) {
      if (uid == widget.channelId) {
        list.add(RtcRemoteView.SurfaceView(uid: uid));
      }
    });

    return list;
  }

  /// Video view wrapper
  Widget _videoView(view) {
    return Expanded(child: Container(child: view));
  }

  /// Video layout wrapper
  Widget _viewRows(PJoinLive host) {
    final views = _getRenderViews(host);
    print('views: ${views.length}');

    if (views.length > 0)
      return Container(
          child: Column(
        children: <Widget>[_videoView(views[0])],
      ));

    return Container();
  }

  /// Toolbar layout
  Widget _toolbar(PJoinLive join) {
    return Positioned(
      bottom: 0,
      child: Container(
        height: 0.3.sh,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
              child: ListView.builder(
                reverse: true,
                itemCount: join.infoStrings.length,
                itemBuilder: (BuildContext context, int index) {
                  if (join.infoStrings.isEmpty) {
                    return null;
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 3,
                      horizontal: 10,
                    ),
                    child: (join.infoStrings[index].type == 'join')
                        ? Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 8,
                                  ),
                                  child: Text(
                                    '${join.infoStrings[index].user} joined',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        : (join.infoStrings[index].type == 'message')
                            ? Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: 0.10.sw,
                                      height: 0.10.sw,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        color: Colors.grey,
                                      ),
                                      child: CachedNetworkImage(
                                          imageUrl:
                                              join.infoStrings[index].image ??
                                                  '',
                                          imageBuilder: (context,
                                                  imageProvider) =>
                                              Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(30),
                                                  image: DecorationImage(
                                                    image: imageProvider,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                          placeholder: (context, url) =>
                                              CircularProgressIndicator(),
                                          errorWidget: (context, url, error) =>
                                              Center(
                                                  child: Text(
                                                      '${join.infoStrings[index].user[0]}',
                                                      style: TextStyle(
                                                          color:
                                                              Colors.white)))),
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                              horizontal: 8,
                                            ),
                                            child: Text(
                                              join.infoStrings[index].user,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                              horizontal: 8,
                                            ),
                                            child: Text(
                                              join.infoStrings[index].message,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 14),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              )
                            : null,
                  );
                },
              ),
            ),
            Container(
              color: Colors.black26,
              padding: EdgeInsets.all(8.0),
              child: SafeArea(
                top: false,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Container(
                        child: TextField(
                            cursorColor: Colors.blue,
                            textInputAction: TextInputAction.send,
                            onSubmitted: join.sendMessage,
                            maxLines: null,
                            minLines: 1,
                            style: TextStyle(color: Colors.white),
                            controller: join.channelMessageController,
                            textCapitalization: TextCapitalization.sentences,
                            decoration: InputDecoration(
                              fillColor: Colors.white10,
                              filled: true,
                              isDense: true,
                              hintText: 'Comment',
                              hintStyle: TextStyle(
                                  color: Colors.white, fontSize: 14.0),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                  borderSide:
                                      BorderSide(color: Colors.transparent)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                  borderSide: BorderSide(color: Colors.white)),
                            )),
                      ),
                    ),
                    SizedBox(width: 10.0),
                    FlatButton(
                      minWidth: 0,
                      onPressed: join.toggleSendChannelMessage,
                      child: Icon(
                        Icons.send,
                        color: Colors.white,
                        size: 16.0,
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      color: Colors.white10,
                      padding: const EdgeInsets.all(14.0),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onCallEnd(BuildContext context, PJoinLive host) {
    host.disposeEngine();
    Navigator.pop(context);
  }

  Widget _viwers(PJoinLive join) {
    return Container(
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: Colors.black12,
          borderRadius: BorderRadius.all(
              Radius.circular(5.0) //                 <--- border radius here
              ),
        ),
        child: Row(
          children: [
            Text('LIVE',
                style:
                    TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
            SizedBox(width: 5),
            Icon(
              MdiIcons.circleMedium,
              color: Colors.white,
              size: 18,
            ),
            SizedBox(width: 5),
            Text('${join.userNo}',
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.w200)),
          ],
        ));
  }

  Widget _viewers(PJoinLive join) {
    return Positioned(
        top: 10,
        right: 10,
        child: SafeArea(
          child: Row(
            children: [_viwers(join)],
          ),
        ));
  }

  Widget _backBtn(PJoinLive join) {
    return Positioned(
      left: 10,
      top: 10,
      child: GestureDetector(
        onTap: () => _onCallEnd(context, join),
        child: SafeArea(
          child: Row(
            children: [
              Icon(
                MdiIcons.chevronLeft,
                color: Colors.white,
                size: 0.08.sw,
              ),
              Container(
                width: 0.08.sw,
                height: 0.08.sw,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.grey,
                ),
                child: CachedNetworkImage(
                    imageUrl: join.hostImageUrl ?? '',
                    imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Center(
                        child: Text('${join.hostName[0]}',
                            style: TextStyle(color: Colors.white)))),
              ),
              SizedBox(width: 10),
              AutoSizeText(
                '${join.hostName}',
                style: TextStyle(color: Colors.white),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Consumer<PJoinLive>(builder: (__, join, ___) {
          print('join.isFinished ${join.isFinished}');
          return join.isFinished
              ? Container(
                  color: Colors.black,
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                    child: Container(
                      height: 50,
                      color: Colors.grey,
                      width: MediaQuery.of(context).size.width,
                      child: Center(child: Text('LIVE HAS BEEN ENDED')),
                    ),
                  ),
                )
              : Stack(
                  children: <Widget>[
                    _viewRows(join),
                    _toolbar(join),
                    _viewers(join),
                    _backBtn(join),
                  ],
                );
        }),
      ),
    );
  }
}
