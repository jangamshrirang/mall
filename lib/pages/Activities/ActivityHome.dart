import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:wblue_customer/pages/Activities/ActivityCart.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/widgets/w_drawer.dart';

import 'ActivityExpantion.dart';
import 'IndoorAndOurdoor.dart';

class ActivityHome extends StatefulWidget {
  @override
  _ActivityHomeState createState() => _ActivityHomeState();
}

class _ActivityHomeState extends State<ActivityHome> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithSearchIcons(context, "Activities", null,
          brightnessType: Brightness.light),
      drawer: WDrawerWidget('activities'),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            text(
              "   Top Activities ",
              size.height * 0.023,
              Colors.black,
              1,
              FontWeight.bold,
              size.width * 0.45,
            ),
            topActivities(),
            text(
              "   Categories ",
              size.height * 0.023,
              Colors.black,
              1,
              FontWeight.bold,
              size.width * 0.45,
            ),
            catogories(),
            text(
              "   Suggestions ",
              size.height * 0.023,
              Colors.black,
              1,
              FontWeight.bold,
              size.width * 0.45,
            ),
            Wrap(
              spacing: 0,
              children: [
                activityCard(
                    "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                    "Sand Dunes 4x4",
                    "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.21),
                activityCard(
                    "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                    "Sand Dunes 4x4",
                    "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.21),
                activityCard(
                    "assets/activities/rodrigo-lourenco-m_VDzGhvg_8-unsplash.jpg",
                    "Island Hoping",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.21),
                activityCard(
                    "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                    "Sand Dunes 4x4",
                    "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.21),
                activityCard(
                    "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                    "Sand Dunes 4x4",
                    "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.21),
                activityCard(
                    "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                    "Sand Dunes 4x4",
                    "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.21),
                activityCard(
                    "assets/activities/rodrigo-lourenco-m_VDzGhvg_8-unsplash.jpg",
                    "Island Hoping",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.21),
                activityCard(
                    "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                    "Sand Dunes 4x4",
                    "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.21),
              ],
            )
          ],
        ),
      ),
    );
  }

  topActivities() {
    var size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.35,
        width: size.width,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              activityCard(
                  "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                  "Sand Dunes 4x4",
                  "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                  size.height * 0.3,
                  size.height * 0.25,
                  size.height * 0.21),
              activityCard(
                  "assets/activities/rodrigo-lourenco-m_VDzGhvg_8-unsplash.jpg",
                  "Island Hoping",
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                  size.height * 0.3,
                  size.height * 0.25,
                  size.height * 0.21),
              activityCard(
                  "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                  "Sand Dunes 4x4",
                  "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                  size.height * 0.3,
                  size.height * 0.25,
                  size.height * 0.21),
            ],
          ),
        ));
  }

  catogories() {
    var size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.2,
        width: size.width,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              activityCard(
                  "assets/activities/pexels-jaime-reimer-2749500.jpg",
                  "Outdoor",
                  "",
                  size.height * 0.15,
                  size.height * 0.15,
                  size.height * 0.09),
              activityCard(
                  "assets/activities/pexels-pixabay-51377.jpg",
                  "Indoor",
                  "",
                  size.height * 0.15,
                  size.height * 0.15,
                  size.height * 0.09),
            ],
          ),
        ));
  }

  activityCard(
      String image, title, subtitle, double heigt, widthh, blacktextmargin) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        title == "Outdoor" || title == "Indoor"
            ? Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => InAndOutDoor(
                          title: title,
                        )))
            : Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ActivityExpantion(
                          title: title,
                          subtitle: subtitle,
                          image: image,
                        )));
      },
      child: Container(
          width: widthh,
          height: heigt,
          margin: EdgeInsets.only(bottom: 10, left: 10, top: 5, right: 5),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 1,
                blurRadius: 3,
                offset: Offset(1, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.all(Radius.circular(8)),
            image: DecorationImage(
              image: AssetImage(
                image,
              ),
              fit: BoxFit.fill,
            ),
          ),
          child: Container(
              //text container
              decoration: BoxDecoration(
                color: Colors.black54,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8)),
              ),
              margin: EdgeInsets.only(
                top: blacktextmargin, //23
              ),
              padding: EdgeInsets.only(
                  left: size.width * 0.02,
                  right: size.width * 0.01,
                  top: size.height * 0.01),
              height: size.height * 0.06,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.normal,
                        color: Colors.white,
                        fontSize: size.height * 0.022),
                  ),
                  Text(
                    subtitle,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.normal,
                        color: Colors.white70,
                        fontSize: size.height * 0.015),
                  ),
                ],
              ))),
    );
  }

  text(String text, double height, Color color, int lines,
      FontWeight fontWeight, double width) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: width,
      margin: EdgeInsets.only(left: 10, top: 5),
      child: Text(
        text,
        maxLines: lines,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: fontWeight,
            color: color,
            fontSize: height),
      ),
    );
  }
}
