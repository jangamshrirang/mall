import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/restaurant/menu.dart';
import 'package:wblue_customer/widgets/w_loading.dart';

class WDishGroupItemWidget extends StatelessWidget {
  String image, fromScan, preOrder, delivery, restId, filterKey, fromTakeAway;
  String coverImage, restimage, restName, onlybooking, restSlogan, categoryName;
  var tableId;
  WDishGroupItemWidget(
      {this.image,
      this.fromScan,
      this.preOrder,
      this.delivery,
      this.coverImage,
      this.restimage,
      this.restName,
      this.onlybooking,
      this.restSlogan,
      this.categoryName,
      this.restId,
      this.filterKey,
      this.fromTakeAway,
      this.tableId});
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return InkWell(
      child:Container(
                                  height: size.height * 0.08,
                                  width: size.width * 0.9,
                                  margin: EdgeInsets.only(bottom: 16.0),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    // image: DecorationImage(
                                    //   image: NetworkImage("https://cdn.dribbble.com/users/1012566/screenshots/4187820/topic-2.jpg"),
                                    //   fit: BoxFit.cover,
                                    // ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 2,
                                        blurRadius: 4,
                                        offset: Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(20.0),
                                      bottomLeft: Radius.circular(20.0),
                                      bottomRight: Radius.circular(20.0),
                                    ),
                                  ),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      padding: EdgeInsets.all(8.0),
                                   
                                      child: Text(
                                        categoryName,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
      //  CachedNetworkImage(
      //   imageUrl: image ?? '',
      //   imageBuilder: (context, imageProvider) => Container(
      //     height: 130.0,
      //     width: size.width * 0.9,
      //     margin: EdgeInsets.only(bottom: 16.0),
      //     decoration: BoxDecoration(
      //       image: DecorationImage(
      //         image: NetworkImage(image ?? ''),
      //         fit: BoxFit.cover,
      //       ),
      //       borderRadius: BorderRadius.only(
      //         topLeft: Radius.circular(20.0),
      //         bottomLeft: Radius.circular(20.0),
      //         bottomRight: Radius.circular(20.0),
      //       ),
      //     ),
      //     child: Align(
      //       alignment: Alignment.topLeft,
      //       child: Container(
      //         padding: EdgeInsets.all(8.0),
      //         decoration: BoxDecoration(
      //           color: Colors.black45,
      //           borderRadius: BorderRadius.only(
      //             topLeft: Radius.circular(20.0),
      //             bottomRight: Radius.circular(20.0),
      //           ),
      //         ),
      //         child: Text(
      //           categoryName,
      //           style: TextStyle(
      //             color: Colors.white,
      //             fontWeight: FontWeight.bold,
      //           ),
      //         ),
      //       ),
      //     ),
      //   ),
      //   placeholder: (context, url) => Padding(
      //     padding: const EdgeInsets.all(8.0),
      //     child: WLoadingWidget(),
      //   ),
      //   errorWidget: (context, url, error) => Container(
      //     height: size.height * 0.08,
      //     width: size.width * 0.9,
      //     margin: EdgeInsets.only(bottom: 16.0),
      //     decoration: BoxDecoration(
      //       color: Colors.white,
      //       // image: DecorationImage(
      //       //   image: NetworkImage("https://cdn.dribbble.com/users/1012566/screenshots/4187820/topic-2.jpg"),
      //       //   fit: BoxFit.cover,
      //       // ),
      //       boxShadow: [
      //         BoxShadow(
      //           color: Colors.grey.withOpacity(0.5),
      //           spreadRadius: 2,
      //           blurRadius: 4,
      //           offset: Offset(0, 3), // changes position of shadow
      //         ),
      //       ],
      //       borderRadius: BorderRadius.only(
      //         topLeft: Radius.circular(20.0),
      //         bottomLeft: Radius.circular(20.0),
      //         bottomRight: Radius.circular(20.0),
      //       ),
      //     ),
      //     child: Align(
      //       alignment: Alignment.centerLeft,
      //       child: Container(
      //         padding: EdgeInsets.all(8.0),
      //         // decoration: BoxDecoration(
      //         //   color: Colors.white,
      //         //   borderRadius: BorderRadius.only(
      //         //     topLeft: Radius.circular(20.0),
      //         //     bottomRight: Radius.circular(20.0),
      //         //   ),
      //         // ),
      //         child: Text(
      //           '$categoryName',
      //           style: TextStyle(
      //             color: Colors.black,
      //             fontWeight: FontWeight.bold,
      //           ),
      //         ),
      //       ),
      //     ),
      //   ),
      // ),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MenuPage(
                      tableID: tableId,
                      fromTakeAway: fromTakeAway,
                      appBarTitle: categoryName,
                      from: "catogries",
                      restId: restId,
                      filterKey: filterKey, //filetkey
                      restSlogan: restSlogan,
                      coverImage: coverImage,
                      restName: restName,
                      restimage: restimage,
                      delivery: delivery,
                      fromScan: fromScan,
                      preOrder: preOrder,
                    )));
        // ExtendedNavigator.of(context).root.push('/restaurants/group-menu');
      },
    );
  }
}
