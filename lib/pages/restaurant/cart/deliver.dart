import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_card.dart';

import '../../../env/config.dart';

class DeliverPage extends StatefulWidget {
  @override
  _DeliverPageState createState() => _DeliverPageState();
}

class _DeliverPageState extends State<DeliverPage> {
  LocationResult _locationResult;
  String address = '';
  LatLng initialCenter = LatLng(25.267389469721426, 51.54438178986311);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Deliver'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.check),
            onPressed: () {
              Global.cart = {};
              ExtendedNavigator.of(context).root.push('/delivery-confirmation');
            },
          ),
        ],
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
          children: <Widget>[
            Text('Instructions'),
            SizedBox(height: 4.0),
            RichText(
              text: TextSpan(
                text: 'Please present the ',
                style: Theme.of(context).textTheme.body1,
                children: [
                  TextSpan(text: 'QR Code ', style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: 'of this transaction to the '),
                  TextSpan(text: 'Delivery Driver', style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            Divider(),
            InkWell(
              onTap: () async {
                LocationResult result = await showLocationPicker(
                  context,
                  Config.googleMapAPI,
                  automaticallyAnimateToCurrentLocation: true,
                  initialCenter: initialCenter,
                  myLocationButtonEnabled: true,
                  layersButtonEnabled: true,
                  appBarColor: Colors.amber,
                );
                print('RESULT = $result');
                setState(() {
                  _locationResult = result;
                  address = _locationResult.address;
                  initialCenter = _locationResult.latLng;
                });
              },
              child: WCardWidget(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Set Location:',
                            style: TextStyle(color: Colors.grey),
                          ),
                          Text(address),
                        ],
                      ),
                    ),
                    Icon(
                      Icons.pin_drop,
                      color: Colors.amber,
                    ),
                  ],
                ),
              ),
            ),
            TextField(
              decoration: InputDecoration(
                labelText: 'Your Instuctions',
              ),
              minLines: 1,
              maxLines: 3,
            ),
            Divider(),
            // WReservationDetailsBodyWidget(),
          ],
        ),
      ),
      // bottomNavigationBar: WReservationDetailsBottomWidget(),
    );
  }
}
