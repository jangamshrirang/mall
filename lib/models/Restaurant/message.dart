class RestaurantMessageModel {
  RestaurantMessageModel({this.senderHashid, this.receiverHashid, this.body});

  String senderHashid;
  String receiverHashid;
  String body;

  factory RestaurantMessageModel.fromJson(Map<String, dynamic> json) =>
      RestaurantMessageModel(
        senderHashid:
            json["sender_hashid"] == null ? null : json["sender_hashid"],
        receiverHashid:
            json["receiver_hashid"] == null ? null : json["sender_hashid"],
        body: json["body"] == null ? null : json["body"],
      );
}
