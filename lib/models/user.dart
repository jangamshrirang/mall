import 'package:hive/hive.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'package:wblue_customer/services/helper.dart';

DioClient _dioClient = new DioClient();

class UserModel {
  UserModel({
    this.type,
    this.store,
    this.hashid,
    this.name,
    this.email,
    this.mobileNumber,
    this.joinedDate,
    this.status,
    this.timestamp,
    this.address,
    this.full,
    this.thumbnail,
  });

  String type;
  dynamic store;
  String hashid;
  String name;
  dynamic email;
  String mobileNumber;
  DateTime joinedDate;
  String status;
  String timestamp;
  UserAddressModel address;
  String full;
  String thumbnail;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        type: json["type"],
        store: json["store"],
        hashid: json["hashid"],
        name: json["name"],
        email: json["email"],
        mobileNumber: json["mobile_number"],
        joinedDate: DateTime.parse(json["joined_date"]),
        status: json["status"],
        timestamp: json["timestamp"],
        address: UserAddressModel.fromJson(json["address"]),
        full: json["full"],
        thumbnail: json["thumbnail"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "store": store,
        "hashid": hashid,
        "name": name,
        "email": email,
        "mobile_number": mobileNumber,
        "joined_date": joinedDate.toIso8601String(),
        "status": status,
        "timestamp": timestamp,
        "address": address.toJson(),
        "full": full,
        "thumbnail": thumbnail,
      };

  static createDummy() {
    return UserModel(name: 'NineCloud', mobileNumber: '+97430120033', email: 'gabrielarlo11@gmail.com');
  }

  static Future<UserModel> fetchMyProfile() async {
    try {
      APIResponse res = await _dioClient.privateGet('/account/my-profile');

      if (res.code == 200) {
        final profile = Hive.box('profile');
        await profile.put(res.data['hashid'], res.data);
        return UserModel.fromJson(res.data);
      }

      return UserModel();
    } catch (e) {
      print('Method fetchMyProfile throw errow: $e');
    }
  }

  static Future<Map> fetchUserProfile(String hashid) async {
    Map<String, dynamic> body = {'hash_id': hashid};

    APIResponse res = await _dioClient.privatePost('/account/user-profile', data: body);

    if (res.code >= 400) {
      return null;
    }
    return res.data;
  }
}

class UserAddressModel {
  UserAddressModel({
    this.hashid,
    this.type,
    this.isCurrent,
    this.latitude,
    this.longitude,
    this.nickname,
    this.area,
    this.street,
    this.house,
    this.additionalDirections,
    this.mobile,
    this.landline,
    this.building,
    this.floor,
    this.apartmentNumber,
    this.office,
  });

  String hashid;
  String type;
  int isCurrent;
  String latitude;
  String longitude;
  String nickname;
  String area;
  String street;
  String house;
  String additionalDirections;
  String mobile;
  String landline;
  String building;
  String floor;
  String apartmentNumber;
  String office;

  factory UserAddressModel.fromJson(Map<String, dynamic> json) => UserAddressModel(
        hashid: json["hashid"],
        type: json["type"],
        isCurrent: json["is_current"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        nickname: json["nickname"],
        area: json["area"],
        street: json["street"],
        house: json["house"],
        additionalDirections: json["additional_directions"],
        mobile: json["mobile"],
        landline: json["landline"],
        building: json["building"],
        floor: json["floor"],
        apartmentNumber: json["apartment_number"],
        office: json["office"],
      );

  Map<String, dynamic> toJson() => {
        "hashid": hashid,
        "type": type,
        "is_current": isCurrent,
        "latitude": latitude,
        "longitude": longitude,
        "nickname": nickname,
        "area": area,
        "street": street,
        "house": house,
        "additional_directions": additionalDirections,
        "mobile": mobile,
        "landline": landline,
        "building": building,
        "floor": floor,
        "apartment_number": apartmentNumber,
        "office": office,
      };
}
