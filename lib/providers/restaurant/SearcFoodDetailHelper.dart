import 'package:wblue_customer/services/dio/dio_client.dart';
import 'dart:collection';

import 'package:flutter/foundation.dart';

DioClient _dioClient = DioClient();

class SearchFoodResultHelper with ChangeNotifier {
   List _restaurants = new List();
  bool _isLoading = true;

  UnmodifiableListView get restaurantsSearchFoodResult =>
  UnmodifiableListView(_restaurants);
  bool get isLoading => _isLoading;

  Future<void> fetchSearchFoodResult(String restasID,foodHashId) async {
    print('searchKey :$restasID');

    setLoading(true);
    _restaurants = await fetchSearchFoodResults(restasID,foodHashId);
    setLoading(false);

    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future<List>fetchSearchFoodResults(String restasID,foodHashId) async {
  Map<String, dynamic> _body = {
    'food_hash_id':foodHashId,
    'restaurant_hash_id': restasID,
  };
  APIResponse res =
      await _dioClient.publicPost('/restaurant/food/detail', data: _body);
  if (res.code >= 400) {
    return [];
  }
print(res.data);
  return res.data;
}
