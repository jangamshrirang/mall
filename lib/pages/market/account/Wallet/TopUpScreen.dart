import 'package:flutter/material.dart';
import 'package:marquee_widget/marquee_widget.dart';
import 'package:wblue_customer/pages/Activities/ActivityPayment.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/pages/market/account/Wallet/walletPayment.dart';

class TopUpScreen extends StatefulWidget {
  @override
  _TopUpScreenState createState() => _TopUpScreenState();
}

class _TopUpScreenState extends State<TopUpScreen> {
  List<String> amount = [
    "    ",
    "1000 Qar",
    "2000 Qar",
    "3000 Qar",
  ];

  int secondaryIndex = 0;
  String totalAmount;
  final amountController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "Top Up"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                height: size.height * 0.28,
                width: size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    image: DecorationImage(
                      image: NetworkImage(
                          "https://i.ytimg.com/vi/pNpbmjSMUwI/hqdefault.jpg"),
                      fit: BoxFit.cover,
                    )),
                child: Container(
                  color: Colors.orange,
                  margin: EdgeInsets.only(
                    top: size.height * 0.25,
                  ),
                  child: Row(
                    children: [
                      Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Icon(
                            Icons.volume_up,
                            color: Colors.white,
                            size: 15,
                          )),
                      Container(
                        color: Colors.orange,
                        width: size.width * 0.5,
                        margin: EdgeInsets.only(left: 10),
                        child: Marquee(
                          child: Text(
                            "Win up to Qr 5,000 Wallet Credits using your Wmall Wallet or Wcash from Aug",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: size.height * 0.020),
                          ),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(left: size.width * 0.1),
                          child: Text(
                            "Winner Board",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.normal,
                                fontSize: size.height * 0.020),
                          ))
                    ],
                  ),
                )),
            rowText("Select Top Up amount (QAR) ", Colors.black,
                "Minumum amount (20 Qar)", Colors.black45),
            Container(
              width: double.infinity,
              height: size.height * 0.1,
              //padding: EdgeInsets.only(right:5),
              child: ListView.separated(
                  physics: NeverScrollableScrollPhysics(),
                  separatorBuilder: (context, index) => Divider(
                        color: Colors.white,
                      ),
                  scrollDirection: Axis.horizontal,
                  itemCount: amount.length,
                  itemBuilder: (BuildContext context, int index) {
                    return customRadio2(index, amount[index]);
                  }),
            ),
            SizedBox(height: size.height * 0.02),
            _amountTextField(),
            Text("Please a number between Qar 20.00 to 50,000.00       "),
            SizedBox(height: size.height * 0.2),
            Container(
              //color: Colors.blue,
              margin: EdgeInsets.only(left: 10, right: 10),
              height: size.height * 0.1,
              alignment: Alignment.bottomLeft,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    children: [
                      text(
                          "Total Amount",
                          size.width * 0.6,
                          size.height * 0.025,
                          size.height * 0.023,
                          FontWeight.normal,
                          1,
                          Colors.black),
                      SizedBox(height: size.height * 0.005),
                      text(
                          totalAmount == null ? " 0 QAR " : totalAmount,
                          size.width * 0.6,
                          size.height * 0.05,
                          size.height * 0.04,
                          FontWeight.bold,
                          1,
                          Color(0xff284E99)),
                    ],
                  ),
                  Spacer(),
                  RaisedButton(
                      child: Text(
                        "Continue",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Color(0xff284E99),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5.0))),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => WalletPayment()));
                      })
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  rowText(String text1, Color color1, String text2, Color color) {
    var size = MediaQuery.of(context).size;
    return Row(
      children: [
        Container(
          margin: EdgeInsets.only(
            left: size.width * 0.05,
            bottom: 10,
            top: 10,
          ),
          //color: Colors.blue,
          width: size.width * 0.4,
          child: Text(
            text1,
            style: TextStyle(
                color: color1,
                fontSize: size.height * 0.019,
                fontWeight: FontWeight.bold),
          ),
        ),
        Spacer(),
        Container(
          margin: EdgeInsets.only(bottom: 0),
          //color: Colors.yellow,
          width: size.width * 0.45,
          child: Text(
            text2,
            style: TextStyle(color: color, fontSize: size.height * 0.019),
          ),
        ),
      ],
    );
  }

  void changeSecondaryIndex(int index, String text) {
    setState(() {
      secondaryIndex = index;
      print(text);
      totalAmount = text;
    });
  }

  Widget customRadio2(int index, String text) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
        onTap: () {
          changeSecondaryIndex(index, text);
        },
        child: index == 0
            ? Text(
                "",
                style: TextStyle(fontSize: 0.01),
              )
            : Container(
                height: size.height * 0.13,
                margin: EdgeInsets.only(
                    left: size.width * 0.05, right: size.width * 0.05),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                      width: 1,
                      color: secondaryIndex == index
                          ? Color(0xff101f40)
                          : Colors.black12),
                ),
                child: Row(
                  children: [
                    SizedBox(width: 5),
                    secondaryIndex == index
                        ? Icon(
                            Icons.radio_button_checked,
                            color: Colors.white,
                            size: 0,
                          )
                        : Icon(
                            Icons.radio_button_unchecked,
                            size: 0,
                          ),
                    SizedBox(width: 5),
                    Text(text + "    ",
                        style:
                            TextStyle(color: Color(0xff101f40), fontSize: 16)),
                  ],
                )));
  }

  _amountTextField() {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(
          left: size.width * 0.03, right: size.width * 0.08, top: 2),
      height: size.height * 0.05,
      width: size.width * 0.85,
      child: TextField(
        cursorColor: Colors.black,
        controller: amountController,
        decoration: InputDecoration(
            filled: true,
            labelStyle:
                TextStyle(color: Colors.black38, fontWeight: FontWeight.normal),
            fillColor: Colors.white,
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black, width: 1),
              borderRadius: BorderRadius.circular(00.0),
            ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(00.0)),
                borderSide: BorderSide(color: Colors.black26, width: 1)),
            labelText: "Enter Top Up Amount"),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            totalAmount = amountController.text + " QAR";
            // _notesForDisplay = _notes.where((note) {
            //   var noteTitle = note.title.toLowerCase();
            //   return noteTitle.contains(text);
            // }
            // ).toList();
          });
        },
      ),
    );
  }

  text(String title, double widthh, heightt, textSize, FontWeight fontWeight,
      int lines, Color color) {
    var size = MediaQuery.of(context).size;
    return Container(
      //    color: Colors.red,
      margin: EdgeInsets.only(left: 5, top: 2),
      height: heightt,
      width: widthh,
      child: Text(
        title,
        maxLines: lines,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            color: color,
            fontFamily: 'Montserrat',
            fontWeight: fontWeight,
            fontSize: textSize),
      ),
    );
  }
}
