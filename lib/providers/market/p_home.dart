import 'dart:collection';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:wblue_customer/models/local-data.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

class PMarketHome extends ChangeNotifier {
  DioClient _dioClient = DioClient();
  List _products = new List();
  List _justForYouProducts = new List();
  List _featuredProducts = new List();
  List _hotDealsProducts = new List();
  Map _exclusiveProduct = new Map();
  String _featuredSelectedImg = '';
  List _featuredSelectedColor = [];
  List _featuredSelectedImages = [];
  bool _isLoading;

  UnmodifiableListView get products => UnmodifiableListView(_products);
  UnmodifiableListView get justForYouProducts =>
      UnmodifiableListView(_justForYouProducts);
  UnmodifiableListView get featuredProducts =>
      UnmodifiableListView(_featuredProducts);
  UnmodifiableListView get featuredSelectedColor =>
      UnmodifiableListView(_featuredSelectedColor);
  UnmodifiableListView get featuredSelectedImages =>
      UnmodifiableListView(_featuredSelectedImages);
  UnmodifiableListView get hotDealsProducts =>
      UnmodifiableListView(_hotDealsProducts);
  Map get exclusiveProduct => _exclusiveProduct;

  bool get isLoading => _isLoading;
  String get featuredSelectedImg => _featuredSelectedImg;

  Future<void> fetchProduct() async {
    await Future.delayed(Duration(seconds: 3));
    _products = await LocalData.products();
    notifyListeners();
  }

  void resetProduct() async {
    _justForYouProducts.clear();
  }

  Future<void> fetchJustForYouProducts() async {
    APIResponse res = await _dioClient.publicPost('/product/items');
    if (res.code != 200) {
      return;
    }
    _justForYouProducts.addAll(res.data);

    notifyListeners();
  }

  Future<void> fetchFeaturedProducts() async {
    await Future.delayed(Duration(seconds: 3));
    _featuredProducts = await LocalData.products();
    notifyListeners();
  }

  Future<void> fetchHotDealsProducts() async {
    await Future.delayed(Duration(seconds: 2));
    _hotDealsProducts = await LocalData.products();
    notifyListeners();
  }

  Future<void> fetchExclusiveProduct() async {
    await Future.delayed(Duration(seconds: 3));
    Random random = new Random();
    int randomNumber = random.nextInt(_hotDealsProducts.length);
    _exclusiveProduct = products[randomNumber];
    notifyListeners();
  }

  void selectFeaturedImg(String img) async {
    _featuredSelectedImg = img;
    notifyListeners();
  }

  void selectFeaturedColor(int index, String color) async {
    _featuredSelectedColor.removeAt(index);
    _featuredSelectedColor.insert(index, color);
    notifyListeners();
  }

  void setLoading(value) {
    _isLoading = value;
    notifyListeners();
  }

  Future search(String value) {
    print('yes');
    var temp = _products.firstWhere((item) => item['name'] == value,
        orElse: () => null);
    print('yes');
    print(_products);
    return temp;
  }
}
