import 'package:flutter/material.dart';

import 'BloggersScreen.dart';

class BloggersList extends StatefulWidget {
  @override
  _BloggersListState createState() => _BloggersListState();
}

class _BloggersListState extends State<BloggersList> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      child: SingleChildScrollView(
        child: Wrap(
            children: List.generate(
                10,
                (index) =>
                    Container(  height: size.height * 0.47,child: BloggersScreen()))),
      ),
    );
  }
}
