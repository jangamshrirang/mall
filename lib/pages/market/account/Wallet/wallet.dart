import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:clippy_flutter/arc.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:wblue_customer/env/Lable_clipper.dart';
import 'package:wblue_customer/pages/market/account/Wallet/BillScreen.dart';
import '../../../../env/config.dart';
import 'package:wblue_customer/widgets/w_blue_background.dart';
import 'package:wblue_customer/widgets/w_card_title_link.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_items_wrapper.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';
import 'package:wblue_customer/widgets/w_top_up_card.dart';

import '../paymentScree.dart';
import 'CouponsScree.dart';
import 'LuckyPios/LuckyPisoScreen.dart';
import 'Promotions.dart';
import 'TopUpItemExpantion.dart';
import 'TopUpScreen.dart';
import 'TransationScreen.dart';
import 'VochersScreen.dart';

final List menus = [
  {
    'image': 'assets/images/market/transaction-hist.png',
    'name': 'Transaction History',
    'route': 'transcations'
  },
  {
    'image': 'assets/images/market/payment-opt.png',
    'name': 'Payment Option',
    'route': 'payment'
  },
  {
    'image': 'assets/images/market/voucher-2.png',
    'name': 'Vouchers',
    'route': 'Vouchers'
  },
  {
    'image': 'assets/images/market/purchased-coupon.png',
    'name': 'Purchased Coupons',
    'route': 'Coupons'
  },
  {
    'image': 'assets/images/market/buy-mobile-load.png',
    'name': 'Buy Mobile Load',
    'route': '/'
  },
  {
    'image': 'assets/images/market/bill-payment.png',
    'name': 'Bills Payment',
    'route': 'Bills Payment'
  },
  {
    'image': 'assets/images/market/lucky-piso.png',
    'name': 'lucky piso',
    'route': 'LuckyPiso'
  },
  {'image': 'assets/images/market/loans.png', 'name': 'loans', 'route': '/'},
  {
    'image': 'assets/images/market/payment-opt.png',
    'name': 'Credit Card',
    'route': '/'
  },
  {
    'image': 'assets/images/market/learn-more.png',
    'name': 'learn-more',
    'route': '/'
  },
];

class Chevron extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final Gradient gradient = new LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: [
        Colors.orange,
        Colors.amber,
      ],
      tileMode: TileMode.clamp,
    );

    final Rect colorBounds = Rect.fromLTRB(0, 0, size.width, size.height);
    final Paint paint = new Paint()
      ..shader = gradient.createShader(colorBounds);

    Path path = Path();
    path.moveTo(0, 0);
    path.lineTo(0, size.height);
    path.lineTo(size.width / 2, size.height - size.height / 25);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class MarketWalletPage extends StatelessWidget {
  Widget _balance(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Stack(
      children: [
        Center(
          child: WContainerBlue(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      AutoSizeText(
                        'Available Balance (QAR)',
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(height: 20.0),
                      AutoSizeText(
                        '0.00',
                        style: TextStyle(
                          fontSize: 38,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(height: 40),
                      Row(
                        children: <Widget>[
                          AutoSizeText(
                            'Reabate (QAR) ',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                          AutoSizeText(
                            '0.00',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.amber,
                            ),
                          ),
                          Icon(MdiIcons.chevronRight, color: Colors.amber)
                        ],
                      ),
                    ],
                  ),
                  WRoundedButton(
                    onCustomButtonPressed: () {
                      walletScreenNavigation(context, TopUpScreen());
                    }, //
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Text('Activate'.toUpperCase(),
                          style: TextStyle(fontSize: 14)),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Positioned(
          top: size.height * 0.078,
          left: size.width * 0.75,
          child: RotatedBox(
            quarterTurns: 3,
            child: CustomPaint(
              painter: Chevron(),
              child: Container(
                width: 15.0,
                height: 80.0,
                child: Padding(
                  padding: EdgeInsets.only(top: 30.0),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: Text("", style: TextStyle(fontSize: 24.0)),
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
            top: size.height * 0.082,
            left: size.width * 0.752,
            child: Text(" Win up to Qr 9,000",
                style: TextStyle(
                  fontSize: 11.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                )))
      ],
    );
  }

  Widget _transactionMenu(BuildContext context) {
    return WItemList(
      crossAxisCellCount: 1,
      crossAxisCount: 5,
      widget: (menu, index) => GestureDetector(
        //LuckyPisoScreen
        onTap: () {
          menu['route'] == "transcations"
              ? walletScreenNavigation(context, TransationScreen())
              : menu['route'] == "payment"
                  ? walletScreenNavigation(context, PaymentScreen())
                  : menu['route'] == "Vouchers"
                      ? walletScreenNavigation(context, VocherScreen())
                      : menu['route'] == "Coupons"
                          ? walletScreenNavigation(context, CoupnsScreen())
                          : menu['route'] == "Bills Payment"
                              ? walletScreenNavigation(context, BillScreen())
                              : menu['route'] == "LuckyPiso"
                                  ? walletScreenNavigation(
                                      context, LuckyPisoScreen())
                                  : print(menu['route']);
        },
        child: Container(
          width: 100,
          height: 100,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  child: Container(
                      height: 40,
                      width: 40,
                      child: WImageWidget(
                          placeholder: AssetImage('${menu['image']}')))),
              Expanded(
                child: AutoSizeText(
                  '${menu['name']}',
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
      ),
      items: menus,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Config.bodyColor,
        appBar: AppBar(
          title: Text(
            "Wallet",
            style: TextStyle(color: Colors.black),
          ),
          //brightness: Brightness.light,
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: FlatButton(
            onPressed: () => ExtendedNavigator.of(context).pop(),
            child: Icon(MdiIcons.arrowLeft),
          ),
        ),
        body: ListView(
          children: [
            _balance(context),
            SizedBox(height: 8),
            Card(
                elevation: 0.0,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: _transactionMenu(context),
                )),
            SizedBox(height: 8),
            //image carosle
            WCardTitleLink(
              title: 'Load in 1 Click',
              linkTitle: 'View All',
              widget: Pramotions(),
            ),
            Text(
              "   Recent Transaction",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 5),
            listtile(
              context,
              "assets/images/TopUpItemIcon.png",
              "133.20",
            ),
            listtile(
              context,
              "assets/images/TopUpItemIcon.png",
              "155.20",
            ),
          ],
        ));
  }

  walletScreenNavigation(BuildContext context, screen) {
    return Navigator.push(
        context, MaterialPageRoute(builder: (context) => screen));
  }

  listtile(
    BuildContext context,
    String image,
    String title,
  ) {
    var size = MediaQuery.of(context).size;
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          ListTile(
              focusColor: Colors.white,
              leading: Image.asset(
                image,
                height: 25,
              ),
              title: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: size.height * 0.02),
                      Text("Purchase"),
                      Text(
                        "Date:20-02-2020",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.normal,
                            color: Colors.black45),
                      ),
                    ],
                  ),
                  Spacer(),
                  Column(
                    children: [
                      SizedBox(height: size.height * 0.02),
                      Text(
                        title,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "Success",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.normal,
                            color: Colors.black45),
                      ),
                    ],
                  ),
                  Icon(
                    Icons.chevron_right,
                    color: Colors.black26,
                  ),
                ],
              ),
              // subtitle: Text("date"),
              onTap: () {
                walletScreenNavigation(context, TransactionExpantion());
              }),
          Divider(),
        ],
      ),
    );
  }
}
