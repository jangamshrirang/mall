import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

class PayScreen extends StatefulWidget {
  int colorVal = 0xff84020e;
int intialIndex;
PayScreen({
  this.intialIndex
});
  @override
  _PayScreenState createState() => _PayScreenState();
}

class _PayScreenState extends State<PayScreen> with TickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() { 
    super.initState();
    _tabController = new TabController(vsync: this, length: 4,initialIndex: widget.intialIndex);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {
      widget.colorVal = 0xff84020e;
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "My Orders"),
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: DefaultTabController(
          length: 4,
          child: Column(
            children: <Widget>[
              TabBar(
                controller: _tabController,
                indicatorSize: TabBarIndicatorSize.tab,
                indicatorColor: Colors.red,
                unselectedLabelColor: Colors.grey,
                tabs: <Widget>[
                  Tab(child: text("All", 0)),
                  Tab(
                    child: text("To Pay", 1),
                  ),
                  Tab(
                    child: text("To Ship", 2),
                  ),
                  Tab(
                    child: text("To Receive", 3),
                  ),
                ],
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    payScreenContainer(),
                    payScreenContainer(),
                    payScreenContainer(),
                    payScreenContainer(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  text(String title, int number) {
    return Text(
      title,
      style: TextStyle(
          color: _tabController.index == number ? Colors.red : Colors.black),
      textAlign: TextAlign.start,
    );
  }

  payScreenContainer() {
    return Column(
      children: [
        SizedBox(
          height: 20,
        ),
        Image.asset(
          "assets/images/empty.png",
          height: 200,
          color: Config.primaryColor,
        ),
        Text(
          "There are no orders placed yet",
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.normal),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: 50,
        ),
        RaisedButton(
          onPressed: () {},
          textColor: Colors.white,
          padding: const EdgeInsets.all(0.0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(2.0)),
          child: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xFF0D47A1),
                    Color(0xFF1976D2),
                    Color(0xFF42A5F5),
                  ],
                ),
                borderRadius: BorderRadius.all(Radius.circular(2.0))),
            padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
            child:
                const Text('Continue Shopping', style: TextStyle(fontSize: 20)),
          ),
        ),
      ],
    );
  }
}
