import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

class BillScreen extends StatefulWidget {
  @override
  _BillScreenState createState() => _BillScreenState();
}

class _BillScreenState extends State<BillScreen> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "Utilities"),
      body: Container(
        height: size.height * 1,
        width: size.width,
        child: ListView(
          children: ListTile.divideTiles(
            context: context,
            tiles: [
              SizedBox(
                height: 10,
              ),
              listtile(
                  context,
                  "https://d338t8kmirgyke.cloudfront.net/icons/icon_pngs/000/007/598/original/technology.png",
                  "Cable / Internet", () {
                print("Cable / Internet");
              }),
              listtile(
                  context,
                  "https://cdn0.iconfinder.com/data/icons/electrical-devices/100/.svg-4-512.png",
                  "Electricity", () {
                print("Electricity");
              }),
              listtile(
                  context,
                  "https://image.flaticon.com/icons/png/512/3031/3031224.png",
                  "Telecommunication", () {
                print("Telecommunication");
              }),
              SizedBox(
                height: 10,
              ),
              Image.network(
                  "https://image.freepik.com/free-vector/online-payment-isometric-concept-landing-page-homepage-website_1124-1410.jpg"),
            ],
          ).toList(),
        ),
      ),
    );
  }

  listtile(BuildContext context, String image, String title, Function ontap) {
    return ListTile(
      leading: Image.network(image, height: 30, color: Colors.amber),
      title: Text(title),
      onTap: ontap,
    );
  }

  billScreenNavigation(BuildContext context, screen) {
    return Navigator.push(
        context, MaterialPageRoute(builder: (context) => screen));
  }
}
