import 'package:wblue_customer/services/dio/dio_client.dart';
import 'dart:collection';

import 'package:flutter/foundation.dart';

DioClient _dioClient = DioClient();

class R_RemoveItemCartHelper with ChangeNotifier {
  var _restaurants;
  bool _isLoading = true;

  UnmodifiableListView get rRemoveItemCart =>
      UnmodifiableListView(_restaurants);
  bool get isLoading => _isLoading;

  Future<void> fetchRestaurantsRemoveItemCart(var foodId, cartNo) async {
    setLoading(true);
    _restaurants = await fetchRestaurantRemoveItemsCart(foodId, cartNo);
    setLoading(false);

    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future fetchRestaurantRemoveItemsCart(var foodId, cartNo) async {
  Map<String, dynamic> _body = {
    'cart_no': cartNo,
    'food_id': foodId,
  };
  APIResponse res =
      await _dioClient.privatePost('/restaurant/cart/remove', data: _body);
  if (res.code >= 400) {
    return [];
  }
  print("removing item from cart result: "+res.message);

  return res.data;
}
