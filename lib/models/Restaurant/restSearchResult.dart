class RestSearcResult {
    RestSearcResult({
        this.fullCover,
        this.thumbnailCover,
        this.hashId,
        this.restaurant,
        this.email,
        this.storeInformation,
        this.contact,
        this.city,
        this.full,
        this.thumbnail,
        this.timings,
        this.ratings,
        this.numberOfFollowers,
    });

    String fullCover;
    String thumbnailCover;
    String hashId;
    String restaurant;
    String email;
    String storeInformation;
    String contact;
    String city;
    String full;
    String thumbnail;
    Timings timings;
    Ratings ratings;
    int numberOfFollowers;

    factory RestSearcResult.fromJson(Map<String, dynamic> json) => RestSearcResult(
        fullCover: json["full_cover"] == null ? null : json["full_cover"],
        thumbnailCover: json["thumbnail_cover"] == null ? null : json["thumbnail_cover"],
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        restaurant: json["Restaurant"] == null ? null : json["Restaurant"],
        email: json["Email"] == null ? null : json["Email"],
        storeInformation: json["Store_Information"] == null ? null : json["Store_Information"],
        contact: json["Contact"] == null ? null : json["Contact"],
        city: json["City"] == null ? null : json["City"],
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        timings: json["timings"] == null ? null : Timings.fromJson(json["timings"]),
        ratings: json["ratings"] == null ? null : Ratings.fromJson(json["ratings"]),
        numberOfFollowers: json["number_of_followers"] == null ? null : json["number_of_followers"],
    );

    Map<String, dynamic> toJson() => {
        "full_cover": fullCover == null ? null : fullCover,
        "thumbnail_cover": thumbnailCover == null ? null : thumbnailCover,
        "hash_id": hashId == null ? null : hashId,
        "Restaurant": restaurant == null ? null : restaurant,
        "Email": email == null ? null : email,
        "Store_Information": storeInformation == null ? null : storeInformation,
        "Contact": contact == null ? null : contact,
        "City": city == null ? null : city,
        "full": full == null ? null : full,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "timings": timings == null ? null : timings.toJson(),
        "ratings": ratings == null ? null : ratings.toJson(),
        "number_of_followers": numberOfFollowers == null ? null : numberOfFollowers,
    };
}

class Ratings {
    Ratings({
        this.count,
        this.average,
    });

    int count;
    dynamic average;

    factory Ratings.fromJson(Map<String, dynamic> json) => Ratings(
        count: json["count"] == null ? null : json["count"],
        average: json["average"],
    );

    Map<String, dynamic> toJson() => {
        "count": count == null ? null : count,
        "average": average,
    };
}

class Timings {
    Timings({
        this.now,
        this.status,
        this.next,
    });

    String now;
    String status;
    String next;

    factory Timings.fromJson(Map<String, dynamic> json) => Timings(
        now: json["now"] == null ? null : json["now"],
        status: json["status"] == null ? null : json["status"],
        next: json["next"] == null ? null : json["next"],
    );

    Map<String, dynamic> toJson() => {
        "now": now == null ? null : now,
        "status": status == null ? null : status,
        "next": next == null ? null : next,
    };
}
