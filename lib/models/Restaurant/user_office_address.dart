class UserOfficeAddressModel {
  UserOfficeAddressModel({
    this.hashid,
    this.addressNickname,
    this.area,
    this.building,
    this.floor,
    this.office,
    this.additionalDirections,
    this.street,
    this.phone,
    this.landline,
    this.iso,
    this.lat,
    this.lng,
  });

  String hashid;
  String addressNickname;
  String area;
  String building;
  String floor;
  String office;
  String additionalDirections;
  String street;
  String phone;
  String landline;
  String iso;
  String lat;
  String lng;

  factory UserOfficeAddressModel.fromJson(Map<String, dynamic> json) => UserOfficeAddressModel(
        hashid: json["hashid"],
        addressNickname: json["address_nickname"],
        area: json["area"],
        building: json["building"],
        floor: json["floor"],
        office: json["office"],
        additionalDirections: json["additional_directions"],
        street: json["street"],
        phone: json["phone"],
        landline: json["landline"],
        iso: json["iso"],
        lat: json["lat"],
        lng: json["lng"],
      );

  Map<String, dynamic> toJson() => {
        "hashid": hashid,
        "address_nickname": addressNickname,
        "area": area,
        "building": building,
        "floor": floor,
        "office": office,
        "additional_directions": additionalDirections,
        "street": street,
        "phone": phone,
        "landline": landline,
        "iso": iso,
        "lat": lat,
        "lng": lng,
      };
}

// FIXME: Remove this soon
Map<String, dynamic> sampleUserOfficeAddr = {
  "hashid": "zwjEe2kMDVq8PqG6vWbRr7Qx",
  "address_nickname": "Building No. 20 (Najma)",
  "area": "Najma",
  "building": "20",
  "floor": "M",
  "office": "04",
  "additional_directions": "",
  "street": "Al Khalidiya Street",
  "phone": "5549 5421",
  'iso': "+974",
  "landline": "9324 2231",
  "lat": "25.267582607201845",
  "lng": "51.54447600245476"
};
