import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/models/validation.dart';

class PCreateLive with ChangeNotifier {
  String _name = '';
  int _channelId;
  File _thumbnail;
  bool _isLoading = false;
  bool _isValid = false;

  ValidationItem _liveTitle = ValidationItem(null, null);

  String get name => _name;
  int get channelId => _channelId;
  File get thumbnail => _thumbnail;
  bool get isLoading => _isLoading;
  bool get isValid => _isValid;
  ValidationItem get liveTitle => _liveTitle;

  void changeLiveTitle(String value) {
    if (value.length > 0) {
      _liveTitle = ValidationItem(value, null);
    } else {
      _liveTitle = ValidationItem(null, 'Must be at least not empty');
    }

    notifyListeners();
  }

  void setName(String name) {
    _name = name;
    notifyListeners();
  }

  void setThumbnail(File image) {
    _thumbnail = image;
    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }

  void checkFields() {
    if (_liveTitle.value == null)
      _liveTitle = ValidationItem(null, 'Must be at least not empty');

    if (_thumbnail != null && _liveTitle.value.length > 0) _isValid = true;
    notifyListeners();
  }

  void setChannelId(int channelId) {
    _channelId = channelId;
  }
}
