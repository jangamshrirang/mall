import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import '../env/config.dart';

class WCategoryType extends StatelessWidget {
  final IconData iconData;
  final String title;
  final bool isActive;
  final Function() onPressed;

  WCategoryType({this.isActive: false, this.iconData: Icons.clear, this.title, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onPressed(),
      child: ConstrainedBox(
        constraints: new BoxConstraints(
          minHeight: 80.0,
        ),
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                blurRadius: 1,
                spreadRadius: 1,
                offset: Offset(1, 1), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), bottomRight: Radius.circular(10.0)),
            color: isActive ? Config.dullColor : Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  iconData,
                  size: 30,
                  color: isActive ? Config.secondaryColor : Colors.grey[600],
                ),
                SizedBox(height: 8),
                AutoSizeText(
                  '$title',
                  style: TextStyle(fontSize: 14, color: isActive ? Config.secondaryColor : Colors.grey[600]),
                  maxLines: 2,
                  minFontSize: 14,
                  textAlign: TextAlign.center,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

void function() {}
