class SearchFoodDetailsModel {
    SearchFoodDetailsModel({
        this.hashid,
        this.name,
        this.price,
        this.description,
        this.status,
        this.isAvailable,
        this.full,
        this.thumbnail,
        this.variation,
    });

    String hashid;
    String name;
    int price;
    String description;
    String status;
    String isAvailable;
    String full;
    String thumbnail;
    List<dynamic> variation;

    factory SearchFoodDetailsModel.fromJson(Map<String, dynamic> json) => SearchFoodDetailsModel(
        hashid: json["hashid"] == null ? null : json["hashid"],
        name: json["name"] == null ? null : json["name"],
        price: json["price"] == null ? null : json["price"],
        description: json["description"] == null ? null : json["description"],
        status: json["status"] == null ? null : json["status"],
        isAvailable: json["is_available"] == null ? null : json["is_available"],
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        variation: json["variation"] == null ? null : List<dynamic>.from(json["variation"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "hashid": hashid == null ? null : hashid,
        "name": name == null ? null : name,
        "price": price == null ? null : price,
        "description": description == null ? null : description,
        "status": status == null ? null : status,
        "is_available": isAvailable == null ? null : isAvailable,
        "full": full == null ? null : full,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "variation": variation == null ? null : List<dynamic>.from(variation.map((x) => x)),
    };
}
