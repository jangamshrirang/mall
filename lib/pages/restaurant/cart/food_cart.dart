import 'package:auto_route/auto_route.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/extensions/w_money_formatter.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/services/helper.dart';

class RestaurantFoodCart extends StatefulWidget {
  final String restId, filterkey, appBarTitle, reservationHashID, fromMenu;
  final String coverImage, restimage, restName, onlybooking, restSlogan;
  final totlaPrice;
  final bool isFromTakeAway, isFromScan, isDelivery, preOrder;

  const RestaurantFoodCart({
    this.isFromScan,
    this.preOrder,
    this.isDelivery,
    this.totlaPrice,
    this.coverImage,
    this.restimage,
    this.restName,
    this.restId,
    this.restSlogan,
    this.onlybooking,
    this.filterkey,
    this.isFromTakeAway,
    this.appBarTitle,
    this.fromMenu,
    this.reservationHashID,
  });

  @override
  _RestaurantFoodCartState createState() => _RestaurantFoodCartState();
}

class _RestaurantFoodCartState extends State<RestaurantFoodCart> {
  TextEditingController _noteTextFieldController = TextEditingController();
  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      context.read<PRestaurantFoodCart>().fetchRCartList();
      context.read<PRestaurantFoodCart>().getAddress();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PRestaurantFoodCart>(
      builder: (context, cart, child) => Scaffold(
        backgroundColor: Colors.white,
        bottomNavigationBar: cart.restaurantCart.food.length > 0
            ? Consumer<PRestaurantFoodCart>(
                builder: (context, checkCart, child) => Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0.0, 1.0), //(x,y)
                        blurRadius: 1.0,
                      ),
                    ]),
                    height: 0.13.sh,
                    child: BottomAppBar(
                      child: Consumer<PRestaurantFoodCart>(
                        builder: (context, cart, child) => Container(
                          padding: EdgeInsets.all(10.0),
                          child: RaisedButton(
                            onPressed: cart.isFetchingAddr
                                ? null
                                : () async {
                                    ExtendedNavigator.of(context).push('/restaurants/place-order',
                                        arguments: RestaurantPlaceOrderPageArguments(
                                            cartId: checkCart.restaurantCart.cartHashId,
                                            note: _noteTextFieldController.text));
                                  },
                            color: Config.primaryColor,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'CHECKOUT',
                                  style: TextStyle(color: Colors.white, fontSize: 34.sp, fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )),
              )
            : SizedBox(),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(0.07.sh), // here the desired height
          child: AppBar(
            backgroundColor: Config.primaryColor,
            title: Column(
              children: [
                Text(
                  'Cart',
                  style: TextStyle(fontSize: 42.sp),
                ),
                cart.restaurantCart.food.length > 0
                    ? Column(
                        children: [
                          SizedBox(height: 0.01.sh),
                          Text(
                            '${cart.restName}',
                            style: TextStyle(fontSize: 32.sp),
                          ),
                        ],
                      )
                    : SizedBox(),
              ],
            ),
          ),
        ),
        body: cart.restaurantCart.food.length > 0
            ? CustomScrollView(
                slivers: [
                  SliverToBoxAdapter(
                      child: Column(
                    children: [
                      Column(
                        children: List.generate(
                            cart.restaurantCart.food.length,
                            (index) => Container(
                                margin: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(color: Colors.black12, width: 1.5),
                                  borderRadius: BorderRadius.circular(12.0),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Column(
                                    children: [
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            child: CachedNetworkImage(
                                              imageUrl: cart.restaurantCart.food[index].foodInfo.full ?? '',
                                              imageBuilder: (context, imageProvider) => Row(
                                                children: [
                                                  Container(
                                                    width: 0.2.sw,
                                                    height: 0.2.sw,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(10),
                                                      image: DecorationImage(
                                                        image: imageProvider,
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(width: 0.02.sw),
                                                ],
                                              ),
                                              placeholder: (context, url) => Row(
                                                children: [
                                                  Container(
                                                    width: 0.2.sw,
                                                    height: 0.2.sw,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(10),
                                                      image: DecorationImage(
                                                        image: NetworkImage(
                                                            cart.restaurantCart.food[index].foodInfo.thumbnail ?? ''),
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(width: 0.02.sw),
                                                ],
                                              ),
                                              errorWidget: (context, url, error) => SizedBox(),
                                            ),
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  '${cart.restaurantCart.food[index].foodInfo.name}',
                                                  style: TextStyle(fontSize: 34.sp, fontWeight: FontWeight.bold),
                                                  overflow: TextOverflow.ellipsis,
                                                ),
                                                SizedBox(height: 0.01.sw),
                                                Text(
                                                  'QR ${cart.restaurantCart.food[index].foodInfo.price}',
                                                  style: TextStyle(fontSize: 30.sp, fontWeight: FontWeight.bold),
                                                  overflow: TextOverflow.ellipsis,
                                                ),
                                                SizedBox(height: 0.005.sw),
                                                variationValue(cart, index),
                                                cart.restaurantCart.food[index].foodInfo.description.length > 0
                                                    ? Text(
                                                        '${cart.restaurantCart.food[index].foodInfo.description}',
                                                        style: TextStyle(
                                                          fontSize: 32.sp,
                                                        ),
                                                        overflow: TextOverflow.ellipsis,
                                                      )
                                                    : SizedBox(),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'QR ${(cart.restaurantCart.food[index].totalamount)}',
                                            style: TextStyle(fontSize: 32.sp, fontWeight: FontWeight.bold),
                                          ),
                                          cartCounter(cart, index),
                                        ],
                                      ),
                                      //  Text('${cart.items[index].}')
                                    ],
                                  ),
                                ))),
                      ),
                      SizedBox(height: 0.05.sh),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'General Request',
                              style: TextStyle(fontSize: 34.sp, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 0.02.sh),
                            Text(
                              'Add a note',
                              style: TextStyle(fontSize: 28.sp, fontWeight: FontWeight.w500),
                            ),
                            TextField(
                              minLines: 1,
                              maxLines: null,
                              controller: _noteTextFieldController,
                              keyboardType: TextInputType.text,
                              onSubmitted: (value) {
                                FocusScope.of(context).unfocus();
                              },
                              decoration: InputDecoration(
                                hintText: 'Anything else we need to know?',
                                hintStyle: TextStyle(fontSize: 26.sp),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Config.primaryColor),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 0.03.sh),
                      Container(
                        color: Colors.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                                margin: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Payment summary',
                                      style: TextStyle(fontSize: 34.sp, fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(height: 0.022.sh),
                                    Container(
                                      height: 0.2.sw,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                'Cart total',
                                                style: TextStyle(fontSize: 30.sp),
                                              ),
                                              Text(
                                                '${cart.restaurantCart.totalInfo.total.moneyFromNum()}',
                                                style: TextStyle(fontSize: 30.sp),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                'Delivery fee',
                                                style: TextStyle(fontSize: 30.sp),
                                              ),
                                              Text(
                                                '${cart.deliveryFee.moneyFromNum()}',
                                                style: TextStyle(fontSize: 30.sp),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                'Total Amount',
                                                style: TextStyle(fontSize: 32.sp, fontWeight: FontWeight.bold),
                                              ),
                                              Text(
                                                '${(cart.totalAmount.moneyFromNum())}',
                                                style: TextStyle(fontSize: 32.sp, fontWeight: FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      )
                    ],
                  ))
                ],
              )
            : SizedBox(),
      ),
    );
  }

  Widget variationValue(PRestaurantFoodCart cart, int index) {
    List<String> _variations = new List<String>();
    for (var variations in cart.restaurantCart.food[index].foodvariation) {
      _variations.add(variations.variationTypeSelected.name);
    }

    return _variations.length > 0
        ? Wrap(
            children: List.generate(
            _variations.length,
            (i) => Text(
              '${_variations[i]}${(_variations.length - 1) == i ? '' : ', '}',
              style: TextStyle(fontSize: 28.sp, fontWeight: FontWeight.w500, color: Colors.black38),
              overflow: TextOverflow.ellipsis,
            ),
          ))
        : SizedBox();
  }

  Widget cartCounter(PRestaurantFoodCart cart, int index) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            RawMaterialButton(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              constraints: BoxConstraints.tightFor(width: 0.08.sw, height: 0.08.sw),
              elevation: 0,
              onPressed: () {
                cart.deductQty(food: cart.restaurantCart, index: index);
              },
              fillColor: Colors.transparent,
              child: Icon(
                MdiIcons.minus,
                color: Colors.black87,
                size: 36.sp,
              ),
            ),
            Container(
              width: 0.1.sw,
              child: Text(
                '${cart.restaurantCart.food[index].quantity}',
                style: TextStyle(
                  fontSize: 22 * 0.8,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            RawMaterialButton(
              constraints: BoxConstraints.tightFor(width: 0.08.sw, height: 0.08.sw),
              elevation: 0,
              onPressed: () {
                cart.addQty(food: cart.restaurantCart, index: index);
              },
              fillColor: Colors.transparent,
              child: Icon(
                MdiIcons.plus,
                color: Colors.black87,
                size: 36.sp,
              ),
            ),
          ],
        ),
      ],
    );
  }

  void showCartAlertDialog(BackButtonBehavior backButtonBehavior,
      {VoidCallback cancel, VoidCallback confirm, VoidCallback backgroundReturn}) {
    BotToast.showAnimationWidget(
        clickClose: true,
        allowClick: false,
        onlyOne: true,
        crossPage: true,
        backgroundColor: Colors.black54,
        backButtonBehavior: backButtonBehavior,
        toastBuilder: (cancelFunc) => AlertDialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              title: const Text('Clear Cart?'),
              content: Text(
                  'You have already selected different restaurant. If you continue your cart and selection will be removed.'),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    cancelFunc();
                    cancel?.call();
                  },
                  highlightColor: const Color(0x55FF8A80),
                  splashColor: const Color(0x99FF8A80),
                  child: Text(
                    'cancel',
                    style: TextStyle(color: Colors.redAccent, fontSize: 36.sp),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    cancelFunc();
                    confirm?.call();
                  },
                  child: Text(
                    'confirm',
                    style: TextStyle(fontSize: 36.sp),
                  ),
                ),
              ],
            ),
        animationDuration: Duration(milliseconds: 300));
  }
}
