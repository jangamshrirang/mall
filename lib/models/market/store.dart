class StoreModel {
  StoreModel({
    this.hashid,
    this.storeName,
    this.storeDescription,
    this.businessType,
    this.storePhoneNumber,
    this.storeEmail,
    this.street,
    this.latitude,
    this.longitude,
    this.state,
    this.city,
    this.otherAddressInfo,
    this.vendor,
    this.statusInfo,
    this.ratings,
    this.themes,
    this.totalStaff,
    this.totalProduct,
    this.totalFoods,
    this.totalBranch,
    this.totalLike,
    this.totalFollowing,
    this.mainBranchHashid,
    this.timestamp,
    this.profilePhotoFull,
    this.profilePhotoThumbnail,
    this.coverPhotoFull,
    this.coverPhotoThumbnail,
  });

  String hashid;
  String storeName;
  String storeDescription;
  String businessType;
  String storePhoneNumber;
  String storeEmail;
  String street;
  String latitude;
  String longitude;
  String state;
  String city;
  String otherAddressInfo;
  Vendor vendor;
  String statusInfo;
  Ratings ratings;
  StoreThemesModel themes;
  int totalStaff;
  int totalProduct;
  int totalFoods;
  int totalBranch;
  int totalLike;
  int totalFollowing;
  String mainBranchHashid;
  String timestamp;
  String profilePhotoFull;
  String profilePhotoThumbnail;
  String coverPhotoFull;
  String coverPhotoThumbnail;

  factory StoreModel.fromJson(Map<String, dynamic> json) => StoreModel(
        hashid: json["hashid"] == null ? null : json["hashid"],
        storeName: json["store_name"] == null ? null : json["store_name"],
        storeDescription: json["store_description"] == null
            ? null
            : json["store_description"],
        businessType:
            json["business_type"] == null ? null : json["business_type"],
        storePhoneNumber: json["store_phone_number"] == null
            ? null
            : json["store_phone_number"],
        storeEmail: json["store_email"] == null ? null : json["store_email"],
        street: json["street"] == null ? null : json["street"],
        latitude: json["latitude"] == null ? null : json["latitude"],
        longitude: json["longitude"] == null ? null : json["longitude"],
        state: json["state"] == null ? null : json["state"],
        city: json["city"] == null ? null : json["city"],
        otherAddressInfo: json["other_address_info"] == null
            ? null
            : json["other_address_info"],
        vendor: json["vendor"] == null ? null : Vendor.fromJson(json["vendor"]),
        statusInfo: json["status_info"] == null ? null : json["status_info"],
        ratings:
            json["ratings"] == null ? null : Ratings.fromJson(json["ratings"]),
        themes: json["themes"] == null
            ? null
            : StoreThemesModel.fromJson(json["themes"]),
        totalStaff: json["total_staff"] == null ? null : json["total_staff"],
        totalProduct:
            json["total_product"] == null ? null : json["total_product"],
        totalFoods: json["total_foods"] == null ? null : json["total_foods"],
        totalBranch: json["total_branch"] == null ? null : json["total_branch"],
        totalLike: json["total_like"] == null ? null : json["total_like"],
        totalFollowing:
            json["total_following"] == null ? null : json["total_following"],
        mainBranchHashid: json["main_branch_hashid"] == null
            ? null
            : json["main_branch_hashid"],
        timestamp: json["timestamp"] == null ? null : json["timestamp"],
        profilePhotoFull: json["profile_photo_full"] == null
            ? null
            : json["profile_photo_full"],
        profilePhotoThumbnail: json["profile_photo_thumbnail"] == null
            ? null
            : json["profile_photo_thumbnail"],
        coverPhotoFull:
            json["cover_photo_full"] == null ? null : json["cover_photo_full"],
        coverPhotoThumbnail: json["cover_photo_thumbnail"] == null
            ? null
            : json["cover_photo_thumbnail"],
      );

  Map<String, dynamic> toJson() => {
        "hashid": hashid == null ? null : hashid,
        "store_name": storeName == null ? null : storeName,
        "store_description": storeDescription == null ? null : storeDescription,
        "business_type": businessType == null ? null : businessType,
        "store_phone_number":
            storePhoneNumber == null ? null : storePhoneNumber,
        "store_email": storeEmail == null ? null : storeEmail,
        "street": street == null ? null : street,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "state": state == null ? null : state,
        "city": city == null ? null : city,
        "other_address_info":
            otherAddressInfo == null ? null : otherAddressInfo,
        "vendor": vendor == null ? null : vendor.toJson(),
        "status_info": statusInfo == null ? null : statusInfo,
        "ratings": ratings == null ? null : ratings.toJson(),
        "themes": themes == null ? null : themes.toJson(),
        "total_staff": totalStaff == null ? null : totalStaff,
        "total_product": totalProduct == null ? null : totalProduct,
        "total_foods": totalFoods == null ? null : totalFoods,
        "total_branch": totalBranch == null ? null : totalBranch,
        "total_like": totalLike == null ? null : totalLike,
        "total_following": totalFollowing == null ? null : totalFollowing,
        "main_branch_hashid":
            mainBranchHashid == null ? null : mainBranchHashid,
        "timestamp": timestamp == null ? null : timestamp,
        "profile_photo_full":
            profilePhotoFull == null ? null : profilePhotoFull,
        "profile_photo_thumbnail":
            profilePhotoThumbnail == null ? null : profilePhotoThumbnail,
        "cover_photo_full": coverPhotoFull == null ? null : coverPhotoFull,
        "cover_photo_thumbnail":
            coverPhotoThumbnail == null ? null : coverPhotoThumbnail,
      };
}

class Ratings {
  Ratings({
    this.count,
    this.average,
  });

  int count;
  int average;

  factory Ratings.fromJson(Map<String, dynamic> json) => Ratings(
        count: json["count"] == null ? null : json["count"],
        average: json["average"] == null ? null : json["average"],
      );

  Map<String, dynamic> toJson() => {
        "count": count == null ? null : count,
        "average": average == null ? null : average,
      };
}

class StoreThemesModel {
  StoreThemesModel({
    this.id,
    this.name,
    this.color1,
    this.color2,
    this.color3,
    this.color4,
  });

  int id;
  String name;
  String color1;
  String color2;
  String color3;
  String color4;

  factory StoreThemesModel.fromJson(Map<String, dynamic> json) =>
      StoreThemesModel(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        color1: json["color1"] == null ? null : json["color1"],
        color2: json["color2"] == null ? null : json["color2"],
        color3: json["color3"] == null ? null : json["color3"],
        color4: json["color4"] == null ? null : json["color4"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "color1": color1 == null ? null : color1,
        "color2": color2 == null ? null : color2,
        "color3": color3 == null ? null : color3,
        "color4": color4 == null ? null : color4,
      };
}

class Vendor {
  Vendor({
    this.name,
    this.email,
    this.mobile,
  });

  String name;
  String email;
  String mobile;

  factory Vendor.fromJson(Map<String, dynamic> json) => Vendor(
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        mobile: json["mobile"] == null ? null : json["mobile"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "email": email == null ? null : email,
        "mobile": mobile == null ? null : mobile,
      };
}

class StoreListModel {
  StoreListModel({
    this.hashId,
    this.businessType,
    this.name,
    this.totalLike,
    this.totalFollowing,
    this.timestamp,
    this.profilePhotoFull,
    this.profilePhotoThumbnail,
    this.coverPhotoFull,
    this.coverPhotoThumbnail,
  });

  String hashId;
  String businessType;
  String name;
  int totalLike;
  int totalFollowing;
  String timestamp;
  String profilePhotoFull;
  String profilePhotoThumbnail;
  String coverPhotoFull;
  String coverPhotoThumbnail;

  factory StoreListModel.fromJson(Map<String, dynamic> json) => StoreListModel(
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        businessType:
            json["business_type"] == null ? null : json["business_type"],
        name: json["name"] == null ? null : json["name"],
        totalLike: json["total_like"] == null ? null : json["total_like"],
        totalFollowing:
            json["total_following"] == null ? null : json["total_following"],
        timestamp: json["timestamp"] == null ? null : json["timestamp"],
        profilePhotoFull: json["profile_photo_full"] == null
            ? null
            : json["profile_photo_full"],
        profilePhotoThumbnail: json["profile_photo_thumbnail"] == null
            ? null
            : json["profile_photo_thumbnail"],
        coverPhotoFull:
            json["cover_photo_full"] == null ? null : json["cover_photo_full"],
        coverPhotoThumbnail: json["cover_photo_thumbnail"] == null
            ? null
            : json["cover_photo_thumbnail"],
      );

  Map<String, dynamic> toJson() => {
        "hash_id": hashId == null ? null : hashId,
        "business_type": businessType == null ? null : businessType,
        "name": name == null ? null : name,
        "total_like": totalLike == null ? null : totalLike,
        "total_following": totalFollowing == null ? null : totalFollowing,
        "timestamp": timestamp == null ? null : timestamp,
        "profile_photo_full":
            profilePhotoFull == null ? null : profilePhotoFull,
        "profile_photo_thumbnail":
            profilePhotoThumbnail == null ? null : profilePhotoThumbnail,
        "cover_photo_full": coverPhotoFull == null ? null : coverPhotoFull,
        "cover_photo_thumbnail":
            coverPhotoThumbnail == null ? null : coverPhotoThumbnail,
      };
}
