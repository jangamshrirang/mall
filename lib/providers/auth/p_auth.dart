import 'package:auto_route/auto_route.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:wblue_customer/models/auth.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/models/validation.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/widgets/w_loading.dart';

class PAuth with ChangeNotifier {
  bool isNumberValid = false;
  bool _isLoading = false;

  String _phoneNumber;
  String _name;
  String _code;
  String _method;
  String _verification;
  AuthCredential _phoneAuthCredential;
  UserCredential _firebaseUser;
  AuthModel authApi = AuthModel();

  ValidationItem number = ValidationItem(null, null);
  ValidationItem fullname = ValidationItem(null, null);
  ValidationItem smsCode = ValidationItem(null, null);

  bool get isLoading => _isLoading;
  String get method => _method;
  String get name => _name;
  String get phoneNumber => _phoneNumber;
  String get verification => _verification;
  String get code => _code;
  AuthCredential get phoneAuthCredential => _phoneAuthCredential;
  UserCredential get firebaseUser => _firebaseUser;

  void setLoading(bool value) {
    print('===== ${value}');
    _isLoading = value;
    notifyListeners();
  }

  void setNumberValid(bool valid, {String msg: 'Invalid phone number'}) {
    if (valid) {
      number = ValidationItem(null, null);
      isNumberValid = valid;
    } else {
      number = ValidationItem(null, msg);
      isNumberValid = valid;
    }

    notifyListeners();
  }

  void setFullname(String name,
      {String msg: 'Full Name at least 6 characters'}) {
    if (name.length > 5 || name == '') {
      fullname = ValidationItem(name, null);
    } else {
      fullname = ValidationItem(name, msg);
    }

    notifyListeners();
  }

  void setPhoneNumber(String mobile) {
    _phoneNumber = mobile;
  }

  void setName(String name) {
    _name = name;
  }

  void setVerificationId(String id) {
    _verification = id;
  }

  void setCode(String code) {
    _code = code;
  }

  void setPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) {
    _phoneAuthCredential = phoneAuthCredential;
  }

  void setFirebaseUser(UserCredential firebaseUser) {
    _firebaseUser = firebaseUser;
  }

  void setMethod(String method) {
    _method = method;
  }

  void setSmsCode(String error) {
    smsCode = ValidationItem(null, error);
    notifyListeners();
  }

  Future<void> submitPhoneNumber({String type: 'sign-in'}) async {
    setMethod(type);

    /// NOTE: Either append your phone number country code or add in the code itself
    /// Since I'm in India we use "+91 " as prefix `phoneNumber`

    /// The below functions are the callbacks, separated so as to make code more readable
    void verificationCompleted(AuthCredential phoneAuthCredential) {
      print('verificationCompleted');

      setPhoneAuthCredential(phoneAuthCredential);
    }

    void verificationFailed(FirebaseAuthException error) {
      setLoading(false);
      setNumberValid(false, msg: '${error.message}');
    }

    void codeSent(String verificationId, [int code]) {
      setVerificationId(verificationId);

      if (type == 'sign-up') {
        ExtendedNavigator.root.replace('/otp').then((value) {
          ExtendedNavigator.root.pop(value);
        });
      } else {
        ExtendedNavigator.root.push('/otp').then((value) {
          ExtendedNavigator.root.pop(value);
        });
      }
    }

    void codeAutoRetrievalTimeout(String verificationId) {
      print('codeAutoRetrievalTimeout');
    }

    String code = Helper.state[StatesNames.selectedCountry]['dialCode'];

    await FirebaseAuth.instance.verifyPhoneNumber(
      /// Make sure to prefix with your country code
      phoneNumber: '$code$phoneNumber',

      /// `seconds` didn't work. The underlying implementation code only reads in `milliseconds`
      timeout: Duration(milliseconds: 10000),

      /// If the SIM (with phoneNumber) is in the current device this function is called.
      /// This function gives `AuthCredential`. Moreover `login` function can be called from this callback
      verificationCompleted: verificationCompleted,

      /// Called when the verification is failed
      verificationFailed: verificationFailed,

      /// This is called after the OTP is sent. Gives a `verificationId` and `code`
      codeSent: codeSent,

      /// After automatic code retrival `tmeout` this function is called
      codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
    ); // All the callbacks are above
  }

  void submitOTP() {
    if (isLoading) {
      return;
    }
    setLoading(true);

    /// get the `smsCode` from the user

    /// when used different phoneNumber other than the current (running) device
    /// we need to use OTP to get `phoneAuthCredential` which is inturn used to signIn/login
    ///
    ///
    try {
      final __phoneAuthCredential = PhoneAuthProvider.credential(
          verificationId: verification, smsCode: code);
      setPhoneAuthCredential(__phoneAuthCredential);
      login();
    } catch (error) {
      setLoading(false);
      BotToast.showText(text: 'Something wrong, Please try again!');
    }
  }

  Future<void> logout() async {
    /// Method to Logout the `FirebaseUser` (`_firebaseUser`)
    try {
      await FirebaseAuth.instance.signOut();
      setFirebaseUser(null);
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> login() async {
    /// This method is used to login the user
    /// `AuthCredential`(`_phoneAuthCredential`) is needed for the signIn method
    /// After the signIn method from `AuthResult` we can get `FirebaserUser`(`_firebaseUser`)

    try {
      await FirebaseAuth.instance
          .signInWithCredential(phoneAuthCredential)
          .then((UserCredential authRes) async {
        setFirebaseUser(authRes);
        if (method == 'sign-up') {
          final isRegSuccess =
              await authApi.registration(fullname: name, mobile: phoneNumber);
          if (!isRegSuccess)
            setNumberValid(false, msg: 'Verification error, Please try Again!');
        }

        if (firebaseUser != null) {
          final res = await authApi.loginMobile(phoneNumber, withVerify: true);
          // Helper.userCurrent.put('isLogin', res);
          if (res) {
            Global.user = UserModel.createDummy();
            setLoading(false);
            ExtendedNavigator.root.pop(true);
          }
        }
      });
    } catch (e) {
      WLoading.type(Loading.dismiss);
      handleError(e);
    }
  }

  handleError(FirebaseAuthException error) {
    setLoading(false);
    switch (error.code) {
      case 'invalid-verification-code':
        setSmsCode('Invalid OTP Code');
        break;
      case 'session-expired':
        BotToast.showText(text: 'Session expired, Please try again');
        ExtendedNavigator.root.pop();
        break;
      default:
        ExtendedNavigator.root.pop();
        break;
    }
  }
}
