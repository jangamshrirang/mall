import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WCardWidget extends StatelessWidget {
  final Widget child;
  final BorderRadiusGeometry borderRadius;
  final Color color;
  final EdgeInsetsGeometry padding;

  WCardWidget({
    this.child,
    this.borderRadius,
    this.color: Colors.white,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5.0),
      child: Container(
        margin: EdgeInsets.only(bottom: 4.0),
        padding: padding,
        decoration: BoxDecoration(
          borderRadius: borderRadius,
          color: color,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 1.0),
              blurRadius: 4.0,
            ),
          ],
        ),
        child: child,
      ),
    );
  }
}
