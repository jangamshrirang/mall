// import 'dart:async';
// import 'dart:io';
// import 'package:agora_rtm/agora_rtm.dart';
// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// //import 'package:fluttertoast/fluttertoast.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:permission_handler/permission_handler.dart';
// import 'package:wakelock/wakelock.dart';
// import 'package:wblue_customer/firebaseDb/firestoreDB.dart';
// import 'package:wblue_customer/models/live/message.dart';
// import 'package:wblue_customer/models/live/user.dart';
// import 'dart:math' as math;

// import '../../../../../env/config.example.dart';
// import '../HearAnim.dart';

// class CallPage extends StatefulWidget {
//   /// non-modifiable channel name of the page
//   final String channelName;

//   final String name;
//   final String image;
//   final String contentTitle;
//   final String contentImage;

//   final time;

//   /// Creates a call page with given channel name.
//   const CallPage(
//       {Key key,
//       this.channelName,
//       this.time,
//       this.name,
//       this.image,
//       this.contentTitle,
//       this.contentImage})
//       : super(key: key);

//   @override
//   _CallPageState createState() => _CallPageState();
// }

// class _CallPageState extends State<CallPage> {
//   static final _users = <int>[];
//   String channelName;
//   List<User> userList = [];

//   bool _isLogin = true;
//   bool _isInChannel = true;
//   int userNo = 0;
//   var userMap;
//   var tryingToEnd = false;

//   final _channelMessageController = TextEditingController();

//   final _infoStrings = <Message>[];

//   AgoraRtmClient _client;
//   AgoraRtmChannel _channel;
//   bool heart = false;
//   bool anyPerson = false;

//   //Love animation
//   final _random = math.Random();
//   Timer _timer;
//   double height = 0.0;
//   int _numConfetti = 5;
//   int guestID = -1;
//   bool waiting = false;

//   @override
//   void initState() {
//     super.initState();
//     // initialize agora sdk
//     initialize();
//     userMap = {widget.channelName: widget.image};
//     _createClient();
//   }

//   // Platform messages are asynchronous, so we initialize in an async method.
//   Future<void> initPlatformState() async {
//     await [Permission.camera, Permission.microphone, Permission.storage]
//         .request();

//     var engine = await RtcEngine.create(YOUR_APP_ID);
//     engine.setEventHandler(RtcEngineEventHandler(
//         joinChannelSuccess: (String channel, int uid, int elapsed) {
//       print('joinChannelSuccess ${channel} ${uid}');
//       setState(() {
//         _joined = true;
//       });
//     }, userJoined: (int uid, int elapsed) {
//       print('userJoined ${uid}');
//       setState(() {
//         _remoteUid = uid;
//       });
//     }, userOffline: (int uid, UserOfflineReason reason) {
//       print('userOffline ${uid}');
//       setState(() {
//         _remoteUid = null;
//       });
//     }));
//     await engine.enableVideo();
//     await engine.joinChannel(null, '123', null, 0);
//   }

//   Future<void> initialize() async {
//     await _initAgoraRtcEngine();
//     _addAgoraEventHandlers();
//     await AgoraRtcEngine.enableWebSdkInteroperability(true);
//     await AgoraRtcEngine.setParameters(
//         '''{\"che.video.lowBitRateStreamParameter\":{\"width\":320,\"height\":180,\"frameRate\":15,\"bitRate\":140}}''');
//     await AgoraRtcEngine.joinChannel(null, widget.channelName, null, 0);
//   }

//   /// Create agora sdk instance and initialize
//   Future<void> _initAgoraRtcEngine() async {
//     await AgoraRtcEngine.create(Config.APP_ID);
//     await AgoraRtcEngine.enableVideo();
//   }

//   /// Add agora event handlers
//   void _addAgoraEventHandlers() {
//     AgoraRtcEngine.onJoinChannelSuccess = (
//       String channel,
//       int uid,
//       int elapsed,
//     ) async {
//       final documentId = widget.channelName;
//       channelName = documentId;

//       FireStoreClass.createLiveUser(
//           id: uid,
//           time: widget.time,
//           image: widget.image,
//           name: widget.name,
//           contentTitle: widget.contentTitle,
//           contentImage: widget.contentImage,
//           viewer: 0);

//       // The above line create a document in the firestore with username as documentID
//       await Wakelock.enable();
//       // This is used for Keeping the device awake. Its now enabled
//     };

//     AgoraRtcEngine.onLeaveChannel = () {
//       setState(() {
//         _users.clear();
//       });
//     };

//     AgoraRtcEngine.onUserJoined = (int uid, int elapsed) {
//       setState(() {
//         _users.add(uid);
//       });
//     };

//     AgoraRtcEngine.onUserOffline = (int uid, int reason) async {
//       setState(() {
//         _users.remove(uid);
//         userNo = _users.length;
//       });
//       FireStoreClass.updateViewer(username: widget.channelName, viewer: userNo);
//     };
//   }

//   /// Helper function to get list of native views
//   List<Widget> _getRenderViews() {
//     final list = [
//       AgoraRenderWidget(0, local: true, preview: true),
//     ];

//     return list;
//   }

//   /// Video view wrapper
//   Widget _videoView(view) {
//     return Expanded(child: ClipRRect(child: view));
//   }

//   /// Video view row wrapper
//   Widget _expandedVideoRow(List<Widget> views) {
//     final wrappedViews = views.map<Widget>(_videoView).toList();
//     return Expanded(
//       child: Row(
//         children: wrappedViews,
//       ),
//     );
//   }

//   /// Video layout wrapper
//   Widget _viewRows() {
//     final views = _getRenderViews();

//     return Container(
//         child: Column(
//       children: <Widget>[_videoView(views[0])],
//     ));
//   }

//   void popUp() async {
//     setState(() {
//       heart = true;
//     });

//     _timer = Timer.periodic(Duration(milliseconds: 125), (Timer t) {
//       setState(() {
//         height += _random.nextInt(20);
//       });
//     });

//     Timer(
//         Duration(seconds: 4),
//         () => {
//               _timer.cancel(),
//               setState(() {
//                 heart = false;
//               })
//             });
//   }

//   Widget heartPop() {
//     final size = MediaQuery.of(context).size;
//     final confetti = <Widget>[];
//     for (var i = 0; i < _numConfetti; i++) {
//       final height = _random.nextInt(size.height.floor());
//       final width = 20;
//       confetti.add(HeartAnim(
//         height % 200.0,
//         width.toDouble(),
//         0.5,
//       ));
//     }

//     return Container(
//       child: Padding(
//         padding: const EdgeInsets.only(bottom: 20),
//         child: Align(
//           alignment: Alignment.bottomRight,
//           child: Container(
//             height: 400,
//             width: 200,
//             child: Stack(
//               children: confetti,
//             ),
//           ),
//         ),
//       ),
//     );
//   }

//   /// Info panel to show logs
//   Widget messageList() {
//     return Container(
//       padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
//       alignment: Alignment.bottomCenter,
//       child: FractionallySizedBox(
//         heightFactor: 0.5,
//         child: Container(
//           padding: const EdgeInsets.symmetric(vertical: 48),
//           child: ListView.builder(
//             reverse: true,
//             itemCount: _infoStrings.length,
//             itemBuilder: (BuildContext context, int index) {
//               if (_infoStrings.isEmpty) {
//                 return null;
//               }
//               return Padding(
//                 padding: const EdgeInsets.symmetric(
//                   vertical: 3,
//                   horizontal: 10,
//                 ),
//                 child: (_infoStrings[index].type == 'join')
//                     ? Padding(
//                         padding: const EdgeInsets.only(bottom: 10),
//                         child: Row(
//                           mainAxisSize: MainAxisSize.max,
//                           mainAxisAlignment: MainAxisAlignment.start,
//                           children: <Widget>[
//                             CachedNetworkImage(
//                               imageUrl: _infoStrings[index].image,
//                               imageBuilder: (context, imageProvider) =>
//                                   Container(
//                                 width: 32.0,
//                                 height: 32.0,
//                                 decoration: BoxDecoration(
//                                   shape: BoxShape.circle,
//                                   image: DecorationImage(
//                                       image: imageProvider, fit: BoxFit.cover),
//                                 ),
//                               ),
//                             ),
//                             Padding(
//                               padding: const EdgeInsets.symmetric(
//                                 horizontal: 8,
//                               ),
//                               child: Text(
//                                 '${_infoStrings[index].user} joined',
//                                 style: TextStyle(
//                                   color: Colors.white,
//                                   fontSize: 14,
//                                 ),
//                               ),
//                             ),
//                           ],
//                         ),
//                       )
//                     : (_infoStrings[index].type == 'message')
//                         ? Padding(
//                             padding: const EdgeInsets.only(bottom: 10),
//                             child: Row(
//                               mainAxisSize: MainAxisSize.max,
//                               mainAxisAlignment: MainAxisAlignment.start,
//                               children: <Widget>[
//                                 CachedNetworkImage(
//                                   imageUrl: _infoStrings[index].image,
//                                   imageBuilder: (context, imageProvider) =>
//                                       Container(
//                                     width: 32.0,
//                                     height: 32.0,
//                                     decoration: BoxDecoration(
//                                       shape: BoxShape.circle,
//                                       image: DecorationImage(
//                                           image: imageProvider,
//                                           fit: BoxFit.cover),
//                                     ),
//                                   ),
//                                 ),
//                                 Column(
//                                   crossAxisAlignment: CrossAxisAlignment.start,
//                                   children: <Widget>[
//                                     Padding(
//                                       padding: const EdgeInsets.symmetric(
//                                         horizontal: 8,
//                                       ),
//                                       child: Text(
//                                         _infoStrings[index].user,
//                                         style: TextStyle(
//                                             color: Colors.white,
//                                             fontSize: 14,
//                                             fontWeight: FontWeight.bold),
//                                       ),
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Padding(
//                                       padding: const EdgeInsets.symmetric(
//                                         horizontal: 8,
//                                       ),
//                                       child: Text(
//                                         _infoStrings[index].message,
//                                         style: TextStyle(
//                                             color: Colors.white, fontSize: 14),
//                                       ),
//                                     ),
//                                   ],
//                                 )
//                               ],
//                             ),
//                           )
//                         : null,
//               );
//             },
//           ),
//         ),
//       ),
//     );
//   }

//   void _onSwitchCamera() {
//     AgoraRtcEngine.switchCamera();
//   }

//   Future<bool> _willPopCallback() async {
//     setState(() {
//       tryingToEnd = !tryingToEnd;
//     });

//     return false; // return true if the route to be popped
//   }

//   Widget _endCall() {
//     return Container(
//       width: MediaQuery.of(context).size.width,
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.end,
//         children: <Widget>[
//           Padding(
//             padding: const EdgeInsets.fromLTRB(15, 15, 15, 15),
//             child: GestureDetector(
//               onTap: () {
//                 setState(() {
//                   if (waiting == true) {
//                     waiting = false;
//                   }
//                   tryingToEnd = true;
//                 });
//               },
//               child: Text(
//                 'END',
//                 style: TextStyle(
//                     color: Colors.indigo,
//                     fontSize: 20,
//                     fontWeight: FontWeight.bold),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   Widget _liveText() {
//     return Padding(
//       padding: const EdgeInsets.all(15.0),
//       child: Container(
//         child: Row(
//           mainAxisSize: MainAxisSize.min,
//           mainAxisAlignment: MainAxisAlignment.start,
//           children: <Widget>[
//             Container(
//               decoration: BoxDecoration(
//                   color: Colors.red,
//                   borderRadius: BorderRadius.all(Radius.circular(4.0))),
//               child: Padding(
//                 padding:
//                     const EdgeInsets.symmetric(vertical: 5.0, horizontal: 8.0),
//                 child: Text(
//                   'LIVE',
//                   style: TextStyle(
//                       color: Colors.white,
//                       fontSize: 15,
//                       fontWeight: FontWeight.bold),
//                 ),
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.only(left: 5, right: 10),
//               child: Container(
//                   decoration: BoxDecoration(
//                       color: Colors.black.withOpacity(.6),
//                       borderRadius: BorderRadius.all(Radius.circular(4.0))),
//                   height: 28,
//                   alignment: Alignment.center,
//                   child: Padding(
//                     padding: const EdgeInsets.symmetric(horizontal: 8.0),
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: <Widget>[
//                         Icon(
//                           FontAwesomeIcons.eye,
//                           color: Colors.white,
//                           size: 13,
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Text(
//                           '$userNo',
//                           style: TextStyle(color: Colors.white, fontSize: 11),
//                         ),
//                       ],
//                     ),
//                   )),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   Widget endLive() {
//     return Container(
//       color: Colors.black.withOpacity(0.5),
//       child: Stack(
//         children: <Widget>[
//           Align(
//             alignment: Alignment.center,
//             child: Padding(
//               padding: const EdgeInsets.all(30.0),
//               child: Text(
//                 'Are you sure you want to end your live video?',
//                 textAlign: TextAlign.center,
//                 style: TextStyle(color: Colors.white, fontSize: 20),
//               ),
//             ),
//           ),
//           Container(
//             alignment: Alignment.bottomCenter,
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: <Widget>[
//                 Expanded(
//                   child: Padding(
//                     padding: const EdgeInsets.only(
//                         left: 8.0, right: 4.0, top: 8.0, bottom: 8.0),
//                     child: RaisedButton(
//                       child: Padding(
//                         padding: const EdgeInsets.symmetric(vertical: 15),
//                         child: Text(
//                           'End Video',
//                           style: TextStyle(color: Colors.white),
//                         ),
//                       ),
//                       elevation: 2.0,
//                       color: Colors.blue,
//                       onPressed: () async {
//                         await Wakelock.disable();
//                         _logout();
//                         _leaveChannel();
//                         AgoraRtcEngine.leaveChannel();
//                         AgoraRtcEngine.destroy();
//                         FireStoreClass.deleteUser(username: channelName);
//                         Navigator.pop(context);
//                       },
//                     ),
//                   ),
//                 ),
//                 Expanded(
//                   child: Padding(
//                     padding: const EdgeInsets.only(
//                         left: 4.0, right: 8.0, top: 8.0, bottom: 8.0),
//                     child: RaisedButton(
//                       child: Padding(
//                         padding: const EdgeInsets.symmetric(vertical: 15),
//                         child: Text(
//                           'Cancel',
//                           style: TextStyle(color: Colors.white),
//                         ),
//                       ),
//                       elevation: 2.0,
//                       color: Colors.grey,
//                       onPressed: () {
//                         setState(() {
//                           tryingToEnd = false;
//                         });
//                       },
//                     ),
//                   ),
//                 )
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }

//   List<Widget> getUserStories() {
//     List<Widget> stories = [];
//     for (User users in userList) {
//       stories.add(getStory(users));
//     }
//     return stories;
//   }

//   Widget getStory(User users) {
//     return Container(
//       margin: EdgeInsets.symmetric(vertical: 7.5),
//       child: Column(
//         children: <Widget>[
//           GestureDetector(
//             onTap: () async {
//               setState(() {
//                 waiting = true;
//               });
//               await _channel.sendMessage(
//                   AgoraRtmMessage.fromText('d1a2v3i4s5h6 ${users.username}'));
//             },
//             child: Container(
//                 padding: EdgeInsets.only(left: 15),
//                 color: Colors.grey[850],
//                 child: Row(
//                   children: <Widget>[
//                     CachedNetworkImage(
//                       imageUrl: users.image,
//                       imageBuilder: (context, imageProvider) => Container(
//                         width: 40.0,
//                         height: 40.0,
//                         decoration: BoxDecoration(
//                           shape: BoxShape.circle,
//                           image: DecorationImage(
//                               image: imageProvider, fit: BoxFit.cover),
//                         ),
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.only(left: 10),
//                       child: Column(
//                         children: <Widget>[
//                           Text(
//                             users.username,
//                             style: TextStyle(fontSize: 18, color: Colors.white),
//                           ),
//                           SizedBox(
//                             height: 2,
//                           ),
//                           Text(
//                             users.name,
//                             style: TextStyle(color: Colors.grey),
//                           ),
//                         ],
//                       ),
//                     )
//                   ],
//                 )),
//           ),
//         ],
//       ),
//     );
//   }

//   Widget guestWaiting() {
//     return Container(
//       alignment: Alignment.bottomRight,
//       child: Container(
//           height: 100,
//           width: double.maxFinite,
//           alignment: Alignment.center,
//           color: Colors.black,
//           child: Wrap(
//             children: <Widget>[
//               Text(
//                 'Waiting for the user to accept...',
//                 style: TextStyle(color: Colors.white, fontSize: 20),
//               )
//             ],
//           )),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//         child: SafeArea(
//           child: Scaffold(
//             body: Container(
//               color: Colors.black,
//               child: Center(
//                 child: Stack(
//                   children: <Widget>[
//                     _viewRows(), // Video Widget
//                     if (tryingToEnd == false) _endCall(),
//                     if (tryingToEnd == false) _liveText(),
//                     if (heart == true && tryingToEnd == false) heartPop(),
//                     if (tryingToEnd == false) _bottomBar(), // send message
//                     if (tryingToEnd == false) messageList(),
//                     if (tryingToEnd == true) endLive(), // view message
//                     if (waiting == true) guestWaiting(),
//                   ],
//                 ),
//               ),
//             ),
//           ),
//         ),
//         onWillPop: _willPopCallback);
//   }
// // Agora RTM

//   Widget _bottomBar() {
//     if (!_isLogin || !_isInChannel) {
//       return Container();
//     }
//     return Container(
//       alignment: Alignment.bottomRight,
//       child: Container(
//         color: Colors.black,
//         child: Padding(
//           padding: const EdgeInsets.only(left: 8, top: 5, right: 8, bottom: 5),
//           child:
//               Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
//             new Expanded(
//                 child: Padding(
//               padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 0),
//               child: new TextField(
//                   cursorColor: Colors.blue,
//                   textInputAction: TextInputAction.send,
//                   onSubmitted: _sendMessage,
//                   style: TextStyle(color: Colors.white),
//                   controller: _channelMessageController,
//                   textCapitalization: TextCapitalization.sentences,
//                   decoration: InputDecoration(
//                     isDense: true,
//                     hintText: 'Comment',
//                     hintStyle: TextStyle(color: Colors.white),
//                     enabledBorder: OutlineInputBorder(
//                         borderRadius: BorderRadius.circular(30.0),
//                         borderSide: BorderSide(color: Colors.white)),
//                     focusedBorder: OutlineInputBorder(
//                         borderRadius: BorderRadius.circular(30.0),
//                         borderSide: BorderSide(color: Colors.white)),
//                   )),
//             )),
//             Padding(
//               padding: const EdgeInsets.fromLTRB(4.0, 0, 0, 0),
//               child: MaterialButton(
//                 minWidth: 0,
//                 onPressed: _toggleSendChannelMessage,
//                 child: Icon(
//                   Icons.send,
//                   color: Colors.white,
//                   size: 20.0,
//                 ),
//                 shape: CircleBorder(),
//                 elevation: 2.0,
//                 color: Colors.blue[400],
//                 padding: const EdgeInsets.all(12.0),
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.fromLTRB(4.0, 0, 0, 0),
//               child: MaterialButton(
//                 minWidth: 0,
//                 onPressed: _onSwitchCamera,
//                 child: Icon(
//                   Icons.switch_camera,
//                   color: Colors.blue[400],
//                   size: 20.0,
//                 ),
//                 shape: CircleBorder(),
//                 elevation: 2.0,
//                 color: Colors.white,
//                 padding: const EdgeInsets.all(12.0),
//               ),
//             )
//           ]),
//         ),
//       ),
//     );
//   }

//   void _logout() async {
//     try {
//       await _client.logout();
//       //_log(info:'Logout success.',type: 'logout');
//     } catch (errorCode) {
//       //_log(info: 'Logout error: ' + errorCode.toString(), type: 'error');
//     }
//   }

//   void _leaveChannel() async {
//     try {
//       await _channel.leave();
//       //_log(info: 'Leave channel success.',type: 'leave');
//       _client.releaseChannel(_channel.channelId);
//       _channelMessageController.text = null;
//     } catch (errorCode) {
//       // _log(info: 'Leave channel error: ' + errorCode.toString(),type: 'error');
//     }
//   }

//   void _toggleSendChannelMessage() async {
//     String text = _channelMessageController.text;
//     if (text.isEmpty) {
//       return;
//     }
//     try {
//       _channelMessageController.clear();
//       await _channel.sendMessage(AgoraRtmMessage.fromText(text));
//       _log(user: widget.channelName, info: text, type: 'message');
//     } catch (errorCode) {
//       //_log(info: 'Send channel message error: ' + errorCode.toString(), type: 'error');
//     }
//   }

//   void _sendMessage(text) async {
//     if (text.isEmpty) {
//       return;
//     }
//     try {
//       _channelMessageController.clear();
//       await _channel.sendMessage(AgoraRtmMessage.fromText(text));
//       _log(user: widget.channelName, info: text, type: 'message');
//     } catch (errorCode) {
//       // _log('Send channel message error: ' + errorCode.toString());
//     }
//   }

//   void _createClient() async {
//     _client =
//         await AgoraRtmClient.createInstance('b42ce8d86225475c9558e478f1ed4e8e');
//     _client.onMessageReceived = (AgoraRtmMessage message, String peerId) {
//       _log(user: peerId, info: message.text, type: 'message');
//     };
//     _client.onConnectionStateChanged = (int state, int reason) {
//       if (state == 5) {
//         _client.logout();
//         //_log('Logout.');
//         setState(() {
//           _isLogin = false;
//         });
//       }
//     };

//     await _client.login(null, widget.channelName);
//     _channel = await _createChannel(widget.channelName);
//     await _channel.join();
//   }

//   Future<AgoraRtmChannel> _createChannel(String name) async {
//     AgoraRtmChannel channel = await _client.createChannel(name);
//     channel.onMemberJoined = (AgoraRtmMember member) async {
//       var img = await FireStoreClass.getImage(username: member.userId);
//       var nm = await FireStoreClass.getName(username: member.userId);
//       setState(() {
//         userList.add(new User(username: member.userId, name: nm, image: img));
//         if (userList.length > 0) anyPerson = true;
//       });
//       userMap.putIfAbsent(member.userId, () => img);
//       var len;
//       _channel.getMembers().then((value) {
//         print('value 1 :$value');

//         len = value.length;
//         print('len= 1:$len');

//         setState(() {
//           userNo = len - 1;
//         });

//         print('userNo= :$userNo');
//         FireStoreClass.updateViewer(
//             username: widget.channelName, viewer: userNo);
//       });

//       _log(info: 'Member joined: ', user: member.userId, type: 'join');
//     };

//     channel.onMemberLeft = (AgoraRtmMember member) {
//       var len;

//       setState(() {
//         userList.removeWhere((element) => element.username == member.userId);
//         if (userList.length == 0) anyPerson = false;
//       });

//       _channel.getMembers().then((value) {
//         print('value2 :$value');

//         len = value.length;

//         setState(() {
//           userNo = len - 1;
//         });

//         print('userNo= :$userNo');
//         FireStoreClass.updateViewer(
//             username: widget.channelName, viewer: userNo);
//       });

//       print('onMemberLeft');
//     };

//     channel.onMessageReceived =
//         (AgoraRtmMessage message, AgoraRtmMember member) {
//       print('--onMessageReceived');

//       _log(user: member.userId, info: message.text, type: 'message');
//     };
//     return channel;
//   }

//   void _log({String info, String type, String user}) {
//     if (type == 'message' && info.contains('m1x2y3z4p5t6l7k8')) {
//       popUp();
//     } else if (type == 'message' && info.contains('k1r2i3s4t5i6e7')) {
//       setState(() {
//         waiting = false;
//       });
//     } else if (type == 'message' && info.contains('R1e2j3e4c5t6i7o8n9e0d')) {
//       setState(() {
//         waiting = false;
//       });
//       /*FlutterToast.showToast(
//           msg: "Guest Declined",
//           toastLength: Toast.LENGTH_LONG,
//           gravity: ToastGravity.TOP,
//           timeInSecForIosWeb: 1,
//           backgroundColor: Colors.black,
//           textColor: Colors.white,
//           fontSize: 16.0
//       );*/

//     } else {
//       var image = userMap[user];
//       Message m =
//           new Message(message: info, type: type, user: user, image: image);
//       setState(() {
//         _infoStrings.insert(0, m);
//       });
//     }
//   }
// }
