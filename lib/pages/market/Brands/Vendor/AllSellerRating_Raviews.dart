import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

class AllSellerRatingRevies extends StatefulWidget {
  List sellereRevies;
  AllSellerRatingRevies({this.sellereRevies});
  int colorVal = 0xff84020e;
  @override
  _AllSellerRatingReviesState createState() => _AllSellerRatingReviesState();
}

class _AllSellerRatingReviesState extends State<AllSellerRatingRevies>
    with TickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 3, initialIndex: 2);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {
      widget.colorVal = 0xff84020e;
    });
  }

  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "Seller Ratings & Reviews"),
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: DefaultTabController(
          length: 3,
          child: Column(
            children: <Widget>[
              ratingsBloc(),
              TabBar(
                controller: _tabController,
                indicatorSize: TabBarIndicatorSize.tab,
                indicatorColor: Colors.transparent,
                unselectedLabelColor: Colors.grey,
                tabs: <Widget>[
                  Tab(
                      child: customTab(0, "assets/images/sad.png",
                          "https://banner2.cleanpng.com/20180401/wye/kisspng-face-sadness-smiley-computer-icons-clip-art-sad-5ac17cc2a4a668.6423909115226298266744.jpg")),
                  Tab(
                      child: customTab(1, "assets/images/neutral.png",
                          "https://icon-library.com/images/icon-smile/icon-smile-17.jpg")),
                  Tab(
                      child: customTab(2, "assets/images/happyEmoji.png",
                          "https://i7.uihere.com/icons/573/826/466/smiling-face-bdf876b7941485bf5062a27f5f4c3d2a.png")),
                ],
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    sellerRatingtiles(
                        widget.sellereRevies, "assets/images/sad.png"),
                    sellerRatingtiles(
                        widget.sellereRevies, "assets/images/neutral.png"),
                    sellerRatingtiles(
                        widget.sellereRevies, "assets/images/happyEmoji.png"),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  customTab(int number, String image1, image2) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.04,
      decoration: BoxDecoration(
          color: _tabController.index == number
              ? Colors.red[50]
              : Colors.grey[100],
          borderRadius: BorderRadius.circular(3),
          border: Border.all(color: Colors.transparent, width: 0.5)),
      child: Align(
          alignment: Alignment.center,
          child: _tabController.index == number
              ? Image.asset(
                  image1,
                  height: 20,
                )
              : Image.network(
                  image2,
                  height: 20,
                )),
    );
  }

  ratingsBloc() {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: 10),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              textContainer2(
                  "88%",
                  Colors.black,
                  FontWeight.normal,
                  size.height * 0.05,
                  size.width * 0.0,
                  size.height * 0.04,
                  TextAlign.center),
              textContainer2(
                  "Positive Seller Ratings",
                  Colors.black,
                  FontWeight.normal,
                  size.height * 0.023,
                  size.width * 0.0,
                  size.height * 0.022,
                  TextAlign.center),
              textContainer2(
                  "7491 Customer Reviews",
                  Colors.black54,
                  FontWeight.normal,
                  size.height * 0.018,
                  size.width * 0.0,
                  size.height * 0.018,
                  TextAlign.center),
            ],
          ),
          Container(height: 60, child: VerticalDivider(color: Colors.black45)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ratingBarWithText("Positive", 0.8, 999),
              ratingBarWithText("Netural", 0.1, 549),
              ratingBarWithText("Negative", 0.1, 555),
            ],
          ),
        ],
      ),
    );
  }

  textContainer2(
      String text,
      Color color,
      FontWeight fontWeight,
      double fontsize,
      double margin,
      double containerHeight,
      TextAlign textAlign) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.45,
      height: containerHeight,
      //color: Colors.yellow,
      margin: EdgeInsets.only(left: margin, top: 5),
      child: Text(
        text,
        textAlign: textAlign,
        style:
            TextStyle(color: color, fontWeight: fontWeight, fontSize: fontsize),
      ),
    );
  }

  ratingBarWithText(String text, double value, int count) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      child: Row(
        children: [
          Container(
            width: size.width * 0.12,
            child: Text(
              text,
              style: TextStyle(color: Colors.black45, fontSize: 12),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(0.0),
            child: new LinearPercentIndicator(
              width: size.width * 0.3,
              animation: true,
              animationDuration: 3000,
              lineHeight: 8.0,
              //leading: new Text(""),
              // trailing: new Text(""),
              percent: value,
              center: Text(""),
              linearStrokeCap: LinearStrokeCap.butt,
              progressColor: Colors.amber,
            ),
          ),
          Text(
            "$count",
            style: TextStyle(color: Colors.black45, fontSize: 10),
          ),
        ],
      ),
    );
  }

  sellerRatingtiles(List hours, String image) {
    return Wrap(
        children: List.generate(hours.length, (index) {
      return Column(
        children: [
          ListTile(
            // leading: Image.network(image, height: 30, color: Colors.amber),
            title: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Wmall Customer - " + "${hours[index]}" + " hours ago",
                      style: TextStyle(color: Colors.black45, fontSize: 15),
                    ),
                    SizedBox(height: 5),
                    Text(
                      "This Seller didn't receive any detail review",
                      style: TextStyle(color: Colors.black, fontSize: 16),
                    ),
                  ],
                ),
                Spacer(),
                Column(
                  children: [
                    Image.asset(image, height: 20),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Icon(
                          Icons.thumb_up,
                          size: 12,
                          color: Colors.black26,
                        ),
                        Text(
                          "  2",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black26,
                          ),
                        )
                      ],
                    ),
                  ],
                )
              ],
            ),
            onTap: () {},
          ),
          Divider()
        ],
      );
    }));
  }
}
