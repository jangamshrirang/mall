import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/user_office_address.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/providers/restaurant/checkout/p_user_address.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/services/helper.dart';

class RestaurantCheckoutAddressPage extends StatefulWidget {
  // ADD BELOW THE OTHER MODEL FOR APARTMENT AND HOUSE
  final UserAddressModel userAddressModel;
  RestaurantCheckoutAddressPage({this.userAddressModel});

  @override
  _RestaurantCheckoutAddressPageState createState() => _RestaurantCheckoutAddressPageState();
}

class _RestaurantCheckoutAddressPageState extends State<RestaurantCheckoutAddressPage> {
  final _formKey = GlobalKey<FormState>();
  Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      context.read<PRestaurantUserAddress>().setUpAddress(widget.userAddressModel.toJson());
      context.read<PRestaurantUserAddress>().setUpMap();
    });
  }

  @override
  Widget build(BuildContext context) {
    double mapWidth = MediaQuery.of(context).size.width;
    double mapHeight = MediaQuery.of(context).size.height * 0.5 - 215;
    double iconSize = 40.0;

    return Consumer<PRestaurantUserAddress>(
      builder: (context, address, child) => WillPopScope(
        onWillPop: address.back,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Config.primaryColor,
              title: Text('Edit Address'),
            ),
            body: ListView(
              physics: ClampingScrollPhysics(),
              children: [
                Container(
                  color: Colors.black12,
                  child: Stack(
                    children: [
                      address.isFetchingLoc
                          ? SizedBox()
                          : Container(
                              width: mapWidth,
                              height: mapHeight,
                              child: GoogleMap(
                                zoomControlsEnabled: false,
                                mapType: MapType.normal,
                                scrollGesturesEnabled: false,
                                myLocationButtonEnabled: false,
                                buildingsEnabled: true,
                                zoomGesturesEnabled: false,
                                rotateGesturesEnabled: false,
                                tiltGesturesEnabled: false,
                                initialCameraPosition: address.usersLocation,
                                onMapCreated: (GoogleMapController controller) {
                                  _controller.complete(controller);
                                  address.setChangeMapController(_controller);
                                },
                              )),
                      new Positioned(
                        top: (mapHeight - iconSize) / 2,
                        right: (mapWidth - iconSize) / 2,
                        child: new Icon(Icons.person_pin_circle, size: iconSize),
                      ),
                      !address.isFetchingLoc
                          ? Positioned(
                              right: 10,
                              bottom: 10,
                              child: GestureDetector(
                                // onTap: () async {
                                //   await openMap(context, address);
                                // },
                                onTap: () => ExtendedNavigator.of(context).push(
                                  '/open-map',
                                ),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Config.primaryColor,
                                    borderRadius: BorderRadius.all(Radius.circular(3.0)),
                                  ),
                                  padding: EdgeInsets.all(12.0),
                                  child: Text(
                                    'CHANGE',
                                    style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox()
                    ],
                  ),
                ),
                SizedBox(height: 0.01.sh),
                RadioButtonGroup(
                  orientation: GroupedButtonsOrientation.HORIZONTAL,
                  labels: List<String>.generate(address.addrTypes.length, (index) => address.addrTypes[index]['name']),
                  onChange: (label, y) {
                    address.setAddrType(address.addrTypes[y]['hashid']);
                  },
                  picked: initialPicked(address),
                  labelStyle: TextStyle(fontSize: 32.sp),
                  itemBuilder: (radioButton, label, i) => Row(
                    children: [
                      radioButton,
                      label,
                    ],
                  ),
                ),
                Form(
                  key: _formKey,
                  child: Container(
                      padding: EdgeInsets.all(20.0),
                      child: Column(
                        children: [
                          textFieldMobileOption(address,
                              labelText: 'Address Nickname (option)', txtController: address.nicknameController),
                          textFieldMobileOption(address, labelText: 'Area', txtController: address.areaController),
                          textFieldMobileOption(address, labelText: 'Street', txtController: address.streetController),
                          buildType(address),
                          textFieldMobileOption(address,
                              labelText: 'Additional Directions (optional)',
                              isOptional: true,
                              txtController: address.addDirectionController),
                          textFieldMobileOption(address,
                              labelText: 'Mobile Number',
                              isMobileNumber: true,
                              txtController: address.mobileController),
                          textFieldMobileOption(address,
                              labelText: 'Landline (optional)',
                              txtController: address.landLineController,
                              isOptional: true),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              SizedBox(
                                height: 0.06.sh,
                                child: RaisedButton(
                                  elevation: 0.0,
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      address.saveAddress();
                                    }
                                  },
                                  color: Config.primaryColor,
                                  child: Text('${address.isSaving ? 'Saving address...' : 'Save address'}',
                                      style: TextStyle(color: Colors.white, fontSize: 32.sp)),
                                ),
                              ),
                            ],
                          )
                        ],
                      )),
                ),
              ],
            )),
      ),
    );
  }

  Widget buildType(PRestaurantUserAddress address) {
    if (address.addrTypeHashId == address.addrTypes[0]['hashid'])
      return house(address);
    else
      return others(address);
  }

  String initialPicked(PRestaurantUserAddress address) {
    String picked = '';
    for (int i = 0; i < address.addrTypes.length; i++) {
      if (address.addrTypeHashId == address.addrTypes[i]['hashid']) {
        picked = address.addrTypes[i]['name'];
        break;
      }
    }
    return picked;
  }

  Widget house(PRestaurantUserAddress address) {
    return Column(
      children: [textFieldMobileOption(address, labelText: 'House', txtController: address.houseController)],
    );
  }

  Widget others(PRestaurantUserAddress address) {
    return Column(
      children: [
        textFieldMobileOption(address, labelText: 'Building', txtController: address.buildingController),
        address.addrTypeHashId == address.addrTypes[1]['hashid']
            ? Column(
                children: [
                  textFieldMobileOption(address, labelText: 'Floor', txtController: address.floorController),
                  textFieldMobileOption(address, labelText: 'Office', txtController: address.officeController),
                ],
              )
            : Column(
                children: [
                  textFieldMobileOption(address, labelText: 'Floor', txtController: address.floorController),
                  textFieldMobileOption(address, labelText: 'Apartment No.', txtController: address.aparmentNo),
                ],
              )
      ],
    );
  }

  Widget textFieldMobileOption(PRestaurantUserAddress address,
      {String labelText,
      Function(String) onSubmitted,
      Function(String) onChanged,
      bool isMobileNumber: false,
      TextEditingController txtController,
      isOptional: false}) {
    PhoneNumber number = PhoneNumber(isoCode: 'QAT', phoneNumber: txtController.text);

    return Column(
      children: [
        isMobileNumber
            ? InternationalPhoneNumberInput(
                onInputChanged: (PhoneNumber number) {},
                onInputValidated: (valid) {},
                countries: ['QA'],
                ignoreBlank: true,
                initialValue: number,
                validator: (value) {
                  if (value.isEmpty && !isOptional) {
                    return '';
                  }
                  return null;
                },
                textFieldController: txtController,
                selectorTextStyle: TextStyle(color: Colors.black),
                inputDecoration: InputDecoration(
                  labelText: labelText,
                  contentPadding: EdgeInsets.zero,
                  labelStyle: TextStyle(color: Colors.black45),
                  hintStyle: TextStyle(fontSize: 32.sp),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black12),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black54),
                  ),
                ))
            : TextFormField(
                key: Key(txtController.text),
                minLines: 1,
                controller: txtController,
                maxLines: null,
                validator: (value) {
                  if (value.isEmpty && !isOptional) {
                    return '';
                  }
                  return null;
                },

                // initialValue: initialValue,
                onFieldSubmitted: (value) => onSubmitted(value),
                keyboardType: TextInputType.text,
                onChanged: (value) => onChanged(value),
                decoration: InputDecoration(
                  labelText: labelText,
                  contentPadding: EdgeInsets.zero,
                  labelStyle: TextStyle(color: Colors.black45),
                  hintStyle: TextStyle(fontSize: 32.sp),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black12),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black54),
                  ),
                )),
        SizedBox(height: 0.02.sh)
      ],
    );
  }

  Future<void> openMap(context, PRestaurantUserAddress address) async {
    LocationResult location = await showLocationPicker(
      context,
      Config.googleMapAPI,
      initialZoom: 16,
      // automaticallyAnimateToCurrentLocation: true,
      myLocationButtonEnabled: true,
      requiredGPS: true,
    );
    if (location != null) address.getData(LatLng(location.latLng.latitude, location.latLng.longitude));
  }
}
