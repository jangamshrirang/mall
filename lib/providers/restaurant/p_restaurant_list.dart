import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:wblue_customer/models/Restaurant/R_ListByRatings.dart';
import 'package:wblue_customer/models/Restaurant/advertisement.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'dart:collection';

import 'package:flutter/foundation.dart';

DioClient _dioClient = DioClient();

const int limit = 10;

class PRestaurantList with ChangeNotifier {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  final _swiperControl = SwiperController();
  RefreshController get refreshController => _refreshController;
  SwiperController get swiperControl => _swiperControl;
  List<RestaurantListModel> _restaurants = new List<RestaurantListModel>();
  List<RestaurantAdvertisementModel> _advertisements =
      new List<RestaurantAdvertisementModel>();
  bool _isLoading = true;
  int _pageIndex = 0;
  int _page = 1;

  UnmodifiableListView<RestaurantListModel> get restaurants =>
      UnmodifiableListView(_restaurants);

  UnmodifiableListView<RestaurantAdvertisementModel> get advertisements =>
      UnmodifiableListView<RestaurantAdvertisementModel>(_advertisements);

  int get pageIndex => _pageIndex;
  bool get isLoading => _isLoading;

  Future<void> fetchAllRestaurants() async {
    try {
      setLoading(true);
      _restaurants = await fetchRestaurants();
      _advertisements = await fetchAdvertisements();
      setLoading(false);
      notifyListeners();
    } catch (e) {
      print('Method (fetchAllRestaurants) error: $e');
    }
  }

  void followApi(RestaurantListModel restaurantListModel) async {
    try {
      Map<String, dynamic> _body = {'store_hashid': restaurantListModel.hashId};
      APIResponse res = await _dioClient.privatePost(
          '/vendor/store/customer-followeOrUnfollow-store',
          data: _body);

      if (res.code >= 400) {
        if (restaurantListModel.isFollowed)
          restaurantListModel.isFollowed = false;
        else
          restaurantListModel.isFollowed = true;
      } else {
        if (restaurantListModel.isFollowed)
          restaurantListModel.isFollowed = false;
        else
          restaurantListModel.isFollowed = true;
      }
      notifyListeners();
    } catch (e) {
      print('Method (followApi) error: $e');
      restaurantListModel.isFollowed = false;
      notifyListeners();
    }
  }

  void setNextPage() {
    _page++;
  }

  void resetPage() {
    _page = 1;
  }

  void setPageIndex({int page: 0}) {
    _pageIndex = page;
    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }

  void onRefresh() async {
    resetPage();
    _restaurants = await fetchRestaurants();

    if (_restaurants.length > 0) {
      _refreshController.refreshCompleted(resetFooterState: true);
    } else {
      _refreshController.refreshFailed();
    }

    notifyListeners();
  }

  void onLoading() async {
    List<RestaurantListModel> data =
        await fetchRestaurants(page: _page, limit: limit);

    if (data.length > 0) {
      _refreshController.loadComplete();
      setNextPage();
    } else if (data.length <= 0) {
      _refreshController.loadNoData();
    } else {
      _refreshController.loadFailed();
    }
    _restaurants.addAll(data);
    notifyListeners();
  }

  Future<List<RestaurantListModel>> fetchRestaurants(
      {int page: 1, int limit: limit}) async {
    try {
      Map<String, dynamic> _body = {'limit': limit.toString()};

      APIResponse res = await _dioClient
          .publicPost('/restaurant/list/all?page=$_page', data: _body);

      if (res.code >= 400) {
        return new List<RestaurantListModel>();
      }
      setNextPage();
      return res.data
          .map<RestaurantListModel>(
              (json) => RestaurantListModel.fromJson(json))
          .toList();
    } catch (e) {
      print('Method (fetchRestaurants) error: $e');
      return new List<RestaurantListModel>();
    }
  }

  Future<List<RestaurantAdvertisementModel>> fetchAdvertisements() async {
    try {
      APIResponse res =
          await _dioClient.publicPost('/store-advertisement-list');

      if (res.code >= 400) {
        return new List<RestaurantAdvertisementModel>();
      }
      return res.data
          .map<RestaurantAdvertisementModel>(
              (json) => RestaurantAdvertisementModel.fromJson(json))
          .toList();
    } catch (e) {
      return new List<RestaurantAdvertisementModel>();
    }
  }
}
