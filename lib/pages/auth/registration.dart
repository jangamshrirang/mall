import 'dart:convert';

import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/auth.dart';
import 'package:wblue_customer/models/validation.dart';
import 'package:wblue_customer/providers/auth/p_auth.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_input.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

class RegistrationPage extends StatefulWidget {
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  final scaffoldKey =
      GlobalKey<ScaffoldState>(debugLabel: "scaffold-get-phone");
  Map<String, dynamic> privacyPolicy;
  String mobile, fullname = '', phone = '', smsCode;
  AuthModel auth = AuthModel();
  bool phoneIsValid;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          width: size.width,
          height: size.height,
          decoration: BoxDecoration(
            gradient: RadialGradient(
              colors: [Config.primaryColor, Color(0xFF284d99).withOpacity(0.9)],
              stops: [0.4, 1],
              center: Alignment(0.0, 0.0),
              focal: Alignment(0.0, -0.0),
              focalRadius: 1.9,
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Center(
              child: SafeArea(
                child: SingleChildScrollView(
                  physics: ClampingScrollPhysics(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: size.width * 0.5,
                        width: size.width * 0.5,
                        child: Center(
                            child: WImageWidget(
                                placeholder: AssetImage(
                                    'assets/images/logos/wmall-512.png'))),
                      ),
                      SizedBox(
                        height: size.height * 0.05,
                      ),
                      Container(
                        width: size.width * 0.80,
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              AutoSizeText(
                                'Register',
                                style: TextStyle(
                                    color: Config.primaryColor,
                                    fontSize: 26.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              SizedBox(
                                child: Selector<PAuth, ValidationItem>(
                                  builder: (_, name, __) {
                                    fullname = name.value;

                                    return WInputWidget(
                                      label: 'Full Name',
                                      initialValue: name.value ?? '',
                                      customErrorText: name.error,
                                      onChanged: (val) {
                                        context.read<PAuth>().setFullname(val);
                                      },
                                    );
                                  },
                                  selector: (_, p) => p.fullname,
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              SizedBox(
                                child: Selector<PAuth, ValidationItem>(
                                  builder: (_, phone, __) {
                                    return SizedBox(
                                      child: InternationalPhoneNumberInput(
                                        onInputChanged: (PhoneNumber number) {
                                          mobile = number.parseNumber();
                                        },

                                        onInputValidated: (valid) {
                                          phoneIsValid = valid;
                                          context
                                              .read<PAuth>()
                                              .setNumberValid(true);
                                        },
                                        countries: ['QA'],
                                        ignoreBlank: false,
                                        // autoValidate: true,
                                        selectorTextStyle:
                                            TextStyle(color: Colors.black),
                                        // initialValue: number,
                                        // textFieldController: controller,
                                        inputDecoration: InputDecoration(
                                            hintText: 'Phone number',
                                            errorText: phone.error,
                                            filled: true,
                                            fillColor: Color(0xFF284d99)
                                                .withOpacity(0.1)),
                                      ),
                                    );
                                  },
                                  selector: (_, p) => p.number,
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Expanded(
                                    child: WRoundedButton(
                                      onCustomButtonPressed: () async {
                                        if (mobile.length > 0 &&
                                            fullname.length > 5 &&
                                            phoneIsValid) {
                                          final isExist = await auth
                                              .checkCredential(mobile);

                                          if (!isExist) {
                                            context
                                                .read<PAuth>()
                                                .setPhoneNumber(mobile);
                                            context
                                                .read<PAuth>()
                                                .setName(fullname);

                                            context
                                                .read<PAuth>()
                                                .submitPhoneNumber(
                                                    type: 'sign-up');
                                          } else
                                            context.read<PAuth>().setNumberValid(
                                                false,
                                                msg: 'Phone is already taken');
                                        }
                                      },
                                      btnColor: Config.primaryColor,
                                      labelColor: Colors.white,
                                      child: AutoSizeText('Register'),
                                    ),
                                  ),
                                ],
                              ),
                              Center(
                                child: FlatButton(
                                  onPressed: () =>
                                      ExtendedNavigator.of(context).pop(),
                                  child: AutoSizeText.rich(
                                    TextSpan(children: [
                                      TextSpan(
                                          text: 'Already have an account? '),
                                      TextSpan(
                                          text: 'Login',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold)),
                                    ]),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          boxShadow: <BoxShadow>[
                            new BoxShadow(
                              color: Colors.black26,
                              blurRadius: 10.0,
                              offset: new Offset(0.0, 0.0),
                            ),
                          ],
                        ),
                        margin: EdgeInsets.all(5.0),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20.0),
                        child: Center(
                          child: Column(
                            children: [
                              AutoSizeText(
                                'By Continuing, you agree to WMall',
                                style: TextStyle(color: Colors.white60),
                              ),
                              AutoSizeText.rich(
                                TextSpan(children: [
                                  TextSpan(
                                      text: 'Terms of Service',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white70,
                                          decoration: TextDecoration.underline),
                                      recognizer: new TapGestureRecognizer()
                                        ..onTap = () {
                                          loadTerms();
                                          displayBottomSheet(context);
                                        }),
                                  TextSpan(
                                      text: ' and ',
                                      style: TextStyle(color: Colors.white60)),
                                  TextSpan(
                                      text: 'Privacy Policy',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white70,
                                          decoration: TextDecoration.underline),
                                      recognizer: new TapGestureRecognizer()
                                        ..onTap = () {
                                          loadTerms();
                                          displayBottomSheet(context);
                                        }),
                                ]),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<Map<String, dynamic>> loadTerms() async {
    if (privacyPolicy == null) {
      final res = await rootBundle.loadString('assets/terms_of_service.json');
      privacyPolicy = jsonDecode(res);
    }
    return privacyPolicy;
  }

  void displayBottomSheet(BuildContext context) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (ctx) {
          return Container(
              height: MediaQuery.of(context).size.height * 0.8,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: AutoSizeText.rich(
                  TextSpan(children: [
                    TextSpan(
                        text: '${privacyPolicy['header']}',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: '\n \n${privacyPolicy['body']}'),
                    TextSpan(
                        text: '\n \n${privacyPolicy['header']}',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: '\n \n${privacyPolicy['body']}'),
                    TextSpan(
                        text: '\n \n${privacyPolicy['header']}',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: '\n \n${privacyPolicy['body']}'),
                    TextSpan(
                        text: '\n \n${privacyPolicy['header']}',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: '\n \n${privacyPolicy['body']}'),
                  ]),
                ),
              ));
        });
  }
}
