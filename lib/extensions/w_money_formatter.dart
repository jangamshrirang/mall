import 'package:money2/money2.dart';

extension WMallNumberParsing on dynamic {
  Money moneyFromNum() {
    Currency qar = Currency.create('QAR', 2, symbol: 'QR ');

    if (this is String) {
      return Money.from(double.parse(this), qar);
    } else if (this is int) {
      return Money.fromInt(this, qar);
    } else {
      return Money.from(this, qar);
    }
  }
}
