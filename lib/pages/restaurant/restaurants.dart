import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/models/Restaurant/resturentMenuModel.dart';
import 'package:wblue_customer/pages/restaurant/restaurentScanner.dart';
import 'package:wblue_customer/pages/restaurant/store/single_restaurant.dart';
import 'package:wblue_customer/providers/restaurant/pbrowse.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_loading.dart';
import 'package:wblue_customer/widgets/w_search_by_name.dart';

import '../../env/config.dart';
import 'package:wblue_customer/widgets/w_cart.dart';
import 'package:wblue_customer/widgets/w_drawer.dart';
import 'package:wblue_customer/widgets/w_restaurant_item.dart';
import 'package:provider/provider.dart';

import '../undeMaintanance.dart';
import 'R_ImageSlider.dart';
import 'menu.dart';

class RestaurantPage extends StatefulWidget {
  @override
  _RestaurantPageState createState() => _RestaurantPageState();
}

class _RestaurantPageState extends State<RestaurantPage> {
  Map filters = {
    'cousine': [],
    'table-location': [],
  };

  void _filterModal(BuildContext context) {
    showModalBottomSheet(
        context: context,
        elevation: 4.0,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState2) {
              Widget _radioItem(String name, String slug, String group) {
                return InkWell(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 4.0),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          filters[group].indexOf(slug) >= 0
                              ? Icons.check_circle
                              : Icons.radio_button_unchecked,
                          color: filters[group].indexOf(slug) >= 0
                              ? Colors.amber
                              : Colors.grey,
                        ),
                        SizedBox(width: 8.0),
                        Text(
                          name,
                          style: TextStyle(
                              fontWeight: filters[group].indexOf(slug) >= 0
                                  ? FontWeight.bold
                                  : FontWeight.normal),
                        ),
                      ],
                    ),
                  ),
                  onTap: () {
                    setState2(() {
                      if (filters[group].indexOf(slug) >= 0) {
                        filters[group].remove(slug);
                      } else {
                        filters[group].add(slug);
                      }
                    });
                  },
                );
              }

              return Container(
                padding: EdgeInsets.only(
                    left: 14.0, right: 14.0, bottom: 14.0, top: 8.0),
                decoration: BoxDecoration(
                  borderRadius:
                      BorderRadius.only(topLeft: Radius.circular(30.0)),
                  color: Colors.white,
                ),
                child: Wrap(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('Filter Result'),
                        IconButton(
                          icon: Icon(Icons.close),
                          onPressed: () {
                            setState(() {
                              Navigator.pop(context);
                            });
                          },
                        ),
                      ],
                    ),
                    ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        Container(
                          color: Colors.grey,
                          padding: EdgeInsets.symmetric(
                              vertical: 8.0, horizontal: 8.0),
                          child: Text('Cousine'),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 8.0, horizontal: 8.0),
                          child: Column(
                            children: <Widget>[
                              _radioItem(
                                  'Mediterranean', 'mediterranean', 'cousine'),
                              _radioItem('Burgers', 'burgers', 'cousine'),
                              _radioItem(
                                  'Coffee Shop', 'coffee-shop', 'cousine'),
                              _radioItem('Grill', 'grill', 'cousine'),
                              _radioItem(
                                  'Asian Fusion', 'asian-fusion', 'cousine'),
                            ],
                          ),
                        ),
                        Container(
                          color: Colors.grey,
                          padding: EdgeInsets.symmetric(
                              vertical: 8.0, horizontal: 8.0),
                          child: Text('Table Location'),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 8.0, horizontal: 8.0),
                          child: Column(
                            children: <Widget>[
                              _radioItem('Indoor', 'indoor', 'table-location'),
                              _radioItem(
                                  'Outdoor', 'outdoor', 'table-location'),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              );
            },
          );
        });
  }

  @override
  void initState() {
    super.initState();
    Future.microtask(
        () => context.read<PBrowseRestaurant>().fetchRestaurants());
  }

  List<bool> isFavorite;
  final controller = PageController(viewportFraction: 1.0);
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Browse Restaurants'),
        backgroundColor: Config.primaryColor,
        centerTitle: true,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(30.0),
          child: Column(
            children: <Widget>[
              Text(
                '0 RESTAURANT FOUND',
                style: TextStyle(fontSize: 12.0, color: Config.bodyColor),
              ),
              SizedBox(height: 14.0),
            ],
          ),
        ),
        actions: <Widget>[
          WCartWidget(),
          IconButton(
              icon: Image.network(
                "https://www.iconfinder.com/data/icons/business-marketing-outline-set-2/91/Business_Marketing_137-512.png",
                height: 20,
                color: Colors.white,
              ),
              onPressed: () {
                _scanQR();
                // Navigator.push(
                //     context,
                //     MaterialPageRoute(
                //         builder: (context) => ResaturentScanner()));
              })
        ],
      ),
      drawer: WDrawerWidget('browse-restaurant'),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {
            print('refreshing...');
          });
        },
        child: Column(
          children: <Widget>[
            WSearchByNameWidget(
              type: Types.restaurant,
              onPressed: () {
                _filterModal(context);
              },
            ),
            Expanded(
              child: Consumer<PBrowseRestaurant>(
                builder: (context, restaurant, child) => restaurant
                            .restaurants.length >
                        0
                    ? ListView.separated(
                        itemCount: restaurant.restaurants.length,
                        separatorBuilder: (BuildContext context, int index) {
                          if (index % 2 == 0) {
                            // Display `AdmobBanner` every 5 'separators'.
                            return index == 0
                                ? Container(
                                    height: 20,
                                  )
                                : Container(
                                    height: size.height * 0.2,
                                    width: double.infinity,
                                    color: Colors.grey[200],
                                    child: ListView(
                                      shrinkWrap: true,
                                      scrollDirection: Axis.horizontal,
                                      children: List.generate(
                                          restaurant.restaurants.length,
                                          (index) {
                                        RestaurantsModel restaurantsModel =
                                            RestaurantsModel.fromJson(
                                                restaurant.restaurants[index]);

                                        return dividercard(
                                            restaurantsModel, index);
                                      }),
                                    ),
                                  );
                          }
                          return Divider(color: Colors.transparent);
                        },
                        itemBuilder: (BuildContext context, int index) {
                          RestaurantsModel restaurantsModel =
                              RestaurantsModel.fromJson(
                                  restaurant.restaurants[index]);
                          if (isFavorite == null) {
                            isFavorite =
                                List<bool>(restaurant.restaurants.length);
                          }
                          return Column(
                            children: [
                              index == 0
                                  ? Container(
                                      height: size.height * 0.2,
                                      width: size.width * 0.95,
                                      child: RestaurantAdsImageSlider())
                                  : _card(restaurantsModel, index),
                            ],
                          );
                        },
                      )
                    : WLoadingWidget(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  dividercard(RestaurantsModel restaurant, int index) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        // stateManagment.setRestFave(null);
        // Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //         builder: (context) => restaurant.status == "CLOSED"
        //             ? UnderMaintanence()
        //             : SingleRestaurantPage(
        //                 restIndex: index,
        //                 restaurantListModel: restaurant,
        //               )));
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        height: size.width * 0.08,
        width: size.width * 0.6,
        decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(
                restaurant?.coverPicture,
              ),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.circular(4.0),
            border: Border.all(color: Colors.white)),
        child: Center(
            child: Text(
          '${restaurant.name}',
          style: TextStyle(
              color: Colors.white,
              fontSize: size.height * 0.022,
              fontWeight: FontWeight.bold),
        )),
      ),
    );
  }

  _card(RestaurantsModel restaurant, int index) {
    var size = MediaQuery.of(context).size;
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
      child: GestureDetector(
        onTap: () {
          stateManagment.setRestFave(null);
          // Navigator.push(
          //     context,
          //     MaterialPageRoute(
          //         builder: (context) => restaurant.status == "CLOSED"
          //             ? UnderMaintanence()
          //             : SingleRestaurantPage(
          //                 restIndex: index,
          //                 description: restaurant.slogan,
          //                 schedule: restaurant.schedule,
          //                 coverImage: restaurant.coverPicture,
          //                 name: restaurant.name,
          //                 profileImage: restaurant.profilePicture,
          //               )));
        },
        //context.navigator.push('/restaurants/single'),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 200,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16.0),
            color: Config.primaryColor,
          ),
          child: Stack(children: [
            Row(
              children: [
                Expanded(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(16.0),
                    child: CachedNetworkImage(
                      imageUrl: restaurant?.coverPicture,
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                      ),
                      placeholder: (context, url) => Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: WLoadingWidget(),
                      ),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                    // child: WImageWidget(
                    //   placeholder:
                    //       AssetImage('assets/images/logos/wmall-256.png'),
                    //   url: restaurant?.coverPicture,
                    //   fit: BoxFit.cover,
                    // )
                  ),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.only(top: 8),
                  height: MediaQuery.of(context).size.width * .1,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16.0),
                          topRight: Radius.circular(16.0)),
                      gradient: LinearGradient(
                          colors: [
                            Colors.black.withOpacity(.7),
                            Colors.transparent
                          ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter)),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 8),
                  height: MediaQuery.of(context).size.width * .1,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(16.0),
                          bottomLeft: Radius.circular(16.0)),
                      gradient: LinearGradient(
                          colors: [
                            Colors.transparent,
                            Colors.black.withOpacity(.7),
                          ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter)),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          AutoSizeText(
                            '${restaurant.name}',
                            style:
                                TextStyle(color: Colors.white, fontSize: 20.0),
                          ),
                          AutoSizeText(
                            '${restaurant.slogan}',
                            style:
                                TextStyle(color: Colors.white, fontSize: 14.0),
                          ),
                          Row(
                            children: [
                              AutoSizeText(
                                '1.1k',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 12.0),
                              ),
                              Icon(
                                Icons.favorite_border,
                                color: Colors.white,
                                size: 15,
                              )
                            ],
                          ),
                        ],
                      ),
                      SizedBox(width: 5.0),
                      Container(
                        padding: EdgeInsets.all(1.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4.0),
                          color: Colors.white,
                        ),
                        height: 60,
                        width: 60,
                        child: CachedNetworkImage(
                          imageUrl: restaurant?.profilePicture,
                          imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.circular(4.0),
                            ),
                          ),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                        // child: ClipRRect(
                        //     borderRadius: BorderRadius.circular(4.0),
                        //     child: WImageWidget(
                        //       placeholder: AssetImage(
                        //           'assets/images/logos/wmall-512.png'),
                        //       url: restaurant?.profilePicture,
                        //       fit: BoxFit.cover,
                        //     )),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.all(3.0),
                    child: Row(
                      children: [
                        AutoSizeText(
                          '${restaurant.schedule}',
                          style: TextStyle(color: Colors.white, fontSize: 12.0),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: restaurant.status.toLowerCase() == 'open'
                                ? Colors.green
                                : Colors.red,
                            borderRadius: BorderRadius.all(Radius.circular(
                                    4.0) //                 <--- border radius here
                                ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: AutoSizeText(
                              '${restaurant.status}',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 9.0),
                              minFontSize: 9.0,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () async {
                isFavorite[index] =
                    isFavorite[index] == null ? true : !isFavorite[index];
                if (isFavorite[index]) {
                  print(restaurant.name);
                } else {}
                setState(() {});
              },
              child: Container(
                margin: EdgeInsets.only(
                    left: size.width * 0.01, top: size.height * 0.01),
                height: size.height * 0.03,
                width: size.width * 0.2,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Config.primaryColor,
                    border: Border.all(color: Colors.white)),
                child: Center(
                    child: Text(
                  isFavorite[index] == null || isFavorite[index] == false
                      ? "Follow"
                      : "Following",
                  style: TextStyle(
                      color: Colors.white, fontSize: size.height * 0.015),
                )),
              ),
            ),
            Container(
                margin: EdgeInsets.only(
                    left: size.width * 0.6, top: size.height * 0.23),
                height: size.height * 0.03,
                width: size.width * 0.3,
                // color: Colors.red,
                child: Row(
                  children: [
                    iconsOnImage("assets/images/book table.png"),
                    iconsOnImage("assets/images/delivery.png"),
                    iconsOnImage("assets/images/take out.png"),
                  ],
                ))
          ]),
        ),
      ),
    );
  }

  iconsOnImage(String image) {
    var size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.025,
        width: size.width * 0.1,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2.0),
        ),
        child: Image.asset(image));
  }

  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        // result = qrResult;
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => SingleRestaurantPage()));
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          // result = "Camera permission was denied";
        });
      } else {
        setState(() {
          // result = "Unknown Error $ex";
        });
      }
    } on FormatException {
      setState(() {
        // result = "You pressed the back button before scanning anything";
      });
    } catch (ex) {
      setState(() {
        // result = "Unknown Error $ex";
      });
    }
  }
}
