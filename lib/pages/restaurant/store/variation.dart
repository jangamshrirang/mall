import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/food_type_model.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/providers/restaurant/p_variation.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/widgets/item_counter_stepper.dart';

class RestaurantFoodVariation extends StatefulWidget {
  final Food food;
  final String restaurantHashid, reservationHashId;
  final bool isFromScan, isFromTakeAway, isDelivery, preorder;

  const RestaurantFoodVariation(
      {this.food,
      this.isDelivery: false,
      this.isFromTakeAway: false,
      this.preorder: false,
      this.restaurantHashid,
      this.isFromScan: false,
      this.reservationHashId});

  @override
  _RestaurantFoodVariationState createState() => _RestaurantFoodVariationState();
}

class _RestaurantFoodVariationState extends State<RestaurantFoodVariation> {
  int qty = 1;
  double total = 0;
  TextEditingController reqController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    total = double.parse(widget.food.price);

    Future.microtask(() => context.read<PRestaurantFoodVariation>().setVariation(widget.food.variations));
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PRestaurantFoodVariation>(
        builder: (context, variation, child) => Scaffold(
              bottomNavigationBar: Container(
                height: 0.1.sh,
                padding: EdgeInsets.all(10),
                alignment: Alignment.topCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    CustomStepper(
                      lowerLimit: 1,
                      upperLimit: 99,
                      stepValue: 1,
                      value: 1,
                      iconSize: 24,
                      onCountChange: (value) {
                        qty = value;
                      },
                    ),
                    Expanded(
                        child: Selector<PRestaurantFoodCart, bool>(
                      selector: (_, clear) =>
                          clear.restaurantCart.food.length > 0 && clear.restHashid != clear.restHashidInCart,
                      builder: (context, isSameRestaurant, child) => RaisedButton(
                        onPressed: () async {
                          List<Map<String, dynamic>> selectedVariation = new List<Map<String, dynamic>>();

                          for (int i = 0; i < widget.food.variations.length; i++) {
                            for (int y = 0; y < widget.food.variations[i].variationTypeSelections.length; y++) {
                              if (widget.food.variations[i].variationTypeSelections[y].isSelected)
                                selectedVariation.add(
                                    {'variation_item_id': widget.food.variations[i].variationTypeSelections[y].hashid});
                              widget.food.variations[i].variationTypeSelections[y].isSelected = false;
                            }
                          }

                          try {
                            if (isSameRestaurant) {
                              showCartAlertDialog(
                                BackButtonBehavior.none,
                                cancel: () {},
                                confirm: () {
                                  context.read<PRestaurantFoodCart>().apiRestaurantAddToCart(
                                      restHashid: widget.restaurantHashid,
                                      foodId: widget.food.hashid,
                                      quantity: qty,
                                      orderType: widget.isFromScan
                                          ? WMallOrderType.dineIn
                                          : widget.isDelivery
                                              ? WMallOrderType.delivery
                                              : WMallOrderType.takeAway,
                                      reservationHashId:
                                          widget.reservationHashId != null ? widget.reservationHashId : "null",
                                      specialRequest: '${reqController.text.length > 0 ? reqController.text : 'any'}',
                                      isReset: true,
                                      variations: selectedVariation);
                                  reqController.clear();
                                  Navigator.of(context).pop();
                                },
                                backgroundReturn: () {},
                              );
                            } else {
                              bool canNav = await ExtendedNavigator.of(context).root.canNavigate('/restaurants/cart');
                              if (!canNav) return;  

                              context.read<PRestaurantFoodCart>().apiRestaurantAddToCart(
                                  restHashid: widget.restaurantHashid,
                                  foodId: widget.food.hashid,
                                  quantity: qty,
                                  orderType: widget.isFromScan
                                      ? WMallOrderType.dineIn
                                      : widget.isDelivery
                                          ? WMallOrderType.delivery
                                          : WMallOrderType.takeAway,
                                  reservationHashId:
                                      widget.reservationHashId != null ? widget.reservationHashId : "null",
                                  specialRequest: '${reqController.text.length > 0 ? reqController.text : 'any'}',
                                  variations: selectedVariation);
                              reqController.clear();
                              Navigator.of(context).pop();
                            }
                          } catch (e) {
                            print('Error in restCatogries.dart file when adding cart: Error ($e)');
                          }

                          // if (isSameRestaurant) {
                          //   showCartAlertDialog(
                          //     BackButtonBehavior.none,
                          //     cancel: () {},
                          //     confirm: () {
                          //       context.read<PRestaurantFoodCart>().setItems(
                          //             restHashid: widget.rHashID,
                          //             food: widget.food,
                          //             qty: qty,
                          //             total: total,
                          //             isReset: true,
                          //           );

                          //       Navigator.of(context).pop();
                          //     },
                          //     backgroundReturn: () {},
                          //   );
                          // } else {
                          //   context.read<PRestaurantFoodCart>().setItems(
                          //         restHashid: widget.rHashID,
                          //         food: widget.food,
                          //         qty: qty,
                          //         total: total,
                          //       );
                          //   Navigator.of(context).pop();
                          // }

                          // TODO: DO NOT REMOVE
                          //Future.microtask(() => context
                          //     .read<R_AddToCartHelper>()
                          //     .fetchRestaurantsAddToCart(
                          //         widget.food.hashid,
                          //         qty,
                          //         "any",
                          //         widget.preorder == "yes"
                          //             ? 1
                          //             : widget.fromTakeAway == "no"
                          //                 ? 2
                          //                 : 3,
                          //         widget.preorder == "yes" ? 1 : 0,
                          //         widget.reservationHashId != null
                          //             ? widget.reservationHashId
                          //             : "null"));

                          // Fluttertoast.showToast(
                          //     msg: "Added to cart",
                          //     toastLength: Toast.LENGTH_SHORT,
                          //     gravity: ToastGravity.CENTER,
                          //     timeInSecForIosWeb: 1,
                          //     backgroundColor: Colors.black,
                          //     textColor: Colors.white,
                          //     fontSize: 16.0);
                        },
                        textColor: Colors.white,
                        color: Config.primaryColor,
                        child: Text('ADD TO CART'),
                      ),
                    )),
                  ],
                ),
              ),
              body: Container(
                color: Config.primaryColor.withAlpha(5),
                child: CustomScrollView(
                  slivers: [
                    SliverPersistentHeader(
                      pinned: true,
                      delegate: SliverFoodHeaderDelegate(
                          collapsedHeight: 0.05.sh,
                          expandedHeight: 0.4.sh,
                          paddingTop: MediaQuery.of(context).padding.top,
                          full: widget.food.full,
                          description: widget.food.description,
                          thumbnail: widget.food.thumbnail),
                    ),
                    SliverToBoxAdapter(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                            child: AutoSizeText(
                              'QR ${widget.food.price}',
                              style: TextStyle(fontSize: 40.sp, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Divider(),
                        ],
                      ),
                    ),
                    SliverList(
                        delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) => Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    AutoSizeText(
                                      '${variation.variations[index].name}',
                                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 36.sp),
                                    ),
                                    Container(
                                      color: Config.primaryColor.withAlpha(16),
                                      padding: EdgeInsets.all(3),
                                      child: AutoSizeText(
                                        '${variation.variations[index].selectionMaxNumber} ${variation.variations[index].isRequired.parseBool() ? 'Required' : 'Optional'}',
                                        style: TextStyle(fontSize: 24.sp),
                                      ),
                                    ),
                                  ],
                                ),
                                AutoSizeText(
                                  'Select up to ${variation.variations[index].selectionMaxNumber}',
                                  style: TextStyle(),
                                ),
                              ],
                            ),
                            variation.variations[index].isMultiple.parseBool()
                                ? CheckboxGroup(
                                    orientation: GroupedButtonsOrientation.VERTICAL,
                                    labels: List<String>.generate(
                                      variation.variations[index].variationTypeSelections.length,
                                      (i) => variation.variations[index].variationTypeSelections[i].name,
                                    ),
                                    onChange: (isChecked, label, cbIndex) {
                                      variation.variations[index].variationTypeSelections[cbIndex].isSelected =
                                          isChecked;
                                    },
                                    labelStyle: TextStyle(fontSize: 32.sp),
                                    itemBuilder: (Checkbox cb, Text txt, int i) {
                                      return Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Row(
                                            children: <Widget>[
                                              cb,
                                              txt,
                                            ],
                                          ),
                                          AutoSizeText(
                                            '+ QR ${variation.variations[index].variationTypeSelections[i].price}',
                                            style: TextStyle(fontWeight: FontWeight.w500),
                                            minFontSize: 26.sp,
                                            stepGranularity: 26.sp,
                                          )
                                        ],
                                      );
                                    },
                                  )
                                : RadioButtonGroup(
                                    orientation: GroupedButtonsOrientation.VERTICAL,
                                    labels: List<String>.generate(
                                      variation.variations[index].variationTypeSelections.length,
                                      (i) => variation.variations[index].variationTypeSelections[i].name,
                                    ),
                                    // onSelected: (String selected) => setState(
                                    //   () {
                                    //     variation.variations[index].selected =
                                    //         selected;
                                    //   },
                                    // ),
                                    onChange: (label, y) {
                                      variation.variations[index].variationTypeSelections[y].isSelected = true;
                                      for (int variationIndex = 0;
                                          variationIndex < variation.variations[index].variationTypeSelections.length;
                                          variationIndex++) {
                                        if (variationIndex != y) {
                                          variation.variations[index].variationTypeSelections[variationIndex]
                                              .isSelected = false;
                                        }
                                      }
                                    },
                                    // picked:
                                    //     variation.variations[index].selected,
                                    labelStyle: TextStyle(fontSize: 32.sp),
                                    itemBuilder: (radioButton, label, i) => GestureDetector(
                                      child: Container(
                                          child: Row(
                                        children: [
                                          Expanded(
                                            child: Container(
                                              child: Row(
                                                mainAxisSize: MainAxisSize.max,
                                                children: [
                                                  radioButton,
                                                  Expanded(
                                                      child: Container(
                                                    padding: EdgeInsets.all(14),
                                                    color: Colors.transparent,
                                                    child: label,
                                                  )),
                                                  AutoSizeText(
                                                    '+ QR ${variation.variations[index].variationTypeSelections[i].price}',
                                                    style: TextStyle(fontWeight: FontWeight.w500),
                                                    minFontSize: 26.sp,
                                                    stepGranularity: 26.sp,
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                          // child: Row(
                                          //   mainAxisAlignment:
                                          //       MainAxisAlignment.spaceBetween,
                                          //   children: [
                                          //     Row(
                                          //       children: <Widget>[
                                          //         radioButton,
                                          //         Container(
                                          //             color: Colors.red, child: label),
                                          //       ],
                                          //     ),
                                          //     AutoSizeText(
                                          //       '+ QR ${variation.variations[index].variationTypeSelections[i].price}',
                                          //       style: TextStyle(
                                          //           fontWeight: FontWeight.w500),
                                          //       minFontSize: 26.sp,
                                          //       stepGranularity: 26.sp,
                                          //     )
                                          //   ],
                                          // ),
                                          ),
                                    ),
                                  ),
                          ],
                        ),
                      ),
                      childCount: variation.variations?.length,
                    ))
                  ],
                ),
              ),
            ));
  }

  void showCartAlertDialog(BackButtonBehavior backButtonBehavior,
      {VoidCallback cancel, VoidCallback confirm, VoidCallback backgroundReturn}) {
    BotToast.showAnimationWidget(
        clickClose: true,
        allowClick: false,
        onlyOne: true,
        crossPage: true,
        backgroundColor: Colors.black54,
        backButtonBehavior: backButtonBehavior,
        toastBuilder: (cancelFunc) => AlertDialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              title: const Text('Clear Cart?'),
              content: Text(
                  'You have already selected different restaurant. If you continue your cart and selection will be removed.'),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    cancelFunc();
                    cancel?.call();
                  },
                  highlightColor: const Color(0x55FF8A80),
                  splashColor: const Color(0x99FF8A80),
                  child: Text(
                    'cancel',
                    style: TextStyle(color: Colors.redAccent, fontSize: 36.sp),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    cancelFunc();
                    confirm?.call();
                  },
                  child: Text(
                    'confirm',
                    style: TextStyle(fontSize: 36.sp),
                  ),
                ),
              ],
            ),
        animationDuration: Duration(milliseconds: 300));
  }
}

class SliverFoodHeaderDelegate extends SliverPersistentHeaderDelegate {
  final double collapsedHeight;
  final double expandedHeight;
  final String full;
  final String thumbnail;
  final String description;
  final double paddingTop;

  SliverFoodHeaderDelegate(
      {this.collapsedHeight, this.expandedHeight, this.full, this.thumbnail, this.description, this.paddingTop});

  @override
  double get minExtent => this.collapsedHeight + this.paddingTop;

  @override
  double get maxExtent => this.expandedHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

  Color makeStickyHeaderBgColor(shrinkOffset) {
    final int alpha = (shrinkOffset / (this.maxExtent - this.minExtent) * 255).clamp(0, 255).toInt();
    return Color.fromARGB(alpha, 255, 255, 255);
  }

  Color makeStickyHeaderTextColor(shrinkOffset, isIcon) {
    if (shrinkOffset <= 50) {
      return isIcon ? Colors.white : Colors.transparent;
    } else {
      final int alpha = (shrinkOffset / (this.maxExtent - this.minExtent) * 255).clamp(0, 255).toInt();
      return Color.fromARGB(alpha, 0, 0, 0);
    }
  }

  Color makeStickyHeaderIconBgColor(shrinkOffset) {
    if (shrinkOffset <= 50) {
      return Colors.black26;
    } else {
      return Colors.transparent;
    }
  }

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      height: this.maxExtent,
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: full ?? '',
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            placeholder: (context, url) => Container(
              color: Colors.black,
              height: 0.26.sh,
              child: SafeArea(
                bottom: false,
                child: Image.network(
                  thumbnail,
                  fit: BoxFit.fitWidth,
                  height: double.infinity,
                  width: double.infinity,
                  alignment: Alignment.center,
                ),
              ),
            ),
            errorWidget: (context, url, error) => Container(
              height: 0.26.sh,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    'Image Not Found',
                    style: TextStyle(fontSize: 28.sp, fontWeight: FontWeight.w300),
                  ),
                  SizedBox(
                    height: 0.02.sh,
                  ),
                  Icon(
                    MdiIcons.alertCircle,
                    color: Colors.red,
                    size: 0.08.sh,
                  )
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            alignment: Alignment.bottomCenter,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: <Color>[Colors.black.withAlpha(0), Colors.black12, Colors.black45],
              ),
            ),
            child: Container(
              height: 100,
              alignment: Alignment.bottomLeft,
              child: AutoSizeText(
                '$description',
                style: TextStyle(color: Colors.white),
                minFontSize: 32.sp,
                stepGranularity: 32.sp,
                maxLines: 5,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),

          // Put your head back
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            child: Container(
              color: this.makeStickyHeaderBgColor(shrinkOffset), // Background color
              child: SafeArea(
                bottom: false,
                child: Container(
                  height: this.collapsedHeight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      FlatButton(
                        onPressed: () => Navigator.pop(context),
                        child: Icon(
                          Icons.keyboard_backspace,
                          color: this.makeStickyHeaderTextColor(shrinkOffset, true),
                          size: 20.0,
                        ),
                        height: 40.0,
                        shape: CircleBorder(),
                        color: this.makeStickyHeaderIconBgColor(shrinkOffset),
                      ),
                      Text(
                        'CUSTOMIZE',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: this.makeStickyHeaderTextColor(shrinkOffset, false), // Title color
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

extension BoolParsing on int {
  bool parseBool() {
    return this == 1 ? true : false;
  }
}
