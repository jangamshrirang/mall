import 'package:flutter/material.dart';

import 'BrandProduts.dart';

class BrandsPage extends StatefulWidget {
  @override
  _BrandsPageState createState() => _BrandsPageState();
}

class _BrandsPageState extends State<BrandsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white, //Config.bodyColor,
        appBar: AppBar(
          leading: SizedBox(),
          elevation: 0.0,
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            'Brands',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                _searchBar(),
                Wrap(
                  spacing: 0,
                  children: [
                    LayoutThree(
                        image:
                            "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/1-5.jpg",
                        premium: "premium"),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/2-8.jpg",
                    ),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/4-5.jpg",
                    ),
                    LayoutThree(
                        image:
                            "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/5-5.jpg",
                        premium: "premium"),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/12-1.jpg",
                    ),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/14-1.jpg",
                    ),
                    LayoutThree(
                        image:
                            "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/9-3.jpg",
                        premium: "premium"),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/22-min-2.jpg",
                    ),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/26-min-1.jpg",
                    ),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/1-5.jpg",
                    ),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/2-8.jpg",
                    ),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/4-5.jpg",
                    ),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/5-5.jpg",
                    ),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/12-1.jpg",
                    ),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/14-1.jpg",
                    ),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/9-3.jpg",
                    ),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/22-min-2.jpg",
                    ),
                    LayoutThree(
                      image:
                          "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/26-min-1.jpg",
                    ),
                  ],
                ),
              ],
            ),
          ),
        ));
  }

  _searchBar() {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20, top: 5),
      height: size.height * 0.06,
      child: TextField(
        cursorColor: Colors.black,
        decoration: InputDecoration(
            filled: true,
            labelStyle:
                TextStyle(color: Colors.black38, fontWeight: FontWeight.normal),
            fillColor: Colors.white,
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black, width: 1),
              borderRadius: BorderRadius.circular(10.0),
            ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                borderSide: BorderSide(color: Colors.black26, width: 1)),
            labelText: "Search..."),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            // _notesForDisplay = _notes.where((note) {
            //   var noteTitle = note.title.toLowerCase();
            //   return noteTitle.contains(text);
            // }
            // ).toList();
          });
        },
      ),
    );
  }
}

class LayoutThree extends StatelessWidget {
  String image, premium;
  LayoutThree({this.image, this.premium});
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => BrandProductsPage()));
      },
      child: Stack(
        children: [
          Container(
              width: size.width * 0.45,
              height: size.height * 0.25,
              margin: EdgeInsets.only(bottom: 5, left: 5, top: 5, right: 2),
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.4),
                    spreadRadius: 1,
                    blurRadius: 3,
                    offset: Offset(1, 3), // changes position of shadow
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(4)),
                image: DecorationImage(
                  image: NetworkImage(
                    image,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              child: Container(
                  //text container
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(4),
                        bottomRight: Radius.circular(4)),
                  ),
                  margin: EdgeInsets.only(
                    top: size.height * 0.20,
                  ),
                  padding: EdgeInsets.only(
                      left: size.width * 0.01,
                      right: size.width * 0.01,
                      top: size.height * 0.01),
                  height: size.height * 0.03,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Brand Name",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.normal,
                            fontSize: size.height * 0.023),
                      ),
                    ],
                  ))),
          Container(
            height: size.height * 0.02,
            width: size.width * 0.2,
            margin: EdgeInsets.only(
                top: size.height * 0.01, left: size.width * 0.015),
            decoration: BoxDecoration(
              color: premium != null ? Colors.amber : Colors.transparent,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0)),
            ),
            child: Text(
              premium != null ? "  Premium" : "",
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
