import 'package:wblue_customer/services/global.dart';

class Cart {
  String restaurant;
  List<CartItem> items;

  Cart(
    this.restaurant, {
    this.items,
  });

  add(CartItem item) {
    items = Global.cart[restaurant]?.items;
    if (items == null) {
      items = [];
    }
    items.add(item);
    Global.cart[restaurant] = this;
  }

  int allCount() {
    int count = 0;
    Global.cart.forEach((key, val) {
      val.items.map((item) {
        count += item.qty;
      }).toList();
    });
    return count;
  }
}

class CartItem {
  final String name,image;
  final double price;
  final int qty;

  CartItem({
    this.name: '',
    this.price: 0,
    this.qty: 0,
    this.image,
  });
}
