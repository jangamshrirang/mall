class RestaurentReservationModel {
  RestaurentReservationModel({
    this.reservationHashId,
    this.restaurant,
    this.fullCover,
    this.thumbnailCover,
    this.full,
    this.thumbnail,
    this.tableNo,
    this.timeIn,
    this.date,
    this.numberOfGuest,
    this.withOrder,
    this.status,
    this.myPreOrder,
    this.totalAmount,
    this.myCart,
    this.qrCode,
    this.qrcodeStatus,
    this.specialRequest,
  });

  String reservationHashId;
  Restaurant restaurant;
  String fullCover;
  String thumbnailCover;
  String full;
  String thumbnail;
  String tableNo;
  String timeIn;
  DateTime date;
  String numberOfGuest;
  String withOrder;
  String status;
  List<MyPreOrder> myPreOrder;
  dynamic totalAmount;
  MyCart myCart;
  String qrCode;
  int qrcodeStatus;
  dynamic specialRequest;

  factory RestaurentReservationModel.fromJson(Map<String, dynamic> json) => RestaurentReservationModel(
        reservationHashId: json["reservation_hash_id"],
        restaurant: Restaurant.fromJson(json["restaurant"]),
        fullCover: json["full_cover"],
        thumbnailCover: json["thumbnail_cover"],
        full: json["full"],
        thumbnail: json["thumbnail"],
        tableNo: json["table_no"],
        timeIn: json["time_in"],
        date: DateTime.parse(json["date"]),
        numberOfGuest: json["number_of_guest"],
        withOrder: json["with_order"],
        status: json["status"],
        myPreOrder: List<MyPreOrder>.from(json["my_pre_order"].map((x) => MyPreOrder.fromJson(x))),
        totalAmount: json["total_amount"],
        myCart: json["my_cart"] != null ? MyCart.fromJson(json["my_cart"]) : null,
        qrCode: json["qr_code"],
        qrcodeStatus: json["qrcode_status"],
        specialRequest: json["special_request"],
      );

  Map<String, dynamic> toJson() => {
        "reservation_hash_id": reservationHashId,
        "restaurant": restaurant.toJson(),
        "full_cover": fullCover,
        "thumbnail_cover": thumbnailCover,
        "full": full,
        "thumbnail": thumbnail,
        "table_no": tableNo,
        "time_in": timeIn,
        "date":
            "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "number_of_guest": numberOfGuest,
        "with_order": withOrder,
        "status": status,
        "my_pre_order": List<dynamic>.from(myPreOrder.map((x) => x.toJson())),
        "total_amount": totalAmount,
        "my_cart": myCart.toJson(),
        "qr_code": qrCode,
        "qrcode_status": qrcodeStatus,
        "special_request": specialRequest,
      };
}

class MyCart {
  MyCart({
    this.transactionNumber,
    this.createdAt,
    this.hashId,
    this.foodsInfo,
    this.totalCount,
    this.totalAmount,
  });

  int transactionNumber;
  DateTime createdAt;
  String hashId;
  List<dynamic> foodsInfo;
  int totalCount;
  String totalAmount;

  factory MyCart.fromJson(Map<String, dynamic> json) => MyCart(
        transactionNumber: json["transaction_number"],
        createdAt: DateTime.parse(json["created_at"]),
        hashId: json["hash_id"],
        foodsInfo: List<dynamic>.from(json["foods_info"].map((x) => x)),
        totalCount: json["total_count"],
        totalAmount: json["total_amount"],
      );

  Map<String, dynamic> toJson() => {
        "transaction_number": transactionNumber,
        "created_at": createdAt.toIso8601String(),
        "hash_id": hashId,
        "foods_info": List<dynamic>.from(foodsInfo.map((x) => x)),
        "total_count": totalCount,
        "total_amount": totalAmount,
      };
}

class MyPreOrder {
  MyPreOrder({
    this.name,
    this.price,
    this.quantity,
    this.status,
    this.request,
    this.hashId,
    this.foodInfo,
  });

  String name;
  String price;
  int quantity;
  int status;
  String request;
  String hashId;
  FoodInfo foodInfo;

  factory MyPreOrder.fromJson(Map<String, dynamic> json) => MyPreOrder(
        name: json["name"],
        price: json["price"],
        quantity: json["quantity"],
        status: json["status"],
        request: json["request"],
        hashId: json["hash_id"],
        foodInfo: FoodInfo.fromJson(json["food_info"]),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "price": price,
        "quantity": quantity,
        "status": status,
        "request": request,
        "hash_id": hashId,
        "food_info": foodInfo.toJson(),
      };
}

class FoodInfo {
  FoodInfo({
    this.full,
    this.thumbnail,
  });

  String full;
  String thumbnail;

  factory FoodInfo.fromJson(Map<String, dynamic> json) => FoodInfo(
        full: json["full"],
        thumbnail: json["thumbnail"],
      );

  Map<String, dynamic> toJson() => {
        "full": full,
        "thumbnail": thumbnail,
      };
}

class Restaurant {
  Restaurant({
    this.name,
    this.storeInformation,
    this.phone,
    this.latitude,
    this.longitude,
    this.hashid,
    this.mainStoreId,
    this.isOpened,
  });

  String name;
  String storeInformation;
  String phone;
  String latitude;
  String longitude;
  String hashid;
  dynamic mainStoreId;
  IsOpened isOpened;

  factory Restaurant.fromJson(Map<String, dynamic> json) => Restaurant(
        name: json["name"],
        storeInformation: json["store_information"],
        phone: json["phone"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        hashid: json["hashid"],
        mainStoreId: json["main_store_id"],
        isOpened: IsOpened.fromJson(json["is_opened"]),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "store_information": storeInformation,
        "phone": phone,
        "latitude": latitude,
        "longitude": longitude,
        "hashid": hashid,
        "main_store_id": mainStoreId,
        "is_opened": isOpened.toJson(),
      };
}

class IsOpened {
  IsOpened({
    this.now,
    this.timeRemaining,
    this.status,
    this.next,
  });

  String now;
  int timeRemaining;
  String status;
  String next;

  factory IsOpened.fromJson(Map<String, dynamic> json) => IsOpened(
        now: json["now"],
        timeRemaining: json["time_remaining"],
        status: json["status"],
        next: json["next"],
      );

  Map<String, dynamic> toJson() => {
        "now": now,
        "time_remaining": timeRemaining,
        "status": status,
        "next": next,
      };
}
