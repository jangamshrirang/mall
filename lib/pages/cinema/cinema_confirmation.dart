import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_qr.dart';

class CinemaConfirmationPage extends StatefulWidget {
  @override
  _CinemaConfirmationPageState createState() => _CinemaConfirmationPageState();
}

class _CinemaConfirmationPageState extends State<CinemaConfirmationPage> {
  @override
  Widget build(BuildContext context) {
    TableRow _tableRow(String label, String value) {
      return TableRow(children: [
        TableCell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(label),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(value),
          ),
        ),
      ]);
    }

    bool payAtCinema = Global.payAtCinema;

    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Center(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  'Cinema Name',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                ),
              ),
              FractionallySizedBox(
                widthFactor: 0.4,
                child:
                    Image.asset('assets/images/samples/cinema-logos/wmall.png'),
              ),
              SizedBox(height: 24.0),
              FractionallySizedBox(
                widthFactor: 0.5,
                child: Image.asset('assets/images/tickets.png'),
              ),
              Icon(
                Icons.check_circle,
                color: Colors.green,
                size: 64.0,
              ),
              Text('Thank You for Your Reservation'),
              SizedBox(height: 24.0),
              Text('We have received your booking.'),
              SizedBox(height: 8.0),
              FractionallySizedBox(
                widthFactor: 0.8,
                child: Table(
                  border: TableBorder.all(color: Colors.grey),
                  children: [
                    _tableRow('Ref Number', '#123'),
                    _tableRow('Date', 'Apr 30, 2020'),
                    _tableRow('Time', '3:00 PM'),
                    _tableRow('Seats', 'B12 | B13'),
                    _tableRow('Tickets', 'WPS123 | WPS124'),
                    _tableRow('Amount', 'QAR 160.00'),
                    _tableRow(
                        'Payment', payAtCinema ? 'Pay At Cinema' : 'PAID'),
                  ],
                ),
              ),
              SizedBox(height: 24.0),
              FractionallySizedBox(
                widthFactor: 0.8,
                child: Text(
                  'Kindly present the QR code when you arrive at the cinema.',
                  textAlign: TextAlign.center,
                ),
              ),
              FractionallySizedBox(
                widthFactor: 0.5,
                child: WButtonWidget(
                  title: 'Qr Code',
                  onPressed: () {
                    setState(() {
                      _showBarcode();
                    });
                  },
                ),
              ),
              Divider(),
              FractionallySizedBox(
                widthFactor: 0.7,
                child: WButtonWidget(
                  title: 'Back to Home',
                  onPressed: () {
                    ExtendedNavigator.of(context).root.push('/');
                  },
                ),
              ),
              Divider(),
              InkWell(
                child: Text('Check Reservations'),
                onTap: () {
                  ExtendedNavigator.of(context).root.push('/reservations');
                },
              ),
              SizedBox(height: 50.0),
            ],
          ),
        ),
      ),
    );
  }

  void _showBarcode() {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            return Container(
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  )),
              child: Wrap(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Transaction Code',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      IconButton(
                        icon: Icon(Icons.close),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                  JQRWidget(
                    size: MediaQuery.of(context).size.width * 0.8,
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
