import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../env/config.dart';
class WCinemaSeatSelectionWidget extends StatefulWidget {
  final Function(List) onUpdate;
  final int count;

  WCinemaSeatSelectionWidget({
    this.onUpdate: functionArg,
    this.count: 1,
  });

  @override
  _WCinemaSeatSelectionWidgetState createState() => _WCinemaSeatSelectionWidgetState();
}

enum SeatAlignment {
  center,
  inside,
  outside,
}

class _WCinemaSeatSelectionWidgetState extends State<WCinemaSeatSelectionWidget> {
  List<String> selectedSeats = new List<String>();
  List<String> reserved = [
    'a4',
    'b8',
    'b9',
    'e36',
    'f41',
  ];
  double size = 30.0;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size.width * 0.08;

    if (widget.count < selectedSeats.length) {
      int toRemove = selectedSeats.length - widget.count;
      for (int i = 0; i < toRemove; i++) {
        selectedSeats.removeAt(0);
      }
    }

    return Column(
      children: <Widget>[
        Card(
          margin: EdgeInsets.all(16.0),
          clipBehavior: Clip.hardEdge,
          child: Container(
            color: Config.primaryColor,
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                _arrangement('j', alignment: SeatAlignment.inside, count: 3, numStart: 69),
                _arrangement('i', count: 3, alignment: SeatAlignment.center, numStart: 63),
                _arrangement('h', numStart: 55),
                _arrangement('g', numStart: 47),
                _arrangement('f', numStart: 39),
                _arrangement('e', numStart: 31),
                _arrangement('d', numStart: 23),
                _arrangement('c', numStart: 15),
                _arrangement('b', specialCount: 1, numStart: 7),
                _arrangement('a', count: 3, specialCount: 3),
                _screen(),
              ],
            ),
          ),
        ),
        _legend(),
      ],
    );
  }

  Widget _legend() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _legendItem(label: 'Reserved', color: Colors.blueGrey),
        _legendItem(label: 'Available', color: Colors.white),
        _legendItem(label: 'PWD/Seniors', color: Colors.amber),
      ],
    );
  }

  Widget _legendItem({String label: '', Color color: Colors.amber}) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: size,
          width: size,
          decoration: BoxDecoration(
            color: color,
            border: Border.all(color: color == Colors.white ? Colors.black : color),
            borderRadius: BorderRadius.circular(4.0),
          ),
          child: SizedBox(),
        ),
        SizedBox(width: 4.0),
        Text(label),
      ],
    );
  }

  Widget _screen() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 32.0),
          child: Transform(
            transform: Matrix4.identity()
              ..setEntry(3, 2, -0.01)
              ..rotateX(0.6),
            alignment: FractionalOffset.center,
            child: Container(
              height: 30.0,
              decoration: BoxDecoration(
                color: Colors.white24,
                border: Border.all(color: Colors.grey, width: 2.0),
                image: DecorationImage(
                  image: AssetImage('assets/images/samples/movie.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ),
        Text(
          'SCREEN',
          style: TextStyle(color: Colors.white),
        ),
      ],
    );
  }

  Widget _arrangement(String letter, {SeatAlignment alignment: SeatAlignment.outside, int count: 4, int specialCount: 0, int numStart: 1}) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Row(
            mainAxisAlignment: alignment == SeatAlignment.inside ? MainAxisAlignment.end : alignment == SeatAlignment.outside ? MainAxisAlignment.start : MainAxisAlignment.center,
            children: List.generate(count, (index) {
              bool special = false;
              int startIndex = count - specialCount;
              if (index >= startIndex) {
                special = true;
              }
              return __seat(letter: letter, isSpecial: special, num: numStart + index);
            }),
          ),
        ),
        Text(
          letter.toUpperCase(),
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14.0),
        ),
        Expanded(
          child: Row(
            mainAxisAlignment: alignment == SeatAlignment.inside ? MainAxisAlignment.start : alignment == SeatAlignment.outside ? MainAxisAlignment.end : MainAxisAlignment.center,
            children: List.generate(count, (index) {
              bool special = false;
              int endIndex = specialCount - 1;
              if (index <= endIndex) {
                special = true;
              }
              return __seat(letter: letter, isSpecial: special, num: (numStart + count) + index);
            }),
          ),
        ),
      ],
    );
  }

  Widget __seat({bool isSpecial: false, int num: 0, String letter: 'a'}) {
    return Card(
      clipBehavior: Clip.hardEdge,
      child: InkWell(
        onTap: () {
          if (reserved.indexOf('$letter$num') < 0 && selectedSeats.indexOf('$letter$num') < 0) {
            setState(() {
              if (selectedSeats.length < widget.count) {
                selectedSeats.add('$letter$num');
              } else {
                selectedSeats.removeAt(0);
                selectedSeats.add('$letter$num');
              }
            });
            widget.onUpdate(selectedSeats);
          }
        },
        child: Container(
          color: reserved.indexOf('$letter$num') >= 0 ? Colors.blueGrey : selectedSeats.indexOf('$letter$num') >= 0 ? Colors.red : isSpecial ? Colors.amber : Colors.transparent,
          height: size,
          width: size,
          child: Center(
            child: Text(
              num.toString(),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
    );
  }
}

void functionArg(dynamic) {}
