import 'package:auto_route/auto_route.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../env/config.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_image.dart';

class TicketDetailsPage extends StatefulWidget {
  @override
  _TicketDetailsPageState createState() => _TicketDetailsPageState();
}

class _TicketDetailsPageState extends State<TicketDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Reservation Details'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            WImageWidget(
              placeholder: ExactAssetImage('assets/images/samples/movie.jpg'),
            ),
            _details(),
          ],
        ),
      ),
      bottomNavigationBar: Wrap(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: InkWell(
                  onTap: () {
                    Global.payAtCinema = false;
                    ExtendedNavigator.of(context).root.push('/cinemas/payment');
                  },
                  child: Container(
                    color: Colors.amber,
                    padding: EdgeInsets.all(16.0),
                    child: Center(
                        child: Text(
                      'USE CARD',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
                  ),
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {
                    Global.payAtCinema = true;
                    ExtendedNavigator.of(context).root.push('/cinemas/confirmation');
                  },
                  child: Container(
                    color: Colors.red,
                    padding: EdgeInsets.all(16.0),
                    child: Center(
                        child: Text(
                      'CASH ON ARRIVAL',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _details() {
    Widget __text(String text) {
      return Text(
        text,
        style: TextStyle(color: Colors.white),
      );
    }

    return Container(
      color: Config.primaryColor,
      width: double.infinity,
      padding: EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'DETAILS',
                style: TextStyle(fontSize: 16.0, color: Colors.blueGrey),
              ),
              Text(
                Global.money(160),
                style: TextStyle(fontSize: 16.0, color: Colors.amber, fontWeight: FontWeight.bold),
              ),
            ],
          ),
          SizedBox(height: 8.0),
          __text('iMax'),
          __text('APR 30, 2020 3:00 PM'),
          __text('Movie Title'),
          SizedBox(height: 8.0),
          _tickets(),
        ],
      ),
    );
  }

  Wrap _tickets() {
    return Wrap(
      spacing: 8.0,
      children: List.generate(2, (index) {
        return __ticket(index);
      }),
    );
  }

  Widget __ticket(int index) {
    return DottedBorder(
      color: Colors.red,
      child: Container(
        padding: EdgeInsets.all(4.0),
        child: Column(
          children: <Widget>[
            Text(
              'WPS${123 + index}',
              style: TextStyle(color: Colors.red),
            ),
            Text(
              'B${12 + index}',
              style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 18.0),
            ),
          ],
        ),
      ),
    );
  }
}
