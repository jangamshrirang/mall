import 'package:flutter/material.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              myAppBarContainer(),
              SizedBox(height: 10),
              Wrap(children: [
                servicesCard(),
                servicesCard(),
                servicesCard(),
                servicesCard(),
                servicesCard(),
                servicesCard(),
                servicesCard(),
                servicesCard(),
              ]),
            ],
          ),
        ),
      ),
    );
  }

  servicesCard() {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(
        left: 10,
        bottom: 10,
      ),
      height: size.height * 0.25,
      width: size.width * 0.9,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          text("                       September 13, 2020 | 4:34 PM", size.height * 0.018,
              Colors.black54, 1, FontWeight.normal, size.width,TextDecoration.none),
            SizedBox(height:5),
          text(
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisi lacus, aliquet eget tempus ut, pellentesque quis tellus. Sed posuere tincidunt urna in dignissim. Phasellus fringilla tincidunt velit, eget cursus nisl rutrum et. Ut condimentum ex et viverra dignissim. Pellentesque sem tellus, condimentum nec purus ut, auctor congue libero.",
              size.height * 0.018,
              Colors.black38,
              7,
              FontWeight.normal,
              size.width,TextDecoration.none),
        ],
      ),
    );
  }

  text(String text, double height, Color color, int lines,
      FontWeight fontWeight, double width,TextDecoration underline,) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: width,
      margin: EdgeInsets.only(left: 10, top: 5),
      child: Text(
        text,
        maxLines: lines,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: fontWeight,
            color: color,
            decoration: underline,
            fontSize: height),
      ),
    );
  }

  myAppBarContainer() {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.1,
      color: Color(0xff101f40),
      child: Row(
        children: [
          IconButton(
              icon: Icon(
                Icons.chevron_left,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context, false);
              }),
          Spacer(),
          Text("Notifications",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 25)),
          Spacer(),
          Spacer(),
        ],
      ),
    );
  }
}
