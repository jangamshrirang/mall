import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:great_circle_distance2/great_circle_distance2.dart';
import 'dart:math' show cos, sqrt, asin;

class DistanceCaluculator extends StatefulWidget {
  double lat1 = 25.275008;
  double lang1 = 51.537721;
  @override
  _DistanceCaluculatorState createState() => _DistanceCaluculatorState();
}

class _DistanceCaluculatorState extends State<DistanceCaluculator> {
  String _locationMessage = "";
  var lat, long;
  void _getCurrentLocation() async {
    final position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print(position);

    setState(() async {
      _locationMessage = "${position.latitude}, ${position.longitude}";
      lat = position.latitude;
      long = position.longitude;
      //openGoogleRoute(position.latitude,position.longitude);
      // _launchURL();

      double totalDistance = calculateDistance(
          position.latitude, position.longitude, 25.275008, 51.537721);
      print("formulaa         " + "$totalDistance");
    });
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.red,
    );
  }

  void distance() {}
}
