class OrderConfirmModel {
  OrderConfirmModel({
    this.orderHashId,
    this.deliveryAddress,
    this.latitude,
    this.longitude,
    this.user,
    this.store,
    this.contactNumber,
    this.landline,
    this.contactless,
    this.foodItems,
    this.orderNumber,
    this.totalAmount,
    this.paymentMethod,
  });

  String orderHashId;
  String deliveryAddress;
  String latitude;
  String longitude;
  User user;
  Store store;
  String contactNumber;
  String landline;
  String contactless;
  List<FoodItem> foodItems;
  String orderNumber;
  String totalAmount;
  String paymentMethod;

  factory OrderConfirmModel.fromJson(Map<String, dynamic> json) => OrderConfirmModel(
        orderHashId: json["order_hash_id"],
        deliveryAddress: json["delivery_address"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        user: User.fromJson(json["user"]),
        store: Store.fromJson(json["store"]),
        contactNumber: json["contact_number"],
        landline: json["landlind"],
        contactless: json["contactless"],
        foodItems: List<FoodItem>.from(json["food_items"].map((x) => FoodItem.fromJson(x))),
        orderNumber: json["order_number"],
        totalAmount: json["total_amount"],
        paymentMethod: json["payment_method"],
      );

  Map<String, dynamic> toJson() => {
        "order_hash_id": orderHashId,
        "delivery_address": deliveryAddress,
        "latitude": latitude,
        "longitude": longitude,
        "user": user.toJson(),
        "store": store.toJson(),
        "contact_number": contactNumber,
        "landlind": landline,
        "contactless": contactless,
        "food_items": List<dynamic>.from(foodItems.map((x) => x.toJson())),
        "order_number": orderNumber,
        "total_amount": totalAmount,
        "payment_method": paymentMethod,
      };
}

class FoodItem {
  FoodItem({
    this.name,
    this.price,
    this.quantity,
    this.hashId,
  });

  String name;
  String price;
  int quantity;
  String hashId;

  factory FoodItem.fromJson(Map<String, dynamic> json) => FoodItem(
        name: json["name"],
        price: json["price"],
        quantity: json["quantity"],
        hashId: json["hash_id"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "price": price,
        "quantity": quantity,
        "hash_id": hashId,
      };
}

class Store {
  Store({
    this.name,
    this.street,
    this.state,
    this.city,
    this.otherAddressInfo,
    this.contactNumber,
    this.contactEmail,
    this.latitude,
    this.longitude,
    this.hashid,
  });

  String name;
  String street;
  String state;
  String city;
  String otherAddressInfo;
  dynamic contactNumber;
  String contactEmail;
  String latitude;
  String longitude;
  String hashid;

  factory Store.fromJson(Map<String, dynamic> json) => Store(
        name: json["name"],
        street: json["street"],
        state: json["state"],
        city: json["city"],
        otherAddressInfo: json["other_address_info"],
        contactNumber: json["contact_number"],
        contactEmail: json["contact_email"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        hashid: json["hashid"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "street": street,
        "state": state,
        "city": city,
        "other_address_info": otherAddressInfo,
        "contact_number": contactNumber,
        "contact_email": contactEmail,
        "latitude": latitude,
        "longitude": longitude,
        "hashid": hashid,
      };
}

class User {
  User({
    this.name,
    this.email,
    this.mobile,
    this.hashId,
    this.currentAddress,
  });

  String name;
  dynamic email;
  String mobile;
  String hashId;
  CurrentAddress currentAddress;

  factory User.fromJson(Map<String, dynamic> json) => User(
        name: json["name"],
        email: json["email"],
        mobile: json["mobile"],
        hashId: json["hash_id"],
        currentAddress: CurrentAddress.fromJson(json["current_address"]),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "mobile": mobile,
        "hash_id": hashId,
        "current_address": currentAddress.toJson(),
      };
}

class CurrentAddress {
  CurrentAddress({
    this.id,
    this.addressTypeId,
    this.placeId,
    this.userId,
    this.nickname,
    this.street,
    this.state,
    this.city,
    this.area,
    this.house,
    this.additionalDirections,
    this.otherAddressInfo,
    this.mobile,
    this.landline,
    this.building,
    this.floor,
    this.apartmentNumber,
    this.office,
    this.latitude,
    this.longitude,
    this.deliveredIn,
    this.notes,
    this.isCurrent,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.hashid,
  });

  int id;
  int addressTypeId;
  dynamic placeId;
  int userId;
  String nickname;
  String street;
  dynamic state;
  dynamic city;
  String area;
  dynamic house;
  String additionalDirections;
  dynamic otherAddressInfo;
  String mobile;
  dynamic landline;
  String building;
  String floor;
  dynamic apartmentNumber;
  String office;
  String latitude;
  String longitude;
  String deliveredIn;
  dynamic notes;
  int isCurrent;
  int status;
  DateTime createdAt;
  DateTime updatedAt;
  String hashid;

  factory CurrentAddress.fromJson(Map<String, dynamic> json) => CurrentAddress(
        id: json["id"],
        addressTypeId: json["address_type_id"],
        placeId: json["place_id"],
        userId: json["user_id"],
        nickname: json["nickname"],
        street: json["street"],
        state: json["state"],
        city: json["city"],
        area: json["area"],
        house: json["house"],
        additionalDirections: json["additional_directions"],
        otherAddressInfo: json["other_address_info"],
        mobile: json["mobile"],
        landline: json["landline"],
        building: json["building"],
        floor: json["floor"],
        apartmentNumber: json["apartment_number"],
        office: json["office"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        deliveredIn: json["delivered_in"],
        notes: json["notes"],
        isCurrent: json["is_current"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        hashid: json["hashid"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "address_type_id": addressTypeId,
        "place_id": placeId,
        "user_id": userId,
        "nickname": nickname,
        "street": street,
        "state": state,
        "city": city,
        "area": area,
        "house": house,
        "additional_directions": additionalDirections,
        "other_address_info": otherAddressInfo,
        "mobile": mobile,
        "landline": landline,
        "building": building,
        "floor": floor,
        "apartment_number": apartmentNumber,
        "office": office,
        "latitude": latitude,
        "longitude": longitude,
        "delivered_in": deliveredIn,
        "notes": notes,
        "is_current": isCurrent,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "hashid": hashid,
      };
}
