import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/providers/socket/client.dart';
import 'package:wblue_customer/workground/chat/ping/p_ping.dart';

class PingPage extends StatefulWidget {
  @override
  _PingPageState createState() => _PingPageState();
}

class _PingPageState extends State<PingPage> {
  @override
  void initState() {
    super.initState();
    // Future.microtask(
    //     () => context.read<PPingStaff>().init(onSelectNotification));
    // socketHandler();
  }

  Future<void> onSelectNotification(String payload) {
    debugPrint("payload : $payload");

    showDialog(
      context: context,
      builder: (_) => new AlertDialog(
        title: new Text('Notification'),
        content: new Text('$payload'),
      ),
    );
  }

  // Future<void> socketHandler() async {
  //   context.read<PSocketClient>().emitterListener(
  //       eventName: WEvents.pingRestaurantStaff,
  //       onReceived: (data) async {
  //         await context.read<PPingStaff>().showNotification();
  //       });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        // onPressed: showNotificationTwo,
        onPressed: () {
          // showNotification();
          Map<String, dynamic> _data = {'hashid': 'MyHashId', 'table_no': '2'};
          context
              .read<PSocketClient>()
              .emitter(eventName: WEvents.pingRestaurantStaff, data: _data);
        },
        backgroundColor: Colors.green,
        child: Icon(MdiIcons.bell),
      ),
      body: Container(),
    );
  }
}
