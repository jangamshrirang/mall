import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/pages/booking/Flr_Table_Copy.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_restaurant_item.dart';
import 'package:wblue_customer/widgets/w_user_card.dart';

import 'ConformatinScreen.dart';
import 'Flare_Table_booking.dart';
import 'FloorPlan.dart';

class BookTablePage extends StatefulWidget {
  @override
  _BookTablePageState createState() => _BookTablePageState();
}

class _BookTablePageState extends State<BookTablePage> {
  UserModel user = Global.user;

  Widget _tile(
      {String title = '', String value = '', Function onPressed: _function}) {
    return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState2) {
      return ListTile(
        title: Text(title),
        trailing: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(value, style: TextStyle(color: Colors.grey, fontSize: 12.0)),
            Icon(Icons.chevron_right),
          ],
        ),
        onTap: () {
          onPressed();
        },
      );
    });
  }

  Map<String, dynamic> body = {
    'guest':
        stateManagment.guestNumber == null ? '1' : stateManagment.guestNumber,
    'date': '',
    'time': '',
    'Table': '',
  };

  Widget _listItem(int number) {
    bool same = false;
    return StatefulBuilder(
      builder: (BuildContext context, StateSetter setState2) {
        same = body['guest'] == number.toString();

        return InkWell(
          onTap: () {
            setState(() {
              setState2(() {
                body['guest'] = number.toString();
                stateManagment.setGuestnumber(number.toString());
                Navigator.pop(context);
              });
            });
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Icon(
                  same ? Icons.check_circle : Icons.radio_button_unchecked,
                  color: same ? Colors.amber : Colors.grey,
                ),
                SizedBox(width: 8.0),
                Text(
                  '${number.toString()} ${number <= 1 ? "GUEST" : 'GUESTS'}',
                  style: TextStyle(fontSize: 12.0),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    DateFormat dateFormatter = new DateFormat('EEE yyyy-MM-dd');
    int startHour = 7;
    int endHour = 23;

    TimeOfDay _getSetTime(TimeOfDay now) {
      DateFormat formatter = new DateFormat('yyyy-MM-dd');
      String bodyDate =
          body['date'] == '' ? formatter.format(DateTime.now()) : body['date'];
      String bodyTime = body['time'] == ''
          ? '${now.hour.toString().padLeft(2, '0')}:${now.minute.toString().padLeft(2, '0')}'
          : body['time'];

      TimeOfDay set =
          TimeOfDay.fromDateTime(DateTime.parse('$bodyDate $bodyTime'));
      return set;
    }

    _numberOfGuest() {
      var size = MediaQuery.of(context).size;
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Number of Guest'),
          content: Container(
            height: size.height * 0.5,
            child: Column(
              children: List.generate(8, (index) {
                return _listItem(index + 1);
              }),
            ),
          ),
          actions: [],
        ),
      );
//      Alert(
//        context: context,
//        title: 'Number of Guest',
//        content: Column(
//          children: List.generate(8, (index) {
//            return _listItem(index + 1);
//          }),
//        ),
//        buttons: [],
//        style: Global.alertStyle(),
//      ).show();
    }

    _selectDate() async {
      DateTime now = new DateTime.now().toLocal();
      DateTime date = await showDatePicker(
        context: context,
        initialDate: body['date'] == ''
            ? now.add(
                Duration(seconds: 5, days: now.hour >= (endHour - 1) ? 1 : 0))
            : DateTime.parse(body['date'])
                .toLocal()
                .add(Duration(hours: now.hour + 1)),
        firstDate:
            now.hour >= (endHour - 1) ? now : now.subtract(Duration(days: 1)),
        lastDate: now.add(Duration(days: 30)),
      );
      now = new DateTime.now().toLocal();
      date = date ??
          (body['date'] == ''
              ? now.add(Duration(seconds: 5))
              : DateTime.parse(body['date'])
                  .toLocal()
                  .add(Duration(hours: now.hour + 1)));
      setState(() {
        DateFormat formatter = new DateFormat('yyyy-MM-dd');
        body['date'] = formatter.format(date);
      });
    }

    _selectTime() async {
      TimeOfDay now = TimeOfDay.now();
      TimeOfDay set = _getSetTime(now);
      TimeOfDay time = await showTimePicker(
          context: context,
          initialTime: body['time'] == ''
              ? now.replacing(hour: now.hour + 1, minute: 0)
              : set);
      time = time ?? (body['time'] == '' ? now : set);
      if (time.hour < startHour) {
        time = time.replacing(hour: 7, minute: 0);
      }
      if (time.hour >= endHour) {
        time = time.replacing(hour: endHour - 1);
      }
      if (time.hour < now.hour) {
        if (time.minute >= 10) {
          time = time.replacing(hour: now.hour + 1, minute: 0);
        } else {
          time = time.replacing(hour: now.hour, minute: 30);
        }
      } else if (time.hour == now.hour) {
        if (now.minute >= 10) {
          time = time.replacing(hour: now.hour + 1, minute: 0);
        } else {
          time = time.replacing(hour: now.hour, minute: 30);
        }
      } else {
        if (time.minute < 30) {
          time = time.replacing(minute: 0);
        } else {
          time = time.replacing(minute: 30);
        }
      }
      setState(() {
        body['time'] =
            '${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}';
      });
    }

    _selectLocation() {
      bool indoor = body['location'] == 'indoor';
      bool outdoor = body['location'] == 'outdoor';

      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Location'),
          content: Column(
            children: List.generate(8, (index) {
              return _listItem(index + 1);
            }),
          ),
          actions: [],
        ),
      );

//      Alert(
//        context: context,
//        title: 'Location',
//        content: Column(
//          children: [
//            InkWell(
//              child: Padding(
//                padding: EdgeInsets.all(8.0),
//                child: Row(
//                  children: <Widget>[
//                    Icon(
//                      indoor ? Icons.check_circle : Icons.radio_button_unchecked,
//                      color: indoor ? Colors.amber : Colors.grey,
//                    ),
//                    SizedBox(width: 8.0),
//                    Text(
//                      'Indoor',
//                      style: TextStyle(fontSize: 12.0),
//                    ),
//                  ],
//                ),
//              ),
//              onTap: () {
//                setState(() {
//                  body['location'] = 'indoor';
//                  Navigator.pop(context);
//                });
//              },
//            ),
//            InkWell(
//              child: Padding(
//                padding: EdgeInsets.all(8.0),
//                child: Row(
//                  children: <Widget>[
//                    Icon(
//                      outdoor ? Icons.check_circle : Icons.radio_button_unchecked,
//                      color: outdoor ? Colors.amber : Colors.grey,
//                    ),
//                    SizedBox(width: 8.0),
//                    Text(
//                      'Outdoor',
//                      style: TextStyle(fontSize: 12.0),
//                    ),
//                  ],
//                ),
//              ),
//              onTap: () {
//                setState(() {
//                  body['location'] = 'outdoor';
//                  Navigator.pop(context);
//                });
//              },
//            ),
//          ],
//        ),
//        buttons: [],
//        style: Global.alertStyle(),
//      ).show();
    }

    return Scaffold(
      appBar: AppBar(
        // leading: IconButton(
        //     icon: Icon(Icons.chevron_left),
        //     onPressed: () {
        //       Navigator.pop(context, false);
        //       }),
        title: Text('Book A Table'),
        centerTitle: true,
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: ListView(
          children: <Widget>[
            WRestaurantTileWidget(),
            _tile(
              title: 'Number of guest',
              value: body['guest'],
              onPressed: () {
                _numberOfGuest();
              },
            ),
            _tile(
              title: 'Date',
              value: body['date'] == ''
                  ? ''
                  : dateFormatter.format(DateTime.parse(body['date'])),
              onPressed: () {
                _selectDate();
              },
            ),
            _tile(
              title: 'Time',
              value: body['time'] == ''
                  ? ''
                  : _getSetTime(TimeOfDay.now()).format(context),
              onPressed: () {
                _selectTime();
              },
            ),
            _tile(
              title: 'Table Booking',
              value: stateManagment.tableNumber == null
                  ? ""
                  : "${stateManagment.tableNumber}",
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => body['guest'] == "1"
                            ? RestTableBookingCopy()
                            : FloorPlan())); //
              },
            ),
            Divider(),
            WUserCardWidget(),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 14.0),
              child: TextField(
                maxLines: 3,
                minLines: 1,
                decoration: InputDecoration(
                  labelText: 'Your Instruction',
                ),
              ),
            ),
            SizedBox(height: 16.0),
            Center(
              child: FractionallySizedBox(
                widthFactor: 0.8,
                child: WButtonWidget(
                  title: 'place booking',
                  onPressed: () {
                    Global.cart = {};
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ConfirmationScreen(
                                  tableNumber: "${stateManagment.tableNumber}",
                                  date: body['date'],
                                  time: body['time'],
                                  guest: body['guest'],
                                )));
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

void _function() {}
