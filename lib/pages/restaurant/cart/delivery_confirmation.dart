import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_qr.dart';

class DeliveryConfirmationPage extends StatefulWidget {
  @override
  _DeliveryConfirmationPageState createState() =>
      _DeliveryConfirmationPageState();
}

class _DeliveryConfirmationPageState extends State<DeliveryConfirmationPage> {
  @override
  Widget build(BuildContext context) {
    TableRow _tableRow(String label, String value) {
      return TableRow(children: [
        TableCell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(label),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(value),
          ),
        ),
      ]);
    }

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'Restaurant Name',
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                  ),
                ),
                FractionallySizedBox(
                  widthFactor: 0.4,
                  child:
                      Image.asset('assets/images/samples/restaurant-logo.png'),
                ),
                SizedBox(height: 24.0),
                FractionallySizedBox(
                  widthFactor: 0.4,
                  child: Image.asset('assets/images/deliver.png'),
                ),
                Icon(
                  Icons.check_circle,
                  color: Colors.green,
                  size: 64.0,
                ),
                Text('Thank You for your Delivery Order'),
                SizedBox(height: 24.0),
                SizedBox(height: 8.0),
                FractionallySizedBox(
                  widthFactor: 0.8,
                  child: Table(
                    border: TableBorder.all(color: Colors.grey),
                    children: [
                      _tableRow('Ref Number', '#123'),
                      _tableRow('Special Instruction', 'Sample'),
                      _tableRow('Ordered Dish', 'QAR 500.00'),
                    ],
                  ),
                ),
                SizedBox(height: 24.0),
                FractionallySizedBox(
                  widthFactor: 0.3,
                  child: WButtonWidget(
                    title: 'Qr Code',
                    onPressed: () {
                      setState(() {
                        _showBarcode();
                      });
                    },
                  ),
                ),
                FractionallySizedBox(
                  widthFactor: 0.8,
                  child: Text(
                    'Kindly present the QR Code when your delivery driver arrived to you',
                    textAlign: TextAlign.center,
                  ),
                ),
                Divider(),
                FractionallySizedBox(
                  widthFactor: 0.7,
                  child: WButtonWidget(
                    title: 'Back to Home',
                    onPressed: () {
                      ExtendedNavigator.of(context).root.push('/');
                    },
                  ),
                ),
                Divider(),
                InkWell(
                  child: Text('Check Orders'),
                  onTap: () {
                    ExtendedNavigator.of(context).root.push('/reservations');
                  },
                ),
                SizedBox(height: 50.0),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showBarcode() {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            return Container(
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  )),
              child: Wrap(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Transaction Code',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      IconButton(
                        icon: Icon(Icons.close),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                  JQRWidget(
                    size: MediaQuery.of(context).size.width * 0.8,
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
