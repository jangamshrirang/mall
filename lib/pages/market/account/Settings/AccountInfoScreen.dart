import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wblue_customer/env/config.dart';
import 'dart:io';

import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

class AccountInfomrmationScreen extends StatefulWidget {
  AccountInfomrmationScreen() : super();

  @override
  _AccountInfomrmationScreenState createState() =>
      _AccountInfomrmationScreenState();
}

class _AccountInfomrmationScreenState extends State<AccountInfomrmationScreen> {
  Future<File> imageFile;

  pickImageFromGallery(ImageSource source) {
    setState(() {
      imageFile = ImagePicker.pickImage(source: source);
    });
  }

  Widget showImage() {
    var size = MediaQuery.of(context).size;
    return FutureBuilder<File>(
      future: imageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return CircleAvatar(
            backgroundImage: new FileImage(snapshot.data),
            radius: 50.0,
          );
        } else if (snapshot.error != null) {
          return const Text(
            'Error Picking Image',
            textAlign: TextAlign.center,
          );
        } else {
          return Container(
            margin: EdgeInsets.only(right: size.width * 0.05),
            child: IconButton(
                icon: Icon(
                  Icons.add_a_photo,
                  color: Colors.orange,
                ),
                onPressed: () {
                  setState(() {
                    pickImageFromGallery(ImageSource.gallery);
                  });
                }),
          );
        }
      },
    );
  }

  final nameController = TextEditingController();
  final mobileNumberController = TextEditingController();
  final emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "Account Information"),
      body: Container(
        margin: EdgeInsets.only(left: size.width * 0.2, top: 10),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Stack(
                children: [
                  showImage(),
                  Positioned(
                    top: size.height * 0.09,
                    left: size.width * 0.15,
                    child: IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                          color: Colors.orange,
                        ),
                        onPressed: () {
                          setState(() {
                            pickImageFromGallery(ImageSource.gallery);
                          });
                        }),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              textfield(
                nameController,
                "Name",
                "",
              ),
              SizedBox(height: size.height * 0.01),
              textfield(
                mobileNumberController,
                "Mobile Number",
                "",
              ),
              SizedBox(height: size.height * 0.01),
              textfield(
                emailController,
                "Email",
                "",
              ),
              SizedBox(height: size.height * 0.03),
              Container(
                height: size.height * 0.0415,
                child: RaisedButton(
                  onPressed: () {
                    // ExtendedNavigator.of(context).root.push('/restaurants/delivery-confirmation');
                    Navigator.pop(context, false);
                  },
                  color: Config.primaryColor,
                  textColor: Colors.white,
                  padding: const EdgeInsets.all(0.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0)),
                  child: Container(
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    padding: const EdgeInsets.fromLTRB(20, 05, 20, 05),
                    child: const Text('        Submit        ',
                        style: TextStyle(fontSize: 20)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  textfield(
    controller,
    String titelname,
    hintText,
  ) {
    var size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 15, bottom: 5),
          child: Text(
            titelname,
            style: TextStyle(
              fontSize: 16,
              // fontFamily: "SFUIText",
            ),
          ),
        ),
        Container(
          width: size.width * 0.65,
          height: size.height * 0.06,
          child: Card(
            color: Colors.white12,
            elevation: 0.0,
            child: TextFormField(
              controller: controller,
              cursorColor: Colors.black,
              maxLines: 1,
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
                fontWeight: FontWeight.w400,
              ),
              decoration: InputDecoration(
                filled: true,
                hintStyle: TextStyle(
                    color: Colors.black12, fontWeight: FontWeight.bold),
                fillColor: Colors.grey[200],
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.black, width: 1),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.black26, width: 1)),
                hintText: hintText,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
