import 'dart:collection';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/extensions/e_auth_user.dart';
import 'package:wblue_customer/models/Restaurant/food_cart.dart';
import 'package:wblue_customer/models/Restaurant/restaurant_cart_mode.dart';
import 'package:wblue_customer/models/map/map.dart';
import 'package:wblue_customer/models/order/order_confirm.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/providers/restaurant/cartActions.dart';
import 'package:wblue_customer/providers/socket/client.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/widgets/w_loading.dart';
import 'package:provider/provider.dart';

DioClient _dioClient = DioClient();

class PRestaurantFoodCart with ChangeNotifier {
  final double constDeliveryFee = 10.00;

  List<RestaurantFoodCartModel> _items = new List<RestaurantFoodCartModel>();
  UnmodifiableListView<RestaurantFoodCartModel> get items => UnmodifiableListView<RestaurantFoodCartModel>(_items);
  static List<Map<String, dynamic>> _addrTypes = [
    {"name": "house", "status": 1, "hashid": "Y2vXWzeN3rJzPmxMOVK4bB0y"},
    {"name": "office", "status": 1, "hashid": "zwjEe2kMDVq8PqG6vWbRr7Qx"},
    {"name": "appartment", "status": 1, "hashid": "ZRQ97PyD5AJ4PJ28eoMNYVbl"}
  ];

  String _restHashid;
  String get restHashid => _restHashid;

  String _restName;
  String get restName => _restName;

  bool _isFetchingAddr = true;
  bool get isFetchingAddr => _isFetchingAddr;

  String _restHashidInCart;
  String get restHashidInCart => _restHashidInCart;

  bool _isLoading = true;
  bool get isLoading => _isLoading;

  OrderConfirmModel orderConfirmModel;

  bool _isCheckOutSuccessful = true;
  bool get isCheckOutSuccessful => _isCheckOutSuccessful;

  RestaurantCartModel _restaurantCart = new RestaurantCartModel();
  RestaurantCartModel get restaurantCart => _restaurantCart;
  UserAddressModel _userAddressModel;
  String _address;

  // ignore: todo
  // TODO: Replace the delivery fee soon
  double _totalAmount = 0.00;
  double _deliveryFee = 10.00;

  double get totalAmount => _totalAmount;
  double get deliveryFee => _deliveryFee;

  String tableBookingDate;
  String tableBookingTime;
  DateTime tableBookingSched;
  int tableGuest = 1;

  void setRestaurantHashId({String hashid, String resName}) {
    print('hashid: $hashid resName : $resName');
    _restHashid = hashid;
    _restName = resName;
    notifyListeners();
  }

  Future<void> fetchRCartList() async {
    try {
      _restaurantCart = await fetchCart();

      if (_restaurantCart.food.length > 0) {
        _restHashidInCart = _restaurantCart.restaurant.hashid;
      }

      notifyListeners();
    } catch (e) {
      print('Method fetchRCartList throw error: $e');
    }
  }

  Future<RestaurantCartModel> fetchCart() async {
    try {
      setLoading(true);
      APIResponse res = await _dioClient.privatePost('/restaurant/cart/my-cart-list');
      setLoading(false);

      if (res.code == 200) {
        if (res.data == null) return new RestaurantCartModel(food: []);

        return RestaurantCartModel.fromJson(res.data);
      } else {
        throw Exception('Failed to load cart list');
      }
    } catch (e) {
      print('fetchCart Error: $e');
      return new RestaurantCartModel(food: []);
    }
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }

  void setCheckout(bool status) {
    _isCheckOutSuccessful = status;
    notifyListeners();
  }

  int getOrderType(WMallOrderType orderType) {
    if (orderType == WMallOrderType.dineIn)
      return 1;
    else if (orderType == WMallOrderType.delivery)
      return 2;
    else
      return 3;
  }

  Future<void> apiRestaurantAddToCart(
      {String restHashid,
      String foodId,
      int quantity,
      String specialRequest,
      WMallOrderType orderType,
      String reservationHashId,
      bool isReset: false,
      List<Map<String, dynamic>> variations: const []}) async {
    if (_restaurantCart.food.length > 0 && _restHashidInCart != restHashid) {
      if (isReset) {
        _restaurantCart.food.clear();
        await fetchRestaurantClearCart();
      } else {
        return;
      }
    }

    Map<String, dynamic> _body = {
      'food_id': foodId,
      'quantity': quantity == null ? 1 : quantity,
      //FIXME: MAKE IT DYNAMIC FOR DineIn = 1, DELIVERY = 2, TAKE AWAY = 3
      'order_type': getOrderType(orderType),
      'special_request': 'any',
      'has_booking': WMallOrderType.dineIn == orderType ? 1 : 0,
      'variations': variations,
      'reservation_hash_id': reservationHashId
    };

    try {
      APIResponse res = await _dioClient.privatePost('/restaurant/cart/add-to-cart', data: _body);

      print("Add to cart code: ${res.code}");
      print("Add to cart result:" + res.message);
      print("Add to cart data: ${res.data}");

      if (res.code == 200) {
        BotToast.showText(text: 'Item added successfully!');
        _restHashidInCart = restHashid;
        fetchRCartList();
      }

      print('fetchRestaurantAddToCart failed code: ${res.code}');
    } catch (r) {
      print('fetchRestaurantAddToCart Error: $r');
    }
  }

  Future<void> deductQty({RestaurantCartModel food, int index}) async {
    int _qty = food.food[index].quantity;
    _qty--;

    WLoading.type(Loading.show);
    await _updateCart(food, index, _qty);
    WLoading.type(Loading.dismiss);
  }

  Future<void> _updateCart(RestaurantCartModel food, int index, int qty) async {
    await updateCart(food.food[index].foodInfo.hashId, food.cartHashId, qty);
    await fetchRCartList();
    getTotalAmount();
  }

  Future<void> addQty({RestaurantCartModel food, int index}) async {
    int _qty = food.food[index].quantity;
    _qty++;

    WLoading.type(Loading.show);
    await _updateCart(food, index, _qty);
    WLoading.type(Loading.dismiss);
  }

  Future<void> getTotalAmount() async {
    _totalAmount = _deliveryFee + double.parse(_restaurantCart.totalInfo.total);
    notifyListeners();
  }

  Future<void> getCurrentLocation() async {
    try {
      _deliveryFee = await calculateDistance(
          destinationAddress: LatLng(
              double.parse(_restaurantCart.restaurant.latitude), double.parse(_restaurantCart.restaurant.longitude)),
          originAddress: LatLng(double.parse(_userAddressModel.latitude), double.parse(_userAddressModel.longitude)));

      getTotalAmount();
    } catch (e) {
      print('getCurrentLocation Error : $e');
    }
  }

  Future<void> getBasicAddress(Box basicAddress) async {
    Map _basicAddress = await basicAddress.get('basic-address');
    Map<String, dynamic> addr = new Map<String, dynamic>.from(_basicAddress);
    _userAddressModel = UserAddressModel.fromJson(addr);
  }

  Future<void> fetchLocalStorageAddress() async {
    try {
      setLoading(true);
      final currentAuthUser = Hive.box('auth');
      Map user = currentAuthUser.get('user');

      bool isLoggedIn = false;
      final _basicAddress = Hive.box('area');
      isLoggedIn = await currentAuthUser.get('isLoggedIn');

      if (user != null && isLoggedIn) {
        String hashId = user['hashid'];

        _userAddressModel = await hashId.address();
        if (_userAddressModel.hashid == null) {
          await getBasicAddress(_basicAddress);
          setLoading(false);
          return;
        }
      } else {
        await getBasicAddress(_basicAddress);
      }

      setLoading(false);
    } catch (e) {
      print('Method fetchLocalStorageAddress throw Error: $e');
    }
  }

  setFetchAddr(bool status) {
    _isFetchingAddr = status;
    notifyListeners();
  }

  Future<void> getAddress() async {
    setFetchAddr(true);
    await fetchLocalStorageAddress();
    getCurrentLocation();

    if (_userAddressModel.type == _addrTypes[0]['name']) {
      _address = '${_userAddressModel.street}, ${_userAddressModel.house}';
    } else if (_userAddressModel.type == _addrTypes[1]['name']) {
      _address =
          '${_userAddressModel.street}, ${_userAddressModel.building}, ${_userAddressModel.floor}, ${_userAddressModel.office}';
    } else if (_userAddressModel.type == _addrTypes[2]['name']) {
      _address =
          '${_userAddressModel.street}, ${_userAddressModel.building}, ${_userAddressModel.floor}, ${_userAddressModel.apartmentNumber}';
    }
    setFetchAddr(false);
  }

  void setTableSched(DateTime date) {
    try {
      tableBookingSched = date;
      tableBookingTime = DateFormat.Hm().format(tableBookingSched);
      tableBookingDate = DateFormat('yyyy-MM-dd').format(tableBookingSched);
      notifyListeners();
    } catch (e) {
      print('setTableTime Error: $e');
    }
  }

  void setTableGuest(int guest) {
    tableGuest = guest;
    notifyListeners();
  }

  void resetSched() {
    tableGuest = 1;
    tableBookingSched = null;
    notifyListeners();
  }

  Future<void> placeORderApi(int payment, BuildContext context, {String generalNotes: ''}) async {
    try {
      Map<String, dynamic> _body = {
        'payment_method_id': payment,
        'shipping_address': _address,
        'shipping_rate': _deliveryFee,
        'shipping_address_latitude': _userAddressModel.latitude,
        'shipping_address_longitude': _userAddressModel.longitude,
        'note': generalNotes,
        'time_arrive': tableBookingTime,
        'date_arrive': tableBookingDate,
        'guest': tableGuest,
      };

      setCheckout(false);
      APIResponse res = await _dioClient.privatePost('/restaurant/order/place', data: _body);
      setCheckout(true);
      if (res.code == 200) {
        _restaurantCart = RestaurantCartModel(food: [], totalInfo: RestaurantCartTotalInfoModel());

        orderConfirmModel = OrderConfirmModel.fromJson(res.data);
        print("order place scoket working fine");
        context.read<PSocketClient>().emitter(eventName: WEvents.checkoutRestaurentHashId, data: {
          'order_type': "Deliery",
          'restaurent_hashid': stateManagment.rHashId,
        });
        notifyListeners();
      }
    } catch (e) {
      print('Error in: $e');
    }
  }

  Future<double> calculateDistance({LatLng destinationAddress, LatLng originAddress}) async {
    const int flatLimit = 5;
    const int flatLimitPrice = 10;
    double total = 0.0;

    // originAddress = LatLng(25.267601102297643, 51.54444146901369);
    // destinationAddress = LatLng(25.221738558508594, 51.51607610285282);

    Map<String, dynamic> params = {
      'units': 'metric',
      'origins': '${originAddress.latitude},${originAddress.longitude}',
      'destinations': '${destinationAddress.latitude},${destinationAddress.longitude}',
      'key': Config.googleMapAPI,
    };

    final response =
        await DioClient().randomGet('https://maps.googleapis.com/maps/api/distancematrix/json', data: params);

    MapModel mapModel = MapModel.fromJson(response);

    if (mapModel.status == 'OK') {
      double totalKm = (mapModel.rows[0].elements[0].distance.value / 1000);
      int _totalkm = totalKm.floor();

      if (_totalkm >= flatLimit) {
        total = _totalkm.toDouble() - flatLimit;
        total = total * 0.50;
      }

      return total += flatLimitPrice;
    } else {
      throw ('Location Error');
    }
  }
}
