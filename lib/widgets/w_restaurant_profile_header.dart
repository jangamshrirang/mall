import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/R_ListByRatings.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_cart.dart';

class WRestaurantProfileHeaderWidget extends StatefulWidget {
  final RestaurantListModel restaurantListModel;
  final int restindex;
  final Key key;
  const WRestaurantProfileHeaderWidget(
      {this.restindex, this.restaurantListModel, this.key});

  @override
  _WRestaurantProfileHeaderWidgetState createState() =>
      _WRestaurantProfileHeaderWidgetState();
}

class _WRestaurantProfileHeaderWidgetState
    extends State<WRestaurantProfileHeaderWidget> {
  bool fav = stateManagment.restFav;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.3,
      child: Stack(
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: widget.restaurantListModel?.fullCover ?? '',
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                  alignment: Alignment.center,
                ),
                // borderRadius: BorderRadius.circular(5.0),
              ),
              child: Container(
                color: Colors.black54,
              ),
            ),
            placeholder: (context, url) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                      widget.restaurantListModel?.thumbnailCover ?? ''),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(5.0),
              ),
            ),
          ),
          Positioned.fill(
            child: Align(
                alignment: Alignment.center,
                child: Container(
                  width: 0.8.sw,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.all(1.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4.0),
                          color: Colors.white,
                        ),
                        height: 60,
                        width: 60,
                        child: CachedNetworkImage(
                          imageUrl: widget.restaurantListModel?.full ?? '',
                          imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.circular(4.0),
                            ),
                          ),
                          placeholder: (context, url) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                    widget.restaurantListModel?.thumbnail ??
                                        ''),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.circular(4.0),
                            ),
                          ),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      AutoSizeText(
                        '${widget.restaurantListModel.restaurant}',
                        maxLines: 5,
                        style: TextStyle(
                          fontFamily: Config.fontFamily,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                        minFontSize: 48.sp,
                        stepGranularity: 48.sp,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: 0.45.sw,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Row(
                              children: [
                                Icon(
                                  MdiIcons.star,
                                  color: Colors.white,
                                  size: 16,
                                ),
                                SizedBox(
                                  width: 3,
                                ),
                                AutoSizeText('4.5',
                                    style: TextStyle(color: Colors.white)),
                                SizedBox(
                                  width: 3,
                                ),
                                AutoSizeText('(500)',
                                    style: TextStyle(color: Colors.white)),
                              ],
                            ),
                            Row(
                              children: [
                                Icon(
                                  MdiIcons.heart,
                                  color: Colors.white,
                                  size: 16,
                                ),
                                SizedBox(
                                  width: 3,
                                ),
                                AutoSizeText('(2k)',
                                    style: TextStyle(color: Colors.white)),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 0.02.sw),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FlatButton(
                            height: 0.05.sw,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                side: BorderSide(color: Colors.white)),
                            textColor: Colors.white,
                            padding: EdgeInsets.all(8.0),
                            onPressed: () async {
                              bool canNav = await ExtendedNavigator.of(context)
                                  .root
                                  .canNavigate('/restaurants/cart');

                              // denied
                              if (!canNav) return;
                            },
                            child: Text(
                              'FOLLOW',
                              style: TextStyle(
                                fontSize: 12.0,
                              ),
                            ),
                          ),
                          SizedBox(width: 20),
                          FlatButton(
                            height: 20,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                side: BorderSide(color: Colors.white)),
                            textColor: Colors.white,
                            padding: EdgeInsets.all(8.0),
                            onPressed: () {
                              ExtendedNavigator.of(context).root.push(
                                  '/restaurans/chats/room',
                                  arguments: RestaurantMessagePageArguments(
                                      restaurantName:
                                          widget.restaurantListModel.restaurant,
                                      restaurantHashid:
                                          widget.restaurantListModel.hashId));
                            },
                            child: Text(
                              'MESSAGE',
                              style: TextStyle(
                                fontSize: 12.0,
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                )),
          ),
          Container(
            margin: EdgeInsets.only(top: size.height * 0.05),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      left: size.width * 0.04, top: size.height * 0.01),
                  decoration: BoxDecoration(
                      color: Colors.white70, shape: BoxShape.circle),
                  child: IconButton(
                    icon: Icon(
                      Icons.keyboard_backspace,
                      color: Config.primaryColor,
                    ),
                    onPressed: () async {
                      ExtendedNavigator.of(context).pop();

                      // context
                      //     .read<PBrowseRestaurant>()
                      //     .swiperControl
                      //     .startAutoplay();
                      // // bool res = onCancelTransaction(context);
                      // if (Global.cart.length > 0) {
                      //   bool res = await onCancelTransaction(context);
                      //   if (res) context.navigator.pop();
                      // } else {
                      //   context.navigator.pop();
                      // }
                    },
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(
                        right: size.width * 0.04, top: size.height * 0.01),
                    decoration: BoxDecoration(
                        color: Colors.white70, shape: BoxShape.circle),
                    child: WCartWidget(
                      color: Config.primaryColor,
                    )),
              ],
            ),
          ),
          // Container(
          //   margin: EdgeInsets.only(
          //       top: size.height * 0.38, left: size.width * 0.04, bottom: 5),
          //   height: size.height * 0.12,
          //   // color: Colors.red,
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //     children: [
          //       Text(
          //         '${widget.restName}',
          //         style: TextStyle(
          //           color: Colors.black,
          //           fontFamily: Config.fontFamily,
          //           fontWeight: FontWeight.bold,
          //           fontSize: size.height * 0.03,
          //         ),
          //       ),
          //       Text(
          //         '${widget.restSlogan}',
          //         style: TextStyle(
          //           color: Colors.black54,
          //           fontFamily: Config.fontFamily,
          //           fontWeight: FontWeight.normal,
          //           fontSize: size.height * 0.02,
          //         ),
          //       ),
          //       WRatingsWidget(3.5, size: 15.0),
          //       Row(
          //         children: [
          //           Text(
          //             "1.12k",
          //             style: TextStyle(
          //               color: Colors.black54,
          //               fontFamily: Config.fontFamily,
          //               fontWeight: FontWeight.normal,
          //               fontSize: 16.0,
          //             ),
          //           ),
          //           fav == false || fav == null
          //               ? GestureDetector(
          //                   onTap: () {
          //                     setState(() {
          //                       fav = true;
          //                       stateManagment.setRestFave(true);
          //                     });
          //                   },
          //                   child: Icon(
          //                     Icons.favorite_border,
          //                     color: Colors.black54,
          //                     size: 20,
          //                   ),
          //                 )
          //               // IconButton(
          //               //   padding: EdgeInsets.all(0),
          //               //     icon: Icon(Icons.favorite_border,
          //               //         size: 20, color: Colors.black38),
          //               //     onPressed: () {
          //               // setState(() {
          //               //   fav = true;
          //               // });
          //               //     })
          //               : GestureDetector(
          //                   onTap: () {
          //                     setState(() {
          //                       fav = false;
          //                       stateManagment.setRestFave(null);
          //                     });
          //                   },
          //                   child:
          //                       Icon(Icons.favorite, size: 20, color: Colors.red),
          //                 )
          //         ],
          //       )
          //     ],
          //   ),
          // ),
          // Container(
          //   margin:
          //       EdgeInsets.only(top: size.height * 0.3, left: size.width * 0.05),
          //   // color: Colors.red,
          //   height: size.height * 0.02,
          //   width: size.width * 0.3,
          //   child: Text(
          //     'QWEQWEQWEQW',
          //     textAlign: TextAlign.start,
          //     style: TextStyle(color: Colors.white, fontSize: 12.0),
          //   ),
          // )
        ],
      ),
    );
  }

  Future<bool> onCancelTransaction(BuildContext context) async {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text('No'),
      onPressed: () => context.navigator.pop(false),
    );
    Widget continueButton = FlatButton(
        child: Text('Yes'),
        onPressed: () {
          Global.cart = {};
          context.navigator.pop(true);
        });

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text('CONFIRMATION'),
      content: Text('Are you sure to cancel this transaction?'),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
