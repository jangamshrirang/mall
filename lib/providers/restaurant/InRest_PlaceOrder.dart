
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();
Future inRestplaceORder(
    var shippingRate, paymentMethod, lat, lang, orderType, note) async {
  Map<String, dynamic> _body = {
    'shipping_rate': 0,
    'payment_method_id': paymentMethod,
    'shipping_address': 'qatar',
    'shipping_address_coordinates': '{lat: $lat, lng: $lang}',
    'order_type': orderType,
    // stateManagment.tableNumber == null ? 0 : stateManagment.tableNumber,
    'time_arrive': stateManagment.time == null ? "any" : stateManagment.time,
    'date_arrive': stateManagment.date == null ? "any" : stateManagment.date,
    'guest':
        stateManagment.guestNumber == null ? "any" : stateManagment.guestNumber,
    'note': "any"
  };
  APIResponse res =
      await _dioClient.privatePost('/restaurant/order/place', data: _body);
  if (res.code >= 400) {
    return [];
  }
  print("data from place order api:${res.data}");
  return res.data;
}