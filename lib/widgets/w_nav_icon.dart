import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WNavIconWidget extends StatefulWidget {
  final String name;
  final IconData iconData;
  final Function onTap;

  WNavIconWidget(
    this.name,
    this.iconData, {
    this.onTap: _function,
  });
  @override
  _WNavIconWidgetState createState() => _WNavIconWidgetState();
}

class _WNavIconWidgetState extends State<WNavIconWidget> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return InkWell(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Icon(
              widget.iconData,
              color: Colors.black54,
              size: 20.0,
            ),
            SizedBox(height: 4.0),
            Text(
              widget.name.toUpperCase(),
              style: TextStyle(
                fontWeight: FontWeight.w300,
                color: Colors.grey,
                fontSize: size.height * 0.018,
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        widget.onTap();
      },
    );
  }
}

void _function() {}
