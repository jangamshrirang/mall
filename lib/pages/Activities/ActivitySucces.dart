import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/pages/restaurant/restaurant_list.dart';
import 'package:wblue_customer/widgets/navigateScreen.dart';
import 'package:wblue_customer/widgets/w_qr.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ActivityBookingSuccess extends StatefulWidget {
  String from;
  ActivityBookingSuccess({this.from});
  @override
  _ActivityBookingSuccessState createState() => _ActivityBookingSuccessState();
}

class _ActivityBookingSuccessState extends State<ActivityBookingSuccess> {
  int count = 0;
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
          // appBar: appBarWithOutIcon(context, ""),
          body: Center(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SafeArea(
                    child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        ExtendedNavigator.of(context)
                            .root
                            .replace('/restaurants');
                      }),
                )),
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("assets/activities/celeb2.png"),
                    Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: AutoSizeText(
                          stateManagment.fromReservation == null ||
                                  stateManagment.fromReservation == false
                              ? 'Thank you for purchasing with us!'
                              : "",
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.w500)),
                    ),
                    SizedBox(height: 5.0),
                    FractionallySizedBox(
                        widthFactor: 0.5,
                        child: RaisedButton(
                          onPressed: () {
                            setState(() {
                              _showBarcode();
                            });
                          },
                          color: Config.primaryColor,
                          textColor: Colors.white,
                          child: Text('SHOW QR CODE'),
                        )),
                    GestureDetector(
                      child: stateManagment.fromReservation == null ||
                              stateManagment.fromReservation == false
                          ? Padding(
                              padding: const EdgeInsets.all(25.0),
                              child: Text(
                                "You can see your booking in \nReservations menu drawer.",
                                style: TextStyle(
                                    fontSize: 32.sp, color: Colors.black54),
                              ),
                            )
                          : Text("Hope you enjoyed our food . \b BACK TO HOME"),
                      onTap: () {
                        // widget.from == "rest"
                        //     ? navigate(context, Restaurantlist())
                        //     : Navigator.of(context)
                        //         .popUntil((_) => count++ >= 6);

                        widget.from == "rest"
                            ? navigate(context, Restaurantlist())
                            : ExtendedNavigator.of(context)
                                .root
                                .replace('/restaurants');

                        stateManagment.setFromReservastion(null);
                      },
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }

  void _showBarcode() {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            return Container(
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  )),
              child: Wrap(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        ' Confirmation code \n Show to staff',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      IconButton(
                        icon: Icon(Icons.close),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                  JQRWidget(
                    size: MediaQuery.of(context).size.width * 0.8,
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
