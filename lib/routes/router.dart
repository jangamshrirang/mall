import 'package:auto_route/auto_route.dart';
import 'package:auto_route/auto_route_annotations.dart';
import 'package:wblue_customer/pages/Activities/ActivityCart.dart';
import 'package:wblue_customer/pages/Activities/ActivityHome.dart';
import 'package:wblue_customer/pages/Activities/ActivitySucces.dart';
import 'package:wblue_customer/pages/MyOrders/OrderList.dart';
import 'package:wblue_customer/pages/auth/login.dart';
import 'package:wblue_customer/pages/auth/otp.dart';
import 'package:wblue_customer/pages/auth/registration.dart';
import 'package:wblue_customer/pages/booking/ConformatinScreen.dart';
import 'package:wblue_customer/pages/booking/book_table.dart';
import 'package:wblue_customer/pages/booking/booking_confirmation.dart';
import 'package:wblue_customer/pages/coming_soon.dart';
import 'package:wblue_customer/pages/hotel/hotel_confirmation.dart';
import 'package:wblue_customer/pages/hotel/hotel_payment.dart';
import 'package:wblue_customer/pages/hotel/hotels.dart';
import 'package:wblue_customer/pages/hotel/review_booking.dart';
import 'package:wblue_customer/pages/hotel/single_hotel.dart';
import 'package:wblue_customer/pages/landing.dart';
import 'package:wblue_customer/pages/locations/location.dart';
import 'package:wblue_customer/pages/market/Brands/Brands.dart';
import 'package:wblue_customer/pages/market/account/Wallet/TransationScreen.dart';
import 'package:wblue_customer/pages/market/account/Wallet/wallet.dart';
import 'package:wblue_customer/pages/market/account/acount.dart';
import 'package:wblue_customer/pages/market/category/category.dart';
import 'package:wblue_customer/pages/market/detector/camera.dart';
import 'package:wblue_customer/pages/market/detector/detector_copy.dart';
import 'package:wblue_customer/pages/market/feed/feed.dart';
import 'package:wblue_customer/pages/market/home/home.dart';
import 'package:wblue_customer/pages/market/home/live.dart';
import 'package:wblue_customer/pages/market/home/live/host.dart';
import 'package:wblue_customer/pages/market/home/live/join.dart';
import 'package:wblue_customer/pages/market/home/tab/live/create_live_content.dart';
import 'package:wblue_customer/pages/market/hot_deals.dart';
import 'package:wblue_customer/pages/market/market_main.dart';
import 'package:wblue_customer/pages/market/order/cart/cart.dart';
import 'package:wblue_customer/pages/market/order/payment.dart';
import 'package:wblue_customer/pages/market/order/shipping_billing.dart';
import 'package:wblue_customer/pages/market/order/success_purchased.dart';
import 'package:wblue_customer/pages/market/order/track-order.dart';
import 'package:wblue_customer/pages/market/search.dart';
import 'package:wblue_customer/pages/market/view_other_product.dart';
import 'package:wblue_customer/pages/market/view_product.dart';
import 'package:wblue_customer/pages/market/w_fullscreen_image.dart';
import 'package:wblue_customer/pages/payment/payment.dart';
import 'package:wblue_customer/pages/reservation/reservation_details.dart';
import 'package:wblue_customer/pages/reservation/reservation_timeline.dart';
import 'package:wblue_customer/pages/reservation/reservations.dart';
import 'package:wblue_customer/pages/restaurant/cart/checkout/change_delivery_address.dart';
import 'package:wblue_customer/pages/restaurant/cart/checkout/change_delivery_time.dart';
import 'package:wblue_customer/pages/restaurant/cart/checkout/main.dart';
import 'package:wblue_customer/pages/restaurant/cart/checkout/map.dart';
import 'package:wblue_customer/pages/restaurant/cart/checkout/payment-method.dart';
import 'package:wblue_customer/pages/restaurant/cart/d8_soon_pickup_confirmation.dart';
import 'package:wblue_customer/pages/restaurant/cart/food_cart.dart';
import 'package:wblue_customer/pages/restaurant/orders/view_order.dart';
import 'package:wblue_customer/pages/restaurant/restaurant_list.dart';
import 'package:wblue_customer/pages/restaurant/cart/cart.dart';
import 'package:wblue_customer/pages/restaurant/cart/delivery_confirmation.dart';
import 'package:wblue_customer/pages/restaurant/cart/food_order_confirmation.dart';
import 'package:wblue_customer/pages/restaurant/message.dart';
import 'package:wblue_customer/pages/restaurant/store/main.dart';
import 'package:wblue_customer/pages/restaurant/store/restCatogries.dart';
import 'package:wblue_customer/pages/restaurant/store/variation.dart';
import 'package:wblue_customer/pages/restaurant/store/single_restaurant.dart';
import 'package:wblue_customer/pages/unknown_route.dart';
import 'package:wblue_customer/pages/user/profile.dart';
import 'package:wblue_customer/routes/route_guards.dart';
import 'package:wblue_customer/workground/chat/message.dart';
import 'package:wblue_customer/workground/chat/room.dart';

@MaterialAutoRouter(
  generateNavigationHelperExtension: true,
  routes: <AutoRoute>[
    AdaptiveRoute(page: LandingPage, initial: true, fullscreenDialog: true),

    AdaptiveRoute(page: LoginPage, path: '/login', fullscreenDialog: true),
    AdaptiveRoute(
      path: '/registration',
      page: RegistrationPage,
    ),
    AdaptiveRoute<bool>(path: '/otp', page: OtpPage, fullscreenDialog: true),

    //location
    AdaptiveRoute<bool>(page: LocationPage, path: '/location', fullscreenDialog: true),

    // RESTAURANT
    AdaptiveRoute(path: '/restaurants', page: Restaurantlist, guards: [LocGuard]),
    AdaptiveRoute(path: '/restaurants/view/order', page: RestaurantViewOrderPage),

    CustomRoute<String>(
        path: '/restaurants/single',
        page: SingleRestaurantPage,
        name: 'restaurantsScreen',
        transitionsBuilder: TransitionsBuilders.slideBottom,
        durationInMilliseconds: 200,
        children: <AutoRoute>[
          AdaptiveRoute(path: '/', page: SingeRestaurantMain),
        ]),

    CustomRoute<String>(
        path: '/restaurants/menu', page: RestCatogries, transitionsBuilder: TransitionsBuilders.slideBottom),

    CustomRoute(path: '/variation', page: RestaurantFoodVariation, transitionsBuilder: TransitionsBuilders.slideBottom),

    CustomRoute(
        path: '/restaurant-food-cart',
        page: RestaurantFoodCart,
        durationInMilliseconds: 200,
        transitionsBuilder: TransitionsBuilders.slideBottom,
        guards: [AuthGuard]),

    AdaptiveRoute(path: '/restaurans/chats/room', page: RestaurantMessagePage, guards: [AuthGuard]),
    AdaptiveRoute(path: '/restaurants/cart', page: RestaurantCartPage, guards: [AuthGuard]),

    // -- CHECKOUT PAGES  -- //
    AdaptiveRoute(path: '/restaurants/place-order', page: RestaurantPlaceOrderPage),
    AdaptiveRoute(path: '/select-address', page: RestaurantCheckoutAddressPage),
    AdaptiveRoute(path: '/open-map', page: SelectDeliveryAddressPage),
    AdaptiveRoute(path: '/select-delivery-time', page: RestaurantCheckoutDeliveryPage),
    AdaptiveRoute(path: '/select-payment-method', page: RestaurantCheckoutPaymentPage),
    // -- END OF CHECKOUT PAGES  -- //
    // -- ORDER -- //

    AdaptiveRoute(path: '/restaurants/my-order', page: MyordeList, guards: [AuthGuard]),

    // -- ORDER -- //

    // -- RESERVATION PAGES -- //
    AdaptiveRoute(path: '/reservations', page: ReservationPage, guards: [AuthGuard]),
    AdaptiveRoute(path: '/reservations/details', page: ReservationDetailsPage),
    AdaptiveRoute(path: '/reservations/timeline', page: ReservationTimelinePage),
    // -- END RESERVATION -- //

    AdaptiveRoute(path: '/restaurants/order-confirmation', page: RestaurantOrderConfirmation, fullscreenDialog: true),
    AdaptiveRoute(path: '/restaurants/pickup-confirmation', page: PickupConfirmationPage),
    AdaptiveRoute(path: '/delivery-confirmation', page: DeliveryConfirmationPage),
    AdaptiveRoute(path: '/restaurants/book-table', page: BookTablePage, guards: [AuthGuard]),
    AdaptiveRoute(path: '/restaurants/book/table/confirmation', page: ConfirmationScreen, fullscreenDialog: true),
    AdaptiveRoute(path: '/restaurants/book/table/complete', page: ActivityBookingSuccess, fullscreenDialog: true),

    AdaptiveRoute(
      path: '/booking-confirmation',
      page: BookingConfirmationPage,
    ),

    AdaptiveRoute(path: '/hotels', page: HotelPage),
    AdaptiveRoute(path: '/hotels/single', page: SingleHotelPage),
    AdaptiveRoute(path: '/hotels/payment', page: HotelPaymentPage),
    AdaptiveRoute(path: '/hotels/confirmation', page: HotelConfirmationPage),

    AdaptiveRoute(path: '/hotels/review-booking', page: HotelReviewBookingPage, guards: [AuthGuard]),

    AdaptiveRoute(path: '/activity', page: ActivityHome),
    AdaptiveRoute(path: '/activity/cart', page: ActivityCart, guards: [AuthGuard]),

    // AdaptiveRoute(
    //   path: '/cinemas',
    //   page: ActivityHome, //CinemaMainPage,
    //   children: <AutoRoute>[
    //     AdaptiveRoute(path: '/', page: ActivityHome), //CinemaPage),
    //     AdaptiveRoute(path: '/single', page: SingleCinemaPage),
    //     AdaptiveRoute(path: '/movie-details', page: MovieDetailsPage),
    //     AdaptiveRoute(
    //         path: '/movie-fullscreen-image', page: FullScreenPhotoPage),
    //     AdaptiveRoute(path: '/select-cinema-seat', page: SelectCinemaSeatPage),
    //     AdaptiveRoute(path: '/ticket-details', page: TicketDetailsPage),
    //     AdaptiveRoute(path: '/payment', page: CinemaPaymentPage),
    //     AdaptiveRoute(path: '/confirmation', page: CinemaConfirmationPage),
    //   ],
    // ),

    AdaptiveRoute(
      path: '/market',
      page: MainMarketPage,
      children: <AutoRoute>[
        AdaptiveRoute(path: '/', page: HomeMarketPage),
        AdaptiveRoute(path: '/feed', page: FeedMarketPage),
        AdaptiveRoute(path: '/message', page: BrandsPage),
      ],
    ),

    AdaptiveRoute(
      path: '/market/cart/items',
      page: CartMarketPage,
      guards: [AuthGuard],
    ),
    AdaptiveRoute(path: '/market/account', page: AccountMarketPage, guards: [AuthGuard]),
    AdaptiveRoute(path: '/user', page: UserProfile, guards: [AuthGuard]),
    AdaptiveRoute(path: '/chats', page: MessagePage),
    AdaptiveRoute(path: '/chats/room', page: MessageRoomPage, guards: [AuthGuard]),

    AdaptiveRoute(path: '/market/user/wallet', page: MarketWalletPage, guards: [AuthGuard]),
    AdaptiveRoute(path: '/market/live', page: LiveMarketPage),

    AdaptiveRoute(path: '/market/live/host', page: CallPage, fullscreenDialog: true),
    AdaptiveRoute(path: '/market/live/join', page: JoinPage, guards: [AuthGuard], fullscreenDialog: true),

    AdaptiveRoute(path: '/market/live/content', page: CreateLiveContentPage, guards: [AuthGuard]),

    // AdaptiveRoute(path: '/market/go-to-cart/:isBack?', page: CartMarketPage),
    AdaptiveRoute(path: '/market/search-product', page: MarketSearchProduct),
    AdaptiveRoute(path: '/market/payment', page: MarketCartPayment),
    AdaptiveRoute(path: '/market/shipping-billing', page: ShippingBillingPage),
    AdaptiveRoute(path: '/market/order-success', page: MarketSuccessPurchasedPage),
    AdaptiveRoute(path: '/market/order-track', page: OrderTrackPage),
    AdaptiveRoute(path: '/market/hot-deals', page: HotDealsPage),
    AdaptiveRoute(path: '/market-camera-recognizer', page: MarketCameraPage),
    AdaptiveRoute(path: '/market-camera', page: ProductRecognizerPage),
    AdaptiveRoute(path: '/product-images', page: WProductImage),
    AdaptiveRoute(path: '/view-product', page: ViewProduct),
    AdaptiveRoute(path: '/view-other-product', page: ViewOtherProduct),
    AdaptiveRoute(path: '/market-categories', page: CategoryPage),
    AdaptiveRoute(path: '/wmall-payment', page: WmallPayment),
    AdaptiveRoute(path: '/coming-soon', page: ComingSoonPage),

    // Account => Wallet Screens
    AdaptiveRoute(path: '/wallet-transcations', page: TransationScreen),
    AdaptiveRoute(path: "*", page: UnknownRouteScreen),

    //Payment
  ],
)
class $AppRouter {}
