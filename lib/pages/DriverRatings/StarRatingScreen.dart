import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

import 'ReasonforRatiingScreen.dart';

class DriverStarRatingScreen extends StatefulWidget {
  @override
  _DriverStarRatingScreenState createState() => _DriverStarRatingScreenState();
}

class _DriverStarRatingScreenState extends State<DriverStarRatingScreen> {
  List<String> starRatings = [
    "    ",
    "5 - Excellent",
    "4 - Good",
    "3 - neutral",
    "2 - Poor",
    "1 - Awflu"
  ];
  int secondaryIndex = 0;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBarWithOutIcon(context, "Delivery Survey"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: FractionallySizedBox(
                widthFactor: 0.3,
                child: Image.asset('assets/images/logos/wmall-512.png'),
              ),
            ),
            text(
                "Thank you for shopping with Wmall.We would like to understand more about your delivery experiene !",
                FontWeight.w700),
            SizedBox(height: 20),
            text("How Would rate your experience with the delivery courier ?",
                FontWeight.normal),
            SizedBox(height: 20),
            Container(
              width: double.infinity,
              height: size.height * 0.4,
              child: ListView.separated(
                  separatorBuilder: (context, index) => Divider(
                        color: Colors.white,
                      ),
                  scrollDirection: Axis.vertical,
                  itemCount: starRatings.length,
                  itemBuilder: (BuildContext context, int index) {
                    return customRadio2(index, starRatings[index]);
                  }),
            ),
          ],
        ),
      ),
    );
  }

  void changeSecondaryIndex(int index, String text) {
    setState(() {
      secondaryIndex = index;
      print(text);
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => ReasonForRatingScreen()));
    });
  }

  Widget customRadio2(int index, String text) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
        onTap: () {
          changeSecondaryIndex(index, text);
        },
        child: index == 0
            ? Text(
                "",
                style: TextStyle(fontSize: 0.01),
              )
            : Container(
                height: size.height * 0.05,
                margin: EdgeInsets.only(
                    left: size.width * 0.05, right: size.width * 0.05),
                decoration: BoxDecoration(
                  color: secondaryIndex == index
                      ? Config.primaryColor
                      : Colors.white,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(
                      width: 1,
                      color: secondaryIndex == index
                          ? Config.primaryColor
                          : Colors.black),
                ),
                child: Row(
                  children: [
                    SizedBox(width: 5),
                    secondaryIndex == index
                        ? Icon(
                            Icons.radio_button_checked,
                            color: Colors.white,
                            size: 22,
                          )
                        : Icon(
                            Icons.radio_button_unchecked,
                            size: 22,
                          ),
                    SizedBox(width: 5),
                    Text(text,
                        style: TextStyle(
                            color: secondaryIndex == index
                                ? Colors.white
                                : Colors.black,
                            fontSize: 16)),
                  ],
                )));
  }

  text(String text, FontWeight fontWeight) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Text(
        text,
        style: TextStyle(
            color: Colors.black, fontWeight: fontWeight, fontSize: 18),
      ),
    );
  }
}
