import 'package:wblue_customer/services/dio/dio_client.dart';
import 'dart:collection';

import 'package:flutter/foundation.dart';

DioClient _dioClient = DioClient();

class R_SearchHelper with ChangeNotifier {
  List _restaurants;
  bool _isLoading = true;

  UnmodifiableListView get restaurantsSearch =>
      UnmodifiableListView(_restaurants);
  bool get isLoading => _isLoading;

  Future<void> fetchSearchRest(String searchKey) async {
    print('searchKey :$searchKey');

    setLoading(true);
    _restaurants = await fetchSearchRestaurents(searchKey);
    setLoading(false);

    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future fetchSearchRestaurents(String searchKey) async {
  Map<String, dynamic> _body = {
    'key': searchKey,
  };
  APIResponse res =
      await _dioClient.privatePost('/restaurant/search/key', data: _body);
  if (res.code >= 400) {
    return [];
  }

  return res.data;
}
