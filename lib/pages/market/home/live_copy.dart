// import 'package:flare_flutter/flare_actor.dart';
// import 'package:flutter/material.dart';
// import 'package:wblue_customer/widgets/live/w_bottom.dart';
// import 'package:wblue_customer/widgets/live/w_chat.dart';
// import 'package:wblue_customer/widgets/live/w_darker.dart';
// import 'package:wblue_customer/widgets/live/w_header.dart';
// import 'package:wblue_customer/widgets/live/w_joined.dart';
// import 'package:wblue_customer/widgets/live/w_video.dart';

// class LiveMarketPage extends StatefulWidget {
//   @override
//   _LiveMarketPageState createState() => _LiveMarketPageState();
// }

// class _LiveMarketPageState extends State<LiveMarketPage> {
//   int _index = 0;

//   @override
//   void initState() {
//     super.initState();

//     Future.delayed(Duration(milliseconds: 2000), () {
//       _index = 1;
//       setState(() {});
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Container(
//         color: Color(0xff1C1A1A),
//         width: MediaQuery.of(context).size.width,
//         height: MediaQuery.of(context).size.height,
//         child: IndexedStack(
//           index: _index,
//           children: [
//             _loading(),
//             Stack(children: [
//               WLiveVideoWidget(),
//               WLiveDarkerWidget(),
//               _live()
//             ])
//           ],
//         ),
//       ),
//     );
//   }

//   Widget _loading() {
//     return Container(
//       width: MediaQuery.of(context).size.width,
//       height: MediaQuery.of(context).size.height,
//       padding: EdgeInsets.all(80),
//       child: FlareActor(
//         'assets/loading/live-loading.flr',
//         animation: 'normal-loop',
//         fit: BoxFit.contain,
//         alignment: Alignment.center,
//       ),
//     );
//   }

//   Widget _live() {
//     return Container(
//       color: Colors.transparent,
//       width: MediaQuery.of(context).size.width,
//       height: MediaQuery.of(context).size.height,
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           WLiveHeaderWidget(),
//           Expanded(child: SizedBox()),
//           WLiveJoinedWidget(),
//           WLiveChatWidget(),
//           WLiveBottomWidget()
//         ],
//       ),
//     );
//   }
// }
