import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:wblue_customer/models/Restaurant/R_ListByRatings.dart';
import 'package:wblue_customer/models/Restaurant/food_type_model.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'dart:collection';

import 'package:flutter/foundation.dart';

DioClient _dioClient = DioClient();

class R_SearchHelper with ChangeNotifier {
  List<RestaurantListModel> _restaurants = new List<RestaurantListModel>();

  RefreshController _refreshRestaurantController =
      RefreshController(initialRefresh: false);
  RefreshController get refreshRestaurantController =>
      _refreshRestaurantController;

  UnmodifiableListView get restaurantsSearch =>
      UnmodifiableListView(_restaurants);

  List<Food> _foods = new List<Food>();

  UnmodifiableListView get foodsSearch => UnmodifiableListView(_foods);

  bool _isLoading = true;

  bool get isLoading => _isLoading;

  int _restPage = 1;
  int _restLimit = 10;

  int foodPage = 1;
  int foodLimit = 10;

  String _searchKey = '';

  Future<void> fetchSearchRest(String searchKey) async {
    if (searchKey.length < 2) return;
    _searchKey = searchKey;

    setLoading(true);
    _restaurants = await fetchSearchRestaurents();
    if (_restaurants.length <= 0) _refreshRestaurantController.loadNoData();

    _foods = await fetchSearchFoods();
    setLoading(false);
    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }

  Future<List<RestaurantListModel>> fetchSearchRestaurents() async {
    try {
      Map<String, dynamic> _body = {'key': _searchKey, 'limit': _restLimit};

      APIResponse res = await _dioClient
          .publicPost('/restaurant/search-key?page=$_restPage', data: _body);

      if (res.code >= 400) {
        return new List<RestaurantListModel>();
      }
      return res.data
          .map<RestaurantListModel>(
              (json) => RestaurantListModel.fromJson(json))
          .toList();
    } catch (e) {
      return new List<RestaurantListModel>();
    }
  }

  Future<List<Food>> fetchSearchFoods() async {
    try {
      Map<String, dynamic> _body = {'key': _searchKey, 'limit': foodLimit};

      APIResponse res = await _dioClient.publicPost(
          '/restaurant/search-food-key?page=$foodPage',
          data: _body);

      if (res.code >= 400) {
        return new List<Food>();
      }
      return res.data.map<Food>((json) => Food.fromJson(json)).toList();
    } catch (e) {
      return new List<Food>();
    }
  }

  void onRestaurantRefresh() async {
    resetRestaurantPage();
    _restaurants = await fetchSearchRestaurents();

    if (_restaurants.length > 0) {
      _refreshRestaurantController.refreshCompleted(resetFooterState: true);
    } else if (_restaurants.length <= 0) {
      _refreshRestaurantController.refreshCompleted(resetFooterState: true);
      _refreshRestaurantController.loadNoData();
    } else {
      _refreshRestaurantController.refreshFailed();
    }

    notifyListeners();
  }

  void setRestaurantNextPage() {
    _restPage++;
  }

  void resetRestaurantPage() {
    _restPage = 1;
  }

  void onRestaurantLoading() async {
    List<RestaurantListModel> data = await fetchSearchRestaurents();

    if (data.length > 0) {
      _refreshRestaurantController.loadComplete();
      setRestaurantNextPage();
    } else if (data.length <= 0) {
      _refreshRestaurantController.loadNoData();
    } else {
      _refreshRestaurantController.loadFailed();
    }
    _restaurants.addAll(data);
    notifyListeners();
  }
}
