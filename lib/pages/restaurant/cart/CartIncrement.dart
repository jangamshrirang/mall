import 'package:flutter/material.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/providers/restaurant/cartActions.dart';
import 'package:wblue_customer/providers/restaurant/onlyPostApis.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

class CartQuaintityIncreament extends StatefulWidget {
  int count, price;
  var productId, cartID;
  CartQuaintityIncreament(
      {this.count, this.productId, this.price, this.cartID});

  @override
  _CartQuaintityIncreamentState createState() =>
      _CartQuaintityIncreamentState();
}

class _CartQuaintityIncreamentState extends State<CartQuaintityIncreament> {
  void add() async {
    setState(() {
      widget.count++;
    });
    //await stateManagmentData.setQuantityValue(count);
  }

  void minus() async {
    setState(() {
      if (widget.count != 0) widget.count--;
    });
    //await stateManagmentData.setQuantityValue(count);
  }

  DioClient _dioClient = DioClient();
  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Container(
        width: MediaQuery.of(context).size.width * 0.1,
        height: MediaQuery.of(context).size.height * 0.02,
        child: Column(
          children: [
            Text(
              "${widget.price * widget.count}",
              style: TextStyle(
                color: Colors.black45,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    if (widget.count >= 1) {
                      setState(() {
                        widget.count--;
                        // updateCart(
                        //     widget.productId, widget.cartID, widget.count);
                        // stateManagment.setTotalPrict(stateManagment.totalPrice -
                        //     widget.price * widget.count);
                        fetchTotlaPrice(widget.cartID);
                      });
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.09,
                    height: MediaQuery.of(context).size.height * 0.035,
                    child: Icon(Icons.remove,
                        size: MediaQuery.of(context).size.height * 0.04,
                        color: Colors.black),
                  ),
                ),
                Text('${widget.count}',
                    style: new TextStyle(
                        fontSize: MediaQuery.of(context).size.height * 0.04,
                        color: Colors.black)),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      widget.count++;
                      // updateCart(widget.productId, widget.cartID, widget.count);
                      // stateManagment.setTotalPrict(stateManagment.totalPrice +
                      //   widget.price * widget.count);
                      fetchTotlaPrice(widget.cartID);
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.only(right: 0),
                    width: MediaQuery.of(context).size.width * 0.09,
                    height: MediaQuery.of(context).size.height * 0.05,
                    // color: Colors.red,
                    child: Icon(
                      Icons.add,
                      size: MediaQuery.of(context).size.height * 0.04,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

// Column(
//                                               children: [
//                                                 Row(
//                                                   children: [
//                                                     IconButton(
//                                                       icon: Icon(Icons.remove,
//                                                           color: Colors.black,
//                                                           size: size.height *
//                                                               0.05),
//                                                       padding:
//                                                           EdgeInsets.all(0),
//                                                       onPressed: () {
//                                                         if (rCartListModel
//                                                                 .food[index]
//                                                                 .quantity >=
//                                                             1) {
//                                                           setState(() {
//                                                             int count =
//                                                                 rCartListModel
//                                                                     .food[index]
//                                                                     .quantity;
//                                                             rCartListModel
//                                                                 .food[index]
//                                                                 .quantity--;
//                                                             updateCart(
//                                                                 rCartListModel
//                                                                     .food[index]
//                                                                     .foodInfo
//                                                                     .hashId,
//                                                                 cartID,
//                                                                 count);
//                                                             // stateManagment.setTotalPrict(stateManagment.totalPrice -
//                                                             //     widget.price * widget.count);
//                                                             fetchTotlaPrice(
//                                                                 cartID);
//                                                                 print(count);
//                                                           });
//                                                         }
//                                                       },
//                                                     ),
//                                                     IconButton(
//                                                       icon: Icon(Icons.add,
//                                                           color: Colors.black,
//                                                           size: size.height *
//                                                               0.05),
//                                                       padding:
//                                                           EdgeInsets.all(0),
//                                                       onPressed: null,
//                                                     )
//                                                   ],
//                                                 ),
//                                               ],
//                                             )
