import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:slide_countdown_clock/slide_countdown_clock.dart';

class HotDealsPage extends StatefulWidget {
  @override
  _HotDealsPageState createState() => _HotDealsPageState();
}

class _HotDealsPageState extends State<HotDealsPage> {
  int _index = 0;
  int _saleIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text('Hot Deals'),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(60),
            child: Row(
              children: [
                _tab(
                    label: 'Today',
                    isCurrent: _index == 0 ? true : false,
                    onTap: () {
                      _index = 0;
                      setState(() {});
                    }),
                _tab(
                    label: 'Tomorrow',
                    isCurrent: _index == 1 ? true : false,
                    onTap: () {
                      _index = 1;
                      setState(() {});
                    }),
              ],
            ),
          )),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: IndexedStack(
          index: _index,
          children: [
            _today(),
            _today(),
          ],
        ),
      ),
    );
  }

  Widget _tab({String label, bool isCurrent, Function onTap}) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        width: MediaQuery.of(context).size.width * .5,
        height: 60,
        decoration: BoxDecoration(color: Colors.transparent, border: Border(bottom: BorderSide(color: isCurrent ? Colors.white : Colors.transparent, width: 3))),
        alignment: Alignment.center,
        child: Text(
          label,
          style: TextStyle(color: isCurrent ? Colors.white : Colors.white60, fontSize: isCurrent ? 18 : 16),
        ),
      ),
    );
  }

  Widget _today() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 5),
          Text(
            'Crazy Hot Deals',
            style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 5),
          Card(
            child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    _timer()
                  ],
                )),
          )
        ],
      ),
    );
  }

  Widget _timer() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _saleIndex == 0
            ? _countdown()
            : _later(
                label: '12:00 AM',
                onTap: () {
                  _saleIndex = 0;
                  setState(() {});
                }),
        Container(
          width: 1,
          height: 50,
          color: Colors.black26,
        ),
        _later(
            label: '10:00 AM',
            isCurrent: _saleIndex == 1 ? true : false,
            onTap: () {
              _saleIndex = 1;
              setState(() {});
            }),
        Container(
          width: 1,
          height: 50,
          color: Colors.black26,
        ),
        _later(
            label: '4:00 PM',
            isCurrent: _saleIndex == 2 ? true : false,
            onTap: () {
              _saleIndex = 2;
              setState(() {});
            }),
      ],
    );
  }

  Widget _later({String label, bool isCurrent: false, Function onTap}) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        color: Colors.transparent,
        padding: EdgeInsets.all(15),
        child: Text(
          label,
          style: TextStyle(fontSize: 18, fontWeight: isCurrent ? FontWeight.bold : FontWeight.normal),
        ),
      ),
    );
  }

  Widget _countdown() {
    return SlideCountdownClock(
      duration: Duration(hours: 10),
      slideDirection: SlideDirection.Up,
      tightLabel: true,
      textStyle: TextStyle(fontSize: 14, color: Colors.white, fontWeight: FontWeight.bold),
      decoration: BoxDecoration(
        color: Color(0xff1C1A1A),
      ),
      padding: EdgeInsets.symmetric(vertical: 10),
      onDone: () {
        print('done');
      },
    );
  }
}
