import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/DriverRatings/yesOrnoScreen.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

class ReasonForRatingScreen extends StatefulWidget {
  @override
  _ReasonForRatingScreenState createState() => _ReasonForRatingScreenState();
}

class _ReasonForRatingScreenState extends State<ReasonForRatingScreen> {
  List<String> reasons = [
    "   ",
    "Attitude of courier",
    "Courier requesting signature for 'Proof of Delivery'",
    "Courier following instruction to delivery to reqested location/ receriver",
    "Courier handeling of package",
    "Courier handeling of Cash/Payment (Not applicable in my place)",
    "Speed of delivery",
    "feedback on seller (eg. Packing,Product Quality)"
  ];
  int secondaryIndex = 0;
  final noteController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBarWithOutIcon(context, "Delivery Survey"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: FractionallySizedBox(
                widthFactor: 0.3,
                child: Image.asset('assets/images/logos/wmall-512.png'),
              ),
            ),
            text(
                "What is the reason for your rating? [Feel free to provide more information ]",
                FontWeight.normal),
            SizedBox(height: 20),
            Container(
              width: double.infinity,
              height: size.height * 0.6,
              child: ListView.separated(
                  separatorBuilder: (context, index) => Divider(
                        color: Colors.white,
                      ),
                  scrollDirection: Axis.vertical,
                  itemCount: reasons.length,
                  itemBuilder: (BuildContext context, int index) {
                    return customRadio2(index, reasons[index]);
                  }),
            ),
          ],
        ),
      ),
    );
  }

  void changeSecondaryIndex(int index, String text) {
    setState(() {
      secondaryIndex = index;
      print(text);
    });
  }

  Widget customRadio2(int index, String text) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
        onTap: () {
          changeSecondaryIndex(index, text);
        },
        child: Column(
          children: [
            index == 0
                ? Text(
                    "",
                    style: TextStyle(fontSize: 0.01),
                  )
                : Container(
                    height: size.height * 0.06,
                    margin: EdgeInsets.only(
                        left: size.width * 0.05, right: size.width * 0.05),
                    decoration: BoxDecoration(
                      color: secondaryIndex == index
                          ? Config.primaryColor
                          : Colors.white,
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(
                          width: 1,
                          color: secondaryIndex == index
                              ? Config.primaryColor
                              : Colors.black),
                    ),
                    child: Row(
                      children: [
                        SizedBox(width: 5),
                        secondaryIndex == index
                            ? Icon(
                                Icons.radio_button_checked,
                                color: Colors.white,
                                size: 22,
                              )
                            : Icon(
                                Icons.radio_button_unchecked,
                                size: 22,
                              ),
                        SizedBox(width: 5),
                        Container(
                          width: size.width * 0.7,
                          child: Text(text,
                              maxLines: 2,
                              style: TextStyle(
                                  color: secondaryIndex == index
                                      ? Colors.white
                                      : Colors.black,
                                  fontSize: 16)),
                        ),
                      ],
                    )),
            SizedBox(
              height: 5,
            ),
            secondaryIndex == index
                ? index == 0
                    ? Text(
                        "",
                        style: TextStyle(fontSize: 0.01),
                      )
                    : Container(
                        margin: EdgeInsets.only(left: 0),
                        height: size.height * 0.05,
                        width: size.width * 0.9,
                        child: TextFormField(
                          maxLines: 1,
                          cursorColor: Colors.black,
                          controller: noteController,
                          decoration: InputDecoration(
                              suffixIcon: IconButton(
                                  icon: Icon(
                                    Icons.done,
                                    color: Colors.black45,
                                  ),
                                  onPressed: () {
                                    print(noteController.text);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                YesOrNoScreen()));
                                  }),
                              filled: true,
                              hintStyle: TextStyle(
                                  color: Colors.black12,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.none),
                              fillColor: Colors.grey[200],
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.black, width: 1),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  borderSide: BorderSide(
                                      color: Colors.black38, width: 1)),
                              contentPadding: EdgeInsets.only(
                                  left: 15, bottom: 10, top: 0, right: 15),
                              hintText: "Type here..."),
                        ),
                      )
                : Text("")
          ],
        ));
  }

  text(String text, FontWeight fontWeight) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Text(
        text,
        maxLines: 2,
        style: TextStyle(
            color: Colors.black, fontWeight: fontWeight, fontSize: 18),
      ),
    );
  }
}
