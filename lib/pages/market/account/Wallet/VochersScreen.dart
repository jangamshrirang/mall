import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';

import 'AppbarForAllWalwtScreens.dart';

class VocherScreen extends StatefulWidget {
  int colorVal = 0xff84020e;
int intialIndex;
VocherScreen({
  this.intialIndex
});
  @override
  _VocherScreenState createState() => _VocherScreenState();
}

class _VocherScreenState extends State<VocherScreen> with TickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() { 
    super.initState();
    _tabController = new TabController(vsync: this, length: 5);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {
      widget.colorVal = 0xff84020e;
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "My Voucher"),
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: DefaultTabController(
          length: 5,
          child: Column(
            children: <Widget>[
              TabBar(
                isScrollable: true,
                controller: _tabController,
                indicatorSize: TabBarIndicatorSize.tab,
                indicatorColor: Colors.red,
                unselectedLabelColor: Colors.grey,
                tabs: <Widget>[
                  Tab(child: text("All", 0)),
                  Tab(
                    child: text("9.9", 1),
                  ),
                  Tab(
                    child: text("Wmall", 2),
                  ),
                  Tab(
                    child: text("Store", 3),
                  ),
                  Tab(
                    child: text("Store credit", 4),
                  ),
                ],
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    payScreenContainer(),
                    payScreenContainer(),
                    payScreenContainer(),
                    payScreenContainer(),
                    payScreenContainer(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  text(String title, int number) {
    return Text(
      title,
      style: TextStyle(
          color: _tabController.index == number ? Colors.red : Colors.black),
      textAlign: TextAlign.start,
    );
  }

  payScreenContainer() {
    return Column(
      children: [
        SizedBox(
          height: 20,
        ),
        Image.asset(
          "assets/images/empty.png",
          height: 200,
          color: Config.primaryColor,
        ),
        Text(
          "Go collect vouche now",
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.normal),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: 50,
        ),
        RaisedButton(
          onPressed: () {},
          textColor: Colors.white,
          padding: const EdgeInsets.all(0.0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(2.0)),
          child: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xFF0D47A1),
                    Color(0xFF1976D2),
                    Color(0xFF42A5F5),
                  ],
                ),
                borderRadius: BorderRadius.all(Radius.circular(2.0))),
            padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
            child:
                const Text('Collect more voucher', style: TextStyle(fontSize: 20)),
          ),
        ),
      ],
    );
  }
}
