import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:titled_navigation_bar/titled_navigation_bar.dart';

import 'package:wblue_customer/pages/market/account/acount.dart';
import 'package:wblue_customer/pages/market/feed/feed.dart';
import 'package:wblue_customer/pages/market/home/home.dart';

import 'package:wblue_customer/pages/market/order/cart/cart.dart';
import 'package:wblue_customer/providers/market/p_bottom_nav.dart';

import '../../env/config.dart';
import 'Brands/Brands.dart';

List<Widget> pages = [
  HomeMarketPage(),
  FeedMarketPage(),
  BrandsPage(),
  CartMarketPage(),
  AccountMarketPage()
];

class MainMarketPage extends StatefulWidget {
  @override
  _MainMarketPageState createState() => _MainMarketPageState();
}

class _MainMarketPageState extends State<MainMarketPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  final List<TitledNavigationBarItem> items = [
    TitledNavigationBarItem(title: Text('Home'), icon: MdiIcons.home),
    TitledNavigationBarItem(title: Text('Feed'), icon: MdiIcons.accessPoint),
    TitledNavigationBarItem(title: Text('Brands'), icon: MdiIcons.tagHeart),
    TitledNavigationBarItem(title: Text('Cart'), icon: MdiIcons.cart),
    TitledNavigationBarItem(title: Text('Account'), icon: MdiIcons.account),
  ];

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Consumer<PBottomNavigation>(
      builder: (_, page, __) => Scaffold(
          backgroundColor: Colors.white,
          body: IndexedStack(index: page.tab, children: pages),
          bottomNavigationBar: SafeArea(
            child: Theme(
              data: Theme.of(context).copyWith(
                // sets the background color of the `BottomNavigationBar`
                canvasColor: Colors.white,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
              ),
              child: BottomNavigationBar(
                items: <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                      icon: Icon(
                          page.tab == 0 ? MdiIcons.home : MdiIcons.homeOutline),
                      label: 'Home'),
                  BottomNavigationBarItem(
                      icon: Icon(page.tab == 1
                          ? MdiIcons.viewGrid
                          : MdiIcons.viewGridOutline),
                      label: 'Feed'),
                  BottomNavigationBarItem(
                      icon: Icon(page.tab == 2
                          ? MdiIcons.heart
                          : MdiIcons.heartOutline),
                      label: 'Brand'),
                  BottomNavigationBarItem(
                      icon: Icon(
                          page.tab == 3 ? MdiIcons.cart : MdiIcons.cartOutline),
                      label: 'cart'),
                  BottomNavigationBarItem(
                      icon: Icon(
                          page.tab == 4 ? Icons.person : Icons.person_outline),
                      label: 'Account'),
                ],
                backgroundColor: Colors.white,
                showSelectedLabels: true,
                showUnselectedLabels: true,
                fixedColor: Config.primaryColor,
                type: BottomNavigationBarType.fixed,
                unselectedFontSize: 12,
                selectedFontSize: 12,
                unselectedItemColor: Colors.black,
                elevation: 0.0,
                currentIndex: page.tab,
                onTap: (index) async {
                  if (index == 3 || index == 4) {
                    String uri =
                        index == 3 ? '/market/cart/items' : '/market/account';

                    bool res = await ExtendedNavigator.of(context)
                        .root
                        .canNavigate(uri);
                    if (!res) return;
                  }
                  context.read<PBottomNavigation>().changeTab(index);
                },
              ),
            ),
            // child: TitledBottomNavigationBar(
            //   onTap: (index) async {
            //     int i = index;

            //     if (index == 3) {
            //       await ExtendedNavigator.of(context)
            //           .root
            //           .canNavigate('/market/cart/items');
            //     }

            //     context.read<PBottomNavigation>().changeTab(i);
            //   },
            //   currentIndex: page.tab,
            //   curve: Curves.easeInBack,
            //   reverse: true,
            //   items: items,
            //   enableShadow: false,
            //   activeColor: Config.ternaryColor,
            //   inactiveColor: Config.primaryColor,
            // ),
          )),
    );
  }
}
