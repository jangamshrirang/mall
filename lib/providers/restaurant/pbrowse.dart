import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:wblue_customer/models/Restaurant/resturentMenuModel.dart';

class PBrowseRestaurant with ChangeNotifier {
  RestaurantsModel restaurantsModel = RestaurantsModel();
  final _swiperControl = SwiperController();

  List _restaurants = new List();
  bool _isLoading = true;
  SwiperController get swiperControl => _swiperControl;

  UnmodifiableListView get restaurants => UnmodifiableListView(_restaurants);
  bool get isLoading => _isLoading;

  Future<void> fetchRestaurants() async {
    setLoading(true);
    _restaurants = await restaurantsModel.fetchRestaurants();
    setLoading(false);
    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}
