import 'dart:typed_data';

import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:google_map_polyline/google_map_polyline.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'dart:ui' as ui;
import 'package:steps_indicator/steps_indicator.dart';

import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Settings/ReviewScreen.dart';

class OrderTrackPage extends StatefulWidget {
  String deliverd;
  List<LatLng> cordinates;
  BitmapDescriptor pinLocationIcon;
  OrderTrackPage({this.cordinates, this.pinLocationIcon, this.deliverd});
  @override
  _OrderTrackPageState createState() => _OrderTrackPageState();
}

class _OrderTrackPageState extends State<OrderTrackPage> {
  final Set<Polyline> polyline = {};
  Set<Marker> _markers = {};
  GoogleMapController _controller;
  List<LatLng> routeCoords;
  GoogleMapPolyline googleMapPolyline =
      new GoogleMapPolyline(apiKey: "AIzaSyCpZybhi4ZzUBXImzkKtaggzrOSsfcH8p4");
  BitmapDescriptor icon;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getIcons();
  }

  getIcons() async {
    var icon = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(devicePixelRatio: 0.1, size: Size.fromHeight(0.2)),
      "assets/images/van.png",
    );
    setState(() {
      this.icon = icon;
    });
  }

  LatLng pinPosition = LatLng(25.254246, 51.536136);
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        elevation: 0.0,
        leading: IconButton(
          onPressed: () => ExtendedNavigator.of(context).pop(),
          icon: Icon(MdiIcons.arrowLeft),
          color: Colors.black,
        ),
        centerTitle: false,
        title: AutoSizeText('Track Your Order',
            style: TextStyle(color: Colors.black)),
      ),
      body: ListView(
        physics: NeverScrollableScrollPhysics(),
        children: [
          Stack(
            children: [
              Container(
                // color: Colors.red,
                height: size.height * 0.08,
                margin: EdgeInsets.only(top: 20),
                child: ListView(
                  children: [
                    StepsIndicator(
                      selectedStep: 2,
                      doneLineThickness: 3,
                      nbSteps: 5,
                      lineLength: 55,
                      doneStepColor: Colors.green,
                      selectedStepColorIn: Colors.white,
                      unselectedStepColorIn: Colors.grey[200],
                      undoneLineColor: Colors.grey[200],
                      doneLineColor: Colors.green,
                    ),
                  ],
                ),
              ),
              positioned("Processing", size.height * 0.06, size.width * 0.05,
                  size.width * 0.1, 0.0),
              positioned("Ready to ship", size.height * 0.00, size.width * 0.25,
                  size.width * 0.1, 0.0),
              positioned("shipped", size.height * 0.06, size.width * 0.43,
                  size.width * 0.1, 0.0),
              positioned("To be Delivered ", size.height * 0.00,
                  size.width * 0.55, size.width * 0.1, 0.0),
              positioned("Delivered", size.height * 0.06, size.width * 0.75,
                  size.width * 0.1, 0.0),
            ],
          ),
          Container(
            height: size.height * 0.25,
            margin: EdgeInsets.only(left: 5, right: 5),
            padding: EdgeInsets.only(top: 20),
            decoration: BoxDecoration(
                color: Colors.grey[200],
                border: Border.all(
                  color: Colors.grey[200],
                ),
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  rowText("07/08/2020", Colors.black45,
                      "Your Order has beem verified", Colors.black),
                  rowText(
                      "07/08/2020",
                      Colors.black45,
                      "Your item(s) is being packed and ready for shipment at our merchant's warehouse",
                      Colors.black),
                  rowText(
                      "07/08/2020",
                      Colors.black45,
                      "Your item(s) is being shipped by w-express",
                      Colors.black),
                  rowText(
                      "07/08/2020",
                      Colors.black45,
                      "Your item(s) has been arrived.Thank you for shopping with W mall",
                      Colors.black),
                ],
              ),
            ),
          ),
          SizedBox(height: 10),
          Divider(
            thickness: 5,
          ),
          Container(
              margin: EdgeInsets.only(top: 5),
              child: Column(
                children: [
                  Text(
                    "Deliver partner",
                    style: TextStyle(fontSize: 15),
                  ),
                  SizedBox(height: 5),
                  Image.asset(
                    "assets/images/logox.png",
                    height: 60,
                  )
                ],
              )),
          Divider(
            thickness: 5,
          ),
          widget.deliverd != "delivered"
              ? Container(
                  margin: EdgeInsets.only(top: 5),
                  child: Column(
                    children: [
                      Text(
                        "Live Track",
                        style: TextStyle(fontSize: 15),
                      ),
                      SizedBox(height: 5),
                      Container(
                          height: size.height * 0.28,
                          width: double.infinity,
                          child: GoogleMap(
                            tiltGesturesEnabled: false,
                            onMapCreated: onMapCreated,
                            polylines: polyline,
                            initialCameraPosition: CameraPosition(
                                target: LatLng(25.254246, 51.536136),
                                zoom: 14.0),
                            mapType: MapType.terrain,
                            markers: _markers,
                          ))
                    ],
                  ))
              : Container(
                  margin: EdgeInsets.only(left: 20, right: 20),
                  child: RaisedButton(
                      child: Text(
                        "Review",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Config.primaryColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5.0))),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ReviewScreen()));
                      }),
                ),
          Divider(
            thickness: 5,
          ),
        ],
      ),
    );
  }

  button(String title, Function onTap, Color color) {
    return Container(
      width: 10,
      child: RaisedButton(
          child: Text(
            title,
            style: TextStyle(color: Colors.white),
          ),
          color: color,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          onPressed: onTap),
    );
  }

  void onMapCreated(GoogleMapController controller) {
    setState(() {
      _controller = controller;

      polyline.add(Polyline(
          polylineId: PolylineId('route1'),
          visible: true,
          points: widget.cordinates,
          width: 2,
          color: Colors.blue,
          startCap: Cap.roundCap,
          endCap: Cap.buttCap));

      _markers.add(Marker(
          markerId: MarkerId('<MARKER_ID>'),
          position: pinPosition,
          icon: widget.pinLocationIcon));
      _markers.add(Marker(
          markerId: MarkerId('<MARKER_ID1>'),
          position: LatLng(25.246684, 51.533330),
          icon: BitmapDescriptor.defaultMarkerWithHue(10.0)));
    });
  }

  positioned(String text, double top, left, right, bottom) {
    return Positioned(
        top: top,
        left: left,
        right: right,
        bottom: bottom,
        child: Text(
          text,
          style: TextStyle(fontSize: 15),
        ));
  }

  rowText(String text1, Color color1, String text2, Color color) {
    var size = MediaQuery.of(context).size;
    return Row(
      children: [
        Container(
          margin: EdgeInsets.only(left: size.width * 0.1, bottom: 10),
          //color: Colors.blue,
          width: size.width * 0.4,
          child: Text(
            text1,
            style: TextStyle(color: color1, fontSize: size.height * 0.018),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 9),
          //color: Colors.yellow,
          width: size.width * 0.45,
          child: Text(
            text2,
            style: TextStyle(color: color, fontSize: size.height * 0.019),
          ),
        ),
      ],
    );
  }
}
