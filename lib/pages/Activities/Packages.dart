import 'package:flutter/material.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

import 'DropInDropoff.dart';

class Packages extends StatefulWidget {
  String image, subtitle, title;
  Packages({this.image, this.subtitle, this.title});
  @override
  _PackagesState createState() => _PackagesState();
}

class _PackagesState extends State<Packages> {
  List<String> starRatings = [
    "    ",
    "  Package 3 \n This Package includes the following \n 100 QAR per person \n 5 Free bottled water",
    "  Package 2 \n This Package includes the following \n 100 QAR per person \n 5 Free bottled water",
    "  Package 1 \n This Package includes the following \n 100 QAR per person \n 5 Free bottled water",
  ];

  int secondaryIndex = 0;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithSearchIcons(context, "Packages",(){}),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                height: size.height * 0.3,
                width: size.width * 0.9,
                margin: EdgeInsets.only(top: size.height * 0.01),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      spreadRadius: 1,
                      blurRadius: 3,
                      offset: Offset(1, 3), // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  image: DecorationImage(
                    image: AssetImage(
                      widget.image,
                    ),
                    fit: BoxFit.fill,
                  ),
                ),
                child: Container(
                    //text container
                    decoration: BoxDecoration(
                      color: Colors.black54,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(8),
                          bottomRight: Radius.circular(8)),
                    ),
                    margin: EdgeInsets.only(top: size.height * 0.21 //23
                        ),
                    padding: EdgeInsets.only(
                        left: size.width * 0.02,
                        right: size.width * 0.01,
                        top: size.height * 0.01),
                    height: size.height * 0.06,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.normal,
                              color: Colors.white,
                              fontSize: size.height * 0.023),
                        ),
                        Text(
                          widget.subtitle,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.normal,
                              color: Colors.white70,
                              fontSize: size.height * 0.018),
                        ),
                      ],
                    ))),
            SizedBox(height: 20),
            text("Select packages you want", FontWeight.bold),
            Container(
              width: double.infinity,
              height: size.height * 0.48,
              child: ListView.separated(
                  physics: NeverScrollableScrollPhysics(),
                  separatorBuilder: (context, index) => Divider(
                        color: Colors.white,
                      ),
                  scrollDirection: Axis.vertical,
                  itemCount: starRatings.length,
                  itemBuilder: (BuildContext context, int index) {
                    return customRadio2(index, starRatings[index]);
                  }),
            ),
            button("Next", () {
              stateManagment.setPickUpLocation(null);
              stateManagment.setDropOffLocation(null);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DropInDropOff()));
              //_displayDialog(context);
            }, Color(0xff284E99)),
          ],
        ),
      ),
    );
  }

  text(String text, FontWeight fontWeight) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Text(
        text,
        style: TextStyle(
            color: Colors.black,
            fontWeight: fontWeight,
            fontSize: 18,
            decoration: TextDecoration.none),
      ),
    );
  }

  //buttons
  button(String title, Function onTap, Color color) {
    return RaisedButton(
        child: Text(
          title,
          style: TextStyle(color: Colors.white),
        ),
        color: color,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        onPressed: onTap);
  }

  void changeSecondaryIndex(int index, String text) {
    setState(() {
      secondaryIndex = index;
      print(text);
    });
  }

  Widget customRadio2(int index, String text) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
        onTap: () {
          changeSecondaryIndex(index, text);
        },
        child: index == 0
            ? Text(
                "",
                style: TextStyle(fontSize: 0.01),
              )
            : Container(
                height: size.height * 0.13,
                margin: EdgeInsets.only(
                    left: size.width * 0.05, right: size.width * 0.05),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                      width: 1,
                      color: secondaryIndex == index
                          ? Color(0xff101f40)
                          : Colors.black12),
                ),
                child: Row(
                  children: [
                    SizedBox(width: 5),
                    secondaryIndex == index
                        ? Icon(
                            Icons.radio_button_checked,
                            color: Colors.white,
                            size: 0,
                          )
                        : Icon(
                            Icons.radio_button_unchecked,
                            size: 0,
                          ),
                    SizedBox(width: 5),
                    Text(text,
                        style:
                            TextStyle(color: Color(0xff101f40), fontSize: 16)),
                  ],
                )));
  }
}
