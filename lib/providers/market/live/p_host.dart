import 'dart:async';
import 'dart:collection';
import 'dart:io';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtm/agora_rtm.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wakelock/wakelock.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/firebaseDb/firestoreDB.dart';
import 'package:wblue_customer/models/live/message.dart';
import 'package:wblue_customer/models/live/user.dart';
import 'package:wblue_customer/models/user.dart';

class PHostLive extends ChangeNotifier {
  UserModel _user;

  AgoraRtmClient _client;
  AgoraRtmChannel _channel;

  List<User> _userList = [];

  var userMap;

  bool _muted = false;
  int _userNo = 0;
  RtcEngine _engine;
  final _infoStrings = <Message>[];
  List<int> _users = <int>[];
  bool get muted => _muted;

  //setup live
  String _contentTitle;
  File _contentThumbnail;

  int get userNo => _userNo;
  String _timerDisplay = '00:00:00';
  final duration = const Duration(seconds: 1);
  Stopwatch _stopwatch = Stopwatch();

  String get timerDisplay => _timerDisplay;
  Stopwatch get stopwatch => _stopwatch;
  UnmodifiableListView<Message> get infoStrings => UnmodifiableListView(_infoStrings);
  UnmodifiableListView<int> get users => UnmodifiableListView(_users);

  Future<void> initialize() async {
    if (Config.agoraApi.isEmpty) {
      print(
        'APP_ID missing, please provide your APP_ID in settings.dart',
      );
      print(
        'Agora Engine is not starting',
      );

      notifyListeners();
      return;
    }

    await _initAgoraRtcEngine();
    _addAgoraEventHandlers();

    VideoEncoderConfiguration configuration = VideoEncoderConfiguration();
    configuration.dimensions = VideoDimensions(1920, 1080);
    await _engine.setVideoEncoderConfiguration(configuration);

    // fetch user hash id from api
    _user = await UserModel.fetchMyProfile();

    userMap = {
      _user.hashid: {'name': _user.name, 'img': _user.full}
    };

    await _engine.joinChannel(null, _user.hashid, null, 0);
  }

  /// Create agora sdk instance and initialize
  Future<void> _initAgoraRtcEngine() async {
    _engine = await RtcEngine.create(Config.agoraApi);
    await _engine.enableVideo();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await _engine.setClientRole(ClientRole.Broadcaster);
  }

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    _engine.setEventHandler(RtcEngineEventHandler(error: (code) {
      print('onError: $code');
      notifyListeners();
    }, joinChannelSuccess: (channel, uid, elapsed) async {
      await Wakelock.enable();
      DateTime date = DateTime.now();

      FireStoreClass.createLiveUser(
          id: uid,
          userid: _user.hashid,
          contentImage: _contentThumbnail,
          contentTitle: _contentTitle,
          image: _user.full,
          name: _user.name,
          time: DateFormat("dd-MM-yyyy hh:mm:ss").format(date),
          viewer: 0);

      final info = 'onJoinChannel: $channel, uid: $uid';
      print(info);
      runStartTimer();
      notifyListeners();
    }, leaveChannel: (stats) {
      print('onLeaveChannel');
      _users.clear();
      notifyListeners();
    }, userJoined: (uid, elapsed) {
      final info = 'userJoined: $uid';
      print(info);
      _users.add(uid);
      notifyListeners();
    }, userOffline: (uid, elapsed) {
      final info = 'userOffline: $uid';
      print(info);
      _users.remove(uid);
      notifyListeners();
    }, firstRemoteVideoFrame: (uid, width, height, elapsed) {
      final info = 'firstRemoteVideo: $uid ${width}x $height';
      print(info);
      notifyListeners();
    }));
  }

  void _logout() async {
    try {
      await _client.logout();
      // _log('Logout success.');
    } catch (errorCode) {
      print('leave logout error: $errorCode');

      //_log('Logout error: ' + errorCode.toString());
    }
  }

  void _leaveChannel() async {
    try {
      await _channel.leave();
      //_log('Leave channel success.');
      print('_channel.channelId: ${_channel.channelId}');
      _client.releaseChannel(_channel.channelId);
    } catch (errorCode) {
      print('leave channel error: $errorCode');
      //_log('Leave channel error: ' + errorCode.toString());
    }
  }

  void createClient() async {
    print('createClient starting...');

    if (_user == null) {
      _user = await UserModel.fetchMyProfile();
    }

    _client = await AgoraRtmClient.createInstance('b42ce8d86225475c9558e478f1ed4e8e');

    _client.onMessageReceived = (AgoraRtmMessage message, String peerId) {
      print('-------------- message -----------');
      _log(user: peerId, info: message.text, type: 'message');
    };

    _client.onConnectionStateChanged = (int state, int reason) {
      if (state == 5) {
        _client.logout();
      }
    };

    print('_user.hashid: ${_user.hashid}');
    await _client.login(null, _user.hashid);
    _channel = await _createChannel(_user.hashid);

    await _channel.join();
    print('joined at channel: ${_channel.channelId}');
  }

  Future<AgoraRtmChannel> _createChannel(String name) async {
    AgoraRtmChannel channel = await _client.createChannel(name);

    channel.onMemberJoined = (AgoraRtmMember member) async {
      print('-- MEMBER USER ID-- ${member.userId} --');
      final uprofile = await UserModel.fetchUserProfile(member.userId);
      UserModel otherUser = UserModel.fromJson(uprofile);
      _userList.add(new User(username: member.userId, name: otherUser.name, image: otherUser.full));
      notifyListeners();
      userMap.putIfAbsent(member.userId, () => {'name': otherUser.name, 'img': otherUser.full});
      var len;

      _channel.getMembers().then((value) {
        len = value.length;
        _userNo = len - 1;
        notifyListeners();
      });

      _log(info: 'Member joined: ', user: member.userId, type: 'join');
    };

    channel.onMemberLeft = (AgoraRtmMember member) {
      var len;
      _userList.removeWhere((element) => element.username == member.userId);

      notifyListeners();

      _channel.getMembers().then((value) {
        len = value.length;
        _userNo = len - 1;
        notifyListeners();
      });
    };
    channel.onMessageReceived = (AgoraRtmMessage message, AgoraRtmMember member) {
      _log(user: member.userId, info: message.text, type: 'message');
    };

    return channel;
  }

  void onToggleMute() {
    _muted = !muted;
    notifyListeners();
    _engine.muteLocalAudioStream(muted);
  }

  void onSwitchCamera() {
    _engine.switchCamera();
  }

  void disposeEngine() {
    // clear users
    _users.clear();
    _infoStrings.clear();
    _userNo = 0;

    // destroy sdk
    stopwatch.stop();
    stopwatch.reset();
    _leaveChannel();
    _logout();
    FireStoreClass.deleteUser(username: _user.hashid);
    _engine.leaveChannel().catchError((onError) {
      print('onleaveChannelError: $onError');
    });
    _engine.destroy().catchError((onError) {
      print('ondestroyError: $onError');
    });
  }

  void startTimer() {
    Timer(duration, keepRunning);
  }

  void runStartTimer() {
    stopwatch.start();
    startTimer();
  }

  void keepRunning() {
    if (stopwatch.isRunning) {
      startTimer();
    }
    _timerDisplay = stopwatch.elapsed.inHours.toString().padLeft(2, '0') +
        ':' +
        (stopwatch.elapsed.inMinutes % 60).toString().padLeft(2, '0') +
        ':' +
        (stopwatch.elapsed.inSeconds % 60).toString().padLeft(2, '0');

    notifyListeners();
  }

  Future<void> setupLiveContent({String title, File img}) async {
    _contentTitle = title;
    _contentThumbnail = img;
  }

  void _log({String info, String type, String user}) {
    if (type == 'message' && info.contains('m1x2y3z4p5t6l7k8')) {
      // popUp();
    } else {
      var u = userMap[user];

      Message m = new Message(message: info, type: type, user: u['name'], image: u['img']);
      _infoStrings.insert(0, m);
    }
  }

  // void setupUserMap() {
  //   userMap = {_hostId: _hostImageUrl};
  // }
}
