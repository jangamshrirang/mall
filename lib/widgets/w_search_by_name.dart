import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WSearchByNameWidget extends StatefulWidget {
  final Types type;
  final Function onPressed;
  final bool showFilter;

  WSearchByNameWidget({
    this.type: Types.restaurant,
    this.onPressed: _function,
    this.showFilter: true,
  });

  @override
  _WSearchByNameWidgetState createState() => _WSearchByNameWidgetState();
}

enum Types {
  restaurant,
  hotel,
  cinema,
}

class _WSearchByNameWidgetState extends State<WSearchByNameWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      height: 50.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
            child: TextField(
              decoration: InputDecoration.collapsed(hintText: 'Enter ${widget.type == Types.restaurant ? 'restaurant' : (widget.type == Types.hotel ? 'hotel' : 'cinema')} name'),
            ),
          ),
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
          ),
          (widget.showFilter
              ? IconButton(
                  icon: Icon(Icons.filter_list),
                  onPressed: () {
                    widget.onPressed();
                  },
                )
              : Container()),
        ],
      ),
    );
  }
}

void _function() {}
