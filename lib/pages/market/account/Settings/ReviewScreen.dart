import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

class ReviewScreen extends StatefulWidget {
  @override
  _ReviewScreenState createState() => _ReviewScreenState();
}

class _ReviewScreenState extends State<ReviewScreen> {
  List<String> reviews = [
    "    ",
    "  Driver",
    "  Vendor",
    "  Product",
  ];
  int secondaryIndex = 0;
  List<Asset> images = List<Asset>();
  String _error = 'No Error Dectected';

  @override
  void initState() {
    super.initState();
  }

  Widget buildGridView() {
    return GridView.count(
      crossAxisCount: 3,
      crossAxisSpacing: 5,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return AssetThumb(
          asset: asset,
          width: 300,
          height: 300,
        );
      }),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 3,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
      _error = error;
    });
  }

  final noteController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "Select for Review"),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(left: size.width * 0.04),
              width: double.infinity,
              height: size.height * 0.05,
              child: ListView.separated(
                  //  physics: NeverScrollableScrollPhysics(),
                  separatorBuilder: (context, index) => Divider(
                        color: Colors.white,
                      ),
                  scrollDirection: Axis.horizontal,
                  itemCount: reviews.length,
                  itemBuilder: (BuildContext context, int index) {
                    return customRadio2(index, reviews[index]);
                  }),
            ),
            SizedBox(
              height: size.height * 0.02,
            ),
            Container(
              margin: EdgeInsets.only(left: 20, right: 10),
              height: size.height * 0.5,
              width: size.width * 0.9,
              child: TextFormField(
                maxLines: 25,
                cursorColor: Colors.black,
                controller: noteController,
                decoration: InputDecoration(
                    filled: true,
                    hintStyle: TextStyle(
                        color: Colors.black12,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none),
                    fillColor: Colors.grey[200],
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.black, width: 1),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Colors.black38, width: 1)),
                    contentPadding: EdgeInsets.only(
                        left: 15, bottom: 10, top: 0, right: 15),
                    hintText: "Type here..... "),
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Container(
              margin: EdgeInsets.only(
                  left: size.width * 0.06, right: size.width * 0.05),
              height: size.height * 0.18,
              child: buildGridView(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  // margin: EdgeInsets.only(
                  //   left: size.width * 0.15,
                  // ),
                  width: size.width * 0.35,
                  child: RaisedButton(
                    child: Text(
                      "Select Image",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Config.primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5.0))),
                    onPressed: loadAssets,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: size.width * 0.1,
                  ),
                  width: size.width * 0.35,
                  child: RaisedButton(
                    child: Text(
                      "Submit ",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Config.primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5.0))),
                    onPressed: () {},
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  void changeSecondaryIndex(int index, String text) {
    setState(() {
      secondaryIndex = index;
      print(text);
    });
  }

  Widget customRadio2(int index, String text) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
        onTap: () {
          changeSecondaryIndex(index, text);
        },
        child: index == 0
            ? Text(
                "",
                style: TextStyle(fontSize: 0.01),
              )
            : Container(
                height: size.height * 0.1,
                width: size.width * 0.25,
                margin: EdgeInsets.only(
                  left: size.width * 0.05,
                ),
                padding: EdgeInsets.only(
                  left: size.width * 0.02,
                ),
                decoration: BoxDecoration(
                  color: secondaryIndex == index
                      ? Color(0xff101f40)
                      : Colors.white,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                      width: 1,
                      color: secondaryIndex == index
                          ? Color(0xff101f40)
                          : Colors.black12),
                ),
                child: Row(
                  children: [
                    SizedBox(width: 5),
                    secondaryIndex == index
                        ? Icon(
                            Icons.radio_button_checked,
                            color: Colors.white,
                            size: 0,
                          )
                        : Icon(
                            Icons.radio_button_unchecked,
                            size: 0,
                          ),
                    SizedBox(width: 5),
                    Text(text,
                        style: TextStyle(
                            color: secondaryIndex == index
                                ? Colors.white
                                : Color(0xff101f40),
                            fontSize: 16)),
                  ],
                )));
  }

  text(String text, FontWeight fontWeight) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Text(
        text,
        style: TextStyle(
            color: Colors.black,
            fontWeight: fontWeight,
            fontSize: 18,
            decoration: TextDecoration.none),
      ),
    );
  }
}
