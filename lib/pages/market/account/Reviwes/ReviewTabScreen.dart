import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

import 'NestedTabbar.dart';
import 'OrderHistory.dart';

class RviewTabScreen extends StatefulWidget {
  int colorVal = 0xff84020e;
  int intialIndex;
  RviewTabScreen({this.intialIndex});
  @override
  _RviewTabScreenState createState() => _RviewTabScreenState();
}

class _RviewTabScreenState extends State<RviewTabScreen>
    with TickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(
      vsync: this,
      length: 2,
    );
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {
      widget.colorVal = 0xff84020e;
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar:  appBarWithIcons(context, "My Orders"),
       
      body: DefaultTabController(
        length: 2,
        child: Column(
          children: <Widget>[
            TabBar(
              controller: _tabController,
              indicatorSize: TabBarIndicatorSize.tab,
              indicatorColor: Colors.red,
              unselectedLabelColor: Colors.grey,
              tabs: <Widget>[
                Tab(child: text("To Reviews", 0)),
                Tab(
                  child: text("History", 1),
                ),
              ],
            ),
            SizedBox(height: 10),
             
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  NestedTabbar(),
                  OrderHistory(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

//icons for app bar
  Widget iconInAppbar({IconData icon, String label, Function onTap}) {
    return IconButton(icon: Icon(icon), onPressed: onTap);
  }

//text for tabbar tabs
  text(String title, int number) {
    return Text(
      title,
      style: TextStyle(
          color: _tabController.index == number ? Colors.red : Colors.black),
      textAlign: TextAlign.start,
    );
  }

//for pop up menu in app barr
  void choiceAction(String choice) {
    if (choice == Constants.Home) {
      print('Home');
    } else if (choice == Constants.Messages) {
      print('Messages');
    } else if (choice == Constants.MyAccount) {
      print('MyAccount');
    } else if (choice == Constants.NeedHelp) {
      print('NeedHelp');
    } else if (choice == Constants.Feedback) {
      print('Feedback');
    }
  }

//data in tabs
  payScreenContainer() {
    return Column(
      children: [
        SizedBox(
          height: 20,
        ),
        Image.asset(
          "assets/images/empty.png",
          height: 200,
          color: Config.primaryColor,
        ),
        Text(
          "There are no orders placed yet",
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.normal),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: 50,
        ),
        RaisedButton(
          onPressed: () {},
          textColor: Colors.white,
          padding: const EdgeInsets.all(0.0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(2.0)),
          child: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xFF0D47A1),
                    Color(0xFF1976D2),
                    Color(0xFF42A5F5),
                  ],
                ),
                borderRadius: BorderRadius.all(Radius.circular(2.0))),
            padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
            child:
                const Text('Continue Shopping', style: TextStyle(fontSize: 20)),
          ),
        ),
      ],
    );
  }
//nested Tab Sreens
  subTabScreens() {
    TabController _tabController;
    return Container(
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: DefaultTabController(
        length: 4,
        child: Column(
          children: <Widget>[
            TabBar(
              isScrollable: true,
              controller: _tabController,
              indicatorSize: TabBarIndicatorSize.tab,
              indicatorColor: Colors.red,
              unselectedLabelColor: Colors.grey,
              tabs: <Widget>[
                Tab(child: text("For you", 0)),
                Tab(
                  child: text("Fashion", 1),
                ),
                Tab(
                  child: text("Health&beauty", 2),
                ),
                Tab(
                  child: text("Mobiles", 3),
                ),
              ],
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  payScreenContainer(),
                  payScreenContainer(),
                  payScreenContainer(),
                  payScreenContainer(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
