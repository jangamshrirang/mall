import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/Activities/ActivityExpantion.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

class InAndOutDoor extends StatefulWidget {
  String  title;
  InAndOutDoor({this.title});

  @override
  _InAndOutDoorState createState() => _InAndOutDoorState();
}

class _InAndOutDoorState extends State<InAndOutDoor> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithSearchIcons(context, widget.title,(){}),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Wrap(
              spacing: 0,
              children: [
                activityCard(
                    "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                    "Sand Dunes 4x4",
                    "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.22),
                activityCard(
                    "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                    "Sand Dunes 4x4",
                    "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.22),
                activityCard(
                    "assets/activities/rodrigo-lourenco-m_VDzGhvg_8-unsplash.jpg",
                    "Island Hoping",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.22),
                activityCard(
                    "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                    "Sand Dunes 4x4",
                    "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.22),
                activityCard(
                    "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                    "Sand Dunes 4x4",
                    "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.22),
                activityCard(
                    "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                    "Sand Dunes 4x4",
                    "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.22),
                activityCard(
                    "assets/activities/rodrigo-lourenco-m_VDzGhvg_8-unsplash.jpg",
                    "Island Hoping",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.22),
                activityCard(
                    "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                    "Sand Dunes 4x4",
                    "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                    size.height * 0.3,
                    size.width * 0.45,
                    size.height * 0.22),
              ],
            )
          ],
        ),
      ),
    );
  }

  activityCard(
      String image, title, subtitle, double heigt, widthh, blacktextmargin) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ActivityExpantion(
                      title: title,
                      subtitle: subtitle,
                      image: image,
                    )));
      },
      child: Container(
          width: widthh,
          height: heigt,
          margin: EdgeInsets.only(bottom: 10, left: 10, top: 5, right: 5),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 1,
                blurRadius: 3,
                offset: Offset(1, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.all(Radius.circular(8)),
            image: DecorationImage(
              image: AssetImage(
                image,
              ),
              fit: BoxFit.fill,
            ),
          ),
          child: Container(
              //text container
              decoration: BoxDecoration(
                color: Colors.black54,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8)),
              ),
              margin: EdgeInsets.only(
                top: blacktextmargin, //22
              ),
              padding: EdgeInsets.only(
                  left: size.width * 0.02,
                  right: size.width * 0.01,
                  top: size.height * 0.01),
              height: size.height * 0.06,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.normal,
                        color: Colors.white,
                        fontSize: size.height * 0.022),
                  ),
                  Text(
                    subtitle,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.normal,
                        color: Colors.white70,
                        fontSize: size.height * 0.018),
                  ),
                ],
              ))),
    );
  }
}
