import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/home/live/home.dart';

class LiveTab extends StatefulWidget {
  @override
  _LiveTabState createState() => _LiveTabState();
}

class _LiveTabState extends State<LiveTab> {
  @override
  Widget build(BuildContext context) {
    return LiveHomePage();
  }
}
