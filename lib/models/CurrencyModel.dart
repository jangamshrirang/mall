class CurrencyModel {
    CurrencyModel({
        this.to,
        this.rate,
        this.from,
    });

    String to;
    double rate;
    String from;

    factory CurrencyModel.fromJson(Map<String, dynamic> json) => CurrencyModel(
        to: json["to"],
        rate: json["rate"].toDouble(),
        from: json["from"],
    );

    Map<String, dynamic> toJson() => {
        "to": to,
        "rate": rate,
        "from": from,
    };
}
