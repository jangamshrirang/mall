import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:wblue_customer/models/Restaurant/MyOrderListModel.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();

class PMyOrderList with ChangeNotifier {
  List _myOrders = List();
  bool _isLoading = true;

  UnmodifiableListView get myOrders =>
      UnmodifiableListView(_myOrders);
  bool get isLoading => _isLoading;

  Future<void> fetchrMyOrderList() async {
    setLoading(true);
    _myOrders = await fetchMyOrderList();
    setLoading(false);

    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future<List> fetchMyOrderList() async {
  APIResponse res = await _dioClient.privatePost('/restaurant/order/my-order-list');
  if (res.code >= 400) {
    return [];
  }
  print(res.data);
  return res.data;
}

