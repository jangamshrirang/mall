import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_rating.dart';
import 'package:wblue_customer/widgets/w_ribbon.dart';

class WHotelItemWidget extends StatefulWidget {
  @override
  _WHotelItemWidgetState createState() => _WHotelItemWidgetState();
}

class _WHotelItemWidgetState extends State<WHotelItemWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        ExtendedNavigator.of(context).root.push('/hotels/single');
      },
      child: Container(
        padding: EdgeInsets.only(top: 8.0, left: 8.0, bottom: 8.0),
        margin: EdgeInsets.only(bottom: 8.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(color: Colors.grey, offset: Offset(0, 1)),
          ],
        ),
        child: Row(
          children: <Widget>[
            Container(
              height: 70.0,
              width: 70.0,
              child: WImageWidget(
                url: '',
                placeholder: ExactAssetImage('assets/images/samples/hotel.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(width: 16.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Very Long Name of a Hotel or Apartment',
                    style: TextStyle(fontSize: 12.0),
                  ),
                  Text(
                    'Location Here',
                    style: TextStyle(fontSize: 8.0),
                  ),
                  WRatingsWidget(
                    5.0,
                    size: 12,
                  ),
                ],
              ),
            ),
            Column(
              children: <Widget>[
                WRibbonWidget(
                  label: 'QAR 120.0',
                ),
                SizedBox(height: 8.0),
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 12.0),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black12,
                      ),
                      borderRadius: BorderRadius.circular(50.0),
                    ),
                    child: Text(
                      'Book Here',
                      style: TextStyle(fontSize: 10.0),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
