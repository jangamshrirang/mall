import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

import 'LadyScreen.dart';

class L_Expansion extends StatefulWidget {
  @override
  _L_ExpansionState createState() => _L_ExpansionState();
}

class _L_ExpansionState extends State<L_Expansion> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithIcons(context, ""),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                height: size.height * 0.54,
                child: LadyScreen(
                  tapped: true,
                  from: "Not",
                )),
            Container(
              margin: EdgeInsets.only(
                left: size.width * 0.02,
                right: size.width * 0.02,
              ),
              height: size.height * 0.45,
              // color: Colors.blue,
              child: Column(
                children: [
                  text("Quick  Details", Colors.black, size.height * 0.025),
                  SizedBox(
                    height: size.height * 0.01,
                  ),
                  rowText("Place of Origin:", "Nukhbaa Women's Abaya",
                      Colors.grey[200]),
                  rowText("Product:", "Dress", Colors.white),
                  rowText("Mobile Number:", "5555 5555", Colors.grey[200]),
                  Container(
                    margin: EdgeInsets.only(left: 0),
                    color: Colors.white,
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            AutoSizeText(
                              'Specifications',
                              style: TextStyle(fontSize: 16),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                            Icon(MdiIcons.chevronRight)
                          ],
                        ),
                        AutoSizeText(
                          'Brand, Model, Box Content',
                          style: TextStyle(fontSize: 14, color: Colors.grey),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          minFontSize: 14,
                        ),
                        Divider(color: Colors.grey),
                        AutoSizeText(
                          'Return & Warranty',
                          style: TextStyle(fontSize: 16),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          minFontSize: 16,
                        ),
                        SizedBox(height: 5.0),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    MdiIcons.subdirectoryArrowRight,
                                    size: 16,
                                  ),
                                  Expanded(
                                    child: AutoSizeText(
                                      '7 Days return to seller change of mind is not applicable',
                                      style: TextStyle(fontSize: 16),
                                      overflow: TextOverflow.ellipsis,
                                      minFontSize: 16,
                                      maxLines: 5,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Row(
                                children: [
                                  Icon(
                                    MdiIcons.shieldOutline,
                                    size: 16,
                                  ),
                                  AutoSizeText(
                                    '2 years warranty',
                                    style: TextStyle(fontSize: 16),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    minFontSize: 16,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  btns()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  text(String text, Color color, double fontSize) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: size.width * 0.01),
      width: double.infinity,
      child: Text(text, style: TextStyle(color: color, fontSize: fontSize)),
    );
  }

  button(String title, Function onTap, Color color) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.4,
      child: RaisedButton(
          child: Text(
            title,
            style: TextStyle(color: Colors.white),
          ),
          color: color,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          onPressed: onTap),
    );
  }

  rowText(String text1, text2, Color color) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.04,
      width: double.infinity,
      color: color,
      child: Row(
        children: [
          Container(
            //  color: Colors.red,
            width: size.width * 0.3,
            child: text(text1, Colors.black, size.height * 0.02),
          ),
          Container(
            //  color: Colors.red,
            width: size.width * 0.5,
            child: text(text2, Colors.black, size.height * 0.02),
          ),
        ],
      ),
    );
  }

  btns() {
    return Row(
      children: [
        Spacer(),
        button("Add To Cart", () {}, Config.primaryColor),
        Spacer(),
        button("buy Now", () {
          // Navigator.push(
          //     context,
          //     MaterialPageRoute(
          //         builder: (context) => Payment(
          //               from: "rest",
          //             )));
        }, Config.primaryColor),
        Spacer(),
      ],
    );
  }
}
