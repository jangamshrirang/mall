import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/Brands/BrandProduts.dart';

class PremiumTab extends StatefulWidget {
  @override
  _PremiumTabState createState() => _PremiumTabState();
}

class _PremiumTabState extends State<PremiumTab> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white, //Config.bodyColor,

        body: SafeArea(
          child: SingleChildScrollView(
            child: Stack(
              children: [
                Column(
                  children: [
                    _searchBar(),
                    Wrap(
                      spacing: 0,
                      children: [
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/1-5.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/2-8.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/4-5.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/5-5.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/12-1.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/14-1.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/9-3.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/22-min-2.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/26-min-1.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/1-5.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/2-8.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/4-5.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/5-5.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/12-1.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/14-1.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/9-3.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/22-min-2.jpg",
                        ),
                        LayoutThree(
                          image:
                              "https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_750/https://www.designhill.com/design-blog/wp-content/uploads/2019/04/26-min-1.jpg",
                        ),
                      ],
                    ),
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    sortData();
                  },
                  child: Container(
                    height: size.height * 0.05,
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        border: Border.all(width: 0.5, color: Colors.black26)),
                    margin: EdgeInsets.only(left: size.width * 0.85, top: 2),
                    child: Image.network(
                      "https://i.pinimg.com/originals/03/8e/85/038e8523b874a3911abc668e36ba6571.png",
                      height: size.height * 0.03,
                      color: Colors.black54,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  void sortData() {
    var size = MediaQuery.of(context).size;
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            return Container(
              height: size.height * 0.6,
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  )),
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                children: ListTile.divideTiles(
                  context: context,
                  tiles: [
                    SizedBox(
                      height: 10,
                    ),
                    listtile(context, "Featured", () {
                      print("Featured");
                    }),
                    listtile(context, "Newest", () {
                      print("Newest");
                    }),
                    listtile(context, "Oldest", () {
                      print("Oldest");
                    }),
                    listtile(context, "Best Sellers", () {
                      print("Price : Best Sellers");
                    }),
                    listtile(context, "Price : Low to High", () {
                      print("Price : Low to High");
                    }),
                    listtile(context, "Price :  High to Low", () {
                      print("Price :  High to Low");
                    }),
                    listtile(context, "A to Z", () {
                      print("A to Z");
                    }),
                  ],
                ).toList(),
              ),
            );
          },
        );
      },
    );
  }

  listtile(BuildContext context, String title, Function ontap) {
    return ListTile(
      // leading: Image.network(image, height: 30, color: Colors.amber),
      title: Text(
        title,
      ),
      onTap: ontap,
    );
  }

  settingsScreenNavigation(BuildContext context, screen) {
    return Navigator.push(
        context, MaterialPageRoute(builder: (context) => screen));
  }

  _searchBar() {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(
          left: size.width * 0.03, right: size.width * 0.10, top: 2),
      height: size.height * 0.05,
      width: size.width * 0.82,
      child: TextField(
        cursorColor: Colors.black,
        decoration: InputDecoration(
            filled: true,
            labelStyle:
                TextStyle(color: Colors.black38, fontWeight: FontWeight.normal),
            fillColor: Colors.white,
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black26, width: 1),
              borderRadius: BorderRadius.circular(00.0),
            ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(00.0)),
                borderSide: BorderSide(color: Colors.black26, width: 1)),
            labelText: "Search..."),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            // _notesForDisplay = _notes.where((note) {
            //   var noteTitle = note.title.toLowerCase();
            //   return noteTitle.contains(text);
            // }
            // ).toList();
          });
        },
      ),
    );
  }
}

class LayoutThree extends StatelessWidget {
  String image;
  LayoutThree({
    this.image,
  });
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => BrandProductsPage()));
      },
      child: Stack(
        children: [
          Container(
              width: size.width * 0.9,
              height: size.height * 0.20,
              margin: EdgeInsets.only(
                  bottom: 5, left: size.width * 0.03, top: 5, right: 2),
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.4),
                    spreadRadius: 1,
                    blurRadius: 3,
                    offset: Offset(1, 3), // changes position of shadow
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(10)),
                image: DecorationImage(
                  image: NetworkImage(
                    image,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              child: Container(
                  //text container
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(4),
                        bottomRight: Radius.circular(4)),
                  ),
                  margin: EdgeInsets.only(
                    top: size.height * 0.15,
                  ),
                  padding: EdgeInsets.only(
                      left: size.width * 0.01,
                      right: size.width * 0.01,
                      top: size.height * 0.01),
                  height: size.height * 0.03,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Brand Name",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.normal,
                            fontSize: size.height * 0.023),
                      ),
                    ],
                  ))),
        ],
      ),
    );
  }
}
