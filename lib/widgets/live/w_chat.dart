import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/data/market/d_chat.dart';

class WLiveChatWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      width: MediaQuery.of(context).size.width * .65,
      child: SingleChildScrollView(child: _getChat(context)),
    );
  }

  Widget _getChat(BuildContext context) {
    List<Widget> _mChat = [];
    List<Widget> mChat = DChatData.chat
        .asMap()
        .map((i, item) {
          return MapEntry(
              i,
              Container(
                decoration: BoxDecoration(color: Colors.black.withOpacity(.7), borderRadius: BorderRadius.circular(80)),
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 3),
                margin: EdgeInsets.only(bottom: 5),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    _message(label: item['name'], isSender: true),
                    _message(label: item['message']),
                  ],
                ),
              ));
        })
        .values
        .toList();

    _mChat.addAll(mChat);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: _mChat,
    );
  }

  Widget _message({String label, bool isSender: false}) {
    return Text(
      isSender ? '$label: ' : label,
      style: TextStyle(color: isSender ? Colors.white70 : Colors.white, fontSize: 18),
    );
  }
}
