import 'package:wblue_customer/services/dio/dio_client.dart';

class CountryModel {
  final String name;
  final String slug;
  final String iso;
  final String dialCode;

  CountryModel({
    this.name,
    this.slug,
    this.iso,
    this.dialCode,
  });

  static List defaultItems = [
    {'slug': 'qatar', 'name': 'Qatar', 'iso': 'qat', 'dialCode': '+974'},
    {'slug': 'bahrain', 'name': 'Bahrain', 'iso': 'bhr'},
    {'slug': 'oman', 'name': 'Oman', 'iso': 'omn'},
    {'slug': 'saudi-arabia', 'name': 'Saudi Arabia', 'iso': 'sau'},
    {'slug': 'jordan', 'name': 'Jordan', 'iso': 'jor'},
    {
      'slug': 'united-arab-emirates',
      'name': 'United Arab Emirate',
      'iso': 'are'
    },
  ];

  factory CountryModel.fromJson(Map<String, dynamic> json) {
    return CountryModel(
      name: json['name'],
      slug: json['slug'],
      iso: json['iso'],
      dialCode: json['dialCode'],
    );
  }
}

class StatesModel {
  final int id;
  final String name;
  final List cityInfo;

  StatesModel({this.id, this.name, this.cityInfo});

  factory StatesModel.fromJson(Map<String, dynamic> json) {
    return StatesModel(
        id: json['id'], name: json['name'], cityInfo: json['city_info']);
  }

  DioClient _dioClient = DioClient();

  Future<List> list() async {
    APIResponse res = await _dioClient.publicGet('/get-states');

    if (res.code >= 400) return [];
    return res.data;
  }
}

class ShippingModel {
  final int id;
  final String name;
  final int maxRate;
  final int doStorePickUp;
  final int doOversea;
  final String number;
  final String email;
  final String location;
  final String website;
  final String iso;

  ShippingModel({
    this.id,
    this.name,
    this.maxRate,
    this.doStorePickUp,
    this.doOversea,
    this.number,
    this.email,
    this.location,
    this.website,
    this.iso,
  });

  factory ShippingModel.fromJson(Map<String, dynamic> json) {
    return ShippingModel(
      id: json['id'],
      name: json['name'],
      maxRate: json['max_rate'],
      doStorePickUp: json['do_store_pickup'],
      doOversea: json['do_oversea'],
      number: json['number'],
      email: json['email'],
      location: json['location'],
      website: json['website'],
      iso: json['iso'],
    );
  }

  DioClient _dioClient = DioClient();

  Future<List> list() async {
    APIResponse res = await _dioClient.publicGet('/get-shipping-providers');

    if (res.code >= 400) return [];
    return res.data;
  }
}
