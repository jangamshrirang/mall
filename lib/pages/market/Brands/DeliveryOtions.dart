import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:math' show cos, sqrt, asin;

import 'package:wblue_customer/Bloc/Statemanagement.dart';

class DeliveryOption extends StatefulWidget {
  int colorVal = 0xff84020e;
  int kilometer;
  @override
  _DeliveryOptionState createState() => _DeliveryOptionState();
}

class _DeliveryOptionState extends State<DeliveryOption>
    with TickerProviderStateMixin {
  TabController _tabController;
  int langValue;
  double lat1 = 25.275008;
  double lang1 = 51.537721;
  String kilometer;

  String _locationMessage = "";
  var lat, long;
  //getting current location
  void _getCurrentLocation() async {
    final position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print(position);
    double totalDistance = calculateDistance(
        position.latitude, position.longitude, 25.275008, 51.537721);
    print("formulaa         " + "$totalDistance");
    kilometer = totalDistance.toString();
    print('${kilometer[0]}');
    stateManagment.setKm(int.parse(kilometer[0]));
    //caluculating deliviry charge
    priceCaluculation(int.parse(kilometer[0]), 7);
  }

//distance calulation formula
  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 3, initialIndex: 0);
    _tabController.addListener(_handleTabSelection);
    _getCurrentLocation();
  }

  void _handleTabSelection() {
    setState(() {
      widget.colorVal = 0xff84020e;
      _tabController.index == 1
          ? priceCaluculation(stateManagment.km, 7)
          : priceCaluculation(stateManagment.km, 9);
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          backgroundColor: Colors.white,
          body: Container(
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.12,
            child: DefaultTabController(
              length: 3,
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.03,
                    child: TabBar(
                      labelColor: Colors.orangeAccent,
                      controller: _tabController,
                      indicatorSize: TabBarIndicatorSize.label,
                      indicatorColor: Colors.transparent,
                      unselectedLabelColor: Colors.grey,
                      tabs: <Widget>[
                        Tab(icon: container(context, "Standerd", 0)),
                        Tab(icon: container(context, "Economy", 1)),
                        Tab(
                            icon: Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                          child: Center(
                            child: Text(
                              "Express",
                              style: TextStyle(
                                  color: _tabController.index == 2
                                      ? Colors.red
                                      : Colors.black54,
                                  fontWeight: _tabController.index == 2
                                      ? FontWeight.bold
                                      : FontWeight.normal),
                            ),
                          ),
                        )),
                      ],
                    ),
                  ),
                  Expanded(
                    child: TabBarView(
                      controller: _tabController,
                      children: <Widget>[
                        ///// APPLICANT TAB////////////
                        expantionContainer(Colors.red, 0, "Standerd"),
                        expantionContainer(Colors.blue, 1, "Economy"),

                        expantionContainer(Colors.yellow, 2, "Express"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }

  container(BuildContext context, String name, int number) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.1,
      decoration: BoxDecoration(
          border: Border(
              right: BorderSide(
                  color: Colors.black12, width: 1, style: BorderStyle.solid))),
      child: Center(
        child: Text(
          name,
          style: TextStyle(
              color:
                  _tabController.index == number ? Colors.red : Colors.black54,
              fontWeight: _tabController.index == number
                  ? FontWeight.bold
                  : FontWeight.normal),
          textAlign: TextAlign.start,
        ),
      ),
    );
  }

  expantionContainer(
    Color color,
    int number,
    String name,
  ) {
    return Container(
      color: Colors.grey[200],
      margin: EdgeInsets.only(left: 10, right: 0),
      height: 50,
      child: Center(
          child: Row(
        children: [
    
          Text(
            " $name Delivery Charage to Your Location : ",
            style: TextStyle(fontSize: 14),
          ),
          Text(
            number == 0 ? "5 QAR" : number == 1 ? "7 QAR " : "8 QAR",
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
          ),
      
        ],
      )),
    );
  }

  priceCaluculation(int km, int baseprice) {
    if (km >= 6) {
      var above5Km = km * 0.5 + baseprice;
      print(above5Km);
    } else {
      print("km in below 5" + "{$km}");
    }
  }
}
