import 'package:dio/dio.dart';
import 'package:wblue_customer/models/Restaurant/R.CatogeryModel.dart';
import 'package:wblue_customer/models/Restaurant/resturentMenuModel.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:wblue_customer/services/helper.dart';

DioClient _dioClient = DioClient();

class RGetCartHelper with ChangeNotifier {
  List _rFoodCartList = List();
  bool _isLoading = true;

  UnmodifiableListView get rFoodCart => UnmodifiableListView(_rFoodCartList);
  bool get isLoading => _isLoading;

  Future<void> fetchRCartList() async {
    setLoading(true);
    _rFoodCartList = await fetchCart();
    setLoading(false);

    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future<List> fetchCart() async {
  APIResponse res =
      await _dioClient.privatePost('/restaurant/cart/my-cart-list');
  if (res.code >= 400) {
    return [];
  }
  print(res.data);
  return res.data;
}
