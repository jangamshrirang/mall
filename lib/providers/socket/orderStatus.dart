import 'package:flutter/material.dart';

class OrderStatusScocket extends ChangeNotifier {
  Map<String, dynamic> updatedStatusData = new Map<String, dynamic>();
  Map get updatedstatusData => updatedStatusData;
  bool _isUpdated = false;
  bool get isUpdated => _isUpdated;
  bool _canBack = false;

  bool get canBack => _canBack;

  void whenOrderStatusUpdated(data) {
    _isUpdated = true;
    updatedStatusData = data;
    notifyListeners();
  }

  void setOrderStatusUpdated(bool status) {
    _isUpdated = status;
    notifyListeners();
  }

  void setBackStatus(bool status) {
    _canBack = status;
  }
}
