import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/cart.dart';
import 'package:wblue_customer/pages/market/Brands/Variations.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_rating.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

import 'DeliveryOtions.dart';
import 'DistanceCaluculation.dart';
import 'Vendor/VendorDetails.dart';

class ProductExpantion extends StatefulWidget {
  String image, pTitle;
  double price;
  ProductExpantion({this.image, this.pTitle, this.price});
  @override
  _ProductExpantionState createState() => _ProductExpantionState();
}

class _ProductExpantionState extends State<ProductExpantion> {
  int qty = 1;
  final noteController = TextEditingController();
  bool fav;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "Products"),
      body: Container(
        child: SingleChildScrollView(
          child: Wrap(
            children: <Widget>[
              Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.width * 0.8,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(
                          'https://i5.walmartimages.com/asr/6e1b1508-d1d8-46e5-93a3-8a35612c61b2_1.e0e3988fd0851d2e5025d935a8eee6f7.jpeg',
                        ),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Positioned(
                    top: size.height * 0.35,
                    left: size.width * 0.85,
                    child: fav == false || fav == null
                        ? IconButton(
                            icon: Icon(Icons.favorite_border,
                                size: 20, color: Colors.black38),
                            onPressed: () {
                              setState(() {
                                fav = true;
                              });
                            })
                        : IconButton(
                            icon: Icon(Icons.favorite,
                                size: 20, color: Colors.red),
                            onPressed: () {
                              setState(() {
                                fav = false;
                              });
                            }),
                  )
                ],
              ),
              Container(
                color: Colors.white,
                padding: EdgeInsets.only(bottom: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ListTile(
                      title: Text(
                        widget.pTitle,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: size.height * 0.03),
                      ),
                      subtitle: Text(
                        'QAR ' + "${widget.price}",
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontWeight: FontWeight.w300, fontSize: 15.0),
                      ),
                      trailing: WRatingsWidget(3.5, size: 10.0, compact: true),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: size.width * 0.05,
                          right: size.width * 0.02,
                          bottom: 10),
                      child: Text(
                        'Real-time audio calibration preserves a premium listening experience',
                        style: TextStyle(
                            fontWeight: FontWeight.w300, fontSize: 15.0),
                      ),
                    ),

                    //Star Ratings
                    Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(width: size.width * 0.05),
                        Row(
                          children: [
                            WRatingsWidget(50 / 100),
                            SizedBox(width:0),
                            AutoSizeText(
                              '(4) Reviews',
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.grey.withOpacity(0.9)),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                          ],
                        ),
                        SizedBox(width: size.width * 0.05),
                        AutoSizeText(
                          'Open Until : 11:59 PM',
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.green.withOpacity(0.9)),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          minFontSize: 16,
                        ),
                      ],
                    ),
                    Divider(),
                    //Pramotions
                    textWithRow('Promotions', 'Save more with wallet', () {}),
                    Divider(),
                    //delivery
                    textWithRow('Delivery', 'QR ${250}', () {}),
                    Divider(),
                    textWithRow('Variations', '  ', () {
                      // navigation(context, ColorVariations());
                      variatioBottomSheet(context);
                    }),
                    Divider(),
                    SizedBox(
                        height: size.height * 0.08, child: DeliveryOption()),
                    Divider(),
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      color: Colors.white,
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              AutoSizeText(
                                'Specifications',
                                style: TextStyle(fontSize: 16),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                minFontSize: 16,
                              ),
                              Icon(MdiIcons.chevronRight)
                            ],
                          ),
                          AutoSizeText(
                            'Brand, Model, Box Content',
                            style: TextStyle(fontSize: 14, color: Colors.grey),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            minFontSize: 14,
                          ),
                          Divider(color: Colors.grey),
                          AutoSizeText(
                            'Return & Warranty',
                            style: TextStyle(fontSize: 16),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            minFontSize: 16,
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                flex: 1,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      MdiIcons.subdirectoryArrowRight,
                                      size: 16,
                                    ),
                                    Expanded(
                                      child: AutoSizeText(
                                        '7 Days return to seller change of mind is not applicable',
                                        style: TextStyle(fontSize: 16),
                                        overflow: TextOverflow.ellipsis,
                                        minFontSize: 16,
                                        maxLines: 5,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Icon(
                                      MdiIcons.shieldOutline,
                                      size: 16,
                                    ),
                                    AutoSizeText(
                                      '2 years warranty',
                                      style: TextStyle(fontSize: 16),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      minFontSize: 16,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    Divider(),
                    Container(
                      color: Colors.white,
                      margin: EdgeInsets.only(left: 10),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AutoSizeText(
                              'Ratings & Reviews (0)',
                              style: TextStyle(fontSize: 16),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: AutoSizeText(
                                  'This products has no reviews.',
                                  maxLines: 2,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.grey),
                                  overflow: TextOverflow.ellipsis,
                                  minFontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Divider(),
                    // SizedBox(height: 10.0),
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AutoSizeText(
                              'Questions about this Products (0)',
                              style: TextStyle(fontSize: 16),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: AutoSizeText(
                                  'There are no questions yet. Ask the seller now and their answer will show here.',
                                  maxLines: 2,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.grey),
                                  overflow: TextOverflow.ellipsis,
                                  minFontSize: 16,
                                ),
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                FlatButton(
                                  onPressed: () {},
                                  child: AutoSizeText('Ask Questions',
                                      style: TextStyle(
                                          color: Config.primaryColor)),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Divider(),
                    // note Bloc
                    Container(
                      margin: EdgeInsets.only(left: 20),
                      height: size.height * 0.1,
                      width: size.width * 0.9,
                      child: TextFormField(
                        maxLines: 5,
                        cursorColor: Colors.black,
                        controller: noteController,
                        decoration: InputDecoration(
                            filled: true,
                            hintStyle: TextStyle(
                                color: Colors.black12,
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.none),
                            fillColor: Colors.grey[200],
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.black, width: 1),
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(
                                    color: Colors.black38, width: 1)),
                            contentPadding: EdgeInsets.only(
                                left: 15, bottom: 10, top: 0, right: 15),
                            hintText: "Type here..... "),
                      ),
                    ),
                    //Vendor Container
                    GestureDetector(
                      onTap: () {
                        navigation(context, VendorDetails()); //
                      },
                      child: Container(
                        color: Colors.white,
                        margin: EdgeInsets.only(left: 10),
                        child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        WImageWidget(
                                          placeholder: AssetImage(
                                              'assets/images/market/vendor.png'),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: AutoSizeText(
                                            'Seller seller',
                                            style: TextStyle(fontSize: 18),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            minFontSize: 18,
                                          ),
                                        ),
                                      ],
                                    ),
                                    FlatButton(
                                      onPressed: () {},
                                      child: AutoSizeText(
                                        'Follow',
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Config.primaryColor),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        minFontSize: 18,
                                      ),
                                    )
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Column(
                                            children: [
                                              AutoSizeText(
                                                '92 %',
                                                style: TextStyle(fontSize: 16),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                minFontSize: 16,
                                              ),
                                              AutoSizeText(
                                                'Positive Seller Ratings',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    color: Colors.grey),
                                                maxLines: 2,
                                                textAlign: TextAlign.center,
                                                overflow: TextOverflow.ellipsis,
                                                minFontSize: 16,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border(
                                              right: BorderSide(
                                                color: Colors.grey,
                                                width: 1.5,
                                              ),
                                              left: BorderSide(
                                                color: Colors.grey,
                                                width: 1.5,
                                              ),
                                            ),
                                          ),
                                          child: Column(
                                            children: [
                                              AutoSizeText(
                                                '92 %',
                                                style: TextStyle(fontSize: 16),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                minFontSize: 16,
                                              ),
                                              AutoSizeText(
                                                'Positive Seller Ratings',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    color: Colors.grey),
                                                textAlign: TextAlign.center,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                minFontSize: 16,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: Column(
                                            children: [
                                              AutoSizeText(
                                                '92 %',
                                                style: TextStyle(fontSize: 16),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                minFontSize: 16,
                                              ),
                                              AutoSizeText(
                                                'Positive Seller Ratings',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    color: Colors.grey),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                textAlign: TextAlign.center,
                                                minFontSize: 16,
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      WRoundedButton(
                                        onCustomButtonPressed: () {},
                                        elevation: 0.0,
                                        child: AutoSizeText('Chat'),
                                      ),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      WRoundedButton(
                                        onCustomButtonPressed: () {},
                                        elevation: 0.0,
                                        labelColor: Colors.white,
                                        btnColor: Config.primaryColor,
                                        child: AutoSizeText('Visit Store'),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      ),
                    ),
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: WButtonWidget(
                        title: 'add to cart',
                        expanded: true,
                        onPressed: () {
                          setState(() {
                            // Cart('Restaurant Name').add(CartItem(name: 'noodle', price: 100, qty: qty));
                            // Navigator.pop(context);
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) =>
                            //             DeliveryOption()));
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  textWithRow(String title, text, Function ontap) {
    return GestureDetector(
      onTap: ontap,
      child: Container(
        margin: EdgeInsets.only(left: 10),
        color: Colors.white,
        padding: EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            AutoSizeText(
              title,
              style: TextStyle(fontSize: 16),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              minFontSize: 16,
            ),
            Row(
              children: [
                AutoSizeText(
                  text,
                  style: TextStyle(fontSize: 16),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  minFontSize: 16,
                ),
                Icon(MdiIcons.chevronRight)
              ],
            )
          ],
        ),
      ),
    );
  }

  navigation(BuildContext context, screen) {
    return Navigator.push(
        context, MaterialPageRoute(builder: (context) => screen));
  }
}
