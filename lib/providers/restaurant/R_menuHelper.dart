import 'package:dio/dio.dart';
import 'package:wblue_customer/models/Restaurant/resturentMenuModel.dart';

Future<Foodmenu> fetchMenu() async {
  final response = await Dio()
      .get('https://run.mocky.io/v3/c2484339-5387-483f-a2ca-dfd06f3c940c');

  if (response.statusCode == 200) {
    return Foodmenu.fromJson(response.data);
  } else {
    throw Exception('Failed to load album');
  }
}
