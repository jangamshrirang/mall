class MyOrderExpantionModel {
    MyOrderExpantionModel({
        this.orderHashId,
        this.orderNumber,
        this.orderTypeInfo,
        this.paymentType,
        this.qrcode,
        this.storeName,
        this.storeMobile,
        this.customerAddress,
        this.customerMobile,
        this.orderList,
        this.subTotal,
        this.shippingRate,
        this.total,
        this.driverName,
        this.driverNumber,
    });

    String orderHashId;
    int orderNumber;
    String orderTypeInfo;
    String paymentType;
    String qrcode;
    String storeName;
    String storeMobile;
    String customerAddress;
    dynamic customerMobile;
    List<OrderList> orderList;
    String subTotal;
    String shippingRate;
    String total;
    dynamic driverName;
    dynamic driverNumber;

    factory MyOrderExpantionModel.fromJson(Map<String, dynamic> json) => MyOrderExpantionModel(
        orderHashId: json["order_hash_id"] == null ? null : json["order_hash_id"],
        orderNumber: json["order_number"] == null ? null : json["order_number"],
        orderTypeInfo: json["order_type_info"] == null ? null : json["order_type_info"],
        paymentType: json["payment_type"] == null ? null : json["payment_type"],
        qrcode: json["qrcode"] == null ? null : json["qrcode"],
        storeName: json["store_name"] == null ? null : json["store_name"],
        storeMobile: json["store_mobile"] == null ? null : json["store_mobile"],
        customerAddress: json["customer_address"] == null ? null : json["customer_address"],
        customerMobile: json["customer_mobile"],
        orderList: json["order_list"] == null ? null : List<OrderList>.from(json["order_list"].map((x) => OrderList.fromJson(x))),
        subTotal: json["sub_total"] == null ? null : json["sub_total"],
        shippingRate: json["shipping_rate"] == null ? null : json["shipping_rate"],
        total: json["total"] == null ? null : json["total"],
        driverName: json["driver_name"],
        driverNumber: json["driver_number"],
    );

    Map<String, dynamic> toJson() => {
        "order_hash_id": orderHashId == null ? null : orderHashId,
        "order_number": orderNumber == null ? null : orderNumber,
        "order_type_info": orderTypeInfo == null ? null : orderTypeInfo,
        "payment_type": paymentType == null ? null : paymentType,
        "qrcode": qrcode == null ? null : qrcode,
        "store_name": storeName == null ? null : storeName,
        "store_mobile": storeMobile == null ? null : storeMobile,
        "customer_address": customerAddress == null ? null : customerAddress,
        "customer_mobile": customerMobile,
        "order_list": orderList == null ? null : List<dynamic>.from(orderList.map((x) => x.toJson())),
        "sub_total": subTotal == null ? null : subTotal,
        "shipping_rate": shippingRate == null ? null : shippingRate,
        "total": total == null ? null : total,
        "driver_name": driverName,
        "driver_number": driverNumber,
    };
}

class OrderList {
    OrderList({
        this.foodId,
        this.name,
        this.price,
        this.quantity,
        this.request,
        this.hashId,
        this.foodInfo,
    });

    int foodId;
    String name;
    String price;
    int quantity;
    String request;
    String hashId;
    FoodInfo foodInfo;

    factory OrderList.fromJson(Map<String, dynamic> json) => OrderList(
        foodId: json["food_id"] == null ? null : json["food_id"],
        name: json["name"] == null ? null : json["name"],
        price: json["price"] == null ? null : json["price"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        request: json["request"] == null ? null : json["request"],
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        foodInfo: json["food_info"] == null ? null : FoodInfo.fromJson(json["food_info"]),
    );

    Map<String, dynamic> toJson() => {
        "food_id": foodId == null ? null : foodId,
        "name": name == null ? null : name,
        "price": price == null ? null : price,
        "quantity": quantity == null ? null : quantity,
        "request": request == null ? null : request,
        "hash_id": hashId == null ? null : hashId,
        "food_info": foodInfo == null ? null : foodInfo.toJson(),
    };
}

class FoodInfo {
    FoodInfo({
        this.full,
        this.thumbnail,
    });

    String full;
    String thumbnail;

    factory FoodInfo.fromJson(Map<String, dynamic> json) => FoodInfo(
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "full": full == null ? null : full,
        "thumbnail": thumbnail == null ? null : thumbnail,
    };
}
