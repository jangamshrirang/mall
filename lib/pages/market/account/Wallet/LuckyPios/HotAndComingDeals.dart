import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';

final List promotions = [
  {
    'image':
        "https://qa.dohabank.com/wp-content/uploads/sites/12/Al-Dana-6th-Draw-Winners-En.jpg",
    'text':
        "Balloon Blast: Cash in from Aug 29 -Sep8 for a chance to win up to Qr 9,000 CREDITS",
    'validDate': "01/05/2020"
  },
  {
    'image': "https://i.ytimg.com/vi/pNpbmjSMUwI/hqdefault.jpg",
    'text':
        "Win up to Qr 5,000 Wallet Credits using your Wmall Wallet or Wcash from Aug",
    'validDate': "02/10/2020"
  },
  {
    'image':
        "https://www.ibs-uae.com/innovate/wp-content/uploads/2015/04/MARK-1200x629.jpg",
    'text':
        "Learn how to activ,cas in,and pay with your Wmall Wallet by cliking here",
    'validDate': "03/08/2020"
  }
];

class HotAndComingDeals extends StatefulWidget {
  @override
  _HotAndComingDealsState createState() => _HotAndComingDealsState();
}

class _HotAndComingDealsState extends State<HotAndComingDeals> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.28,
        width: size.width * 4,
        child: Wrap(
            spacing: 5,
            runSpacing: 5,
            children: List.generate(promotions.length, (index) {
              return Container(
                  width: size.width * 0.85 / 2,
                  height: size.height * 0.25,
                  margin: EdgeInsets.only(left: size.width * 0.05, top: 10),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 3,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      image: DecorationImage(
                        image: NetworkImage(promotions[index]['image']),
                        fit: BoxFit.cover,
                      )),
                  child: Container(
                      //text container
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(7)),
                      ),
                      margin: EdgeInsets.only(
                        top: size.height * 0.155,
                      ),
                      padding: EdgeInsets.only(
                          left: size.width * 0.01,
                          right: size.width * 0.01,
                          top: size.height * 0.01),
                      height: size.height * 0.06,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            promotions[index]['text'],
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: size.height * 0.020),
                          ),
                          SizedBox(height: 8),
                          Text("Valid till:" + promotions[index]['validDate'],
                              style: TextStyle(
                                color: Colors.black38,
                              ))
                        ],
                      )));
            })));
  }
}
