import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:wblue_customer/env/config.dart';

import 'L_customSize.dart';
import 'L_expantion.dart';

class LadyScreen extends StatefulWidget {
  bool tapped;
  String from;
  LadyScreen({this.tapped, this.from});
  @override
  _LadyScreenState createState() => _LadyScreenState();
}

class _LadyScreenState extends State<LadyScreen> {
  final controller = PageController(viewportFraction: 1.0);
  final List<dynamic> colors = [
    0xffFF5733,
    0xff2B2625,
    0xff5052E8,
    0xffFF5733,
    0xff2B2625,
    0xff5052E8,
    0xffFF5733,
    0xff2B2625,
    0xff5052E8
  ];
  final List<String> sizes = [
    "XS",
    'S',
    'M',
    'L',
    'XL',
    'XXL',
    'XXXL',
  ];
  int colorSecondaryIndex;
  int sizeSecondaryIndex;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          setState(() {
            widget.from == "ListHome"
                ? Navigator.push(context,
                    MaterialPageRoute(builder: (context) => L_Expansion()))
                : null;
          });
        },
        child: Container(
            margin: EdgeInsets.only(left: size.width * 0.03, top: 5),
            width: size.width * 0.95,
            height: size.height * 0.55,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Column(
                      children: [
                        Container(
                          height: size.height * 0.4,
                          width: size.width * 0.5,
                          child: PageView(
                            controller: controller,
                            children: <Widget>[
                              bloggerImages('assets/botique/L1.png'),
                              bloggerImages('assets/botique/b2.png'),
                              bloggerImages('assets/botique/b3.png'),
                              bloggerImages('assets/botique/b1.png'),
                            ],
                            pageSnapping: true,
                          ),
                        ),
                        SizedBox(
                          height: size.height * 0.01,
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            bottom: size.height * 0.015,
                          ),
                          child: SmoothPageIndicator(
                            controller: controller,
                            count: 4,
                            effect: ExpandingDotsEffect(
                              activeDotColor: Colors.black38,
                              dotHeight: 05.0,
                              expansionFactor: 2,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: size.height * 0.4,
                      width: size.width * 0.45,
                      color: Colors.white,
                      child: Column(
                        children: [
                          text("Nukhbaa Women's Abaya", Colors.black,
                              size.height * 0.025),
                          SizedBox(height: size.height * 0.001),
                          text(
                              "Silk velvet Floral Print Patchwork Trendy Abaya",
                              Colors.black45,
                              size.height * 0.018),
                          SizedBox(height: size.height * 0.01),
                          text("Colors", Colors.black45, size.height * 0.018),
                          Container(
                            height: size.height * 0.02,
                            margin: EdgeInsets.only(
                                left: size.width * 0.01,
                                top: size.height * 0.005),
                            width: double.infinity,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: colors.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return availableColors(
                                      Color(colors[index]), index);
                                }),
                          ),
                          SizedBox(height: size.height * 0.01),
                          text("Sizes", Colors.black45, size.height * 0.018),
                          SizedBox(height: size.height * 0.005),
                          Wrap(
                            runSpacing: 5,
                            children: List.generate(
                                sizes.length,
                                (index) => availbleSizes(
                                      sizes[index],
                                      size.width * 0.1,
                                      index,
                                    )),
                          ),
                          SizedBox(height: size.height * 0.02),
                          availbleSizes(" CUSTUM SIZE ", size.width * 0.4, 100)
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  height: size.height * 0.06,
                  width: double.infinity,
                  //color: Colors.red,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Spacer(),
                      Container(
                        height: size.height * 0.06,
                        width: size.width * 0.2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            text("QAR 512", Colors.black, size.height * 0.03),
                            text("      Price     ", Colors.black45,
                                size.height * 0.018),
                          ],
                        ),
                      ),
                      Spacer(),
                      Container(
                        height: size.height * 0.06,
                        width: size.width * 0.2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            text("512", Colors.black, size.height * 0.03),
                            text("Products", Colors.black45,
                                size.height * 0.018),
                          ],
                        ),
                      ),
                      Spacer(),
                      Icon(Icons.favorite_border, size: 20),
                      Spacer(),
                      Icon(Icons.chat_bubble_outline, size: 20),
                      Spacer(),
                      Container(
                        height: size.height * 0.05,
                        width: size.width * 0.2,
                        child: text("More Info...", Colors.black45,
                            size.height * 0.018),
                      )
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }

  bloggerImages(String image) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(
        left: size.width * 0.02,
        top: size.height * 0.01,
      ),
      decoration: BoxDecoration(
        color: Colors.transparent,
        image: DecorationImage(
            image: AssetImage(
              image,
            ),
            fit: BoxFit.fill),
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
    );
  }

  text(String text, Color color, double fontSize) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: size.width * 0.01),
      width: double.infinity,
      child: Text(text, style: TextStyle(color: color, fontSize: fontSize)),
    );
  }

  availableColors(Color color, int index) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        widget.tapped == true
            ? changeColorSecondaryIndex(index, color)
            : Navigator.push(context,
                MaterialPageRoute(builder: (context) => L_Expansion()));
      },
      child: Container(
        decoration: BoxDecoration(
          color: Color(
            colors[index],
          ),
          border: Border.all(
              width: 3,
              color: widget.tapped == true
                  ? colorSecondaryIndex == index ? color : Colors.white
                  : color),
          shape: BoxShape.circle,
        ),
        width: size.width * 0.05,
      ),
    );
  }

  void changeColorSecondaryIndex(int index, Color color) {
    setState(() {
      colorSecondaryIndex = index;
      print(color);
    });
  }

  void changeSizeSecondaryIndex(int index, String text) {
    setState(() {
      sizeSecondaryIndex = index;
       print(text);
    });
  }

  availbleSizes(String text, double widthh, int index) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        widget.tapped == true
            ? index == 100
                ? Navigator.push(context,
                    MaterialPageRoute(builder: (context) => L_CustomSize()))
                : changeSizeSecondaryIndex(index, text)
            : Navigator.push(context,
                MaterialPageRoute(builder: (context) => L_Expansion()));
      },
      child: Container(
        margin: EdgeInsets.only(right: size.width * 0.01),
        height: size.height * 0.05,
        width: widthh,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              spreadRadius: 1,
              blurRadius: 3,
              offset: Offset(1, 3), // changes position of shadow
            ),
          ],
          color:
              sizeSecondaryIndex == index ? Config.primaryColor : Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              color: sizeSecondaryIndex == index ? Colors.white : Colors.black,
            ),
          ),
        ),
      ),
    );
  }
}
