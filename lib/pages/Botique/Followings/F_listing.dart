import 'package:flutter/material.dart';


import 'Following_Screen.dart';

class FollowingListing extends StatefulWidget {
  @override
  _FollowingListingState createState() => _FollowingListingState();
}

class _FollowingListingState extends State<FollowingListing> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      child: SingleChildScrollView(
        child: Wrap(
            children: List.generate(
                10,
                (index) =>
                    Container(  height: size.height * 0.42,child: Following()))),
      ),
    );
  }
}
