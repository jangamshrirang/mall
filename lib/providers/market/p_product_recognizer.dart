import 'dart:collection';

import 'package:flutter/foundation.dart';

class PProductRecognizer extends ChangeNotifier {
  List _products = new List();
  String _error = '';

  UnmodifiableListView get products => UnmodifiableListView(_products);
  String get error => _error;

  void setFile(List product) {
    _products = product;
    notifyListeners();
  }

  void setError(String error) {
    _error = error;
    notifyListeners();
  }
}
