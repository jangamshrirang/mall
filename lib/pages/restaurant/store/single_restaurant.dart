import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/models/Restaurant/R_ListByRatings.dart';
import 'package:wblue_customer/pages/restaurant/store/main.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/providers/restaurant/p_food_list_type.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/services/helper.dart';

class SingleRestaurantPage extends StatefulWidget {
  final RestaurantListModel restaurantListModel;
  final int restIndex;
  const SingleRestaurantPage({this.restaurantListModel, this.restIndex});
  @override
  _SingleRestaurantPageState createState() => _SingleRestaurantPageState();
}

class _SingleRestaurantPageState extends State<SingleRestaurantPage> {
  @override
  void initState() {
    super.initState();

    Future.microtask(() {
      context.read<PFoodListType>().fetchFoodTypes(widget.restaurantListModel.hashId);
      context.read<PRestaurantFoodCart>().fetchRCartList();
    });

    //im using this to this in pick up confirmation page
    stateManagment.setRname(widget.restaurantListModel.restaurant);
    stateManagment.setRimage(widget.restaurantListModel.thumbnailCover);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        physics: ClampingScrollPhysics(),
        slivers: [
          SliverPersistentHeader(
            pinned: true,
            delegate: SliverCustomHeaderDelegate(
              collapsedHeight: 0.07.sh,
              expandedHeight: 0.4.sh,
              paddingTop: MediaQuery.of(context).padding.top,
              restaurant: widget.restaurantListModel,
            ),
          ),
          SliverToBoxAdapter(
              child: SingeRestaurantMain(
            restaurantListModel: widget.restaurantListModel,
          ))
        ],
      ),
    );
  }
}

class SliverCustomHeaderDelegate extends SliverPersistentHeaderDelegate {
  final double collapsedHeight;
  final double expandedHeight;
  final double paddingTop;
  final RestaurantListModel restaurant;
  final String name;

  SliverCustomHeaderDelegate(
      {this.collapsedHeight, this.expandedHeight, this.paddingTop, this.restaurant, this.name: ''});

  @override
  double get minExtent => this.collapsedHeight + this.paddingTop;

  @override
  double get maxExtent => this.expandedHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

  Color makeStickyHeaderBgColor(shrinkOffset) {
    final int alpha = (shrinkOffset / (this.maxExtent - this.minExtent) * 255).clamp(0, 255).toInt();
    return Color.fromARGB(alpha, 255, 255, 255);
  }

  Color makeStickyHeaderTextColor(shrinkOffset, isIcon) {
    if (shrinkOffset <= 50) {
      return isIcon ? Colors.white : Colors.transparent;
    } else {
      final int alpha = (shrinkOffset / (this.maxExtent - this.minExtent) * 255).clamp(0, 255).toInt();
      return Color.fromARGB(alpha, 0, 0, 0);
    }
  }

  Color makeStickyHeaderIconBgColor(shrinkOffset) {
    if (shrinkOffset <= 50) {
      return Colors.black26;
    } else {
      return Colors.transparent;
    }
  }

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      height: this.maxExtent,
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: restaurant.fullCover ?? '',
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            placeholder: (context, url) => Container(
              color: Colors.black,
              height: 0.26.sh,
              child: SafeArea(
                bottom: false,
                child: Image.network(
                  restaurant.thumbnailCover,
                  fit: BoxFit.fitWidth,
                  height: double.infinity,
                  width: double.infinity,
                  alignment: Alignment.center,
                ),
              ),
            ),
            errorWidget: (context, url, error) => Container(
              height: 0.26.sh,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AutoSizeText(
                    'Image Not Found',
                    style: TextStyle(fontSize: 28.sp, fontWeight: FontWeight.w300),
                  ),
                  SizedBox(
                    height: 0.02.sh,
                  ),
                  Icon(
                    MdiIcons.alertCircle,
                    color: Colors.red,
                    size: 0.08.sh,
                  )
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () => showImages(
                context: context, full: restaurant?.fullCover, thumbnail: restaurant?.thumbnailCover, isCover: true),
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: <Color>[Colors.black.withAlpha(0), Colors.black12, Colors.black45],
                ),
              ),
            ),
          ),
          Positioned(
            left: 10,
            bottom: 10,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: () => showImages(context: context, full: restaurant?.full, thumbnail: restaurant?.thumbnail),
                  child: Container(
                    padding: EdgeInsets.all(1.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      color: Colors.white,
                    ),
                    height: 0.2.sw,
                    width: 0.2.sw,
                    child: CachedNetworkImage(
                      imageUrl: restaurant?.full ?? '',
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                      ),
                      placeholder: (context, url) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: NetworkImage(restaurant?.thumbnail ?? ''),
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                      ),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        restaurant.restaurant,
                        style: TextStyle(fontSize: 36.sp, color: Colors.white),
                      ),
                      SizedBox(height: 5.0),
                      Row(
                        children: [
                          Container(
                            padding: EdgeInsets.all(3.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              color: Colors.amber,
                            ),
                            child: Row(
                              children: [
                                Icon(
                                  MdiIcons.starHalfFull,
                                  color: Colors.white,
                                  size: 14,
                                ),
                                SizedBox(
                                  width: 3,
                                ),
                                AutoSizeText('4.5',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.sp,
                                    )),
                                SizedBox(
                                  width: 3,
                                ),
                                AutoSizeText('(1k)', style: TextStyle(fontSize: 16.sp, color: Colors.white)),
                              ],
                            ),
                          ),
                          SizedBox(width: 5.0),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              color: Colors.redAccent,
                            ),
                            padding: EdgeInsets.all(3.0),
                            child: Row(
                              children: [
                                Icon(
                                  MdiIcons.heart,
                                  color: Colors.white,
                                  size: 14,
                                ),
                                SizedBox(
                                  width: 3,
                                ),
                                SizedBox(
                                  width: 3,
                                ),
                                AutoSizeText('(1k)', style: TextStyle(fontSize: 16.sp, color: Colors.white)),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          // Put your head back
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            child: Container(
              color: this.makeStickyHeaderBgColor(shrinkOffset), // Background color
              child: SafeArea(
                bottom: false,
                child: Container(
                  height: this.collapsedHeight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FlatButton(
                        onPressed: () => Navigator.pop(context),
                        child: Icon(
                          Icons.keyboard_backspace,
                          color: this.makeStickyHeaderTextColor(shrinkOffset, true),
                          size: 20.0,
                        ),
                        height: 40.0,
                        shape: CircleBorder(),
                        color: this.makeStickyHeaderIconBgColor(shrinkOffset),
                      ),
                      Text(
                        name,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: this.makeStickyHeaderTextColor(shrinkOffset, false), // Title color
                        ),
                      ),
                      Consumer<PRestaurantFoodCart>(
                        builder: (context, checkCart, child) => checkCart.restaurantCart.food.length > 0 &&
                                checkCart.restHashidInCart == checkCart.restHashid
                            ? FlatButton(
                                onPressed: () => ExtendedNavigator.of(context).root.push('/restaurant-food-cart'),
                                child: Icon(
                                  Icons.shopping_bag,
                                  color: this.makeStickyHeaderTextColor(shrinkOffset, true),
                                  size: 20.0,
                                ),
                                height: 40.0,
                                shape: CircleBorder(),
                                color: this.makeStickyHeaderIconBgColor(shrinkOffset),
                              )
                            : SizedBox(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> showImages({BuildContext context, String full, String thumbnail, bool isCover: false}) async {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.black,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return CachedNetworkImage(
          imageUrl: full ?? '',
          imageBuilder: (context, imageProvider) => GestureDetector(
              onTap: () {},
              child: Column(
                children: [
                  Expanded(
                    child: Stack(
                      children: [
                        Positioned(
                          top: 50,
                          right: 0,
                          child: FlatButton(
                              onPressed: () => Navigator.pop(context),
                              child: Icon(
                                Icons.close,
                                color: Colors.white,
                                size: 26.0,
                              ),
                              height: 40.0,
                              shape: CircleBorder(),
                              color: Colors.black45),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: AspectRatio(
                            aspectRatio: isCover ? 7 / 5 : 2 / 2,
                            child: Container(
                              // width: 1.sw,
                              // height: 1.sw,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
          errorWidget: (context, url, error) => Container(
            color: Colors.black12,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AutoSizeText('Image Not Found'),
                SizedBox(height: 8.0),
                Icon(
                  Icons.error_outline,
                  size: 30.0,
                  color: Colors.red,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
