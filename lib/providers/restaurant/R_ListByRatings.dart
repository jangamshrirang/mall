import 'package:wblue_customer/services/dio/dio_client.dart';
import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:wblue_customer/models/Restaurant/resturentMenuModel.dart';

DioClient _dioClient = DioClient();

class R_ListRatingHelper with ChangeNotifier {
  List _restaurants = new List();
  bool _isLoading = true;

  UnmodifiableListView get restaurantsByRatings =>
      UnmodifiableListView(_restaurants);
  bool get isLoading => _isLoading;

  Future<void> fetchRestaurantsByRating() async {
    setLoading(true);
    _restaurants = await fetchRestaurantsByRatings();
    setLoading(false);

    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future<List> fetchRestaurantsByRatings() async {
  APIResponse res =
      await _dioClient.publicGet('/restaurant/by-rate');///restaurant/by-rate
  if (res.code >= 400) {
    return [];
  }

  return res.data;
}
