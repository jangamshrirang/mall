import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/widgets/w_rating.dart';

class AllProductRatings extends StatefulWidget {
  List allProductReviews;
  AllProductRatings({this.allProductReviews});
  int colorVal = 0xff84020e;
  @override
  _AllProductRatingsState createState() => _AllProductRatingsState();
}

class _AllProductRatingsState extends State<AllProductRatings>
    with TickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 5, initialIndex: 4);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {
      widget.colorVal = 0xff84020e;
    });
  }

  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "Product Rating & Review"),
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: DefaultTabController(
          length: 5,
          child: Column(
            children: <Widget>[
              TabBar(
                controller: _tabController,
                indicatorSize: TabBarIndicatorSize.tab,
                indicatorColor: Colors.transparent,
                unselectedLabelColor: Colors.grey,
                tabs: <Widget>[
                  Tab(
                      child: customTab(
                    0,
                    1,
                  )),
                  Tab(child: customTab(1, 2)),
                  Tab(child: customTab(2, 3)),
                  Tab(child: customTab(3, 4)),
                  Tab(child: customTab(4, 5)),
                ],
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    producReviwesListTile(widget.allProductReviews),
                    producReviwesListTile(widget.allProductReviews),
                    producReviwesListTile(widget.allProductReviews),
                    producReviwesListTile(widget.allProductReviews),
                    producReviwesListTile(widget.allProductReviews),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  customTab(int number, starnumber) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.04,
      width: size.width*0.1,
      decoration: BoxDecoration(
          color: _tabController.index == number
              ? Colors.red[50]
              : Colors.grey[100],
          borderRadius: BorderRadius.circular(3),
          border: Border.all(color: Colors.transparent, width: 0.5)),
      child: Row(
        children: [
        SizedBox(width: 8,),
          Text("$starnumber",style: TextStyle(color:Colors.black),),
          Align(
              alignment: Alignment.center,
              child: _tabController.index == number
                  ? Icon(
                      Icons.star,
                      size: 14,
                      color: Colors.amber,
                    )
                  : Icon(
                      Icons.star,
                      size: 14,
                      color: Colors.grey[300],
                    )),
        ],
      ),
    );
  }

  producReviwesListTile(List products) {
    var size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Wrap(
          children: List.generate(products.length, (index) {
        return Column(
          children: [
            productRatingtileImage(),
            Row(
              children: [
                Container(
                  margin: EdgeInsets.only(left: size.width * 0.05, top: 5),
                  child: Text(
                    "Myrna J -" + "${products[index]}" + " Weeks ago",
                    style: TextStyle(color: Colors.black45, fontSize: 13),
                  ),
                ),
                Spacer(),
                WRatingsWidget(3.5, size: 16.0, compact: true),
                SizedBox(
                  width: 8,
                ),
              ],
            ),
            SizedBox(
              height: 6,
            ),
            Container(
                width: size.width * 0.9,
                child: Text(
                  "adidas_alphabounce rc 2.0 mens running shoe270 ...are one of the best shoe i ever had and im giving 5 Star for this product",
                  style: TextStyle(fontSize: 16),
                )),
            SizedBox(
              height: 10,
            ),
            Container(
                height: size.height * 0.1,
                width: size.width * 0.8,
                child: Wrap(
                    spacing: 3,
                    children: List.generate(products.length, (index) {
                      return Image.network(
                        ('https://images-na.ssl-images-amazon.com/images/I/41Leu3gBUFL._UX395_.jpg'),
                        width: 80,
                      );
                    }))),
            Divider(),
          ],
        );
      })),
    );
  }

  productRatingtileImage() {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: size.height * 0.01),
          color: Colors.grey[200],
          width: size.width * 0.93,
          height: size.height * 0.1,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: size.height * 0.01),
                color: Colors.transparent,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    ('https://images-na.ssl-images-amazon.com/images/I/61Nh8BmRvoL.__AC_SY395_QL70_ML2_.jpg'),
                    width: 60,
                  ),
                ),
              ),
              SizedBox(
                width: 4,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Spacer(),
                  Container(
                    width: size.width * 0.6,
                    margin: EdgeInsets.only(top: 5, bottom: 5),
                    child: Text(
                      'adidas_alphabounce rc 2.0 mens running shoe270 ',
                      maxLines: 2,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: size.height * 0.023,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    'Color family: Purple,' + " Size:EU:36",
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: size.height * 0.018,
                        fontWeight: FontWeight.normal),
                  ),
                  Spacer(),
                  Spacer(),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
