import 'package:flutter/material.dart';
import '../env/config.dart';

class WRoundedButton extends StatelessWidget {
  final Widget child;
  final Color btnColor;
  final Color labelColor;
  final Color borderColor;
  final double elevation;
  VoidNavigate onCustomButtonPressed;

  WRoundedButton({this.child, this.elevation: 1.0, @required this.onCustomButtonPressed, this.btnColor: Colors.white, this.labelColor: Config.primaryColor, this.borderColor: Config.primaryColor});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: elevation,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0), side: BorderSide(color: borderColor)),
      onPressed: () => onCustomButtonPressed(),
      color: btnColor,
      textColor: labelColor,
      child: child,
    );
  }
}

typedef VoidNavigate = void Function();
