import 'package:bot_toast/bot_toast.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WLoading {
  static type(Loading loading) {
    switch (loading) {
      case Loading.show:
        return BotToast.showCustomLoading(
            backButtonBehavior: BackButtonBehavior.none,
            ignoreContentClick: false,
            animationDuration: Duration(milliseconds: 200),
            animationReverseDuration: Duration(milliseconds: 200),
            backgroundColor: Colors.black45,
            align: Alignment.center,
            toastBuilder: (cancelFunc) {
              return WLoadingWidget();
            });
        break;
      case Loading.dismiss:
        return BotToast.closeAllLoading();
      default:
        return BotToast.showCustomLoading(
            backButtonBehavior: BackButtonBehavior.none,
            ignoreContentClick: false,
            animationDuration: Duration(milliseconds: 200),
            animationReverseDuration: Duration(milliseconds: 200),
            backgroundColor: Colors.black45,
            align: Alignment.center,
            toastBuilder: (cancelFunc) {
              return WLoadingWidget();
            });
    }
  }
}

enum Loading { show, dismiss }

class WLoadingWidget extends StatefulWidget {
  @override
  _WLoadingWidgetState createState() => _WLoadingWidgetState();
}

class _WLoadingWidgetState extends State<WLoadingWidget>
    with TickerProviderStateMixin {
  List animations = ['open', 'loop'];

  String animation;
  @override
  void initState() {
    animation = animations[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Container(
      height: size.width * 0.2,
      width: size.width * 0.2,
      child: FlareActor(
        'assets/flares/w-loading.flr',
        animation: animation,
        fit: BoxFit.contain,
        callback: (String _val) {
          setState(() {
            animation = animations[1];
          });
        },
      ),
    );
  }
}
