import 'package:clippy_flutter/trapezoid.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/home/Services/serviceCarosel.dart';
import 'package:wblue_customer/widgets/w_rating.dart';

class ServiceVendorExpantion extends StatefulWidget {
  @override
  _ServiceVendorExpantionState createState() => _ServiceVendorExpantionState();
}

class _ServiceVendorExpantionState extends State<ServiceVendorExpantion> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Config.bodyColor,
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(right: 285.0),
        child: FloatingActionButton(
          mini: true,
          child: Icon(
            Icons.chat_bubble_outline,
            color: Colors.white,
            size: 20,
          ),
          backgroundColor: Color(0xff101f40),
          onPressed: () {},
        ),
      ),
      appBar: AppBar(
        backgroundColor: Color(0xff101f40),
        title: Container(
            color: Color(0xff101f40),
            width: size.width,
            child: Text(
              "Mark Spa & Massage",
              style: TextStyle(fontSize: size.height * 0.035),
            )),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            topImageContainer(),
            text(
              "   Services ",
              size.height * 0.023,
              Colors.black,
              1,
              FontWeight.bold,
              size.width * 0.48,
            ),
            SizedBox(
              height: 2,
            ),
            Container(
             // margin: EdgeInsets.only(left: 10),
              height: size.height * 0.6,
              width: size.width * 0.95,
              // color: Colors.blue,
              child: ServiceCarocel(),
            ),
            Divider(
              color: Colors.transparent,
              thickness: 5,
            ),
            contactInfoBlok(),
            Divider(
              color: Colors.transparent,
              thickness: 5,
            ),
            addressInfoBlok(),
            Divider(
              color: Colors.transparent,
              thickness: 5,
            ),
            lastDetailsBloc(),
            Divider(
              color: Colors.transparent,
              thickness: 5,
            ),
          ],
        ),
      ),
    );
  }

  text(String text, double height, Color color, int lines,
      FontWeight fontWeight, double width) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: width,
      margin: EdgeInsets.only(left: 10, top: 4),
      child: Text(
        text,
        maxLines: lines,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: fontWeight,
            color: color,
            fontSize: height),
      ),
    );
  }

  topImageContainer() {
    var size = MediaQuery.of(context).size;
    return Container(
        width: size.width * 0.95,
        height: size.height * 0.2,
        margin: EdgeInsets.only(bottom: 10, left: 10, top: 5, right: 5),
        decoration: BoxDecoration(
          color: Colors.black54,
          borderRadius: BorderRadius.all(Radius.circular(0)),
          image: DecorationImage(
            image: AssetImage(
              'assets/services/physiotherapy-567021_1920.jpg',
            ),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
            decoration: BoxDecoration(
              color: Colors.black38,
            ),
            margin: EdgeInsets.only(
              top: size.height * 0.0,
            ),
            height: size.height * 0.06,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    margin: EdgeInsets.only(top: size.height * 0.01),
                    height: size.height * 0.18,
                    width: size.width * 0.9,
                    // color: Colors.red,
                    child: listItemCard())
              ],
            )));
  }

  listItemCard() {
    var size = MediaQuery.of(context).size;
    return Container(
        width: size.width * 0.9,
        height: size.height * 0.22,
        child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            padding: EdgeInsets.only(
                left: size.width * 0.02,
                right: size.width * 0.01,
                top: size.height * 0.01),
            height: size.height * 0.07,
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(color: Colors.white)),
                  child: Image.asset(
                    'assets/services/downloa.png',
                    height: size.height * 0.14,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        SizedBox(width: 10),
                        Icon(
                          Icons.favorite_border,
                          color: Colors.white,
                          size: 15,
                        ),
                        text(
                          "1.1 K",
                          size.height * 0.015,
                          Colors.white,
                          1,
                          FontWeight.normal,
                          size.width * 0.45,
                        )
                      ],
                    ),
                    text(
                      "95% Positive seller Ratings",
                      size.height * 0.020,
                      Colors.white,
                      2,
                      FontWeight.normal,
                      size.width * 0.45,
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 10, top: 5, bottom: 0),
                        child: WRatingsWidget(400 / 100)),
                    text(
                      "QAR 500 - QAR 1500 ",
                      size.height * 0.018,
                      Colors.white,
                      1,
                      FontWeight.normal,
                      size.width * 0.45,
                    ),
                    text(
                      "OPEN | 4PM - 12MIDNIGHT ",
                      size.height * 0.018,
                      Color(0xff009500),
                      1,
                      FontWeight.normal,
                      size.width * 0.5,
                    ),
                  ],
                ),
              ],
            )));
  }

  contactInfoBlok() {
    var size = MediaQuery.of(context).size;
    return Container(
        width: size.width * 0.95,
        height: size.height * 0.25,
        margin: EdgeInsets.only(left: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            text(
              "  Contact Information",
              size.height * 0.025,
              Colors.black54,
              1,
              FontWeight.normal,
              size.width * 0.6,
            ),
            SizedBox(
              height: 10,
            ),
            rowText(
              "Store Email",
              "Mobile",
              size.height * 0.025,
              Colors.black54,
              Colors.black54,
            ),
            rowText(
              "WmallAgmaol.com",
              "55500055",
              size.height * 0.022,
              Colors.black26,
              Colors.black26,
            ),
            SizedBox(
              height: 10,
            ),
            Divider(),
            rowText(
              "Contact Email",
              "Mobile",
              size.height * 0.025,
              Colors.black54,
              Colors.black54,
            ),
            rowText(
              "WmallAgmaol.com",
              "55500055",
              size.height * 0.022,
              Colors.black26,
              Colors.black26,
            ),
          ],
        ));
  }

  addressInfoBlok() {
    var size = MediaQuery.of(context).size;
    return Container(
        width: size.width * 0.95,
        height: size.height * 0.36,
        margin: EdgeInsets.only(left: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            text(
              "  Address",
              size.height * 0.025,
              Colors.black54,
              1,
              FontWeight.normal,
              size.width * 0.45,
            ),
            SizedBox(
              height: 10,
            ),
            text(
              "  Steet",
              size.height * 0.025,
              Colors.black54,
              1,
              FontWeight.normal,
              size.width * 0.45,
            ),
            text(
              "  Al Khalid street",
              size.height * 0.022,
              Colors.black26,
              1,
              FontWeight.normal,
              size.width * 0.45,
            ),
            SizedBox(
              height: 10,
            ),
            Divider(),
            rowText("State", "City", size.height * 0.025, Colors.black54,
                Colors.black26),
            rowText("Ad Dawah", "Doha", size.height * 0.022, Colors.black26,
                Colors.black26),
            SizedBox(
              height: 10,
            ),
            Divider(),
            text(
              "  Others",
              size.height * 0.025,
              Colors.black54,
              1,
              FontWeight.normal,
              size.width * 0.45,
            ),
            text(
              "  Al Khalid street",
              size.height * 0.022,
              Colors.black26,
              1,
              FontWeight.normal,
              size.width * 0.45,
            ),
          ],
        ));
  }

  rowText(String title, title2, double height, Color color, color2) {
    var size = MediaQuery.of(context).size;
    return Row(
      children: [
        SizedBox(width: 05),
        text(
          title,
          height,
          color,
          1,
          FontWeight.normal,
          size.width * 0.45,
        ),
        SizedBox(width: 05),
        text(
          title2,
          height,
          color2,
          1,
          FontWeight.normal,
          size.width * 0.40,
        ),
      ],
    );
  }

  ratingBarWithText(String text, double value, int count) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(bottom: 5, left: size.width * 0.15),
      child: Row(
        children: [
          Container(
            width: size.width * 0.18,
            child: Text(
              text,
              style: TextStyle(color: Colors.black45, fontSize: 16),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(0.0),
            child: new LinearPercentIndicator(
              width: size.width * 0.5,
              animation: true,
              animationDuration: 3000,
              lineHeight: 15.0,
              //leading: new Text(""),
              // trailing: new Text(""),
              percent: value,
              center: Text(""),
              linearStrokeCap: LinearStrokeCap.butt,
              progressColor: Colors.pinkAccent,
            ),
          ),
          Text(
            "$count",
            style: TextStyle(color: Colors.black45, fontSize: 13),
          ),
        ],
      ),
    );
  }

  lastDetailsBloc() {
    var size = MediaQuery.of(context).size;
    return Container(
        width: size.width * 0.95,
        height: size.height * 0.40,
        margin: EdgeInsets.only(left: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 10),
            rowText(
              "Chat Response Time",
              "Active in 5:min",
              size.height * 0.021,
              Colors.black,
              Colors.green,
            ),
            rowText(
              "Chat Response Rate",
              "98.21%",
              size.height * 0.021,
              Colors.black,
              Colors.green,
            ),
            SizedBox(height: 10),
            text(
              " 95% ",
              size.height * 0.05,
              Colors.black,
              2,
              FontWeight.normal,
              size.width * 0.3,
            ),
            text(
              " Positive customer Rating  ",
              size.height * 0.025,
              Colors.black,
              1,
              FontWeight.normal,
              size.width * 0.6,
            ),
            text(
              "    41345 Customer Reviwes ",
              size.height * 0.018,
              Colors.black38,
              1,
              FontWeight.normal,
              size.width * 0.6,
            ),
            SizedBox(height: size.height * 0.01),
            ratingBarWithText("Positive", 0.8, 999),
            ratingBarWithText("Netural", 0.1, 549),
            ratingBarWithText("Negative", 0.1, 555),
          ],
        ));
  }
}
