import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/market/category.dart';
import 'package:wblue_customer/widgets/w_category_type.dart';
import 'package:wblue_customer/widgets/w_sub_category.dart';

class CategoryPage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  int state = 0;
  Random random = new Random();

  Future _loadProducts;

  List subCategory;
  int expandedIndex = 0;

  List category = Categories.category();

  Completer _responseCompleter = new Completer();

  @override
  void initState() {
    subCategory = category[0]['subCategories'];

    super.initState();
  }

  Future _getProducts(List groups) async {
    int itemQuantity = random.nextInt(10) + 1;

    await Future.delayed(Duration(seconds: 1));
    Map product = {
      'productName': 'Sample product',
      'productImage': AssetImage('assets/images/placeholders/product.png'),
    };

    if (groups.length <= 0) {
      for (int i = 0; i < itemQuantity; i++) {
        groups.add(product);
      }
    }

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Config.primaryColor,
          elevation: 0.0,
          title: Center(child: Text('Category')),
          actions: [
            IconButton(
              icon: Icon(
                Icons.notifications_none,
                size: 30,
              ),
              onPressed: () => null,
            ),
          ],
        ),
        body: Container(
          child: Row(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    right: BorderSide(
                      color: Colors.grey[300],
                      width: 1.0,
                    ),
                  ),
                ),
                width: MediaQuery.of(context).size.width * 0.3,
                child: ListView.separated(
                    padding: EdgeInsets.all(8.0),
                    itemCount: category.length,
                    separatorBuilder: (BuildContext context, int index) {
                      return SizedBox(height: 8.0);
                    },
                    itemBuilder: (BuildContext context, int index) {
                      return WCategoryType(
                        iconData: category[index]['icon'],
                        title: category[index]['title'],
                        isActive: category[index]['isSelected'],
                        onPressed: () {
                          setState(() {
                            category.forEach(
                                (element) => element['isSelected'] = false);
                            category[index]['isSelected'] = true;
                            subCategory = category[index]['subCategories'];
                            expandedIndex = index;
                          });
                        },
                      );
                    }),
              ),
              Expanded(
                  child: ListView.builder(
                key: Key('builder ${expandedIndex.toString()}'), //attention
                itemCount: subCategory.length,
                itemBuilder: (context, index) {
                  return Theme(
                    data: ThemeData(
                      dividerColor: Colors.transparent,
                    ),
                    child: ExpansionTile(
                      key: Key(index.toString()),
                      onExpansionChanged: (isOpened) async {
                        List groups = subCategory[index]['groups'];
                        if (isOpened && groups.length <= 0) {
                          _loadProducts = _getProducts(groups);
                        }
                        setState(() {});
                      },
                      backgroundColor: Config.dullColor,
                      title: GestureDetector(
                          onTap: () {
                            // Goto.push('/products', args: {
                            //   'keyword': subCategory[index]['name']
                            // });
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                color: Colors.transparent,
                                border: Border(
                                  right: BorderSide(
                                    color: Colors.grey[500],
                                    width: .5,
                                  ),
                                ),
                              ),
                              child: Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Text('${subCategory[index]['name']}'),
                              ))),
                      children: <Widget>[
                        FutureBuilder(
                          future: _loadProducts,
                          builder: (BuildContext context, response) {
                            if (!response.hasData) {
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Center(
                                  child: Text(
                                    'Loading...',
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              );
                            } else {
                              return _product(subCategory[index]['groups']);
                            }
                          },
                        )
//                        FutureBuilder(
//                            future: getProducts(),
//                            initialData: [],
//                            builder: (context, snapshot) {
//                              return _product(subCategory[index]['groups']);
//                            }),
                      ],
                    ),
                  );
                },
              ))
            ],
          ),
        ));
  }
}

_product(List groups) {
  return Column(children: <Widget>[
    GridView.count(
      physics: ClampingScrollPhysics(),
      shrinkWrap: true,
      crossAxisCount: 3,
      childAspectRatio: .7,
      padding: EdgeInsets.all(4.0),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
      children: List.generate(groups.length, (index) {
        return WSubCategoryWidget(
            productImg: groups[index]['productImage'],
            productName: groups[index]['productName']);
      }),
    )
  ]);
}
