import 'package:flutter/cupertino.dart';

class DMarketHomeData {
  static List<Map<String, dynamic>> banners = [
    {
      'title': 'THE BIGGEST SALE',
      'description': 'The Special For Today',
      'image': AssetImage('assets/images/carousel/banner1.jpg')
    },
    {
      'title': 'SUMMER COLLECTION',
      'description': 'New Arrival On Sale',
      'image': AssetImage('assets/images/carousel/banner2.jpg')
    },
    {
      'title': 'THE BIGGEST SALE',
      'description': 'The Special For Today',
      'image': AssetImage('assets/images/carousel/banner3.jpg')
    },
    {
      'title': 'SUMMER COLLECTION',
      'description': 'New Arrival On Sale',
      'image': AssetImage('assets/images/carousel/banner4.jpg')
    },
    {
      'title': 'THE BIGGEST SALE',
      'description': 'The Special For Today',
      'image': AssetImage('assets/images/carousel/banner5.jpg')
    },
  ];

  static List<Map<String, dynamic>> popularCategories = [
    {
      'title': 'Headset',
      'image': 'assets/images/products/headset/1-small.png'
    },
    {
      'title': 'Keyboard',
      'image': 'assets/images/products/keyboard/1-small.png'
    },
    {
      'title': 'PC',
      'image': 'assets/images/products/pc/1-small.png'
    },
    {
      'title': 'Phone',
      'image': 'assets/images/products/phone/1-small.png'
    },
    {
      'title': 'Printer',
      'image': 'assets/images/products/printer/1-small.png'
    },
    {
      'title': 'Probook',
      'image': 'assets/images/products/probook/1-small.png'
    },
    {
      'title': 'Keyboard',
      'image': 'assets/images/products/keyboard/1-small.png'
    },
    {
      'title': 'Headset',
      'image': 'assets/images/products/headset/1-small.png'
    },
  ];

  static List<Map<String, dynamic>> popularProducts = [
    {
      'title': 'BAGS',
      'value': '1,200',
      'image': AssetImage('assets/images/carousel/banner1.jpg')
    },
    {
      'title': 'SHOES',
      'value': '900',
      'image': AssetImage('assets/images/carousel/banner2.jpg')
    },
    {
      'title': 'CLOTHES',
      'value': '120',
      'image': AssetImage('assets/images/carousel/banner3.jpg')
    },
    {
      'title': 'SANDALS',
      'value': '200',
      'image': AssetImage('assets/images/carousel/banner4.jpg')
    },
  ];

  static List popularUsers = [
    {
      'description': 'BILI NA KAYO',
      'viewers': '1,230',
      'likes': '1,300',
      'image': 'assets/images/users/1-boy.png',
    },
    {
      'description': 'mga mare bili na kayo ng damit',
      'viewers': '10',
      'likes': '630',
      'image': 'assets/images/users/1-girl.png',
    },
    {
      'description': 'kuya wampipti',
      'viewers': '5,200',
      'likes': '3,000,010',
      'image': 'assets/images/users/2-boy.png',
    },
  ];

  static List menu = [
    {
      'name': 'Load, Bills & eCoupon',
      'image': 'https://via.placeholder.com/60x60/0000FF/',
    },
    {
      'name': 'WMALL',
      'image': 'https://via.placeholder.com/60x60/0000FF/',
    },
    {
      'name': 'WLive',
      'image': 'https://via.placeholder.com/60x60/0000FF/',
    },
    {
      'name': 'Catergories',
      'image': 'https://via.placeholder.com/60x60/0000FF/',
    },
    {
      'name': 'Vouchers',
      'image': 'https://via.placeholder.com/60x60/0000FF/',
    },
    {
      'name': 'WGlobal',
      'image': 'https://via.placeholder.com/60x60/0000FF/',
    },
    {
      'name': 'WMart',
      'image': 'https://via.placeholder.com/60x60/0000FF/',
    },
    {
      'name': 'Coins & Rewards',
      'image': 'https://via.placeholder.com/60x60/0000FF/',
    },
    {
      'name': 'WGames',
      'image': 'https://via.placeholder.com/60x60/0000FF/',
    }
  ];
}
