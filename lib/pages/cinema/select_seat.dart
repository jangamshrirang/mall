import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../env/config.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_cinema_seat_selection.dart';

class SelectCinemaSeatPage extends StatefulWidget {
  @override
  _SelectCinemaSeatPageState createState() => _SelectCinemaSeatPageState();
}

class _SelectCinemaSeatPageState extends State<SelectCinemaSeatPage> {
  DateFormat dateFormatter = new DateFormat('yyyy-MM-dd');
  DateFormat timeFormatter = new DateFormat('hh:mm a');

  List<String> selectedSeats = new List<String>();
  int count = 2;
  String date, time;
  DateTime dateTime;

  List<String> times = [
    '10:00',
    '12:15',
    '15:00',
    '17:30',
    '19:45',
    '22:00',
    '00:15',
    '03:00',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _header(),
            WCinemaSeatSelectionWidget(
              count: count,
              onUpdate: (_selectedSeats) {
                setState(() {
                  selectedSeats = _selectedSeats;
                });
              },
            ),
            Divider(),
            _paxCounter(),
            Divider(),
            _dateSelector(),
            _timeSelector(),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(16.0),
        color: Colors.white,
        child: Wrap(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('${selectedSeats.length} SELECTED'),
                Text(
                  Global.money(80.0 * selectedSeats.length),
                  style: TextStyle(color: Colors.amber, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            WButtonWidget(
              title: 'reserve now',
              onPressed: selectedSeats.length > 0 && date != null && time != null
                  ? () {
                      ExtendedNavigator.of(context).root.push('/cinemas/ticket-details');
                    }
                  : null,
            ),
          ],
        ),
      ),
    );
  }

  Widget _timeSelector() {
    return Container(
      height: 60.0,
      padding: EdgeInsets.all(16.0),
      child: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: times.map((item) {
          return InkWell(
            onTap: () {
              setState(() {
                time = item;
                dateTimeParser(reWrite: true, time: item);
              });
            },
            child: Container(
              padding: EdgeInsets.all(4.0),
              margin: EdgeInsets.symmetric(horizontal: 4.0),
              decoration: BoxDecoration(
                color: time == item ? Colors.amber : Colors.transparent,
                border: Border.all(color: Colors.black),
              ),
              child: Center(child: Text(dateTimeParser(time: item))),
            ),
          );
        }).toList(),
      ),
    );
  }

  Widget _dateSelector() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Expanded(
          child: __dateSelectorButton('today'),
        ),
        Expanded(
          child: __dateSelectorButton('tomorrow'),
        ),
      ],
    );
  }

  Widget __dateSelectorButton(String label) {
    DateTime now = DateTime.now().toLocal();
    DateTime today = new DateTime(now.year, now.month, now.day);
    DateTime tomorrow = new DateTime(now.year, now.month, now.day).add(Duration(days: 1));
    date = date ?? dateFormatter.format(today);

    return InkWell(
      onTap: () {
        if (label == 'today') {
          date = dateFormatter.format(today);
        } else {
          date = dateFormatter.format(tomorrow);
        }
        setState(() {});
      },
      child: Container(
        padding: EdgeInsets.all(16.0),
        margin: EdgeInsets.symmetric(horizontal: 16.0),
        decoration: BoxDecoration(
          color: date == dateFormatter.format(today) && label == 'today' ? Colors.amber : date == dateFormatter.format(tomorrow) && label == 'tomorrow' ? Colors.amber : Colors.transparent,
          border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: Center(
          child: Text(
            label.toUpperCase(),
            style: TextStyle(fontSize: 14.0),
          ),
        ),
      ),
    );
  }

  String dateTimeParser({bool reWrite: false, bool timeOnly: true, String time: '00:00'}) {
    DateTime _dateTime = DateTime.parse('$date $time').toLocal();
    if (reWrite) {
      dateTime = _dateTime;
    }
    if (timeOnly) {
      return timeFormatter.format(_dateTime);
    }
    return dateFormatter.format(_dateTime);
  }

  Widget _paxCounter() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        children: <Widget>[
          Expanded(child: Text('Pax Count')),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.remove_circle),
                color: Colors.amber,
                onPressed: count <= 1
                    ? null
                    : () {
                        if (count > 1) {
                          setState(() {
                            --count;
                            if (count < selectedSeats.length) {
                              selectedSeats.removeAt(0);
                            }
                          });
                        }
                      },
              ),
              Text(count.toString()),
              IconButton(
                icon: Icon(Icons.add_circle),
                color: Colors.amber,
                onPressed: () {
                  setState(() {
                    ++count;
                  });
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _header() {
    return Container(
      color: Config.primaryColor,
      child: SafeArea(
        bottom: false,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.arrow_back),
                  color: Colors.amber,
                  onPressed: () {
                    ExtendedNavigator.of(context).pop();
                  },
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                  child: Text(
                    'iMax',
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14.0),
                  ),
                ),
              ],
            ),
            Expanded(
              child: Container(
                height: 80.0,
                padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                color: Colors.amber,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Text(
                        'Movie Title',
                        style: TextStyle(fontSize: 18.0),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Text(
                      Global.money(80),
                      style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
