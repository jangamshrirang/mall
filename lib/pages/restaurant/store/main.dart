import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_shimmer/flutter_shimmer.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/R_ListByRatings.dart';
import 'package:wblue_customer/models/Restaurant/info.dart';
import 'package:wblue_customer/models/Restaurant/restaurant_review.dart';
import 'package:wblue_customer/pages/booking/UpdatedTableBooking.dart';
import 'package:wblue_customer/pages/restaurant/store/restCatogries.dart';
import 'package:wblue_customer/providers/restaurant/p_food_list_type.dart';
import 'package:wblue_customer/providers/restaurant/pbrowse.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/widgets/w_nav_icon.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class SingeRestaurantMain extends StatefulWidget {
  final RestaurantListModel restaurantListModel;
  final int restIndex;

  const SingeRestaurantMain({Key key, this.restaurantListModel, this.restIndex})
      : super(key: key);

  @override
  _SingeRestaurantMainState createState() => _SingeRestaurantMainState();
}

class _SingeRestaurantMainState extends State<SingeRestaurantMain> {
  Set<Marker> markers = Set();
  CameraPosition location;

  Future<bool> _canFollow() async {
    bool res = false;

    bool canNav = await ExtendedNavigator.of(context)
        .root
        .canNavigate('/restaurants/cart');

    res = canNav ?? false;

    return res;
  }

  @override
  void initState() {
    super.initState();
    Future.microtask(() =>
        context.read<PFoodListType>().isFollowInit(widget.restaurantListModel));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              color: Config.primaryColor.withAlpha(16),
              padding: EdgeInsets.all(20),
              child: AutoSizeText(
                  '${widget.restaurantListModel.storeInformation}'),
            ),
          ],
        ),
        Container(
          height: 0.05.sh,
          color: Colors.white,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Consumer<PFoodListType>(
                builder: (context, value, child) => Expanded(
                  child: FlatButton(
                    onPressed: () async {
                      bool canFollow = await _canFollow();
                      if (canFollow)
                        context
                            .read<PFoodListType>()
                            .followApi(widget.restaurantListModel);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          widget.restaurantListModel.isFollowed
                              ? MdiIcons.heart
                              : MdiIcons.heartOutline,
                          size: 0.04.sw,
                          color: widget.restaurantListModel.isFollowed
                              ? Colors.red
                              : Colors.black,
                        ),
                        SizedBox(width: 0.02.sw),
                        Text(
                          widget.restaurantListModel.isFollowed
                              ? 'FOLLOWING'
                              : 'FOLLOW',
                          style: TextStyle(fontSize: 26.sp),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                child: FlatButton(
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(MdiIcons.emailOutline, size: 0.04.sw),
                      SizedBox(width: 0.02.sw),
                      Text(
                        'MESSAGE',
                        style: TextStyle(fontSize: 26.sp),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              WNavIconWidget(
                'info',
                Icons.info_outline,
                onTap: () => showInfo(widget.restaurantListModel.hashId),
              ),
              WNavIconWidget('reviews', Icons.star_half, onTap: () {
                showReviews(widget.restaurantListModel);
              }),
              WNavIconWidget(
                'photos',
                Icons.photo_library,
                onTap: () {
                  showImages(widget.restaurantListModel.hashId);
                },
              ),
              WNavIconWidget(
                'map',
                Icons.map,
                onTap: () => showMap(widget.restaurantListModel.hashId),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Consumer<PFoodListType>(
            builder: (context, info, child) => info.foodListType.length > 0
                ? Container(
                    padding: EdgeInsets.symmetric(horizontal: 14.0),
                    child: Column(
                      children: [
                        catogoryContainer(
                          "Table Booking",
                          "assets/images/book table.png",
                          "assets/restaurent/tablebg.png",
                          () async {
                            bool res = false;
                            bool canNav = await ExtendedNavigator.of(context)
                                .root
                                .canNavigate('/restaurants/cart');

                            res = canNav ?? false;
                            if (!res) return;
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => BookTable(
                                          restId:
                                              widget.restaurantListModel.hashId,
                                          restind: widget.restIndex,
                                          restSlogan: widget.restaurantListModel
                                              .storeInformation,
                                          onlybooking: 'onlytable',
                                          coverImage: widget
                                              .restaurantListModel.fullCover,
                                          restName: widget
                                              .restaurantListModel.restaurant,
                                          restimage:
                                              widget.restaurantListModel.full,
                                        )));
                          },
                          0.03.sh,
                          0.55.sh,
                        ),
                        Divider(
                          thickness: 5,
                          color: Colors.transparent,
                        ),
                        catogoryContainer(
                            "  Delivery",
                            "assets/images/delivery.png",
                            "assets/restaurent/deliverbg.png", () {
                          ExtendedNavigator.of(context).push(
                              '/restaurants/menu',
                              arguments: RestCatogriesArguments(
                                isFromTakeAway: false,
                                restId: widget.restaurantListModel.hashId,
                                restindex: widget.restIndex,
                                restSlogan:
                                    widget.restaurantListModel.storeInformation,
                                isDelivery: true,
                                coverImage:
                                    widget.restaurantListModel.fullCover,
                                coverThumbnail:
                                    widget.restaurantListModel.thumbnailCover,
                                restName: widget.restaurantListModel.restaurant,
                                restimage: widget.restaurantListModel.full,
                                restThumbnail:
                                    widget.restaurantListModel.thumbnail,
                              ));
                        }, 0.05.sh, 0.62.sh),
                        Divider(
                          thickness: 5,
                          color: Colors.transparent,
                        ),
                        catogoryContainer(
                          "Take away",
                          "assets/images/take out.png",
                          "assets/restaurent/takeaway.png",
                          () {
                            ExtendedNavigator.of(context).push(
                                '/restaurants/menu',
                                arguments: RestCatogriesArguments(
                                  isFromTakeAway: true,
                                  restId: widget.restaurantListModel.hashId,
                                  restSlogan: widget
                                      .restaurantListModel.storeInformation,
                                  isDelivery: false,
                                  coverImage:
                                      widget.restaurantListModel.fullCover,
                                  restName:
                                      widget.restaurantListModel.restaurant,
                                  restimage: widget.restaurantListModel.full,
                                ));
                          },
                          0.05.sh,
                          0.65.sh,
                        ),
                        Divider(
                          thickness: 5,
                          color: Colors.transparent,
                        ),
                      ],
                    ))
                : info.isLoading
                    ? Column(
                        children: List.generate(
                            MediaQuery.of(context).size.height ~/ 5,
                            (index) => Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: 0.01.sh,
                                      ),
                                      Container(
                                        width: 1.sw,
                                        height: 0.1.sh,
                                        child: Shimmer.fromColors(
                                          baseColor:
                                              Color.fromRGBO(0, 0, 0, 0.1),
                                          highlightColor: Color(0x44CCCCCC),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.grey,
                                              borderRadius:
                                                  BorderRadius.circular(8.0),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                      )
                    : Center(
                        child: Text(
                            'We are looking forward to serve you again!'))),
      ],
    );
  }

  void showAlertDialog(BackButtonBehavior backButtonBehavior,
      {VoidCallback cancel,
      VoidCallback confirm,
      VoidCallback backgroundReturn,
      Map imgUrl}) {
    BotToast.showAnimationWidget(
        clickClose: true,
        allowClick: true,
        onlyOne: true,
        crossPage: true,
        backButtonBehavior: backButtonBehavior,
        toastBuilder: (cancelFunc) => GestureDetector(
              onTap: () {
                cancelFunc();
                cancel?.call();
              },
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      color: Colors.black,
                      padding: EdgeInsets.all(10),
                      child: CachedNetworkImage(
                        imageUrl: imgUrl['full'] ?? '',
                        imageBuilder: (context, imageProvider) => Image.network(
                          imgUrl['full'],
                        ),
                        errorWidget: (context, url, error) => Container(
                          color: Colors.black12,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              AutoSizeText(
                                'Image Not Found',
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 8.0),
                              Icon(
                                Icons.error_outline,
                                size: 30.0,
                                color: Colors.red,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
        animationDuration: Duration(milliseconds: 300));
  }

  Future<RestaurantLocationModel> fetchLocation() =>
      RestaurantListModel.fetchRestaurantLocation(
              widget.restaurantListModel.hashId)
          .then((value) async {
        RestaurantLocationModel restaurantLocationModel =
            RestaurantLocationModel.fromJson(value);

        location = CameraPosition(
          target: LatLng(double.parse(restaurantLocationModel.latitude),
              double.parse(restaurantLocationModel.longitude)),
          zoom: 17.4746,
        );

        Marker resultMarker = Marker(
            markerId: MarkerId(restaurantLocationModel.hashid),
            infoWindow: InfoWindow(title: restaurantLocationModel.name),
            position: LatLng(double.parse(restaurantLocationModel.latitude),
                double.parse(restaurantLocationModel.longitude)));
        markers.add(resultMarker);

        return restaurantLocationModel;
      });

  Future<void> showMap(String hashid) async {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.white,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            return Stack(children: [
              FutureBuilder<RestaurantLocationModel>(
                  builder: (context,
                      AsyncSnapshot<RestaurantLocationModel> snapshot) {
                    if (snapshot.hasData)
                      return GoogleMap(
                        mapType: MapType.normal,
                        buildingsEnabled: true,
                        zoomGesturesEnabled: false,
                        scrollGesturesEnabled: false,
                        myLocationEnabled: false,
                        myLocationButtonEnabled: false,
                        markers: markers,
                        initialCameraPosition: location,
                        onMapCreated: (GoogleMapController controller) {},
                      );
                    else if (snapshot.hasError)
                      return Text(snapshot.error.toString());
                    else
                      return Center(child: CircularProgressIndicator());
                  },
                  future: fetchLocation()),
              Positioned(
                top: 30,
                left: 10,
                child: IconButton(
                  onPressed: () {
                    context
                        .read<PBrowseRestaurant>()
                        .swiperControl
                        .startAutoplay();
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    MdiIcons.chevronLeft,
                    size: 36,
                  ),
                  color: Colors.black,
                ),
              )
            ]);
          },
        );
      },
    );
  }

  Future<void> showImages(String hashid) async {
    List images = new List();
    bool hasResponse = false;

    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            if (!hasResponse)
              RestaurantListModel.fetchRestaurantImages(hashid).then((value) {
                setState2(() {
                  hasResponse = true;
                  images = value;
                });
              });

            return Container(
              height: 0.8.sh,
              color: Colors.white,
              child: images.length > 0
                  ? GridView.count(
                      physics: ClampingScrollPhysics(),
                      crossAxisCount: 3,
                      childAspectRatio: 1.0,
                      padding: const EdgeInsets.all(4.0),
                      mainAxisSpacing: 4.0,
                      crossAxisSpacing: 4.0,
                      children: List.generate(
                        images.length,
                        (index) => Container(
                          color: Colors.white,
                          child: CachedNetworkImage(
                            imageUrl: images[index]['full'] ?? '',
                            imageBuilder: (context, imageProvider) =>
                                GestureDetector(
                              onTap: () {
                                showAlertDialog(BackButtonBehavior.none,
                                    imgUrl: images[index]);
                              },
                              child: Image.network(
                                images[index]['full'],
                              ),
                            ),
                            errorWidget: (context, url, error) => Container(
                              color: Colors.black12,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  AutoSizeText('Image Not Found'),
                                  SizedBox(height: 8.0),
                                  Icon(
                                    Icons.error_outline,
                                    size: 30.0,
                                    color: Colors.red,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  : hasResponse
                      ? Column(
                          children: [
                            Expanded(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  MdiIcons.alertCircleOutline,
                                  color: Colors.red,
                                  size: 48,
                                ),
                                SizedBox(height: 8),
                                Text('IMAGE NOT FOUND',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500)),
                              ],
                            )),
                          ],
                        )
                      : ListView(
                          children: [
                            VideoShimmer(),
                            VideoShimmer(),
                            VideoShimmer(),
                            VideoShimmer()
                          ],
                        ),
            );
          },
        );
      },
    );
  }

  Future<void> showReviews(RestaurantListModel restaurantListModel) async {
    List<RestaurantReviewModel> reviews = new List<RestaurantReviewModel>();
    bool hasResponse = false;

    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            if (!hasResponse)
              RestaurantListModel.fetchRestaurantReviews(
                      restaurantListModel.hashId)
                  .then((value) {
                if (this.mounted) {
                  setState2(() {
                    hasResponse = true;
                    reviews = value;
                  });
                }
              });

            return Container(
              height: 0.8.sh,
              color: Colors.white,
              child: reviews.length > 0
                  ? Container(
                      margin: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 0.1.sh,
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  '${restaurantListModel.ratings.average.toDouble()}',
                                  style: TextStyle(
                                      fontSize: 42.sp,
                                      fontWeight: FontWeight.bold),
                                ),
                                RatingBarIndicator(
                                  rating: restaurantListModel.ratings.average
                                      .toDouble(),
                                  itemBuilder: (context, index) => Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  ),
                                  itemCount: 5,
                                  itemSize: 0.02.sh,
                                  direction: Axis.horizontal,
                                ),
                                SizedBox(height: 5),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      '(${restaurantListModel.ratings.count})',
                                      style: TextStyle(fontSize: 28.sp),
                                    ),
                                    Icon(
                                      MdiIcons.account,
                                      size: 30.sp,
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Divider(),
                          Expanded(
                            child: ListView.builder(
                              itemCount: reviews.length,
                              physics: ClampingScrollPhysics(),
                              itemBuilder: (context, index) => Card(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '${reviews[index].user}',
                                        style: TextStyle(
                                          fontSize: 34.sp,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          RatingBarIndicator(
                                            rating: reviews[index]
                                                .rating
                                                .toDouble(),
                                            itemBuilder: (context, index) =>
                                                Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            itemCount: 5,
                                            itemSize: 28.sp,
                                            direction: Axis.horizontal,
                                          ),
                                          datetime(reviews, index),
                                        ],
                                      ),
                                      SizedBox(height: 5.0),
                                      Text(
                                        '${reviews[index].content}',
                                        style: TextStyle(
                                          fontSize: 32.sp,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : hasResponse
                      ? Column(
                          children: [
                            Expanded(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  MdiIcons.alertCircleOutline,
                                  color: Colors.red,
                                  size: 48,
                                ),
                                SizedBox(height: 8),
                                Text('REVIEW NOT FOUND',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500)),
                              ],
                            )),
                          ],
                        )
                      : ListView(
                          physics: ClampingScrollPhysics(),
                          children: [
                            ListTileShimmer(),
                            ListTileShimmer(),
                            ListTileShimmer(),
                            ListTileShimmer(),
                          ],
                        ),
            );
          },
        );
      },
    );
  }

  Widget datetime(List<RestaurantReviewModel> reviews, int index) {
    final DateFormat formatter = DateFormat('MMM d yyyy');
    final String formatted = formatter.format(reviews[index].createdAt);

    return Text(
      '$formatted',
      style: TextStyle(fontSize: 24.sp, color: Colors.black38),
    );
  }

  Future<void> showInfo(String hashid) async {
    RestaurantInfo info;

    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            if (info == null)
              RestaurantListModel.fetchRestaurantInfo(hashid).then((value) {
                setState2(() {
                  info = RestaurantInfo.fromJson(value);
                });
              });

            return Container(
              height: 0.8.sh,
              color: Colors.white,
              child: info != null
                  ? ListView(
                      physics: ClampingScrollPhysics(),
                      children: [
                        Container(
                            decoration: BoxDecoration(
                                // gradient: LinearGradient(
                                //     begin: Alignment.topCenter,
                                //     end: Alignment.bottomCenter,
                                //     colors: [
                                //   Colors.blueAccent,
                                //   Config.primaryColor
                                // ],
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                        widget.restaurantListModel.fullCover ??
                                            ''))),
                            child: Container(
                              height: 350.0,
                              child: Center(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    CachedNetworkImage(
                                      imageUrl:
                                          widget.restaurantListModel.full ?? '',
                                      imageBuilder: (context, imageProvider) =>
                                          CircleAvatar(
                                        backgroundImage: NetworkImage(
                                          widget.restaurantListModel.full ?? '',
                                        ),
                                        radius: 50.0,
                                      ),
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                      errorWidget: (context, url, error) =>
                                          Icon(
                                        MdiIcons.alertCircle,
                                        color: Colors.red,
                                        size: 0.08.sh,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Text(
                                      '${info.name}',
                                      style: TextStyle(
                                        fontSize: 22.0,
                                        color: Colors.white,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Card(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 20.0, vertical: 5.0),
                                      clipBehavior: Clip.antiAlias,
                                      color: Colors.white,
                                      elevation: 5.0,
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8.0, vertical: 22.0),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Expanded(
                                              child: Column(
                                                children: [
                                                  Text(
                                                    'Phone',
                                                    style: TextStyle(
                                                      color: Colors.black87,
                                                      fontSize: 18.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 5.0,
                                                  ),
                                                  Text(
                                                    '${info.phone}',
                                                    style: TextStyle(
                                                      fontSize: 20.0,
                                                      color: Colors.black54,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                              child: Column(
                                                children: [
                                                  Text(
                                                    'Email',
                                                    style: TextStyle(
                                                      color: Colors.black87,
                                                      fontSize: 18.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 5.0,
                                                  ),
                                                  Text(
                                                    '${info.storeEmail}',
                                                    style: TextStyle(
                                                      fontSize: 20.0,
                                                      color: Colors.black54,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 30.0, horizontal: 16.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Info:",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontStyle: FontStyle.normal,
                                      fontSize: 28.0),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                AutoSizeText(
                                  '${info.storeInformation}',
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w300,
                                    color: Colors.black,
                                    letterSpacing: 2.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    )
                  : ProfilePageShimmer(hasBottomBox: true),
            );
          },
        );
      },
    );
  }

  catogoryContainer(String title, image, bgimag, Function onTap,
      double imageheight, bgimageWidth) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: onTap,
      child: Stack(
        children: [
          Container(
            height: size.height * 0.10,
            width: size.width * 0.9,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: <Color>[
                  Color(0xFF1c366d),
                  Color(0xFF182e5e),
                  Color(0xFF14264f),
                ],
              ),
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
          ),
          Positioned.fill(
            child: Align(
                alignment: Alignment.centerRight,
                child: Container(child: Image.asset(bgimag, height: 0.18.sw))),
          ),
          Positioned.fill(
              child: Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                children: [
                  Image.asset(
                    image,
                    height: imageheight,
                  ),
                  SizedBox(width: 20),
                  Container(
                    width: size.width * 0.5,
                    child: Text(
                      title,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                  ),
                ],
              ),
            ),
          ))
        ],
      ),
    );
  }
}
