import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/widgets/w_button.dart';

import '../../widgets/w_qr.dart';

class BookingConfirmationPage extends StatefulWidget {
  String guest, date, time, tableNumber;
  BookingConfirmationPage({this.time, this.date, this.tableNumber, this.guest});
  @override
  _BookingConfirmationPageState createState() =>
      _BookingConfirmationPageState();
}

class _BookingConfirmationPageState extends State<BookingConfirmationPage> {
  @override
  Widget build(BuildContext context) {
    TableRow _tableRow(String label, String value) {
      return TableRow(children: [
        TableCell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(label),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(value),
          ),
        ),
      ]);
    }

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'Restaurant Name',
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                  ),
                ),
                FractionallySizedBox(
                  widthFactor: 0.4,
                  child:
                      Image.asset('assets/images/samples/restaurant-logo.png'),
                ),
                SizedBox(height: 24.0),
                FractionallySizedBox(
                  widthFactor: 0.5,
                  child: Image.asset('assets/images/table-chairs.png'),
                ),
                Icon(
                  Icons.check_circle,
                  color: Colors.green,
                  size: 64.0,
                ),
                Text('Thank You for Your Reservation'),
                SizedBox(height: 24.0),
                Text('We have received your booking.'),
                SizedBox(height: 8.0),
                FractionallySizedBox(
                  widthFactor: 0.8,
                  child: Table(
                    border: TableBorder.all(color: Colors.grey),
                    children: [
                      _tableRow('Ref Number', '#123'),
                      _tableRow('Number of guest', '${widget.guest}'),
                      _tableRow('Date', '${widget.date}'),
                      _tableRow('Time', '${widget.time}'),
                      _tableRow('Table number', '${widget.tableNumber}'),
                      _tableRow('Seats', '4'),
                      _tableRow('Location', 'Indoor'),
                      _tableRow('Special Instruction', 'Sample'),
                      _tableRow('Ordered Dish', 'QAR 500.00'),
                    ],
                  ),
                ),
                SizedBox(height: 24.0),
                FractionallySizedBox(
                  widthFactor: 0.5,
                  child: WButtonWidget(
                    title: 'Qr Code',
                    onPressed: () {
                      setState(() {
                        _showBarcode();
                      });
                    },
                  ),
                ),
                FractionallySizedBox(
                  widthFactor: 0.8,
                  child: Text(
                    'Kindly note that your reservation will be valid until 15 minutes from the time of booking time. Kindly make sure to arrive before that or the reservation will be cancelled automatically.',
                    textAlign: TextAlign.center,
                  ),
                ),
                Divider(),
                FractionallySizedBox(
                  widthFactor: 0.7,
                  child: WButtonWidget(
                    title: 'Back to Home',
                    onPressed: () {
                      ExtendedNavigator.of(context).root.push('/');
                    },
                  ),
                ),
                Divider(),
                InkWell(
                  child: Text('Check Reservations'),
                  onTap: () {
                    ExtendedNavigator.of(context).root.push('/reservations');
                  },
                ),
                SizedBox(height: 50.0),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showBarcode() {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            return Container(
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  )),
              child: Wrap(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Confirmation code\n Show to staff',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      IconButton(
                        icon: Icon(Icons.close),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                  JQRWidget(
                    size: MediaQuery.of(context).size.width * 0.8,
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
