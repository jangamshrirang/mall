import 'package:flutter/material.dart';
import 'package:marquee_widget/marquee_widget.dart';

import '../AppbarForAllWalwtScreens.dart';
import '../CouponsScree.dart';
import 'LuckyPioseTabScreen.dart';

final List promotions = [
  {
    'image':
        "https://qa.dohabank.com/wp-content/uploads/sites/12/Al-Dana-6th-Draw-Winners-En.jpg",
    'text':
        "Balloon Blast: Cash in from Aug 29 -Sep8 for a chance to win up to Qr 9,000 CREDITS",
    'validDate': "01/05/2020"
  },
  {
    'image': "https://i.ytimg.com/vi/pNpbmjSMUwI/hqdefault.jpg",
    'text':
        "Win up to Qr 5,000 Wallet Credits using your Wmall Wallet or Wcash from Aug",
    'validDate': "02/10/2020"
  },
  {
    'image':
        "https://www.ibs-uae.com/innovate/wp-content/uploads/2015/04/MARK-1200x629.jpg",
    'text':
        "Learn how to activ,cas in,and pay with your Wmall Wallet by cliking here",
    'validDate': "03/08/2020"
  }
];

class LuckyPisoScreen extends StatefulWidget {
  @override
  _LuckyPisoScreenState createState() => _LuckyPisoScreenState();
}

class _LuckyPisoScreenState extends State<LuckyPisoScreen> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "Lucky Piso"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                height: size.height * 0.28,
                width: size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    image: DecorationImage(
                      image: NetworkImage(promotions[1]['image']),
                      fit: BoxFit.cover,
                    )),
                child: Container(
                  color: Colors.orange,
                  margin: EdgeInsets.only(
                    top: size.height * 0.25,
                  ),
                  child: Row(
                    children: [
                      Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Icon(
                            Icons.volume_up,
                            color: Colors.white,
                            size: 15,
                          )),
                      Container(
                        color: Colors.orange,
                        width: size.width * 0.5,
                        margin: EdgeInsets.only(left: 10),
                        child: Marquee(
                          child: Text(
                            promotions[1]['text'],
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: size.height * 0.020),
                          ),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(left: size.width * 0.1),
                          child: Text(
                            "Winner Board",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.normal,
                                fontSize: size.height * 0.020),
                          ))
                    ],
                  ),
                )),
            SizedBox(
              height: size.height,
              child: LuckyPiosTabScreen(),
            )
          ],
        ),
      ),
    );
  }
}
