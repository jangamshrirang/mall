import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/widgets/w_loading.dart';
import 'package:wblue_customer/widgets/w_rating.dart';

class WMenuGridItemWidget extends StatelessWidget {
  final double itemHeight;
  final Function onTap;
  String image, name;
  var price;

  WMenuGridItemWidget(
      {this.itemHeight: 200,
      this.onTap: _function,
      this.image,
      this.name,
      this.price});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: InkWell(
        child: Container(
    
          decoration: BoxDecoration(
            color: Colors.white,
            // image: DecorationImage(
            //   image: NetworkImage("https://cdn.dribbble.com/users/1012566/screenshots/4187820/topic-2.jpg"),
            //   fit: BoxFit.cover,
            // ),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 2,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CachedNetworkImage(
                height: size.height * 0.18,
                imageUrl: image ?? '',
                imageBuilder: (context, imageProvider) => Container(
                  height: itemHeight * 0.8,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(image ?? ''),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0),
                    ),
                  ),
                ),
                placeholder: (context, url) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: WLoadingWidget(),
                ),
                errorWidget: (context, url, error) => Container(
                  height: itemHeight * 0.6,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(
                          "https://cdn.dribbble.com/users/1012566/screenshots/4187820/topic-2.jpg"),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 4.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      name,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: Config.fontFamily,
                          fontSize: size.height * 0.02),
                    ),
                    SizedBox(height: size.height * 0.001),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'QAR ' + "$price",
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: size.height * 0.02),
                        ),
                        WRatingsWidget(3.5,
                            size: size.height * 0.015, compact: true),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        onTap: () {
          onTap();
        },
      ),
    );
  }
}

void _function() {}
