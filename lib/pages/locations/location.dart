import 'package:auto_route/auto_route.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hive/hive.dart';
import 'package:latlong/latlong.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/google/map.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/services/helper.dart';

class LocationPage extends StatefulWidget {
  @override
  _LocationPageState createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  String msgProgress = 'Detecting...';
  String status = 'Use Current Location';
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        // appBar: AppBar(
        //   automaticallyImplyLeading: false,
        //   leading: IconButton(
        //     icon: Icon(Icons.keyboard_backspace),
        //     onPressed: () => ExtendedNavigator.of(context).pop(false),
        //   ),
        // ),
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        children: [
                          Text(
                            'Find restaurants near you!',
                            style: TextStyle(fontSize: 40.sp, fontWeight: FontWeight.bold, color: Config.primaryColor),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 28.0, vertical: 12.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Image.asset('assets/images/find-restaurants.png'),
                                SizedBox(height: 0.03.sw),
                                SizedBox(
                                  height: 0.1.sw,
                                  child: RaisedButton(
                                    highlightColor: Colors.transparent,
                                    highlightElevation: 0.0,
                                    onPressed: getLocation,
                                    child: Text(status, style: TextStyle(fontSize: 32.sp)),
                                    color: Config.primaryColor,
                                    elevation: 0,
                                    textColor: Colors.white,
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                                  ),
                                ),
                                SizedBox(height: 0.03.sw),
                                Text(
                                  'WMall wil laccess your location only while using the app',
                                  style: TextStyle(fontSize: 28.sp, fontWeight: FontWeight.w400, color: Colors.black38),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      // FlatButton(
                      //   color: Colors.transparent,
                      //   splashColor: Colors.transparent,
                      //   highlightColor: Colors.transparent,
                      //   onPressed: () {},
                      //   child: Text(
                      //     'Select another location',
                      //     style: TextStyle(fontSize: 28.sp, fontWeight: FontWeight.bold, color: Config.primaryColor),
                      //     textAlign: TextAlign.center,
                      //   ),
                      // ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  getLocation() async {
    try {
      GoogleAddressModel _location;

      setState(() {
        status = msgProgress;
      });

      Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      debugPrint('location: ${position.latitude}');

      final coordinates = new Coordinates(position.latitude, position.longitude);
      var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);

      final client = Dio();

      final url =
          'https://maps.googleapis.com/maps/api/geocode/json?latlng=${addresses[0].coordinates.latitude},${addresses[0].coordinates.longitude}&key=${Config.googleMapAPI}';

      final response = await client.get(url);
      setState(() {
        status = 'Use Current Location';
      });

      if (response.statusCode == 200) {
        List result = response.data['results'];
        _location = GoogleAddressModel.fromJson(result[0]);

        String street = _location.addressComponents[0].longName;
        String area = _location.addressComponents[1].longName;
        String lat = _location.geometry.location.lat.toString();
        String lng = _location.geometry.location.lng.toString();

        UserAddressModel userAddressModel = UserAddressModel(
            nickname: '$street ($area)',
            hashid: Helper.defaultAddrHashid,
            area: area,
            street: street,
            latitude: lat,
            longitude: lng);

        final addr = Hive.box('area');
        await addr.put('basic-address', userAddressModel.toJson());
      } else {
        print('Field to fetch the location');
      }
    } catch (error) {
      print(error);
    }

    ExtendedNavigator.of(context).pop(true);
  }
}

class WMap extends StatefulWidget {
  @override
  _WMapState createState() => _WMapState();
}

class _WMapState extends State<WMap> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
