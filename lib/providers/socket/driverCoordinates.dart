import 'package:flutter/material.dart';

class DriverCoordinatesScocket extends ChangeNotifier {
  Map<String, dynamic> driverCoordinates = new Map<String, dynamic>();
  Map get driverCoordinate => driverCoordinates;
  bool _isUpdated = false;
  bool get isUpdated => _isUpdated;
  bool _canBack = false;

  bool get canBack => _canBack;

  void whenDriverCoordinatesUpdated(data) {
    _isUpdated = true;
    driverCoordinates = data;
    notifyListeners();
  }

  void setDriverCoordinatesUpdated(bool status) {
    _isUpdated = status;
    notifyListeners();
  }

  void setBackStatus(bool status) {
    _canBack = status;
  }
}
