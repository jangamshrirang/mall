import 'package:flutter/material.dart';
import 'package:wblue_customer/models/auth.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();

class PUserProfile with ChangeNotifier {
  AuthModel authModel = AuthModel();
  UserModel _user = UserModel();
  bool _isAuth = false;
  bool get isAuth => _isAuth;
  UserModel get user => _user;

  Future<void> fetchUserProfile() async {
    _user = await UserModel.fetchMyProfile();
    notifyListeners();
  }

  Future<void> isAuthenticated() async {
    _isAuth = await authModel.checkAuthentication();
    notifyListeners();
  }
}
