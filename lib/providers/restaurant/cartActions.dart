import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();
Future fetchRestaurantClearCart() async {
  // Map<String, dynamic> _body = {
  //   'food_id': foodId,
  // };
  APIResponse res = await _dioClient.privatePost(
    '/restaurant/cart/clear',
  );
  if (res.code >= 400) {
    return [];
  }

  return res.data;
}

Future updateCart(var foodId, cartNo, quantity) async {
  Map<String, dynamic> _body = {
    'cart_hash_id': cartNo,
    'food_hash_id': foodId,
    'quantity': quantity,
  };
  APIResponse res = await _dioClient
      .privatePost('/restaurant/cart/update-quantity', data: _body);
  if (res.code >= 400) {
    return [];
  }

  return res.data;
}

Future fetchRestaurantRemoveItemCarts(var foodId, cartNo) async {
  Map<String, dynamic> _body = {
    'cart_no': cartNo,
    'food_id': foodId,
  };
  APIResponse res =
      await _dioClient.privatePost('/restaurant/cart/remove', data: _body);
  if (res.code >= 400) {
    return [];
  }

  return res.data;
}
