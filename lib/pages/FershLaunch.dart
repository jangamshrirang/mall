import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/landing.dart';

class FereshLaunch extends StatelessWidget {
  final controller = PageController(viewportFraction: 1.0);
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        mini: true,
        elevation: 0,
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => LandingPage()));
        },
        child: Icon(Icons.arrow_forward, color: Config.primaryColor),
      ),
      body: SafeArea(
        child: Stack(
          children: [
            PageView(
              controller: controller,
              children: <Widget>[
                Container(
                  child: Image.asset(
                    "assets/launcher/1.png",
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  child: Image.asset(
                    "assets/launcher/2.png",
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  child: Image.asset(
                    "assets/launcher/3.png",
                    fit: BoxFit.fill,
                  ),
                ),
              ],
              pageSnapping: true,
            ),
            Container(
              margin: EdgeInsets.only(
                  top: size.height * 0.7, left: size.width * 0.35),
              child: SmoothPageIndicator(
                controller: controller,
                count: 3,
                effect: ExpandingDotsEffect(
                  activeDotColor: Colors.white,
                  dotHeight: 10.0,
                  expansionFactor: 2,
                ),
              ),
            ),
            // Container(
            //     margin: EdgeInsets.only(
            //         top: size.height * 0.9, left: size.width * 0.05),
            //     child: Text(
            //       "Skip",
            //       style: TextStyle(color: Colors.white),
            //     ))
          ],
        ),
      ),
    );
  }
}
