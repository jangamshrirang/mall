import 'package:auto_route/auto_route.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_payment.dart';

class HotelPaymentPage extends StatefulWidget {
  @override
  _HotelPaymentPageState createState() => _HotelPaymentPageState();
}

class _HotelPaymentPageState extends State<HotelPaymentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Payment'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Text('Amount to be Paid'),
            SizedBox(height: 4.0),
            Text(
              Global.money(692.1),
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 16.0),
            // WPaymentWidget(
            //   itemName: 'Twin Room - Hotel Name',
            //   amount: 692.1,
            //   onConfirmTap: () {
            //     ExtendedNavigator.of(context).root.push('/hotels/confirmation');
            //   },
            // ),
            SizedBox(height: 16.0),
            DottedBorder(
              color: Colors.lightBlue,
              child: Container(
                padding: EdgeInsets.all(16.0),
                decoration: BoxDecoration(color: Colors.lightBlue[100], borderRadius: BorderRadius.circular(10.0)),
                child: Column(
                  children: <Widget>[
                    Text(
                      'For Question and Inquiries',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 8.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.email),
                        SizedBox(width: 4.0),
                        Text('support@jahzly.com'),
                      ],
                    ),
                    SizedBox(height: 8.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.phone),
                        SizedBox(width: 4.0),
                        Text('4400 0000'),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
