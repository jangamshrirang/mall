// import 'dart:typed_data';

// import 'package:flare_flutter/flare_actor.dart';
// import 'package:flutter/foundation.dart';
// import 'package:flutter/gestures.dart';
// import 'package:flutter/material.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:google_map_polyline/google_map_polyline.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:wblue_customer/env/config.dart';
// import 'dart:ui' as ui;
// import 'package:flutter/services.dart';
// import 'package:wblue_customer/models/Restaurant/MyOrderListModel.dart';
// import 'package:wblue_customer/pages/MyOrders/OrderCardExp.dart';
// import 'package:wblue_customer/widgets/navigateScreen.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:wblue_customer/widgets/w_loading.dart';

// class MyOrderMapCard extends StatefulWidget {
//   MyOrderListModel myOrderListModel;

//   String rname,
//       rImage,
//       status,
//       orderNo,
//       date,
//       time,
//       orderType,
//       paymentType,
//       rLattitude,
//       rLangitude,
//       dLattitude,
//       dLangitude,
//       dName,
//       dRatings,
//       estimatedTime,
//       total,
//       subTotal,
//       serviceCharge,
//       qrCode,
//       cNumber,
//       cLangitude,
//       cLattitude;
//   List foodList;
//   int noOfDeliveries;
//   MyOrderMapCard(
//       {this.rname,
//       this.rImage,
//       this.status,
//       this.orderNo,
//       this.date,
//       this.time,
//       this.orderType,
//       this.paymentType,
//       this.rLattitude,
//       this.rLangitude,
//       this.dLattitude,
//       this.dLangitude,
//       this.dName,
//       this.dRatings,
//       this.noOfDeliveries,
//       this.estimatedTime,
//       this.total,
//       this.subTotal,
//       this.serviceCharge,
//       this.qrCode,
//       this.cNumber,
//       this.cLangitude,
//       this.cLattitude,
//       this.foodList});
//   @override
//   _MyOrderMapCardState createState() => _MyOrderMapCardState();
// }

// class _MyOrderMapCardState extends State<MyOrderMapCard> {
//   BitmapDescriptor driverPointIcon;
//   BitmapDescriptor restaurentPointIcon;
//   BitmapDescriptor customerPointIcon;
//   GoogleMapPolyline googleMapPolyline =
//       new GoogleMapPolyline(apiKey: "AIzaSyCpZybhi4ZzUBXImzkKtaggzrOSsfcH8p4");
//   final Set<Polyline> polyline = {};
//   Set<Marker> _markers = {};
//   GoogleMapController _controller;
//   List<LatLng> driverTOrestaurentCordinates;
//   List<LatLng> driverTOcustometCordinates;
//   bool initialiesedLocation;
//   var lat, long;

//   @override
//   void initState() {
//     super.initState();
//     getIcons();
//     _getCurrentLocation();
//   }

// //this method will convert image to bytes so that we can use this on map point icon
//   Future<Uint8List> getBytesFromAsset(String path, int width) async {
//     ByteData data = await rootBundle.load(path);
//     ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
//         targetWidth: width);
//     ui.FrameInfo fi = await codec.getNextFrame();
//     return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
//         .buffer
//         .asUint8List();
//   }

// // getiing map points images
//   getIcons() async {
//     var driverIcon = await getBytesFromAsset('assets/images/rider.png', 80);
//     var restaurentIcon = await getBytesFromAsset('assets/images/resto.png', 80);
//     var customerIcon = await getBytesFromAsset('assets/images/van.png', 80);
//     setState(() {
//       this.driverPointIcon = BitmapDescriptor.fromBytes(driverIcon);
//       this.restaurentPointIcon = BitmapDescriptor.fromBytes(restaurentIcon);
//       this.customerPointIcon = BitmapDescriptor.fromBytes(customerIcon);
//     });
//   }

// //getting cordinates for polyLine in map
//   location(
//     var lattiude,
//     longitude,
//   ) async {
//     driverTOrestaurentCordinates =
//         await googleMapPolyline.getCoordinatesWithLocation(
//             origin: LatLng(lattiude, longitude),
//             destination: LatLng(double.parse(widget.rLattitude),
//                 double.parse(widget.rLangitude)),
//             mode: RouteMode.driving);

//     driverTOcustometCordinates =
//         await googleMapPolyline.getCoordinatesWithLocation(
//             origin: LatLng(double.parse(widget.rLattitude),
//                 double.parse(widget.rLangitude)),
//             destination: LatLng(25.301236, 51.488511),
//             mode: RouteMode.driving);

//     setState(() {
//       this.driverTOrestaurentCordinates = driverTOrestaurentCordinates;
//       this.driverTOcustometCordinates = driverTOcustometCordinates;

//       print(driverTOcustometCordinates);
//       print(driverTOrestaurentCordinates);
//     });
//   }

//   //to get the current location
//   void _getCurrentLocation() async {
//     final position = await Geolocator()
//         .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
//     print(position);

//     setState(() {
//       _locationMessage = "${position.latitude}, ${position.longitude}";
//       lat = position.latitude;
//       long = position.longitude;
//       initialiesedLocation == null || initialiesedLocation == false
//           ? location(lat, long)
//           : print(_locationMessage);
//     });
//   }

//   String _locationMessage = "";

//   @override
//   Widget build(BuildContext context) {
//     var size = MediaQuery.of(context).size;
//     return Scaffold(
//         body: Container(
//             // height: size.height * 0.39,
//             // width: size.width,
//             // decoration: BoxDecoration(
//             //   borderRadius: BorderRadius.circular(10.0),
//             //   border: Border.all(color: Colors.black26),
//             // ),
//             child: Column(children: [
//       // restaurent details
//       GestureDetector(
//         onTap: () {
//           navigate(
//               context,
//               MyOrderCardExp(
//                 rname: widget.rname,
//                 rImage: widget.rImage,
//                 orderNo: widget.orderNo,
//                 orderType: widget.orderType,
//                 status: widget.status,
//                 foodList: widget.foodList,
//                 paymentType: widget.paymentType,
//                 dName: widget.dName,
//                 dRatings: widget.dRatings,
//                 noOfDeliveries: widget.noOfDeliveries,
//                 estimatedTime: widget.estimatedTime,
//                 total: widget.total,
//                 cNumber: widget.cNumber,
//                 serviceCharge: widget.serviceCharge,
//                 qrCode: widget.qrCode,
//                 subTotal: widget.subTotal,
//                 time: widget.time,
//                 date: widget.date,
//               ));
//         },
//         child: Row(
//           children: [
//             // Container(
//             //   margin: EdgeInsets.only(left: size.width * 0.02, top: size.height * 0.01),
//             //   height: size.height * 0.1,
//             //   width: size.width * 0.25,
//             //   decoration: BoxDecoration(
//             //       image: DecorationImage(
//             //         image: NetworkImage(widget.rImage),
//             //         fit: BoxFit.contain,
//             //       ),
//             //       borderRadius: BorderRadius.circular(10.0),
//             //       border: Border.all(color: Colors.white)),
//             // ),
//             // SizedBox(width: size.width * 0.01),
//             // Column(
//             //   crossAxisAlignment: CrossAxisAlignment.start,
//             //   children: [
//             //     Container(
//             //       width: size.width * 0.35,
//             //       child: text(
//             //           widget.rname == null ? "No Name" : widget.rname, Colors.black, FontWeight.bold, 36.sp),
//             //     ),
//             //     SizedBox(height: size.height * 0.005),
//             //     Row(
//             //       children: [
//             //         text(
//             //             widget.status,
//             //             widget.status == "Order Recieved" ? Config.primaryColor : Colors.amber,
//             //             FontWeight.normal,
//             //             32.sp),
//             //         SizedBox(height: size.height * 0.01),
//             //         widget.status == "Order Recieved"
//             //             ? Text("")
//             //             : Image.asset("assets/images/fire.png", width: size.width * 0.03)
//             //       ],
//             //     ),
//             //     SizedBox(height: size.height * 0.005),
//             //     text(widget.orderNo, Colors.black54, FontWeight.normal, size.height * 0.02),
//             //     SizedBox(height: size.height * 0.005),
//             //   ],
//             // ),
//             // Stack(
//             //   children: [
//             //     Container(
//             //       height: size.height * 0.1,
//             //       width: size.width * 0.2,
//             //       child: widget.status == "Order Recieved"
//             //           ? Text("")
//             //           : FlareActor("assets/flares/landing/cooking.flr", animation: "normal-loop"),
//             //     ),
//             //     Container(
//             //         margin: EdgeInsets.only(left: size.width * 0.17, top: size.height * 0.009),
//             //         child: Icon(
//             //           Icons.arrow_forward_ios,
//             //           color: Colors.black26,
//             //           size: size.height * 0.015,
//             //         ))
//             //   ],
//             // ),
//           ],
//         ),
//       ),
//       // SizedBox(height: size.height * 0.01),
//       // Row(
//       //   children: [
//       //     iconContainer(widget.date.toString().substring(0, 10), Icons.calendar_today),
//       //     iconContainer(widget.time, Icons.watch_later),
//       //     iconContainer(widget.orderType, Icons.dinner_dining),
//       //   ],
//       // ),
//       // SizedBox(height: size.height * 0.01),
//       //google map
//       driverTOrestaurentCordinates == null
//           ? Center(child: WLoadingWidget())
//           : Container(
//               height:
//                   //  widget.orderType == "Deliver"
//                   //     ? size.height * 0.2
//                   //     :
//                   size.height * 0.5,
//               width: size.width * 0.85,
//               child: Container(
//                 margin: EdgeInsets.only(top: size.height * 0.2),
//                 height: size.height * 0.6,
//                 width: double.infinity,
//                 child: GoogleMap(
//                   tiltGesturesEnabled: false,
//                   onMapCreated: onMapCreated,
//                   polylines: polyline,
//                   initialCameraPosition:
//                       CameraPosition(target: LatLng(lat, long), zoom: 14.3),
//                   mapType: MapType.terrain,
//                   markers: _markers,
//                 ),
//               ),
//             )
//     ])));
//   }

//   void onMapCreated(GoogleMapController controller) {
//     setState(() {
//       _controller = controller;
//       polyline.add(Polyline(
//           polylineId: PolylineId('route1'),
//           visible: true,
//           points: driverTOrestaurentCordinates,
//           width: 3,
//           color: Colors.amber,
//           startCap: Cap.roundCap,
//           endCap: Cap.buttCap));
//       polyline.add(Polyline(
//           polylineId: PolylineId('route2'),
//           visible: true,
//           points: driverTOcustometCordinates,
//           width: 3,
//           color: Config.primaryColor,
//           startCap: Cap.roundCap,
//           endCap: Cap.buttCap));
// //driver to rectaurent points
//       _markers.add(Marker(
//           markerId: MarkerId('<MARKER_DriverId>'),
//           position: LatLng(lat, long), //Keep driber cordinates
//           icon: driverPointIcon));
//       _markers.add(Marker(
//           markerId: MarkerId('<MARKER_RestaurenId>'),
//           position: LatLng(double.parse(widget.rLattitude),
//               double.parse(widget.rLangitude)), //keep resturent cordinates here
//           icon: restaurentPointIcon));
//       _markers.add(Marker(
//           markerId: MarkerId('<MARKER_CustomerId>'),
//           position:
//               LatLng(25.301236, 51.488511), //keep Customer cordinates here
//           icon: BitmapDescriptor.defaultMarkerWithHue(10.0)));
//     });
//   }

//   text(String title, Color color, FontWeight fontWeight, double fontsize) {
//     return Container(
//       child: Text(
//         title ?? '',
//         maxLines: 1,
//         style: TextStyle(
//             color: color,
//             fontSize: fontsize,
//             fontWeight: fontWeight,
//             fontFamily: Config.fontFamily),
//       ),
//     );
//   }

//   iconContainer(String title, IconData iconData) {
//     var size = MediaQuery.of(context).size;
//     return Container(
//       margin: EdgeInsets.only(left: size.width * 0.035),
//       width: size.width * 0.25,
//       height: size.height * 0.04,
//       decoration: BoxDecoration(
//         color: Colors.black12,
//         borderRadius: BorderRadius.circular(10.0),
//       ),
//       child: Row(
//         children: [
//           Spacer(),
//           Icon(
//             iconData,
//             size: size.height * 0.02,
//           ),
//           Spacer(),
//           text(title, Colors.black, FontWeight.normal, 28.sp),
//           Spacer(),
//         ],
//       ),
//     );
//   }
// }
