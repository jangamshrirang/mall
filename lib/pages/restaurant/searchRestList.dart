import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/R_ListByRatings.dart';
import 'package:wblue_customer/models/Restaurant/food_type_model.dart';
import 'package:wblue_customer/providers/restaurant/R_searchHelper.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/providers/restaurant/p_food_list_type.dart';
import 'package:wblue_customer/providers/restaurant/p_restaurant_list.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/widgets/w_drawer.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RestSearch extends StatefulWidget {
  @override
  _RestSearchState createState() => _RestSearchState();
}

class _RestSearchState extends State<RestSearch> with SingleTickerProviderStateMixin {
  final searchController = TextEditingController();
  List<bool> isFavorite;
  TabController _tabController;
  bool hasText;
  int selectedTab = 0;

  final List<Tab> myTabs = <Tab>[
    Tab(text: 'Restaurants'),
    Tab(text: 'Foods'),
  ];

  Future<bool> _canFollow() async {
    bool res = false;

    bool canNav = await ExtendedNavigator.of(context).root.canNavigate('/restaurants/cart');

    res = canNav ?? false;

    return res;
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: myTabs.length, initialIndex: selectedTab);
  }

  @override
  void dispose() {
    _tabController.dispose();
    searchController.dispose();
    super.dispose();
  }

  Widget footer = CustomFooter(
    builder: (BuildContext context, LoadStatus mode) {
      Widget body;

      if (mode == LoadStatus.idle) {
        body = Text("pull up load");
      } else if (mode == LoadStatus.loading) {
        body = CupertinoActivityIndicator();
      } else if (mode == LoadStatus.failed) {
        body = Text("Load Failed!Click retry!");
      } else if (mode == LoadStatus.canLoading) {
        body = Text("release to load more");
      } else {
        body = Text("No more Data");
      }
      return Container(
        height: 55.0,
        child: Center(child: body),
      );
    },
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: WDrawerWidget('browse-restaurant'),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: SafeArea(
          child: Container(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  height: 50.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      IconButton(
                        highlightColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        icon: Icon(
                          Icons.keyboard_backspace,
                          size: 30,
                        ),
                        onPressed: () => ExtendedNavigator.of(context).pop(),
                      ),
                      SizedBox(width: 0.05.sw),
                      Expanded(
                        child: TextField(
                          controller: searchController,
                          autofocus: true,
                          decoration:
                              InputDecoration.collapsed(border: InputBorder.none, hintText: 'Enter Resturent name'),
                          onChanged: (value) async {
                            value.length != 0 ? hasText = true : hasText = false;

                            context.read<R_SearchHelper>().fetchSearchRest(value);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  color: Config.primaryColor.withAlpha(06),
                  child: Row(
                    children: [
                      TabBar(
                          controller: _tabController,
                          indicator: UnderlineTabIndicator(
                              borderSide: BorderSide(width: 3, color: Config.primaryColor),
                              insets: EdgeInsets.only(
                                left: 8,
                                right: 8,
                              )),
                          labelPadding: EdgeInsets.only(left: 8, right: 8),
                          isScrollable: true,
                          unselectedLabelColor: Colors.black.withOpacity(0.3),
                          indicatorColor: Colors.black,
                          labelColor: Config.primaryColor,
                          labelStyle: TextStyle(fontSize: 28.sp),
                          onTap: (index) => setState(() {
                                selectedTab = index;
                              }),
                          tabs: myTabs),
                    ],
                  ),
                ),
                Consumer2<R_SearchHelper, PRestaurantList>(
                  builder: (context, search, pRestaurantList, child) => Expanded(
                    child: IndexedStack(
                      index: selectedTab,
                      children: [
                        SmartRefresher(
                            enablePullDown: true,
                            enablePullUp: true,
                            footer: footer,
                            controller: search.refreshRestaurantController,
                            onRefresh: search.onRestaurantRefresh,
                            onLoading: search.onRestaurantLoading,
                            child: ListView.builder(
                              itemBuilder: (context, index) => _card(search.restaurantsSearch[index], search),
                              itemCount: search.restaurantsSearch.length,
                            )),
                        SmartRefresher(
                            enablePullDown: true,
                            enablePullUp: true,
                            footer: footer,
                            controller: search.refreshRestaurantController,
                            onRefresh: search.onRestaurantRefresh,
                            onLoading: search.onRestaurantLoading,
                            child: GridView.builder(
                                itemCount: search.foodsSearch.length,
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 1,
                                  childAspectRatio: 3,
                                  crossAxisSpacing: 8,
                                ),
                                itemBuilder: (context, index) {
                                  return GestureDetector(
                                    onTap: () {
                                      // search.foodsSearch[index]
                                      //         .isContainsVariation
                                      //     ? ExtendedNavigator.of(context).push(
                                      //         '/variation',
                                      //         arguments:
                                      //             RestaurantFoodVariationArguments(
                                      //           food: search.foodsSearch[index],
                                      //           rHashID: widget.restId,
                                      //           fromTakeAway:
                                      //               widget.fromTakeAway,
                                      //           delivery: widget.delivery,
                                      //           preorder: widget.preOrder,
                                      //           fromScan: widget.fromScan,
                                      //           reservationHashId:
                                      //               widget.reservationHashId,
                                      //         ))
                                      //     :
                                      _addToCartBtmSheet(search.foodsSearch[index]);
                                    },
                                    child: Stack(
                                      children: [
                                        Container(
                                            margin: const EdgeInsets.symmetric(horizontal: 8.0),
                                            decoration: BoxDecoration(
                                              border: Border(
                                                bottom: BorderSide(color: Colors.black12, width: 0.5),
                                              ),
                                            ),
                                            child: Container(
                                              margin: EdgeInsets.symmetric(
                                                vertical: 8.0,
                                              ),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Flexible(
                                                    child: Container(
                                                      padding: EdgeInsets.only(right: 8.0),
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Flexible(
                                                            child: Text(
                                                              '${search.foodsSearch[index].name}',
                                                              style: TextStyle(
                                                                  fontWeight: FontWeight.bold, fontSize: 34.sp),
                                                              maxLines: 1,
                                                              overflow: TextOverflow.ellipsis,
                                                            ),
                                                          ),
                                                          SizedBox(height: 0.008.sh),
                                                          AutoSizeText(
                                                            '${search.foodsSearch[index].description}',
                                                            style: TextStyle(
                                                                fontWeight: FontWeight.w400,
                                                                color: Colors.black54,
                                                                fontSize: 29.sp),
                                                            maxLines: 4,
                                                            overflow: TextOverflow.ellipsis,
                                                          ),
                                                          SizedBox(height: 0.008.sh),
                                                          Flexible(
                                                            child: AutoSizeText(
                                                              'QR ${search.foodsSearch[index].price}',
                                                              style: TextStyle(
                                                                  fontWeight: FontWeight.w500, fontSize: 32.sp),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  CachedNetworkImage(
                                                    imageUrl: search.foodsSearch[index].full ?? '',
                                                    imageBuilder: (context, imageProvider) => AspectRatio(
                                                      aspectRatio: 5 / 5,
                                                      child: Container(
                                                        decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.circular(10),
                                                          image: DecorationImage(
                                                            image: imageProvider,
                                                            fit: BoxFit.fill,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    placeholder: (context, url) => AspectRatio(
                                                      aspectRatio: 5 / 5,
                                                      child: Container(
                                                        decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.circular(10),
                                                          image: DecorationImage(
                                                            image:
                                                                NetworkImage(search.foodsSearch[index].thumbnail ?? ''),
                                                            fit: BoxFit.fill,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    errorWidget: (context, url, error) => SizedBox(),
                                                  ),
                                                ],
                                              ),
                                            )),
                                      ],
                                    ),
                                  );
                                })),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _card(RestaurantListModel restaurant, R_SearchHelper rsearchHelper) {
    var size = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () {
        // ExtendedNavigator.of(context).push('/restaurants/single/');
        ExtendedNavigator.of(context).push(
          '/restaurants/single',
          arguments: SingleRestaurantPageArguments(restaurantListModel: restaurant),
        );
      },
      child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6.0)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: [
                  CachedNetworkImage(
                    imageUrl: restaurant?.fullCover,
                    imageBuilder: (context, imageProvider) => Container(
                      height: size.width * 0.5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(6), topRight: Radius.circular(6)),
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => Container(
                      height: size.width * 0.5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(6), topRight: Radius.circular(6)),
                        image: DecorationImage(
                          image: NetworkImage(restaurant?.thumbnailCover ?? ''),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                      right: 10,
                      top: 10,
                      child: GestureDetector(
                        onTap: () async {
                          bool canFollow = await _canFollow();
                          if (canFollow) context.read<PRestaurantList>().followApi(restaurant);
                        },
                        child: Container(
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                            ),
                            child: Icon(
                              restaurant.isFollowed ? MdiIcons.heart : MdiIcons.heartOutline,
                              color: Colors.red,
                              size: 18,
                            )),
                      )),
                  Positioned(
                    left: 10,
                    top: 10,
                    child: GestureDetector(
                      onTap: () {
                        showImages(context: context, full: restaurant.full, thumbnail: restaurant.thumbnail);
                      },
                      child: Container(
                        padding: EdgeInsets.all(1.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4.0),
                          color: Colors.white,
                        ),
                        height: 60,
                        width: 60,
                        child: CachedNetworkImage(
                          imageUrl: restaurant?.full ?? '',
                          imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.circular(4.0),
                            ),
                          ),
                          placeholder: (context, url) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(restaurant?.thumbnail ?? ''),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.circular(4.0),
                            ),
                          ),
                          errorWidget: (context, url, error) => Icon(Icons.error),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Container(
                            child: Text(
                              '${restaurant.restaurant}',
                              maxLines: 1,
                              style: TextStyle(
                                fontFamily: Config.fontFamily,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 32.sp,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            iconsOnImage('assets/images/take out.png'),
                            SizedBox(width: 5),
                            iconsOnImage('assets/images/delivery.png'),
                            SizedBox(width: 5),
                            iconsOnImage('assets/images/book table.png')
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 0.002.sh),
                    Text(
                      '${restaurant.storeInformation}',
                      maxLines: 1,
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 30.sp,
                        fontWeight: FontWeight.w400,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 0.005.sh),
                    Row(
                      children: [
                        Icon(
                          MdiIcons.heart,
                          size: 14.0,
                          color: Colors.redAccent,
                        ),
                        SizedBox(width: 3),
                        AutoSizeText(
                          '${restaurant.numberOfFollowers}',
                          maxLines: 1,
                          style: TextStyle(
                            color: Colors.black45,
                          ),
                          minFontSize: 24.sp,
                          stepGranularity: 24.sp,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(width: 10),
                        Icon(
                          restaurant.ratings.average != null && restaurant.ratings.average > 0
                              ? restaurant.ratings.average < 5
                                  ? MdiIcons.starHalfFull
                                  : MdiIcons.star
                              : MdiIcons.starOutline,
                          size: 18.0,
                          color: Colors.amberAccent,
                        ),
                        SizedBox(width: 3),
                        AutoSizeText(
                          '${restaurant.ratings.average ?? 0}',
                          maxLines: 1,
                          style: TextStyle(
                            color: Colors.black45,
                          ),
                          minFontSize: 24.sp,
                          stepGranularity: 24.sp,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  iconsOnImage(String image) {
    return Container(width: 0.07.sw, child: Image.asset(image, color: Colors.black));
  }

  Future<void> showImages({BuildContext context, String full, String thumbnail}) async {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.black,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return CachedNetworkImage(
          imageUrl: full ?? '',
          imageBuilder: (context, imageProvider) => GestureDetector(
              onTap: () {},
              child: Column(
                children: [
                  Expanded(
                    child: Stack(
                      children: [
                        Positioned(
                          top: 50,
                          right: 0,
                          child: FlatButton(
                              onPressed: () => Navigator.pop(context),
                              child: Icon(
                                Icons.close,
                                color: Colors.white,
                                size: 26.0,
                              ),
                              height: 40.0,
                              shape: CircleBorder(),
                              color: Colors.black45),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: AspectRatio(
                            aspectRatio: 1 / 1,
                            child: Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
          errorWidget: (context, url, error) => Container(
            color: Colors.black12,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AutoSizeText('Image Not Found'),
                SizedBox(height: 8.0),
                Icon(
                  Icons.error_outline,
                  size: 30.0,
                  color: Colors.red,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _addToCartBtmSheet(Food food) {
    double baseAmount = double.parse(food.price);
    double totalAmount;

    int dynamicQty = 1;
    int max = 99;
    int min = 1;
    int stepValue = 1;

    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return SingleChildScrollView(
          child: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState2) {
              totalAmount = baseAmount * dynamicQty;
              return GestureDetector(
                onTap: () => FocusScope.of(context).unfocus(),
                child: Container(
                  padding: EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0),
                      )),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          CachedNetworkImage(
                            imageUrl: food.full ?? '',
                            imageBuilder: (context, imageProvider) => Container(
                              height: 0.3.sw,
                              width: 0.3.sw,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                            ),
                            placeholder: (context, url) => Container(
                              height: 0.3.sw,
                              width: 0.3.sw,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                image: DecorationImage(
                                  image: NetworkImage(food.thumbnail ?? ''),
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                            ),
                            errorWidget: (context, url, error) => SizedBox(),
                          ),
                          SizedBox(width: 0.02.sw),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${food.name}',
                                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32.sp),
                                ),
                                Text(
                                  'QAR ${food.price}',
                                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32.sp),
                                ),
                                food.description.length > 0
                                    ? Column(
                                        children: [
                                          SizedBox(height: 0.005.sh),
                                          Text('${food.description}',
                                              style: TextStyle(fontSize: 32.sp, color: Colors.black54)),
                                        ],
                                      )
                                    : SizedBox(),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 0.005.sh),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              RawMaterialButton(
                                constraints: BoxConstraints.tightFor(width: 0.08.sw, height: 0.08.sw),
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    side: BorderSide(
                                        color: dynamicQty > min ? Colors.black87 : Colors.black38, width: 1.5)),
                                onPressed: dynamicQty == min
                                    ? null
                                    : () {
                                        setState2(() {
                                          dynamicQty = dynamicQty == min ? min : dynamicQty -= stepValue;
                                        });
                                      },
                                fillColor: Colors.transparent,
                                child: Icon(
                                  MdiIcons.minus,
                                  color: dynamicQty > min ? Colors.black87 : Colors.black38,
                                  size: 50.sp,
                                ),
                              ),
                              Container(
                                width: 0.1.sw,
                                child: Text(
                                  '$dynamicQty',
                                  style: TextStyle(
                                    fontSize: 22 * 0.8,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              RawMaterialButton(
                                constraints: BoxConstraints.tightFor(width: 0.08.sw, height: 0.08.sw),
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    side: BorderSide(
                                        color: dynamicQty < max ? Colors.black87 : Colors.black38, width: 1.5)),
                                onPressed: dynamicQty == max
                                    ? null
                                    : () {
                                        setState2(() {
                                          dynamicQty = dynamicQty == max ? max : dynamicQty += stepValue;
                                        });
                                      },
                                fillColor: Colors.transparent,
                                child: Icon(
                                  MdiIcons.plus,
                                  color: dynamicQty < max ? Colors.black87 : Colors.black38,
                                  size: 50.sp,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 0.005.sh),
                          Text(
                            'TOTAL: $totalAmount',
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32.sp),
                          ),
                        ],
                      ),
                      SizedBox(height: 0.01.sh),
                      Text(
                        'Add a note (Optional)',
                        style: TextStyle(fontSize: 32.sp),
                      ),
                      SizedBox(height: 0.01.sh),
                      Padding(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom > 0
                                ? MediaQuery.of(context).viewInsets.bottom - 0.09.sh
                                : MediaQuery.of(context).viewInsets.bottom),
                        child: TextField(
                          minLines: 1,
                          maxLines: null,
                          keyboardType: TextInputType.text,
                          onSubmitted: (value) {
                            FocusScope.of(context).unfocus();
                          },
                          decoration: InputDecoration(
                            hintText: 'Do you have any request?',
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 0.02.sh),
                      Selector<PRestaurantFoodCart, bool>(
                        selector: (_, clear) => clear.items.length > 0 && clear.restHashid != clear.restHashidInCart,
                        builder: (context, isSameRestaurant, child) => Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            SizedBox(
                              height: 0.06.sh,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                                onPressed: () async {
                                  // bool canNav =
                                  //     await ExtendedNavigator.of(context)
                                  //         .root
                                  //         .canNavigate('/restaurants/cart');
                                  // if (!canNav) return;

                                  // if (isSameRestaurant) {
                                  //   showCartAlertDialog(
                                  //     BackButtonBehavior.none,
                                  //     cancel: () {},
                                  //     confirm: () {
                                  //       context
                                  //           .read<PRestaurantFoodCart>()
                                  //           .setItems(
                                  //               restHashid: widget.restId,
                                  //               food: food,
                                  //               qty: dynamicQty,
                                  //               total: food.price,
                                  //               isReset: true);

                                  //       Navigator.of(context).pop();
                                  //     },
                                  //     backgroundReturn: () {},
                                  //   );
                                  // } else {
                                  //   context
                                  //       .read<PRestaurantFoodCart>()
                                  //       .setItems(
                                  //         restHashid: widget.restId,
                                  //         food: food,
                                  //         qty: dynamicQty,
                                  //         total: food.price,
                                  //       );
                                  //   Navigator.of(context).pop();
                                  // }

                                  //popup a text toast;
                                },
                                elevation: 0.0,
                                color: Config.primaryColor,
                                textColor: Colors.white,
                                child: Text('ADD TO CART'),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }

  void showCartAlertDialog(BackButtonBehavior backButtonBehavior,
      {VoidCallback cancel, VoidCallback confirm, VoidCallback backgroundReturn}) {
    BotToast.showAnimationWidget(
        clickClose: true,
        allowClick: false,
        onlyOne: true,
        crossPage: true,
        backgroundColor: Colors.black54,
        backButtonBehavior: backButtonBehavior,
        toastBuilder: (cancelFunc) => AlertDialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              title: const Text('Clear Cart?'),
              content: Text(
                  'You have already selected different restaurant. If you continue your cart and selection will be removed.'),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    cancelFunc();
                    cancel?.call();
                  },
                  highlightColor: const Color(0x55FF8A80),
                  splashColor: const Color(0x99FF8A80),
                  child: Text(
                    'cancel',
                    style: TextStyle(color: Colors.redAccent, fontSize: 36.sp),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    cancelFunc();
                    confirm?.call();
                  },
                  child: Text(
                    'confirm',
                    style: TextStyle(fontSize: 36.sp),
                  ),
                ),
              ],
            ),
        animationDuration: Duration(milliseconds: 300));
  }
}
