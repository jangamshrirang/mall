import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/pages/Botique/BotiqueHome.dart';

import 'package:wblue_customer/pages/market/home/tab/home_tab.dart';
import 'package:wblue_customer/pages/market/home/tab/live_tab.dart';
import 'package:wblue_customer/pages/market/home/tab/premium_tab.dart';
import 'package:wblue_customer/providers/market/p_home.dart';
import 'package:wblue_customer/widgets/w_drawer.dart';

import '../../../env/config.dart';

class HomeMarketPage extends StatefulWidget {
  @override
  _HomeMarketPageState createState() => _HomeMarketPageState();
}

class _HomeMarketPageState extends State<HomeMarketPage>
    with TickerProviderStateMixin<HomeMarketPage> {
  int _index = 0;

  @override
  initState() {
    super.initState();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    print('WARNING !!! REBUILD HAPPENED');
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        actions: <Widget>[
          GestureDetector(
            onTap: () =>
                ExtendedNavigator.of(context).push('/market/user/wallet'),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.0),
              child: Icon(
                MdiIcons.walletOutline,
                color: _index == 1 ? Colors.blueGrey : Colors.white,
                size: 30,
              ),
            ),
          ),
        ],
        leading: IconButton(
          icon: Icon(MdiIcons.menu),
          color: _index == 1 ? Colors.blueGrey : Colors.white,
          onPressed: () {
            _scaffoldKey.currentState.openDrawer();
          },
        ),
        centerTitle: true,
        title: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _tabHeader('Home',
                  isCurrent: _index == 0 ? true : false,
                  onTap: () => _changePage(index: 0)),
              _tabHeader('Premium',
                  isCurrent: _index == 1 ? true : false,
                  onTap: () => _changePage(index: 1)),
              _tabHeader('Botique',
                  isCurrent: _index == 2 ? true : false,
                  onTap: () => _changePage(index: 2)),
              _tabHeader('Live',
                  isCurrent: _index == 3 ? true : false,
                  onTap: () => _changePage(index: 3)),
            ],
          ),
        ),
        backgroundColor: _index == 1 ? Colors.white : Config.primaryColor,//Color(0xff2A2A2A),
        elevation: 0.0,
        brightness: _index == 1 ? Brightness.light : Brightness.dark,
      ),
      backgroundColor: Config.bodyColor,
      drawer: WDrawerWidget('marketplace'),
      body: IndexedStack(
        index: _index,
        children: [
          HomeTab(),
          PremiumTab(), //home,premi,bot,live
          BotiqueHome(),
          LiveTab(),
        ],
      ),
    );
  }

  Widget _tabHeader(String label, {bool isCurrent, Function onTap}) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        color: Colors.transparent,
        padding: EdgeInsets.symmetric(horizontal: isCurrent ? 8.0 : 5),
        child: Text(
          label,
          style: TextStyle(
              color: isCurrent
                  ? _index == 1 ? Config.primaryColor : Colors.white
                  : _index == 1 ? Colors.blueGrey : Colors.white70,
              fontSize: isCurrent ? 20 : 20),
        ),
      ),
    );
  }

  _changePage({int index}) {
    _index = index;
    setState(() {});
  }
}
