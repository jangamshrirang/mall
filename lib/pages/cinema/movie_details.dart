import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../env/config.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_movie_header.dart';
import 'package:wblue_customer/widgets/w_movie_item.dart';

class MovieDetailsPage extends StatefulWidget {
  @override
  _MovieDetailsPageState createState() => _MovieDetailsPageState();
}

class _MovieDetailsPageState extends State<MovieDetailsPage> {
  String cinema = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Column(
          children: <Widget>[
            WMovieHeaderWidget(),
            _glance(),
            _cinema(),
            _about(),
            Divider(),
            _cinemaSelection(),
            Divider(),
            _related(),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        child: Wrap(
          children: <Widget>[
            WButtonWidget(
              title: 'Proceed to Select Seat',
              onPressed: cinema == ''
                  ? null
                  : () {
                      ExtendedNavigator.of(context).root.push('/cinemas/select-cinema-seat');
                    },
            ),
          ],
        ),
      ),
    );
  }

  Widget _glance() {
    TextStyle fonts = TextStyle(color: Colors.white54);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
      color: Config.primaryColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Text('16+', style: fonts),
          Text('2hrs 20m', style: fonts),
          Text('Action / Adventure / Fantacy', style: fonts),
          Text('2018', style: fonts),
        ],
      ),
    );
  }

  Widget _cinema() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
      color: Config.primaryColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            'Available on:',
            style: TextStyle(color: Colors.white54),
          ),
          Text(
            'iMax | C1 | C2 | C3',
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );
  }

  Widget _about() {
    String about = 'Fusce ante turpis, tempus vitae semper ac, pharetra eget ex. Donec a elementum lacus. Vivamus efficitur venenatis nibh et laoreet. Phasellus molestie vehicula pellentesque. Suspendisse potenti. Nunc condimentum neque eu mauris laoreet, ac dictum libero molestie. Integer sed mi eleifend, lacinia nunc vel, tincidunt dui. Morbi faucibus est eget luctus feugiat.';
    String summary = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tempor molestie neque, sed pharetra nunc finibus ut. Duis sagittis luctus euismod. Suspendisse porttitor scelerisque tincidunt. Praesent interdum quis risus in porttitor. Nullam porta consectetur commodo. Nulla nisl arcu, cursus sed tellus vitae, aliquet volutpat tellus. Nulla vulputate sapien sed euismod ullamcorper. Aliquam erat volutpat. Sed imperdiet euismod tellus a sodales. Aliquam erat volutpat. Maecenas viverra et nisl ut euismod. Aenean egestas enim ac nisi varius lobortis. Etiam ut aliquam erat, eget feugiat turpis. Duis vulputate nunc eu tincidunt euismod.';

    return Container(
      padding: EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                'About',
                style: TextStyle(fontSize: 18.0),
              ),
            ],
          ),
          SizedBox(height: 4.0),
          Text(about, textAlign: TextAlign.justify),
          SizedBox(height: 8.0),
          Row(
            children: <Widget>[
              Text(
                'Summary',
                style: TextStyle(fontSize: 18.0),
              ),
            ],
          ),
          SizedBox(height: 4.0),
          Text(summary, textAlign: TextAlign.justify),
        ],
      ),
    );
  }

  Widget _cinemaSelection() {
    Widget __cinemaCard(String label) {
      return Expanded(
        child: InkWell(
          onTap: () {
            setState(() {
              cinema = label;
            });
          },
          child: Card(
            clipBehavior: Clip.hardEdge,
            child: Container(
              color: cinema == label ? Colors.amber : Colors.transparent,
              padding: EdgeInsets.all(8.0),
              child: Center(
                child: Text(label),
              ),
            ),
          ),
        ),
      );
    }

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Select Cinema',
            style: TextStyle(fontSize: 16.0),
          ),
          SizedBox(height: 8.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              __cinemaCard('iMax'),
              __cinemaCard('C1'),
              __cinemaCard('C2'),
              __cinemaCard('C3'),
            ],
          ),
        ],
      ),
    );
  }

  Widget _related() {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'RELATED MOVIES',
            style: TextStyle(fontSize: 16.0),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 8.0),
            height: 200.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: List.generate(5, (index) {
                return WMovieItemWidget(isCompact: true);
              }),
            ),
          ),
        ],
      ),
    );
  }
}
