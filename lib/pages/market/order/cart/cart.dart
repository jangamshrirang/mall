import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/pages/market/home/Services/serviceCarosel.dart';

import 'package:wblue_customer/providers/market/p_bottom_nav.dart';
import 'package:wblue_customer/providers/market/p_cart.dart';
import 'package:wblue_customer/providers/market/p_view_product.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

import '../../../../env/config.dart';

class CartMarketPage extends StatefulWidget {
  final bool canBack;
  CartMarketPage({this.canBack: false});

  @override
  _CartMarketPageState createState() => _CartMarketPageState();
}

class _CartMarketPageState extends State<CartMarketPage> {
  void displayBottomSheet(BuildContext context, Map product) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (ctx) {
          return Consumer<PMarketCart>(
            builder: (context, cart, child) => Container(
                padding: EdgeInsets.all(8.0),
                color: Colors.white,
                height: MediaQuery.of(context).size.height * 0.8,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                height: 150,
                                width: 150,
                                child: WImageWidget(
                                    placeholder: AssetImage(product['images'][cart.selectedImgCartInfo]['big']))),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  AutoSizeText('QAR ${product['newPrice']}',
                                      style: TextStyle(
                                          color: Colors.red.withOpacity(1),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20.0)),
                                  SizedBox(height: 2.0),
                                  product['oldPrice'] != null
                                      ? Row(
                                          children: [
                                            AutoSizeText('QAR ${product['oldPrice']}',
                                                style: TextStyle(
                                                    color: Colors.grey.withOpacity(1),
                                                    decoration: TextDecoration.lineThrough,
                                                    fontSize: 16.0)),
                                            SizedBox(width: 5.0),
                                            AutoSizeText('-53%', style: TextStyle(fontSize: 18.0)),
                                          ],
                                        )
                                      : SizedBox(),
                                  SizedBox(height: 10.0),
                                  AutoSizeText('Black, 128GB')
                                ],
                              ),
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.grey.withOpacity(0.6),
                        ),
                        SizedBox(height: 10),
                        AutoSizeText(
                          'Color Family',
                          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 10),
                        Wrap(
                          direction: Axis.horizontal,
                          spacing: 8.0,
                          runSpacing: 8.0,
                          children: List.generate(
                            product['images'].length,
                            (index) => GestureDetector(
                              onTap: () => context.read<PMarketCart>().setCartInfoImg(index),
                              child: Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: cart.selectedImgCartInfo == index
                                              ? Colors.blueAccent
                                              : Colors.grey.withOpacity(0.3))),
                                  height: 75,
                                  width: 75,
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: WImageWidget(placeholder: AssetImage(product['images'][index]['medium'])),
                                  )),
                            ),
                          ),
                        ),
                        SizedBox(height: 30),
//                      AutoSizeText(
//                        'Storage Capacity',
//                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
//                      ),
//                      Wrap(
//                        children: [
//                          Padding(
//                            padding: const EdgeInsets.symmetric(horizontal: 2.0),
//                            child: FilterChip(
//                              label: AutoSizeText('128GB', style: TextStyle(color: Config.primaryColor)),
//                              backgroundColor: Colors.transparent,
//                              shape: StadiumBorder(side: BorderSide(color: Config.primaryColor)),
//                              onSelected: (bool value) {},
//                            ),
//                          ),
//                        ],
//                      ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: WRoundedButton(
                            btnColor: Config.primaryColor,
                            labelColor: Colors.white,
                            onCustomButtonPressed: () {
                              context.read<PViewProduct>().setProduct(product);
                              ExtendedNavigator.of(context).push('/view-product');
                            },
                            child: AutoSizeText('Product detail'),
                          ),
                        ),
                        SizedBox(width: 10.0),
                        Expanded(
                          child: WRoundedButton(
                            btnColor: Config.primaryColor,
                            labelColor: Colors.white,
                            onCustomButtonPressed: () {},
                            child: AutoSizeText('Confirm'),
                          ),
                        )
                      ],
                    )
                  ],
                )),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Consumer<PMarketCart>(
      builder: (context, cart, child) => Scaffold(
          backgroundColor: Config.bodyColor,
          body: Column(
            children: [
              Stack(
                children: [
                  Container(
                    padding: EdgeInsets.all(16.0),
                    color: Colors.white,
                    child: SafeArea(
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  widget.canBack
                                      ? IconButton(
                                          onPressed: () => ExtendedNavigator.of(context).pop(),
                                          icon: Icon(MdiIcons.chevronLeft),
                                        )
                                      : SizedBox(),
                                  SizedBox(width: 10.0),
                                  AutoSizeText('Cart ${cart.items.length > 0 ? '(${cart.items.length})' : ''}',
                                      style: TextStyle(
                                        fontSize: 20.0,
                                      )),
                                ],
                              ),
                              AutoSizeText('Delete',
                                  style: TextStyle(
                                    fontSize: 20.0,
                                  )),
                            ],
                          ),
                          SizedBox(height: 10),
                          AutoSizeText(
                            'Cecilia Chapman 711-2880 Nulla St. Mankato Mississippi 96522  (257) 563-7401',
                            style: TextStyle(
                              fontSize: 18.0,
                            ),
                            minFontSize: 18.0,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.zero,
                  physics: ClampingScrollPhysics(),
                  children: [
                    SizedBox(height: 5.0),
                    cart.items.length > 0
                        ? Column(
                            children: List.generate(
                                cart.items.length,
                                (index) => Column(
                                      children: [
                                        _cartItems(context, cart.items[index]),
                                        SizedBox(height: 5.0),
                                      ],
                                    )),
                          )
                        : Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Column(
                              children: [
                                AutoSizeText('There are no items in this cart', style: TextStyle(fontSize: 18.0)),
                                WRoundedButton(
                                    onCustomButtonPressed: () => context.read<PBottomNavigation>().changeTab(0),
                                    borderColor: Colors.grey.withOpacity(0.3),
                                    btnColor: Colors.white,
                                    labelColor: Colors.black,
                                    child: AutoSizeText('CONTINUE SHOPPING')),
                              ],
                            ),
                          )
                  ],
                ),
              ),
              Container(
                color: Colors.white,
                height: size.height * 0.055,
                child: Exercise(
                  title: "Do you want to make private transcation!",
                ),
              ),
              Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          SizedBox(width: 10),
                          AutoSizeText('Total: ${cart.totalAllItems}',
                              style:
                                  TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Config.primaryColor)),
                        ],
                      ),
                      WRoundedButton(
                        onCustomButtonPressed: () => cart.items.length > 0
                            ? ExtendedNavigator.of(context).push('/market/shipping-billing')
                            : null,
                        child: AutoSizeText('Check Out'),
                        btnColor: Config.primaryColor,
                        labelColor: Colors.white,
                      )
                    ],
                  ),
                ),
              )
            ],
          )),
    );
  }

  Widget _cartItems(BuildContext context, Map product) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(8.0),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                AutoSizeText(
                  '${product['storeName']}',
                  style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                ),
                AutoSizeText('Get Voucher', style: TextStyle(color: Config.primaryColor, fontSize: 18.0))
              ],
            ),
          ),
          Divider(height: 2),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {
                        context.read<PViewProduct>().setProduct(product);
                        ExtendedNavigator.of(context).push('/view-product');
                      },
                      child: Container(
                        height: 100,
                        width: 100,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                            child: WImageWidget(
                              placeholder: AssetImage(product['images'][0]['small']),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AutoSizeText('${product['name']}', style: TextStyle(fontSize: 18.0)),
                        SizedBox(height: 4.0),
                        GestureDetector(
                          onTap: () {
                            context.read<PMarketCart>().setCartInfoImg(0);
                            displayBottomSheet(context, product);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                color: Colors.grey.withOpacity(0.2),
                                border: Border.all(color: Colors.grey, width: 0.5)),
                            child: Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Row(
                                children: [
                                  Text(
                                    'No Brand, Color Family: Blue',
                                    style: TextStyle(
                                      color: Colors.black.withOpacity(0.5),
                                    ),
                                  ),
                                  SizedBox(width: 3.0),
                                  Icon(
                                    MdiIcons.chevronDown,
                                    color: Colors.black.withOpacity(0.5),
                                    size: 16,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 2.0),
                        AutoSizeText(
                          'Only ${product['availibilityCount']} Items(s) in stock',
                          style: TextStyle(color: Colors.red.withOpacity(0.8)),
                        ),
                        SizedBox(height: 5.0),
                        AutoSizeText('QAR ${product['newPrice']}',
                            style: TextStyle(
                                color: Colors.red.withOpacity(1), fontWeight: FontWeight.bold, fontSize: 16.0)),
                        SizedBox(height: 2.0),
                        product['oldPrice'] != null
                            ? Row(
                                children: [
                                  AutoSizeText('QAR ${product['oldPrice']}',
                                      style: TextStyle(
                                          color: Colors.grey.withOpacity(1),
                                          decoration: TextDecoration.lineThrough,
                                          fontSize: 16.0)),
                                  SizedBox(width: 5.0),
                                  AutoSizeText('-53%', style: TextStyle(fontSize: 18.0)),
                                ],
                              )
                            : SizedBox(),
                      ],
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: 50,
                      child: FlatButton(
                        onPressed: () => context.read<PMarketCart>().setQuantity(false, product),
                        child: Icon(MdiIcons.minus),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: AutoSizeText('${product['cartCount']}',
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(
                      width: 50,
                      child: FlatButton(
                        onPressed: () => context.read<PMarketCart>().setQuantity(true, product),
                        child: Icon(MdiIcons.plus),
                      ),
                    ),
//                    AutoSizeText('${product['cartCount']}', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20.0)),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
