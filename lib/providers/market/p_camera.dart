import 'dart:collection';

import 'package:flutter/foundation.dart';

class PCamera with ChangeNotifier {
  String _scannerType = 'camera';
  List _recognitions = new List();
  String _imagePath;
  bool _busy = true;

  String get scannerType => _scannerType;
  String get imagePath => _imagePath;
  bool get busy => _busy;
  UnmodifiableListView get recognitions => UnmodifiableListView(_recognitions);

  void setScanner(String type) {
    _scannerType = type;
    notifyListeners();
  }

  void setRecognitions(List recogniations) {
    _recognitions = recogniations;
    notifyListeners();
  }

  void setImagePath(String imagePath) {
    _imagePath = imagePath;
    notifyListeners();
  }

  void setBusyStatus(bool status) {
    _busy = status;
    notifyListeners();
  }
}
