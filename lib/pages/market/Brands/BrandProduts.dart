import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/Brands/ProductExpantion.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

import '../../../env/config.dart';

class BrandProductsPage extends StatefulWidget {
  @override
  _BrandProductsPageState createState() => _BrandProductsPageState();
}

class _BrandProductsPageState extends State<BrandProductsPage> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white, //Config.bodyColor,
        appBar: appBarWithOutIcon(context, "Products"),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Stack(
              children: [
                Column(
                  children: [
                    _searchBar(),
                    Wrap(
                      spacing: 0,
                      children: [
                        LayoutThree(),
                        LayoutThree(),
                        LayoutThree(),
                        LayoutThree(),
                        LayoutThree(),
                        LayoutThree(),
                        LayoutThree(),
                        LayoutThree(),
                        LayoutThree(),
                        LayoutThree(),
                        LayoutThree(),
                        LayoutThree(),
                      ],
                    ),
                  ],
                ),
              GestureDetector(
                onTap: (){
                   sortData();
                },
                  child: Container(
                    height: size.height * 0.05,
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        border: Border.all(width: 0.5, color: Colors.black26)),
                    margin: EdgeInsets.only(left: size.width * 0.85, top: 2),
                    child: Image.network(
                      "https://i.pinimg.com/originals/03/8e/85/038e8523b874a3911abc668e36ba6571.png",
                      height: size.height * 0.03,
                      color: Colors.black54,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  _searchBar() {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: 0, right: size.width * 0.10, top: 2),
      height: size.height * 0.05,
      width: size.width * 0.82,
      child: TextField(
        cursorColor: Colors.black,
        decoration: InputDecoration(
            filled: true,
            labelStyle:
                TextStyle(color: Colors.black38, fontWeight: FontWeight.normal),
            fillColor: Colors.white,
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black26, width: 1),
              borderRadius: BorderRadius.circular(00.0),
            ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(00.0)),
                borderSide: BorderSide(color: Colors.black26, width: 1)),
            labelText: "Search..."),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            // _notesForDisplay = _notes.where((note) {
            //   var noteTitle = note.title.toLowerCase();
            //   return noteTitle.contains(text);
            // }
            // ).toList();
          });
        },
      ),
    );
  }

 void sortData() {
   var size = MediaQuery.of(context).size;
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            return Container(
              height: size.height*0.6,
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  )),
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                children: ListTile.divideTiles(
                  context: context,
                  tiles: [
                    SizedBox(
                      height: 10,
                    ),
                    listtile(context, "Featured", () {
                      print("Featured");
                    }),
                    listtile(context, "Newest", () {
                      print("Newest");
                    }),
                    listtile(context, "Oldest", () {
                      print("Oldest");
                    }),
                    listtile(context, "Best Sellers", () {
                      print("Price : Best Sellers");
                    }),
                    listtile(context, "Price : Low to High", () {
                      print("Price : Low to High");
                    }),
                    listtile(context, "Price :  High to Low", () {
                      print("Price :  High to Low");
                    }),
                    listtile(context, "A to Z", () {
                      print("A to Z");
                    }),
                  ],
                ).toList(),
              ),
            );
          },
        );
      },
    );
  }

  listtile(BuildContext context, String title, Function ontap) {
    return ListTile(
      // leading: Image.network(image, height: 30, color: Colors.amber),
      title: Text(
        title,
      ),
      onTap: ontap,
    );
  }

  settingsScreenNavigation(BuildContext context, screen) {
    return Navigator.push(
        context, MaterialPageRoute(builder: (context) => screen));
  }
}

class LayoutThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductExpantion(
                      pTitle: "Beats headpones",
                      price: 300,
                    )));
      },
      child: Container(
          width: size.width * 0.45,
          height: size.height * 0.3,
          margin: EdgeInsets.only(bottom: 10, left: 10, top: 5, right: 5),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 1,
                blurRadius: 3,
                offset: Offset(1, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.all(Radius.circular(0)),
            image: DecorationImage(
              image: NetworkImage(
                'https://i5.walmartimages.com/asr/6e1b1508-d1d8-46e5-93a3-8a35612c61b2_1.e0e3988fd0851d2e5025d935a8eee6f7.jpeg',
              ),
              fit: BoxFit.fill,
            ),
          ),
          child: Container(
              //text container
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8)),
              ),
              margin: EdgeInsets.only(
                top: size.height * 0.23,
              ),
              padding: EdgeInsets.only(
                  left: size.width * 0.02,
                  right: size.width * 0.01,
                  top: size.height * 0.01),
              height: size.height * 0.06,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Beats headpones",
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.normal,
                        fontSize: size.height * 0.023),
                  ),
                  Text(
                    "QAR 300",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.normal,
                        color: Colors.black38,
                        fontSize: size.height * 0.018),
                  ),
                ],
              ))),
    );
  }
}
