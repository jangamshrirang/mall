import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_map_polyline/google_map_polyline.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:ui' as ui;

import 'package:wblue_customer/pages/market/order/track-order.dart';

class OrderHistory extends StatefulWidget {
  @override
  _OrderHistoryState createState() => _OrderHistoryState();
}

final searcController = TextEditingController();

class _OrderHistoryState extends State<OrderHistory> {
  List<LatLng> routeCoords;
  GoogleMapPolyline googleMapPolyline =
      new GoogleMapPolyline(apiKey: "AIzaSyCpZybhi4ZzUBXImzkKtaggzrOSsfcH8p4");
  BitmapDescriptor pinLocationIcon;
  //converting image string to bytes
  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

//getting the location cordinates
  location() async {
    routeCoords = await googleMapPolyline.getCoordinatesWithLocation(
        origin: LatLng(25.254246, 51.536136),
        destination: LatLng(25.246684, 51.533330),
        mode: RouteMode.driving);
    setState(() {
      this.routeCoords = routeCoords;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    location();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Column(
              children: [
                _searchBar(),
                Wrap(
                  spacing: 0,
                  children: [productCard(), productCard()],
                ),
              ],
            ),
            GestureDetector(
              onTap: () {
                sortData();
              },
              child: Container(
                height: size.height * 0.05,
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    border: Border.all(width: 0.5, color: Colors.black26)),
                margin: EdgeInsets.only(left: size.width * 0.86, top: 5),
                child: Image.network(
                  "https://i.pinimg.com/originals/03/8e/85/038e8523b874a3911abc668e36ba6571.png",
                  height: size.height * 0.03,
                  color: Colors.black54,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  naviatingwithimage() async {
    final Uint8List markerIcon =
        await getBytesFromAsset('assets/images/van.png', 80);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OrderTrackPage(
                  cordinates: routeCoords,
                  pinLocationIcon: BitmapDescriptor.fromBytes(markerIcon),
                )));
  }

  productCard() {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        naviatingwithimage();
      },
      child: Container(
          width: size.width * 0.9,
          height: size.height * 0.08,
          margin: EdgeInsets.only(bottom: 10, left: 20, top: 5, right: 5),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 1,
                blurRadius: 3,
                offset: Offset(1, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.all(Radius.circular(0)),
          ),
          child: Container(
              //text container
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(0), topRight: Radius.circular(0)),
              ),
              margin: EdgeInsets.only(
                top: size.height * 0,
              ),
              padding: EdgeInsets.only(
                  left: size.width * 0.02,
                  right: size.width * 0.01,
                  top: size.height * 0.01),
              height: size.height * 0.06,
              child: Row(
                children: [
                  Image.network(
                    'https://i5.walmartimages.com/asr/6e1b1508-d1d8-46e5-93a3-8a35612c61b2_1.e0e3988fd0851d2e5025d935a8eee6f7.jpeg',
                    height: 50,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Beats headpones",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.normal,
                            fontSize: size.height * 0.023),
                      ),
                      Text(
                        "QAR 300",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.normal,
                            color: Colors.black38,
                            fontSize: size.height * 0.018),
                      ),
                    ],
                  ),
                  SizedBox(width: size.width * 0.1),
                  Text(
                    "Ordered on 07/08/2020",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.normal,
                        color: Colors.black38,
                        fontSize: size.height * 0.018),
                  ),
                ],
              ))),
    );
  }

  _searchBar() {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: 0, right: 20, top: 5),
      height: size.height * 0.05,
      width: size.width * 0.80,
      child: TextField(
        controller: searcController,
        cursorColor: Colors.black,
        decoration: InputDecoration(
            filled: true,
            labelStyle:
                TextStyle(color: Colors.black38, fontWeight: FontWeight.normal),
            fillColor: Colors.white,
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black, width: 1),
              borderRadius: BorderRadius.circular(0.0),
            ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(0.0)),
                borderSide: BorderSide(color: Colors.black26, width: 1)),
            labelText: "Search..."),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            // _notesForDisplay = _notes.where((note) {
            //   var noteTitle = note.title.toLowerCase();
            //   return noteTitle.contains(text);
            // }
            // ).toList();
          });
        },
      ),
    );
  }

  void sortData() {
    var size = MediaQuery.of(context).size;
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            return Container(
              height: size.height * 0.6,
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  )),
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                children: ListTile.divideTiles(
                  context: context,
                  tiles: [
                    SizedBox(
                      height: 10,
                    ),
                    listtile(context, "Featured", () {
                      print("Featured");
                    }),
                    listtile(context, "Newest", () {
                      print("Newest");
                    }),
                    listtile(context, "Oldest", () {
                      print("Oldest");
                    }),
                    listtile(context, "Best Sellers", () {
                      print("Price : Best Sellers");
                    }),
                    listtile(context, "Price : Low to High", () {
                      print("Price : Low to High");
                    }),
                    listtile(context, "Price :  High to Low", () {
                      print("Price :  High to Low");
                    }),
                    listtile(context, "A to Z", () {
                      print("A to Z");
                    }),
                  ],
                ).toList(),
              ),
            );
          },
        );
      },
    );
  }

  listtile(BuildContext context, String title, Function ontap) {
    return ListTile(
      // leading: Image.network(image, height: 30, color: Colors.amber),
      title: Text(
        title,
      ),
      onTap: ontap,
    );
  }
}
