import 'package:flutter/material.dart';

class WCardTitleButton extends StatelessWidget {
  final Widget titleWidget;
  final Widget linkWidget;
  final Widget bodyWidget;
  final Color backGroundColor;
  final Function onTap;

  WCardTitleButton(
      {this.titleWidget,
      this.linkWidget,
      this.bodyWidget,
      this.backGroundColor: Colors.white,
      this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: backGroundColor,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [titleWidget, linkWidget],
              ),
              SizedBox(height: 8),
              bodyWidget
            ],
          ),
        ),
      ),
    );
  }
}
