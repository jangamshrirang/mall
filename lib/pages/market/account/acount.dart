import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/account/paymentScree.dart';
import 'package:wblue_customer/pages/market/account/Wallet/wallet.dart';

import '../../../env/config.dart';
import 'PayTabSreens/PayScreen.dart';
import 'Reviwes/ReviewTabScreen.dart';
import 'Settings/SettingsScreen.dart';
import 'Wallet/VochersScreen.dart';

class AccountMarketPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Config.bodyColor,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: _title(),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            color: Colors.white,
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SettingsScreen()));
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _image(context),
            _myOrders(context),
            SizedBox(height: 5),
            _mallWallet(context),
            SizedBox(height: 5),
            _myService(context)
          ],
        ),
      ),
    );
  }

  Widget _title() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        GestureDetector(
//          onTap: () => Goto.push('/main/profile'),
          child: Container(
            color: Colors.transparent,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(35),
              child: Image.asset(
                ('assets/images/users/1-boy.png'),
                width: 50,
              ),
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Namra Ebradu',
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
            Text(
              '5094 5874',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.w600),
            ),
          ],
        )
      ],
    );
  }

  Widget _image(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: AppBar().preferredSize.height +
          MediaQuery.of(context).padding.top +
          70,
      alignment: Alignment.bottomCenter,
      decoration: BoxDecoration(
          color: Colors.red,
          image: DecorationImage(
              image: AssetImage('assets/images/account/bg.jpeg'),
              fit: BoxFit.cover)),
      child: Row(
        children: [
          _mineCard(context, value: '0', label: 'My Wishlist'),
          _mineCard(context, value: '0', label: 'Followed Stores'),
          _mineCard(context, value: '0', label: 'Recently Viewed'),
        ],
      ),
    );
  }

  Widget _mineCard(BuildContext context, {String value, String label}) {
    return Container(
      width: MediaQuery.of(context).size.width * .33,
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(value, style: TextStyle(color: Colors.white, fontSize: 16)),
          Text(label, style: TextStyle(color: Colors.white, fontSize: 14)),
        ],
      ),
    );
  }

  Widget _myOrders(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('My Orders',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600)),
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _orderColumn(
                  icon: Icons.credit_card,
                  label: 'To Pay',
                  onTap: () {
                    navigationScreen(context, 1);
                  }),
              _orderColumn(
                  icon: Icons.directions_boat,
                  label: 'To Ship',
                  onTap: () {
                    navigationScreen(context, 2);
                  }),
              _orderColumn(
                  icon: Icons.local_shipping,
                  label: 'To Received',
                  onTap: () {
                    navigationScreen(context, 3);
                  }),
              _orderColumn(
                  icon: Icons.insert_comment,
                  label: 'To Review',
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RviewTabScreen()));
                  }),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _orderRow(
                  icon: Icons.assignment_return,
                  label: 'Returns',
                  onTap: () {}),
              _orderRow(
                  icon: Icons.cancel, label: 'Cancellations', onTap: () {}),
            ],
          ),
        ],
      ),
    );
  }

  Widget _orderColumn({IconData icon, String label, Function onTap}) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        padding: EdgeInsets.all(12),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(icon, color: Colors.black54),
            SizedBox(height: 5),
            Container(
              constraints: BoxConstraints(maxWidth: 70),
              child: Text(
                label,
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black, fontSize: 12),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _orderRow({IconData icon, String label, Function onTap}) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        padding: EdgeInsets.all(12),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(icon, color: Colors.black54),
            SizedBox(width: 5),
            Text(
              label,
              style: TextStyle(color: Colors.black, fontSize: 12),
            )
          ],
        ),
      ),
    );
  }

  Widget _mallWallet(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('WMall Wallet',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _walletCard(context, label: 'QAR', value: '100.00', onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MarketWalletPage()));
              }),
              Container(
                width: 1,
                height: 50,
                color: Colors.black26,
              ),
              _walletCard(context, label: 'Voucher', value: '0', onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => VocherScreen()));
              }),
            ],
          ),
        ],
      ),
    );
  }

  Widget _walletCard(BuildContext context,
      {String label, String value, Function onTap}) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        width: MediaQuery.of(context).size.width * .33,
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(label, style: TextStyle(color: Colors.black, fontSize: 14)),
            SizedBox(height: 10),
            Text(value,
                style: TextStyle(
                    color: Config.secondaryColor,
                    fontSize: 26,
                    fontWeight: FontWeight.w600)),
          ],
        ),
      ),
    );
  }

  Widget _myService(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('My Orders',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600)),
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _orderColumn(
                  icon: Icons.comment,
                  label: 'My Review',
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RviewTabScreen()));
                  }),
              _orderColumn(
                  icon: Icons.payment,
                  label: 'Payment Options',
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PaymentScreen()));
                  }),
              _orderColumn(icon: Icons.help, label: 'Help', onTap: () {}),
              _orderColumn(
                  icon: Icons.headset_mic,
                  label: 'Chat with Customer Care',
                  onTap: () {}),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _orderColumn(
                  icon: Icons.monetization_on, label: 'To Up', onTap: () {}),
              _orderColumn(
                  icon: Icons.music_video, label: 'WTalent', onTap: () {}),
            ],
          ),
        ],
      ),
    );
  }

  navigationScreen(BuildContext context, int initialvalue) {
    return Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PayScreen(
                  intialIndex: initialvalue,
                )));
  }
}
