import 'dart:collection';

import 'package:dash_chat/dash_chat.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/providers/socket/client.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:provider/provider.dart';

class PRestaurantMessage with ChangeNotifier {
  List _messages = List();
  String _hashid;
  String _name;
  String _mobile;
  ChatUser _user = ChatUser();
  ChatUser get user => _user;
  String get hashid => _hashid;
  String get name => _name;
  String get mobile => _mobile;

  UnmodifiableListView get messages => UnmodifiableListView(_messages);

  void onSend({String message, BuildContext context}) {
    Map<String, dynamic> _msg = {
      'sender_hashid': _hashid,
      'receiver_hashid': null,
      'body': message,
    };

    context.read<PSocketClient>().emitter(eventName: WEvents.supportMessage, data: _msg);

    notifyListeners();
  }

  void setMessage(msg) {
    if (msg != null) _messages.add(msg);
    notifyListeners();
  }

  void setUser() {
    Map<dynamic, dynamic> userHive = Helper.currentAuthUser.get('user');
    _hashid = userHive['hashid'];
    _name = userHive['name'];
    _mobile = userHive['mobile'];

    ChatUser user =
        ChatUser(name: _name, uid: _hashid, avatar: Config.api, customProperties: {'mobile': mobile, 'hashid': hashid});

    if (_hashid == null) return;

    _user = user;

    notifyListeners();
  }
}
