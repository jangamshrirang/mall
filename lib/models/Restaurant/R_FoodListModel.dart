class RFoodListModel {
    RFoodListModel({
        this.hashId,
        this.category,
        this.full,
        this.thumbnail,
        this.foods,
    });

    String hashId;
    String category;
    dynamic full;
    dynamic thumbnail;
    List<Food> foods;

    factory RFoodListModel.fromJson(Map<String, dynamic> json) => RFoodListModel(
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        category: json["category"] == null ? null : json["category"],
        full: json["full"],
        thumbnail: json["thumbnail"],
        foods: json["foods"] == null ? null : List<Food>.from(json["foods"].map((x) => Food.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "hash_id": hashId == null ? null : hashId,
        "category": category == null ? null : category,
        "full": full,
        "thumbnail": thumbnail,
        "foods": foods == null ? null : List<dynamic>.from(foods.map((x) => x.toJson())),
    };
}

class Food {
    Food({
        this.hashId,
        this.name,
        this.price,
        this.full,
        this.thumbnail,
    });

    String hashId;
    String name;
    int price;
    String full;
    String thumbnail;

    factory Food.fromJson(Map<String, dynamic> json) => Food(
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        name: json["Name"] == null ? null : json["Name"],
        price: json["Price"] == null ? null : json["Price"],
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "hash_id": hashId == null ? null : hashId,
        "Name": name == null ? null : name,
        "Price": price == null ? null : price,
        "full": full == null ? null : full,
        "thumbnail": thumbnail == null ? null : thumbnail,
    };
}
