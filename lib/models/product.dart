class ProductListModel {
  ProductListModel({
    this.hashId,
    this.name,
    this.shortDescription,
    this.onSale,
    this.salePrice,
    this.stockStatus,
    this.initialQuantity,
    this.rating,
    this.reviews,
    this.timestamp,
    this.productPrice,
    this.imageFull,
    this.imageThumbnail,
  });

  String hashId;
  String name;
  String shortDescription;
  int onSale;
  int salePrice;
  String stockStatus;
  int initialQuantity;
  int rating;
  int reviews;
  String timestamp;
  int productPrice;
  String imageFull;
  String imageThumbnail;

  factory ProductListModel.fromJson(Map<String, dynamic> json) =>
      ProductListModel(
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        name: json["name"] == null ? null : json["name"],
        shortDescription: json["short_description"] == null
            ? null
            : json["short_description"],
        onSale: json["on_sale"] == null ? null : json["on_sale"],
        salePrice: json["sale_price"] == null ? null : json["sale_price"],
        stockStatus: json["stock_status"] == null ? null : json["stock_status"],
        initialQuantity:
            json["initial_quantity"] == null ? null : json["initial_quantity"],
        rating: json["rating"] == null ? null : json["rating"],
        reviews: json["Reviews"] == null ? null : json["Reviews"],
        timestamp: json["timestamp"] == null ? null : json["timestamp"],
        productPrice:
            json["product_price"] == null ? null : json["product_price"],
        imageFull: json["image_full"] == null
            ? null
            : '${json["image_full"]}?image=${json["timestamp"]}',
        imageThumbnail: json["image_thumbnail"] == null
            ? null
            : '${json["image_thumbnail"]}?image=${json["timestamp"]}',
      );
}

class SingleProductModel {
  SingleProductModel({
    this.hashId,
    this.storeId,
    this.productType,
    this.category,
    this.tags,
    this.name,
    this.price,
    this.shortDescription,
    this.quickDetails,
    this.brandId,
    this.daysOfReturn,
    this.yearsOfWaranty,
    this.onSale,
    this.salePrice,
    this.saleDateFrom,
    this.saleDateTo,
    this.stockStatus,
    this.sku,
    this.dimensions,
    this.length,
    this.width,
    this.height,
    this.weight,
    this.quantity,
    this.processingTime,
    this.variations,
    this.totalReviews,
    this.branchId,
    this.uuid,
    this.deliveryOption,
    this.timestamp,
    this.images,
  });

  String hashId;
  String storeId;
  String productType;
  List<String> category;
  List<String> tags;
  String name;
  int price;
  String shortDescription;
  List<QuickDetail> quickDetails;
  int brandId;
  int daysOfReturn;
  dynamic yearsOfWaranty;
  int onSale;
  int salePrice;
  dynamic saleDateFrom;
  dynamic saleDateTo;
  String stockStatus;
  dynamic sku;
  Dimensions dimensions;
  int length;
  int width;
  int height;
  int weight;
  int quantity;
  int processingTime;
  List<Variation> variations;
  int totalReviews;
  List<int> branchId;
  dynamic uuid;
  int deliveryOption;
  String timestamp;
  List<SingleProductImageModel> images;

  factory SingleProductModel.fromJson(Map<String, dynamic> json) =>
      SingleProductModel(
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        storeId: json["store_id"] == null ? null : json["store_id"],
        productType: json["product_type"] == null ? null : json["product_type"],
        category: json["category"] == null
            ? null
            : List<String>.from(json["category"].map((x) => x)),
        tags: json["tags"] == null
            ? null
            : List<String>.from(json["tags"].map((x) => x)),
        name: json["name"] == null ? null : json["name"],
        price: json["price"] == null ? null : json["price"],
        shortDescription: json["short_description"] == null
            ? null
            : json["short_description"],
        quickDetails: json["quick_details"] == null
            ? null
            : List<QuickDetail>.from(
                json["quick_details"].map((x) => QuickDetail.fromJson(x))),
        brandId: json["brand_id"] == null ? null : json["brand_id"],
        daysOfReturn:
            json["days_of_return"] == null ? null : json["days_of_return"],
        yearsOfWaranty: json["years_of_waranty"],
        onSale: json["on_sale"] == null ? null : json["on_sale"],
        salePrice: json["sale_price"] == null ? null : json["sale_price"],
        saleDateFrom: json["sale_date_from"],
        saleDateTo: json["sale_date_to"],
        stockStatus: json["stock_status"] == null ? null : json["stock_status"],
        sku: json["sku"],
        dimensions: json["dimensions"] == null
            ? null
            : Dimensions.fromJson(json["dimensions"]),
        length: json["length"] == null ? null : json["length"],
        width: json["width"] == null ? null : json["width"],
        height: json["height"] == null ? null : json["height"],
        weight: json["weight"] == null ? null : json["weight"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        processingTime:
            json["processing_time"] == null ? null : json["processing_time"],
        variations: json["variations"] == null
            ? null
            : List<Variation>.from(
                json["variations"].map((x) => Variation.fromJson(x))),
        totalReviews:
            json["total_reviews"] == null ? null : json["total_reviews"],
        branchId: json["branch_id"] == null
            ? null
            : List<int>.from(json["branch_id"].map((x) => x)),
        uuid: json["uuid"],
        deliveryOption:
            json["delivery_option"] == null ? null : json["delivery_option"],
        timestamp: json["timestamp"] == null ? null : json["timestamp"],
        images: json["images"] == null
            ? null
            : List<SingleProductImageModel>.from(
                json["images"].map((x) => SingleProductImageModel.fromJson(x))),
      );
}

class Dimensions {
  Dimensions({
    this.l,
    this.w,
    this.h,
  });

  String l;
  String w;
  String h;

  factory Dimensions.fromJson(Map<String, dynamic> json) => Dimensions(
        l: json["L"] == null ? null : json["L"],
        w: json["W"] == null ? null : json["W"],
        h: json["H"] == null ? null : json["H"],
      );
}

class SingleProductImageModel {
  SingleProductImageModel({
    this.full,
    this.thumbnail,
  });

  String full;
  String thumbnail;

  factory SingleProductImageModel.fromJson(Map<String, dynamic> json) =>
      SingleProductImageModel(
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
      );
}

class QuickDetail {
  QuickDetail({
    this.label,
    this.value,
  });

  String label;
  String value;

  factory QuickDetail.fromJson(Map<String, dynamic> json) => QuickDetail(
        label: json["label"] == null ? null : json["label"],
        value: json["value"] == null ? null : json["value"],
      );
}

class Variation {
  Variation({this.id, this.name, this.values});

  int id;
  String name;
  List<String> values;

  factory Variation.fromJson(Map<String, dynamic> json) => Variation(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        values: json["values"] == null
            ? null
            : List<String>.from(json["values"].map((x) => x)),
      );
}
