import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class HideNavbar {
  final ScrollController controller = ScrollController();
  ValueNotifier<bool> visible = ValueNotifier<bool>(true);

  HideNavbar() {
    visible.value = false;
    controller.addListener(
      () {
        if (controller.position.userScrollDirection == ScrollDirection.reverse) {
          if (controller.position.pixels > 40) {
            visible.value = true;
          }
        }

        if (controller.position.userScrollDirection == ScrollDirection.forward) {
          if (controller.position.pixels < 50) {
            visible.value = false;
          }
        }
      },
    );
  }
}
