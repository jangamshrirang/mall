import 'package:flutter/material.dart';

class WLiveDarkerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _darker(context, isTop: true),
        Expanded(
          child: SizedBox(),
        ),
        _darker(context)
      ],
    );
  }

  Widget _darker(BuildContext context, {bool isTop: false}) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * .3,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
            Colors.black.withOpacity(.6),
            Colors.transparent,
          ],
              begin: isTop ? Alignment.topCenter : Alignment.bottomCenter,
              end: isTop ? Alignment.bottomCenter : Alignment.topCenter)),
    );
  }
}
