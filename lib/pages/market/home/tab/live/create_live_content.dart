import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/providers/market/live/p_create_live.dart';
import 'package:wblue_customer/routes/router.gr.dart';

class CreateLiveContentPage extends StatefulWidget {
  @override
  _CreateLiveContentPageState createState() => _CreateLiveContentPageState();
}

class _CreateLiveContentPageState extends State<CreateLiveContentPage> {
  String title;
  File contentThumbnail;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Create content'),
        ),
        body: ListView(
          physics: ClampingScrollPhysics(),
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Consumer<PCreateLive>(builder: (__, user, _) {
                contentThumbnail = user.thumbnail;
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    textField(
                      title: 'Live Title',
                      errorText: user.liveTitle.error,
                      onChange: (v) {
                        title = v;
                        context.read<PCreateLive>().changeLiveTitle(v);
                      },
                    ),
                    SizedBox(height: 20),
                    AutoSizeText(
                      'Thumbnial',
                      style: TextStyle(fontSize: 16),
                    ),
                    SizedBox(height: 8),
                    DottedBorder(
                      borderType: BorderType.RRect,
                      color: user.thumbnail == null ? Colors.red : Colors.grey,
                      radius: Radius.circular(5),
                      child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          child: GestureDetector(
                            onTap: () => loadAssets(),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Expanded(
                                  child: Container(
                                    color: Colors.white,
                                    height: 200,
                                    child: user.thumbnail == null
                                        ? Center(
                                            child: AutoSizeText(
                                              'Please Select Thumbnail',
                                              style: TextStyle(
                                                  color: user.thumbnail == null
                                                      ? Colors.red
                                                      : Colors.white,
                                                  fontSize: 22.0),
                                            ),
                                          )
                                        : Image.file(
                                            user.thumbnail,
                                            fit: BoxFit.fitHeight,
                                          ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: RaisedButton(
                            color: Colors.blue,
                            onPressed: () async {
                              user.checkFields();
                              if (!user.isValid) return;
                              ExtendedNavigator.of(context).push(
                                  '/market/live/host',
                                  arguments: CallPageArguments(
                                      title: title,
                                      contentThumbnail: contentThumbnail));
                            },
                            textColor: Colors.white,
                            child: Container(
                              height: 50,
                              child: user.isLoading
                                  ? Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: CircularProgressIndicator(
                                        backgroundColor: Colors.white,
                                      ),
                                    )
                                  : Center(child: Text('START LIVE')),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                );
              }),
            ),
          ],
        ),
      ),
    );
  }

  Widget textField(
      {String title,
      String hint,
      @required Function onChange,
      bool readOnly: false,
      String errorText}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        AutoSizeText('$title', style: TextStyle(fontSize: 16)),
        SizedBox(height: 8),
        TextField(
            readOnly: readOnly,
            textInputAction: TextInputAction.send,
            onChanged: onChange,
            decoration: new InputDecoration(
              errorText: errorText,
              border: OutlineInputBorder(),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color: readOnly ? Colors.grey : Config.primaryColor,
                    width: 1.0),
              ),
              contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
              enabledBorder: OutlineInputBorder(
                borderSide:
                    BorderSide(color: Colors.grey.withOpacity(0.4), width: 1.0),
              ),
              hintText: '${hint ?? title}',
            )),
      ],
    );
  }

  Future<void> loadAssets() async {
    FocusScope.of(context).unfocus();
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 1,
        enableCamera: true,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "WMall",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
      BotToast.showText(text: '$error');
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted || resultList.length <= 0) return;

    final byteData = await resultList[0].getByteData();
    final tempFile =
        File("${(await getTemporaryDirectory()).path}/${resultList[0].name}");
    final file = await tempFile.writeAsBytes(
      byteData.buffer
          .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes),
    );
    context.read<PCreateLive>().setThumbnail(file);
  }
}
