import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:marquee_widget/marquee_widget.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import '../../env/config.dart';

class WLiveHeaderWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _announcement(context),
          _head(context),
          _user(context)
        ],
      ),
    );
  }

  Widget _announcement(BuildContext context) {
    return Container(
      color: Config.primaryColor,
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(5.0),
      margin: EdgeInsets.only(bottom: 5),
      child: Marquee(
        pauseDuration: Duration(milliseconds: 2000),
        animationDuration: Duration(milliseconds: 5000),
        backDuration: Duration(milliseconds: 5000),
        child: Text(
          'All earphones, Android, Iphone can be used, including Huawei, Apple, Samsung, etc.',
          style: TextStyle(fontSize: 18, color: Colors.white, fontFamily: 'OpenSans'),
          overflow: TextOverflow.fade,
        ),
      ),
    );
  }

  Widget _head(BuildContext context) {
    return Container(
      child: Row(
        children: [
          _title(context),
          _details(context),
          IconButton(
            icon: Icon(MdiIcons.closeCircle),
            color: Colors.black.withOpacity(.8),
            iconSize: 30,
            onPressed: () => Navigator.of(context).pop(),
          )
        ],
      ),
    );
  }

  Widget _title(BuildContext context) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 3),
        decoration: BoxDecoration(
          color: Colors.black.withOpacity(.3),
          borderRadius: BorderRadius.only(topRight: Radius.circular(50), bottomRight: Radius.circular(50)),
        ),
        child: Row(
          children: [
            SizedBox(width: 8),
            ClipRRect(
              borderRadius: BorderRadius.circular(35),
              child: Image.asset(
                ('assets/images/users/1-boy.png'),
                width: 35,
              ),
            ),
            SizedBox(width: 15),
            Expanded(
              child: Text(
                ' Namra Ebradu Namra Ebradu Namra Ebradu',
                style: TextStyle(color: Colors.white, fontSize: 16),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            SizedBox(
              height: 30,
              width: 70,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(80.0),
                ),
                color: Config.primaryColor,
                onPressed: () {},
                child: Text(
                  'Follow',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _details(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Opacity(
            opacity: .9,
            child: Row(
              children: [
                Text(
                  'WMall',
                  style: TextStyle(color: Colors.white70, fontSize: 16, fontWeight: FontWeight.w700),
                ),
                SizedBox(width: 5),
                Icon(
                  Icons.videocam,
                  color: Colors.white70,
                ),
              ],
            ),
          ),
          SizedBox(height: 5),
          Text(
            'ID: 123123',
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
          )
        ],
      ),
    );
  }

  Widget _user(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(width: 5),
        Icon(
          Icons.person,
          color: Colors.white,
          size: 18,
        ),
        SizedBox(width: 5),
        Text(
          '1,234 views',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16,
          ),
        ),
      ],
    );
  }
}
