class RAllFoodMenuList {
    RAllFoodMenuList({
        this.foodTypeHashid,
        this.foodTypeName,
        this.foods,
    });

    String foodTypeHashid;
    String foodTypeName;
    List<Food> foods;

    factory RAllFoodMenuList.fromJson(Map<String, dynamic> json) => RAllFoodMenuList(
        foodTypeHashid: json["food_type_hashid"] == null ? null : json["food_type_hashid"],
        foodTypeName: json["food_type_name"] == null ? null : json["food_type_name"],
        foods: json["foods"] == null ? null : List<Food>.from(json["foods"].map((x) => Food.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "food_type_hashid": foodTypeHashid == null ? null : foodTypeHashid,
        "food_type_name": foodTypeName == null ? null : foodTypeName,
        "foods": foods == null ? null : List<dynamic>.from(foods.map((x) => x.toJson())),
    };
}

class Food {
    Food({
        this.hashid,
        this.name,
        this.price,
        this.full,
        this.thumbnail,
    });

    String hashid;
    String name;
    int price;
    String full;
    String thumbnail;

    factory Food.fromJson(Map<String, dynamic> json) => Food(
        hashid: json["hashid"] == null ? null : json["hashid"],
        name: json["name"] == null ? null : json["name"],
        price: json["price"] == null ? null : json["price"],
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "hashid": hashid == null ? null : hashid,
        "name": name == null ? null : name,
        "price": price == null ? null : price,
        "full": full == null ? null : full,
        "thumbnail": thumbnail == null ? null : thumbnail,
    };
}
