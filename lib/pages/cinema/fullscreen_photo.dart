import 'dart:ui';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class FullScreenPhotoPage extends StatefulWidget {
  @override
  _FullScreenPhotoPageState createState() => _FullScreenPhotoPageState();
}

class _FullScreenPhotoPageState extends State<FullScreenPhotoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.black.withOpacity(0.2),
      body: BackdropFilter(
        filter: ImageFilter.blur(sigmaY: 5.0, sigmaX: 5.0),
        child: PhotoView(
          imageProvider: AssetImage('assets/images/samples/movie.jpg'),
          heroAttributes: PhotoViewHeroAttributes(
            tag: 'movie-image',
            transitionOnUserGestures: true,
          ),
          minScale: 0.3,
          maxScale: 5.0,
          backgroundDecoration: BoxDecoration(
            color: Colors.transparent,
          ),
          onTapUp: (context, details, value) {
            ExtendedNavigator.of(context).pop();
          },
        ),
      ),
    );
  }
}
