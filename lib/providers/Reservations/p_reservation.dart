import 'package:wblue_customer/models/Reservations/reservation_model.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'dart:collection';

import 'package:flutter/foundation.dart';

DioClient _dioClient = DioClient();

class PReservation with ChangeNotifier {
  List<RestaurentReservationModel> _reservations = new List();
  bool _isLoading = true;

  UnmodifiableListView get reservations => UnmodifiableListView(_reservations);
  bool get isLoading => _isLoading;

  Future<void> fetchRestaurantsReservation() async {
    setLoading(true);
    _reservations = await fetchRestaurantsReservations();
    setLoading(false);
    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future<List> fetchRestaurantsReservations() async {
  try {
    APIResponse res = await _dioClient.privatePost('/restaurant/table/reservation/myreservation');

    if (res.code >= 400) {
      return new List<RestaurentReservationModel>();
    }

    return res.data.map<RestaurentReservationModel>((json) => RestaurentReservationModel.fromJson(json)).toList();
  } catch (e) {
    print('method fetchRestaurantsReservations throw error: $e');
    return new List<RestaurentReservationModel>();
  }
}
