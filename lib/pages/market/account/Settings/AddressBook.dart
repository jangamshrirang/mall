import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:geolocator/geolocator.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';

class AddressBookScreen extends StatefulWidget {
  @override
  _AddressBookScreenState createState() => _AddressBookScreenState();
}

class _AddressBookScreenState extends State<AddressBookScreen> {
  String _locationMessage = "";
  var lat, long;
  void _getCurrentLocation() async {
    final position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print(position);

    setState(() {
      _locationMessage = "${position.latitude}, ${position.longitude}";
      lat = position.latitude;
      long = position.longitude;
      //openGoogleRoute(position.latitude,position.longitude);
      // _launchURL();
    });
  }

  final addressController = TextEditingController();
  final zoneController = TextEditingController();
  final streetController = TextEditingController();
  final buildingController = TextEditingController();
  final plotNoController = TextEditingController();
  LocationResult _pickedLocation;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return MaterialApp(
       
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                color: Colors.black,
                onPressed: () => Navigator.pop(context, false),
              ),
              title: Text("Address",
                  style: TextStyle(
                      decoration: TextDecoration.none,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 20)),
            ),
            body: SingleChildScrollView(
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        SizedBox(height: 10),
                       Card(
                          child: Container(
                            width: size.width*0.9,
                            height: size.height*0.08,
                            padding: EdgeInsets.only(top:10,left:10),
                            child: Text("Address: Al Kalidiy St,Bulding No: 20 ,Office No:68, Doha, Qatar.",style:TextStyle(fontSize:18,)),
                          ),
                        ),
                      Row(
                        children: [
                          Spacer(),
                          Spacer(),
                          Spacer(),
                          RaisedButton(
                            onPressed: () async {
                              LocationResult result = await showLocationPicker(
                                context,
                                "AIzaSyCpZybhi4ZzUBXImzkKtaggzrOSsfcH8p4",
                                myLocationButtonEnabled: true,
                                layersButtonEnabled: true,
                              );

                              print("result = ${result}");
                              setState(() {
                                _pickedLocation = result;
                                stateManagment
                                    .setLocationAreaName(result.address);
                                stateManagment
                                    .setLocationLat("${result.latLng}");
                              });

                              //      setState(() {
                              //            Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //       builder: (context) => LocationPinScreen()),
                              // );
                              //      });
                            },
                            color: Config.primaryColor,
                            textColor: Colors.white,
                            padding: const EdgeInsets.all(0.0),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            child: Container(
                              decoration: const BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20.0))),
                              padding:
                                  const EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: const Text('      Add Location      ',
                                  style: TextStyle(fontSize: 20)),
                            ),
                          ),
                        SizedBox(width: size.height*0.12,)
                        ],
                      ),
                      _locationMessage != "" ||
                              stateManagment.locationAreaname != null
                          ? Column(
                              children: [
                                Icon(
                                  Icons.check_circle,
                                  color: Colors.green,
                                  size: 35.0,
                                ),
                                Text('Location Added'),
                              ],
                            )
                          : Text(""),
                      SizedBox(height: size.height * 0.01),
                      textfield(
                        addressController,
                        "Address",
                        "",
                      ),
                      SizedBox(height: size.height * 0.01),
                      textfield(
                        zoneController,
                        "Zone",
                        "",
                      ),
                      SizedBox(height: size.height * 0.01),
                      textfield(
                        streetController,
                        "Street",
                        "",
                      ),
                      SizedBox(height: size.height * 0.01),
                      textfield(
                        buildingController,
                        "Building No",
                        "",
                      ),
                      SizedBox(height: size.height * 0.01),
                      textfield(
                        plotNoController,
                        "Plot No",
                        "",
                      ),
                      SizedBox(height: size.height * 0.05),
                      Container(
                        height: size.height * 0.0415,
                        child: RaisedButton(
                          onPressed: () {
                            // ExtendedNavigator.of(context)
                            //     .root
                            //     .push('/restaurants/delivery-confirmation');
                            Fluttertoast.showToast(
                                msg: "Address added ",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.green,
                                textColor: Colors.white,
                                fontSize: 16.0);
                          },
                          color: Config.primaryColor,
                          textColor: Colors.white,
                          padding: const EdgeInsets.all(0.0),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                          child: Container(
                            decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0))),
                            padding: const EdgeInsets.fromLTRB(20, 05, 20, 05),
                            child: const Text('        Proceed         ',
                                style: TextStyle(fontSize: 20)),
                          ),
                        ),
                      ),
                      SizedBox(height: size.height * 0.05),
                    ]),
              ),
            )));
  }

  textfield(
    controller,
    String titelname,
    hintText,
  ) {
    var size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 15, bottom: 5),
          child: Text(
            titelname,
            style: TextStyle(
              fontSize: 16,
              // fontFamily: "SFUIText",
            ),
          ),
        ),
        Container(
          width: size.width * 0.65,
          height: size.height * 0.06,
          child: Card(
            color: Colors.white12,
            elevation: 0.0,
            child: TextFormField(
              controller: controller,
              cursorColor: Colors.black,
              maxLines: 1,
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
                fontWeight: FontWeight.w400,
              ),
              decoration: InputDecoration(
                filled: true,
                hintStyle: TextStyle(
                    color: Colors.black12, fontWeight: FontWeight.bold),
                fillColor: Colors.grey[200],
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.black, width: 1),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.black26, width: 1)),
                hintText: hintText,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
