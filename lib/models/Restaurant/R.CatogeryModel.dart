class RCategeriesListModel {
    RCategeriesListModel({
        this.name,
        this.hashid,
    });

    String name;
    String hashid;

    factory RCategeriesListModel.fromJson(Map<String, dynamic> json) => RCategeriesListModel(
        name: json["name"] == null ? null : json["name"],
        hashid: json["hashid"] == null ? null : json["hashid"],
    );

    Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "hashid": hashid == null ? null : hashid,
    };
}
