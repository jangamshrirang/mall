import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../env/config.dart';

class WButtonWidget extends StatelessWidget {
  final String title, subtitle;
  final IconData iconData;
  final bool expanded;
  final double paddingSize;
  final Function() onPressed;

  WButtonWidget({
    this.title = '',
    this.subtitle = '',
    this.iconData,
    this.expanded: false,
    this.paddingSize: 4.0,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: expanded ? double.infinity : null,
      child: RaisedButton(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            (iconData != null ? Icon(iconData, size: 16.0) : Container()),
            SizedBox(width: iconData != null ? 4.0 : 0.0),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  title.toUpperCase(),
                  textAlign: TextAlign.center,
                ),
                subtitle.length > 0
                    ? Text(
                        subtitle,
                        style: TextStyle(fontSize: 10.0),
                      )
                    : SizedBox(),
              ],
            ),
          ],
        ),
        color: Config.primaryColor,
        textColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        padding: EdgeInsets.all(paddingSize),
        onPressed: onPressed,
      ),
    );
  }
}

void function() {}
