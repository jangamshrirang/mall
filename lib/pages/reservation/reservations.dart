import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Reservations/reservation_model.dart';
import 'package:wblue_customer/pages/reservation/reservation_details.dart';
import 'package:wblue_customer/providers/Reservations/p_reservation.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/widgets/navigateScreen.dart';
import 'package:wblue_customer/widgets/w_card.dart';
import 'package:wblue_customer/widgets/w_chip.dart';
import 'package:wblue_customer/widgets/w_drawer.dart';
import 'package:wblue_customer/widgets/w_loading.dart';

class ReservationPage extends StatefulWidget {
  @override
  _ReservationPageState createState() => _ReservationPageState();
}

class _ReservationPageState extends State<ReservationPage> {
  Widget _item(Types type, RestaurentReservationModel restaurentReservationModel, {Function onTap: _function}) {
    return InkWell(
      splashColor: Colors.amber,
      child: WCardWidget(
        padding: EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'DineIn #123',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                WChipWidget(
                  type: type,
                ),
              ],
            ),
            Text(
              'Restaurant Name',
              style: TextStyle(fontSize: 12.0),
            ),
            Text(
              'Placed on Tue - Apr 07, 2020',
              style: TextStyle(fontSize: 12.0),
            ),
            Text(
              'Pay in Person',
              style: TextStyle(fontSize: 12.0),
            ),
            (type == Types.cancelled
                ? Text(
                    'Request Cancel: Approved',
                    style: TextStyle(fontSize: 12.0, color: Colors.green),
                  )
                : Container()),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  'Total ',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text('QAR 0.00'),
              ],
            ),
          ],
        ),
      ),
      onTap: () {
        onTap();
      },
    );
  }

  @override
  void initState() {
    super.initState();
    print(Helper.state[StatesNames.token]);
    Future.microtask(() => context.read<PReservation>().fetchRestaurantsReservation());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Config.primaryColor,
        title: Text('Reservations'),
        centerTitle: true,
      ),
      drawer: WDrawerWidget('reservation'),
      body: Container(
        child: Consumer<PReservation>(
            builder: (context, restaurant, child) => restaurant.reservations.length > 0
                ? ListView(
                    children: List.generate(restaurant.reservations.length, (index) {
                    return GestureDetector(
                      onTap: () => ExtendedNavigator.of(context).push('/reservations/details',
                          arguments: ReservationDetailsPageArguments(
                              restaurentReservationModel: restaurant.reservations[index])),
                      child: Card(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 0.3.sw,
                              width: 0.3.sw,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: NetworkImage(restaurant.reservations[index].thumbnail), fit: BoxFit.fill),
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(restaurant.reservations[index].restaurant.name,
                                        maxLines: 2, style: TextStyle(fontSize: 36.sp, fontWeight: FontWeight.bold)),
                                    SizedBox(height: 0.01.sh),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text('+974' + restaurant.reservations[index].restaurant.phone,
                                            style: TextStyle(
                                                fontSize: 30.sp, fontWeight: FontWeight.normal, color: Colors.black)),
                                        Text('Guests: ' + '${restaurant.reservations[index].numberOfGuest}',
                                            style: TextStyle(
                                                fontSize: 30.sp, fontWeight: FontWeight.normal, color: Colors.black)),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 0.005.sh,
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text('Time: ' + restaurant.reservations[index].timeIn,
                                            style: TextStyle(
                                                fontSize: 30.sp, fontWeight: FontWeight.normal, color: Colors.black)),
                                        Text('${restaurant.reservations[index].status}',
                                            style: TextStyle(
                                                fontSize: 30.sp, fontWeight: FontWeight.normal, color: Colors.black))
                                      ],
                                    ),
                                    SizedBox(
                                      height: 0.005.sh,
                                    ),
                                    Text(
                                        'Date: ' + '${restaurant.reservations[index].date.toString().substring(0, 10)}',
                                        style: TextStyle(
                                            fontSize: 30.sp, fontWeight: FontWeight.normal, color: Colors.black)),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }))
                : ListView.builder(
                    itemBuilder: (context, index) => Container(
                      width: 1.sw,
                      height: 0.1.sh,
                      margin: EdgeInsets.all(8.0),
                      child: Shimmer.fromColors(
                        baseColor: Color.fromRGBO(0, 0, 0, 0.1),
                        highlightColor: Color(0x44CCCCCC),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                      ),
                    ),
                    itemCount: (1.sh / 3).round(),
                  )),
      ),
    );
  }
}

void _function() {}
