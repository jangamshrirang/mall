import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/widgets/w_button.dart';

import 'ThankYouScreen.dart';

class AdditionalInfo extends StatelessWidget {
  final noteController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBarWithOutIcon(context, "Delivery Survey"),
      body: Column(
        children: [
          Center(
            child: FractionallySizedBox(
              widthFactor: 0.3,
              child: Image.asset('assets/images/logos/wmall-512.png'),
            ),
          ),
          text(
              "Do you have any additional comments pr feedback on your delivery expereience ?",
              FontWeight.normal,
              FontStyle.normal),
          SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.only(left: 0),
            height: size.height * 0.15,
            width: size.width * 0.75,
            child: TextFormField(
              maxLines: 10,
              cursorColor: Colors.black,
              controller: noteController,
              decoration: InputDecoration(
                  filled: true,
                  hintStyle: TextStyle(
                      color: Colors.black38,
                      fontWeight: FontWeight.normal,
                      decoration: TextDecoration.none),
                  fillColor: Colors.white,
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.black, width: 1),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      borderSide: BorderSide(color: Colors.black38, width: 1)),
                  contentPadding:
                      EdgeInsets.only(left: 15, bottom: 10, top: 5, right: 15),
                  hintText: "Type here..... "),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => TankyouScreen()));
            },
            child: Container(
                width: size.width * 0.85,
                child: Text(
                  "Skip",
                  textAlign: TextAlign.end,
                  style: TextStyle(color: Colors.blue, fontSize: 15),
                )),
          ),
          SizedBox(
            height: 20,
          ),
          FractionallySizedBox(
            widthFactor: 0.3,
            child: WButtonWidget(
              title: 'Done',
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => TankyouScreen()));
              },
            ),
          ),
        ],
      ),
    );
  }

  text(String text, FontWeight fontWeight, FontStyle fontStyle) {
    return Container(
      margin: EdgeInsets.only(left: 50, right: 20),
      child: Text(
        text,
        style: TextStyle(
            color: Colors.black,
            fontWeight: fontWeight,
            fontSize: 18,
            fontStyle: fontStyle),
      ),
    );
  }
}
