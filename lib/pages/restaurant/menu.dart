import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/R_FoodListModel.dart';
import 'package:wblue_customer/models/Restaurant/cart.dart';
import 'package:wblue_customer/pages/booking/Flr_Table_Copy.dart';
import 'package:wblue_customer/providers/restaurant/R_AddToCartHelper.dart';
import 'package:wblue_customer/providers/restaurant/R_FoodListHlper.dart';
import 'package:wblue_customer/providers/restaurant/R_menuHelper.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_loading.dart';
import 'package:wblue_customer/widgets/w_menu_grid_item.dart';
import 'package:wblue_customer/widgets/w_rating.dart';

import 'cart/cart.dart';

class MenuPage extends StatefulWidget {
  String fromScan,
      preOrder,
      delivery,
      restId,
      filterKey,
      appBarTitle,
      fromTakeAway;
  String coverImage, restimage, restName, onlybooking, restSlogan, from;
  var totalPrice, tableID;

  MenuPage(
      {this.fromScan,
      this.preOrder,
      this.delivery,
      this.coverImage,
      this.restimage,
      this.restName,
      this.onlybooking,
      this.restSlogan,
      this.filterKey,
      this.restId,
      this.from,
      this.fromTakeAway,
      this.appBarTitle,
      this.totalPrice,
      this.tableID});
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  @override
  void initState() {
    fetchMenu();
    Future.microtask(() => context
        .read<RFoodListHelper>()
        .fetchRFoodList(widget.restId, widget.filterKey));
    super.initState();
  }

  Future<void> verify() async {
    bool res = false;

    bool canNav = await ExtendedNavigator.of(context)
        .root
        .canNavigate('/restaurants/cart');

    res = canNav ?? false;
    if (res) return;
  }

  int count = 0;
  bool fav;
  @override
  Widget build(BuildContext context) {
    int column = 2;
    double itemWidth = MediaQuery.of(context).size.width / column;
    double itemHeight = MediaQuery.of(context).size.height * 0.28;

    void _detailsModal(String image, name, var foodId, double price) {
      int qty = 1;
      var size = MediaQuery.of(context).size;
      showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        backgroundColor: Colors.white,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState2) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        height: MediaQuery.of(context).size.width * 0.8,
                        width: size.width,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: NetworkImage(image ?? ''),
                            fit: BoxFit.cover,
                          ),
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(25.0)),
                        ),
                        child: Align(
                          alignment: Alignment.topRight,
                          child: IconButton(
                            icon: Icon(Icons.close),
                            onPressed: () {
                              setState(() {
                                Navigator.pop(context);
                              });
                            },
                          ),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          name,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: size.height * 0.04),
                        ),
                        subtitle: Text(
                          'QAR ' + '$price',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.w300,
                              fontSize: size.height * 0.03),
                        ),
                        trailing: WRatingsWidget(3.5,
                            size: size.height * 0.015, compact: true),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Text(
                          'lorem ipsum jklsjadlk dahdkja hdkasjdhjakdhaskj dhaks dhaksjdh sakjdhsakj',
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: size.height * 0.02),
                        ),
                      ),
                      Divider(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    'QTY: ',
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: size.height * 0.025),
                                  ),
                                  Text(
                                    qty.toString(),
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: size.height * 0.04),
                                  ),
                                ],
                              ),
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.remove,
                                size: size.height * 0.04,
                              ),
                              color:
                                  qty > 1 ? Config.primaryColor : Colors.grey,
                              onPressed: () {
                                if (qty > 1) {
                                  setState2(() {
                                    --qty;
                                  });
                                }
                              },
                            ),
                            IconButton(
                              icon: Icon(Icons.add, size: size.height * 0.04),
                              color: Config.primaryColor,
                              onPressed: () async {
                                await verify();

                                setState2(() {
                                  ++qty;
                                });
                              },
                            ),
                            fav == false || fav == null
                                ? IconButton(
                                    icon: Icon(Icons.favorite_border,
                                        size: 20, color: Colors.black38),
                                    onPressed: () {
                                      setState2(() {
                                        fav = true;
                                      });
                                    })
                                : IconButton(
                                    icon: Icon(Icons.favorite,
                                        size: 20, color: Colors.red),
                                    onPressed: () {
                                      setState2(() {
                                        fav = false;
                                      });
                                    }),
                          ],
                        ),
                      ),
                      Divider(),
                      Padding(
                          padding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom),
                          child: TextField(
                              decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Config.primaryColor, width: 0.5),
                                  ),
                                  contentPadding:
                                      EdgeInsets.symmetric(horizontal: 8.0),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.grey, width: 0.5),
                                  ),
                                  hintText: "Special Request"))),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: WButtonWidget(
                          title: 'add to cart',
                          expanded: true,
                          onPressed: () async {
                            await verify();
                            setState(() {
                              if (widget.delivery == "yes" ||
                                  widget.preOrder == "yes" ||
                                  stateManagment.deliverOrTakeString == "yes" ||
                                  widget.fromScan == "yes") {
                                print(foodId);
                                Future.microtask(() => context
                                    .read<R_AddToCartHelper>()
                                    .fetchRestaurantsAddToCart(
                                        foodId,
                                        qty,
                                        "any",
                                        widget.preOrder == "yes"
                                            ? 1
                                            : widget.fromTakeAway == "no"
                                                ? 2
                                                : 3,
                                        widget.preOrder == "yes" ? 1 : 0,""));
                                Cart('Restaurant Name').add(CartItem(
                                    image: image,
                                    name: name,
                                    price: price,
                                    qty: qty));
                                stateManagment.setTotalPrict(
                                    stateManagment.totalPrice == null
                                        ? 0 + price * qty
                                        : stateManagment.totalPrice +
                                            price * qty);
                                Fluttertoast.showToast(
                                    msg: "Added to cart",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1,
                                    backgroundColor: Colors.black,
                                    textColor: Colors.white,
                                    fontSize: 16.0);
                                Navigator.pop(context);
                              } else {
                                _selection(context);
                              }
                            });
                          },
                        ),
                      ),
                      SizedBox(height: size.height * 0.15)
                    ],
                  ),
                ),
              );
            },
          );
        },
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Config.primaryColor,
        leading: IconButton(
            icon: Icon(Icons.chevron_left),
            onPressed: () {
              Navigator.of(context).popUntil(
                  (_) => widget.from != null ? count++ >= 1 : count++ >= 2);
            }),
        title: Text(widget.appBarTitle),
        centerTitle: true,
        actions: <Widget>[
          // WCartWidget(
          //   delivery: widget.delivery == null
          //       ? stateManagment.deliverOrTakeString
          //       : widget.delivery,
          //   preOrder: widget.preOrder == null
          //       ? stateManagment.deliverOrTakeString
          //       : widget.preOrder,
          //   fromScan: widget.fromScan,
          // ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {
            print('refreshing...');
          });
        },
        child: Container(child:
            Consumer<RFoodListHelper>(builder: (context, restaurant, child) {
          print("Menu error  :  ${restaurant.rFoodMenu}");
          if (restaurant.rFoodMenu != null) {
            RFoodListModel rFoodListModel =
                RFoodListModel.fromJson(restaurant.rFoodMenu);

            return rFoodListModel.foods.length == null ||
                    rFoodListModel.foods.length == 0
                ? Center(
                    child: Text("no food data",
                        style: TextStyle(color: Colors.black, fontSize: 40)))
                : GridView.count(
                    crossAxisCount: column,
                    childAspectRatio: (itemWidth / itemHeight),
                    controller: new ScrollController(keepScrollOffset: false),
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    children:
                        List.generate(rFoodListModel.foods.length, (index) {
                      return WMenuGridItemWidget(
                        price: rFoodListModel.foods[index].price,
                        itemHeight: itemHeight,
                        image: rFoodListModel.foods[index].full,
                        name: rFoodListModel.foods[index].name,
                        onTap: () {
                          _detailsModal(
                            rFoodListModel.foods[index].full,
                            rFoodListModel.foods[index].name,
                            rFoodListModel.foods[index].hashId,
                            double.parse(
                                "${rFoodListModel.foods[index].price}"),
                          );
                        },
                      );
                    }),
                  );
          } else {
            return Center(
                child:
                    restaurant.isLoading ? WLoadingWidget() : Text("No Data"));
          }
        })),
      ),
      bottomNavigationBar: Container(
        margin: EdgeInsets.only(bottom: 10),
        height: 60.0,
        padding: EdgeInsets.symmetric(vertical: 5.0),
        child: FractionallySizedBox(
          widthFactor: 0.8,
          child: WButtonWidget(
            iconData: Icons.shopping_basket,
            title: stateManagment.totalPrice == null
                ? 'View Cart              \b\b Total QR ' + "0.0"
                : 'View Cart              \b\b Total QR ' +
                    "${stateManagment.totalPrice}",
            paddingSize: 0.0,
            onPressed: () {
              //
              // widget.fromScan == "yes" ||
              //         widget.preOrder == "yes" ||
              //         widget.delivery == "yes" ||
              //         stateManagment.deliverOrTakeString == "yes"
              //     ?
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RestaurantCartPage(
                            fromMenu: "FromMenu",
                            fromTakeAway: widget.fromTakeAway,
                            appBarTitle: widget.appBarTitle,
                            filterkey: widget.filterKey,
                            restId: widget.restId,
                            restSlogan: widget.restSlogan,
                            coverImage: widget.coverImage,
                            restName: widget.restName,
                            restimage: widget.restimage,
                            delivery: widget.delivery == null
                                ? stateManagment.deliverOrTakeString
                                : widget.delivery,
                            preOrder: widget.preOrder == null
                                ? stateManagment.deliverOrTakeString
                                : widget.preOrder,
                            fromScan: widget.fromScan,
                            totlaPrice: stateManagment.totalPrice,
                          )));
              // : ExtendedNavigator.of(context)
              //     .root
              //     .push('/restaurants/cart');
            },
          ),
        ),
      ),
    );
  }

  _selection(BuildContext context) async {
    var size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 300,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: ListView(
                  children: <Widget>[
                    Container(
                      height: size.height * 0.05,
                      child: Text(
                        "Please select where your food want to be served",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                    ),
                    catogoryContainer(
                      "Table Booking",
                      "assets/images/book table.png",
                      "assets/restaurent/tablebg.png",
                      () {
                        stateManagment.setDeliveryOrTakeString(null);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RestTableBookingCopy(
                                      restId: widget.restId,
                                      restSlogan: widget.restSlogan,
                                      onlybooking: 'onlytable',
                                      coverImage: widget.coverImage,
                                      restName: widget.restName,
                                      restimage: widget.restimage,
                                    )));
                      },
                      size.height * 0.03,
                      size.width * 0.55,
                    ),
                    Divider(
                      thickness: 5,
                      color: Colors.transparent,
                    ),
                    catogoryContainer(
                        "  Delivery",
                        "assets/images/delivery.png",
                        "assets/restaurent/deliverbg.png", () {
                      stateManagment.setDeliveryOrTakeString("yes");
                      Navigator.of(context).popUntil((_) => count++ >= 1);
                    }, size.height * 0.05, size.width * 0.62),
                    Divider(
                      thickness: 5,
                      color: Colors.transparent,
                    ),
                    catogoryContainer(
                      "Tack away",
                      "assets/images/take out.png",
                      "assets/restaurent/takeaway.png",
                      () {
                        stateManagment.setDeliveryOrTakeString("yes");
                        Navigator.of(context).popUntil((_) => count++ >= 1);
                      },
                      size.height * 0.05,
                      size.width * 0.65,
                    ),
                    Divider(
                      thickness: 5,
                      color: Colors.transparent,
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  catogoryContainer(String title, image, bgimag, Function onTap,
      double imageheight, bgimageWidth) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: onTap,
      child: Stack(
        children: [
          Container(
            height: size.height * 0.08,
            width: size.width * 0.9,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: <Color>[
                  Color(0xFF1c366d),
                  Color(0xFF182e5e),
                  Color(0xFF14264f),
                ],
              ),
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: size.width * 0.05,
                ),
                Image.asset(
                  image,
                  height: imageheight,
                ),
                SizedBox(width: size.width * 0.06),
                Spacer(),
              ],
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: bgimageWidth),
              child: Image.asset(bgimag, height: size.height * 0.08)),
          Container(
            // color: Colors.white,
            margin: EdgeInsets.only(
                left: size.width * 0.3, top: size.height * 0.03),
            width: size.width * 0.5,
            child: Text(
              title,
              textAlign: TextAlign.start,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
          ),
        ],
      ),
    );
  }
}
