import 'dart:convert';

import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/auth.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/pages/landing.dart';
import 'package:wblue_customer/providers/auth/p_auth.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_input.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

class AuthService {
  //Handles Auth
  final FirebaseAuth auth = FirebaseAuth.instance;
  handleAuth(BuildContext context) {
    return StreamBuilder(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (BuildContext context, snapshot) {
          if (snapshot.hasData) {
            Global.user = UserModel.createDummy();
            return LandingPage();
          } else {
            return LoginPage();
          }
        });
  }

  //Sign out
  signOut() {
    FirebaseAuth.instance.signOut();
  }

  //SignIn
  signIn(AuthCredential authCreds, BuildContext context) async {
    FirebaseAuth.instance.signInWithCredential(authCreds);
    delayedNavigation(context);
  }

  signInWithOTP(smsCode, verId, BuildContext context) {
    AuthCredential authCreds = PhoneAuthProvider.credential(verificationId: verId, smsCode: smsCode);
    signIn(authCreds, context);
  }

  // navigating(BuildContext context) async {
  //   FirebaseUser user = await auth.currentUser();

  //   Navigator.push(
  //       context,
  //       PageRouteBuilder(
  //           transitionDuration: Duration(seconds: 5),
  //           pageBuilder: (_, __, ___) =>
  //               LandingPage() // AuthService().handleAuth(),
  //           ));
  // }
  delayedNavigation(BuildContext context) {
    Future.delayed(Duration(seconds: 1), () {
      // 5s over, navigate to a new page
      Global.user = UserModel.createDummy();
      Navigator.push(context, MaterialPageRoute(builder: (context) => LandingPage()));
    });
  }
}

// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';

// class DashboardPage extends StatefulWidget {
//   @override
//   _DashboardPageState createState() => _DashboardPageState();
// }

// class _DashboardPageState extends State<DashboardPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: Center(
//             child: RaisedButton(
//       child: Text('Signout'),
//       onPressed: () {
//         AuthService().signOut();
//       },
//     )));
//   }
// }

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final formKey = new GlobalKey<FormState>();
  Map<String, dynamic> privacyPolicy;
  AuthModel auth = AuthModel();

  String phoneNo, verificationId, smsCode;

  bool codeSent = false, phoneIsValid = false;
  Map<String, dynamic> body = {
    'phone': '',
    'otp': '',
  };

  // FacebookLogin fbLogin = new FacebookLogin();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
              gradient: RadialGradient(
                colors: [Config.primaryColor, Color(0xFF284d99).withOpacity(0.9)],
                stops: [0.4, 1],
                center: Alignment(0.0, 0.0),
                focal: Alignment(0.0, -0.0),
                focalRadius: 1.9,
              ),
            ),
            child: Scaffold(
              backgroundColor: Colors.transparent,
              body: Stack(
                children: [
                  Center(
                    child: SafeArea(
                      child: SingleChildScrollView(
                        physics: ClampingScrollPhysics(),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: size.width * 0.5,
                              width: size.width * 0.5,
                              child: Center(
                                  child: WImageWidget(placeholder: AssetImage('assets/images/logos/wmall-512.png'))),
                            ),
                            SizedBox(
                              height: size.height * 0.05,
                            ),
                            Container(
                              width: size.width * 0.80,
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    AutoSizeText(
                                      'Login',
                                      style: TextStyle(
                                          color: Config.primaryColor, fontSize: 26.0, fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      height: 20.0,
                                    ),
                                    Consumer<PAuth>(
                                      builder: (_, phone, __) {
                                        return SizedBox(
                                          child: InternationalPhoneNumberInput(
                                            onInputChanged: (PhoneNumber number) {
                                              phoneNo = number.parseNumber();
                                            },
                                            onInputValidated: (valid) {
                                              phoneIsValid = valid;
                                              context.read<PAuth>().setNumberValid(true);
                                            },
                                            countries: ['QA'],
                                            ignoreBlank: true,
                                            // autoValidate: true,
                                            selectorTextStyle: TextStyle(color: Colors.black),
                                            // initialValue: number,
                                            // textFieldController: controller,
                                            inputDecoration: InputDecoration(
                                                hintText: 'Phone number',
                                                errorText: phone.number.error,
                                                filled: true,
                                                fillColor: Color(0xFF284d99).withOpacity(0.1)),
                                          ),
                                        );
                                      },
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Expanded(
                                          child: WRoundedButton(
                                            onCustomButtonPressed: () async {
                                              if (phoneNo.length > 0 && phoneIsValid) {
                                                final res = await auth.checkCredential(phoneNo);

                                                if (res) {
                                                  context.read<PAuth>().setPhoneNumber(phoneNo);
                                                  context.read<PAuth>().submitPhoneNumber();
                                                } else {
                                                  context
                                                      .read<PAuth>()
                                                      .setNumberValid(false, msg: 'Phone not yet registered!');
                                                }
                                              }
                                            },
                                            btnColor: Config.primaryColor,
                                            labelColor: Colors.white,
                                            child: AutoSizeText('Login'),
                                          ),
                                        ),
                                      ],
                                    ),
                                    // SizedBox(
                                    //   height: 8,
                                    // ),
                                    // Row(children: <Widget>[
                                    //   Expanded(child: Divider()),
                                    //   SizedBox(
                                    //     width: 5,
                                    //   ),
                                    //   AutoSizeText('OR'),
                                    //   SizedBox(
                                    //     width: 5,
                                    //   ),
                                    //   Expanded(child: Divider()),
                                    // ]),
                                    // SizedBox(
                                    //   height: 8,
                                    // ),
                                    // Row(
                                    //   mainAxisSize: MainAxisSize.max,
                                    //   mainAxisAlignment:
                                    //       MainAxisAlignment.center,
                                    //   children: [
                                    //     GestureDetector(
                                    //         onTap: _googleSignUp,
                                    //         child: Image.asset(
                                    //             'assets/images/logos/google-64.png',
                                    //             height: 30)),
                                    //   ],
                                    // ),
                                    Center(
                                      child: FlatButton(
                                        onPressed: () => ExtendedNavigator.of(context).push(Routes.registrationPage),
                                        child: AutoSizeText.rich(
                                          TextSpan(children: [
                                            TextSpan(text: 'New here? '),
                                            TextSpan(text: 'Sign Up', style: TextStyle(fontWeight: FontWeight.bold)),
                                          ]),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(5.0),
                                ),
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(
                                    color: Colors.black26,
                                    blurRadius: 10.0,
                                    offset: new Offset(0.0, 0.0),
                                  ),
                                ],
                              ),
                              margin: EdgeInsets.all(5.0),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 20.0),
                              child: Center(
                                child: Column(
                                  children: [
                                    AutoSizeText(
                                      'By Continuing, you agree to WMall',
                                      style: TextStyle(color: Colors.white60),
                                    ),
                                    AutoSizeText.rich(
                                      TextSpan(children: [
                                        TextSpan(
                                            text: 'Terms of Service',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white70,
                                                decoration: TextDecoration.underline),
                                            recognizer: new TapGestureRecognizer()
                                              ..onTap = () {
                                                loadTerms();
                                                displayBottomSheet(context);
                                              }),
                                        TextSpan(text: ' and ', style: TextStyle(color: Colors.white60)),
                                        TextSpan(
                                            text: 'Privacy Policy',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white70,
                                                decoration: TextDecoration.underline),
                                            recognizer: new TapGestureRecognizer()
                                              ..onTap = () {
                                                loadTerms();
                                                displayBottomSheet(context);
                                              }),
                                      ]),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 20,
                    left: 20,
                    child: SafeArea(
                        child: IconButton(
                      icon: Icon(MdiIcons.arrowLeft),
                      color: Colors.white,
                      onPressed: () => context.navigator.pop(),
                    )),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );

    Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      backgroundColor: Config.dullColor,
      body: ListView(
        physics: ClampingScrollPhysics(),
        children: [
          Container(
            margin: EdgeInsets.only(left: size.width * 0.1, top: size.height * 0.08, right: size.width * 0.08),
            height: size.height * 0.6,
            width: size.width * 0.8,
            child: Form(
                key: formKey,
                child: ListView(
                  physics: ClampingScrollPhysics(),
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          Global.user = UserModel.createDummy();
                          Navigator.push(
                              context,
                              PageRouteBuilder(
                                  transitionDuration: Duration(seconds: 5),
                                  pageBuilder: (_, __, ___) => LandingPage() // AuthService().handleAuth(),
                                  ));
                        });
                      },
                      child: Center(
                        child: FractionallySizedBox(
                          widthFactor: 0.5,
                          child: Image.asset('assets/images/logos/wmall.png'),
                        ),
                      ),
                    ),
                    SizedBox(height: 50),
                    codeSent
                        ? Container(
                            padding: EdgeInsets.all(16.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              color: Colors.black45,
                            ),
                            child: Column(
                              children: [
                                WInputWidget(
                                  label: 'OTP',
                                  isNumber: true,
                                  initialValue: body['otp'],
                                  onChanged: (val) {
                                    setState(() {
                                      this.smsCode = val;
                                      body['otp'] = val;
                                    });
                                  },
                                ),
                                WButtonWidget(
                                  title: 'Submit',
                                  onPressed: () {
                                    setState(() {
                                      if (smsCode.length == 6) {
                                        Fluttertoast.showToast(
                                            msg: "Verifying ...",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.CENTER,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: Colors.black,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                        AuthService().signInWithOTP(smsCode, verificationId, context);
                                      } else {
                                        Fluttertoast.showToast(
                                            msg: "Please enter valid otp",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.CENTER,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      }
                                    });
                                  },
                                ),
                              ],
                            ))
                        : Container(
                            padding: EdgeInsets.all(16.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              color: Colors.black45,
                            ),
                            child: Column(
                              children: [
                                WInputWidget(
                                  isNumber: true,
                                  label: 'Phone',
                                  initialValue: body['phone'],
                                  onChanged: (val) {
                                    setState(() {
                                      this.phoneNo = val;
                                      body['phone'] = val;
                                    });
                                  },
                                ),
                                WButtonWidget(
                                  title: 'Request OTP',
                                  onPressed: () {
                                    setState(() {
                                      verifyPhone(phoneNo, context);
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                  ],
                )),
          ),
          Container(
              child: Row(
            children: [
              Spacer(),
              Container(
                width: size.width * 0.3,
                child: Divider(
                  thickness: 1.0,
                  color: Colors.white,
                ),
              ),
              Spacer(),
              Text(
                ' Social Login',
                style: TextStyle(color: Colors.white),
              ),
              Spacer(),
              Container(
                  width: size.width * 0.3,
                  child: Divider(
                    thickness: 1.0,
                    color: Colors.white,
                  )),
              Spacer(),
            ],
          )),
          Container(
            margin: EdgeInsets.only(top: 10),
            height: size.height * 0.08,
            // color:Colors.red,
            child: Row(
              children: [
                SizedBox(
                  width: size.width * 0.33,
                ),
                GestureDetector(
                    onTap: () {
                      setState(() {
                        _googleSignUp();
                      });
                    },
                    child: Image.asset("assets/images/google-plus.png", height: 30)),
                Spacer(),
                GestureDetector(
                    onTap: () {
                      setState(() {
                        //_signInFacebook();
                        //signUpWithFacebook();
                      });
                    },
                    child: Image.asset("assets/images/facebook.png", height: 50)),
                SizedBox(
                  width: size.width * 0.35,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<Map<String, dynamic>> loadTerms() async {
    if (privacyPolicy == null) {
      final res = await rootBundle.loadString('assets/terms_of_service.json');
      privacyPolicy = jsonDecode(res);
    }
    return privacyPolicy;
  }

  void displayBottomSheet(BuildContext context) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (ctx) {
          return Container(
              height: MediaQuery.of(context).size.height * 0.8,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: AutoSizeText.rich(
                  TextSpan(children: [
                    TextSpan(text: '${privacyPolicy['header']}', style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: '\n \n${privacyPolicy['body']}'),
                    TextSpan(text: '\n \n${privacyPolicy['header']}', style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: '\n \n${privacyPolicy['body']}'),
                    TextSpan(text: '\n \n${privacyPolicy['header']}', style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: '\n \n${privacyPolicy['body']}'),
                    TextSpan(text: '\n \n${privacyPolicy['header']}', style: TextStyle(fontWeight: FontWeight.bold)),
                    TextSpan(text: '\n \n${privacyPolicy['body']}'),
                  ]),
                ),
              ));
        });
  }

//    Future<Null> _loginn() async {
//     final FacebookLoginResult result = await fbLogin.logIn([
//       'email'
//     ]);

//     switch (result.status) {
//       case FacebookLoginStatus.loggedIn:
//         final FacebookAccessToken accessToken = result.accessToken;

//         print('''
//          Logged in!
//          Token: ${accessToken.token}
//          User id: ${accessToken.userId}
//          Expires: ${accessToken.expires}
//          Expires: ${accessToken.expires}
//          Permissions: ${accessToken.permissions}
//          Declined permissions: ${accessToken.declinedPermissions}
//          ''');

//         var graphResponse = await http.get('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${accessToken.token}');
//         print(graphResponse);
//             _login();
//         // FacebookModel user = FacebookModel.fromJson(json.decode(graphResponse.body));

//         // var res = await AuthModel.socialLogin(accessToken.userId, Helper.customString('${user.email}', isEmail: true, isUsername: true), Helper.customString('${user.email}', isEmail: true), 'facebook');
//         // if (res) {
//         //   Goto.root('/main/home');
//         // }

//         break;
//       case FacebookLoginStatus.cancelledByUser:
//         break;
//       case FacebookLoginStatus.error:
//           print(result.errorMessage);
//         break;
//     }
//   }
//   void _signInFacebook() async {
// //    2407536649-ZFNGnaMhK7tCHBYL4rQ2SGT9nkuTbnL8g3aJCxq acc token
// //    Niz5D73o0BaUMZU4GHHGCTSpJmIoxmoPIITPeuOH46SMO acc token secret
//     FacebookLogin facebookLogin = FacebookLogin();

//     final  result = await facebookLogin.logIn(['email']);
//     final token = result.accessToken.token;
//     final graphResponse = await http.get(
//         'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name&access_token=${result.accessToken.token}');
//     print(graphResponse.body);
//     if (result.status == FacebookLoginStatus.loggedIn) {
//       final credential = FacebookAuthProvider.getCredential(accessToken: token);
//       FirebaseAuth.instance.signInWithCredential(credential);
//     }
//   }
// for google login

  Future<void> _googleSignUp() async {
    try {
      final GoogleSignIn _googleSignIn = GoogleSignIn(
        scopes: ['email'],
      );
      final FirebaseAuth _auth = FirebaseAuth.instance;

      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      final User user = (await _auth.signInWithCredential(credential)).user;

      bool res = await auth.loginEmail(socialId: user.uid, name: user.displayName);

      if (res) ExtendedNavigator.of(context).pop(true);
    } catch (e) {
      BotToast.showText(text: e.message);
    }
  }

  void _login() {
    Global.user = UserModel.createDummy();

    Navigator.push(
      context,
      CupertinoPageRoute(builder: (context) => LandingPage()),
    );

    // ExtendedNavigator.of(context).push('/');
  }

// //for face book login
//   signUpWithFacebook() async {
//     try {
//       var facebookLogin = new FacebookLogin();
//       var result = await facebookLogin.logIn(['email',]);

//       if (result.status == FacebookLoginStatus.loggedIn) {
//         final AuthCredential credential = FacebookAuthProvider.getCredential(
//           accessToken: result.accessToken.token,
//         );
//         final FirebaseUser user =
//             (await FirebaseAuth.instance.signInWithCredential(credential)).user;
//         print('signed in ' + user.displayName);
//         return user;
//       }
//     } catch (e) {
//       print(e.message);
//     }
//   }

  Future<void> verifyPhone(phoneNo, BuildContext context) async {
    final PhoneVerificationCompleted verified = (AuthCredential authResult) {
      AuthService().signIn(authResult, context);
      Fluttertoast.showToast(
          msg: "Verifying ...",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0);
      print(authResult);
    };

    final PhoneVerificationFailed verificationfailed = (FirebaseAuthException authException) {
      print('${authException.message}');
    };

    final PhoneCodeSent smsSent = (String verId, [int forceResend]) {
      this.verificationId = verId;
      setState(() {
        this.codeSent = true;
      });
    };

    final PhoneCodeAutoRetrievalTimeout autoTimeout = (String verId) {
      this.verificationId = verId;
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNo,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verified,
        verificationFailed: verificationfailed,
        codeSent: smsSent,
        codeAutoRetrievalTimeout: autoTimeout);
  }
}

// class AuthService {
//   //Handles Auth
//   handleAuth() {
//     return StreamBuilder(
//         stream: FirebaseAuth.instance.onAuthStateChanged,
//         builder: (BuildContext context, snapshot) {
//           if (snapshot.hasData) {
//             return DashboardPage();
//           } else {
//             return LoginPage();
//           }
//         });
//   }

//   //Sign out
//   signOut() {
//     FirebaseAuth.instance.signOut();
//   }

//   //SignIn
//   signIn(AuthCredential authCreds) {
//     FirebaseAuth.instance.signInWithCredential(authCreds);
//   }

//   signInWithOTP(smsCode, verId) {
//     AuthCredential authCreds = PhoneAuthProvider.getCredential(
//         verificationId: verId, smsCode: smsCode);
//     signIn(authCreds);
//   }
// }
// class LoginPage extends StatefulWidget {
//   @override
//   _LoginPageState createState() => _LoginPageState();
// }

// class _LoginPageState extends State<LoginPage> {
//   final formKey = new GlobalKey<FormState>();

//   String phoneNo, verificationId, smsCode;

//   bool codeSent = false;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Form(
//           key: formKey,
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Padding(
//                   padding: EdgeInsets.only(left: 25.0, right: 25.0),
//                   child: TextFormField(
//                     keyboardType: TextInputType.phone,
//                     decoration: InputDecoration(hintText: 'Enter phone number'),
//                     onChanged: (val) {
//                       setState(() {
//                         this.phoneNo = val;
//                       });
//                     },
//                   )),
//                   codeSent ? Padding(
//                   padding: EdgeInsets.only(left: 25.0, right: 25.0),
//                   child: TextFormField(
//                     keyboardType: TextInputType.phone,
//                     decoration: InputDecoration(hintText: 'Enter OTP'),
//                     onChanged: (val) {
//                       setState(() {
//                         this.smsCode = val;
//                       });
//                     },
//                   )) : Container(),
//               Padding(
//                   padding: EdgeInsets.only(left: 25.0, right: 25.0),
//                   child: RaisedButton(
//                       child: Center(child: codeSent ? Text('Login'):Text('Verify')),
//                       onPressed: () {
//                         codeSent ? AuthService().signInWithOTP(smsCode, verificationId):verifyPhone(phoneNo);
//                       }))
//             ],
//           )),
//     );
//   }

//   Future<void> verifyPhone(phoneNo) async {
//     final PhoneVerificationCompleted verified = (AuthCredential authResult) {
//       AuthService().signIn(authResult);
//     };

//     final PhoneVerificationFailed verificationfailed =
//         (AuthException authException) {
//       print('${authException.message}');
//     };

//     final PhoneCodeSent smsSent = (String verId, [int forceResend]) {
//       this.verificationId = verId;
//       setState(() {
//         this.codeSent = true;
//       });
//     };

//     final PhoneCodeAutoRetrievalTimeout autoTimeout = (String verId) {
//       this.verificationId = verId;
//     };

//     await FirebaseAuth.instance.verifyPhoneNumber(
//         phoneNumber: phoneNo,
//         timeout: const Duration(seconds: 5),
//         verificationCompleted: verified,
//         verificationFailed: verificationfailed,
//         codeSent: smsSent,
//         codeAutoRetrievalTimeout: autoTimeout);
//   }
// }
