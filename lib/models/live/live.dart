class Live {
  String userid;
  String name;
  String image;
  int channelId;
  bool me;
  String contentThumbnail;
  String title;
  int viewer;

  Live(
      {this.userid,
      this.name,
      this.me,
      this.image: 'assets/images/default-avatar.jpg',
      this.channelId,
      this.title,
      this.contentThumbnail,
      this.viewer});
}
