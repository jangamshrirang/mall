import 'package:flutter/material.dart';

import '../BrandProduts.dart';
import '../ProductExpantion.dart';

class VendoroHomePage extends StatefulWidget {
  _VendoroHomePageState createState() => new _VendoroHomePageState();
}

class _VendoroHomePageState extends State<VendoroHomePage> {
  final ScrollController _scrollController = new ScrollController();

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildListDelegate(List<Widget>.generate(5, (i) {
            return Column(
              children: [
                Wrap(
                  spacing: 0,
                  children: [
                    VendorHomePageProducts(),
                    VendorHomePageProducts(),
                    VendorHomePageProducts(),
                  ],
                ),
              ],
            );
          })),
        )
      ],
    );
  }
}

class VendorHomePageProducts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductExpantion(
                      pTitle: "Beats headpones",
                      price: 300,
                    )));
      },
      child: Container(
          width: size.width * 0.3,
          height: size.height * 0.2,
          margin: EdgeInsets.only(bottom: 10, left: 10, top: 5, right: 0),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 1,
                blurRadius: 3,
                offset: Offset(1, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.all(Radius.circular(0)),
            image: DecorationImage(
              image: NetworkImage(
                'https://i5.walmartimages.com/asr/6e1b1508-d1d8-46e5-93a3-8a35612c61b2_1.e0e3988fd0851d2e5025d935a8eee6f7.jpeg',
              ),
              fit: BoxFit.fill,
            ),
          ),
          child: Container(
              //text container
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8)),
              ),
              margin: EdgeInsets.only(
                top: size.height * 0.13,
              ),
              padding: EdgeInsets.only(
                  left: size.width * 0.02,
                  right: size.width * 0.01,
                  top: size.height * 0.01),
              height: size.height * 0.06,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Beats headpones",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.normal,
                        fontSize: size.height * 0.020),
                  ),
                  Text(
                    "QAR 300",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.normal,
                        color: Colors.black38,
                        fontSize: size.height * 0.016),
                  ),
                ],
              ))),
    );
  }
}
