import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/RGetCartModel.dart';
import 'package:wblue_customer/models/Restaurant/cart.dart';
import 'package:wblue_customer/models/auth.dart';
import 'package:wblue_customer/pages/PaymentOption/Payment.dart';
import 'package:wblue_customer/pages/booking/ConformatinScreen.dart';
import 'package:wblue_customer/pages/market/home/Services/serviceCarosel.dart';
import 'package:wblue_customer/pages/restaurant/cart/checkout/main.dart';
import 'package:wblue_customer/providers/restaurant/R_GetCartHelpr.dart';
import 'package:wblue_customer/providers/restaurant/R_remove_item.dart';
import 'package:wblue_customer/providers/restaurant/cartActions.dart';
import 'package:wblue_customer/providers/restaurant/onlyPostApis.dart';
import 'package:wblue_customer/providers/socket/client.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_loading.dart';

import 'CartIncrement.dart';

class RestaurantCartPage extends StatefulWidget {
  String fromScan, preOrder, delivery, restId, filterkey, appBarTitle, fromTakeAway, reservationHashID, fromMenu;
  String coverImage, restimage, restName, onlybooking, restSlogan;
  var totlaPrice;

  RestaurantCartPage(
      {this.fromScan,
      this.preOrder,
      this.delivery,
      this.totlaPrice,
      this.coverImage,
      this.restimage,
      this.restName,
      this.restId,
      this.restSlogan,
      this.onlybooking,
      this.filterkey,
      this.fromTakeAway,
      this.appBarTitle,
      this.fromMenu,
      this.reservationHashID});
  @override
  _RestaurantCartPageState createState() => _RestaurantCartPageState();
}

class _RestaurantCartPageState extends State<RestaurantCartPage> {
  // String _money(int amount) {
  //   return Global.money(amount);
  // }

  @override
  void initState() {
    Future.microtask(() => context.read<RGetCartHelper>().fetchRCartList());
    super.initState();
  }

  int qty;
  var cartID;
  int navigatorPopCount = 0;

  @override
  Widget build(BuildContext context) {
    Map<String, Cart> cart = Global.cart;
    double grandTotal = 0;

    _dineOut() async {
      await showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('DINE-OUT'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Your food will be prepared within 15-20 minutes. How to you want to take your food?',
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 16.0),
              ),
            ],
          ),
          actions: [
            FlatButton(
              child: Text('Pick Up', style: TextStyle(color: Config.primaryColor)),
              onPressed: () {
                Navigator.pop(context);
                if (Global.user == null) {
                  ExtendedNavigator.of(context).root.push('/login');
                } else {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Payment(
                                reservationHashId: widget.reservationHashID,
                                restaurentHashId: widget.restId,
                                cartId: cartID,
                                from: "rest",
                                orderType: 3,
                                lat: 122.32323,
                                lang: 0.321323,
                              )));
                  // ExtendedNavigator.of(context)
                  //     .root
                  //     .push('/restaurants/pickup-confirmation');
                }
              },
            ),
            FlatButton(
              child: Text('Deliver', style: TextStyle(color: Config.primaryColor)),
              onPressed: () {
                Navigator.pop(context);
                if (Global.user == null) {
                  ExtendedNavigator.of(context).root.push('/login');
                } else {
                  // stateManagment.setLocationAreaName(null);
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => RestaurantPlaceOrderPage(cartId: cartID)));
                }
              },
            ),
          ],
        ),
      );
    }

    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Config.primaryColor,
        title: Text("Cart"),
        centerTitle: true,
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              // widget.restId == null
              //     ?
              Navigator.of(context).pop(true);
              // : Navigator.pushReplacement(
              //     context,
              //     MaterialPageRoute(
              //       builder: (context) => MenuPage(
              //         filterKey: widget.filterkey,
              //         fromTakeAway: widget.fromTakeAway,
              //         appBarTitle: widget.appBarTitle,
              //         restId: widget.restId,
              //         restSlogan: widget.restSlogan,
              //         coverImage: widget.coverImage,
              //         restName: widget.restName,
              //         restimage: widget.restimage,
              //         delivery: widget.delivery,
              //         fromScan: widget.fromScan,
              //         preOrder: widget.preOrder,
              //       ),
              //     ),
              //   );
            }),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete_sweep),
            onPressed: () async {
              await showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  title: Text('CONFIRMATION'),
                  content: Text(
                    'Are you sure you want to clear your cart?',
                    textAlign: TextAlign.left,
                  ),
                  actions: [
                    FlatButton(
                      child: Text('Confirm'),
                      onPressed: () {
                        Navigator.of(context).popUntil((_) => navigatorPopCount++ >= 2);
                        setState(() {
                          Global.cart = {};
                          fetchRestaurantClearCart();
                          fetchTotlaPrice(cartID);

                          // widget.fromMenu == "FromMenu"
                          //     ? Navigator.pushReplacement(
                          //         context,
                          //         MaterialPageRoute(
                          //           builder: (context) => MenuPage(
                          //             filterKey: widget.filterkey,
                          //             fromTakeAway: widget.fromTakeAway,
                          //             appBarTitle: widget.appBarTitle,
                          //             restId: widget.restId,
                          //             restSlogan: widget.restSlogan,
                          //             coverImage: widget.coverImage,
                          //             restName: widget.restName,
                          //             restimage: widget.restimage,
                          //             delivery: widget.delivery,
                          //             fromScan: widget.fromScan,
                          //             preOrder: widget.preOrder,
                          //           ),
                          //         ),
                          //       )
                          //     :
                        });
                      },
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
      body: RefreshIndicator(
          onRefresh: () async {
            setState(() {
              print('refreshing...');
            });
          },
          child:
              // cart.length >= 0
              //     ?
              Stack(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 14.0),
                child: Consumer<RGetCartHelper>(
                    builder: (context, restaurant, child) => restaurant.rFoodCart.length > 0
                        ? ListView(
                            children: List.generate(restaurant.rFoodCart.length, (index) {
                            RCartListModel rCartListModel = RCartListModel.fromJson(restaurant.rFoodCart[index]);
                            print(rCartListModel.totalInfo);
                            cartID = rCartListModel.cartNo;
                            fetchTotlaPrice(rCartListModel.cartNo);
                            return Wrap(
                              children: List.generate(rCartListModel.food.length, (index) {
                                // fetchTotlaPrice(rCartListModel.cartNo);
                                final item = rCartListModel.food[index];

                                return Dismissible(
                                  key: UniqueKey(),
                                  onDismissed: (direction) {
                                    fetchTotlaPrice(rCartListModel.cartNo);
                                    print(stateManagment.totalPrice);
                                    fetchRestaurantRemoveItemsCart(
                                        rCartListModel.food[index].foodInfo.hashId, rCartListModel.cartNo);

                                    rCartListModel.food.removeAt(index);
                                    Scaffold.of(context)
                                        .showSnackBar(SnackBar(content: Text("${item.foodInfo.name} dismissed")));
                                  },
                                  background: Container(color: Colors.red),
                                  child: ListTile(
                                    leading: Image.network(rCartListModel.food[index].foodInfo.thumbnail,
                                        height: size.height * 0.2),
                                    title: Text(
                                      rCartListModel.food[index].foodInfo.name,
                                      style: TextStyle(color: Colors.black, fontSize: size.height * 0.03),
                                    ),
                                    subtitle: Text('${rCartListModel.food[index].foodInfo.description}',
                                        maxLines: 3,
                                        style: TextStyle(color: Colors.black45, fontSize: size.height * 0.02)),
                                    trailing: SingleChildScrollView(
                                      physics: NeverScrollableScrollPhysics(),
                                      child: Column(
                                        children: [
                                          Container(
                                            // color: Colors.red,
                                            height: size.height * 0.07,
                                            width: size.width * 0.3,
                                            child: CartQuaintityIncreament(
                                              cartID: rCartListModel.cartNo,
                                              productId: rCartListModel.food[index].foodInfo.hashId,
                                              price: rCartListModel.food[index].foodInfo.price,
                                              count: rCartListModel.food[index].quantity,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              }),
                            );
                          }))
                        : restaurant.rFoodCart.length == 0
                            ? Container(
                                child: Center(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Icon(
                                        Icons.shopping_basket,
                                        size: 40.0,
                                        color: Colors.grey,
                                      ),
                                      Text('Your cart is empty!'),
                                    ],
                                  ),
                                ),
                              )
                            : WLoadingWidget()),
              ),
            ],
          )),
      bottomNavigationBar: Container(
        margin: EdgeInsets.only(bottom: 10),
        child: Wrap(
          children: <Widget>[
            widget.fromScan == "yes"
                ? Container(
                    margin: EdgeInsets.only(left: size.width * 0.3),
                    width: size.width * 0.4,
                    child: button("Place Order", () {
                      Fluttertoast.showToast(
                          msg: "Order placed ",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.green,
                          textColor: Colors.white,
                          fontSize: 16.0);
                    }, Config.primaryColor),
                  )
                : Container(),
            Divider(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('GRAND TOTAL', style: TextStyle(fontWeight: FontWeight.bold)),
                  Text('${stateManagment.totalPrice}', style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            //this is a check box for private transcation
            Exercise(
              title: "Do you want to make private transcation!",
            ),
            Divider(),
            widget.fromScan != "yes" || widget.preOrder != "yes"
                ? widget.fromScan == "yes"
                    ? btnsForPreBoking()
                    : Row(
                        children: <Widget>[
                          widget.delivery != "yes" || widget.delivery == null
                              ? Expanded(
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                                    child: WButtonWidget(
                                      iconData: widget.preOrder != "yes" ? Icons.restaurant : null,
                                      title: widget.preOrder != "yes" ? 'dine in' : "Confirm",
                                      onPressed: () {
                                        if (Global.user == null) {
                                          ExtendedNavigator.of(context).root.push('/login');
                                        }
                                        if (cartID == "" || cartID == null) {
                                          Fluttertoast.showToast(
                                            msg: "Cart is empty",
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.CENTER,
                                          );
                                        } else {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => widget.preOrder != "yes"
                                                    ? ConfirmationScreen(
                                                        //
                                                        tableNumber: "${stateManagment.tableNumber}",
                                                        date: "${stateManagment.date}",
                                                        time: "${stateManagment.time}",
                                                        guest: "${stateManagment.guestNumber}",
                                                      )
                                                    : Payment(
                                                        orderType: 1,
                                                        from: "rest",
                                                        preOrder: widget.preOrder,
                                                        cartId: cartID,
                                                      )),
                                            // ExtendedNavigator.of(context).root.push('/book-table');
                                          );
                                        }
                                      },
                                    ),
                                  ),
                                )
                              : Container(
                                  width: 0.01,
                                ),
                          widget.preOrder != "yes" || stateManagment.deliverOrTakeString == "yes"
                              ? Expanded(
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                                    child: WButtonWidget(
                                      iconData: Icons.markunread_mailbox,
                                      title: 'dine out',
                                      onPressed: () {
                                        if (Global.user == null) {
                                          ExtendedNavigator.of(context).root.push('/login');
                                        }
                                        if (cartID == "" || cartID == null) {
                                          Fluttertoast.showToast(
                                            msg: "Cart is empty",
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.CENTER,
                                          );
                                        } else {
                                          // _dineOut();
                                          widget.fromTakeAway == "no"
                                              ? Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) => RestaurantPlaceOrderPage(cartId: cartID)))
                                              : Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) => Payment(
                                                            reservationHashId: widget.reservationHashID,
                                                            restaurentHashId: widget.restId,
                                                            cartId: cartID,
                                                            from: "rest",
                                                            orderType: 3,
                                                            lat: 122.32323,
                                                            lang: 0.321323,
                                                          )));
                                        }
                                      },
                                    ),
                                  ),
                                )
                              : Container(width: 0.01)
                        ],
                      )
                : btnsForPreBoking()
          ],
        ),
      ),
    );
  }

  button(String title, Function onTap, Color color) {
    return RaisedButton(
        child: Text(
          title,
          style: TextStyle(color: Colors.white),
        ),
        color: color,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
        onPressed: onTap);
  }

  btnsForPreBoking() {
    return widget.preOrder != "yes" || widget.preOrder == null
        ? SafeArea(
            top: false,
            child: Row(
              children: [
                Spacer(),
                button("Add food", () {
                  Navigator.pop(context);
                }, Config.primaryColor),
                Spacer(),
                button("Ping", () {
                  AuthModel authModel = AuthModel.fromJson(Helper.currentAuthUser.get('user'));

                  Map<String, dynamic> _data = {
                    'notif_id': authModel.userId,
                    'hashid': '${widget.restId}',
                    'user_hashid': authModel.hashid,
                    'table_no': '5',
                  };

                  print('_data: $_data');

                  context.read<PSocketClient>().emitter(eventName: WEvents.pingRestaurantStaff, data: _data);

                  Fluttertoast.showToast(
                    msg: "Waiter is on the way",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                  );
                }, Config.primaryColor),
                Spacer(),
                button("Check Out", () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Payment(
                        reservationHashId: widget.reservationHashID,
                        restaurentHashId: widget.restId,
                        orderType: 3,
                        lat: 122.32323,
                        lang: 0.321323,
                        cartId: cartID,
                        from: "rest",
                        fromScan: "yes",
                      ),
                    ),
                  );
                }, Config.primaryColor),
                Spacer(),
              ],
            ),
          )
        : button("Confirm", () {
            Navigator.pop(context);
          }, Config.primaryColor);
  }
}
