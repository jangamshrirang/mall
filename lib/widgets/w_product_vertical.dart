import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/widgets/w_image.dart';

class WProductWidget extends StatelessWidget {
  final String productName;
  final ImageProvider productImage;
  final Function onPicTap;
  final BoxFit fit;

  WProductWidget({
    this.productName,
    this.fit: BoxFit.cover,
    this.productImage,
    this.onPicTap: function,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onPicTap(),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10.0),
              bottomRight: Radius.circular(5.0),
              bottomLeft: Radius.circular(5.0),
              topRight: Radius.circular(10.0)),
          color: Colors.black.withOpacity(0.1),
        ),
        child: Column(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              width: 80,
              child: Padding(
                padding: EdgeInsets.all(5.0),
                child: AutoSizeText(
                  '$productName',
                  style: TextStyle(color: Colors.grey[800]),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(1),
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5.0),
                    child: Container(
                      height: 80,
                      width: 80,
                      child: WImageWidget(
                        fit: fit,
                        placeholder: productImage,
                      ),
                    ),
                  ),
                  Positioned(
                    right: 0,
                    top: -7,
                    child: WImageWidget(
                      placeholder: AssetImage(
                          'assets/images/products/featured-banner.png'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

void function() {}
