import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/pages/market/home/Services/serviceCarosel.dart';

import '../PaymentOption/Payment.dart';

class ActivityCart extends StatefulWidget {
  @override
  _ActivityCartState createState() => _ActivityCartState();
}

class _ActivityCartState extends State<ActivityCart> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      appBar: appBarWithOutIcon(context, "Your Activities"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Colors.white,
              height: size.height * 0.75,
              child: ListView(
                children: [
                  productCard("Sand Dunes 4x4",
                      "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ", "1,200"),
                  productCard("Sand Dunes 4x4",
                      "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ", "1,200"),
                  productCard("Sand Dunes 4x4",
                      "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ", "1,200"),
                ],
              ),
            ),
            Stack(
              children: [
                Container(
                  //color: Colors.blue,
                  margin: EdgeInsets.only(left: 10, right: 10),
                  height: size.height * 0.1,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        children: [
                          text("Total Amount", size.width * 0.6, size.height * 0.025, size.height * 0.023,
                              FontWeight.normal, 1, Colors.black),
                          SizedBox(height: size.height * 0.005),
                          text("QAR 1,200", size.width * 0.6, size.height * 0.05, size.height * 0.04, FontWeight.bold,
                              1, Color(0xff284E99)),
                        ],
                      ),
                      Spacer(),
                      RaisedButton(
                          child: Text(
                            "Proceed",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Config.primaryColor,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Payment(
                                          from: "activities",
                                        ))); //ActivityPayment
                          })
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: size.height * 0.05),
                  child: Exercise(
                    title: "Do you want to make private transcation!",
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  productCard(String title, desc, price) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {},
      child: Stack(
        children: [
          Container(
              width: size.width * 0.9,
              height: size.height * 0.31,
              margin: EdgeInsets.only(bottom: 10, left: 20, top: 5, right: 5),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    spreadRadius: 1,
                    blurRadius: 3,
                    offset: Offset(1, 3), // changes position of shadow
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Container(
                  //text container
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  padding: EdgeInsets.only(left: size.width * 0.02, right: size.width * 0.01, top: size.height * 0.01),
                  height: size.height * 0.06,
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: size.height * 0.12,
                            width: size.width * 0.25,
                            decoration: BoxDecoration(
                              color: Colors.transparent,
                              image: DecorationImage(
                                  image: AssetImage(
                                    'assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg',
                                  ),
                                  fit: BoxFit.fill),
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              text(title, size.width * 0.4, size.height * 0.024, size.height * 0.023, FontWeight.bold,
                                  1, Colors.black),
                              text(desc, size.width * 0.45, size.height * 0.06, size.height * 0.020, FontWeight.normal,
                                  2, Colors.black54),
                              text("QAR " + price, size.width * 0.4, size.height * 0.02, size.height * 0.020,
                                  FontWeight.bold, 1, Colors.red),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: size.height * 0.005),
                      Row(
                        children: [
                          text("Sep 16 2020", size.width * 0.4, size.height * 0.02, size.height * 0.02, FontWeight.bold,
                              1, Colors.black),
                          text("5.15 PM", size.width * 0.4, size.height * 0.02, size.height * 0.02, FontWeight.bold, 1,
                              Colors.black),
                        ],
                      ),
                      SizedBox(height: size.height * 0.005),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 4),
                            child: Icon(
                              Icons.location_on,
                              size: 12,
                            ),
                          ),
                          Column(
                            children: [
                              text("Pick up & drop", size.width * 0.6, size.height * 0.026, size.height * 0.022,
                                  FontWeight.bold, 1, Colors.black),
                              text(desc, size.width * 0.6, size.height * 0.045, size.height * 0.018, FontWeight.normal,
                                  2, Colors.black54),
                            ],
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 3),
                            child: Icon(
                              Icons.phone_android,
                              size: 12,
                            ),
                          ),
                          text("5555 5555", size.width * 0.45, size.height * 0.02, size.height * 0.018,
                              FontWeight.normal, 1, Colors.black54),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 3),
                            child: Icon(
                              Icons.email,
                              size: 12,
                            ),
                          ),
                          text("abc@gmail.com", size.width * 0.45, size.height * 0.023, size.height * 0.018,
                              FontWeight.normal, 1, Colors.black54),
                        ],
                      )
                    ],
                  ))),
          Positioned(
              right: size.width * 0.1,
              bottom: size.height * 0.18,
              child: IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  print("delete");
                },
                iconSize: 15,
              )),
          Positioned(
              right: size.width * 0.03,
              bottom: size.height * 0.18,
              child: IconButton(
                icon: Icon(Icons.edit),
                onPressed: () {
                  print("edit");
                },
                iconSize: 15,
              ))
        ],
      ),
    );
  }

  text(String title, double widthh, heightt, textSize, FontWeight fontWeight, int lines, Color color) {
    var size = MediaQuery.of(context).size;

    return Container(
      //    color: Colors.red,
      margin: EdgeInsets.only(left: 5, top: 2),
      height: heightt,
      width: widthh,
      child: Text(
        title,
        maxLines: lines,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(color: color, fontFamily: 'Montserrat', fontWeight: fontWeight, fontSize: textSize),
      ),
    );
  }
}
