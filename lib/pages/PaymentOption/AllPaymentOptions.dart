import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/Activities/ActivitySucces.dart';
import 'package:wblue_customer/pages/restaurant/cart/d8_soon_pickup_confirmation.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'package:wblue_customer/widgets/w_blue_background.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

Widget cod(BuildContext context, var paymentMethod, lat, lang, orderType, cartId, from, fromScan, reservationHashID,
    restaurentHashID) {
  return Column(
    children: [
      SizedBox(height: 50),
      Text(
        "Amount will be collected at your location.",
        style: TextStyle(
          fontSize: 20,
        ),
      ),
      Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: WRoundedButton(
            onCustomButtonPressed: () {
              fromScan == "yes"
                  ? dineInChekOut(cartId, reservationHashID, restaurentHashID, 1)
                  : placeORder(2, paymentMethod, lat, lang, orderType, "any", cartId);

              Navigator.push(
                  context,
                  MaterialPageRoute(
                      //PickupConfirmationPage
                      builder: (context) => orderType == 2 || stateManagment.fromReservation == true
                          ? ActivityBookingSuccess(from: from)
                          : PickupConfirmationPage()));
            },
            child: AutoSizeText('Done'),
            borderColor: Config.primaryColor,
            btnColor: Config.primaryColor,
            labelColor: Colors.white,
          ),
        ),
      ),
    ],
  );
}

Widget wallet(BuildContext context) {
  var size = MediaQuery.of(context).size;
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      WContainerBlue(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  AutoSizeText(
                    'Available Balance (QAR)',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: 20.0),
                  AutoSizeText(
                    '0.00',
                    style: TextStyle(
                      fontSize: 38,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: 40),
                  Row(
                    children: <Widget>[
                      AutoSizeText(
                        'Reabate (QAR) ',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                      AutoSizeText(
                        '0.00',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.amber,
                        ),
                      ),
                      Icon(MdiIcons.chevronRight, color: Colors.amber)
                    ],
                  ),
                ],
              ),
              WRoundedButton(
                onCustomButtonPressed: () {
                  // walletScreenNavigation(context, TopUpScreen());
                }, //
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Text('Top Up'.toUpperCase(), style: TextStyle(fontSize: 14)),
                ),
              )
            ],
          ),
        ),
      ),
      Container(width: size.width * 0.9, child: field('Enter Amount')),
      Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: WRoundedButton(
            onCustomButtonPressed: () {
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) => ActivityBookingSuccess()));
            },
            child: AutoSizeText('Pay'),
            borderColor: Config.primaryColor,
            btnColor: Config.primaryColor,
            labelColor: Colors.white,
          ),
        ),
      ),
    ],
  );
}

Widget field(String type, {String hint}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      AutoSizeText(
        '* $type',
        style: TextStyle(fontSize: 16.0, color: Colors.red),
      ),
      SizedBox(
        height: 10,
      ),
      TextField(
          decoration: new InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Config.primaryColor, width: 0.5),
        ),
        contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 0.5),
        ),
        hintText: hint ?? type,
      ))
    ],
  );
}

creditCard(BuildContext context, String preOrder, var paymentMethod, lat, lang, orderType, cartId, fromScan,
    reservationHashID, restaurentHashId) {
  return Container(
    child: Column(
      children: [
        Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                field('Card Number'),
                SizedBox(height: 10),
                field('Name on card'),
                SizedBox(height: 10),
                Row(
                  children: [
                    Expanded(
                      child: field('Expiration Date', hint: 'MM/YY'),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: field('CVV'),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              children: [
                Icon(
                  MdiIcons.checkboxMarked,
                  color: Config.primaryColor,
                ),
                SizedBox(width: 10.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AutoSizeText('Save Card'),
                    SizedBox(height: 5.0),
                    AutoSizeText('Information is encrypted and securely stored.', style: TextStyle(color: Colors.grey))
                  ],
                ),
              ],
            ),
          ),
        ),
        Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: WRoundedButton(
              onCustomButtonPressed: () {
                fromScan == "yes"
                    ? dineInChekOut(cartId, reservationHashID, restaurentHashId, 2)
                    : placeORder(2, 2, lat, lang, orderType, "any", cartId);

                Navigator.push(context, MaterialPageRoute(builder: (context) => ActivityBookingSuccess()));
              },
              child: AutoSizeText('Pay Now'),
              borderColor: Config.primaryColor,
              btnColor: Config.primaryColor,
              labelColor: Colors.white,
            ),
          ),
        )
      ],
    ),
  );
}

text(
  BuildContext context,
  String text,
  FontWeight fontWeight,
) {
  var size = MediaQuery.of(context).size;
  return Container(
      width: size.width * 0.8,
      child: Text(text, textAlign: TextAlign.start, style: TextStyle(fontSize: 18, fontWeight: fontWeight)));
}

DioClient _dioClient = DioClient();
Future placeORder(var shippingRate, paymentMethod, lat, lang, orderType, note, cartId) async {
  Map<String, dynamic> _body = {
    'shipping_rate': 0,
    'payment_method_id': paymentMethod,
    'shipping_address': 'qatar',
    'shipping_address_latitude': lat,
    'shipping_address_longitude': lang,

    // stateManagment.tableNumber == null ? 0 : stateManagment.tableNumber,
    'time_arrive': stateManagment.time == null ? "any" : stateManagment.time,
    'date_arrive': stateManagment.date == null ? "any" : stateManagment.date,
    'guest': stateManagment.guestNumber == null ? "any" : stateManagment.guestNumber,
    'note': "any",
    'has_booking': orderType != 1 ? 0 : 1
  };
  APIResponse res = await _dioClient.privatePost('/restaurant/order/place', data: _body);
  if (res.code >= 400) {
    chekOut(cartId);
    return [];
  }
  print("data from place order api data :${res.data} & message : ${res.message}");
  return res.data;
}

Future chekOut(var cartId) async {
  Map<String, dynamic> _body = {'cart_no': cartId};
  APIResponse res = await _dioClient.privatePost('/restaurant/cart/checkout', data: _body);
  if (res.code >= 400) {
    return [];
  }

  return res.data;
}

Future dineInChekOut(var cartId, reservationHashID, restaurentHashId, paymentMethod) async {
  Map<String, dynamic> _body = {
    'cart_hash_id': cartId,
    'restaurant_hash_id': restaurentHashId,
    'reservation_hash_id': reservationHashID,
    'payment_method_id': paymentMethod
  };
  APIResponse res = await _dioClient.privatePost('/restaurant/table/reservation/checkout', data: _body);
  if (res.code >= 400) {
    return [];
  }
  print("Dine In Check Out result :" + res.message);
  return res.data;
}
