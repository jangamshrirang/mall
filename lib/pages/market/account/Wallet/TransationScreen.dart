import 'package:expansion_card/expansion_card.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';

import 'AppbarForAllWalwtScreens.dart';

class TransationScreen extends StatefulWidget {
  @override
  _TransationScreenState createState() => _TransationScreenState();
}

class _TransationScreenState extends State<TransationScreen> {
  List<String> _locations = [
    'All Transactions',
    'Purchase',
    'Cash In',
    'Withdrawal',
    'Refund',
    'Rebate',
    'Wmall Partner'
  ]; // Option 2
  String _selectedLocation;
  String date;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "Recent Transaction"),
      body: Container(
        width: size.width,
        color: Colors.white,
        child: Column(
          children: [
            Row(
              children: [
                Spacer(),
                DropdownButton(
                  underline: SizedBox(),
                  hint: Text('Type'), // Not necessary for Option 1
                  value: _selectedLocation,
                  onChanged: (newValue) {
                    setState(() {
                      _selectedLocation = newValue;
                    });
                  },
                  items: _locations.map((location) {
                    return DropdownMenuItem(
                      child: new Text(location),
                      value: location,
                    );
                  }).toList(),
                ),
                Spacer(),
                Container(
                  width: size.width * 0.3,
                  child: FlatButton(
                      onPressed: () async {
                        final choice = await showDatePicker(
                          
                            context: context,
                            firstDate: DateTime(2010),
                            lastDate: DateTime(2030),
                            initialDate: DateTime.now(),
                            builder: (BuildContext context, Widget child) {
                              return Center(
                                  child: SizedBox(
                                width: 400.0,
                                height: 500.0,
                                child: child,
                              ));
                            });
                        if (choice != null) {
                          setState(() => date = "$choice");
                        }

                        print(date);
                      },
                      child: Text(
                        date == "" || date == null ? "Month" : date,
                        maxLines: 1,
                      )),
                ),
                Spacer(),
              ],
            ),
            SizedBox(
              height: 50,
            ),
            Image.asset(
              "assets/images/nopayment.png",
              height: 100,
            ),
            SizedBox(
              height: 50,
            ),
            Text(
              "No Record Found",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.normal),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    );
  }
}
