// // Copyright 2019 The Chromium Authors. All rights reserved.
// // Use of this source code is governed by a BSD-style license that can be
// // found in the LICENSE file.

// // ignore_for_file: public_member_api_docs

// import 'dart:async';
// import 'dart:io';

// import 'package:auto_route/auto_route.dart';
// import 'package:camera/camera.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:provider/provider.dart';
// import 'package:tflite/tflite.dart';
// import 'package:wblue_customer/main.dart';
// import 'package:wblue_customer/providers/market/p_camera.dart';

// class MarketCameraPage extends StatefulWidget {
//   @override
//   _MarketCameraPageState createState() {
//     return _MarketCameraPageState();
//   }
// }

// /// Returns a suitable camera icon for [direction].
// IconData getCameraLensIcon(CameraLensDirection direction) {
//   switch (direction) {
//     case CameraLensDirection.back:
//       return Icons.camera_rear;
//     case CameraLensDirection.front:
//       return Icons.camera_front;
//     case CameraLensDirection.external:
//       return Icons.camera;
//   }

//   throw ArgumentError('Unknown lens direction');
// }

// class _MarketCameraPageState extends State<MarketCameraPage>
//     with WidgetsBindingObserver {
//   double deviceRatio;
//   Size size;
//   CameraController controller;
//   double _imageHeight;
//   double _imageWidth;

//   @override
//   void initState() {
//     SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);

//     super.initState();
//     WidgetsBinding.instance.addObserver(this);
//     onNewCameraSelected(cameras[0]);
//   }

//   @override
//   void dispose() {
//     SystemChrome.setEnabledSystemUIOverlays(
//         [SystemUiOverlay.top, SystemUiOverlay.bottom]);
//     WidgetsBinding.instance.removeObserver(this);
//     super.dispose();
//   }

//   @override
//   void didChangeAppLifecycleState(AppLifecycleState state) {
//     // App state changed before we got the chance to initialize.
//     if (controller == null || !controller.value.isInitialized) {
//       return;
//     }

//     if (state == AppLifecycleState.inactive) {
//       controller?.dispose();
//     } else if (state == AppLifecycleState.resumed) {
//       if (controller != null) {
//         onNewCameraSelected(controller.description);
//       }
//     }
//   }

//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

//   Future yolov2Tiny(File image) async {
//     int startTime = new DateTime.now().millisecondsSinceEpoch;
//     var recognitions = await Tflite.detectObjectOnImage(
//       path: image.path,
//       model: "YOLO",
//       threshold: 0.3,
//       imageMean: 0.0,
//       imageStd: 255.0,
//       numResultsPerClass: 1,
//     );
//     // var imageBytes = (await rootBundle.load(image.path)).buffer;
//     // img.Image oriImage = img.decodeJpg(imageBytes.asUint8List());
//     // img.Image resizedImage = img.copyResize(oriImage, 416, 416);
//     // var recognitions = await Tflite.detectObjectOnBinary(
//     //   binary: imageToByteListFloat32(resizedImage, 416, 0.0, 255.0),
//     //   model: "YOLO",
//     //   threshold: 0.3,
//     //   numResultsPerClass: 1,
//     // );

//     context.read<PCamera>().setRecognitions(recognitions);
//     print(recognitions);

//     int endTime = new DateTime.now().millisecondsSinceEpoch;
//     print("Inference took ${endTime - startTime}ms");
//   }

//   List<Widget> renderBoxes(Size screen, List _recognitions) {
//     print(_recognitions);

//     if (_recognitions == null) return [];
//     if (_imageHeight == null || _imageWidth == null) return [];

//     double factorX = screen.width;
//     double factorY = _imageHeight / _imageWidth * screen.width;
//     Color blue = Color.fromRGBO(37, 213, 253, 1.0);

//     return _recognitions.map((re) {
//       return Positioned(
//         left: re["rect"]["x"] * factorX,
//         top: re["rect"]["y"] * factorY,
//         width: re["rect"]["w"] * factorX,
//         height: re["rect"]["h"] * factorY,
//         child: Container(
//           decoration: BoxDecoration(
//             borderRadius: BorderRadius.all(Radius.circular(8.0)),
//             border: Border.all(
//               color: blue,
//               width: 2,
//             ),
//           ),
//           child: Text(
//             "${re["detectedClass"]} ${(re["confidenceInClass"] * 100).toStringAsFixed(0)}%",
//             style: TextStyle(
//               background: Paint()..color = blue,
//               color: Colors.white,
//               fontSize: 12.0,
//             ),
//           ),
//         ),
//       );
//     }).toList();
//   }

//   @override
//   Widget build(BuildContext context) {
//     size = MediaQuery.of(context).size;
//     deviceRatio = size.width / size.height;
//     List<Widget> stackChildren = [];

//     return Scaffold(
//       backgroundColor: Colors.black,
//       key: _scaffoldKey,
//       body: Stack(
//         alignment: FractionalOffset.center,
//         children: <Widget>[
//           _cameraPreviewWidget(),
//           Positioned(
//             top: 0,
//             left: 0,
//             child: SafeArea(
//               child: Container(
//                 padding: const EdgeInsets.symmetric(horizontal: 8.0),
//                 width: size.width,
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     IconButton(
//                       color: Colors.white,
//                       onPressed: () => ExtendedNavigator.of(context).pop(),
//                       icon: Icon(MdiIcons.arrowLeft),
//                     ),
//                     IconButton(
//                       color: Colors.white,
//                       onPressed: _toggleCameraLens,
//                       icon: Icon(MdiIcons.cameraSwitchOutline),
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//           ),
//           Positioned(
//             bottom: 0,
//             child: Consumer<PCamera>(
//               builder: (_, camera, __) => Container(
//                 height: 120,
//                 width: size.width,
//                 color: Colors.black38,
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Container(
//                       width: 130,
//                       height: 70,
//                       child: Stack(
//                         alignment: FractionalOffset.center,
//                         children: [
//                           Container(
//                             width: 100,
//                             height: 40,
//                             decoration: BoxDecoration(
//                               borderRadius: BorderRadius.circular(30),
//                               color: Colors.white12,
//                             ),
//                           ),
//                           // QR CIRCLE
//                           Positioned(
//                             left: camera.scannerType == 'qr' ? 12 : 18, // 18
//                             child: GestureDetector(
//                               onTap: () => camera.setScanner('qr'),
//                               child: Container(
//                                 decoration: BoxDecoration(
//                                     color: camera.scannerType == 'qr'
//                                         ? Colors.red
//                                         : Colors.grey,
//                                     borderRadius:
//                                         BorderRadius.all(Radius.circular(30))),
//                                 height:
//                                     camera.scannerType == 'qr' ? 50 : 32, // 32
//                                 width:
//                                     camera.scannerType == 'qr' ? 50 : 32, // 32
//                                 child: Icon(
//                                   MdiIcons.qrcodeScan,
//                                   color: Colors.white,
//                                   size: camera.scannerType == 'qr'
//                                       ? 24.0
//                                       : 18, // 18
//                                 ),
//                               ),
//                             ),
//                           ),
//                           // CAMERA CIRCLE
//                           Positioned(
//                             right:
//                                 camera.scannerType == 'camera' ? 10 : 14, //14
//                             child: GestureDetector(
//                               onTap: () {
//                                 if (controller != null &&
//                                     controller.value.isInitialized &&
//                                     camera.scannerType == 'camera') {
//                                   onTakePictureButtonPressed();
//                                 }
//                                 camera.setScanner('camera');
//                               },
//                               child: Stack(
//                                 alignment: FractionalOffset.center,
//                                 children: [
//                                   Container(
//                                     height: camera.scannerType == 'camera'
//                                         ? 40.0
//                                         : 34, //34
//                                     width: camera.scannerType == 'camera'
//                                         ? 40.0
//                                         : 34, //34
//                                     decoration: BoxDecoration(
//                                         color: Colors.white,
//                                         borderRadius: BorderRadius.all(
//                                             Radius.circular(30))),
//                                   ),
//                                   Icon(
//                                     MdiIcons.circleSlice8,
//                                     color: camera.scannerType == 'camera'
//                                         ? Colors.red
//                                         : Colors.grey,
//                                     size: camera.scannerType == 'camera'
//                                         ? 60.0
//                                         : 42, // 42
//                                   ),
//                                 ],
//                               ),
//                             ),
//                           ),

//                           // Positioned(
//                           //   left: 15,
//                           //   top: 11,
//                           //   child: Container(
//                           //     height: 60.0,
//                           //     width: 60.0,
//                           //     decoration: BoxDecoration(
//                           //         color: Colors.amber,
//                           //         borderRadius:
//                           //             BorderRadius.all(Radius.circular(30))),
//                           //     child: Icon(
//                           //       MdiIcons.qrcodeScan,
//                           //       color: Colors.white,
//                           //       size: 18.0,
//                           //     ),
//                           //   ),
//                           // ),
//                         ],
//                       ),
//                     ),
//                     // Container(
//                     //   width: 1201,
//                     //   child: Row(
//                     //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     //     children: [
//                     //       Text(
//                     //         'QR Scan',
//                     //         style: TextStyle(color: Colors.white, fontSize: 11.0),
//                     //       ),
//                     //       Text(
//                     //         'Camera',
//                     //         style: TextStyle(color: Colors.white, fontSize: 14.0),
//                     //       ),
//                     //     ],
//                     //   ),
//                     // )
//                   ],
//                 ),
//               ),
//             ),
//           ),
//           Consumer<PCamera>(builder: (__, image, ___) {
//             if (image.busy) {
//               stackChildren.add(const Opacity(
//                 child: ModalBarrier(dismissible: false, color: Colors.grey),
//                 opacity: 0.3,
//               ));

//               stackChildren
//                   .add(const Center(child: CircularProgressIndicator()));
//             }

//             if (image.imagePath != null && !image.busy) {
//               stackChildren.add(Positioned(
//                 top: 0.0,
//                 left: 0.0,
//                 width: size.width,
//                 child: image.imagePath == null
//                     ? Text('No image selected.')
//                     : Image.file(File(image.imagePath)),
//               ));
//               stackChildren.addAll(renderBoxes(size, image.recognitions));
//             }

//             return Stack(
//               children: stackChildren,
//             );
//           })

//           // // _captureControlRowWidget(),
//           // _cameraTogglesRowWidget(),
//         ],
//       ),
//     );
//   }

//   /// Display the preview from the camera (or a message if the preview is not available).
//   Widget _cameraPreviewWidget() {
//     if (controller == null || !controller.value.isInitialized) {
//       return const Text(
//         'Loading...',
//         style: TextStyle(
//           color: Colors.white,
//           fontSize: 24.0,
//           fontWeight: FontWeight.w900,
//         ),
//       );
//     } else {
//       return Center(
//         child: Transform.scale(
//           scale: controller.value.aspectRatio / deviceRatio,
//           child: new AspectRatio(
//             aspectRatio: controller.value.aspectRatio,
//             child: new CameraPreview(controller),
//           ),
//         ),
//       );
//     }
//   }

//   /// Display the control bar with buttons to take pictures and record videos.
//   // Widget _captureControlRowWidget() {
//   //   return Row(
//   //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//   //     mainAxisSize: MainAxisSize.max,
//   //     children: <Widget>[
//   //       IconButton(
//   //         icon: const Icon(Icons.camera_alt),
//   //         color: Colors.blue,
//   //         onPressed: controller != null && controller.value.isInitialized
//   //             ? onTakePictureButtonPressed
//   //             : null,
//   //       ),
//   //     ],
//   //   );
//   // }

//   /// Display a row of toggle to select the camera (or a message if no camera is available).
//   Widget _cameraTogglesRowWidget() {
//     final List<Widget> toggles = <Widget>[];

//     if (cameras.isEmpty) {
//       return const Text('No camera found');
//     } else {
//       for (CameraDescription cameraDescription in cameras) {
//         toggles.add(
//           SizedBox(
//             width: 90.0,
//             child: RadioListTile<CameraDescription>(
//               title: Icon(getCameraLensIcon(cameraDescription.lensDirection)),
//               groupValue: controller?.description,
//               value: cameraDescription,
//               onChanged: onNewCameraSelected,
//             ),
//           ),
//         );
//       }
//     }

//     return Row(children: toggles);
//   }

//   String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

//   void showInSnackBar(String message) {
//     _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
//   }

//   void _toggleCameraLens() {
//     // get current lens direction (front / rear)
//     final lensDirection = controller.description.lensDirection;
//     CameraDescription newDescription;
//     if (lensDirection == CameraLensDirection.front) {
//       newDescription = cameras.firstWhere((description) =>
//           description.lensDirection == CameraLensDirection.back);
//     } else {
//       newDescription = cameras.firstWhere((description) =>
//           description.lensDirection == CameraLensDirection.front);
//     }

//     if (newDescription != null) {
//       onNewCameraSelected(newDescription);
//     } else {
//       print('Asked camera not available');
//     }
//   }

//   void onNewCameraSelected(CameraDescription cameraDescription) async {
//     if (controller != null) {
//       await controller.dispose();
//     }

//     controller = CameraController(
//       cameraDescription,
//       ResolutionPreset.medium,
//       enableAudio: false,
//     );

//     // If the controller is updated then update the UI.
//     controller.addListener(() {
//       if (mounted) setState(() {});
//       if (controller.value.hasError) {
//         showInSnackBar('Camera error ${controller.value.errorDescription}');
//       }
//     });

//     try {
//       await controller.initialize();
//     } on CameraException catch (e) {
//       _showCameraException(e);
//     }

//     if (mounted) {
//       setState(() {});
//     }
//   }

//   void onTakePictureButtonPressed() {
//     takePicture().then((String filePath) async {
//       if (mounted) {
//         context.read<PCamera>().setImagePath(filePath);
//         context.read<PCamera>().setBusyStatus(true);

//         await loadModel();
//         await yolov2Tiny(File(filePath));

//         new FileImage(File(filePath))
//             .resolve(new ImageConfiguration())
//             .addListener(ImageStreamListener((ImageInfo info, bool _) {
//           setState(() {
//             _imageHeight = info.image.height.toDouble();
//             _imageWidth = info.image.width.toDouble();
//           });
//         }));

//         if (filePath != null) showInSnackBar('Picture saved to $filePath');
//         context.read<PCamera>().setBusyStatus(false);
//       }
//     });
//   }

//   Future loadModel() async {
//     Tflite.close();
//     try {
//       String res;
//       res = await Tflite.loadModel(
//         model: "assets/yolov2_tiny.tflite",
//         labels: "assets/yolov2_tiny.txt",
//         // useGpuDelegate: true,
//       );
//       print(res);
//     } on PlatformException {
//       print('Failed to load model.');
//     }
//   }

//   Future<String> takePicture() async {
//     if (!controller.value.isInitialized) {
//       showInSnackBar('Error: select a camera first.');
//       return null;
//     }
//     final Directory extDir = await getApplicationDocumentsDirectory();
//     final String dirPath = '${extDir.path}/Pictures/flutter_test';
//     await Directory(dirPath).create(recursive: true);
//     final String filePath = '$dirPath/${timestamp()}.jpg';

//     if (controller.value.isTakingPicture) {
//       // A capture is already pending, do nothing.
//       return null;
//     }

//     try {
//       await controller.takePicture(filePath);
//     } on CameraException catch (e) {
//       _showCameraException(e);
//       return null;
//     }
//     return filePath;
//   }

//   void _showCameraException(CameraException e) {
//     logError(e.code, e.description);
//     showInSnackBar('Error: ${e.code}\n${e.description}');
//   }
// }
