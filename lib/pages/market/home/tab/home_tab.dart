import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:wblue_customer/data/market/d_categories.dart';
import 'package:wblue_customer/data/market/d_home.dart';
import 'package:wblue_customer/models/product.dart';
import 'package:wblue_customer/pages/market/detector/qr.dart';
import 'package:wblue_customer/pages/market/home/Services/servicesHome.dart';
import 'package:wblue_customer/providers/market/p_camera.dart';
import 'package:wblue_customer/providers/market/p_fullscreen_image.dart';
import 'package:wblue_customer/providers/market/p_home.dart';
import 'package:wblue_customer/providers/market/p_view_product.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_blue_background.dart';
import 'package:wblue_customer/widgets/w_card_title_link.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_image_carousel.dart';
import 'package:wblue_customer/widgets/w_image_product_card.dart';
import 'package:wblue_customer/widgets/w_items_wrapper.dart';
import 'package:wblue_customer/widgets/w_rating.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

import '../../../../env/config.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  RefreshController _refreshProductsController = RefreshController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          Container(
            color: Colors.white,
            child: PreferredSize(
                preferredSize: const Size.fromHeight(50.0),
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Container(
                    height: 40,
                    decoration: BoxDecoration(
                        border: Border.all(color: Config.primaryColor),
                        borderRadius: BorderRadius.all(Radius.circular(5.0))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              Global.currentCategory = null;
                              ExtendedNavigator.of(context)
                                  .push('/market-camera-recognizer');
                            },
                            child: Container(
                              color: Colors.transparent,
                              child: Row(
                                children: [
                                  Container(
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Config.primaryColor),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(4.0)),
                                      color: Config.primaryColor,
                                    ),
                                    child: Icon(MdiIcons.magnify,
                                        color: Colors.white),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: AutoSizeText('iphone X',
                                        style: TextStyle(
                                            fontSize: 18, color: Colors.grey)),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            ExtendedNavigator.of(context)
                                .push('/market-camera-recognizer');
                            context.read<PCamera>().setImagePath(null);
                          },
                          onLongPress: () {
                            ExtendedNavigator.of(context)
                                .push('/market-camera');
                          },
                          onDoubleTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => QrPage()),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Icon(
                              MdiIcons.cameraOutline,
                              color: Colors.grey.withOpacity(0.9),
                              size: 30,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )),
          ),
          Expanded(
            child: SmartRefresher(
              enablePullDown: true,
              enablePullUp: false,
              physics: _refreshProductsController.isRefresh
                  ? const NeverScrollableScrollPhysics()
                  : const AlwaysScrollableScrollPhysics(),
              header: CustomHeader(
                builder: (context, mode) {
                  Widget body;
                  if (mode == RefreshStatus.idle) {
                    body = Text('Idle');
                  } else if (mode == RefreshStatus.refreshing) {
                    body = CircularProgressIndicator();
                  } else if (mode == RefreshStatus.canRefresh) {
                    body = Text('Can Refresh');
                  } else if (mode == RefreshStatus.completed) {
                    body = Text('Completed');
                  }
                  return Container(
                    child: Center(
                      child: body,
                    ),
                  );
                },
              ),
              controller: _refreshProductsController,
              onRefresh: () async {
                context.read<PMarketHome>().fetchProduct();
                _refreshProductsController.refreshCompleted();
              },
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
//              _balance(),
                  WImageCarousel(
                    height: 160,
                    banners: DMarketHomeData.banners,
                  ),
                  SizedBox(height: 8.0),
                  SizedBox(
                      height: 100,
                      child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          scrollDirection: Axis.horizontal,
                          itemCount: DCategoryData.categories.length,
                          itemBuilder: (context, index) => GestureDetector(
                                onTap: () => DCategoryData.categories[index]
                                            ['route'] !=
                                        null
                                    ? ExtendedNavigator.of(context).push(
                                        '${DCategoryData.categories[index]['route']}')
                                    : null,
                                child: Container(
                                  width: 80,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/images/market/categories/${DCategoryData.categories[index]['image']}.png',
                                        height: 46,
                                        width: 46,
                                      ),
                                      SizedBox(height: 5.0),
                                      AutoSizeText(
                                        '${DCategoryData.categories[index]['name']}',
                                        textAlign: TextAlign.center,
                                        maxLines: 2,
                                      )
                                    ],
                                  ),
                                ),
                              ))),
                  WCardTitleLink(
                    title: 'Popular Categories',
                    linkTitle: '',
                    backGroundColor: Config.bodyColor,
                    widget: _popularCategories(),
                  ),

                  // Consumer<PMarketHome>(builder: (context, hotDeals, _) {
                  //   return WCardTitleLink(
                  //     title: 'Hot Deals',
                  //     linkTitle: 'Shop More',
                  //     onTap: () => ExtendedNavigator.of(context)
                  //         .push('/market/hot-deals'),
                  //     widget: hotDeals.products.length > 0
                  //         ? _hotDealsProductCard(hotDeals.products)
                  //         : Center(child: CircularProgressIndicator()),
                  //   );
                  // }),
//                   Consumer<PMarketHome>(
//                     builder: (context, featured, _) {
//                       return ConstrainedBox(
//                         constraints: BoxConstraints.loose(
//                             Size(MediaQuery.of(context).size.width, 300.0)),
//                         child: Card(
//                           elevation: 0,
//                           child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                               Padding(
//                                 padding: const EdgeInsets.all(8.0),
//                                 child: AutoSizeText('Featured Products',
//                                     style: TextStyle(
//                                         fontSize: 20,
//                                         fontWeight: FontWeight.bold)),
//                               ),
//                               featured.products.length > 0
//                                   ? Expanded(
//                                       child: Swiper(
//                                         onTap: (v) {
//                                           context
//                                               .read<PViewProduct>()
//                                               .setProduct(featured.products[v]);
//                                           ExtendedNavigator.of(context)
//                                               .push('/view-product');
// //                                      context.read<PFullscreenImage>().productImages(featured.products[v]['images']);
// //                                      ExtendedNavigator.of(context).push('/product-images');
//                                         },
//                                         onIndexChanged: (i) {
//                                           List images =
//                                               featured.products[i]['images'];
//                                           if (images.length > 0)
//                                             context
//                                                 .read<PMarketHome>()
//                                                 .selectFeaturedImg(
//                                                     images[0]['small']);
//                                         },
//                                         outer: false,
//                                         pagination: SwiperCustomPagination(
//                                             builder: (BuildContext context,
//                                                 SwiperPluginConfig config) {
//                                           return Positioned(
//                                             bottom: 6,
//                                             left: 6,
//                                             child: Container(
//                                               width: MediaQuery.of(context)
//                                                   .size
//                                                   .width,
//                                               child: AnimatedSmoothIndicator(
//                                                 activeIndex: config.activeIndex,
//                                                 count: config.itemCount,
//                                                 axisDirection: Axis.horizontal,
//                                                 effect: ExpandingDotsEffect(
//                                                   dotColor: Colors.black12,
//                                                   activeDotColor:
//                                                       Colors.black87,
//                                                   dotHeight: 7,
//                                                   dotWidth: 7,
//                                                   expansionFactor: 5,
//                                                 ),
//                                               ),
//                                             ),
//                                           );
//                                         }),
//                                         itemBuilder:
//                                             (BuildContext context, int index) {
//                                           return Row(
//                                             crossAxisAlignment:
//                                                 CrossAxisAlignment.start,
//                                             children: [
//                                               Container(
//                                                   child: _featuredImages(
//                                                       context,
//                                                       featured.products[index]
//                                                           ['images'])),
//                                               Expanded(
//                                                 flex: 1,
//                                                 child: Column(
//                                                   children: [
//                                                     Padding(
//                                                       padding:
//                                                           const EdgeInsets.all(
//                                                               8.0),
//                                                       child: Column(
//                                                         crossAxisAlignment:
//                                                             CrossAxisAlignment
//                                                                 .start,
//                                                         children: [
//                                                           AutoSizeText(
//                                                             '${featured.products[index]['name']}',
//                                                             maxLines: 3,
//                                                             style: TextStyle(
//                                                                 fontSize: 16,
//                                                                 fontWeight:
//                                                                     FontWeight
//                                                                         .bold),
//                                                             minFontSize: 16,
//                                                           ),
//                                                           SizedBox(height: 8),
//                                                           AutoSizeText(
//                                                             '${featured.products[index]['shortDescription']}',
//                                                             style: TextStyle(
//                                                                 fontSize: 16,
//                                                                 color: Colors
//                                                                     .black54),
//                                                             minFontSize: 16,
//                                                             maxLines: 4,
//                                                             overflow:
//                                                                 TextOverflow
//                                                                     .ellipsis,
//                                                           ),
//                                                           SizedBox(height: 2),
//                                                           WRatingsWidget(
//                                                               featured.products[
//                                                                           index]
//                                                                       [
//                                                                       'ratingsValue'] /
//                                                                   100),
//                                                           SizedBox(height: 2),
//                                                           AutoSizeText(
//                                                             '15 Reviews',
//                                                             style: TextStyle(
//                                                                 fontSize: 16,
//                                                                 color: Colors
//                                                                     .black54),
//                                                             minFontSize: 16,
//                                                           ),
//                                                           SizedBox(height: 2),
//                                                           Row(
//                                                             children: [
//                                                               Icon(
//                                                                 MdiIcons
//                                                                     .checkboxBlankCircle,
//                                                                 size: 12.0,
//                                                                 color: featured.products[index]
//                                                                             [
//                                                                             'availibilityCount'] >
//                                                                         0
//                                                                     ? Colors
//                                                                         .green
//                                                                         .withOpacity(
//                                                                             0.9)
//                                                                     : Colors.red
//                                                                         .withOpacity(
//                                                                             0.9),
//                                                               ),
//                                                               SizedBox(
//                                                                   width: 5.0),
//                                                               AutoSizeText(
//                                                                 '${featured.products[index]['availibilityCount'] > 0 ? 'In' : 'Out'} stock, ${featured.products[index]['availibilityCount'] > 1 ? '${featured.products[index]['availibilityCount']} units' : '${featured.products[index]['availibilityCount']} unit'}',
//                                                                 style: TextStyle(
//                                                                     fontSize:
//                                                                         16,
//                                                                     color: featured.products[index]['availibilityCount'] >
//                                                                             0
//                                                                         ? Colors
//                                                                             .green
//                                                                             .withOpacity(
//                                                                                 0.9)
//                                                                         : Colors
//                                                                             .red
//                                                                             .withOpacity(0.9)),
//                                                                 maxLines: 1,
//                                                                 overflow:
//                                                                     TextOverflow
//                                                                         .ellipsis,
//                                                                 minFontSize: 16,
//                                                               ),
//                                                             ],
//                                                           ),
//                                                           SizedBox(height: 2),
//                                                           AutoSizeText(
//                                                             'Color: Black',
//                                                             style: TextStyle(
//                                                                 fontSize: 16,
//                                                                 color: Colors
//                                                                     .black54),
//                                                             minFontSize: 16,
//                                                           ),
//                                                         ],
//                                                       ),
//                                                     ),
//                                                     SizedBox(height: 2),
//                                                     Container(
//                                                       height: 46,
//                                                       padding:
//                                                           EdgeInsets.symmetric(
//                                                               horizontal: 5.0),
//                                                       child: ListView.builder(
//                                                         scrollDirection:
//                                                             Axis.horizontal,
//                                                         itemBuilder:
//                                                             (context, i) =>
//                                                                 GestureDetector(
//                                                           onTap: () {},
//                                                           child: Container(
//                                                             width: 28,
//                                                             height: 28,
//                                                             margin:
//                                                                 EdgeInsets.all(
//                                                                     2.0),
//                                                             decoration:
//                                                                 BoxDecoration(
//                                                               color: Colors
//                                                                   .transparent, // border color
//                                                               shape: BoxShape
//                                                                   .circle,
//                                                             ),
//                                                             child: Padding(
//                                                               padding:
//                                                                   EdgeInsets.all(
//                                                                       4), // border width
//                                                               child: Container(
//                                                                 // or ClipRRect if you ned to clip the content
//                                                                 decoration:
//                                                                     BoxDecoration(
//                                                                   shape: BoxShape
//                                                                       .circle,
//                                                                   color: HexColor(
//                                                                       '${featured.products[index]['color'][i]}'), // inner circle color
//                                                                 ),
//                                                                 child:
//                                                                     Container(), // inner content
//                                                               ),
//                                                             ),
//                                                           ),
//                                                         ),
//                                                         itemCount: featured
//                                                             .products[index]
//                                                                 ['color']
//                                                             .length,
//                                                       ),
//                                                     )
//                                                   ],
//                                                 ),
//                                               ),
//                                             ],
//                                           );
//                                         },
//                                         itemCount: featured.products.length,
//                                       ),
//                                     )
//                                   : Center(child: CircularProgressIndicator()),
//                             ],
//                           ),
//                         ),
//                       );
//                     },
//                   ),

                  WCardTitleLink(
                    title: 'Popular Products',
                    linkTitle: 'Shop More',
                    widget: _popularProductCard(),
                  ),
                  SizedBox(height: 5),
                  WCardTitleLink(
                    title: 'WMall Live',
                    linkTitle: 'Shop More',
                    widget: _popularUserCard(),
                  ),
                  Center(
                      child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AutoSizeText('Just For You',
                        style: TextStyle(fontSize: 16, color: Colors.black54)),
                  )),
                  Consumer<PMarketHome>(
                    builder: (context, market, child) {
                      return market.justForYouProducts.length > 0
                          ? _productCard(context, market.justForYouProducts)
                          : Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(child: CircularProgressIndicator()),
                            );
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _popularCategories() {
    return WItemList(
      crossAxisCellCount: 1,
      crossAxisCount: 4,
      mainAxisSpacing: 10,
      widget: (category, index) => GestureDetector(
        onTap: () {
          Global.currentCategory = category['title'];
          // ExtendedNavigator.of(context).push('/market/search-product');
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ServicesHome()));
        },
        child: Container(
          height: 80,
          width: 80,
          child: CircleAvatar(
            backgroundColor: Colors.grey.withOpacity(0.3),
            child: Center(
              child: Image.asset(
                category['image'],
                height: 60,
                width: 60,
              ),
            ),
          ),
        ),
      ),
      items: DMarketHomeData.popularCategories,
    );
  }

  Widget _hotDealsProductCard(List hotDeals) {
    return WItemList(
      crossAxisCellCount: 1,
      crossAxisCount: 3,
      crossAxisSpacing: 2,
      widget: (product, index) => GestureDetector(
        onTap: () {
          context.read<PViewProduct>().setProduct(product);
          ExtendedNavigator.of(context).push('/view-product');
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(12.0),
                    bottomRight: Radius.circular(12.0)),
                color: Colors.red,
              ),
              child: Padding(
                padding: const EdgeInsets.all(6.0),
                child: AutoSizeText('Save ${product['saveValue']} QAR',
                    style: TextStyle(color: Colors.white)),
              ),
            ),
            SizedBox(height: 8),
            WImageWidget(
              placeholder: AssetImage(product['images'][0]['small']),
            ),
            SizedBox(height: 6),
            AutoSizeText(
              '${product['name']}',
              style: TextStyle(color: Colors.black, fontSize: 16),
              minFontSize: 16,
            ),
            SizedBox(height: 6),
            AutoSizeText(
              '${product['longDescription']}',
              style: TextStyle(color: Colors.black87, fontSize: 14),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              minFontSize: 14,
            )
          ],
        ),
      ),
      items: hotDeals,
    );
  }

  Widget _featuredImages(BuildContext context, List images) {
    return Row(
      children: [
        Container(
            width: 60,
            height: 240,
            child: ListView.builder(
              itemBuilder: (context, index) => GestureDetector(
                onTap: () {
                  context.read<PFullscreenImage>().setImageIndex(index);
                  context
                      .read<PMarketHome>()
                      .selectFeaturedImg(images[index]['small']);
                },
                child: Card(
                  margin: EdgeInsets.all(5),
                  elevation: 3.0,
                  shape: RoundedRectangleBorder(
                      side: BorderSide(
                          width: 1,
                          color: context
                                      .watch<PMarketHome>()
                                      .featuredSelectedImg ==
                                  images[index]['small']
                              ? Config.primaryColor
                              : Colors.transparent),
                      borderRadius: BorderRadius.circular(4)),
                  child: Container(
                    width: 50,
                    height: 50,
                    padding: EdgeInsets.all(2.0),
                    child: WImageWidget(
                      placeholder: AssetImage('${images[index]['small']}'),
                    ),
                  ),
                ),
              ),
              itemCount: images.length,
            )),
        GestureDetector(
          onTap: () {
            context.read<PFullscreenImage>().productImages(images);
            ExtendedNavigator.of(context).push('/product-images');
          },
          child: Container(
            width: 120,
            height: 120,
            child: WImageWidget(
              placeholder: context.watch<PMarketHome>().featuredSelectedImg ==
                          '' ||
                      context.watch<PMarketHome>().featuredSelectedImg == null
                  ? AssetImage('${images[0]['small']}')
                  : AssetImage(
                      context.watch<PMarketHome>().featuredSelectedImg),
            ),
          ),
        ),
      ],
    );
  }

  Widget _popularProductCard() {
    return WItemList(
      crossAxisCellCount: 1,
      crossAxisCount: 2,
      crossAxisSpacing: 4,
      mainAxisSpacing: 4,
      widget: (product, index) => Container(
        height: 100,
        decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.8),
            borderRadius: BorderRadius.all(Radius.circular(10)),
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.3), BlendMode.dstATop),
              image: product['image'],
              fit: BoxFit.cover,
            )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AutoSizeText(
              '${product['title']}',
              style: TextStyle(color: Colors.white, fontSize: 16),
              minFontSize: 16,
            ),
            AutoSizeText(
              'Price starts at ${product['value']}',
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
      ),
      items: DMarketHomeData.popularProducts,
    );
  }

  Widget _popularUserCard() {
    return WItemList(
      crossAxisCellCount: 1,
      crossAxisCount: 3,
      crossAxisSpacing: 4,
      widget: (user, index) => GestureDetector(
        onTap: () {
          // ExtendedNavigator.of(context).push('/market/live');
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 120,
              width: 120,
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.8),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  image: DecorationImage(
                    image: AssetImage(user['image']),
                    colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.7), BlendMode.dstATop),
                    fit: BoxFit.fitHeight,
                  )),
              child: Stack(
                children: [
                  Positioned(
                    top: 2,
                    left: 2,
                    child: Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(8.0),
                                bottomLeft: Radius.circular(8.0)),
                            color: Colors.red,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Row(
                              children: [
                                Icon(
                                  MdiIcons.checkboxBlankCircle,
                                  size: 9,
                                  color: Colors.white,
                                ),
                                SizedBox(width: 2),
                                AutoSizeText(
                                  'Live',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 8.0),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(8.0),
                                bottomRight: Radius.circular(8.0)),
                            color: Colors.black.withOpacity(0.8),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Row(
                              children: [
                                Icon(
                                  MdiIcons.account,
                                  size: 12,
                                  color: Colors.white,
                                ),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth:
                                        MediaQuery.of(context).size.width * 0.1,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 2.0),
                                    child: AutoSizeText(
                                      '${user['viewers']}',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 8.0),
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 4,
                    left: 4,
                    child: Row(
                      children: [
                        Container(
                            height: 20,
                            width: 20,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                color: Config.primaryColor),
                            child: Icon(
                              MdiIcons.heart,
                              color: Colors.white,
                              size: 12,
                            )),
                        ConstrainedBox(
                          constraints: BoxConstraints(
                            maxWidth: MediaQuery.of(context).size.width * 0.2,
                          ),
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 2.0),
                            child: AutoSizeText(
                              '${user['likes']}',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 16.0),
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                              maxLines: 1,
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            AutoSizeText(
              '${user['description']}',
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black87,
              ),
              maxLines: 2,
            )
          ],
        ),
      ),
      items: DMarketHomeData.popularUsers,
    );
  }

  Widget _productCard(BuildContext context, List products) {
    return WItemList(
      crossAxisCellCount: 2,
      crossAxisCount: 4,
      widget: (product, index) {
        ProductListModel productModel = ProductListModel.fromJson(product);
        return WProductImageCard(
          onTap: () {
            ExtendedNavigator.of(context).push('/view-product',
                arguments: ViewProductArguments(hashid: productModel.hashId));
          },
          isVertical: true,
          imageUrl: '${productModel.imageFull}',
          imageThumbnail: '${productModel.imageThumbnail}?',
          widget: Container(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AutoSizeText(
                '${productModel.name}',
                style: TextStyle(fontSize: 28.sp, fontWeight: FontWeight.bold),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
              SizedBox(height: 0.01.sw),
              AutoSizeText(
                '${productModel.shortDescription}',
                style: TextStyle(fontSize: 18.sp),
                maxLines: 4,
                overflow: TextOverflow.ellipsis,
                minFontSize: 14,
              ),
              SizedBox(height: 0.005.sw),
              Container(
                child: Row(
                  children: <Widget>[
                    AutoSizeText(
                      'QR ${productModel.productPrice}',
                      style:
                          TextStyle(fontSize: 26.sp, color: Colors.redAccent),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(width: 8.0),
                    productModel.salePrice != null && productModel.salePrice > 0
                        ? Expanded(
                            child: AutoSizeText('QR ${productModel.salePrice}',
                                style: TextStyle(
                                    fontSize: 16.sp,
                                    color: Colors.grey,
                                    decoration: TextDecoration.lineThrough),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
              SizedBox(height: 0.0025.sh),
              // Row(
              //   children: [
              //     Icon(
              //       MdiIcons.checkboxBlankCircle,
              //       size: 16.0,
              //       color: product['availibilityCount'] > 0
              //           ? Colors.green.withOpacity(0.9)
              //           : Colors.red.withOpacity(0.9),
              //     ),
              //     SizedBox(width: 8.0),
              //     AutoSizeText(
              //       '${productModel.stockStatus > 0 ? 'In' : 'Out'} stock, ${product['availibilityCount'] > 1 ? '${product['availibilityCount']} units' : '${product['availibilityCount']} unit'}',
              //       style: TextStyle(
              //           fontSize: 16,
              //           color: product['availibilityCount'] > 0
              //               ? Colors.green.withOpacity(0.9)
              //               : Colors.red.withOpacity(0.9)),
              //       maxLines: 1,
              //       overflow: TextOverflow.ellipsis,
              //       minFontSize: 16,
              //     ),
              //   ],
              // ),
              SizedBox(height: 3.0),
              Row(
                children: [
                  WRatingsWidget(productModel.rating / 100),
                  SizedBox(width: 2),
                  AutoSizeText(
                    '(${productModel.rating})',
                    style: TextStyle(
                        fontSize: 16.sp, color: Colors.grey.withOpacity(0.9)),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
              SizedBox(height: 3.0),
              AutoSizeText(
                '${productModel.reviews} Reviews',
                style: TextStyle(fontSize: 16.sp, color: Colors.grey),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          )),
        );
      },
      items: products,
    );
  }

  Widget _balance() {
    return Center(
      child: WContainerBlue(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  AutoSizeText(
                    'Available Balance (QAR)',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: 5.0),
                  AutoSizeText(
                    '0.00',
                    style: TextStyle(
                      fontSize: 24,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: <Widget>[
                      AutoSizeText(
                        'Reabate (QAR) ',
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.white,
                        ),
                      ),
                      AutoSizeText(
                        '0.00',
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.amber,
                        ),
                      ),
                      Icon(MdiIcons.chevronRight, color: Colors.amber)
                    ],
                  ),
                ],
              ),
              WRoundedButton(
                onCustomButtonPressed: () => print('activate'),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Text('Activate'.toUpperCase(),
                      style: TextStyle(fontSize: 14)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
