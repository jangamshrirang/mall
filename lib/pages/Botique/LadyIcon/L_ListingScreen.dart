import 'package:flutter/material.dart';

import 'LadyScreen.dart';

class L_ListingScreen extends StatefulWidget {
  @override
  _L_ListingScreenState createState() => _L_ListingScreenState();
}

class _L_ListingScreenState extends State<L_ListingScreen> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      child: SingleChildScrollView(
        child: Wrap(
            children: List.generate(
                10,
                (index) => Container(
                    height: size.height * 0.51,
                    child: LadyScreen(
                      from: "ListHome",
                    )))),
      ),
    );
  }


}
