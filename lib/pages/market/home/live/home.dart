import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wblue_customer/models/live/live.dart';
import 'package:wblue_customer/routes/router.gr.dart';

class LiveHomePage extends StatefulWidget {
  @override
  _LiveHomePageState createState() => _LiveHomePageState();
}

class _LiveHomePageState extends State<LiveHomePage> {
  final databaseReference = FirebaseFirestore.instance;
  bool loading = true;
  List<Live> list = [];
  var image =
      'https://nichemodels.co/wp-content/uploads/2019/03/user-dummy-pic.png';
  var username;
  var postUsername;
  Live liveUser;

  @override
  void initState() {
    super.initState();

    dbChangeListen();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: getMain(), floatingActionButton: getFloatingActionButton());
  }

  // Future<void> loadMyProfile() async {
  //   var box = await Hive.openBox('auth');

  //   username = await box.get('username') ?? 'JohnDoe';

  //   image = await box.get('image') ??
  //       'https://nichemodels.co/wp-content/uploads/2019/03/user-dummy-pi123c.png';

  //   // liveUser = new Live(username: username, me: true, image: image);

  //   // setState(() {
  //   //   list.add(liveUser);
  //   // });

  //   dbChangeListen();
  // }

  List<Widget> getUserStories() {
    List<Widget> stories = [];
    for (Live users in list) {
      stories.add(getContent(users));
    }
    return stories;
  }

  Widget getContent(Live user, {bool isSelected}) {
    return Column(
      children: [
        Container(
          color: Color(0xff2A2A2A),
          child: Row(
            children: [
              SizedBox(width: 8),
              Container(
                height: 36,
                width: 36,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(35),
                  child: CachedNetworkImage(
                    imageUrl: user.image,
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(width: 15),
              Text(
                '${user.name}',
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
              Expanded(child: SizedBox()),
              IconButton(
                icon: Icon(Icons.zoom_out_map),
                color: Colors.white70,
                onPressed: () => ExtendedNavigator.of(context).push(
                    '/market/live/join',
                    arguments: JoinPageArguments(
                        channelId: user.channelId,
                        hostId: user.userid,
                        hostImageUrl: user.image,
                        hostName: user.name)),
              )
            ],
          ),
        ),
        GestureDetector(
          onTap: () => ExtendedNavigator.of(context).push('/market/live/join',
              arguments: JoinPageArguments(
                  channelId: user.channelId,
                  hostId: user.userid,
                  hostImageUrl: user.image,
                  hostName: user.name)),
          child: Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Color(0xff343131),
            ),
            child: Stack(
              children: [
                Image.network(
                  user.contentThumbnail,
                  width: MediaQuery.of(context).size.width,
                  height: 300,
                  fit: BoxFit.cover,
                ),
                // PreviewPage(
                //     channelId: users.channelId,
                //     username: username,
                //     channelName: users.username),
                Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [
                            Colors.black,
                            Colors.transparent,
                          ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${user.title}',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w600),
                          ),
                          Row(
                            children: [
                              // _live(label: 'LIVE'),
                              // _live(label: '${user.viewer}', isUser: true),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 8.0,
        )
      ],
    );

    // Container(
    //   margin: EdgeInsets.all(5),
    //   child: Column(
    //     children: <Widget>[
    //       Container(
    //           child: GestureDetector(
    //               onTap: () {
    //                 onJoin(
    //                     channelName: users.username,
    //                     channelId: users.channelId,
    //                     username: username,
    //                     hostImage: users.image,
    //                     userImage: image);

    //                 // if (users.me == true) {
    //                 //   // Host function
    //                 //   onCreate(username: users.username, image: users.image);
    //                 // } else {
    //                 //   // Join function
    //                 //   onJoin(
    //                 //       channelName: users.username,
    //                 //       channelId: users.channelId,
    //                 //       username: username,
    //                 //       hostImage: users.image,
    //                 //       userImage: image);
    //                 // }
    //               },
    //               child: _onlineCard())),
    //     ],
    //   ),
    // );
  }

  Future<void> onJoin(
      {channelName, channelId, username, hostImage, userImage}) async {
    // update input validation
    if (channelName.isNotEmpty) {
      // push video page with given channel name
      // await Navigator.push(
      //   context,
      //   MaterialPageRoute(
      //     builder: (context) => JoinPage(
      //       channelName: channelName,
      //       channelId: channelId,
      //       username: username,
      //       hostImage: hostImage,
      //       userImage: userImage,
      //     ),
      //   ),
      // );
    }
  }

  Widget _live({String label, bool isUser: false}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      decoration: BoxDecoration(
          color: isUser ? Color(0xff2A2A2A) : Colors.redAccent,
          borderRadius: isUser
              ? BorderRadius.only(
                  topRight: Radius.circular(20),
                  bottomRight: Radius.circular(20))
              : BorderRadius.only(
                  topLeft: Radius.circular(20),
                  bottomLeft: Radius.circular(20))),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          isUser
              ? Icon(
                  Icons.person,
                  color: Colors.white,
                  size: 14,
                )
              : Container(
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Colors.white),
                ),
          SizedBox(width: 5),
          Text(
            label,
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ],
      ),
    );
  }

  _goToRoom() {
    ExtendedNavigator.of(context).push('/market/live');
  }

  Future<void> onCreate({username, image}) async {
    // await for camera and mic permissions before pushing video page
    // await _handleCameraAndMic();
    var date = DateTime.now();
    var currentTime = '${DateFormat("dd-MM-yyyy hh:mm:ss").format(date)}';
    // push video page with given channel name

    // await Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //     builder: (context) => CallPage(
    //       channelName: username,
    //       time: currentTime,
    //       image: image,
    //     ),
    //   ),
    // );
  }

  // Future<void> _handleCameraAndMic() async {
  //   await PermissionHandler().requestPermissions(
  //     [PermissionGroup.camera, PermissionGroup.microphone],
  //   );
  // }

  void dbChangeListen() {
    databaseReference
        .collection('liveuser')
        .orderBy("time", descending: true)
        .snapshots()
        .listen((result) {
      // Listens to update in appointment collection
      if (!mounted) return;

      setState(() {
        list = [];
        // liveUser = new Live(username: username, me: true, image: image);
        // list.add(liveUser);
      });

      result.docs.forEach((result) {
        setState(() {
          list.add(new Live(
              userid: result.get('userid'),
              name: result.get('name'),
              image: result.get('image'),
              contentThumbnail: result.get('content_thumbnail'),
              title: result.get('title'),
              channelId: result.get('channel'),
              viewer: result.get('viewer'),
              me: false));
        });
      });

      print('list lenght: ${list.length}');
    });
  }

  Widget getFloatingActionButton() {
    return FloatingActionButton.extended(
      onPressed: () async {
        ExtendedNavigator.of(context).push('/market/live/content');
      },
      label: Text('Go Live'),
      icon: Icon(Icons.videocam),
      backgroundColor: Colors.pink,
    );
  }

  Widget getMain() {
    return SafeArea(
        child: Column(
      children: [
        // Expanded(
        //   child: ListView(
        //     physics: ClampingScrollPhysics(),
        //     children: getUserStories(),
        //   ),
        // ),
        Expanded(
            child: RefreshIndicator(
          onRefresh: () async {
            dbChangeListen();
          },
          child: ListView.builder(
            itemCount: list.length,
            itemBuilder: (BuildContext context, int index) {
              return getContent(list[index]);
            },
          ),
        ))
      ],
    ));
  }
}
