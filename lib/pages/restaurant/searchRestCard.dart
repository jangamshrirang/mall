import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/R_ListByRatings.dart';
import 'package:wblue_customer/pages/restaurant/store/single_restaurant.dart';
import 'package:wblue_customer/providers/restaurant/RestSearchResult.dart';
import 'package:wblue_customer/widgets/w_loading.dart';

class RestSearchResult extends StatefulWidget {
  String rHashId;
  RestSearchResult({this.rHashId});
  @override
  _RestSearchResultState createState() => _RestSearchResultState();
}

class _RestSearchResultState extends State<RestSearchResult> {
  @override
  void initState() {
    Future.microtask(() => context
        .read<R_SearchResultHelper>()
        .fetchSearchRestResult(widget.rHashId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Config.primaryColor, title: Text("Restaurents")),
        body: Container(child: Consumer<R_SearchResultHelper>(
            builder: (context, restaurant, child) {
          if (restaurant.restaurantsSearchResult != null) {
            RestaurantListModel restaurantsearchResultModel =
                RestaurantListModel.fromJson(
                    restaurant.restaurantsSearchResult);

            return _card(restaurantsearchResultModel);
            // Container(
            //     child: Text(restaurantsearchResultModel.restaurant)
            // );
          } else {
            return WLoadingWidget();
          }
        })));
  }

  _card(RestaurantListModel restaurant) {
    var size = MediaQuery.of(context).size;

    return Card(
      margin: EdgeInsets.symmetric(horizontal: 10),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => SingleRestaurantPage(
                        restaurantListModel: restaurant,
                      )));
        },
        //context.navigator.push('/restaurants/single'),
        child: Container(
          padding: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width,
          height: size.height * 0.5,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16.0),
            color: Colors.white,
          ),
          child: Stack(children: [
            Row(
              children: [
                Expanded(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5.0),
                    child: CachedNetworkImage(
                      imageUrl: restaurant?.fullCover,
                      imageBuilder: (context, imageProvider) => Container(
                        margin: EdgeInsets.only(bottom: size.height * 0.15),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                      ),
                      placeholder: (context, url) => Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: WLoadingWidget()),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                ),
              ],
            ),
            Container(
                margin: EdgeInsets.only(
                    left: size.height * 0.35, top: size.height * 0.01),
                decoration: BoxDecoration(
                  color: Config.primaryColor,
                  borderRadius: BorderRadius.circular(2.0),
                ),
                child: Text(restaurant.schedule.status.toString(),
                    style: TextStyle(
                      fontFamily: Config.fontFamily,
                      color: Colors.white,
                    ))),
            Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      // Container(
                      //   padding: EdgeInsets.all(1.0),
                      //   decoration: BoxDecoration(
                      //     borderRadius: BorderRadius.circular(4.0),
                      //     color: Colors.white,
                      //   ),
                      //   height: 60,
                      //   width: 60,
                      //   child: CachedNetworkImage(
                      //     imageUrl: restaurant?.thumbnail,
                      //     imageBuilder: (context, imageProvider) => Container(
                      //       decoration: BoxDecoration(
                      //         image: DecorationImage(
                      //           image: imageProvider,
                      //           fit: BoxFit.cover,
                      //         ),
                      //         borderRadius: BorderRadius.circular(4.0),
                      //       ),
                      //     ),
                      //     errorWidget: (context, url, error) =>
                      //         Icon(Icons.error),
                      //   ),
                      //   // child: ClipRRect(
                      //   //     borderRadius: BorderRadius.circular(4.0),
                      //   //     child: WImageWidget(
                      //   //       placeholder: AssetImage(
                      //   //           'assets/images/logos/wmall-512.png'),
                      //   //       url: restaurant?.profilePicture,
                      //   //       fit: BoxFit.cover,
                      //   //     )),
                      // ),
                      SizedBox(width: 5.0),
                    ],
                  ),
                  Row(children: [
                    Container(
                      width: size.width * 0.6,
                      height: size.height * 0.13,
                      // color: Colors.red,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(height: size.height * 0.01),
                          AutoSizeText(
                            '${restaurant.restaurant}',
                            maxLines: 1,
                            style: TextStyle(
                                fontFamily: Config.fontFamily,
                                color: Colors.black,
                                fontSize: size.height * 0.032,
                                fontWeight: FontWeight.w600),
                          ),
                          // SizedBox(height:size.height*0.005),
                          Text(
                            restaurant.storeInformation == null
                                ? 'A quality and relaxing rural country pub, run by award winning owners, in the beautiful Berkshire village of Peasemore just a short drive from the A34 and junctions 13 and 14 of the M4.'
                                : '${restaurant.storeInformation}',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontFamily: Config.fontFamily,
                                color: Colors.black54,
                                fontSize: size.height * 0.022),
                          ),
                          SizedBox(height: size.height * 0.01),
                          Row(
                            children: [
                              AutoSizeText(
                                '1.1k',
                                style: TextStyle(
                                    fontFamily: Config.fontFamily,
                                    color: Colors.black54,
                                    fontSize: size.height * 0.022),
                              ),
                              AutoSizeText(
                                'Followers',
                                style: TextStyle(
                                    fontFamily: Config.fontFamily,
                                    color: Colors.black54,
                                    fontSize: size.height * 0.022),
                              ),
                            ],
                          ),
                          SizedBox(height: 3),
                        ],
                      ),
                    ),
                    SizedBox(width: 10),
                    // GestureDetector(
                    //   onTap: () {},
                    //   child: Icon(
                    //     MdiIcons.bell,
                    //     color: Colors.amberAccent,
                    //   ),
                    // )
                  ]),
                ],
              ),
            ),
            Positioned(
                left: size.width * 0.3,
                top: size.height * 0.41,
                // height: size.height * 0.03,
                // width: size.width * 0.3,
                // color: Colors.red,
                child: Row(
                  children: [
                    iconsOnImage("assets/images/take out.png"),
                    iconsOnImage("assets/images/delivery.png"),
                    iconsOnImage("assets/images/book table.png")
                  ],
                ))
          ]),
        ),
      ),
    );
  }

  iconsOnImage(String image) {
    var size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.025,
        width: size.width * 0.1,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2.0),
        ),
        child: Image.asset(image, color: Colors.black));
  }
}
