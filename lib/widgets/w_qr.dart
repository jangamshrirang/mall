import 'package:flutter/cupertino.dart';
import 'package:pretty_qr_code/pretty_qr_code.dart';

class JQRWidget extends StatelessWidget {
  final double size;
  String qrCode;
  JQRWidget({this.size, this.qrCode});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: PrettyQr(
        data: qrCode == null ? "no Qrcode" : qrCode,
        image: AssetImage('assets/images/logos/wmall-512.png'),
        typeNumber: 8,
        size: size,
        roundEdges: true,
      ),
    );
  }
}
