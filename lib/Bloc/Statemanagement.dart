class StateManagment {
  String locationAreaname,
      pickUpLocation,
      dropOffLocation,
      guestNumber,
      time,
      date,
      convertedCurrency,
      scanString,
      deliverOrTakeString,
      tableFlare,
      rName,
      rImage,
      rHashId,
      userMoileNumber;
  var locatonLat, locationLong, totalPrice, iosToken, cartIdFromReservation;
  int tableNumber;
  int km;
  bool restFav, fromReservation;

  setLocationAreaName(String value) {
    locationAreaname = value;
  }

  setLocationLat(String value) {
    locatonLat = value;
  }

  setLocationLong(String value) {
    locationLong = value;
  }

  setTablenumber(int value) {
    tableNumber = value;
  }

  setGuestnumber(value) {
    guestNumber = value;
  }

  setKm(int value) {
    km = value;
  }

  setPickUpLocation(String value) {
    pickUpLocation = value;
  }

  setDropOffLocation(String value) {
    dropOffLocation = value;
  }

  setTableBookingtime(String value) {
    time = value;
  }

  setTableBookingDate(String value) {
    date = value;
  }

  setConvertedCurrency(String value) {
    convertedCurrency = value;
  }

  setScanString(String value) {
    scanString = value;
  }

  setRestFave(bool value) {
    restFav = value;
  }

  setFromReservastion(bool value) {
    fromReservation = value;
  }

  setDeliveryOrTakeString(String value) {
    deliverOrTakeString = value;
  }

  setTotalPrict(var value) {
    totalPrice = value;
  }

  setIosToken(var value) {
    iosToken = value;
  }

  setCartIdFromReservation(var value) {
    cartIdFromReservation = value;
  }

  setTableFlare(String value) {
    tableFlare = value;
  }

  setRname(String value) {
    rName = value;
  }

  setRimage(String value) {
    rImage = value;
  }

  setRashID(String value) {
    rHashId = value;
  }

  setUserMobileNumber(String value) {
    userMoileNumber = value;
  }
}

final stateManagment = StateManagment();
