import 'dart:collection';

import 'package:flutter/foundation.dart';

class PFullscreenImage extends ChangeNotifier {
  List _images = [];
  int _selectedImgIndex = 0;
  UnmodifiableListView get images => UnmodifiableListView(_images);
  int get selectedImgIndex => _selectedImgIndex;

  void selectImageIndex(int index) {
    _selectedImgIndex = index;
    notifyListeners();
  }

  void productImages(List images) {
    _images = images;
  }

  void setImageIndex(int index) {
    _selectedImgIndex = index;
  }
}
