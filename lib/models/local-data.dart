import 'dart:convert';

import 'package:flutter/services.dart';

class LocalData {
  static Future products() async {
    String data = await rootBundle.loadString('assets/data/products.json');
    return json.decode(data);
  }

  static Future hotDealsProducts() async {
    String data = await rootBundle.loadString('assets/data/hot-deals-products.json');
    return json.decode(data);
  }
}
