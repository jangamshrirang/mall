import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/widgets/w_button.dart';

import 'AdditionalInfo.dart';
import 'ReasonforRatiingScreen.dart';

class TrackingPlace extends StatefulWidget {
  @override
  _TrackingPlaceState createState() => _TrackingPlaceState();
}

class _TrackingPlaceState extends State<TrackingPlace> {
  List<String> starRatings = [
    "   ",
    "Chatbot",
    "Wmall app",
    "Push notificatino (from Wmall App)",
    "Email",
    "Website",
    "Customer service agent"
  ];
  int secondaryIndex = 0;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBarWithOutIcon(context, "Delivery Survey"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: FractionallySizedBox(
                widthFactor: 0.3,
                child: Image.asset('assets/images/logos/wmall-512.png'),
              ),
            ),
            text(
                "How often do you track the location/status updates of your item ?",
                FontWeight.normal,
                FontStyle.normal),
            Container(
              width: double.infinity,
              height: size.height * 0.5,
              child: ListView.separated(
                  separatorBuilder: (context, index) => Divider(
                        color: Colors.white,
                      ),
                  scrollDirection: Axis.vertical,
                  itemCount: starRatings.length,
                  itemBuilder: (BuildContext context, int index) {
                    return customRadio2(index, starRatings[index]);
                  }),
            ),
            // FractionallySizedBox(
            //   widthFactor: 0.7,
            //   child: WButtonWidget(
            //     title: 'Back to Home',
            //     onPressed: () {
            //       ExtendedNavigator.of(context).root.push('/');
            //     },
            //   ),
            // ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AdditionalInfo()));
              },
              child: Container(
                  width: size.width * 0.85,
                  child: Text(
                    "Skip",
                    textAlign: TextAlign.end,
                    style: TextStyle(color: Colors.blue, fontSize: 15),
                  )),
            )
          ],
        ),
      ),
    );
  }

  void changeSecondaryIndex(int index, String text) {
    setState(() {
      secondaryIndex = index;
      print(text);

      Navigator.push(
          context, MaterialPageRoute(builder: (context) => AdditionalInfo()));
    });
  }

  Widget customRadio2(int index, String text) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
        onTap: () {
          changeSecondaryIndex(index, text);
        },
        child: index == 0
            ? Text(
                "",
                style: TextStyle(fontSize: 0.01),
              )
            : Container(
                height: size.height * 0.05,
                margin: EdgeInsets.only(
                    left: size.width * 0.05, right: size.width * 0.05),
                decoration: BoxDecoration(
                  color: secondaryIndex == index
                      ? Config.primaryColor
                      : Colors.white,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(
                      width: 1,
                      color: secondaryIndex == index
                          ? Config.primaryColor
                          : Colors.black),
                ),
                child: Row(
                  children: [
                    SizedBox(width: 5),
                    secondaryIndex == index
                        ? Icon(
                            Icons.radio_button_checked,
                            color: Colors.white,
                            size: 22,
                          )
                        : Icon(
                            Icons.radio_button_unchecked,
                            size: 22,
                          ),
                    SizedBox(width: 5),
                    Text(text,
                        style: TextStyle(
                            color: secondaryIndex == index
                                ? Colors.white
                                : Colors.black,
                            fontSize: 16)),
                  ],
                )));
  }

  text(String text, FontWeight fontWeight, FontStyle fontStyle) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Text(
        text,
        style: TextStyle(
            color: Colors.black,
            fontWeight: fontWeight,
            fontSize: 18,
            fontStyle: fontStyle),
      ),
    );
  }
}
