import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/widgets/w_button.dart';

class TankyouScreen extends StatelessWidget {
  final noteController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBarWithOutIcon(context, "Delivery Survey"),
      body: Column(
        children: [
          Center(
            child: FractionallySizedBox(
              widthFactor: 0.3,
              child: Image.asset('assets/images/logos/wmall-512.png'),
            ),
          ),
          text(
              "Thank you for filling up the survey.We take your inputs seriously in improving our services.",
              FontWeight.normal,
              FontStyle.normal),
          SizedBox(
            height: 10,
          ),
          text(
              "for any further concerns, please contact Wmall Customer Service.",
              FontWeight.normal,
              FontStyle.normal),
          SizedBox(
            height: 100,
          ),
          FractionallySizedBox(
            widthFactor: 0.7,
            child: WButtonWidget(
              title: 'Back to Home',
              onPressed: () {
                ExtendedNavigator.of(context).root.push('/');
              },
            ),
          ),
        ],
      ),
    );
  }

  text(String text, FontWeight fontWeight, FontStyle fontStyle) {
    return Container(
      margin: EdgeInsets.only(left: 50, right: 20),
      child: Text(
        text,
        style: TextStyle(
            color: Colors.black,
            fontWeight: fontWeight,
            fontSize: 18,
            fontStyle: fontStyle),
      ),
    );
  }
}
