import 'package:flutter/material.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/Activities/notifications.dart';

import 'Packages.dart';

class ActivityExpantion extends StatefulWidget {
  String image, title, subtitle;
  ActivityExpantion({this.image, this.subtitle, this.title});
  @override
  _ActivityExpantionState createState() => _ActivityExpantionState();
}

class _ActivityExpantionState extends State<ActivityExpantion> {
  //for date picker
  DateTime selectedDate = DateTime.now();
  //for time picker
  static DateTime _eventdDate = DateTime.now();
  static var now =
      TimeOfDay.fromDateTime(DateTime.parse(_eventdDate.toString()));
  String _eventTime = now.toString().substring(10, 15);
  int secondaryIndex = 0;
  //text editor
  bool fav = stateManagment.restFav;
  bool selected = false;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  myAppBarContainer(),
                  servicesCard(),
                  text("   Image Gallery", size.height * 0.024, Colors.black, 1,
                      FontWeight.bold, size.width * 0.5),
                  SizedBox(height: 5),
                  Wrap(
                    spacing: 0,
                    children: [
                      activityCard(
                        "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                      ),
                      activityCard(
                        "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                      ),
                      activityCard(
                        "assets/activities/rodrigo-lourenco-m_VDzGhvg_8-unsplash.jpg",
                      ),
                      activityCard(
                        "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                      ),
                      activityCard(
                        "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                      ),
                      activityCard(
                        "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                      ),
                      activityCard(
                        "assets/activities/rodrigo-lourenco-m_VDzGhvg_8-unsplash.jpg",
                      ),
                      activityCard(
                        "assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg",
                      ),
                    ],
                  )
                ],
              ),
              Container(
                  height: size.height * 0.3,
                  width: size.width * 0.9,
                  margin: EdgeInsets.only(
                      left: size.width * 0.05, top: size.height * 0.08),
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                        spreadRadius: 1,
                        blurRadius: 3,
                        offset: Offset(1, 3), // changes position of shadow
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    image: DecorationImage(
                      image: AssetImage(
                        widget.image,
                      ),
                      fit: BoxFit.fill,
                    ),
                  ),
                  child: Container(
                      //text container
                      decoration: BoxDecoration(
                        color: Colors.black54,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(8),
                            bottomRight: Radius.circular(8)),
                      ),
                      margin: EdgeInsets.only(top: size.height * 0.21 //23
                          ),
                      padding: EdgeInsets.only(
                          left: size.width * 0.02,
                          right: size.width * 0.01,
                          top: size.height * 0.01),
                      height: size.height * 0.06,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.title,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.normal,
                                color: Colors.white,
                                fontSize: size.height * 0.023),
                          ),
                          Text(
                            widget.subtitle,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.normal,
                                color: Colors.white70,
                                fontSize: size.height * 0.018),
                          ),
                        ],
                      ))),
              Container(
                margin: EdgeInsets.only(top:size.height*0.29,left:size.width*0.8),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "1.12k",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                        fontSize: 12.0,
                      ),
                    ),
                    fav == false || fav == null
                        ? GestureDetector(
                            onTap: () {
                              setState(() {
                                fav = true;
                                stateManagment.setRestFave(true);
                              });
                            },
                            child: Icon(
                              Icons.favorite_border,
                              color: Colors.white,
                              size: 18,
                            ),
                          )
                        : GestureDetector(
                            onTap: () {
                              setState(() {
                                fav = false;
                                stateManagment.setRestFave(null);
                              });
                            },
                            child:
                                Icon(Icons.favorite, size: 18, color: Colors.red),
                          )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  myAppBarContainer() {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.1,
      color: Color(0xff101f40),
      child: Row(
        children: [
          IconButton(
              icon: Icon(
                Icons.chevron_left,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context, false);
              }),
          Spacer(),
          Text("Sand Duns",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 25)),
          Spacer(),
          IconButton(
              icon: Icon(
                Icons.notifications,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Notifications()));
              }),
        ],
      ),
    );
  }

  servicesCard() {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: 10, top: size.height * 0.3),
      height: size.height * 0.3,
      width: size.width * 0.9,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          text("Discription", size.height * 0.024, Colors.black, 1,
              FontWeight.bold, size.width),
          text(
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nisi lacus, aliquet eget tempus ut, pellentesque quis tellus. Sed posuere tincidunt urna in dignissim. Phasellus fringilla tincidunt velit, eget cursus nisl rutrum et. Ut condimentum ex et viverra dignissim. Pellentesque sem tellus, condimentum nec purus ut, auctor congue libero.",
              size.height * 0.02,
              Colors.black38,
              10,
              FontWeight.normal,
              size.width),
          SizedBox(height: size.height * 0.01),
          Row(
            children: [
              Spacer(),
              button(" Book Schedule ", () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Packages(
                              image: widget.image,
                              subtitle: widget.subtitle,
                              title: widget.title,
                            )));
              }, Color(0xff101f40)),
              Spacer(),
              button("Request a Quote", () {}, Color(0xff284E99)),
              Spacer(),
            ],
          ),
        ],
      ),
    );
  }

  text(String text, double height, Color color, int lines,
      FontWeight fontWeight, double width) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: width,
      margin: EdgeInsets.only(left: 10, top: 5),
      child: Text(
        text,
        maxLines: lines,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: fontWeight,
            color: color,
            fontSize: height),
      ),
    );
  }

//buttons
  button(String title, Function onTap, Color color) {
    return RaisedButton(
        child: Text(
          title,
          style: TextStyle(color: Colors.white),
        ),
        color: color,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        onPressed: onTap);
  }

  activityCard(
    String image,
  ) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {},
      child: Container(
        width: size.width * 0.45,
        height: size.height * 0.25,
        margin: EdgeInsets.only(bottom: 10, left: 10, top: 5, right: 5),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              spreadRadius: 1,
              blurRadius: 3,
              offset: Offset(1, 3), // changes position of shadow
            ),
          ],
          borderRadius: BorderRadius.all(Radius.circular(8)),
          image: DecorationImage(
            image: AssetImage(
              image,
            ),
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }
}
