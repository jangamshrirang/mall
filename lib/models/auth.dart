import 'package:hive/hive.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/services/default.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/widgets/w_loading.dart';

DioClient _dioClient = DioClient();

class AuthModel {
  AuthModel({
    this.userId,
    this.hashid,
    this.email,
    this.mobilePrefix,
    this.mobile,
    this.name,
    this.typeInfo,
    this.gender,
    this.bdate,
  });

  final int userId;
  final String hashid;
  final String email;
  final String mobilePrefix;
  final String mobile;
  final String name;
  final String typeInfo;
  final String gender;
  final dynamic bdate;

  factory AuthModel.fromJson(Map<dynamic, dynamic> json) => AuthModel(
      userId: json["user_id"] == null ? null : json["user_id"],
      hashid: json["hashid"] == null ? null : json["hashid"],
      email: json["email"],
      mobilePrefix: json["mobile_prefix"] == null ? null : json["mobile_prefix"],
      mobile: json["mobile"] == null ? null : json["mobile"],
      name: json["name"] == null ? null : json["name"],
      typeInfo: json["type_info"] == null ? null : json["type_info"],
      gender: json["gender"] == null ? null : json["gender"],
      bdate: json["bdate"]);

  Future<bool> loginMobile(number, {bool withVerify: false}) async {
    WLoading.type(Loading.show);
    bool verify;

    // Remove Special Character
    number = number.replaceAll(new RegExp(r'[^\w\s]+'), '');

    if (withVerify) {
      verify = await verifyAccount(number);
    }

    Map<String, dynamic> _body = {'mobile': number};

    APIResponse res = await _dioClient.publicPost('/auth/login/mobile-otp', data: _body);

    if (res.code >= 400 || (!verify && res.code >= 400 && !withVerify)) {
      return false;
    }

    if (res.data['token'] != null) {
      try {
        Helper.authInfo = AuthModel.fromJson(res.data['user']);
        Helper.state[StatesNames.token] = res.data['token'];

        final currentAuthUser = Hive.box('auth');
        await currentAuthUser.put('token', res.data['token']);
        await currentAuthUser.put('user', res.data['user']);

        currentAuthUser.put('isLoggedIn', true);

        await UserModel.fetchMyProfile();
      } catch (e) {
        print('ERROR: ${e.toString()}');
      }
    }
    WLoading.type(Loading.dismiss);

    return true;
  }

  Future<bool> loginEmail({String socialId, String name}) async {
    Map<String, dynamic> _body = {'social_id': socialId, 'name': name, 'base64': Default.avatar};

    WLoading.type(Loading.show);

    APIResponse res = await _dioClient.publicPost('/auth/login/google', data: _body);

    WLoading.type(Loading.dismiss);

    if (res.code >= 400) {
      return false;
    }

    if (res.data['token'] != null) {
      try {
        Helper.authInfo = AuthModel.fromJson(res.data['user']);
      } catch (e) {
        print('ERROR: ${e.toString()}');
      }

      Helper.state[StatesNames.token] = res.data['token'];
      Helper.setUserHive(user: res.data['user'], token: res.data['token']);
    }
    return true;
  }

  Future<bool> verifyAccount(String number) async {
    Map<String, dynamic> _body = {'mobile': number};
    APIResponse res = await _dioClient.publicPost('/auth/register/verify-mobile-otp', data: _body);
    if (res.code >= 400) {
      return false;
    }
    return true;
  }

  Future<void> fetchUserProfile({String hashId}) async {
    Map<String, dynamic> _body = {'hash_id': hashId};

    APIResponse res = await _dioClient.publicPost('/account/user-profile', data: _body);

    if (res.code >= 400) {
      return false;
    }
    return true;
  }

  Future<bool> checkCredential(String number) async {
    print('number: $number');

    WLoading.type(Loading.show);
    Map<String, dynamic> _body = {'mobile': number};

    APIResponse res = await _dioClient.publicPost('/auth/register/check-mobile', data: _body);

    WLoading.type(Loading.dismiss);

    if (res.code >= 400) {
      return false;
    }
    return true;
  }

  Future<bool> checkAuthentication() async {
    final auth = Hive.box('auth');

    APIResponse res = await _dioClient.privateGet('/check-authentication');

    if (res.code == 200) {
      Helper.authInfo = AuthModel.fromJson(res.data['user']);
      auth.put('isLoggedIn', true);
      Helper.state[StatesNames.token] = await auth.get('token');
      return true;
    }
    auth.put('isLoggedIn', false);
    return false;
  }

  Future<bool> logout() async {
    WLoading.type(Loading.show);
    APIResponse res = await _dioClient.privatePost('/logout');
    WLoading.type(Loading.dismiss);
    if (res.code >= 400) {
      return false;
    }

    final currentAuthUser = Hive.box('auth');
    currentAuthUser.delete('token');
    currentAuthUser.put('isLoggedIn', false);

    return true;
  }

  Future<bool> registration({String fullname, String mobile}) async {
    WLoading.type(Loading.show);

    Map<String, dynamic> _body = {'name': fullname, 'mobile': mobile};

    APIResponse res = await _dioClient.publicPost('/auth/register/mobile', data: _body);
    WLoading.type(Loading.dismiss);

    if (res.code >= 400) {
      return false;
    }

    return true;
  }
}

class StatusInfo {
  StatusInfo({
    this.text,
    this.type,
  });

  final String text;
  final String type;

  factory StatusInfo.fromJson(Map<String, dynamic> json) => StatusInfo(
        text: json["text"] == null ? null : json["text"],
        type: json["type"] == null ? null : json["type"],
      );
}
