import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WAvatarWidget extends StatefulWidget {
  final String imageUrl;
  final Function onTap;
  final double radius;

  WAvatarWidget({
    this.imageUrl: '',
    this.onTap,
    this.radius,
  });

  @override
  _WAvatarWidgetState createState() => _WAvatarWidgetState();
}

class _WAvatarWidgetState extends State<WAvatarWidget> {
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: widget.radius,
      child: Container(
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(widget.radius ?? 50.0),
        ),
        child: InkWell(
          onTap: widget.onTap,
          borderRadius: BorderRadius.circular(widget.radius ?? 50.0),
          child: CachedNetworkImage(
            imageUrl: widget.imageUrl,
            placeholder: (context, url) => Image.asset('assets/images/icon.png'),
          ),
        ),
      ),
    );
  }
}
