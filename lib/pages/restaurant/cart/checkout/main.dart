import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/extensions/w_money_formatter.dart';
import 'package:wblue_customer/providers/restaurant/checkout/p_user_address.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/services/helper.dart';

class RestaurantPlaceOrderPage extends StatefulWidget {
  final String cartId;
  final String note;

  const RestaurantPlaceOrderPage({this.cartId, this.note});

  @override
  _RestaurantPlaceOrderPageState createState() =>
      _RestaurantPlaceOrderPageState();
}

class _RestaurantPlaceOrderPageState extends State<RestaurantPlaceOrderPage> {
  Set<Marker> markers = Set();
  bool isSwitched = false;
  Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      context.read<PRestaurantUserAddress>().fetchLocalStorageAddress();
    });
  }

  @override
  Widget build(BuildContext context) {
    double mapWidth = MediaQuery.of(context).size.width;
    double mapHeight = MediaQuery.of(context).size.height * 0.5 - 215;
    double iconSize = 40.0;

    return Consumer<PRestaurantFoodCart>(
      builder: (context, cart, child) => Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(0.07.sh), // here the desired height
          child: AppBar(
            backgroundColor: Config.primaryColor,
            title: Column(
              children: [
                Text(
                  'Checkout',
                  style: TextStyle(fontSize: 42.sp),
                ),
                cart.restaurantCart.food.length > 0
                    ? Column(
                        children: [
                          SizedBox(height: 0.01.sh),
                          Text(
                            '${cart.restName}',
                            style: TextStyle(fontSize: 32.sp),
                          ),
                        ],
                      )
                    : SizedBox(),
              ],
            ),
          ),
        ),
        bottomNavigationBar: cart.restaurantCart.food.length > 0
            ? Consumer2<PRestaurantFoodCart, PRestaurantUserAddress>(
                builder: (context, checkCart, address, child) => Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0.0, 1.0), //(x,y)
                        blurRadius: 1.0,
                      ),
                    ]),
                    height: 0.13.sh,
                    child: BottomAppBar(
                      child: Consumer<PRestaurantFoodCart>(
                        builder: (context, cart, child) => Container(
                          padding: EdgeInsets.all(10.0),
                          child: RaisedButton(
                            onPressed: address.userAddress?.mobile == null
                                ? null
                                : () async {
                                    await cart.placeORderApi(
                                      WMallPayment.cod,
                                      context,
                                      generalNotes: widget.note,
                                    );
                                    ExtendedNavigator.of(context).replace(
                                        '/restaurants/order-confirmation');
                                  },
                            color: Config.primaryColor,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5.0))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '${cart.isCheckOutSuccessful ? 'PLACEORDER' : 'Loading'}',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 34.sp,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )),
              )
            : SizedBox(),
        body: Consumer<PRestaurantUserAddress>(
          builder: (context, providerAddress, child) => ListView(
            physics: ClampingScrollPhysics(),
            children: [
              Container(
                color: Config.primaryColor.withAlpha(16),
                padding: EdgeInsets.all(8),
                child: Row(
                  children: [
                    Container(
                      width: 0.005.sw,
                      color: Config.primaryColor,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Contactless Delivery',
                            style: TextStyle(
                                fontSize: 32.sp, fontWeight: FontWeight.w500),
                          ),
                          Text(
                            'Contactless delivery: your rider will place the order at your door',
                            style: TextStyle(
                                fontSize: 28.sp, color: Colors.black54),
                          )
                        ],
                      ),
                    ),
                    Transform.scale(
                      scale: 0.9,
                      child: CupertinoSwitch(
                        value: isSwitched,
                        onChanged: (bool value) {
                          setState(() {
                            isSwitched = value;
                            print(isSwitched);
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 0.005.sh),
              Container(
                color: Colors.black12,
                child: Stack(
                  children: [
                    providerAddress.isLoading
                        ? SizedBox()
                        : new Container(
                            width: mapWidth,
                            height: mapHeight,
                            child: GoogleMap(
                              zoomControlsEnabled: false,
                              mapType: MapType.normal,
                              scrollGesturesEnabled: false,
                              myLocationButtonEnabled: false,
                              buildingsEnabled: true,
                              zoomGesturesEnabled: false,
                              rotateGesturesEnabled: false,
                              tiltGesturesEnabled: false,
                              initialCameraPosition:
                                  providerAddress.usersLocation,
                              onMapCreated: (GoogleMapController controller) {
                                _controller.complete(controller);
                                providerAddress.setMapController(_controller);
                              },
                            )),
                    // ),
                    //               Align(
                    //                 alignment: Alignment.center,
                    //                 child: Container(
                    //                   child: Container(width: 0.032.sh, child: Image.asset('assets/images/pin.png')),
                    //                 ),
                    //               ),

                    new Positioned(
                      top: (mapHeight - iconSize) / 2,
                      right: (mapWidth - iconSize) / 2,
                      child: new Icon(Icons.person_pin_circle, size: iconSize),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 8.0, left: 8.0),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    providerAddress.isLoading
                        ? CircularProgressIndicator()
                        : _address(context, providerAddress),
                    Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Delivery Time',
                          style: TextStyle(
                              fontWeight: FontWeight.w500, fontSize: 34.sp),
                        ),
                        Row(
                          children: [
                            Text(
                              'ASAP',
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 28.sp,
                                  color: Colors.black45),
                            ),
                            IconButton(
                              onPressed: showDeliverySchedOption,
                              icon: Icon(MdiIcons.chevronRight),
                              iconSize: 52.sp,
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 0.02.sh),
              Container(
                padding: EdgeInsets.only(bottom: 8.0, left: 8.0),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Payment Methods',
                          style: TextStyle(
                              fontWeight: FontWeight.w500, fontSize: 34.sp),
                        ),
                        Row(
                          children: [
                            FlatButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              onPressed: () {
                                ExtendedNavigator.of(context)
                                    .push('/select-payment-method');
                              },
                              child: Text(
                                'Change',
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 28.sp,
                                    color: Config.primaryColor),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Image.asset('assets/images/payment-method/cash.png',
                            width: 0.08.sw),
                        SizedBox(width: 0.02.sw),
                        Text('Cash on delivery',
                            style: TextStyle(fontSize: 32.sp)),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 0.05.sw),
              Container(
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                        margin: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Payment summary',
                              style: TextStyle(
                                  fontSize: 34.sp, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 0.022.sh),
                            Container(
                              height: 0.2.sw,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Cart total',
                                        style: TextStyle(fontSize: 30.sp),
                                      ),
                                      Text(
                                        '${cart.restaurantCart.totalInfo.total.moneyFromNum()}',
                                        style: TextStyle(fontSize: 30.sp),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Delivery fee',
                                        style: TextStyle(fontSize: 30.sp),
                                      ),
                                      Text(
                                        '${cart.deliveryFee.moneyFromNum()}',
                                        style: TextStyle(fontSize: 30.sp),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Total Amount',
                                        style: TextStyle(
                                            fontSize: 32.sp,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        '${(cart.totalAmount.moneyFromNum())}',
                                        style: TextStyle(
                                            fontSize: 32.sp,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _address(BuildContext context, PRestaurantUserAddress address) {
    Widget addressLine = SizedBox();

    if (address.userAddress.type == address.addrTypes[0]['name']) {
      addressLine = Text(
          '${address.userAddress.street}, ${address.userAddress.house}',
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 30.sp,
              color: Colors.black54));
    } else if (address.userAddress.type == address.addrTypes[1]['name']) {
      addressLine = Text(
          '${address.userAddress.street}, ${address.userAddress.building}, ${address.userAddress.floor}, ${address.userAddress.office}',
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 30.sp,
              color: Colors.black54));
    } else if (address.userAddress.type == address.addrTypes[2]['name']) {
      addressLine = Text(
          '${address.userAddress.street}, ${address.userAddress.building}, ${address.userAddress.floor}, ${address.userAddress.apartmentNumber}',
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 30.sp,
              color: Colors.black54));
    } else {}

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
                '${address.userAddress.nickname ?? '${address.userAddress.street} (${address.userAddress.area})'}',
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 34.sp)),
            IconButton(
              onPressed: () => ExtendedNavigator.of(context).push(
                '/select-address',
                arguments: RestaurantCheckoutAddressPageArguments(
                    userAddressModel: address.userAddress),
              ),
              icon: Icon(MdiIcons.chevronRight),
              iconSize: 52.sp,
            )
          ],
        ),
        addressLine,
        address.userAddress.mobile != null
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 0.002.sh),
                  Text('${address.userAddress.mobile}',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 30.sp,
                          color: Colors.black54)),
                  address.userAddress.landline != null
                      ? Column(
                          children: [
                            SizedBox(height: 0.002.sh),
                            Text('${address.userAddress.landline}',
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 30.sp,
                                    color: Colors.black54)),
                          ],
                        )
                      : SizedBox(),
                ],
              )
            : Text('Please update your mobile number',
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 30.sp,
                    color: Colors.redAccent)),
      ],
    );
  }

  showDeliverySchedOption() {
    // FIXME: remove this if pre delivery is ready
    return BotToast.showText(text: 'Pre-delivery is coming soon!');
    //end

    int dayLimit = 3;

    List<DateTime> dateList = new List<DateTime>();
    DateTime today = new DateTime.now();

    for (int i = 0; i < dayLimit; i++) {
      dateList.add(today.add(new Duration(days: i)));
    }

    final DateFormat formatter = DateFormat('EEE, MMM. dd');

    showCupertinoModalPopup(
      context: context,
      builder: (context) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Color(0xffffffff),
                border: Border(
                  bottom: BorderSide(
                    color: Color(0xff999999),
                    width: 0.0,
                  ),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CupertinoButton(
                    child: Text('Cancel'),
                    onPressed: () => Navigator.of(context).pop(),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16.0,
                      vertical: 5.0,
                    ),
                  ),
                  CupertinoButton(
                    child: Text('Confirm'),
                    onPressed: () => Navigator.of(context).pop(),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16.0,
                      vertical: 5.0,
                    ),
                  )
                ],
              ),
            ),
            Container(
                height: 320.0,
                color: Color(0xfff7f7f7),
                child: Row(
                  children: [
                    Expanded(
                      child: CupertinoPicker(
                          onSelectedItemChanged: (value) => print('$value'),
                          scrollController:
                              FixedExtentScrollController(initialItem: 0),
                          magnification: 1.1,
                          itemExtent: 35,
                          children: List.generate(
                              dateList.length,
                              (index) => Container(
                                  margin: EdgeInsets.symmetric(vertical: 5.0),
                                  child: Text(
                                      '${formatter.format(dateList[index])}')))),
                    ),
                    Expanded(
                      child: CupertinoPicker(
                        onSelectedItemChanged: (value) => print('$value'),
                        scrollController:
                            FixedExtentScrollController(initialItem: 0),
                        magnification: 1.1,
                        itemExtent: 25,
                        children: [
                          Text('Test'),
                        ],
                        /* the rest of the picker */
                      ),
                    ),
                  ],
                ))
          ],
        );
      },
    );
  }
}
