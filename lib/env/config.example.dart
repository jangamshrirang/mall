import 'package:flutter/cupertino.dart';

class Pages {
  static String restaurant = 'restaurant';
  static String hotel = 'hotel';
  static String activity = 'activity';
  static String market = 'market';
}

class Config {
  static String appOrigin = 'POSTMAN-00000';
  static String pubKey = '';

  static bool _isLive = true;

  static List<String> comingSoonPages = [
    Pages.activity,
    Pages.hotel,
    Pages.market,
  ];

  static String _localhost = 'http://192.168.0.166';
  static int _localPort = 8080;

  static String _liveHost = "https://api.wmall.qa"; //http://160.153.249.245
  static int _livePort = 159;
  // static String socketHost = 'http://160.153.249.245:3001';

  static String socketLiveHost =
      'http://160.153.249.245:3000'; //http://192.168.0.117:3000

  // static String url = _isLive
  //     ? '$_liveHost:${_livePort.toString()}'
  //     : '$_localhost:${_localPort.toString()}';
  // static String apiUrl = '$url/api';

  static String api = _isLive ? '$_liveHost/api' : '$_localhost/api';

  static const agoraApi = 'bfc29c20e8934e9fa621508b8cc4817c';
  static const APP_ID = 'bfc29c20e8934e9fa621508b8cc4817c';
  static String googleMapAPI = 'AIzaSyCpZybhi4ZzUBXImzkKtaggzrOSsfcH8p4';
  static const Color primaryColor = Color(0xFF101F40);
  static const Color secondaryColor = Color(0xFF77D2FF);
  static const Color ternaryColor = Color(0xFFFF0000);
  static const Color bodyColor = Color(0xFFECECEC);
  static Color warningColor = Color(0xFFFED330);
  static Color dangerColor = Color(0xFFFD3967);
  static Color successColor = Color(0xFF40CC48);
  static Color dullColor = Color(0xFFD8E3F9);
  static Color aliceBlue = Color(0xFFEAF1F4);
  static String fontFamily = "Montserrat";

  static Map<String, String> testStripe = {
    'key': '',
    'secret': '',
  };

  static Map<String, String> liveStripe = {
    'key': '',
    'secret': '',
  };
}

class Constants {
  static const String Home = 'Home';
  static const String Messages = 'Messages';
  static const String MyAccount = 'My Account';
  static const String NeedHelp = 'Need Help';
  static const String Feedback = 'Feedback';

  static const List<String> choices = <String>[
    Home,
    Messages,
    MyAccount,
    NeedHelp,
    Feedback
  ];
}
