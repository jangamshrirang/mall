class MyOrderListModel {
    MyOrderListModel({
       this.orderHashId,
        this.full,
        this.thumbnail,
        this.restaurantName,
        this.shippingAddress,
        this.shippingLatitude,
        this.shippingLongitude,
        this.storeAddress,
        this.storeLatitude,
        this.storeLongitude,
        this.orderDate,
        this.orderTime,
        this.orderStatus,
    });

    String orderHashId;
    String full;
    String thumbnail;
    String restaurantName;
    String shippingAddress;
    String shippingLatitude;
    String shippingLongitude;
    String storeAddress;
    String storeLatitude;
    String storeLongitude;
    DateTime orderDate;
    String orderTime;
    String orderStatus;

    factory MyOrderListModel.fromJson(Map<String, dynamic> json) => MyOrderListModel(
        orderHashId: json["order_hash_id"] == null ? null : json["order_hash_id"],
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        restaurantName: json["restaurant_name"] == null ? null : json["restaurant_name"],
        shippingAddress: json["shipping_address"] == null ? null :json["shipping_address"],
        shippingLatitude: json["shipping_latitude"] == null ? null : json["shipping_latitude"],
        shippingLongitude: json["shipping_longitude"] == null ? null : json["shipping_longitude"],
        storeAddress: json["store_address"] == null ? null : json["store_address"],
        storeLatitude: json["store_latitude"] == null ? null : json["store_latitude"],
        storeLongitude: json["store_longitude"] == null ? null : json["store_longitude"],
        orderDate: json["order_date"] == null ? null : DateTime.parse(json["order_date"]),
        orderTime: json["order_time"] == null ? null : json["order_time"],
        orderStatus: json["order_status"] == null ? null : json["order_status"],
    );

    Map<String, dynamic> toJson() => {
        "order_hash_id": orderHashId == null ? null : orderHashId,
        "full": full == null ? null : full,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "restaurant_name": restaurantName == null ? null : restaurantName,
        // "shipping_address": shippingAddress == null ? null : shippingAddressValues.reverse[shippingAddress],
        "shipping_latitude": shippingLatitude == null ? null : shippingLatitude,
        "shipping_longitude": shippingLongitude == null ? null : shippingLongitude,
        "store_address": storeAddress == null ? null : storeAddress,
        "store_latitude": storeLatitude == null ? null : storeLatitude,
        "store_longitude": storeLongitude == null ? null : storeLongitude,
        "order_date": orderDate == null ? null : "${orderDate.year.toString().padLeft(4, '0')}-${orderDate.month.toString().padLeft(2, '0')}-${orderDate.day.toString().padLeft(2, '0')}",
        "order_time": orderTime == null ? null : orderTime,
        "order_status": orderStatus == null ? null : orderStatus,
    };
}