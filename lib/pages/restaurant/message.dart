import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/message.dart';
import 'package:wblue_customer/providers/restaurant/p_restaurant_message.dart';
import 'package:wblue_customer/providers/socket/client.dart';

class RestaurantMessagePage extends StatefulWidget {
  final String restaurantHashid;
  final String restaurantName;
  RestaurantMessagePage({this.restaurantHashid, this.restaurantName});

  @override
  _RestaurantMessagePageState createState() => _RestaurantMessagePageState();
}

class _RestaurantMessagePageState extends State<RestaurantMessagePage> {
  final msg = TextEditingController();

  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      context.read<PRestaurantMessage>().setUser();
    });
  }

  void onSend() {
    try {
      if (msg.text.length <= 0) return;

      context
          .read<PRestaurantMessage>()
          .onSend(message: msg.text, context: context);
      msg.clear();
    } catch (error) {
      print('Message not send [ Error : $error ]');
    }
  }

  @override
  Widget build(BuildContext context) {
    double pixelRatio = MediaQuery.of(context).devicePixelRatio;
    double px = 1 / pixelRatio;

    BubbleStyle styleSomebody = BubbleStyle(
      nip: BubbleNip.leftTop,
      color: Colors.white,
      elevation: 1 * px,
      margin: BubbleEdges.only(top: 8.0, right: 50.0),
      alignment: Alignment.topLeft,
    );
    BubbleStyle styleMe = BubbleStyle(
      nip: BubbleNip.rightTop,
      color: Color.fromARGB(255, 225, 255, 199),
      elevation: 1 * px,
      margin: BubbleEdges.only(top: 8.0, left: 50.0),
      alignment: Alignment.topRight,
    );

    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text(
            '${widget.restaurantName}',
            style: TextStyle(fontSize: 16.0),
          ),
          backgroundColor: Config.primaryColor,
        ),
        body: Consumer<PRestaurantMessage>(
            builder: (context, messageProvider, child) => Column(
                  children: [
                    Expanded(
                        child: Container(
                            color: Config.primaryColor.withAlpha(20),
                            child: ListView.builder(
                              padding: EdgeInsets.all(8.0),
                              itemBuilder: (context, index) {
                                // return Bubble(
                                //   alignment: Alignment.center,
                                //   color: Color.fromARGB(255, 212, 234, 244),
                                //   elevation: 1 * px,
                                //   margin: BubbleEdges.only(top: 8.0),
                                //   child: Text(
                                //       '${messageProvider.messages[index]}',
                                //       style: TextStyle(fontSize: 10)),
                                // );

                                RestaurantMessageModel message =
                                    RestaurantMessageModel.fromJson(
                                        messageProvider.messages[index]);

                                return Bubble(
                                  style: message.senderHashid ==
                                          messageProvider.hashid
                                      ? styleMe
                                      : styleSomebody,
                                  margin: BubbleEdges.only(top: 2.0),
                                  nip: BubbleNip.rightTop,
                                  child: Text('${message.body}'),
                                );

                                // Bubble(
                                //   style: styleSomebody,
                                //   child: Text(
                                //       'Hi Jason. Sorry to bother you. I have a queston for you.'),
                                // ),
                                // Bubble(
                                //   style: styleMe,
                                //   child: Text('Whats\'up?'),
                                // ),
                                // Bubble(
                                //   style: styleSomebody,
                                //   child: Text(
                                //       'I\'ve been having a problem with my computer.'),
                                // ),
                                // Bubble(
                                //   style: styleSomebody,
                                //   margin: BubbleEdges.only(top: 2.0),
                                //   nip: BubbleNip.no,
                                //   child: Text('Can you help me?'),
                                // ),
                                // Bubble(
                                //   style: styleMe,
                                //   child: Text('Ok'),
                                // ),
                                // Bubble(
                                //   style: styleMe,
                                //   nip: BubbleNip.no,
                                //   margin: BubbleEdges.only(top: 2.0),
                                //   child: Text('What\'s the problem?'),
                                // ),
                              },
                              itemCount: messageProvider.messages.length,
                            ))),
                    SafeArea(
                      top: false,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Expanded(
                                child: TextField(
                                  maxLines: null,
                                  controller: msg,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Enter your message',
                                  ),
                                ),
                              ),
                              IconButton(
                                icon: Icon(Icons.send),
                                color: Colors.blueAccent,
                                onPressed: onSend,
                              ),
                              IconButton(
                                icon: Icon(Icons.image),
                                color: Colors.blueAccent,
                                onPressed: () {},
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                )));
  }
}
