import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_badge.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_image.dart';

class WRoomTypeWidget extends StatelessWidget {
  final String name, type;
  final int left;
  final double newPrice, oldPrice;
  final bool canPayAtHotel, isRefundable;

  WRoomTypeWidget({
    this.name: '',
    this.type: '',
    this.left: 0,
    this.newPrice: 0,
    this.oldPrice: 0,
    this.canPayAtHotel: false,
    this.isRefundable: false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
//      height: 100.0,
      margin: EdgeInsets.only(bottom: 16.0),
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 100.0,
                width: 100.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    bottomLeft: Radius.circular(10.0),
                  ),
                ),
                child: WImageWidget(
                  url: '',
                  placeholder: ExactAssetImage('assets/images/samples/hotel.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(width: 16.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      name,
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
                    ),
                    Row(
                      children: <Widget>[
                        SvgPicture.asset(
                          'assets/svg/bed.svg',
                          height: 30,
                        ),
                        SizedBox(width: 8.0),
                        Text(type),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 8.0),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    WBadgeWidget(
                      label: '${left.toString()} Room(s) left',
                    ),
                    SizedBox(height: 8.0),
                    Wrap(
                      runSpacing: 4.0,
                      spacing: 4.0,
                      children: <Widget>[
                        WBadgeWidget(
                          label: 'Pay Now',
                          color: Colors.amberAccent,
                          fontSize: 10.0,
                        ),
                        canPayAtHotel
                            ? WBadgeWidget(
                                label: 'Pay at Hotel',
                                color: Colors.amberAccent,
                                fontSize: 10.0,
                              )
                            : SizedBox(),
                        isRefundable
                            ? WBadgeWidget(
                                label: 'Refundable',
                                color: Colors.amberAccent,
                                fontSize: 10.0,
                              )
                            : WBadgeWidget(
                                label: 'Non-Refundable',
                                color: Colors.amberAccent,
                                fontSize: 10.0,
                              ),
                        WBadgeWidget(
                          label: 'Room Only',
                          color: Colors.amberAccent,
                          fontSize: 10.0,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    Global.money(newPrice),
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
                  ),
                  SizedBox(width: 4.0),
                  Text(
                    Global.money(oldPrice),
                    style: TextStyle(fontSize: 10.0, decoration: TextDecoration.lineThrough),
                  ),
                  WButtonWidget(
                    title: 'Book Now',
                    onPressed: () {
                      print('book now tap');
                      ExtendedNavigator.of(context).root.push('/hotels/review-booking');
                    },
                  ),
                ],
              ),
            ],
          ),
          Divider(),
        ],
      ),
    );
  }
}
