
// class RListByFollowers {
//     RListByFollowers({
//         this.fullCover,
//         this.thumbnailCover,
//         this.hashId,
//         this.restaurant,
//         this.email,
//         this.contact,
//         this.city,
//         this.full,
//         this.thumbnail,
//         this.numberOfFollowers,
//         this.timings,
//         this.ratings,
//     });

//     String fullCover;
//     String thumbnailCover;
//     String hashId;
//     String restaurant;
//     String email;
//     String contact;
//     String city;
//     String full;
//     String thumbnail;
//     int numberOfFollowers;
//     List<Timing> timings;
//     Ratings ratings;

//     factory RListByFollowers.fromJson(Map<String, dynamic> json) => RListByFollowers(
//         fullCover: json["full_cover"] == null ? null : json["full_cover"],
//         thumbnailCover: json["thumbnail_cover"] == null ? null : json["thumbnail_cover"],
//         hashId: json["hash_id"] == null ? null : json["hash_id"],
//         restaurant: json["Restaurant"] == null ? null : json["Restaurant"],
//         email: json["Email"] == null ? null : json["Email"],
//         contact: json["Contact"] == null ? null : json["Contact"],
//         city: json["City"] == null ? null : json["City"],
//         full: json["full"] == null ? null : json["full"],
//         thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
//         numberOfFollowers: json["number_of_followers"] == null ? null : json["number_of_followers"],
//         timings: json["timings"] == null ? null : List<Timing>.from(json["timings"].map((x) => Timing.fromJson(x))),
//         ratings: json["ratings"] == null ? null : Ratings.fromJson(json["ratings"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "full_cover": fullCover == null ? null : fullCover,
//         "thumbnail_cover": thumbnailCover == null ? null : thumbnailCover,
//         "hash_id": hashId == null ? null : hashId,
//         "Restaurant": restaurant == null ? null : restaurant,
//         "Email": email == null ? null : email,
//         "Contact": contact == null ? null : contact,
//         "City": city == null ? null : city,
//         "full": full == null ? null : full,
//         "thumbnail": thumbnail == null ? null : thumbnail,
//         "number_of_followers": numberOfFollowers == null ? null : numberOfFollowers,
//         "timings": timings == null ? null : List<dynamic>.from(timings.map((x) => x.toJson())),
//         "ratings": ratings == null ? null : ratings.toJson(),
//     };
// }

// class Ratings {
//     Ratings({
//         this.count,
//         this.average,
//     });

//     int count;
//     int average;

//     factory Ratings.fromJson(Map<String, dynamic> json) => Ratings(
//         count: json["count"] == null ? null : json["count"],
//         average: json["average"] == null ? null : json["average"],
//     );

//     Map<String, dynamic> toJson() => {
//         "count": count == null ? null : count,
//         "average": average == null ? null : average,
//     };
// }

// class Timing {
//     Timing({
//         this.name,
//         this.storeOpen,
//         this.storeClose,
//     });

//     Name name;
//     String storeOpen;
//     String storeClose;

//     factory Timing.fromJson(Map<String, dynamic> json) => Timing(
//         name: json["name"] == null ? null : nameValues.map[json["name"]],
//         storeOpen: json["store_open"] == null ? null : json["store_open"],
//         storeClose: json["store_close"] == null ? null : json["store_close"],
//     );

//     Map<String, dynamic> toJson() => {
//         "name": name == null ? null : nameValues.reverse[name],
//         "store_open": storeOpen == null ? null : storeOpen,
//         "store_close": storeClose == null ? null : storeClose,
//     };
// }

// enum Name { MONDAY_SUNDAY }

// final nameValues = EnumValues({
//     "Monday - Sunday": Name.MONDAY_SUNDAY
// });

// class EnumValues<T> {
//     Map<String, T> map;
//     Map<T, String> reverseMap;

//     EnumValues(this.map);

//     Map<T, String> get reverse {
//         if (reverseMap == null) {
//             reverseMap = map.map((k, v) => new MapEntry(v, k));
//         }
//         return reverseMap;
//     }
// }
