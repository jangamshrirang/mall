import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import '../env/config.dart';

class WCardTitleLink extends StatelessWidget {
  final String title;
  final String linkTitle;
  final Widget widget;
  final Color backGroundColor;
  final Function onTap;

  WCardTitleLink({this.title, this.linkTitle: '', @required this.widget, this.backGroundColor: Colors.white, this.onTap()});

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      margin: EdgeInsets.all(3.0),
      elevation: 0.0,
      color: backGroundColor,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                AutoSizeText('$title', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                linkTitle != ''
                    ? Row(
                        children: [
                          GestureDetector(
                            onTap: () => onTap(),
                            child: AutoSizeText(
                              '$linkTitle',
                              style: TextStyle(color: Config.primaryColor, fontSize: 18),
                            ),
                          ),
                          Icon(
                            MdiIcons.chevronRight,
                            color: Config.primaryColor,
                            size: 24,
                          ),
                        ],
                      )
                    : SizedBox(),
              ],
            ),
            SizedBox(height: 8),
            widget
          ],
        ),
      ),
    );
  }
}
