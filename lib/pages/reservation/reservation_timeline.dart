import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/widgets/w_chip.dart';

class ReservationTimelinePage extends StatefulWidget {
  @override
  _ReservationTimelinePageState createState() => _ReservationTimelinePageState();
}

class _ReservationTimelinePageState extends State<ReservationTimelinePage> {
  Widget _tile(Types type, String date) {
    Color color = Colors.blueGrey;
    String status = 'Pending';
    if (type == Types.cancelled) {
      color = Colors.orange;
      status = 'Cancelled';
    } else if (type == Types.processing) {
      color = Colors.blue;
      status = 'Processing';
    } else if (type == Types.completed) {
      color = Colors.green;
      status = 'Completed';
    } else if (type == Types.expired) {
      color = Colors.red;
      status = 'Expired';
    }

    return ListTile(
      leading: Icon(
        Icons.radio_button_checked,
        color: color,
      ),
      title: Text(date),
      subtitle: Text(status),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Reservation Timeline'),
        centerTitle: true,
      ),
      body: ListView(
        padding: EdgeInsets.all(8.0),
        children: <Widget>[
          _tile(Types.completed, 'Apr 08, 2020 8:30 AM'),
          _tile(Types.processing, 'Apr 08, 2020 8:00 AM'),
          _tile(Types.pending, 'Apr 07, 2020 7:00 AM'),
        ],
      ),
    );
  }
}
