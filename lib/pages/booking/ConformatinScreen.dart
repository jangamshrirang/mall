import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/pages/Activities/ActivitySucces.dart';
import 'package:wblue_customer/pages/PaymentOption/Payment.dart';
import 'package:wblue_customer/widgets/w_button.dart';

import '../../widgets/w_qr.dart';

class ConfirmationScreen extends StatefulWidget {
  String guest, date, time, tableNumber, onlybooking;
  ConfirmationScreen(
      {this.time, this.date, this.tableNumber, this.guest, this.onlybooking});

  @override
  _ConfirmationScreenState createState() => _ConfirmationScreenState();
}

class _ConfirmationScreenState extends State<ConfirmationScreen> {
  @override
  Widget build(BuildContext context) {
    TableRow _tableRow(String label, String value) {
      return TableRow(children: [
        TableCell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(label),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(value),
          ),
        ),
      ]);
    }

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text(
                      'Verification',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 18.0),
                    ),
                  ),
                  SizedBox(height: 24.0),
                  FractionallySizedBox(
                    widthFactor: 0.5,
                    child: Image.asset('assets/images/table-chairs.png'),
                  ),
                  SizedBox(height: 8.0),
                  Text('Please Conform your booking'),
                  SizedBox(height: 8.0),
                  FractionallySizedBox(
                    widthFactor: 0.8,
                    child: Table(
                      border: TableBorder.all(color: Colors.grey),
                      children: [
                        _tableRow('Number of guest', '${widget.guest}'),
                        _tableRow('Date', '${stateManagment.date}'),
                        _tableRow('Time', '${stateManagment.time}'),
                        _tableRow('Table number', '${widget.tableNumber}'),
                        _tableRow('Seats', '4'),
                        _tableRow('Location', 'Indoor'),
                        _tableRow('Special Instruction', 'Sample'),
                        _tableRow('Ordered Dish', 'QAR 500.00'),
                      ],
                    ),
                  ),
                  SizedBox(height: 24.0),
                  FractionallySizedBox(
                    widthFactor: 0.8,
                    child: Text(
                      'Kindly note that your reservation will be valid until 15 minutes from the time of booking time. Kindly make sure to arrive before that or the reservation will be cancelled automatically.',
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Divider(),
                  FractionallySizedBox(
                    widthFactor: 0.7,
                    child: WButtonWidget(
                      title: 'confirm',
                      onPressed: () {
                        if (widget.onlybooking == "onlytable") {
                          ExtendedNavigator.of(context)
                              .root
                              .replace('/restaurants/book/table/complete');
                        } else {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Payment(
                                        from: "resr",
                                      )));
                        }
                      },
                    ),
                  ),
                  Divider(),
                  InkWell(
                    child: Text('Check Reservations'),
                    onTap: () {
                      ExtendedNavigator.of(context).root.push('/reservations');
                    },
                  ),
                  SizedBox(height: 50.0),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _showBarcode() {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            return Container(
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  )),
              child: Wrap(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Transaction Code',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      IconButton(
                        icon: Icon(Icons.close),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                  JQRWidget(
                    size: MediaQuery.of(context).size.width * 0.8,
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
