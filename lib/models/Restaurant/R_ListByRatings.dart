import 'dart:collection';

import 'package:wblue_customer/models/Restaurant/restaurant_review.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();

class RestaurantListModel {
  RestaurantListModel({
    this.fullCover,
    this.thumbnailCover,
    this.hashId,
    this.restaurant,
    this.email,
    this.storeInformation,
    this.contact,
    this.city,
    this.isFollowed,
    this.full,
    this.thumbnail,
    this.numberOfFollowers,
    this.schedule,
    this.ratings,
  });

  String fullCover;
  String thumbnailCover;
  String hashId;
  String restaurant;
  String email;
  String storeInformation;
  String contact;
  String city;
  String full;
  bool isFollowed;
  String thumbnail;
  int numberOfFollowers;
  Schedule schedule;
  Ratings ratings;

  factory RestaurantListModel.fromJson(Map<String, dynamic> json) =>
      RestaurantListModel(
        fullCover: json["full_cover"] == null ? null : json["full_cover"],
        thumbnailCover:
            json["thumbnail_cover"] == null ? null : json["thumbnail_cover"],
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        restaurant: json["Restaurant"] == null ? null : json["Restaurant"],
        email: json["Email"] == null ? null : json["Email"],
        storeInformation: json["Store_Information"] == null
            ? null
            : json["Store_Information"],
        isFollowed: json["is_followed"] == null ? false : true,
        contact: json["Contact"] == null ? null : json["Contact"],
        city: json["City"] == null ? null : json["City"],
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        numberOfFollowers: json["number_of_followers"] == null
            ? null
            : json["number_of_followers"],
        schedule:
            json["timings"] == null ? null : Schedule.fromJson(json["timings"]),
        ratings:
            json["ratings"] == null ? null : Ratings.fromJson(json["ratings"]),
      );

  Map<String, dynamic> toJson() => {
        "full_cover": fullCover == null ? null : fullCover,
        "thumbnail_cover": thumbnailCover == null ? null : thumbnailCover,
        "hash_id": hashId == null ? null : hashId,
        "Restaurant": restaurant == null ? null : restaurant,
        "Email": email == null ? null : email,
        "Contact": contact == null ? null : contact,
        "City": city == null ? null : city,
        "full": full == null ? null : full,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "number_of_followers":
            numberOfFollowers == null ? null : numberOfFollowers,
        "timings": schedule == null ? null : schedule.toJson(),
        "ratings": ratings == null ? null : ratings.toJson(),
      };

  static Future<Map> fetchRestaurantInfo(String hashid) async {
    Map<String, dynamic> body = {'restaurant_hash_id': hashid};

    APIResponse res =
        await _dioClient.publicPost('/restaurant/information', data: body);

    if (res.code >= 400) {
      return null;
    }

    return res.data;
  }

  static Future<List<RestaurantReviewModel>> fetchRestaurantReviews(
      String hashid) async {
    Map<String, dynamic> body = {'restaurant_hash_id': hashid};

    APIResponse res =
        await _dioClient.publicPost('/restaurant/reviews/list', data: body);

    if (res.code >= 400) {
      return new List<RestaurantReviewModel>();
    }

    return res.data
        .map<RestaurantReviewModel>(
            (json) => RestaurantReviewModel.fromJson(json))
        .toList();
  }

  static Future<List> fetchRestaurantImages(String hashid) async {
    Map<String, dynamic> body = {'restaurant_hash_id': hashid};

    APIResponse res =
        await _dioClient.publicPost('/restaurant/image/list', data: body);

    if (res.code >= 400) {
      return [];
    }

    return res.data;
  }

  static Future<Map> fetchRestaurantLocation(String hashid) async {
    Map<String, dynamic> body = {'restaurant_hashid': hashid};

    APIResponse res =
        await _dioClient.publicPost('/restaurant/location', data: body);

    if (res.code >= 400) {
      return {};
    }

    return res.data;
  }
}

class Ratings {
  Ratings({
    this.count,
    this.average,
  });

  int count;
  int average;

  factory Ratings.fromJson(Map<String, dynamic> json) => Ratings(
        count: json["count"] == null ? null : json["count"],
        average: json["average"] == null ? null : json["average"],
      );

  Map<String, dynamic> toJson() => {
        "count": count == null ? null : count,
        "average": average == null ? null : average,
      };
}

class Schedule {
  Schedule({
    this.now,
    this.status,
    this.next,
  });

  String now;
  String status;
  String next;

  factory Schedule.fromJson(Map<String, dynamic> json) => Schedule(
        now: json["now"] == null ? null : json["now"],
        status: json["status"] == null ? null : json["status"],
        next: json["next"] == null ? null : json["next"],
      );

  Map<String, dynamic> toJson() => {
        "now": now == null ? null : now,
        "status": status == null ? null : status,
        "next": next == null ? null : next,
      };
}

enum Next { CLOSE_AT_1900 }

final nextValues = EnumValues({"Close at: 19:00": Next.CLOSE_AT_1900});

enum Now { THE_1109 }

final nowValues = EnumValues({"11:09": Now.THE_1109});

enum Status { OPEN }

final statusValues = EnumValues({"Open": Status.OPEN});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
