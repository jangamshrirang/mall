import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../env/config.dart';
import 'package:wblue_customer/widgets/w_cinema_item.dart';
import 'package:wblue_customer/widgets/w_drawer.dart';
import 'package:wblue_customer/widgets/w_search_by_name.dart';

class CinemaPage extends StatefulWidget {
  @override
  _CinemaPageState createState() => _CinemaPageState();
}

class _CinemaPageState extends State<CinemaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Config.primaryColor,
        title: Text('Browse Cinemas'),
        centerTitle: true,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(30.0),
          child: Column(
            children: <Widget>[
              Text(
                '0 CINEMA FOUND',
                style: TextStyle(fontSize: 12.0, color: Config.dullColor),
              ),
              SizedBox(height: 14.0),
            ],
          ),
        ),
      ),
      drawer: WDrawerWidget('browse-cinema'),
      body: RefreshIndicator(
        onRefresh: () async {},
        child: Column(
          children: <Widget>[
            WSearchByNameWidget(
              type: Types.cinema,
              showFilter: false,
            ),
            SizedBox(height: 16.0),
            Expanded(
              child: ListView(
                shrinkWrap: true,
                children: List.generate(10, (index) {
                  return WCinemaItemWidget();
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
