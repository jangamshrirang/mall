class ReservationCartListModel {
    ReservationCartListModel({
        this.cartHashId,
        this.food,
        this.totalInfo,
    });

    String cartHashId;
    List<Food> food;
    List<dynamic> totalInfo;

    factory ReservationCartListModel.fromJson(Map<String, dynamic> json) => ReservationCartListModel(
        cartHashId: json["cart_hash_id"] == null ? null : json["cart_hash_id"],
        food: json["food"] == null ? null : List<Food>.from(json["food"].map((x) => Food.fromJson(x))),
        totalInfo: json["total_info"] == null ? null : List<dynamic>.from(json["total_info"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "cart_hash_id": cartHashId == null ? null : cartHashId,
        "food": food == null ? null : List<dynamic>.from(food.map((x) => x.toJson())),
        "total_info": totalInfo == null ? null : List<dynamic>.from(totalInfo.map((x) => x)),
    };
}

class Food {
    Food({
        this.quantity,
        this.onCheckout,
        this.isOrdered,
        this.foodStatus,
        this.foodInfo,
        this.foodvariation,
    });

    int quantity;
    int onCheckout;
    int isOrdered;
    int foodStatus;
    FoodInfo foodInfo;
    dynamic foodvariation;

    factory Food.fromJson(Map<String, dynamic> json) => Food(
        quantity: json["quantity"] == null ? null : json["quantity"],
        onCheckout: json["on_checkout"] == null ? null : json["on_checkout"],
        isOrdered: json["is_ordered"] == null ? null : json["is_ordered"],
        foodStatus: json["food_status"] == null ? null : json["food_status"],
        foodInfo: json["food_info"] == null ? null : FoodInfo.fromJson(json["food_info"]),
        foodvariation: json["foodvariation"],
    );

    Map<String, dynamic> toJson() => {
        "quantity": quantity == null ? null : quantity,
        "on_checkout": onCheckout == null ? null : onCheckout,
        "is_ordered": isOrdered == null ? null : isOrdered,
        "food_status": foodStatus == null ? null : foodStatus,
        "food_info": foodInfo == null ? null : foodInfo.toJson(),
        "foodvariation": foodvariation,
    };
}

class FoodInfo {
    FoodInfo({
        this.hashId,
        this.name,
        this.type,
        this.price,
        this.description,
        this.full,
        this.thumbnail,
    });

    String hashId;
    String name;
    dynamic type;
    int price;
    String description;
    String full;
    String thumbnail;

    factory FoodInfo.fromJson(Map<String, dynamic> json) => FoodInfo(
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        name: json["name"] == null ? null : json["name"],
        type: json["type"],
        price: json["price"] == null ? null : json["price"],
        description: json["description"] == null ? null : json["description"],
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "hash_id": hashId == null ? null : hashId,
        "name": name == null ? null : name,
        "type": type,
        "price": price == null ? null : price,
        "description": description == null ? null : description,
        "full": full == null ? null : full,
        "thumbnail": thumbnail == null ? null : thumbnail,
    };
}

class TotalInfoClass {
    TotalInfoClass({
        this.total,
        this.totalCount,
    });

    int total;
    int totalCount;

    factory TotalInfoClass.fromJson(Map<String, dynamic> json) => TotalInfoClass(
        total: json["total"] == null ? null : json["total"],
        totalCount: json["total_count"] == null ? null : json["total_count"],
    );

    Map<String, dynamic> toJson() => {
        "total": total == null ? null : total,
        "total_count": totalCount == null ? null : totalCount,
    };
}
