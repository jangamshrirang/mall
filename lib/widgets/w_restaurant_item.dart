import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/widgets/w_rating.dart';

class WRestaurantItemWidget extends StatefulWidget {
  String image,name;
  WRestaurantItemWidget({this.image,this.name});
  @override
  _WRestaurantItemWidgetState createState() => _WRestaurantItemWidgetState();
}

class _WRestaurantItemWidgetState extends State<WRestaurantItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 4.0),
      margin: EdgeInsets.only(
        bottom: 12.0,
      ),
      decoration: BoxDecoration(
        border: BorderDirectional(
          bottom: BorderSide(
            color: Colors.grey[300],
          ),
        ),
      ),
      child: InkWell(
        onTap: () {
          ExtendedNavigator.of(context).root.push('/restaurants/single');
        },
        child: Column(
          children: <Widget>[
            Container(
              height: 150.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                ),
                image: DecorationImage(
                  image: ExactAssetImage('assets/images/samples/restaurant.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Align(
                alignment: Alignment.topRight,
                child: Container(
                  padding: EdgeInsets.all(4.0),
                  decoration: BoxDecoration(
                    color: Colors.green,
                  ),
                  child: Text(
                    'OPEN',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            WRestaurantTileWidget(),
          ],
        ),
      ),
    );
  }
}

class WRestaurantTileWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset('assets/images/samples/restaurant-logo.png'),
      title: Text(
        'Restaurant Name',
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        'Restaurant Slogan Here',
        style: TextStyle(fontSize: 12.0),
      ),
      trailing: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          WRatingsWidget(5.0, size: 12.0, compact: true),
          Text(''),
        ],
      ),
    );
  }
}
