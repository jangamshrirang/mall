import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WInputWidget extends StatelessWidget {
  final String label;
  final String prefixText;
  final Function(String) onChanged;
  final String initialValue;
  final String customErrorText;
  final bool isRequired, showErrorText, isEmail, isNumber, isPassword;
  final Widget prefix;
  final int maxLength;

  WInputWidget({
    this.label: '',
    this.prefixText,
    this.maxLength,
    this.customErrorText,
    this.onChanged: functionArg,
    this.initialValue: '',
    this.isRequired: true,
    this.showErrorText: true,
    this.isEmail: false,
    this.isNumber: false,
    this.isPassword: false,
    this.prefix,
  });

  @override
  Widget build(BuildContext context) {
    String _value = initialValue;
    TextEditingController _controller = new TextEditingController.fromValue(
      TextEditingValue(
        text: _value,
        selection: new TextSelection.collapsed(
          offset: _value.length,
        ),
      ),
    );

    String _errorText;

    if (isRequired) {
      if (_value.length == 0 || customErrorText != null) {
        _errorText = customErrorText ?? '$label is required';
      }
    }

    TextInputType keyboardType = isEmail
        ? TextInputType.emailAddress
        : isNumber
            ? TextInputType.number
            : isPassword ? TextInputType.visiblePassword : TextInputType.text;

    return Container(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextField(
        controller: _controller,
        maxLength: maxLength,
        decoration: InputDecoration(
          hintText: label,
          filled: true,
          fillColor: Color(0xFF284d99).withOpacity(0.1),
          labelText: label,
          prefixText: prefixText,
          errorText: _errorText,
          errorStyle: showErrorText ? null : TextStyle(height: 0, fontSize: 0),
          prefix: prefix,
        ),
        textCapitalization: TextCapitalization.words,
        keyboardType: keyboardType,
        obscureText: isPassword,
        onChanged: (val) {
          onChanged(val);
        },
      ),
    );
  }
}

void functionArg(dynamic) {}
