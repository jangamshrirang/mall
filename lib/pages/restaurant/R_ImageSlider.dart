import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/R_ListByRatings.dart';
import 'package:wblue_customer/pages/restaurant/store/single_restaurant.dart';
import 'package:wblue_customer/providers/restaurant/pbrowse.dart';

class RestaurantAdsImageSlider extends StatefulWidget {
  @override
  _RestaurantAdsImageSliderState createState() =>
      _RestaurantAdsImageSliderState();
}

class _RestaurantAdsImageSliderState extends State<RestaurantAdsImageSlider> {
  int pageIndex = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PBrowseRestaurant>(
      builder: (context, restaurant, child) => restaurant.restaurants.length > 0
          ? Column(children: [
              Expanded(
                child: Swiper(
                  scale: 0.8,
                  onTap: (index) {
                    restaurant.swiperControl.stopAutoplay();
                    RestaurantListModel restaurantsModel =
                        RestaurantListModel.fromJson(
                            restaurant.restaurants[index]);

                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SingleRestaurantPage(
                                  restaurantListModel:
                                      restaurant.restaurants[index],
                                )));
                  },
                  onIndexChanged: (index) async {
                    setState(() {
                      pageIndex = index;
                    });
                  },
                  itemBuilder: (BuildContext context, int index) {
                    RestaurantListModel restaurantsModel =
                        RestaurantListModel.fromJson(
                            restaurant.restaurants[index]);
                    return CachedNetworkImage(
                      imageUrl: restaurantsModel?.full ?? '',
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.contain,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      placeholder: (context, url) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image:
                                NetworkImage(restaurantsModel?.thumbnail ?? ''),
                            fit: BoxFit.contain,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    );
                  },
                  // indicatorLayout: PageIndicatorLayout.COLOR,
                  autoplay: true,
                  controller: restaurant.swiperControl,
                  autoplayDelay: 3000,
                  duration: 1500,
                  itemCount: restaurant.restaurants.length,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AnimatedSmoothIndicator(
                    activeIndex: pageIndex,
                    count: restaurant.restaurants.length,
                    effect: ExpandingDotsEffect(
                      activeDotColor: Config.primaryColor,
                      dotColor: Colors.grey[350],
                      dotHeight: 05.0,
                      expansionFactor: 2,
                    ),
                  ),
                ],
              ),
            ])
          : Shimmer.fromColors(
              baseColor: Color.fromRGBO(0, 0, 0, 0.1),
              highlightColor: Color(0x44CCCCCC),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
            ),
    );
  }
}
