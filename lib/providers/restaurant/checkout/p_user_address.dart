import 'dart:async';
import 'dart:collection';

import 'package:auto_route/auto_route.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/extensions/e_auth_user.dart';
import 'package:wblue_customer/models/google/map.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();

class PRestaurantUserAddress with ChangeNotifier {
  UserAddressModel _userAddressModel;
  UserAddressModel get userOfficeAddressModel => _userAddressModel;

  UserAddressModel _userAddress;
  UserAddressModel get userAddress => _userAddress;

  CameraPosition _usersLocation;
  CameraPosition get usersLocation => _usersLocation;

  CameraPosition _firstUsersLocation;
  CameraPosition get firstUsersLocation => _firstUsersLocation;

  GoogleAddressModel _location;

  LatLng _latLng;
  LatLng get latLang => _latLng;

  Completer<GoogleMapController> _mapController = Completer();

  Completer<GoogleMapController> get mapController => _mapController;

  Completer<GoogleMapController> _changeMapController = Completer();
  Completer<GoogleMapController> get changeMapController => _changeMapController;

  static List<Map<String, dynamic>> _addrTypes = [
    {"name": "house", "status": 1, "hashid": "Y2vXWzeN3rJzPmxMOVK4bB0y"},
    {"name": "office", "status": 1, "hashid": "zwjEe2kMDVq8PqG6vWbRr7Qx"},
    {"name": "appartment", "status": 1, "hashid": "ZRQ97PyD5AJ4PJ28eoMNYVbl"}
  ];

  UnmodifiableListView<Map<String, dynamic>> get addrTypes => UnmodifiableListView<Map<String, dynamic>>(_addrTypes);

  String _addrTypeHashId = _addrTypes[0]['hashid'];
  String get addrTypeHashId => _addrTypeHashId;

  //Default
  TextEditingController _nicknameController = TextEditingController();
  TextEditingController _areaController = TextEditingController();
  TextEditingController _streetController = TextEditingController();
  TextEditingController _addDirectionController = TextEditingController();

  TextEditingController get nicknameController => _nicknameController;
  TextEditingController get areaController => _areaController;
  TextEditingController get streetController => _streetController;
  TextEditingController get addDirectionController => _addDirectionController;

  //Contact
  TextEditingController _mobileController = TextEditingController();
  TextEditingController _landLineController = TextEditingController();

  TextEditingController get mobileController => _mobileController;
  TextEditingController get landLineController => _landLineController;

  //Office
  TextEditingController _buildingController = TextEditingController();
  TextEditingController _floorController = TextEditingController();
  TextEditingController _officeController = TextEditingController();

  TextEditingController get buildingController => _buildingController;
  TextEditingController get floorController => _floorController;
  TextEditingController get officeController => _officeController;

  //House
  TextEditingController _houseController = TextEditingController();
  TextEditingController get houseController => _houseController;

  //Apartment
  TextEditingController _aparmentNo = TextEditingController();
  TextEditingController get aparmentNo => _aparmentNo;

  bool _isSaving = false;
  bool get isSaving => _isSaving;

  bool _isFetchingLoc = true;
  bool get isFetchingLoc => _isFetchingLoc;

  bool _isLoading = true;
  bool get isLoading => _isLoading;

  void setMapController(Completer<GoogleMapController> controller) {
    _mapController = controller;
  }

  void setChangeMapController(Completer<GoogleMapController> controller) {
    _changeMapController = controller;
  }

  Future<void> getBasicAddress(Box basicAddress) async {
    Map _basicAddress = await basicAddress.get('basic-address');
    Map<String, dynamic> addr = new Map<String, dynamic>.from(_basicAddress);
    _userAddress = UserAddressModel.fromJson(addr);
    _usersLocation = CameraPosition(
      target: LatLng(double.parse(_userAddress.latitude), double.parse(_userAddress.longitude)),
      zoom: ScreenUtil().pixelRatio * 8,
    );
  }

  Future<void> fetchLocalStorageAddress() async {
    try {
      setLoading(true);
      bool isLoggedIn = false;
      final currentAuthUser = Hive.box('auth');
      final _basicAddress = Hive.box('area');

      Map user = currentAuthUser.get('user');
      isLoggedIn = await currentAuthUser.get('isLoggedIn');

      if (user != null && isLoggedIn) {
        String hashId = user['hashid'];
        _userAddress = await hashId.address();
        if (_userAddress.hashid == null) {
          await getBasicAddress(_basicAddress);
          setLoading(false);
          return;
        }
        _usersLocation = CameraPosition(
          target: LatLng(double.parse(_userAddress.latitude), double.parse(_userAddress.longitude)),
          zoom: ScreenUtil().pixelRatio * 8,
        );
      } else {
        await getBasicAddress(_basicAddress);
      }
      setLoading(false);
    } catch (e) {
      print('Method fetchLocalStorageAddress throw Error: $e');
    }
  }

  void setFetchingLoc(bool status) {
    _isFetchingLoc = status;
    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }

  void setSaving(bool status) {
    _isSaving = status;
    notifyListeners();
  }

  void setUpMap() {
    // Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    setFetchingLoc(true);

    _usersLocation = CameraPosition(
      target: LatLng(_latLng.latitude, _latLng.longitude),
      zoom: ScreenUtil().pixelRatio * 8,
    );

    setFetchingLoc(false);
    notifyListeners();
  }

  setUpAddress(Map<String, dynamic> userAddress) {
    try {
      _userAddressModel = UserAddressModel.fromJson(userAddress);
      if (_userAddressModel.type == 'house') {
        _addrTypeHashId = _addrTypes[0]['hashid'];
      } else if (_userAddressModel.type == 'office') {
        _addrTypeHashId = _addrTypes[1]['hashid'];
      } else if (_userAddressModel.type == 'appartment') {
        _addrTypeHashId = _addrTypes[2]['hashid'];
      }
      notifyListeners();

      _latLng = LatLng(double.parse(_userAddressModel.latitude), double.parse(_userAddressModel.longitude));
      _nicknameController.text =
          _userAddressModel.nickname ?? '${_userAddressModel.street} (${_userAddressModel.area})';
      _areaController.text = _userAddressModel.area;
      _streetController.text = _userAddressModel.street;

      _buildingController.text = _userAddressModel.building;
      _floorController.text = _userAddressModel.floor;
      _officeController.text = _userAddressModel.office;

      _aparmentNo.text = _userAddressModel.apartmentNumber;

      _houseController.text = _userAddressModel.house;

      _addDirectionController.text = _userAddressModel.additionalDirections;
      _mobileController.text = _userAddressModel.mobile;
      _landLineController.text = _userAddressModel.landline;
    } catch (e) {
      print('method setUpAddress throw error : $e');
    }
  }

  getValue(TextEditingController value) {
    return value.text.length > 0 ? value.text : null;
  }

  Future<void> saveAddress() async {
    try {
      Map<String, dynamic> body = {
        "type_hashid": _addrTypeHashId,
        "is_current": 1,
        "nickname": getValue(_nicknameController),
        "area": getValue(_areaController),
        "street": getValue(_streetController),
        "house": getValue(_houseController),
        "latitude": _latLng.latitude,
        "longitude": _latLng.longitude,
        "additional_directions": getValue(_addDirectionController),
        "mobile": getValue(_mobileController),
        "landline": getValue(_landLineController),
        "building": getValue(_buildingController),
        "floor": getValue(_floorController),
        "apartment_number": getValue(_aparmentNo),
        "office": getValue(_officeController)
      };

      setSaving(true);
      APIResponse res = await _dioClient.privatePost('/customer/address/add', data: body);
      print('message: ${res.message}');

      if (res.code == 200) {
        await UserModel.fetchMyProfile();
        fetchLocalStorageAddress();
        setSaving(false);
        goToSavedLocation(_latLng);
        ExtendedNavigator.root.pop();
      } else {
        setSaving(false);
        BotToast.showText(text: 'Address already exist');
      }
    } catch (e) {
      print('saveAddress Error: $e');
    }
  }

  void setAddrType(String hashid) {
    _addrTypeHashId = hashid;
    notifyListeners();
  }

  void getData(LatLng latLng) async {
    try {
      print('google getting data starting...');
      _latLng = latLng;
      final client = Dio();
      setFetchingLoc(true);
      final url =
          'https://maps.googleapis.com/maps/api/geocode/json?latlng=${latLng.latitude},${latLng.longitude}&key=${Config.googleMapAPI}';
      setFetchingLoc(false);

      final response = await client.get(url);

      if (response.statusCode == 200) {
        List result = response.data['results'];
        _location = GoogleAddressModel.fromJson(result[0]);
        _streetController.text = _location.addressComponents[0].longName;
        _areaController.text = _location.addressComponents[1].longName;
        goToNewLocation(latLng);

        notifyListeners();
      } else {
        print('Field to fetch the location');
        throw GoogleAddressModel(addressComponents: []);
      }
    } catch (error) {
      print(error);
    }
  }

  void goToNewLocation(LatLng latLng) async {
    try {
      final GoogleMapController controller = await _changeMapController.future;

      print('loc 1: ${latLng.latitude} ${latLng.longitude}');
      CameraPosition newLoc = CameraPosition(
        target: LatLng(latLng.latitude, latLng.longitude),
        zoom: ScreenUtil().pixelRatio * 8,
      );

      Future.delayed(Duration(milliseconds: 100), () async {
        await controller.animateCamera(CameraUpdate.newCameraPosition(newLoc));
      });
    } catch (e) {
      print('Method goToNewLocation throw error: $e');
    }
    ExtendedNavigator.root.pop();
  }

  Future<bool> back() async {
    try {
      final GoogleMapController controller = await _changeMapController.future;
      print('map 2: ${controller.mapId}');
      controller.dispose();
      return true;
    } catch (e) {
      return false;
    }
  }

  void goToSavedLocation(LatLng latLng) async {
    try {
      final GoogleMapController controller = await _mapController.future;
      print('loc 2: ${latLng.latitude} ${latLng.longitude}');
      CameraPosition newLoc = CameraPosition(
        target: LatLng(latLng.latitude, latLng.longitude),
        zoom: ScreenUtil().pixelRatio * 8,
      );
      Future.delayed(Duration(milliseconds: 100), () async {
        await controller.animateCamera(CameraUpdate.newCameraPosition(newLoc));
      });
    } catch (e) {
      print('Method goToSavedLocation throw error: $e');
    }
  }
}
