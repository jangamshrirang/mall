import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

import 'L_AddressConfirmation.dart';

class L_CustomSize extends StatefulWidget {
  @override
  _L_CustomSizeState createState() => _L_CustomSizeState();
}

class _L_CustomSizeState extends State<L_CustomSize> {
  final List<dynamic> colors = [
    0xffFF5733,
    0xffFD28FC,
    0xff2CFFEE,
    0xffFFFFFF,
    0xffFF5733,
    0xffFD28FC,
    0xff2CFFEE,
    0xffFFFFFF,
  ];
  Future<File> imageFile;
  int colorSecondaryIndex;
  pickImageFromGallery(ImageSource source) {
    setState(() {
      imageFile = ImagePicker.pickImage(source: source);
    });
  }

  int shoulder, chest, weist, hips, length;
  String dropdownValue;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: appBarWithIcons(context, "Measurements"),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    width: double.infinity,
                    height: size.height * 0.5,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset("assets/botique/girl.png"),
                      ],
                    ),
                  ),
                  inches(size.height * 0.15, size.height * 0.09,
                      size.height * 0.02, "SHOULDER", shoulder),
                  inches(size.height * 0.15, size.height * 0.13,
                      size.height * 0.02, "CHEST", chest),
                  inches(size.height * 0.15, size.height * 0.18,
                      size.height * 0.02, "WEIST", weist),
                  inches(size.height * 0.15, size.height * 0.22,
                      size.height * 0.02, "HIPS", hips),
                  inches(size.height * 0.15, size.height * 0.47,
                      size.height * 0.01, "LENGTH", length),
                  Positioned(
                      right: size.height * 0.04, child: dropDown()) //dropDown
                ],
              ),
              SizedBox(height: size.height * 0.005),
              text("  Dont know your size ?", Colors.black45, Colors.white,
                  size.height * 0.02, false),
              SizedBox(height: size.height * 0.005),
              text("  Our Professional Female Taylor Will Help you !",
                  Colors.black, Colors.grey[200], size.height * 0.02, false),
              SizedBox(height: size.height * 0.005),
              text("  Choose color.", Colors.black45, Colors.white,
                  size.height * 0.02, false),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: size.width * 0.03),
                    width: size.width * 0.5,
                    height: size.height * 0.16,
                    // color: Colors.grey[200],
                    child: Wrap(
                        runSpacing: 5,
                        children: List.generate(
                            colors.length,
                            (index) =>
                                availableColors(Color(colors[index]), index))),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: size.width * 0.1),
                    child: Stack(
                      children: [
                        showImage(),
                        Positioned(
                          top: size.height * 0.09,
                          left: size.width * 0.15,
                          child: IconButton(
                              icon: Icon(
                                Icons.camera_alt,
                                color: Colors.grey,
                              ),
                              onPressed: () {
                                setState(() {
                                  pickImageFromGallery(ImageSource.gallery);
                                });
                              }),
                        )
                      ],
                    ),
                  )
                ],
              ),
              btns(),
              text(
                  "Our Professional Female Taylor Will Help you WITHIN 40 MINUTES.",
                  Colors.black,
                  Colors.white,
                  size.height * 0.018,
                  false),
            ],
          ),
        ));
  }

  text(String text, Color color, color2, double fontSize, bool center) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.03,
      padding: EdgeInsets.only(top: size.height * 0.002),
      margin:
          EdgeInsets.only(left: size.width * 0.03, right: size.width * 0.03),
      width: double.infinity,
      decoration: BoxDecoration(
        color: color2,
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: center == true
          ? Center(
              child: Text(text,
                  style: TextStyle(color: color, fontSize: fontSize)))
          : Text(text, style: TextStyle(color: color, fontSize: fontSize)),
    );
  }

//image picker
  Widget showImage() {
    var size = MediaQuery.of(context).size;
    return FutureBuilder<File>(
      future: imageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return Container(
            width: size.width * 0.3,
            height: size.height * 0.16,
            decoration: BoxDecoration(
                image: DecorationImage(
              fit: BoxFit.fill,
              image: FileImage(snapshot.data),
            )),
          );
        } else if (snapshot.error != null) {
          return const Text(
            'Error Picking Image',
            textAlign: TextAlign.center,
          );
        } else {
          return Container(
            margin: EdgeInsets.only(right: size.width * 0.05),
            child: IconButton(
                icon: Icon(
                  Icons.add_a_photo,
                  color: Colors.grey,
                ),
                onPressed: () {
                  setState(() {
                    pickImageFromGallery(ImageSource.gallery);
                  });
                }),
          );
        }
      },
    );
  }

//inches
  Future _showIntDialog(String title) async {
    final theme = Theme.of(context);
    await showDialog<int>(
      context: context,
      builder: (BuildContext context) {
        return Theme(
            data: theme.copyWith(
                dialogBackgroundColor: Colors.black,
                accentColor: Colors.white, // highlted color
                textTheme: theme.textTheme.copyWith(
                  headline5: theme.textTheme.headline5
                      .copyWith(color: Colors.red), //other highlighted style
                  bodyText2: theme.textTheme.headline5
                      .copyWith(color: Colors.white38), //not highlighted styles
                )),
            child: NumberPickerDialog.integer(
              minValue: 0,
              maxValue: 100,
              step: 1,
              initialIntegerValue: 30,
            ));
      },
    ).then((num value) {
      if (value != null) {
        setState(() {
          title == "SHOULDER"
              ? shoulder = value
              : title == "CHEST"
                  ? chest = value
                  : title == "WEIST"
                      ? weist = value
                      : title == "HIPS"
                          ? hips = value
                          : title == "LENGTH"
                              ? length = value
                              : print(value);
        });
      }
    });
  }

//color selection
  availableColors(Color color, int index) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        changeColorSecondaryIndex(index, color);
      },
      child: Container(
        height: size.height * 0.05,
        width: size.width * 0.1,
        margin: EdgeInsets.only(left: size.width * 0.01),
        decoration: BoxDecoration(
          color: Color(
            colors[index],
          ),
          borderRadius: BorderRadius.all(Radius.circular(5)),
          border: Border.all(
              width: 2,
              color: colorSecondaryIndex == index
                  ? Config.primaryColor
                  : Colors.black38),
        ),
      ),
    );
  }

  void changeColorSecondaryIndex(int index, Color color) {
    setState(() {
      colorSecondaryIndex = index;
      print(color);
    });
  }

  button(String title, Function onTap, Color color) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.4,
      child: RaisedButton(
          child: Text(
            title,
            style: TextStyle(color: Colors.white),
          ),
          color: color,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          onPressed: onTap),
    );
  }

  btns() {
    var size = MediaQuery.of(context).size;
    return Row(
      children: [
        SizedBox(width: size.height * 0.01),
        button("Locate your place", () async {
          LocationResult result = await showLocationPicker(
            context,
            "AIzaSyCpZybhi4ZzUBXImzkKtaggzrOSsfcH8p4",
            myLocationButtonEnabled: true,
            layersButtonEnabled: true,
          );

          print("result = ${result}");
          setState(() {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => L_AddressConfirmation(
                          address: result.address,
                        )));
          });
        }, Config.primaryColor),
        Spacer(),
        Spacer(),
        button("Buy Now", () {
      Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => L_AddressConfirmation(
                          address: "any",
                        )));
        }, Config.primaryColor),
        Spacer(),
      ],
    );
  }

//stack arow design
  inches(double left, top, imageHeigt, String title, int inches) {
    var size = MediaQuery.of(context).size;
    return Row(
      children: [
        Container(
          //leftmargin,topmargin,image height,
          margin: EdgeInsets.only(left: left, top: top),
          child: Row(
            children: [
              title == "LENGTH"
                  ? SizedBox(
                      width: size.width * 0.1,
                    )
                  : SizedBox(
                      width: size.width * 0,
                    ),
              Image.asset("assets/botique/Leftarrow.png", height: imageHeigt),
              Container(
                height: size.height * 0.03,
                width: size.width * 0.2,
                decoration: BoxDecoration(
                  color: Color(0xffF47797),
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: Center(
                    child: Text(title, style: TextStyle(color: Colors.white))),
              ),
              SizedBox(
                width: size.width * 0.01,
              ),
              GestureDetector(
                onTap: () {
                  _showIntDialog(title);
                },
                child: Container(
                  height: size.height * 0.03,
                  width: size.width * 0.2,
                  decoration: BoxDecoration(
                    color: Color(0xff585052),
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  child: Center(
                      child: Column(
                    children: [
                      Spacer(),
                      Text(inches == null ? "Inches" : "$inches " + "cm",
                          style: TextStyle(
                            color: Colors.white,
                          )),
                      Text(
                          //this is just for decoraton pink line under this
                          "         Inches                  Inches          ",
                          maxLines: 1,
                          style: TextStyle(
                              color: Colors.transparent,
                              fontSize: 5,
                              decoration: TextDecoration.underline,
                              decorationThickness: 5,
                              decorationColor: Color(0xffF47797))),
                      Spacer(),
                    ],
                  )),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  dropDown() {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.04,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
          color: Color(0xffF47797), borderRadius: BorderRadius.circular(10)),
      child: Theme(
        data: Theme.of(context).copyWith(canvasColor: Color(0xffF47797)),
        child: DropdownButton<String>(
            value: dropdownValue,
            hint: Text(
              "My Size",
              style: TextStyle(color: Colors.white),
            ),
            icon: Icon(Icons.keyboard_arrow_down, color: Colors.white),
            iconSize: 22,
            underline: SizedBox(),
            onChanged: (String newValue) {
              setState(() {
                dropdownValue = newValue;
              });
            },
            items: <String>['My Size', 'My Sister', 'Others']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(
                  value,
                  style: TextStyle(color: Colors.white),
                ),
              );
            }).toList()),
      ),
    );
  }
}
