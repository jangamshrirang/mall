import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:wblue_customer/models/Restaurant/food_variation.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();

class PRestaurantFoodVariation with ChangeNotifier {
  String _foodHashid = '';

  List<RestaurantFoodVariationModel> _variations = new List<RestaurantFoodVariationModel>();

  UnmodifiableListView<RestaurantFoodVariationModel> get variations =>
      UnmodifiableListView<RestaurantFoodVariationModel>(_variations);

  Future<void> setVariation(List<RestaurantFoodVariationModel> variations) async {
    _variations = variations;
    for (int i = 0; i < _variations.length; i++) {
      for (int j = 0; j < _variations[i].variationTypeSelections.length; j++) {
        _variations[i].variationTypeSelections[j].isSelected = false;
      }
    }
    notifyListeners();
  }

  //TODO:unused
  // Future<void> fetchVariation(String foodHashid) async {
  //   _foodHashid = foodHashid;
  //   _variations = await fetchVariationApi();

  //   notifyListeners();
  // }

  // Future<List<RestaurantFoodVariationModel>> fetchVariationApi() async {
  //   try {
  //     APIResponse res = await _dioClient.publicPost(
  //         '/restaurant/variation-list',
  //         data: {'food_hashid': _foodHashid});

  //     if (res.code >= 400) {
  //       return new List<RestaurantFoodVariationModel>();
  //     }
  //     return res.data
  //         .map<RestaurantFoodVariationModel>(
  //             (json) => RestaurantFoodVariationModel.fromJson(json))
  //         .toList();
  //   } catch (e) {
  //     print('Method (fetchAdvertisements) error: $e');
  //     return new List<RestaurantFoodVariationModel>();
  //   }
  // }
}
