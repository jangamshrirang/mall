import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

class UnderMaintanence extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, " "),
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/account/bg-2.png"))),
          child: Center(
              child: Image.asset("assets/images/account/maintanence.png")),
        ),
      ),
    );
  }
}
