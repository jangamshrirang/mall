import 'dart:convert';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';
import '../../../env/config.dart';
import 'package:wblue_customer/providers/market/p_product_recognizer.dart';
import 'package:wblue_customer/providers/market/p_view_product.dart';
import 'package:wblue_customer/widgets/w_image_product_card.dart';
import 'package:wblue_customer/widgets/w_items_wrapper.dart';
import 'package:wblue_customer/widgets/w_rating.dart';

class ProductRecognizerPage extends StatefulWidget {
  @override
  _ProductRecognizerPageState createState() => _ProductRecognizerPageState();
}

class _ProductRecognizerPageState extends State<ProductRecognizerPage> {
//  final ImagePicker _picker = ImagePicker();
  List<Asset> images = List<Asset>();
  List<File> _files;
  List products = new List();

  String _error = 'No Error Dectected';
//  PickedFile _imageFile;
//  File pickedImage;
  var text = '';
  bool imageLoaded = false;

//  Future pickImage() async {
////    var awaitImage = await _picker.getImage(source: ImageSource.camera);
//
////    setState(() {
////      pickedImage = File(awaitImage.path);
////      imageLoaded = true;
////    });
//
//    FirebaseVisionImage visionImage = FirebaseVisionImage.fromFile(pickedImage);
//
//    final ImageLabeler cloudLabeler = FirebaseVision.instance.cloudImageLabeler();
//
//    final List<ImageLabel> cloudLabels = await cloudLabeler.processImage(visionImage);
//
//    for (ImageLabel label in cloudLabels) {
//      final double confidence = label.confidence;
//      setState(() {
//        text = "$text ${label.text}   ${confidence.toStringAsFixed(2)}\n";
//        print(text);
//      });
//    }
//
//    cloudLabeler.close();
//  }

  @override
  void initState() {
    super.initState();
    init();
  }

  Future<void> loadProduct() async {
    String data =
        await rootBundle.loadString('assets/data/scanned-products.json');
    products = json.decode(data);
  }

  Future<void> init() async {
    List recognizedResult = new List();
    List<Asset> assets = await selectImagesFromGallery();

    // final filePath =
    //     await FlutterAbsolutePath.getAbsolutePath(assets[0].identifier);
    // FirebaseVisionImage visionImage =
    //     FirebaseVisionImage.fromFile(File(filePath));

    // final ImageLabeler cloudLabeler =
    //     FirebaseVision.instance.cloudImageLabeler();

    // final List<ImageLabel> cloudLabels =
    //     await cloudLabeler.processImage(visionImage);

    // //DANGER SHOULD QUERY IN QPI
    // for (ImageLabel label in cloudLabels) {
    //   print(label.text);

    //   for (int i = 0; i < products.length; i++) {
    //     List tags = products[i]['tags'];
    //     bool isContained = tags.contains(label.text.toLowerCase());
    //     if (isContained) {
    //       recognizedResult.add(products[i]);
    //       products.removeAt(i);
    //     }
    //   }
    // }

    // cloudLabeler.close();
    if (!mounted) return;
    print('--recognizedResult-- $recognizedResult');
    if (recognizedResult.length <= 0) {
      context.read<PProductRecognizer>().setError('0 Result');
      return;
    }
    context.read<PProductRecognizer>().setFile(recognizedResult);
  }

  Future<List<Asset>> selectImagesFromGallery() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 1,
        enableCamera: true,
        materialOptions: MaterialOptions(
          actionBarColor: "#FF147cfa",
          statusBarColor: "#FF147cfa",
        ),
      );
    } on Exception catch (e) {
      BotToast.showText(text: '$e');

      if (products.length <= 0) {
        context.read<PProductRecognizer>().setError(e.toString());
      }
      return resultList;
    }

    if (resultList.length > 0) {
      context.read<PProductRecognizer>().setError('');
      context.read<PProductRecognizer>().setFile(new List());

      await loadProduct();
    }
    return resultList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Results'),
        elevation: 0.0,
        backgroundColor: Config.primaryColor,
        automaticallyImplyLeading: true,
        actions: [
          FlatButton(
            onPressed: () => init(),
            child: Icon(MdiIcons.camera, color: Colors.white),
          )
        ],
      ),
      body: Consumer<PProductRecognizer>(
        builder: (__, label, _) {
          return label.error == ''
              ? label.products.length > 0
                  ? _productCard(context, label.products)
                  : Center(child: CircularProgressIndicator())
              : Center(child: Text('${label.error}'));
        },
      ),
    );
  }

  Widget _productCard(BuildContext context, List products) {
    return WItemList(
      crossAxisCellCount: 2,
      crossAxisCount: 4,
      widget: (product, index) => Container(
        width: MediaQuery.of(context).size.width * 0.5,
        child: WProductImageCard(
          onTap: () {
            context.read<PViewProduct>().setProduct(product);
            ExtendedNavigator.of(context).push('/view-product');
          },
          isVertical: true,
          imageUrl: '${product['images'][0]['small']}',
          widget: Container(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AutoSizeText(
                '${product['name']}',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                minFontSize: 16,
              ),
              SizedBox(height: 10.0),
              AutoSizeText(
                '${product['shortDescription']}',
                style: TextStyle(fontSize: 16),
                maxLines: 4,
                overflow: TextOverflow.ellipsis,
                minFontSize: 14,
              ),
              SizedBox(height: 10.0),
              Container(
                child: Row(
                  children: <Widget>[
                    AutoSizeText(
                      'QR ${product['newPrice']}',
                      style: TextStyle(fontSize: 18, color: Colors.redAccent),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      minFontSize: 16,
                    ),
                    SizedBox(width: 8.0),
                    product['oldPrice'] != null
                        ? Expanded(
                            child: AutoSizeText(
                              'QR ${product['oldPrice']}',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.grey,
                                  decoration: TextDecoration.lineThrough),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
              SizedBox(height: 10.0),
              Row(
                children: [
                  Icon(
                    MdiIcons.checkboxBlankCircle,
                    size: 16.0,
                    color: product['availibilityCount'] > 0
                        ? Colors.green.withOpacity(0.9)
                        : Colors.red.withOpacity(0.9),
                  ),
                  SizedBox(width: 8.0),
                  AutoSizeText(
                    '${product['availibilityCount'] > 0 ? 'In' : 'Out'} stock, ${product['availibilityCount'] > 1 ? '${product['availibilityCount']} units' : '${product['availibilityCount']} unit'}',
                    style: TextStyle(
                        fontSize: 16,
                        color: product['availibilityCount'] > 0
                            ? Colors.green.withOpacity(0.9)
                            : Colors.red.withOpacity(0.9)),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    minFontSize: 16,
                  ),
                ],
              ),
              SizedBox(height: 3.0),
              Row(
                children: [
                  WRatingsWidget(product['ratingsValue'] / 100),
                  SizedBox(width: 2),
                  AutoSizeText(
                    '(${product['ratingsCount']})',
                    style: TextStyle(
                        fontSize: 16, color: Colors.grey.withOpacity(0.9)),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    minFontSize: 16,
                  ),
                ],
              ),
              SizedBox(height: 3.0),
              AutoSizeText(
                '15 Reviews',
                style: TextStyle(fontSize: 16, color: Colors.grey),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                minFontSize: 16,
              ),
            ],
          )),
        ),
      ),
      items: products,
    );
  }
}
