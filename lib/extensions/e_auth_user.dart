import 'package:hive/hive.dart';
import 'package:wblue_customer/models/user.dart';

extension LoggedInUser on String {
  Future<UserAddressModel> address() async {
    try {
      final profile = Hive.box('profile');
      Map<dynamic, dynamic> _profile = await profile.get(this);
      Map<String, dynamic> addr = new Map<String, dynamic>.from(_profile['address']);
      UserAddressModel userAddress = UserAddressModel.fromJson(addr);
      return userAddress;
    } catch (e) {
      print('Extension LoggedInUser error: $e');
    }
  }
}
