import 'package:wblue_customer/models/Restaurant/R_ListByRatings.dart';

class RestaurantAdvertisementModel {
  RestaurantAdvertisementModel(
      {this.hashid,
      this.name,
      this.email,
      this.information,
      this.schedules,
      this.adRatings,
      this.numberOfFollowers,
      this.contact,
      this.city,
      this.timestamp,
      this.imageFull,
      this.imageThumbnail,
      this.profilePhotoFull,
      this.profilePhotoThumb,
      this.coverPhotoFull,
      this.coverPhotoThumb,
      this.isFollowed});

  String hashid;
  String name;
  dynamic email;
  String information;
  Schedule schedules;
  Ratings adRatings;
  int numberOfFollowers;
  String contact;
  String city;
  String timestamp;
  bool isFollowed;
  String imageFull;
  String imageThumbnail;
  String profilePhotoFull;
  String profilePhotoThumb;
  String coverPhotoFull;
  String coverPhotoThumb;

  factory RestaurantAdvertisementModel.fromJson(Map<String, dynamic> json) =>
      RestaurantAdvertisementModel(
        hashid: json["hashid"] == null ? null : json["hashid"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"],
        information: json["information"] == null ? null : json["information"],
        schedules: json["schedules"] == null
            ? null
            : Schedule.fromJson(json["schedules"]),
        adRatings:
            json["ratings"] == null ? null : Ratings.fromJson(json["ratings"]),
        numberOfFollowers: json["number_of_followers"] == null
            ? null
            : json["number_of_followers"],
        contact: json["contact"] == null ? null : json["contact"],
        city: json["city"] == null ? null : json["city"],
        isFollowed: json["is_followed"] == null ? false : true,
        timestamp: json["timestamp"] == null ? null : json["timestamp"],
        imageFull: json["image_full"] == null ? null : json["image_full"],
        imageThumbnail:
            json["image_thumbnail"] == null ? null : json["image_thumbnail"],
        profilePhotoFull: json["profile_photo_full"] == null
            ? null
            : json["profile_photo_full"],
        profilePhotoThumb: json["profile_photo_thumb"] == null
            ? null
            : json["profile_photo_thumb"],
        coverPhotoFull:
            json["cover_photo_full"] == null ? null : json["cover_photo_full"],
        coverPhotoThumb: json["cover_photo_thumb"] == null
            ? null
            : json["cover_photo_thumb"],
      );

  Map<String, dynamic> toJson() => {
        "hashid": hashid == null ? null : hashid,
        "name": name == null ? null : name,
        "email": email,
        "information": information == null ? null : information,
        "schedules": schedules == null ? null : schedules.toJson(),
        "ratings": adRatings == null ? null : adRatings.toJson(),
        "number_of_followers":
            numberOfFollowers == null ? null : numberOfFollowers,
        "contact": contact == null ? null : contact,
        "city": city == null ? null : city,
        "timestamp": timestamp == null ? null : timestamp,
        "image_full": imageFull == null ? null : imageFull,
        "image_thumbnail": imageThumbnail == null ? null : imageThumbnail,
        "profile_photo_full":
            profilePhotoFull == null ? null : profilePhotoFull,
        "profile_photo_thumb":
            profilePhotoThumb == null ? null : profilePhotoThumb,
        "cover_photo_full": coverPhotoFull == null ? null : coverPhotoFull,
        "cover_photo_thumb": coverPhotoThumb == null ? null : coverPhotoThumb,
      };
}
