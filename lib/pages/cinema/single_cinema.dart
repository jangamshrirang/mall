import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wblue_customer/widgets/w_about_property.dart';
import 'package:wblue_customer/widgets/w_cinema_profile_header.dart';
import 'package:wblue_customer/widgets/w_movie_item.dart';

class SingleCinemaPage extends StatefulWidget {
  @override
  _SingleCinemaPageState createState() => _SingleCinemaPageState();
}

class _SingleCinemaPageState extends State<SingleCinemaPage> {
  LatLng location = LatLng(37.43296265331129, -122.08832357078792);
  String dummyText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum facilisis commodo congue. Etiam quis feugiat metus. Suspendisse sollicitudin tincidunt nisl, vitae scelerisque magna dapibus at. Mauris ornare in odio eu efficitur. Duis tempus lorem in rutrum varius. Morbi convallis nulla at ligula ultrices luctus. Aenean vitae porta nisi. Pellentesque sed dui eget est lobortis ultrices vitae nec libero. Aliquam consectetur augue vitae lectus venenatis dapibus. Nam vel risus eget est elementum vulputate. Integer vulputate velit quis quam iaculis eleifend. Maecenas id magna dolor. Donec accumsan elit ac aliquam tincidunt. Nulla cursus risus dui, at interdum felis maximus vel.';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Column(
          children: <Widget>[
            WCinemaProfileHeaderWidget(),
            WAboutPropertyWidget(latLng: location, about: dummyText),
            Column(
              children: List.generate(
                10,
                (index) {
                  return Card(
                    margin: EdgeInsets.only(bottom: 16.0),
                    child: WMovieItemWidget(),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
