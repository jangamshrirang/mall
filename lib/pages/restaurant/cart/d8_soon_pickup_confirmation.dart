import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/widgets/w_button.dart';

class PickupConfirmationPage extends StatefulWidget {
  String rImage, rName, totalprice;
  PickupConfirmationPage({this.totalprice, this.rImage, this.rName});
  @override
  _PickupConfirmationPageState createState() => _PickupConfirmationPageState();
}

class _PickupConfirmationPageState extends State<PickupConfirmationPage> {
  @override
  Widget build(BuildContext context) {
    TableRow _tableRow(String label, String value) {
      return TableRow(children: [
        TableCell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(label),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(value),
          ),
        ),
      ]);
    }

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    stateManagment.rName,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
                  ),
                ),
                FractionallySizedBox(
                  widthFactor: 0.4,
                  child: Image.network(stateManagment.rImage),
                ),
                SizedBox(height: 24.0),
                FractionallySizedBox(
                  widthFactor: 0.4,
                  child: Image.asset('assets/images/serve.png'),
                ),
                Icon(
                  Icons.check_circle,
                  color: Colors.green,
                  size: 64.0,
                ),
                Text('Thank You for your Pickup Order'),
                SizedBox(height: 24.0),
                SizedBox(height: 8.0),
                FractionallySizedBox(
                  widthFactor: 0.8,
                  child: Table(
                    border: TableBorder.all(color: Colors.grey),
                    children: [
                      // _tableRow('Ref Number', '#123'),
                      // _tableRow('Special Instruction', 'Sample'),
                      _tableRow('Total Amount', "${stateManagment.totalPrice}"),
                    ],
                  ),
                ),
                SizedBox(height: 24.0),
                FractionallySizedBox(
                  widthFactor: 0.8,
                  child: Text(
                    'Kindly present the QR Code when you arrived at the restaurant',
                    textAlign: TextAlign.center,
                  ),
                ),
                Divider(),
                FractionallySizedBox(
                  widthFactor: 0.7,
                  child: WButtonWidget(
                    title: 'Back to Home',
                    onPressed: () {
                      ExtendedNavigator.of(context).root.push('/');
                    },
                  ),
                ),
                Divider(),
                InkWell(
                  child: Text('Check Orders'),
                  onTap: () {
                    ExtendedNavigator.of(context).root.push('/reservations');
                  },
                ),
                SizedBox(height: 50.0),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
