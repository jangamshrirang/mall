import 'package:custom_radio_grouped_button/CustomButtons/ButtonTextStyle.dart';
import 'package:custom_radio_grouped_button/CustomButtons/CustomRadioButton.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/widgets/w_button.dart';

class ColorVariations extends StatefulWidget {
  ColorVariations({Key key}) : super(key: key);

  @override
  _ColorVariationsState createState() => _ColorVariationsState();
}

class _ColorVariationsState extends State<ColorVariations> {
  List<IconData> iconList = [
    Icons.settings,
    Icons.bookmark,
    Icons.account_circle
  ];

  int secondaryIndex = 0;
  final List<dynamic> colors = [0xffFF5733, 0xff2B2625, 0xff5052E8];

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: colors.length,
          itemBuilder: (BuildContext context, int index) {
            return customRadio2(Color(colors[index]), index);
          }),
    );
  }

  ///changing the animation of color
  void changeSecondaryIndex(int index, Color color) {
    setState(() {
      secondaryIndex = index;
      print(color);
    });
  }

//custom radio buttons
  Widget customRadio2(Color color, int index) {
    return GestureDetector(
      onTap: () {
        changeSecondaryIndex(index, color);
      },
      child: Container(
        margin: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          color: Color(
            colors[index],
          ),
          border: Border.all(
              width: 4, color: secondaryIndex == index ? color : Colors.white),
          shape: BoxShape.circle,
        ),
        width: 35.0,
      ),
    );
  }
}

//bottomsheet
variatioBottomSheet(BuildContext context) {
  List<String> variations = [
    'Small',
    'Medium',
    'Large',
  ];

  var size = MediaQuery.of(context).size;
  showModalBottomSheet(
    context: context,
    backgroundColor: Colors.transparent,
    isScrollControlled: true,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState2) {
          return Container(
            height: size.height * 0.4,
            padding: EdgeInsets.all(16.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0),
                )),
            child: Container(
              margin: EdgeInsets.only(left: size.width * 0.1),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Size",
                    style: TextStyle(fontSize: 25),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CustomRadioButton(
                    width: size.width * 0.25,
                    elevation: 2,
                    absoluteZeroSpacing: false,
                    enableButtonWrap: false,
                    unSelectedColor: Colors.white,
                    buttonLables: variations,
                    buttonValues: variations,
                    buttonTextStyle: ButtonTextStyle(
                        selectedColor: Colors.red,
                        unSelectedColor: Colors.black,
                        textStyle: TextStyle(fontSize: 16)),
                    radioButtonValue: (value) {
                      print(value);
                    },
                    selectedColor: Colors.white,
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Colors",
                    style: TextStyle(fontSize: 25),
                  ),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 10.0),
                      height: 25.0,
                      width: size.width,
                      child: ColorVariations()),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 0.0),
                    child: WButtonWidget(
                      title: 'Done',
                      expanded: true,
                      onPressed: () {
                        Navigator.pop(context);
                        // setState(() {
                        // Cart('Restaurant Name').add(CartItem(name: 'noodle', price: 100, qty: qty));
                        //
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) =>
                        //             DeliveryOption()));
                        // });
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    },
  );
}
