import 'package:auto_route/auto_route.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/home/Services/serviceCarosel.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/w_arrival_selection.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_checking_dates.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_input.dart';

class HotelReviewBookingPage extends StatefulWidget {
  @override
  _HotelReviewBookingPageState createState() => _HotelReviewBookingPageState();
}

class _HotelReviewBookingPageState extends State<HotelReviewBookingPage> {
  String dummyText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum facilisis commodo congue. Etiam quis feugiat metus. Suspendisse sollicitudin tincidunt nisl, vitae scelerisque magna dapibus at. Mauris ornare in odio eu efficitur. Duis tempus lorem in rutrum varius. Morbi convallis nulla at ligula ultrices luctus. Aenean vitae porta nisi. Pellentesque sed dui eget est lobortis ultrices vitae nec libero. Aliquam consectetur augue vitae lectus venenatis dapibus. Nam vel risus eget est elementum vulputate. Integer vulputate velit quis quam iaculis eleifend. Maecenas id magna dolor. Donec accumsan elit ac aliquam tincidunt. Nulla cursus risus dui, at interdum felis maximus vel.';
  bool __mySelf = true;

  String promoCode = '';

  int index;

  Map<String, dynamic> body = {
    'email': 'username@email.com',
    'firstName': 'Juan',
    'lastName': 'Dela Cruz',
    'namePrefix': 'mr',
    'numberPrefix': '+974',
    'number': '30004000',
  };

  bool hasError = true;
  bool agreed = false;
  bool withTravelProtection = false;

  @override
  Widget build(BuildContext context) {
        var size = MediaQuery.of(context).size;
    checkError();

    return Scaffold(
      appBar: AppBar(
        title: Text('Review Booking'),
        centerTitle: true,
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {});
        },
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
            child: Column(
              children: <Widget>[
                _basicCard(),
                _promoCard(),
                _checkingDates(),
                _totalAmountCard(),
                _userDetailsCard(),
                WArrivalSelectionWidget(
                  onSelected: ($index) {
                    setState(() {
                      index = $index;
                    });
                  },
                ),
                _agreement(),
                _travelProtection(),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(offset: Offset(0, -0.5), color: Colors.black12, spreadRadius: 1.0),
          ],
        ),
        padding: EdgeInsets.all(16.0),
        child: Wrap(
          children: <Widget>[
          //        Container(
          //                color: Colors.white,
          //          margin: EdgeInsets.only(top:size.height*0.005),
          //          child: Exercise(
          //   title: "Do you want to make private transcation!",
          // ),
          //        ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Total Payable Amount'),
                SizedBox(width: 8.0),
                Text(
                  Global.money(withTravelProtection ? 692.1 + 55 : 692.1),
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  child: WButtonWidget(
                    title: 'Pay At Hotel',
                    subtitle: 'REFUNDABLE',
                    onPressed: !hasError && agreed && index != null
                        ? () {
                            Global.payAtHotel = true;
                            ExtendedNavigator.of(context).root.push('/hotels/confirmation');
                          }
                        : null,
                  ),
                ),
                SizedBox(width: 8.0),
                Expanded(
                  child: WButtonWidget(
                    title: 'Pay Now',
                    subtitle: 'NON-REFUNDABLE',
                    onPressed: !hasError && agreed && index != null
                        ? () {
                            Global.payAtHotel = false;
                            ExtendedNavigator.of(context).root.push('/hotels/payment');
                          }
                        : null,
                  ),
                ),
              ],
            ),
                  Container(
                         color: Colors.white,
                   margin: EdgeInsets.only(top:size.height*0.005),
                   child: Exercise(
            title: "Do you want to make private transcation!",
          ),
                 ),
          ],
        ),
      ),
    );
  }

  Widget _basicCard() {
    return Card(
      margin: EdgeInsets.only(bottom: 16.0),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Hotel Name',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 8.0),
                      Text(
                        dummyText,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 14.0),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Container(
                    height: 100.0,
                    width: 100.0,
                    child: WImageWidget(
                      url: '',
                      placeholder: ExactAssetImage('assets/images/samples/hotel.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
            Divider(),
            Text(
              'Twin Room',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text('Accomudation Only'),
            SizedBox(height: 8.0),
            Text(
              'Wed 29 Apr 2020 - Thu 30 Apr 2020',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text('1Room, 1Night(s)'),
          ],
        ),
      ),
    );
  }

  Widget _promoCard() {
    return Card(
      margin: EdgeInsets.only(bottom: 16.0),
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: WInputWidget(
                label: 'Promo Code',
                isRequired: false,
                initialValue: promoCode,
                onChanged: (val) {
                  setState(() {
                    promoCode = val.toUpperCase();
                  });
                },
              ),
            ),
            WButtonWidget(
              title: 'Use',
              onPressed: promoCode.length > 0 ? () {} : null,
            ),
          ],
        ),
      ),
    );
  }

  Widget _checkingDates() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Check-In and Check-Out Dates',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 8.0),
        WCheckingDatesWidget(),
        SizedBox(height: 16.0),
      ],
    );
  }

  Widget _totalAmountCard() {
    return Card(
      margin: EdgeInsets.only(bottom: 16.0),
      child: Container(
        padding: EdgeInsets.all(16.0),
        color: Colors.green[100],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Total Payable Amount',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
                ),
                Text(
                  Global.money(692.1),
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
                ),
              ],
            ),
            SizedBox(height: 8.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('1Room(s) | 2 Guest(s) | 1Night(s)'),
                Text('All Inclusive'),
              ],
            ),
            SizedBox(height: 8.0),
            DottedBorder(
              color: Colors.green,
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: Center(
                  child: RichText(
                    text: TextSpan(
                      text: 'Saves up to ',
                      children: [
                        TextSpan(text: Global.money(108.2), style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(text: ' on this booking.'),
                      ],
                      style: TextStyle(
                        color: Colors.green,
                        fontSize: 12.0,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _userDetailsCard() {
    TextStyle __bold = TextStyle(fontWeight: FontWeight.bold);

    return Card(
      margin: EdgeInsets.only(bottom: 16.0),
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text('I\'m booking for', style: __bold),
                    SizedBox(width: 8.0),
                    InkWell(
                      onTap: () {
                        setState(() {
                          __mySelf = true;
                        });
                      },
                      child: Row(
                        children: <Widget>[
                          Icon(__mySelf ? Icons.radio_button_checked : Icons.radio_button_unchecked, color: Colors.amber),
                          Text('Myself', style: __bold),
                        ],
                      ),
                    ),
                    SizedBox(width: 8.0),
                    InkWell(
                      onTap: () {
                        setState(() {
                          __mySelf = false;
                        });
                      },
                      child: Row(
                        children: <Widget>[
                          Icon(!__mySelf ? Icons.radio_button_checked : Icons.radio_button_unchecked, color: Colors.amber),
                          Text('Someone Else', style: __bold),
                        ],
                      ),
                    ),
                  ],
                ),
                WInputWidget(
                  label: 'Email Address',
                  isRequired: true,
                  initialValue: body['email'],
                  showErrorText: false,
                  isEmail: true,
                  onChanged: (val) {
                    setState(() {
                      body['email'] = val;
                    });
                  },
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    DropdownButton(
                      hint: Text('Prefix'),
                      onChanged: (val) {
                        setState(() {
                          body['namePrefix'] = val;
                        });
                      },
                      value: body['namePrefix'],
                      items: [
                        'mr',
                        'ms',
                        'mrs'
                      ].map<DropdownMenuItem>((item) {
                        return DropdownMenuItem(
                          value: item,
                          child: Text(Global.capitalize(item)),
                        );
                      }).toList(),
                    ),
                    SizedBox(width: 4.0),
                    Expanded(
                      flex: 5,
                      child: WInputWidget(
                        label: 'First Name',
                        showErrorText: false,
                        initialValue: body['firstName'],
                        onChanged: (val) {
                          setState(() {
                            body['firstName'] = val;
                          });
                        },
                      ),
                    ),
                    SizedBox(width: 4.0),
                    Expanded(
                      flex: 5,
                      child: WInputWidget(
                        label: 'Last Name',
                        showErrorText: false,
                        initialValue: body['lastName'],
                        onChanged: (val) {
                          setState(() {
                            body['lastName'] = val;
                          });
                        },
                      ),
                    ),
                  ],
                ),
                WInputWidget(
                  label: 'Phone Number',
                  showErrorText: false,
                  isRequired: true,
                  initialValue: body['number'],
                  isNumber: true,
                  onChanged: (val) {
                    setState(() {
                      body['number'] = val;
                    });
                  },
                  prefix: DropdownButton(
                    value: body['numberPrefix'],
                    isDense: true,
                    onChanged: (val) {},
                    items: [
                      '+974',
                    ].map((item) {
                      return DropdownMenuItem(
                        value: item,
                        child: Text(item),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _agreement() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            InkWell(
              onTap: () {
                setState(() {
                  agreed = !agreed;
                });
              },
              child: Icon(agreed ? Icons.check_box : Icons.check_box_outline_blank, color: Colors.amber),
            ),
            Text('I agree with '),
            InkWell(
              onTap: () {
                __showContentModal('Booking Conditions', dummyText);
              },
              child: Text(
                'booking conditions',
                style: TextStyle(color: Colors.amber, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        SizedBox(height: 16.0),
      ],
    );
  }

  Widget _travelProtection() {
    String text = 'Get protection for accident or baggage lost (non-refundable)...';

    return Card(
      margin: EdgeInsets.only(bottom: 16.0),
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Travel Protection',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 8.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.security,
                  size: 50.0,
                  color: Colors.amber,
                ),
                SizedBox(width: 16.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(text),
                      InkWell(
                        onTap: () {
                          __showContentModal('Terms & Conditions', dummyText);
                        },
                        child: Text(
                          'Terms & Conditions',
                          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.amber),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 16.0),
                InkWell(
                  onTap: () {
                    setState(() {
                      withTravelProtection = !withTravelProtection;
                    });
                  },
                  child: Column(
                    children: <Widget>[
                      Icon(
                        withTravelProtection ? Icons.check_box : Icons.check_box_outline_blank,
                        color: Colors.amber,
                      ),
                      SizedBox(width: 8.0),
                      Text(
                        Global.money(55),
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10.0),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void checkError() {
    hasError = false;
    if (body['email'].length == 0) {
      hasError = true;
    }
    if (body['firstName'].length == 0) {
      hasError = true;
    }
    if (body['lastName'].length == 0) {
      hasError = true;
    }
    if (body['number'].length == 0) {
      hasError = true;
    }
  }

  void __showContentModal(String title, String content) {
    Global.simpleModal(
      context,
      title: title,
      children: [
        SingleChildScrollView(
          child: Text(content, textAlign: TextAlign.justify),
        )
      ],
    );
  }
}
