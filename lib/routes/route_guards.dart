import 'package:auto_route/auto_route.dart';
import 'package:hive/hive.dart';
import 'package:wblue_customer/extensions/e_auth_user.dart';
import 'package:wblue_customer/models/auth.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/widgets/w_loading.dart';

import 'router.gr.dart';

// guard
class AuthGuard extends RouteGuard {
  Helper helper = Helper();
  AuthModel auth = AuthModel();
  bool isAuthenticated;

  @override
  Future<bool> canNavigate(
    ExtendedNavigatorState navigator,
    String routeName,
    Object arguments,
  ) async {
    WLoading.type(Loading.show);
    final bool isAuth = await auth.checkAuthentication();
    WLoading.type(Loading.dismiss);

    if (isAuth) {
      Global.user = UserModel.createDummy();
      return true;
    } else {
      bool isSuccess = await navigator.root.pushLoginPage().then((value) async {
        if (value == null) {
          return false;
        }
        return value;
      });

      print('************************ $isSuccess **************************');
      return isSuccess;
    }
  }
}

class LocGuard extends RouteGuard {
  @override
  Future<bool> canNavigate(
    ExtendedNavigatorState navigator,
    String routeName,
    Object arguments,
  ) async {
    try {
      bool isLoggedIn = false;

      final auth = Hive.box('auth');
      final area = Hive.box('area');

      isLoggedIn = auth.get('isLoggedIn');

      Map user = auth.get('user');

      if (user != null && isLoggedIn) {
        String hashId = user['hashid'];
        UserAddressModel userAddressModel = await hashId.address();
        Map basicAddress = area.get('basic-address');

        if (userAddressModel.hashid == null && basicAddress == null) {
          bool isSuccess = await navigator.root.pushLocationPage().then((value) async {
            if (value == null) {
              return false;
            }
            return value;
          });
          return isSuccess;
        } else {
          return true;
        }
      } else {
        Map basicAddress = area.get('basic-address');

        if (basicAddress == null) {
          bool isSuccess = await navigator.root.pushLocationPage().then((value) async {
            if (value == null) {
              return false;
            }
            return value;
          });
          return isSuccess;
        } else {
          return true;
        }
      }
    } catch (e) {
      print('Method LocGuard error: $e');
    }
  }
}
