import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WModalContainerWidget extends StatelessWidget {
  final String title;
  final List<Widget> children;
  final bool isWrap;

  WModalContainerWidget({
    this.title: '',
    this.children,
    this.isWrap: true,
  });
  @override
  Widget build(BuildContext context) {
    List<Widget> _children = [];
    _children.add(Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ],
    ));
    if (children != null) {
      _children.addAll(children);
    }
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(30.0)),
      ),
      child: isWrap
          ? SafeArea(child: Wrap(children: _children))
          : SafeArea(child: ListView(children: _children)),
    );
  }
}
