import 'package:auto_route/auto_route.dart';
import 'package:dash_chat/dash_chat.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';

class MessageRoomPage extends StatefulWidget {
  ChatUser user;

  MessageRoomPage({this.user});
  @override
  _MessageRoomPageState createState() => _MessageRoomPageState();
}

class _MessageRoomPageState extends State<MessageRoomPage> {
  final GlobalKey<DashChatState> _chatViewKey = GlobalKey<DashChatState>();
  // bMVO95wlAznDzqZxLK1aQ84E sensei
  // L2Nyjdx5b1n5vm8elpOvZQKz emulator

  // void onSend(ChatMessage message) {
  //   context.read<PSocketClient>().setMessage(message);
  // }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => ExtendedNavigator.of(context).root.pop(),
          ),
          backgroundColor: Config.primaryColor,
          title: Text(widget.user.name),
          elevation: 0.0,
        ),
        // body: SafeArea(
            // child:
            //     Selector<PRestaurantMessage, Tuple2<List<ChatMessage>, ChatUser>>(
            //   builder: (_, messages, __) {
            //     if (messages.item1.length > 0 &&
            //         _chatViewKey.currentState != null) {
            //       _chatViewKey.currentState.scrollController
            //         ..animateTo(
            //           _chatViewKey
            //               .currentState.scrollController.position.maxScrollExtent,
            //           curve: Curves.easeOut,
            //           duration: const Duration(milliseconds: 300),
            //         );
            //     }

            //     return messages.item2.customProperties?.containsKey('hashid') !=
            //             null
            //         ? DashChat(
            //             key: _chatViewKey,
            //             inverted: false,
            //             // onSend: onSend,
            //             onSend: (msg) {},
            //             sendOnEnter: true,
            //             textInputAction: TextInputAction.send,
            //             user: widget.user,
            //             inputDecoration: InputDecoration.collapsed(
            //                 hintText: "Add message here..."),
            //             dateFormat: DateFormat('yyyy-MMM-dd'),
            //             timeFormat: DateFormat('HH:mm'),
            //             messages: messages.item1,
            //             showUserAvatar: true,
            //             showAvatarForEveryMessage: false,
            //             scrollToBottom: true,
            //             dateBuilder: (date) {
            //               return Text(date);
            //             },
            //             messageBuilder: (message) {
            //               return Row(
            //                 mainAxisAlignment:
            //                     message.user.uid == messages.item2.uid
            //                         ? MainAxisAlignment.start
            //                         : MainAxisAlignment.end,
            //                 mainAxisSize: MainAxisSize.min,
            //                 children: [
            //                   Flexible(
            //                     child: Container(
            //                       decoration: BoxDecoration(
            //                           color: Color(0xff284d99),
            //                           borderRadius: BorderRadius.circular(5)),
            //                       margin: EdgeInsets.symmetric(vertical: 3.0),
            //                       child: Padding(
            //                         padding: const EdgeInsets.all(10.0),
            //                         child: AutoSizeText(
            //                           message.text,
            //                           style: TextStyle(color: Colors.white),
            //                           textAlign: TextAlign.left,
            //                         ),
            //                       ),
            //                     ),
            //                   ),
            //                 ],
            //               );
            //             },
            //             onPressAvatar: (ChatUser user) {
            //               print("OnPressAvatar: ${user.name}");
            //             },
            //             onLongPressAvatar: (ChatUser user) {
            //               print("OnLongPressAvatar: ${user.name}");
            //             },
            //             inputMaxLines: 5,
            //             messageContainerPadding:
            //                 EdgeInsets.only(left: 5.0, right: 5.0),
            //             alwaysShowSend: true,
            //             inputTextStyle: TextStyle(fontSize: 16.0),
            //             inputContainerStyle: BoxDecoration(
            //               border: Border.all(width: 0.0),
            //               color: Colors.white,
            //             ),
            //             onQuickReply: (Reply reply) {},
            //             onLoadEarlier: () {
            //               print("laoding...");
            //             },
            //             shouldShowLoadEarlier: false,
            //             showTraillingBeforeSend: true,
            //             trailing: <Widget>[
            //               IconButton(
            //                 icon: Icon(Icons.photo),
            //                 onPressed: () async {},
            //               )
            //             ],
            //           )
            //         : CircularProgressIndicator();
            //   },
            //   selector: (_, p) => Tuple2(p.messages, p.user),
            // ),
            // )
    );
  }
}
