// import 'package:agora_rtc_engine/agora_rtc_engine.dart';
// import 'package:agora_rtm/agora_rtm.dart';
// // import 'package:agora_rtm/agora_rtm.dart';
// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:wakelock/wakelock.dart';
// import 'package:wblue_customer/env/config.dart';
// import 'package:wblue_customer/providers/market/live/p_create_live.dart';

// class PreviewPage extends StatefulWidget {
//   /// non-modifiable channel name of the page
//   final String channelName;
//   final String username;
//   final int channelId;

//   /// Creates a call page with given channel name.
//   const PreviewPage({Key key, this.channelName, this.channelId, this.username})
//       : super(key: key);

//   @override
//   _PreviewPageState createState() => _PreviewPageState();
// }

// class _PreviewPageState extends State<PreviewPage> {
//   static final _users = <int>[];
//   AgoraRtmClient _client;
//   AgoraRtmChannel _channel;

//   bool accepted = false;
//   bool loading = true;
//   int _channelId;

//   @override
//   void initState() {
//     print('init');
//     super.initState();

//     initialize();
//     _createClient();
//   }

//   @override
//   void dispose() {
//     // clear users
//     _users.clear();
//     // destroy sdk
//     AgoraRtcEngine.leaveChannel();
//     AgoraRtcEngine.destroy();

//     print('disposed');
//     super.dispose();
//   }

//   Future<void> initialize() async {
//     context.read<PCreateLive>().setChannelId(widget.channelId);

//     _channelId = context.read<PCreateLive>().channelId;

//     print(_channelId != null && _channelId != widget.channelId);
//     if (_channelId != null && _channelId != widget.channelId) {
//       AgoraRtcEngine.leaveChannel();
//     }

//     await _initAgoraRtcEngine();
//     _addAgoraEventHandlers();

//     await AgoraRtcEngine.enableWebSdkInteroperability(true);
//     await AgoraRtcEngine.setParameters(
//         '''{\"che.video.lowBitRateStreamParameter\":{\"width\":320,\"height\":180,\"frameRate\":15,\"bitRate\":140}}''');
//     await AgoraRtcEngine.joinChannel(null, widget.channelName, null, 0);
//     context.read<PCreateLive>().setChannelId(widget.channelId);
//   }

//   /// Create agora sdk instance and initialize
//   Future<void> _initAgoraRtcEngine() async {
//     await AgoraRtcEngine.create(Config.APP_ID);
//     await AgoraRtcEngine.enableVideo();
//     //await AgoraRtcEngine.muteLocalAudioStream(true);
//     await AgoraRtcEngine.enableLocalAudio(false);
//     await AgoraRtcEngine.enableLocalVideo(false);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return _viewRows();
//   }

//   /// Add agora event handlers
//   void _addAgoraEventHandlers() {
//     AgoraRtcEngine.onJoinChannelSuccess =
//         (String channel, int uid, int elapsed) {
//       Wakelock.enable();
//     };

//     AgoraRtcEngine.onUserJoined = (int uid, int elapsed) {
//       setState(() {
//         _users.add(uid);
//       });
//     };
//   }

//   /// Video layout wrapper
//   Widget _viewRows() {
//     final views = _getRenderViews();

//     print('views: $views');
//     print('loading: $loading');

//     switch (views.length) {
//       case 1:
//         if (loading == false)
//           return Container(
//               child: Column(
//             children: <Widget>[_videoView(views[0])],
//           ));
//     }

//     return Container();
//   }

//   /// Video view wrapper
//   Widget _videoView(view) {
//     return Expanded(child: ClipRRect(child: view));
//   }

//   /// Helper function to get list of native views
//   List<Widget> _getRenderViews() {
//     final List<AgoraRenderWidget> list = [];
//     print('_users ${_users.length}');

//     //user.add(widget.channelId);
//     _users.forEach((int uid) {
//       if (uid == widget.channelId) {
//         list.add(AgoraRenderWidget(uid));
//       }
//     });

//     if (accepted == true) {
//       list.add(AgoraRenderWidget(0, local: true, preview: true));
//     }

//     if (list.isEmpty) {
//       setState(() {
//         loading = true;
//       });
//     } else {
//       setState(() {
//         loading = false;
//       });
//     }

//     return list;
//   }

//   void _createClient() async {
//     await _client.login(null, widget.username);
//     _channel = await _createChannel(widget.channelName);
//     await _channel.join();

//     // var len;
//     // _channel.getMembers().then((value) {
//     //   len = value.length;
//     //   setState(() {
//     //     userNo = len - 1;
//     //   });
//     // });
//   }

//   Future<AgoraRtmChannel> _createChannel(String name) async {
//     AgoraRtmChannel channel = await _client.createChannel(name);
//     channel.onMemberJoined = (AgoraRtmMember member) async {
//       print('onMemberJoined');
//       // var img = await FireStoreClass.getImage(username: member.userId);
//       // userMap.putIfAbsent(member.userId, () => img);

//       // _channel.getMembers().then((value) {
//       //   len = value.length;
//       //   setState(() {
//       //     userNo = len - 1;
//       //   });
//       // });
//     };
//     channel.onMemberLeft = (AgoraRtmMember member) {
//       print('onMemberLeft');

//       // var len;
//       // _channel.getMembers().then((value) {
//       //   len = value.length;
//       //   setState(() {
//       //     userNo = len - 1;
//       //   });
//       // });
//     };
//     channel.onMessageReceived =
//         (AgoraRtmMessage message, AgoraRtmMember member) async {
//       print('onMessageReceived');

//       // var img = await FireStoreClass.getImage(username: member.userId);
//       // userMap.putIfAbsent(member.userId, () => img);
//       // _log(user: member.userId, info: message.text, type: 'message');
//     };
//     return channel;
//   }
// }
