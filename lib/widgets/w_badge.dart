import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WBadgeWidget extends StatelessWidget {
  final String label;
  final Color color;
  final double fontSize;

  const WBadgeWidget({
    Key key,
    this.label: '',
    this.color: Colors.amber,
    this.fontSize: 12.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: color,
      ),
      child: Text(
        label,
        style: TextStyle(fontSize: fontSize),
      ),
    );
  }
}
