import 'package:auto_route/auto_route.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/auth.dart';
import 'package:wblue_customer/pages/MyOrders/MyOrderMapCard.dart';
import 'package:wblue_customer/pages/MyOrders/OrderList.dart';
import 'package:wblue_customer/pages/PushNotification/firebase_pushNotificatoin.dart';
import 'package:wblue_customer/providers/auth/p_auth.dart';
import 'package:wblue_customer/providers/user/profile.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/widgets/navigateScreen.dart';
import 'package:wblue_customer/widgets/w_image.dart';

class WDrawerWidget extends StatefulWidget {
  final String current;

  WDrawerWidget(this.current);

  @override
  _WDrawerWidgetState createState() => _WDrawerWidgetState();
}

class _WDrawerWidgetState extends State<WDrawerWidget> {
  AuthModel authApi = AuthModel();

  BackButtonBehavior backButtonBehavior = BackButtonBehavior.none;

  Widget _title({Function onTap, String select, String title, IconData icon}) {
    return ListTileTheme(
      child: ListTile(
        selected: widget.current == '$select',
        leading: Icon(icon, color: Colors.white70),
        title: Text('$title', style: TextStyle(color: Colors.white70)),
        onTap: onTap,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    Future.microtask(() => context.read<PUserProfile>().isAuthenticated());
  }

  @override
  Widget build(BuildContext context) {
    FocusScope.of(context).unfocus();
    return Drawer(
      child: Container(
        decoration: BoxDecoration(
          gradient: RadialGradient(
            colors: [Config.primaryColor, Config.primaryColor.withOpacity(0.94)],
            stops: [0.4, 1],
            center: Alignment(0.0, 0.0),
            focal: Alignment(0.0, -0.0),
            focalRadius: 1.9,
          ),
        ),
        child: SafeArea(
          child: Column(
            children: <Widget>[
              DrawerHeader(
                padding: EdgeInsets.all(32.0),
                child: Container(
                  child: Center(
                      child: WImageWidget(
                    placeholder: AssetImage('assets/images/logos/wmall-256.png'),
                  )),
                ),
              ),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    _title(
                        icon: Icons.home,
                        select: 'home',
                        title: 'HOME',
                        onTap: () {
                          Navigator.pop(context);
                          Future.delayed(Duration(milliseconds: 250), () {
                            ExtendedNavigator.of(context).root.popUntilPath('/');
                          });
                        }),
                    if (!Config.comingSoonPages.contains('restaurant'))
                      _title(
                          icon: Icons.restaurant,
                          select: 'browse-restaurant',
                          title: 'BROWSE RESTAURANT',
                          onTap: () {
                            ExtendedNavigator.of(context).root.replace('/restaurants');
                          }),
                    if (!Config.comingSoonPages.contains('hotel'))
                      _title(
                          icon: Icons.hotel,
                          select: 'browse-hotel',
                          title: 'BROWSE HOTEL',
                          onTap: () {
                            ExtendedNavigator.of(context).root.replace('/hotels',
                                arguments: ComingSoonPageArguments(showDrawer: true, page: 'HOTEL'));

                            // ExtendedNavigator.of(context).root.replace('/hotels');
                          }),
                    if (!Config.comingSoonPages.contains('activity'))
                      _title(
                          icon: Icons.movie,
                          select: 'browse-activity',
                          title: 'BROWSE ACTIVITY',
                          onTap: () {
                            ExtendedNavigator.of(context).root.replace('/activity',
                                arguments: ComingSoonPageArguments(showDrawer: true, page: 'ACTIVITY'));
                            // ExtendedNavigator.of(context)
                            //     .root
                            //     .replace('/activity');
                          }),
                    if (!Config.comingSoonPages.contains('market'))
                      _title(
                          icon: Icons.store,
                          select: 'w-mall',
                          title: 'BROWSE W MALL',
                          onTap: () {
                            ExtendedNavigator.of(context).root.replace('/market',
                                arguments: ComingSoonPageArguments(showDrawer: true, page: 'WMALL MARKETPLACE'));
                            // ExtendedNavigator.of(context).root.replace('/market');
                          }),
                    _title(
                        icon: Icons.people,
                        select: 'profile',
                        title: 'PROFILE',
                        onTap: () {
                          ExtendedNavigator.of(context).root.replace('/user');
                        }),
                    _title(
                        icon: Icons.people,
                        select: 'My Orders',
                        title: 'MY ORDERS',
                        onTap: () {
                          ExtendedNavigator.of(context).push('/restaurants/my-order');
                        }),
                    // _title(
                    //     icon: Icons.edit,
                    //     select: 'registration',
                    //     title: 'REGESTRATION',
                    //     onTap: () {
                    //       if (current != 'registration') {
                    //         Navigator.push(
                    //             context,
                    //             MaterialPageRoute(
                    //                 builder: (context) => RegistrationPage()));
                    //       } else {
                    //         Navigator.pop(context);
                    //       }
                    //     }),
                    _title(
                        icon: Icons.access_time,
                        select: 'reservation',
                        title: 'RESERVATIONS',
                        onTap: () {
                          ExtendedNavigator.of(context).root.replace('/reservations');
                        }),
                    _title(
                        icon: Icons.library_books,
                        select: 'terms',
                        title: 'TERMS AND CONDITIONS',
                        onTap: () {
                          if (widget.current != 'terms') {
                          } else {
                            ExtendedNavigator.of(context).root.push('/');
                          }
                        }),
                    _title(
                        icon: Icons.mode_comment,
                        select: 'support',
                        title: 'SUPPORT',
                        onTap: () {
                          if (widget.current != 'support') {
                          } else {
                            ExtendedNavigator.of(context).root.push('/');
                          }
                        }),
                    _title(
                        icon: Icons.notifications,
                        select: 'notification',
                        title: 'NOTIFICATIONS',
                        onTap: () {
                          if (widget.current != 'notification') {
                            Navigator.push(
                                context, MaterialPageRoute(builder: (context) => FireBasePushNotification()));
                          } else {
                            ExtendedNavigator.of(context).root.push('/');
                          }
                        }),
                    _title(
                        icon: Icons.settings,
                        select: 'settings',
                        title: 'SETTINGS',
                        onTap: () {
                          if (widget.current != 'settings') {
                          } else {
                            ExtendedNavigator.of(context).root.push('/');
                          }
                        }),

                    Selector<PUserProfile, bool>(
                      builder: (_, isAuth, __) {
                        return isAuth
                            ? _title(
                                icon: Icons.exit_to_app,
                                select: 'wmall_logout',
                                title: 'LOGOUT',
                                onTap: () async {
                                  Global.user = null;

                                  showAlertDialog(backButtonBehavior, confirm: () async {
                                    final res = await authApi.logout();
                                    if (res) {
                                      context.read<PAuth>().logout();
                                      Future.delayed(Duration(milliseconds: 250), () {
                                        ExtendedNavigator.of(context).root.popUntilPath('/');
                                      });
                                      Navigator.pop(context);
                                    }
                                  });
                                })
                            : SizedBox();
                      },
                      selector: (_, user) => user.isAuth,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void showAlertDialog(BackButtonBehavior backButtonBehavior,
      {VoidCallback cancel, VoidCallback confirm, VoidCallback backgroundReturn}) {
    BotToast.showAnimationWidget(
        clickClose: false,
        allowClick: false,
        onlyOne: true,
        crossPage: true,
        backButtonBehavior: backButtonBehavior,
        wrapToastAnimation: (controller, cancel, child) => Stack(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    cancel();
                    backgroundReturn?.call();
                  },
                  //The DecoratedBox here is very important,he will fill the entire parent component
                  child: AnimatedBuilder(
                    builder: (_, child) => Opacity(
                      opacity: controller.value,
                      child: child,
                    ),
                    child: DecoratedBox(
                      decoration: BoxDecoration(color: Colors.black26),
                      child: SizedBox.expand(),
                    ),
                    animation: controller,
                  ),
                ),
                CustomOffsetAnimation(
                  controller: controller,
                  child: child,
                )
              ],
            ),
        toastBuilder: (cancelFunc) => AlertDialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              title: const Text('Are you sure to logout?'),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    cancelFunc();
                    cancel?.call();
                  },
                  highlightColor: const Color(0x55FF8A80),
                  splashColor: const Color(0x99FF8A80),
                  child: const Text(
                    'cancel',
                    style: TextStyle(color: Colors.redAccent),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    cancelFunc();
                    confirm?.call();
                  },
                  child: const Text('confirm'),
                ),
              ],
            ),
        animationDuration: Duration(milliseconds: 300));
  }
}

class CustomOffsetAnimation extends StatefulWidget {
  final AnimationController controller;
  final Widget child;

  const CustomOffsetAnimation({Key key, this.controller, this.child}) : super(key: key);

  @override
  _CustomOffsetAnimationState createState() => _CustomOffsetAnimationState();
}

class _CustomOffsetAnimationState extends State<CustomOffsetAnimation> {
  Tween<Offset> tweenOffset;
  Tween<double> tweenScale;

  Animation<double> animation;

  @override
  void initState() {
    tweenOffset = Tween<Offset>(
      begin: const Offset(0.0, 0.8),
      end: Offset.zero,
    );
    tweenScale = Tween<double>(begin: 0.3, end: 1.0);
    animation = CurvedAnimation(parent: widget.controller, curve: Curves.decelerate);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      child: widget.child,
      animation: widget.controller,
      builder: (BuildContext context, Widget child) {
        return FractionalTranslation(
            translation: tweenOffset.evaluate(animation),
            child: ClipRect(
              child: Transform.scale(
                scale: tweenScale.evaluate(animation),
                child: Opacity(
                  child: child,
                  opacity: animation.value,
                ),
              ),
            ));
      },
    );
  }
}
