import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:google_map_polyline/google_map_polyline.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/providers/socket/driverCoordinates.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/widgets/w_loading.dart';
import 'dart:ui' as ui;
import 'package:flutter/services.dart';

import 'OrderCardExp.dart';

class LiveTrackingMap extends StatefulWidget {
  String orderStatus, orderHashID, orderNumber, rName, date, time, rImage;
  var rLat, rLang, cLat, cLang;
  LiveTrackingMap(
      {this.orderStatus,
      this.orderHashID,
      this.orderNumber,
      this.cLang,
      this.cLat,
      this.rLang,
      this.rLat,
      this.rName,
      this.date,
      this.time,
      this.rImage});
  @override
  _LiveTrackingMapState createState() => _LiveTrackingMapState();
}

class _LiveTrackingMapState extends State<LiveTrackingMap> {
  final Set<Polyline> polyline = {};
  Set<Marker> _markers = {};
  GoogleMapController _controller;
  bool stopUpdateLocatin;
  BitmapDescriptor icon;
  GoogleMapPolyline googleMapPolyline =
      new GoogleMapPolyline(apiKey: "AIzaSyCpZybhi4ZzUBXImzkKtaggzrOSsfcH8p4");
  bool initialiesedLocation;
  List<LatLng> routeCoords;
  List<LatLng> driverToRestaurent;
  List<LatLng> driverTOcustomer;
  BitmapDescriptor driverPointIcon;
  BitmapDescriptor restaurentPointIcon;
  BitmapDescriptor customerPointIcon;
  String userHashId;
  //to draw the polilines
  location(double dlat, dlang) async {
    routeCoords = await googleMapPolyline.getCoordinatesWithLocation(
        origin: LatLng(dlat, dlang), //driver coordinates
        destination: LatLng(25.294525, 51.472844), //restaurent cordinates
        mode: RouteMode.driving);
    setState(() {
      this.routeCoords = routeCoords;
      initialiesedLocation = true;
    });
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  getIcons() async {
    var driverIcon = await getBytesFromAsset('assets/images/rider.png', 80);
    var restaurentIcon = await getBytesFromAsset('assets/images/resto.png', 80);
    var customerIcon = await getBytesFromAsset('assets/images/van.png', 80);
    setState(() {
      this.driverPointIcon = BitmapDescriptor.fromBytes(driverIcon);
      this.restaurentPointIcon = BitmapDescriptor.fromBytes(restaurentIcon);
      this.customerPointIcon = BitmapDescriptor.fromBytes(customerIcon);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    getIcons();
    location(25.272583, 51.545032);
    gettingUserDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Config.primaryColor,
        title: Text("My Order"),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            userHashId == null && routeCoords == null
                ? Center(child: WLoadingWidget())
                : widget.orderStatus == "Delivered"
                    ? Text("")
                    : Consumer<DriverCoordinatesScocket>(
                        builder: (_, driverCoordinates, __) {
                        print("user Hash ID ::: $userHashId");
                        var socketORderHashID =
                            driverCoordinates.driverCoordinates['order_hashid'];
                        print(widget.orderHashID == socketORderHashID
                            ? "MAtching ${widget.orderHashID} &&  $socketORderHashID"
                            : "NOT MAtching ${widget.orderHashID} &&  $socketORderHashID");
                        if (driverCoordinates.isUpdated &&
                            widget.orderHashID ==
                                driverCoordinates
                                    .driverCoordinates['order_hashid'] &&
                            userHashId ==
                                driverCoordinates
                                    .driverCoordinates['user_hashid']) {
                          print(driverCoordinates.isUpdated);
                          var dlat =
                              driverCoordinates.driverCoordinates['lattitude'];
                          var dlang =
                              driverCoordinates.driverCoordinates['langittude'];
                          setLinesAndMarkers(LatLng(dlat, dlang),
                              driverCoordinates.isUpdated, _controller);
                          context
                              .read<DriverCoordinatesScocket>()
                              .setDriverCoordinatesUpdated(false);
                          print(
                              "hitting the scoket consumer in Live tracking map screen");

                          return Container(
                            // margin: EdgeInsets.only(top: size.height * 0.1),
                            height: size.height * 0.25,
                            width: double.infinity,
                            child: GoogleMap(
                              tiltGesturesEnabled: false,
                              onMapCreated: onMapCreated,
                              polylines: polyline,
                              initialCameraPosition: CameraPosition(
                                  target: LatLng(dlat, dlang), zoom: 14.3),
                              mapType: MapType.terrain,
                              markers: _markers,
                            ),
                          );
                        } else {
                          return Container(
                            // margin: EdgeInsets.only(top: size.height * 0.1),
                            height: size.height * 0.25,
                            width: double.infinity,
                            child: GoogleMap(
                              tiltGesturesEnabled: false,
                              onMapCreated: onMapCreated,
                              polylines: polyline,
                              initialCameraPosition: CameraPosition(
                                  target: LatLng(widget.rLat, widget.rLang),
                                  zoom: 14.3),
                              mapType: MapType.terrain,
                              markers: _markers,
                            ),
                          );
                        }
                      }),
            Column(
              children: [
                Container(
                  height: size.height * 0.6,
                  width: size.width,
                  color: Colors.red,
                  child: MyOrderCardExp(
                    rname: widget.rName,
                    date: widget.date,
                    orderHashID: widget.orderHashID,
                    status: widget.orderStatus,
                    orderNo: widget.orderNumber,
                    // rImage: widget.rImage,
                    time: widget.time,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  void onMapCreated(GoogleMapController controller) {
    setState(() {
      print("socket hitting the onMaPCreated");
      _controller = controller;
      setLinesAndMarkers(LatLng(widget.cLat, widget.cLang), false, controller);
      //  print("working"+lat+long);
      //restaurent location
    });
  }

  setLinesAndMarkers(LatLng dcoordinates, bool updatedLocation,
      GoogleMapController controller) async {
    updatedLocation == true
        ? driverToRestaurent =
            await googleMapPolyline.getCoordinatesWithLocation(
                origin: dcoordinates, //driver coordinates
                destination:
                    LatLng(widget.rLat, widget.rLang), //restaurent cordinates
                mode: RouteMode.driving)
        : null;
    driverTOcustomer = await googleMapPolyline.getCoordinatesWithLocation(
        origin: widget.orderStatus == "Delivering"
            ? dcoordinates
            : LatLng(widget.rLat, widget.rLang),
        destination: LatLng(widget.cLat, widget.cLang),
        mode: RouteMode.driving);
    setState(() {
      _controller = controller;
      print("hititng to lines and marker");
      print(" location $driverTOcustomer");
      print(updatedLocation);
      updatedLocation == true
          ? polyline.add(Polyline(
              polylineId: PolylineId('route1'),
              visible: true,
              points:
                  driverToRestaurent, // driver lotation to restaurent location
              width: 2,
              color: Colors.amber,
              startCap: Cap.roundCap,
              endCap: Cap.buttCap))
          : null;
      //Driver to customer
      polyline.add(Polyline(
          polylineId: PolylineId('route2'),
          visible: true,
          points: driverTOcustomer, // driver lotation to restaurent location
          width: 2,
          color: Config.primaryColor,
          startCap: Cap.roundCap,
          endCap: Cap.buttCap));
      //driver location
      updatedLocation == true
          ? _markers.add(Marker(
              markerId: MarkerId('driver'),
              position: dcoordinates,
              icon: driverPointIcon))
          : null;
      // restaurent location
      _markers.add(Marker(
          markerId: MarkerId('restaurent'),
          position: LatLng(widget.rLat, widget.rLang),
          icon: restaurentPointIcon));
      //customer location
      _markers.add(Marker(
          markerId: MarkerId('Customer'),
          position: LatLng(widget.cLat, widget.cLang),
          icon: BitmapDescriptor.defaultMarkerWithHue(05.0)));
    });
  }

  gettingUserDetails() async {
    final currentAuthUser = Hive.box('auth');
    Map user = await currentAuthUser.get('user');
    setState(() {
      userHashId = user['hashid'];
    });
  }
}
