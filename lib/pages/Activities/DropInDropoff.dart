import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'dart:math' show cos, sqrt, asin;
import 'package:google_directions_api/google_directions_api.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
//import 'package:wblue_customer/services/dio/dio_client.dart';
import 'ActivityCart.dart';

class DropInDropOff extends StatefulWidget {
  @override
  _DropInDropOffState createState() => _DropInDropOffState();
}

class _DropInDropOffState extends State<DropInDropOff> {
  LocationResult _pickedLocation;
  double lat1, lng1, lat2, lng2;
  // final Distance distance = Distance();
  double totalDistanceInKm = 0;
  Dio dio = new Dio();

  TextEditingController _textFieldController = TextEditingController();
  Map _distance;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: appBarWithOutIcon(context, "Location"),
      body: Column(
        children: [
          locationContainer(stateManagment.pickUpLocation == null
              ? "Pick up Location"
              : stateManagment.pickUpLocation),
          locationContainer(stateManagment.dropOffLocation == null
              ? "Drop of Location"
              : stateManagment.dropOffLocation),
          button("Next", () {
            _displayDialog(context);
          }, Color(0xff284E99)),
        ],
      ),
    );
  }

  button(String title, Function onTap, Color color) {
    return RaisedButton(
        child: Text(
          title,
          style: TextStyle(color: Colors.white),
        ),
        color: color,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        onPressed: onTap);
  }

  locationContainer(String title) {
    var size = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () async {
        LocationResult result = await showLocationPicker(
          context,
          "AIzaSyCpZybhi4ZzUBXImzkKtaggzrOSsfcH8p4",
          myLocationButtonEnabled: true,
          layersButtonEnabled: true,
        );
        setState(() {
          _pickedLocation = result;
          print(result.latLng);

          if (title == "Pick up Location") {
            lat1 = result.latLng.latitude;
            lng1 = result.latLng.longitude;
            stateManagment.setPickUpLocation(result.address);
          } else {
            lat2 = result.latLng.latitude;
            lng2 = result.latLng.longitude;
            stateManagment.setDropOffLocation(result.address);
          }
          if (stateManagment.pickUpLocation != null &&
              stateManagment.dropOffLocation != null) {
            _updateInfo(lat1, lng1, lat2, lng2);
          }
        });
      },
      child: Container(
        margin: EdgeInsets.only(
          top: 10,
          left: 15,
        ),
        height: size.height * 0.1,
        width: size.width * 0.9,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            text(title, size.height * 0.022, Colors.black54, 3,
                FontWeight.normal, size.width * 0.8, TextDecoration.none),
            Icon(
              Icons.location_on,
              color: Colors.black26,
            ),
          ],
        ),
      ),
    );
  }

  text(
    String text,
    double height,
    Color color,
    int lines,
    FontWeight fontWeight,
    double width,
    TextDecoration underline,
  ) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: width,
      margin: EdgeInsets.only(left: 10, top: 0),
      child: Text(
        text,
        maxLines: lines,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: fontWeight,
            color: color,
            decoration: underline,
            fontSize: height),
      ),
    );
  }

  _displayDialog(BuildContext context) async {
    var size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 150,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    text(
                        " Activity add to your Cart",
                        size.height * 0.024,
                        Colors.black,
                        1,
                        FontWeight.bold,
                        size.width * 0.5,
                        TextDecoration.none),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        Spacer(),
                        button(" Continue", () {
                          Navigator.pop(context);
                        }, Color(0xff101f40)),
                        Spacer(),
                        button(" Go to Cart", () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ActivityCart()));
                        }, Color(0xff101f40)),
                        Spacer(),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Future _updateInfo(double lat1, lng1, lat2, lng2) async {
    Map<String, dynamic> params = {
      'units': 'metric',
      'origins': '$lat1,$lng1',
      'destinations': '$lat2,$lng2',
      'key': Config.googleMapAPI,
    };
    final response = await DioClient().randomGet(
        'https://maps.googleapis.com/maps/api/distancematrix/json',
        data: params);
    if (response != null) {
      _distance = response['rows'][0]['elements'][0]['distance'];
      String distance = "$_distance";

      String result = distance.substring(0, distance.indexOf(','));
      String km = result
          .replaceAll(new RegExp(r'[^\w\s]+'), '')
          .toString()
          .substring(5);
      print(km);
      print(km.length == 6 ? km.substring(0, 2) : km.substring(0, 1));
    }
    setState(() {
      // print("Distance is "+"$_distance");
    });
  }
}
