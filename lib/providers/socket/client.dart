import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/providers/restaurant/p_qr_reservation.dart';
import 'package:wblue_customer/providers/restaurant/p_restaurant_message.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:provider/provider.dart';

import 'driverCoordinates.dart';
import 'orderStatus.dart';

class PSocketClient with ChangeNotifier {
  Socket _socketHelper;
  BuildContext _context;

  Socket get socketHelper => _socketHelper;

  void connect(BuildContext context) {
    _context = context;

    _socketHelper = io(Config.socketLiveHost, <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
    });

    if (!_socketHelper.connected) {
      _socketHelper.connect();
    }
    //ALL LISTERS
    handlers();
  }

  void handlers() {
    _socketHelper.on(WEvents.supportMessage, (data) {
      try {
        _context.read<PRestaurantMessage>().setMessage(data);
        notifyListeners();
      } catch (e) {
        print('event error: $e');
      }
    });

    _socketHelper.on(WEvents.waiterandcustomeraddingfood, (data) {
      try {
        print('event ${WEvents.waiterandcustomeraddingfood} received');
        _context.read<PRestaurantFoodCart>().fetchRCartList();
      } catch (e) {
        print('${WEvents.waiterandcustomeraddingfood} error: $e');
      }
    });

    _socketHelper.on(WEvents.waiterScanner, (data) {
      try {
        _context.read<PQRReserevation>().whenScan(data);
      } catch (e) {
        print('event error: $e');
      }
    });
    //this is for updatating order status from RESTAURENT && DRIVER
    _socketHelper.on(WEvents.orderStatusUpdated, (data) {
      print('Order Status updated');

      try {
        _context.read<OrderStatusScocket>().whenOrderStatusUpdated(data);
      } catch (e) {
        print('event error: $e');
      }
    });
    //this is for driver updated coordinates
    _socketHelper.on(WEvents.driverUpdatedCoordinates, (data) {
      print('driver Coordinates Updated');

      try {
        _context.read<DriverCoordinatesScocket>().whenDriverCoordinatesUpdated(data);
      } catch (e) {
        print('event error: $e');
      }
    });
  }

  // PASS DATA HERE TO SOCKET CHANNEL
  void emitter({String eventName, Map<String, dynamic> data}) {
    _socketHelper.emit(eventName, data);
  }

  Future<void> emitterListener({String eventName, Future<VoidCallback> onReceived(Map data)}) async {
    _socketHelper.once(eventName, (data) async {
      String token = await Helper.currentAuthUser.get('token') ?? '';

      if (token.length <= 0 || token == null) {
        return;
      }

      Helper.state[StatesNames.token] = token;
      onReceived(data);
    });
  }
}

class WEvents {
  static String supportMessage = 'support-message';
  static String pingRestaurantStaff = 'ping-restaurant-staff';
  static String managerRestaurantAcceptOrder = 'accept-restaurant-order';
  static String waiterScanner = 'waiter-qr-scanner';
  static String checkoutRestaurentHashId = 'r-hashid-to-web-and-mobile';
  static String orderStatusUpdated = 'status-of-order';
  static String driverUpdatedCoordinates = 'driver-coordinates';
  static String waiterandcustomeraddingfood = 'add-new-food-to-cart';
}
