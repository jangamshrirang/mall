import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shimmer/shimmer.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/R_ListByRatings.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/pages/restaurant/searchRestList.dart';
import 'package:wblue_customer/pages/restaurant/store/single_restaurant.dart';
import 'package:wblue_customer/providers/order/p_order.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/providers/restaurant/p_restaurant_list.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/widgets/w_drawer.dart';

class Restaurantlist extends StatefulWidget {
  @override
  _RestaurantlistState createState() => _RestaurantlistState();
}

class _RestaurantlistState extends State<Restaurantlist> {
  Map filters = {
    'cousine': [],
    'table-location': [],
  };

  @override
  void initState() {
    super.initState();
    Future.microtask(() => context.read<PRestaurantOrders>().oneOrder());
  }

  Future<bool> _canFollow() async {
    bool res = false;

    bool canNav = await ExtendedNavigator.of(context).root.canNavigate('/restaurants/cart');
    res = canNav ?? false;

    return res;
  }

  Widget footer = CustomFooter(
    builder: (BuildContext context, LoadStatus mode) {
      Widget body;

      if (mode == LoadStatus.idle) {
        body = Text("pull up load");
      } else if (mode == LoadStatus.loading) {
        body = CupertinoActivityIndicator();
      } else if (mode == LoadStatus.failed) {
        body = Text("Load Failed!Click retry!");
      } else if (mode == LoadStatus.canLoading) {
        body = Text("release to load more");
      } else {
        body = Text("No more Data");
      }
      return Container(
        height: 55.0,
        child: Center(child: body),
      );
    },
  );

  Widget restaurantList(PRestaurantList restaurant) {
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
            child: Consumer<PRestaurantOrders>(
          builder: (context, order, child) => order.showLatestOneOrder != null
              ? GestureDetector(
                  onTap: () => ExtendedNavigator.of(context).push('/restaurants/my-order'),
                  child: Container(
                    height: 0.15.sh,
                    margin: EdgeInsets.symmetric(vertical: 2),
                    child: Container(
                      color: Colors.amberAccent,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: _order(order),
                      ),
                    ),
                  ),
                )
              : SizedBox(),
        )),
        restaurant.advertisements.length > 0
            ? SliverToBoxAdapter(
                child: Container(
                  height: 0.5.sw,
                  child: Swiper(
                    scale: 0.8,
                    viewportFraction: 0.975,
                    onTap: (index) {
                      restaurant.swiperControl.stopAutoplay();
                      ExtendedNavigator.of(context).root.push('/restaurants/single',
                          arguments: SingleRestaurantPageArguments(
                              restaurantListModel: RestaurantListModel(
                                  ratings: restaurant.advertisements[index].adRatings,
                                  city: restaurant.advertisements[index].city,
                                  contact: restaurant.advertisements[index].contact,
                                  email: restaurant.advertisements[index].email,
                                  full: restaurant.advertisements[index].profilePhotoFull,
                                  fullCover: restaurant.advertisements[index].coverPhotoFull,
                                  hashId: restaurant.advertisements[index].hashid,
                                  thumbnailCover: restaurant.advertisements[index].coverPhotoThumb,
                                  restaurant: restaurant.advertisements[index].name,
                                  schedule: restaurant.advertisements[index].schedules,
                                  thumbnail: restaurant.advertisements[index].profilePhotoThumb,
                                  storeInformation: restaurant.advertisements[index].information,
                                  isFollowed: restaurant.advertisements[index].isFollowed,
                                  numberOfFollowers: restaurant.advertisements[index].numberOfFollowers)));
                    },
                    onIndexChanged: (index) async {
                      restaurant.setPageIndex(page: index);
                    },
                    itemBuilder: (BuildContext context, int index) {
                      return CachedNetworkImage(
                        imageUrl: restaurant.advertisements[index].imageFull ?? '',
                        imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.fill,
                            ),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        placeholder: (context, url) => Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(restaurant.advertisements[index].imageThumbnail ?? ''),
                              fit: BoxFit.contain,
                            ),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      );
                    },
                    // indicatorLayout: PageIndicatorLayout.COLOR,
                    autoplay: true,
                    controller: restaurant.swiperControl,
                    autoplayDelay: 3000,
                    duration: 1500,
                    itemCount: restaurant.advertisements.length,
                  ),
                ),
              )
            : SliverToBoxAdapter(),
        restaurant.advertisements.length > 0
            ? SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AnimatedSmoothIndicator(
                        activeIndex: restaurant?.pageIndex ?? 0,
                        count: restaurant.advertisements.length,
                        effect: ExpandingDotsEffect(
                          activeDotColor: Config.primaryColor,
                          dotColor: Colors.grey[350],
                          dotHeight: 5,
                          expansionFactor: 2,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            : SliverToBoxAdapter(),
        SliverGrid(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1, childAspectRatio: 1.36),
          delegate: SliverChildBuilderDelegate((context, index) => _card(restaurant.restaurants[index], restaurant),
              childCount: restaurant.restaurants.length),
        )
      ],
    );
  }

  Column _order(PRestaurantOrders order) {
    String msg;
    String flare = "assets/flares/restaurant/glass-hour.flr";
    String animation = "normal-loop";

    if (order.showLatestOneOrder.statusInfo == 'Pending') {
      msg = 'Your order already sent to';
      flare = "assets/flares/restaurant/glass-hour.flr";
    } else if (order.showLatestOneOrder.statusInfo == 'Preparing') {
      msg = 'Your food is now preparing';
      flare = "assets/flares/restaurant/cooking.flr";
    } else if (order.showLatestOneOrder.statusInfo == 'Ready To Deliver') {
      msg = 'Your food is now ready to deliver';
      flare = "assets/flares/restaurant/delivery-man.flr";
      animation = "normal";
    } else if (order.showLatestOneOrder.statusInfo == 'Delivering') {
      msg = 'Your food is now on the way';
      flare = "assets/flares/restaurant/delivery-man.flr";
    } else if (order.showLatestOneOrder.statusInfo == 'Delivered') msg = 'Thank you, Enjoy your food!';

    return Column(
      children: [
        Container(
          width: 0.12.sw,
          height: 0.12.sw,
          child: FlareActor(
            flare,
            animation: animation,
          ),
        ),
        SizedBox(height: 0.005.sh),
        Text('$msg', style: TextStyle(color: Colors.black87, fontSize: 32.sp)),
        SizedBox(height: 0.01.sh),
        Text('${order.showLatestOneOrder.restaurant.name}',
            style: TextStyle(color: Colors.black87, fontSize: 38.sp, fontWeight: FontWeight.bold))
      ],
    );
  }

  Widget totalCart(PRestaurantFoodCart cart) {
    int total = 0;

    cart.restaurantCart.food.forEach((item) {
      total += item.quantity;
    });

    return Positioned(
      top: 12,
      right: 10,
      child: Container(
        width: 0.05.sw,
        height: 0.05.sw,
        child: CircleAvatar(
          backgroundColor: Colors.red,
          child: Text('$total',
              style: TextStyle(
                fontSize: 24.sp,
                color: Colors.white,
              )),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              'Deliver to:',
              style: TextStyle(fontSize: 36.sp),
            ),
            SizedBox(
              width: 8,
            ),
            Expanded(
              child: _getArea(),
            ),
          ],
        ),
        backgroundColor: Config.primaryColor,
        actions: <Widget>[
          Consumer<PRestaurantFoodCart>(
            builder: (context, checkCart, child) => checkCart.restaurantCart.food.length > 0
                ? SizedBox(
                    width: 0.15.sw,
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: FlatButton(
                            onPressed: () => ExtendedNavigator.of(context).root.push('/restaurant-food-cart'),
                            child: Icon(
                              Icons.shopping_bag,
                              color: Colors.white,
                              size: 52.sp,
                            ),
                            height: 40.0,
                          ),
                        ),
                        totalCart(checkCart),
                      ],
                    ),
                  )
                : SizedBox(),
          ),
          IconButton(
            icon: Image.network(
              "https://www.iconfinder.com/data/icons/business-marketing-outline-set-2/91/Business_Marketing_137-512.png",
              height: 20,
              color: Colors.white,
            ),
            onPressed: () {
              _scanQR();
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (context) => ResaturentScanner()));
            },
          )
        ],
      ),
      drawer: WDrawerWidget('browse-restaurant'),
      body: Column(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => RestSearch()));
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              height: 50.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text('Enter Restaurent Name'),
                  Spacer(),
                  IconButton(
                    icon: Icon(Icons.search),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => RestSearch()));
                    },
                  ),
                  // IconButton(
                  //   icon: Icon(Icons.filter_list),
                  //   onPressed: () {
                  //     _filterModal(context);
                  //   },
                  // )
                ],
              ),
            ),
          ),
          Expanded(
            child: Consumer<PRestaurantList>(
              builder: (context, restaurant, child) => SmartRefresher(
                enablePullDown: true,
                enablePullUp: true,
                footer: footer,
                physics: ClampingScrollPhysics(),
                controller: restaurant.refreshController,
                onRefresh: restaurant.onRefresh,
                onLoading: restaurant.onLoading,
                child: restaurant.restaurants.length > 0
                    ? restaurantList(restaurant)
                    : SingleChildScrollView(
                        physics: NeverScrollableScrollPhysics(),
                        child: Column(
                          children: [
                            Container(
                              width: 1.sw,
                              height: 0.2.sh,
                              child: Shimmer.fromColors(
                                baseColor: Color.fromRGBO(0, 0, 0, 0.1),
                                highlightColor: Color(0x44CCCCCC),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 0.01.sh,
                            ),
                            Container(
                              alignment: Alignment.center,
                              width: 0.6.sw,
                              height: 0.01.sh,
                              child: Shimmer.fromColors(
                                baseColor: Color.fromRGBO(0, 0, 0, 0.1),
                                highlightColor: Color(0x44CCCCCC),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 0.02.sh,
                            ),
                            Column(
                              children: List.generate(
                                  MediaQuery.of(context).size.height ~/ 5,
                                  (index) => Column(
                                        children: [
                                          SizedBox(
                                            height: 0.01.sh,
                                          ),
                                          Container(
                                            width: 1.sw,
                                            height: 0.2.sh,
                                            child: Shimmer.fromColors(
                                              baseColor: Color.fromRGBO(0, 0, 0, 0.1),
                                              highlightColor: Color(0x44CCCCCC),
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: Colors.grey,
                                                  borderRadius: BorderRadius.circular(8.0),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      )),
                            )
                          ],
                        ),
                      ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _getArea() {
    final _user = Hive.box('auth');
    final _basicAddress = Hive.box('area');

    Map<String, dynamic> addr;
    UserAddressModel userAddress;
    Map user = _user.get('user');
    bool isLoggedIn = false;

    return ValueListenableBuilder(
      valueListenable: Hive.box('profile').listenable(),
      builder: (context, box, widget) {
        isLoggedIn = _user.get('isLoggedIn');
        if (user != null && isLoggedIn) {
          String hashid = user['hashid'];
          Map profile = box.get(hashid);
          addr = new Map<String, dynamic>.from(profile['address']);
          userAddress = UserAddressModel.fromJson(addr);

          if (userAddress.hashid == null) {
            Map basic = _basicAddress.get('basic-address');
            addr = new Map<String, dynamic>.from(basic);
          }
        } else {
          Map basic = _basicAddress.get('basic-address');
          addr = new Map<String, dynamic>.from(basic);
        }

        userAddress = UserAddressModel.fromJson(addr);

        return Text(
          '${userAddress.area}',
          style: TextStyle(fontSize: 32.sp, fontWeight: FontWeight.w400),
          overflow: TextOverflow.ellipsis,
        );
      },
    );
  }

  _card(RestaurantListModel restaurant, PRestaurantList pRestaurantList) {
    var size = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () {
        // ExtendedNavigator.of(context).push('/restaurants/single/');
        ExtendedNavigator.of(context).push(
          '/restaurants/single',
          arguments: SingleRestaurantPageArguments(restaurantListModel: restaurant),
        );
      },
      child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6.0)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: [
                  CachedNetworkImage(
                    imageUrl: restaurant?.fullCover,
                    imageBuilder: (context, imageProvider) => Container(
                      height: size.width * 0.5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(6), topRight: Radius.circular(6)),
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => Container(
                      height: size.width * 0.5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(6), topRight: Radius.circular(6)),
                        image: DecorationImage(
                          image: NetworkImage(restaurant?.thumbnailCover ?? ''),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                      right: 10,
                      top: 10,
                      child: GestureDetector(
                        onTap: () async {
                          bool canFollow = await _canFollow();
                          if (canFollow) pRestaurantList.followApi(restaurant);
                        },
                        child: Container(
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                            ),
                            child: Icon(
                              restaurant.isFollowed ? MdiIcons.heart : MdiIcons.heartOutline,
                              color: Colors.red,
                              size: 18,
                            )),
                      )),
                  Positioned(
                    left: 10,
                    top: 10,
                    child: GestureDetector(
                      onTap: () {
                        showImages(context: context, full: restaurant.full, thumbnail: restaurant.thumbnail);
                      },
                      child: Container(
                        padding: EdgeInsets.all(1.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4.0),
                          color: Colors.white,
                        ),
                        height: 60,
                        width: 60,
                        child: CachedNetworkImage(
                          imageUrl: restaurant?.full ?? '',
                          imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.circular(4.0),
                            ),
                          ),
                          placeholder: (context, url) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(restaurant?.thumbnail ?? ''),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.circular(4.0),
                            ),
                          ),
                          errorWidget: (context, url, error) => Icon(Icons.error),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Container(
                            child: Text(
                              '${restaurant.restaurant}',
                              maxLines: 1,
                              style: TextStyle(
                                fontFamily: Config.fontFamily,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 32.sp,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            iconsOnImage('assets/images/take out.png'),
                            SizedBox(width: 5),
                            iconsOnImage('assets/images/delivery.png'),
                            SizedBox(width: 5),
                            iconsOnImage('assets/images/book table.png')
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 0.002.sh),
                    Text(
                      '${restaurant.storeInformation}',
                      maxLines: 1,
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 30.sp,
                        fontWeight: FontWeight.w400,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 0.005.sh),
                    Row(
                      children: [
                        Icon(
                          MdiIcons.heart,
                          size: 14.0,
                          color: Colors.redAccent,
                        ),
                        SizedBox(width: 3),
                        AutoSizeText(
                          '${restaurant.numberOfFollowers}',
                          maxLines: 1,
                          style: TextStyle(
                            color: Colors.black45,
                          ),
                          minFontSize: 24.sp,
                          stepGranularity: 24.sp,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(width: 10),
                        Icon(
                          restaurant.ratings.average != null && restaurant.ratings.average > 0
                              ? restaurant.ratings.average < 5
                                  ? MdiIcons.starHalfFull
                                  : MdiIcons.star
                              : MdiIcons.starOutline,
                          size: 18.0,
                          color: Colors.amberAccent,
                        ),
                        SizedBox(width: 3),
                        AutoSizeText(
                          '${restaurant.ratings.average ?? 0}',
                          maxLines: 1,
                          style: TextStyle(
                            color: Colors.black45,
                          ),
                          minFontSize: 24.sp,
                          stepGranularity: 24.sp,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  void _filterModal(BuildContext context) {
    showModalBottomSheet(
        context: context,
        elevation: 4.0,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState2) {
              Widget _radioItem(String name, String slug, String group) {
                return InkWell(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 4.0),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          filters[group].indexOf(slug) >= 0 ? Icons.check_circle : Icons.radio_button_unchecked,
                          color: filters[group].indexOf(slug) >= 0 ? Colors.amber : Colors.grey,
                        ),
                        SizedBox(width: 8.0),
                        Text(
                          name,
                          style: TextStyle(
                              fontFamily: Config.fontFamily,
                              fontWeight: filters[group].indexOf(slug) >= 0 ? FontWeight.bold : FontWeight.normal),
                        ),
                      ],
                    ),
                  ),
                  onTap: () {
                    setState2(() {
                      if (filters[group].indexOf(slug) >= 0) {
                        filters[group].remove(slug);
                      } else {
                        filters[group].add(slug);
                      }
                    });
                  },
                );
              }

              return Container(
                padding: EdgeInsets.only(left: 14.0, right: 14.0, bottom: 14.0, top: 8.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(30.0)),
                  color: Colors.white,
                ),
                child: Wrap(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('Filter Result'),
                        IconButton(
                          icon: Icon(Icons.close),
                          onPressed: () {
                            setState(() {
                              Navigator.pop(context);
                            });
                          },
                        ),
                      ],
                    ),
                    ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        Container(
                          color: Colors.grey,
                          padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                          child: Text('Cousine'),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                          child: Column(
                            children: <Widget>[
                              _radioItem('Mediterranean', 'mediterranean', 'cousine'),
                              _radioItem('Burgers', 'burgers', 'cousine'),
                              _radioItem('Coffee Shop', 'coffee-shop', 'cousine'),
                              _radioItem('Grill', 'grill', 'cousine'),
                              _radioItem('Asian Fusion', 'asian-fusion', 'cousine'),
                            ],
                          ),
                        ),
                        Container(
                          color: Colors.grey,
                          padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                          child: Text('Table Location'),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                          child: Column(
                            children: <Widget>[
                              _radioItem('Indoor', 'indoor', 'table-location'),
                              _radioItem('Outdoor', 'outdoor', 'table-location'),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              );
            },
          );
        });
  }

  Future<void> showImages({BuildContext context, String full, String thumbnail}) async {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.black,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return CachedNetworkImage(
          imageUrl: full ?? '',
          imageBuilder: (context, imageProvider) => GestureDetector(
              onTap: () {},
              child: Column(
                children: [
                  Expanded(
                    child: Stack(
                      children: [
                        Positioned(
                          top: 50,
                          right: 0,
                          child: FlatButton(
                              onPressed: () => Navigator.pop(context),
                              child: Icon(
                                Icons.close,
                                color: Colors.white,
                                size: 26.0,
                              ),
                              height: 40.0,
                              shape: CircleBorder(),
                              color: Colors.black45),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: AspectRatio(
                            aspectRatio: 1 / 1,
                            child: Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
          errorWidget: (context, url, error) => Container(
            color: Colors.black12,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AutoSizeText('Image Not Found'),
                SizedBox(height: 8.0),
                Icon(
                  Icons.error_outline,
                  size: 30.0,
                  color: Colors.red,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  iconsOnImage(String image) {
    return Container(width: 0.07.sw, child: Image.asset(image, color: Colors.black));
  }

  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        // result = qrResult;
        Navigator.push(context, MaterialPageRoute(builder: (context) => SingleRestaurantPage()));
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          // result = "Camera permission was denied";
        });
      } else {
        setState(() {
          // result = "Unknown Error $ex";
        });
      }
    } on FormatException {
      setState(() {
        // result = "You pressed the back button before scanning anything";
      });
    } catch (ex) {
      setState(() {
        // result = "Unknown Error $ex";
      });
    }
  }
}
