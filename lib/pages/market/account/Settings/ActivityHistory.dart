import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

class ActivityHistory extends StatefulWidget {
  @override
  _ActivityHistoryState createState() => _ActivityHistoryState();
}

class _ActivityHistoryState extends State<ActivityHistory> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "Your Order History"),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Colors.white,
              height: size.height * 0.95,
              child: ListView(
                children: [
                  productCard(
                      "Sand Dunes 4x4",
                      "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                      "1,200"),
                  productCard(
                      "Sand Dunes 4x4",
                      "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                      "1,200"),
                  productCard(
                      "Sand Dunes 4x4",
                      "Rolls Royce Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ",
                      "1,200"),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  productCard(String title, desc, price) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {},
      child: Stack(
        children: [
          Container(
              width: size.width * 0.9,
              height: size.height * 0.22,
              margin: EdgeInsets.only(bottom: 10, left: 20, top: 5, right: 5),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    spreadRadius: 1,
                    blurRadius: 3,
                    offset: Offset(1, 3), // changes position of shadow
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Container(
                  //text container
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  padding: EdgeInsets.only(
                      left: size.width * 0.02,
                      right: size.width * 0.01,
                      top: size.height * 0.01),
                  height: size.height * 0.06,
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: size.height * 0.12,
                            width: size.width * 0.25,
                            decoration: BoxDecoration(
                              color: Colors.transparent,
                              image: DecorationImage(
                                  image: AssetImage(
                                    'assets/activities/rolls-royce-cullinan-drifts-on-sand-and-snow-in-new-video-125398_1.jpg',
                                  ),
                                  fit: BoxFit.fill),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              text(
                                  title,
                                  size.width * 0.3,
                                  size.height * 0.024,
                                  size.height * 0.023,
                                  FontWeight.bold,
                                  1,
                                  Colors.black),
                              text(
                                  desc,
                                  size.width * 0.45,
                                  size.height * 0.06,
                                  size.height * 0.020,
                                  FontWeight.normal,
                                  2,
                                  Colors.black54),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: size.height * 0.005),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            children: [
                              text(
                                  "20-20-2020",
                                  size.width * 0.25,
                                  size.height * 0.026,
                                  size.height * 0.02,
                                  FontWeight.bold,
                                  1,
                                  Colors.black),
                              text(
                                  "Date",
                                  size.width * 0.25,
                                  size.height * 0.040,
                                  size.height * 0.018,
                                  FontWeight.normal,
                                  1,
                                  Colors.black54),
                            ],
                          ),
                          Column(
                            children: [
                              text(
                                  "QAR " + price,
                                  size.width * 0.25,
                                  size.height * 0.026,
                                  size.height * 0.020,
                                  FontWeight.bold,
                                  1,
                                  Colors.red),
                              text(
                                  "Price",
                                  size.width * 0.25,
                                  size.height * 0.040,
                                  size.height * 0.018,
                                  FontWeight.normal,
                                  1,
                                  Colors.black54),
                            ],
                          ),
                          Column(
                            children: [
                              text(
                                  "Cash on deliviry",
                                  size.width * 0.30,
                                  size.height * 0.026,
                                  size.height * 0.02,
                                  FontWeight.bold,
                                  1,
                                  Colors.black),
                              text(
                                  "Payment method",
                                  size.width * 0.3,
                                  size.height * 0.040,
                                  size.height * 0.018,
                                  FontWeight.normal,
                                  1,
                                  Colors.black54),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ))),
          Positioned(
              right: size.width * 0.1,
              top: size.height * 0.03,
              child: Text(
                "12.00pm",
                style: TextStyle(fontSize: 15, color: Colors.black45),
              )),
                 Positioned(
              left: size.width * 0.07,
              top: size.height * 0.02,
              child: Image.asset("assets/images/icons/activities.png",height: 25)),
        ],
      ),
    );
  }

  text(String title, double widthh, heightt, textSize, FontWeight fontWeight,
      int lines, Color color) {
    var size = MediaQuery.of(context).size;
    return Container(
      //    color: Colors.red,
      margin: EdgeInsets.only(left: 5, top: 2),
      height: heightt,
      width: widthh,
      child: Text(
        title,
        maxLines: lines,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            color: color,
            fontFamily: 'Montserrat',
            fontWeight: fontWeight,
            fontSize: textSize),
      ),
    );
  }
}
