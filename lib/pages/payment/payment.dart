import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'package:webview_flutter/webview_flutter.dart';

DioClient _dioClient = DioClient();

class WmallPayment extends StatefulWidget {
  @override
  _WmallPaymentState createState() => _WmallPaymentState();
}

class _WmallPaymentState extends State<WmallPayment> {
  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: WebView(
          javascriptMode: JavascriptMode.unrestricted,
          navigationDelegate: (request) {
            if (request.url.contains('W1pNjxmNHNQ')) {
              print('request: ${request.url}');
              // TODO when api success
            } else if (request.url.contains('connect-fail')) {
              // TODO when api fail
            }
            return NavigationDecision.navigate;
          },
          initialUrl: 'https://flutter.dev',
          onPageFinished: (onPageFinished) {
            print(onPageFinished);
          },
        ),
      ),
    );
  }
}
