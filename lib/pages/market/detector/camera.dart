// Copyright 2019 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// ignore_for_file: public_member_api_docs

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:camera/camera.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/main.dart';
import 'package:wblue_customer/pages/market/detector/qr.dart';
import 'package:wblue_customer/providers/market/p_camera.dart';
import 'package:wblue_customer/providers/market/p_product_recognizer.dart';
import 'package:wblue_customer/providers/market/p_view_product.dart';
import 'package:wblue_customer/widgets/w_image_product_card.dart';
import 'package:wblue_customer/widgets/w_items_wrapper.dart';
import 'package:wblue_customer/widgets/w_rating.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';

class MarketCameraPage extends StatefulWidget {
  @override
  _MarketCameraPageState createState() {
    return _MarketCameraPageState();
  }
}

/// Returns a suitable camera icon for [direction].
IconData getCameraLensIcon(CameraLensDirection direction) {
  switch (direction) {
    case CameraLensDirection.back:
      return Icons.camera_rear;
    case CameraLensDirection.front:
      return Icons.camera_front;
    case CameraLensDirection.external:
      return Icons.camera;
  }

  throw ArgumentError('Unknown lens direction');
}

class _MarketCameraPageState extends State<MarketCameraPage>
    with WidgetsBindingObserver {
  List products = new List();

  double deviceRatio;
  Size size;
  CameraController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  var qrText = "";
  QRViewController qrController;
  final picker = ImagePicker();
  File _image;
  List<ImageLabel> _labels;

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);

    super.initState();
    WidgetsBinding.instance.addObserver(this);
    onNewCameraSelected(cameras[0]);
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIOverlays(
        [SystemUiOverlay.top, SystemUiOverlay.bottom]);
    WidgetsBinding.instance.removeObserver(this);
    qrController?.dispose();
    controller?.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // App state changed before we got the chance to initialize.
    if (controller == null || !controller.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      controller?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      if (controller != null) {
        onNewCameraSelected(controller.description);
      }
    }
  }

  void _onQRViewCreated(QRViewController controller) {
    this.qrController = qrController;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        qrText = scanData;
      });
    });
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    deviceRatio = size.width / size.height;
    List<Widget> stackChildren = [];

    return Scaffold(
      backgroundColor: Colors.black,
      key: _scaffoldKey,
      body: Stack(
        alignment: FractionalOffset.center,
        children: <Widget>[
          _cameraPreviewWidget(),
          Positioned(
            top: 0,
            left: 0,
            child: SafeArea(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                width: size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      color: Colors.white,
                      onPressed: () => ExtendedNavigator.of(context).pop(),
                      icon: Icon(MdiIcons.arrowLeft),
                    ),
                    Selector<PCamera, Widget>(
                      builder: (context, data, child) {
                        return data;
                      },
                      selector: (buildContext, camera) =>
                          camera.scannerType == 'camera'
                              ? IconButton(
                                  color: Colors.white,
                                  onPressed: _toggleCameraLens,
                                  icon: Icon(MdiIcons.cameraSwitchOutline),
                                )
                              : SizedBox(),
                    ),
                  ],
                ),
              ),
            ),
          ),

          Positioned(
            bottom: 0,
            child: Consumer<PCamera>(
              builder: (_, camera, __) => Container(
                height: 120,
                width: size.width,
                color: Colors.black38,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 130,
                      height: 70,
                      child: Stack(
                        alignment: FractionalOffset.center,
                        children: [
                          Container(
                            width: 100,
                            height: 40,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Colors.white12,
                            ),
                          ),

                          // QR CIRCLE
                          Positioned(
                            left: camera.scannerType == 'qr' ? 12 : 18, // 18
                            child: GestureDetector(
                              onTap: () {
                                camera.setScanner('qr');
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: camera.scannerType == 'qr'
                                        ? Colors.red
                                        : Colors.grey,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30))),
                                height:
                                    camera.scannerType == 'qr' ? 50 : 32, // 32
                                width:
                                    camera.scannerType == 'qr' ? 50 : 32, // 32
                                child: Icon(
                                  MdiIcons.qrcodeScan,
                                  color: Colors.white,
                                  size: camera.scannerType == 'qr'
                                      ? 24.0
                                      : 18, // 18
                                ),
                              ),
                            ),
                          ),

                          // CAMERA CIRCLE
                          Positioned(
                            right:
                                camera.scannerType == 'camera' ? 10 : 14, //14
                            child: GestureDetector(
                              onTap: () {
                                if (controller != null &&
                                    controller.value.isInitialized &&
                                    camera.scannerType == 'camera') {
                                  context
                                      .read<PProductRecognizer>()
                                      .setError('');
                                  context
                                      .read<PProductRecognizer>()
                                      .setFile(new List());

                                  context.read<PCamera>().setBusyStatus(true);

                                  onTakePictureButtonPressed();
                                }
                                if (camera.scannerType == 'qr') {
                                  onNewCameraSelected(cameras[0]);
                                }

                                camera.setScanner('camera');
                              },
                              child: Stack(
                                alignment: FractionalOffset.center,
                                children: [
                                  Container(
                                    height: camera.scannerType == 'camera'
                                        ? 40.0
                                        : 34, //34
                                    width: camera.scannerType == 'camera'
                                        ? 40.0
                                        : 34, //34
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30))),
                                  ),
                                  Icon(
                                    MdiIcons.circleSlice8,
                                    color: camera.scannerType == 'camera'
                                        ? Colors.red
                                        : Colors.grey,
                                    size: camera.scannerType == 'camera'
                                        ? 60.0
                                        : 42, // 42
                                  ),
                                ],
                              ),
                            ),
                          ),

                          // Positioned(
                          //   left: 15,
                          //   top: 11,
                          //   child: Container(
                          //     height: 60.0,
                          //     width: 60.0,
                          //     decoration: BoxDecoration(
                          //         color: Colors.amber,
                          //         borderRadius:
                          //             BorderRadius.all(Radius.circular(30))),
                          //     child: Icon(
                          //       MdiIcons.qrcodeScan,
                          //       color: Colors.white,
                          //       size: 18.0,
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                    // Container(
                    //   width: 1201,
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       Text(
                    //         'QR Scan',
                    //         style: TextStyle(color: Colors.white, fontSize: 11.0),
                    //       ),
                    //       Text(
                    //         'Camera',
                    //         style: TextStyle(color: Colors.white, fontSize: 14.0),
                    //       ),
                    //     ],
                    //   ),
                    // )
                  ],
                ),
              ),
            ),
          ),
          Consumer<PCamera>(
            builder: (_, image, __) => image.scannerType == 'camera'
                ? Positioned(
                    left: 30,
                    bottom: 36,
                    child: GestureDetector(
                      onTap: getImage,
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Image.asset(
                            'assets/images/qr/albums.png',
                            height: 36,
                            width: 36,
                          ),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
          ),
          Consumer<PCamera>(builder: (__, image, ___) {
            // if (image.busy) {
            //   stackChildren.add(const Opacity(
            //     child: ModalBarrier(dismissible: false, color: Colors.grey),
            //     opacity: 0.3,
            //   ));

            //   stackChildren
            //       .add(const Center(child: CircularProgressIndicator()));
            // }

            if (image.imagePath != null) {
              stackChildren.add(Image.file(
                File(image.imagePath),
                fit: BoxFit.cover,
                width: size.width,
                height: size.height,
              ));
            }

            stackChildren.add(Positioned(
              bottom: 0,
              child: AnimatedContainer(
                width: size.width,
                color: Config.bodyColor,
                height: image.imagePath == null ? 0 : size.height,
                duration: Duration(milliseconds: 600),
                curve: Curves.fastOutSlowIn,
                onEnd: () {
                  if (image.busy && image.imagePath != null) {
                    image.setBusyStatus(false);
                  }
                },
                child: !image.busy
                    ? SafeArea(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            FlatButton(
                              onPressed: () {
                                image.setBusyStatus(true);

                                context
                                    .read<PProductRecognizer>()
                                    .setFile(new List());
                                context.read<PCamera>().setImagePath(null);
                                setState(() {});
                              },
                              child: Icon(Icons.close),
                            ),
                            Consumer<PProductRecognizer>(
                              builder: (__, label, _) {
                                return label.error == ''
                                    ? label.products.length > 0
                                        ? _productCard(context, label.products)
                                        : Expanded(
                                            child: Center(
                                                child: Container(
                                              height: 80,
                                              width: 100,
                                              child: FlareActor(
                                                'assets/flares/market/w-scan.flr',
                                                animation: 'scanning',
                                                fit: BoxFit.contain,
                                              ),
                                            )),
                                          )
                                    : Center(child: Text('${label.error}'));
                              },
                            ),
                          ],
                        ),
                      )
                    : SizedBox(),
              ),
            ));

            return Stack(
              children: stackChildren,
            );
          })

          // // _captureControlRowWidget(),
          // _cameraTogglesRowWidget(),
        ],
      ),
    );
  }

  // Future<void> initDetector(String filePath) async {
  //   List recognizedResult = new List();

  //   FirebaseVisionImage visionImage =
  //       FirebaseVisionImage.fromFile(File(filePath));

  //   final ImageLabeler cloudLabeler =
  //       FirebaseVision.instance.cloudImageLabeler();

  //   final List<ImageLabel> cloudLabels =
  //       await cloudLabeler.processImage(visionImage);

  //   //DANGER SHOULD QUERY IN QPI
  //   for (ImageLabel label in cloudLabels) {
  //     for (int i = 0; i < products.length; i++) {
  //       List tags = products[i]['tags'];
  //       bool isContained = tags.contains(label.text.toLowerCase());
  //       if (isContained) {
  //         recognizedResult.add(products[i]);
  //         products.removeAt(i);
  //       }
  //     }
  //   }

  //   cloudLabeler.close();
  //   if (!mounted) return;
  //   if (recognizedResult.length <= 0) {
  //     context.read<PProductRecognizer>().setError('0 Result');
  //     return;
  //   }

  //   context.read<PProductRecognizer>().setFile(recognizedResult);
  // }

  Future<void> loadProduct() async {
    String data =
        await rootBundle.loadString('assets/data/scanned-products.json');
    products = json.decode(data);
  }

  Widget _productCard(BuildContext context, List products) {
    return WItemList(
      crossAxisCellCount: 2,
      crossAxisCount: 4,
      widget: (product, index) => Container(
        width: MediaQuery.of(context).size.width * 0.5,
        child: WProductImageCard(
          onTap: () {
            context.read<PViewProduct>().setProduct(product);
            ExtendedNavigator.of(context).push('/view-product');
          },
          isVertical: true,
          imageUrl: '${product['images'][0]['small']}',
          widget: Container(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AutoSizeText(
                '${product['name']}',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                minFontSize: 16,
              ),
              SizedBox(height: 10.0),
              AutoSizeText(
                '${product['shortDescription']}',
                style: TextStyle(fontSize: 16),
                maxLines: 4,
                overflow: TextOverflow.ellipsis,
                minFontSize: 14,
              ),
              SizedBox(height: 10.0),
              Container(
                child: Row(
                  children: <Widget>[
                    AutoSizeText(
                      'QR ${product['newPrice']}',
                      style: TextStyle(fontSize: 18, color: Colors.redAccent),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      minFontSize: 16,
                    ),
                    SizedBox(width: 8.0),
                    product['oldPrice'] != null
                        ? Expanded(
                            child: AutoSizeText(
                              'QR ${product['oldPrice']}',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.grey,
                                  decoration: TextDecoration.lineThrough),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
              SizedBox(height: 10.0),
              Row(
                children: [
                  Icon(
                    MdiIcons.checkboxBlankCircle,
                    size: 16.0,
                    color: product['availibilityCount'] > 0
                        ? Colors.green.withOpacity(0.9)
                        : Colors.red.withOpacity(0.9),
                  ),
                  SizedBox(width: 8.0),
                  AutoSizeText(
                    '${product['availibilityCount'] > 0 ? 'In' : 'Out'} stock, ${product['availibilityCount'] > 1 ? '${product['availibilityCount']} units' : '${product['availibilityCount']} unit'}',
                    style: TextStyle(
                        fontSize: 16,
                        color: product['availibilityCount'] > 0
                            ? Colors.green.withOpacity(0.9)
                            : Colors.red.withOpacity(0.9)),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    minFontSize: 16,
                  ),
                ],
              ),
              SizedBox(height: 3.0),
              Row(
                children: [
                  WRatingsWidget(product['ratingsValue'] / 100),
                  SizedBox(width: 2),
                  AutoSizeText(
                    '(${product['ratingsCount']})',
                    style: TextStyle(
                        fontSize: 16, color: Colors.grey.withOpacity(0.9)),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    minFontSize: 16,
                  ),
                ],
              ),
              SizedBox(height: 3.0),
              AutoSizeText(
                '15 Reviews',
                style: TextStyle(fontSize: 16, color: Colors.grey),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                minFontSize: 16,
              ),
            ],
          )),
        ),
      ),
      items: products,
    );
  }

  /// Display the preview from the camera (or a message if the preview is not available).
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        'Loading...',
        style: TextStyle(
          color: Colors.white,
          fontSize: 24.0,
        ),
      );
    } else {
      if (context.watch<PCamera>().scannerType == 'camera') {
        return Center(
          child: Transform.scale(
            scale: controller.value.aspectRatio / deviceRatio,
            child: new AspectRatio(
              aspectRatio: controller.value.aspectRatio,
              child: new CameraPreview(controller),
            ),
          ),
        );
      }
      return Center(
        child: QRView(
          overlay: QrScannerOverlayShape(
            borderColor: Colors.red,
            borderRadius: 10,
            borderLength: 30,
            borderWidth: 10,
            cutOutSize: 300,
          ),
          key: qrKey,
          onQRViewCreated: _onQRViewCreated,
        ),
      );
    }
  }

  /// Display a row of toggle to select the camera (or a message if no camera is available).
  Widget _cameraTogglesRowWidget() {
    final List<Widget> toggles = <Widget>[];

    if (cameras.isEmpty) {
      return const Text('No camera found');
    } else {
      for (CameraDescription cameraDescription in cameras) {
        toggles.add(
          SizedBox(
            width: 90.0,
            child: RadioListTile<CameraDescription>(
              title: Icon(getCameraLensIcon(cameraDescription.lensDirection)),
              groupValue: controller?.description,
              value: cameraDescription,
              onChanged: onNewCameraSelected,
            ),
          ),
        );
      }
    }

    return Row(children: toggles);
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void showInSnackBar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
  }

  void _toggleCameraLens() {
    // get current lens direction (front / rear)
    final lensDirection = controller.description.lensDirection;
    CameraDescription newDescription;
    if (lensDirection == CameraLensDirection.front) {
      newDescription = cameras.firstWhere((description) =>
          description.lensDirection == CameraLensDirection.back);
    } else {
      newDescription = cameras.firstWhere((description) =>
          description.lensDirection == CameraLensDirection.front);
    }

    if (newDescription != null) {
      onNewCameraSelected(newDescription);
    } else {
      print('Asked camera not available');
    }
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }

    controller = CameraController(
      cameraDescription,
      ResolutionPreset.medium,
      enableAudio: false,
    );

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        showInSnackBar('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  void onTakePictureButtonPressed() {
    takePicture().then((String filePath) async {
      if (mounted) {
        if (filePath != null) {
          // context.read<PCamera>().setImagePath(filePath);
          // await loadProduct();
          // print(filePath);
          File image = File(filePath);

          final FirebaseVisionImage visionImage =
              FirebaseVisionImage.fromFile(image);
          final ImageLabeler labeler = FirebaseVision.instance.imageLabeler();
          final List<ImageLabel> labels =
              await labeler.processImage(visionImage);
          setState(() {
            _image = image;
            _labels = labels;
          });
          // showModalBottomSheet(

          //   context: null, builder: null)
          Future.delayed(Duration(seconds: 1), () {
            // 5s over, navigate to a new page
            showModalBottomSheet(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(20),
                  ),
                ),
                clipBehavior: Clip.antiAliasWithSaveLayer,
                context: context,
                isScrollControlled: true,
                builder: (context) => Container(
                      // decoration: BoxDecoration(
                      //   color: Colors.white,

                      //   // borderRadius: BorderRadius.all(Radius.circular(15)),
                      //   // boxShadow: [
                      //   //   BoxShadow(
                      //   //     blurRadius: 10,
                      //   //     color: Colors.grey[300],
                      //   //     // spreadRadius: 5
                      //   //   )
                      //   // ]
                      // ),
                      height: MediaQuery.of(context).size.height / 1.1,
                      // width: double.infinity,
                      // color: Colors.white,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        // crossAxisAlignment: Cross,
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                              // decoration: BoxDecoration(
                              //     color: Colors.white,
                              //     borderRadius:
                              //         BorderRadius.all(Radius.circular(15)),
                              //     boxShadow: [
                              //       BoxShadow(
                              //           blurRadius: 10,
                              //           color: Colors.grey[300],
                              //           spreadRadius: 5)
                              //     ]
                              //     ),
                              child: Row(
                            // mainAxisAlignment: MainAxisAlignment.,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: InkWell(
                                    onTap: () => Navigator.pop(context),
                                    child: Icon(Icons.close)),
                              ),
                              SizedBox(
                                width: 25,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child:
                                    Text('Following products are recognized'),
                              ),
                            ],
                          )),
                          // Text('$_image'),
                          Image.file(
                            _image,
                            height: 240.0,
                          ),
                          Text(
                            _labels == null
                                ? "empty"
                                : _labels
                                    .map((label) => '${label.text} '
                                        // 'with confidence ${label.confidence.toStringAsFixed(2)}'
                                        )
                                    .join('\n'),
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 15.0),
                          ),
                        ],
                      ),
                    ));
          });

          // _showBottomSheet( );
          // initDetector(filePath);
        }
      }
    });
  }

  Future<String> takePicture() async {
    if (!controller.value.isInitialized) {
      showInSnackBar('Error: select a camera first.');
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await controller.takePicture(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return filePath;
  }

  void _showCameraException(CameraException e) {
    logError(e.code, e.description);
    showInSnackBar('Error: ${e.code}\n${e.description}');
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      context.read<PCamera>().setImagePath(pickedFile.path);

      await loadProduct();

      // initDetector(pickedFile.path);
    }
  }
}

class QrResult extends StatelessWidget {
  final String qrResult;

  QrResult({this.qrResult});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Expanded(
        child: Center(child: Text(qrResult)),
      ),
    );
  }
}
