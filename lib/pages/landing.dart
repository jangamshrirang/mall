import 'dart:async';
import 'dart:convert';

import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:dio/dio.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/auth.dart';
import 'package:wblue_customer/providers/market/p_home.dart';
import 'package:wblue_customer/providers/restaurant/R_ListByRatings.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/providers/restaurant/p_restaurant_list.dart';
import 'package:wblue_customer/providers/socket/client.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/workground/chat/ping/p_ping.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage>
    with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin<LandingPage> {
  @override
  bool get wantKeepAlive => true;

  Dio dio = new Dio();

  String address = Global.myAddress;
  LatLng initialCenter = Global.myLocation;
  Size size;

  final FlareControls logo = FlareControls();
  final FlareControls building = FlareControls();

  bool playLogoAndBuildings = true;

  AnimationController expandController;
  Animation<double> animation;
  bool expand = false;
  String addr = 'Please select your location';

  @override
  void initState() {
    super.initState();
    prepareAnimations();
    socketHandler();
    Future.microtask(() {
      context.read<PMarketHome>().fetchJustForYouProducts();
      context.read<PPingStaff>().init(onSelectNotification);
      //Restaurant
      context.read<PRestaurantList>().fetchAllRestaurants();
      // context.read<PBrowseRestaurant>().fetchRestaurants();
      context.read<R_ListRatingHelper>().fetchRestaurantsByRating();
    });

    forAuthenticated();
  }

  Future<void> forAuthenticated() async {
    bool res = false;
    AuthModel authModel = AuthModel();
    bool canNav = await authModel.checkAuthentication();
    res = canNav ?? false;
    if (!res) return;

    context.read<PSocketClient>().connect(context);
    context.read<PRestaurantFoodCart>().fetchRCartList();
  }

  Future<void> showNotification(Map data) async {
    var android = new AndroidNotificationDetails('channel id', 'channel NAME', 'CHANNEL DESCRIPTION',
        priority: Priority.high, importance: Importance.max);
    var iOS = new IOSNotificationDetails();
    var platform = new NotificationDetails(android: android, iOS: iOS);

    // if (Helper.storeHashId == data['hashid']) {
    await context.read<PPingStaff>().flutterLocalNotificationsPlugin.show(
          0,
          'Order Accepted',
          '${data['waiter_name']} accept your order',
          platform,
          payload: json.encode(data),
        );
    // }
  }

  Future<void> onSelectNotification(String payload) {
    // debugPrint("payload : $payload");
    showDialog(
      context: context,
      builder: (_) => new AlertDialog(
        title: new Text('WMall'),
        content: new Text('ORDER NOW ACCEPTED!'),
      ),
    );
  }

  Future<void> socketHandler() async {
    context.read<PSocketClient>().emitterListener(
        eventName: WEvents.managerRestaurantAcceptOrder,
        onReceived: (data) async {
          // print(data);

          // AuthModel authModel =
          //     AuthModel.fromJson(Helper.currentAuthUser.get('user'));
          // if (authModel.hashid == data['user_hash_id'])
          await showNotification(data);
        });
  }

  void prepareAnimations() {
    expandController = AnimationController(vsync: this, duration: Duration(milliseconds: 1300));
    animation = CurvedAnimation(
      parent: expandController,
      curve: Curves.fastOutSlowIn,
    );
  }

  void _runExpandCheck() {
    if (expand) {
      expandController.forward();
    } else {
      expandController.reverse();
    }
  }

  @override
  void didUpdateWidget(LandingPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    print('-');
    _runExpandCheck();
  }

  @override
  void dispose() {
    expandController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, designSize: Size(828, 1792), allowFontScaling: false);
    super.build(context);
    size = MediaQuery.of(context).size;
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: playLogoAndBuildings ? SystemUiOverlayStyle.dark : SystemUiOverlayStyle.light,
      child: Scaffold(
        // floatingActionButton: FloatingActionButton(
        //   onPressed: () async {
        //     final res = await sendAndRetrieveMessage();
        //     print('RES: $res');
        //   },
        //   child: Icon(Icons.sms),
        // ),
        body: Stack(
          children: [
            FlareActor(
              'assets/flares/landing/blue.flr',
              animation: 'normal',
              fit: BoxFit.cover,
              callback: (value) {
                expand = true;
                _runExpandCheck();
                setState(() {
                  playLogoAndBuildings = false;
                });
              },
            ),
            SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Column(
                children: [
                  Container(
                    height: 0.35.sh,
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: 0.50.sw,
                      width: 0.50.sw,
                      child: FlareActor(
                        'assets/flares/landing/w-mall.flr',
                        animation: 'normal',
                        fit: BoxFit.contain,
                        isPaused: playLogoAndBuildings,
                      ),
                    ),
                  ),
                  Container(
                    height: 0.25.sh,
                    child: FlareActor(
                      'assets/flares/landing/building.flr',
                      animation: 'normal-loop',
                      alignment: Alignment.bottomCenter,
                      fit: BoxFit.fitWidth,
                      isPaused: playLogoAndBuildings,
                    ),
                  ),
                  SlideTransition(
                    position: Tween<Offset>(
                      begin: const Offset(0, 1),
                      end: Offset.zero,
                    ).animate(animation),
                    child: Container(
                      color: Colors.white,
                      height: 0.40.sh,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: EdgeInsets.all(30.0),
                            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                              AutoSizeText('Hello It\'s nice to meet you'),
                              SizedBox(height: 0.010.sh),
                              AutoSizeText(
                                'Locate your place',
                                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                              ),
                              SizedBox(height: 0.02.h),
                              GestureDetector(
                                onTap: _openMap,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(5),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey[200],
                                        spreadRadius: 1,
                                        blurRadius: 1,
                                        offset: Offset(0, 1), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  width: size.width,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Row(
                                            children: [
                                              Icon(
                                                MdiIcons.mapMarker,
                                                size: 0.02.sh,
                                                color: Colors.black54,
                                              ),
                                              SizedBox(width: 0.005.sw),
                                              Expanded(
                                                child: AutoSizeText(
                                                  addr,
                                                  style: TextStyle(
                                                    fontSize: 34.sp,
                                                    color: Colors.black54,
                                                  ),
                                                  minFontSize: 34.sp,
                                                  stepGranularity: 34.sp,
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Icon(MdiIcons.crosshairsGps, color: Config.primaryColor)
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ]),
                          ),
                          bottomNav(),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Positioned(
              right: 20,
              child: SafeArea(
                child: Opacity(
                    opacity: !playLogoAndBuildings ? 1.0 : 0.0,
                    child: GestureDetector(
                      onTap: () => ExtendedNavigator.of(context).root.push('/user'),
                      child: Icon(
                        MdiIcons.accountBox,
                        color: Colors.white,
                        size: 0.035.sh,
                      ),
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _openMap() async {
    LocationResult location = await showLocationPicker(
      context,
      Config.googleMapAPI,
      // myLocationButtonEnabled: true,
      // automaticallyAnimateToCurrentLocation: true,
      initialCenter: LatLng(25.267599586306304, 51.544433422386646),
    );

    if (location != null) {
      addr = location.address;
      ExtendedNavigator.of(context).root.push('/restaurants');
    }
  }

  //FUTURE RELEASE
  Widget bottomNav() {
    return Container(
      decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(color: Colors.grey[200], blurRadius: 5)]),
      child: SafeArea(
        top: false,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              if (!Config.comingSoonPages.contains('restaurant'))
                type(filename: 'restaurant', label: 'Restaurant', route: '/restaurants'),
              if (!Config.comingSoonPages.contains('hotel'))
                type(
                  filename: 'hotel',
                  label: 'Hotel',
                  route: '/hotels',
                ),
              if (!Config.comingSoonPages.contains('activity'))
                type(
                  filename: 'activities',
                  label: 'Activity',
                  route: '/activity',
                ),
              if (!Config.comingSoonPages.contains('market'))
                type(
                  filename: 'marketplace',
                  label: 'Marketplace',
                  route: '/market',
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget type({String filename, String label, String route, String comingSoonPageName}) {
    return GestureDetector(
      onTap: () => ExtendedNavigator.of(context).push(route),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 0.10.sw,
            width: 0.10.sw,
            alignment: Alignment.bottomLeft,
            child: Image.asset(
              'assets/images/icons/$filename.png',
              fit: BoxFit.cover,
            ),
          ),
          AutoSizeText(
            '$label',
            style: TextStyle(fontWeight: FontWeight.bold, color: Config.primaryColor),
          ),
        ],
      ),
    );
  }
}
