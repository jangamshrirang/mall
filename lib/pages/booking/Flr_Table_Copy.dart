import 'package:auto_route/auto_route.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_cache_builder.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/pages/restaurant/store/restCatogries.dart';
import 'package:wblue_customer/providers/restaurant/onlyPostApis.dart';
import 'package:wblue_customer/services/global.dart';
import '../../env/config.dart';
import '../auth/login.dart';
import 'ConformatinScreen.dart';
import 'book_table.dart';

class RestTableBookingCopy extends StatefulWidget {
  String coverImage, restimage, restName, onlybooking, restSlogan, restId;
  int restind;
  RestTableBookingCopy(
      {this.coverImage, this.restimage, this.restName, this.onlybooking, this.restSlogan, this.restId, this.restind});
  @override
  _RestTableBookingCopyState createState() => _RestTableBookingCopyState();
}

class _RestTableBookingCopyState extends State<RestTableBookingCopy> {
  String animation = "deselected";
  String animation1 = "idle";
  String animation2 = "idle";
  String animation3 = "idle";
  String animation4 = "idle";
  int value;

  UserModel user = Global.user;

  Widget _tile({String title = '', String value = '', Function onPressed: _function}) {
    return StatefulBuilder(builder: (BuildContext context, StateSetter setState2) {
      return ListTile(
        title: Text(title),
        trailing: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text("$value", style: TextStyle(color: Colors.grey, fontSize: 12.0)),
            Icon(Icons.chevron_right),
          ],
        ),
        onTap: () {
          onPressed();
        },
      );
    });
  }

  Map<String, dynamic> body = {
    'guest': stateManagment.guestNumber == null ? '1' : stateManagment.guestNumber,
    'date': '',
    'time': '',
    'Table': '',
  };

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBarWithOutIcon(context, "Table Booking"),
      body: InteractiveViewer(
        panEnabled: true,
        minScale: 0.5,
        maxScale: 5,
        child: Stack(
          children: [
            Container(
              constraints: BoxConstraints.expand(),
              height: size.height * 1,
              width: size.width * 0.9,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/floor plan.jpg"),
                ),
              ),
            ),
            flareTableContainer(
              size.height * 0.030 * 10, //heigt

              size.width * 0.048 * 10, //width from left
              "assets/images/LaMesa.flr",
              // "http://192.168.0.166:8080/app/public/flr/zP1bgWj9X2mgomyd87aZONAL/intro.flr",
              1,
              animation,
              45,
            ),
            flareTableContainer(
              size.height * 0.30,
              size.width * 0.63,
              // "assets/Rest/$Rname/$flareNAme"
              "assets/images/LaMesa.flr",
              2,
              animation,
              43,
            ),
            flareTableContainer(
              size.height * 0.040 * 10,
              size.width * 0.063 * 10,
              "assets/images/LaMesa.flr",
              3,
              animation,
              44,
            ),
            Container(
              // padding: EdgeInsets.only(left: 4, top: 8),
              margin: EdgeInsets.only(top: size.height * 0.42, left: size.width * 0.488),
              height: size.height * 0.04,
              width: size.width * 0.15,
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    value = 5;
                    if (value == 5) {
                      animation3 == "idle" || animation3 == "deselected"
                          ? animation3 = "selected"
                          : animation3 = "deselected";
                      print(value);
                    }
                    print(animation3);
                  });
                },
                child: RotationTransition(
                    turns: new AlwaysStoppedAnimation(46 / 360), child: Image.asset("assets/images/all.png")),
              ),
            ),
            flareTableContainer(
              size.height * 0.35,
              size.width * 0.56,
              "assets/images/LaMesa.flr",
              4,
              animation,
              46,
            ),
          ],
        ),
      ),
    );
  }

  flareTableContainer(
    double marginTop,
    marginLeft,
    String flareImage,
    int value,
    String animationValue,
    int rotation,
  ) {
    var size = MediaQuery.of(context).size;
    return StatefulBuilder(builder: (context, StateSetter setState2) {
      return Container(
        margin: EdgeInsets.only(top: marginTop, left: marginLeft),
        height: size.height * 0.07,
        width: size.width * 0.15,
        //  color: Colors.blue,
        child: GestureDetector(
          onTap: () {
            setState2(() {
              value = value;

              if (value == value) {
                animationValue == "idle" || animationValue == "deselected"
                    ? animationValue = "selected"
                    : animationValue = "deselected";
                print(value);
              }
              print(animationValue);
              animationValue == "idle" || animationValue == "selected" ? _modalBottomSheetMenu(value) : null;
            });
          },
          child: RotationTransition(
            turns: new AlwaysStoppedAnimation(rotation / 360),
            child: FlareActor(flareImage,
                alignment: Alignment.centerRight, animation: value == value ? animationValue : "idle"),
          ),
        ),
      );
    });
  }

//dont feel bad of more lines in this method
  void _modalBottomSheetMenu(int tableNumber) {
    var size = MediaQuery.of(context).size;

    int endHour = 23;
    int qty = stateManagment.guestNumber == null ? 1 : int.parse(stateManagment.guestNumber);

    TimeOfDay _getSetTime(TimeOfDay now) {
      DateFormat formatter = new DateFormat('yyyy-MM-dd');
      String bodyDate = body['date'] == '' ? formatter.format(DateTime.now()) : body['date'];
      String bodyTime = body['time'] == ''
          ? '${now.hour.toString().padLeft(2, '0')}:${now.minute.toString().padLeft(2, '0')}'
          : body['time'];
      print(body['time']);

      TimeOfDay set = TimeOfDay.fromDateTime(DateTime.parse('$bodyDate $bodyTime'));
      return set;
    }

    DateTime dateNow = new DateTime.now().toLocal();
    TimeOfDay now = TimeOfDay.now();
    TimeOfDay set = _getSetTime(now);

    Future<void> future = showModalBottomSheet<void>(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(40.0),
      ),
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (BuildContext context, StateSetter setState2) {
          return Container(
            height: size.height * 0.5,
            color: Colors.transparent, //could change this to Color(0xFF737373),
            //so you don't have to change MaterialApp canvasColor
            child: new Container(
                decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(50.0), topRight: const Radius.circular(50.0))),
                child: Column(
                  children: [
                    SizedBox(
                      height: size.height * 0.01,
                    ),
                    new Text(
                      "Booking",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                    ),
                    SizedBox(
                      height: size.height * 0.01,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Row(
                              children: <Widget>[
                                Text('Guest '),
                              ],
                            ),
                          ),
                          IconButton(
                              icon: Icon(Icons.remove),
                              color: Config.primaryColor,
                              onPressed: () {
                                setState2(() {
                                  if (qty >= 1) {
                                    qty--;
                                    stateManagment.setGuestnumber(qty.toString());
                                  }
                                });
                              }),
                          Text(
                            stateManagment.guestNumber == null ? "1" : stateManagment.guestNumber,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          IconButton(
                            icon: Icon(Icons.add),
                            color: Config.primaryColor,
                            onPressed: () {
                              setState2(() {
                                qty++;
                                stateManagment.setGuestnumber(qty.toString());
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                    //////
                    _tile(
                      title: 'Date',
                      value: stateManagment.date == null ? "" : stateManagment.date,
                      onPressed: () async {
                        DateTime date = await showDatePicker(
                          builder: (BuildContext context, Widget child) {
                            return Theme(
                              data: ThemeData.dark().copyWith(
                                colorScheme: ColorScheme.dark(
                                  primary: Colors.deepPurple,
                                  onPrimary: Colors.white,
                                  surface: Config.primaryColor,
                                  onSurface: Colors.white,
                                ),
                                dialogBackgroundColor: Config.primaryColor,
                              ),
                              child: child,
                            );
                          },
                          context: context,
                          initialDate: body['date'] == ''
                              ? dateNow.add(Duration(seconds: 5, days: dateNow.hour >= (endHour - 1) ? 1 : 0))
                              : DateTime.parse(body['date']).toLocal().add(Duration(hours: dateNow.hour + 1)),
                          firstDate: dateNow.hour >= (endHour - 1) ? dateNow : dateNow.subtract(Duration(days: 1)),
                          lastDate: dateNow.add(Duration(days: 30)),
                        );
                        dateNow = new DateTime.now().toLocal();

                        setState2(() {
                          DateFormat formatter = new DateFormat('yyyy-MM-dd');
                          body['date'] = formatter.format(date);
                          stateManagment.setTableBookingDate(formatter.format(date));
                        });
                      },
                    ),
                    _tile(
                      title: 'Time',
                      value: stateManagment.time == null ? "" : stateManagment.time,
                      onPressed: () async {
                        TimeOfDay time = await showTimePicker(
                            builder: (BuildContext context, Widget child) {
                              return Theme(
                                data: ThemeData.dark().copyWith(
                                  colorScheme: ColorScheme.dark(
                                    primary: Colors.white,
                                    onPrimary: Config.primaryColor,
                                    surface: Config.primaryColor,
                                    onSurface: Colors.white,
                                    onSecondary: Colors.white,
                                    onBackground: Colors.white,
                                  ),
                                  dialogBackgroundColor: Config.primaryColor,
                                ),
                                child: child,
                              );
                            },
                            context: context,
                            initialTime: body['time'] == '' ? now.replacing(hour: now.hour + 1, minute: 0) : set);

                        setState2(() {
                          body['time'] =
                              '${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}';
                          stateManagment.setTableBookingtime(body['time']);
                          print(stateManagment.time);
                        });
                      },
                    ),
                    Container(
                        child: RichText(
                      text: TextSpan(
                        style: DefaultTextStyle.of(context).style,
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Selected Table Number :',
                              style: TextStyle(
                                  decoration: TextDecoration.none,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black,
                                  fontSize: 14)),
                          TextSpan(
                              text: "$tableNumber",
                              style: TextStyle(
                                  decoration: TextDecoration.none,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 17)),
                        ],
                      ),
                    )),
                    Spacer(),
                    RaisedButton(
                      onPressed: () {
                        setState(() {
                          stateManagment.setTablenumber(tableNumber);
                          //  Navigator.of(context).pop();
                          stateManagment.date != null && stateManagment.time != null
                              ? _displayDialog(context)
                              : Fluttertoast.showToast(
                                  msg: "Please select Date and Time",
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                        });
                      },
                      color: Config.primaryColor,
                      textColor: Colors.white,
                      padding: const EdgeInsets.all(0.0),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                      child: Container(
                        decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(80.0))),
                        padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: const Text('        Confirm          ', style: TextStyle(fontSize: 20)),
                      ),
                    ),
                    Spacer(),
                  ],
                )),
          );
        });
      },
    );
    future.then((void value) => setState(() {
          animation = "deselected";
          stateManagment.setTableBookingtime(null);
          stateManagment.setTableBookingDate(null);
        }));
  }

  _displayDialog(BuildContext context) async {
    var size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
            child: Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      " Would you like to ",
                      style: TextStyle(
                        fontSize: size.height * 0.024,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 10),
                    button(" Place order", () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RestCatogries(
                                    tableID: "oBP0bax9jDmQ7JReglQzNd8M",
                                    restId: widget.restId,
                                    restindex: widget.restind,
                                    restSlogan: widget.restSlogan,
                                    // preOrder: "yes",
                                    coverImage: widget.coverImage,
                                    restName: widget.restName,
                                    restimage: widget.restimage,
                                  )));
                    }, Color(0xff101f40), 150),
                    button(" Checkout", () {
                      print("tbale no" + "${stateManagment.guestNumber}");
                      onlyTableReservatoion(
                        widget.restId,
                      );

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ConfirmationScreen(
                                  onlybooking: widget.onlybooking,
                                  tableNumber: "${stateManagment.tableNumber}",
                                  date: "${stateManagment.date}",
                                  time: "${stateManagment.time}",
                                  guest: stateManagment.guestNumber == null ? "1" : "${stateManagment.guestNumber}",
                                )),
                        // ExtendedNavigator.of(context).root.push('/book-table');
                      );
                    }, Color(0xff101f40), 100),
                    Row(
                      children: [
                        Spacer(),
                        Spacer(),
                        Spacer(),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  button(String title, Function onTap, Color color, double widthh) {
    return Container(
      width: widthh,
      child: RaisedButton(
          child: Text(
            title,
            style: TextStyle(color: Colors.white),
          ),
          color: color,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
          onPressed: onTap),
    );
  }
}

void _function() {}
