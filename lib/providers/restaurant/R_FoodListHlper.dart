import 'package:dio/dio.dart';
import 'package:wblue_customer/models/Restaurant/R.CatogeryModel.dart';
import 'package:wblue_customer/models/Restaurant/resturentMenuModel.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'dart:collection';

import 'package:flutter/foundation.dart';

DioClient _dioClient = DioClient();
// Future<List> fetchFoodCatogery() async {
//   //http://192.168.0.131:8080
//   final response = await _dioClient.publicGet('/restaurant/food-type/list');

//   List<Datum> result = new List<Datum>();
//   if (response.code == 200) {
//     response.data.map((x) {
//       result.add(Datum.fromJson(x));
//     });`
//     return result;
//   } else {
//     throw Exception('Failed to load album');
//   }
// }

class RFoodListHelper with ChangeNotifier {
  Map<String, dynamic> _rFoodMenuList = new Map<String, dynamic>();
  bool _isLoading = true;

  Map get rFoodMenu => _rFoodMenuList;
  bool get isLoading => _isLoading;

  Future<void> fetchRFoodList(String rHashId, key) async {
    setLoading(true);
    _rFoodMenuList = await fetchFood(rHashId, key);
    setLoading(false);

    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future<Map<String, dynamic>> fetchFood(String rHashId, key) async {
  Map<String, dynamic> _body = {
    'restaurant_hashid': rHashId,
    'category_hashid': key,
  };

  APIResponse res =
      await _dioClient.publicPost('/restaurant/category/list', data: _body);

  if (res.code >= 400) {
    return {};
  }

  return res.data;
}
