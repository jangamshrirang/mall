import 'dart:collection';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:wblue_customer/models/Restaurant/floor_plan_model.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();
final Dio _dio = Dio();

class PFloorPlan with ChangeNotifier {
  List<FloorPlanModel> _floors = new List<FloorPlanModel>();
  FloorPlanModel _selectedFloor;
  List _tables = new List();

  UnmodifiableListView<FloorPlanModel> get floors =>
      UnmodifiableListView<FloorPlanModel>(_floors);

  UnmodifiableListView get tables => UnmodifiableListView(_tables);

  FloorPlanModel get selectedFloor => _selectedFloor;

  void fetchFloorInfo(String hashid) async {
    Map<String, dynamic> body = {'restaurant_hash_id': hashid};

    APIResponse res = await _dioClient
        .privatePost('/restaurant/table/floor/list', data: body);

    if (res.code == 200) {
      List result = res.data;
      List<FloorPlanModel> data = new List<FloorPlanModel>();

      for (int i = 0; i < result.length; i++) {
        FloorPlanModel floorPlanModel = FloorPlanModel.fromJson(result[i]);
        data.add(floorPlanModel);
      }
      try {
        _floors = data;
      } catch (e) {
        print('ERROR: $e');
      }

      data = new List();
    }

    notifyListeners();
  }

  Future<void> selectFloor(FloorPlanModel floorPlanModel) async {
    _selectedFloor = floorPlanModel;

    fetchTables();
    // for (int i = 0; i < floorPlanModel.tables.length; i++) {
    //   print(floorPlanModel.tables[i].flrAnimation);
    //   // _dio.download(, savePath);
    // }

    notifyListeners();
  }

  Future<void> fetchTables() async {
    Random random = new Random();
    _tables.clear();

    for (int i = 0; i < _selectedFloor.tables.length; i++) {
      // var resp = await http.get(_selectedFloor.tables[i].flrAnimation);
      int randomNumber = random.nextInt(50);

      var resp = await http.get(_selectedFloor.tables[6].flrAnimation);
      // _tables.add(resp.bodyBytes);

      Map<String, dynamic> tableInfo = {
        'table': _selectedFloor.tables[i],
        'bytes': resp.bodyBytes,
        'LT': randomNumber / 1000,
        'RB': randomNumber / 1000
      };

      _tables.add(tableInfo);
    }

    notifyListeners();
  }
}
