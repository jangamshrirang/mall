import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:wblue_customer/providers/market/p_home.dart';
import 'package:wblue_customer/services/global.dart';

import '../../env/config.dart';

class MarketSearchProduct extends StatefulWidget {
  @override
  _MarketSearchProductState createState() => _MarketSearchProductState();
}

class _MarketSearchProductState extends State<MarketSearchProduct> {
  TextEditingController _controller;

  String _search = '';

  List _products = new List();

  Future _loadProduct() async {
    PMarketHome _market;

    _products = await _market.search(_search);
    print('yes');

    return _products;
  }

  @override
  void initState() {
    super.initState();
    _controller = new TextEditingController();
    _controller.text = Global.currentCategory ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0,
        backgroundColor: Colors.grey.withOpacity(0.1),
        automaticallyImplyLeading: false,
        title: SizedBox(
          height: 40,
          child: TextField(
              textInputAction: TextInputAction.send,
              controller: _controller,
              onSubmitted: (String _val) {
                _search = _val;
                _loadProduct();
              },
              decoration: new InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Config.primaryColor, width: 1.0),
                ),
                contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey.withOpacity(0.4), width: 1.0),
                ),
                hintText: 'Search',
              )),
        ),
        actions: [
          FlatButton(onPressed: () => ExtendedNavigator.of(context).pop(), child: AutoSizeText('Cancel'))
        ],
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AutoSizeText('Search Discovery'),
                Wrap(
                  spacing: 8.0,
                  children: [
                    FilterChip(
                      label: AutoSizeText('Electronics', style: TextStyle(color: Colors.black.withOpacity(0.8))),
                      backgroundColor: Colors.grey.withOpacity(0.3),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
                      onSelected: (bool value) {
                        print("selected");
                      },
                    ),
                    FilterChip(
                      label: AutoSizeText('Technology', style: TextStyle(color: Colors.black.withOpacity(0.8))),
                      backgroundColor: Colors.grey.withOpacity(0.3),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
                      onSelected: (bool value) {
                        print("selected");
                      },
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
