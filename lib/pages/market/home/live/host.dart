import 'dart:io';

import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/providers/market/live/p_host.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CallPage extends StatefulWidget {
  final String title;
  final File contentThumbnail;

  const CallPage({this.title, this.contentThumbnail});
  @override
  _CallPageState createState() => _CallPageState();
}

class _CallPageState extends State<CallPage> {
  @override
  void initState() {
    super.initState();
    context.read<PHostLive>().initialize();
    context.read<PHostLive>().createClient();

    context
        .read<PHostLive>()
        .setupLiveContent(title: widget.title, img: widget.contentThumbnail);
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews(PHostLive host) {
    final List<StatefulWidget> list = [];
    list.add(RtcLocalView.SurfaceView());
    host.users
        .forEach((int uid) => list.add(RtcRemoteView.SurfaceView(uid: uid)));
    return list;
  }

  /// Video view wrapper
  Widget _videoView(view) {
    return Expanded(child: Container(child: view));
  }

  /// Video layout wrapper
  Widget _viewRows(PHostLive host) {
    final views = _getRenderViews(host);

    if (views.length > 0)
      return Container(
          child: Column(
        children: <Widget>[_videoView(views[0])],
      ));

    return Container();
  }

  /// Toolbar layout
  Widget _toolbar(PHostLive host) {
    return Container(
      alignment: Alignment.bottomCenter,
      padding: const EdgeInsets.symmetric(vertical: 48),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: host.onToggleMute,
            child: Container(
              width: 40,
              height: 40,
              child: Icon(
                host.muted ? Icons.mic_off : Icons.mic,
                color: Config.primaryColor,
                size: 20,
              ),
              decoration:
                  BoxDecoration(shape: BoxShape.circle, color: Colors.white),
            ),
          ),
          SizedBox(width: 20),
          GestureDetector(
            onTap: host.onSwitchCamera,
            child: Container(
              width: 40,
              height: 40,
              child: Icon(
                Icons.switch_camera,
                color: Config.primaryColor,
                size: 20,
              ),
              decoration:
                  BoxDecoration(shape: BoxShape.circle, color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  /// Info panel to show logs
  Widget _panel(PHostLive host) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 48),
      alignment: Alignment.bottomCenter,
      child: FractionallySizedBox(
        heightFactor: 0.5,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 48),
          child: ListView.builder(
            reverse: true,
            itemCount: host.infoStrings.length,
            itemBuilder: (BuildContext context, int index) {
              if (host.infoStrings.isEmpty) {
                return null;
              }
              return Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 3,
                  horizontal: 10,
                ),
                child: (host.infoStrings[index].type == 'join')
                    ? Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 8,
                              ),
                              child: Text(
                                '${host.infoStrings[index].user} joined',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    : (host.infoStrings[index].type == 'message')
                        ? Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: 0.10.sw,
                                  height: 0.10.sw,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Colors.grey,
                                  ),
                                  child: CachedNetworkImage(
                                      imageUrl:
                                          host.infoStrings[index].image ?? '',
                                      imageBuilder: (context, imageProvider) =>
                                          Container(
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(30),
                                              image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                      errorWidget: (context, url, error) => Center(
                                          child: Text(
                                              '${host.infoStrings[index].user[0]}',
                                              style: TextStyle(
                                                  color: Colors.white)))),
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                          horizontal: 8,
                                        ),
                                        child: Text(
                                          host.infoStrings[index].user,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                          horizontal: 8,
                                        ),
                                        child: Text(
                                          host.infoStrings[index].message,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 14),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        : null,
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _timer() {
    return Selector<PHostLive, String>(
      builder: (_, timer, __) {
        return Container(
            padding: EdgeInsets.all(8.0),
            decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.all(Radius.circular(
                      5.0) //                 <--- border radius here
                  ),
            ),
            child: Row(
              children: [
                Text('LIVE',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold)),
                SizedBox(width: 5),
                Text('$timer',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold))
              ],
            ));
      },
      selector: (_, timer) => timer.timerDisplay,
    );
  }

  Widget _viwers(PHostLive host) {
    return Container(
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: Colors.black26,
          borderRadius: BorderRadius.all(
              Radius.circular(5.0) //                 <--- border radius here
              ),
        ),
        child: Row(
          children: [
            Icon(
              MdiIcons.eye,
              color: Colors.white,
              size: 18,
            ),
            SizedBox(width: 5),
            Text('${host.userNo}',
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.w200)),
          ],
        ));
  }

  Widget _timerWithViewers(PHostLive host) {
    return Positioned(
        left: 10,
        child: SafeArea(
          child: Row(
            children: [_timer(), SizedBox(width: 5), _viwers(host)],
          ),
        ));
  }

  Widget _finishBtn(PHostLive host) {
    return Positioned(
      right: 10,
      child: GestureDetector(
        onTap: () {
          showAlertDialog(BackButtonBehavior.none, cancel: () {}, confirm: () {
            host.disposeEngine();
            Navigator.pop(context);
          }, backgroundReturn: () {});
        },
        child: SafeArea(
          child: Container(
              padding: EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.all(Radius.circular(
                        5.0) //                 <--- border radius here
                    ),
              ),
              child: Text('FINISH', style: TextStyle(color: Colors.white))),
        ),
      ),
    );
  }

  void showAlertDialog(BackButtonBehavior backButtonBehavior,
      {VoidCallback cancel,
      VoidCallback confirm,
      VoidCallback backgroundReturn}) {
    BotToast.showAnimationWidget(
        clickClose: true,
        allowClick: false,
        onlyOne: true,
        crossPage: true,
        backButtonBehavior: backButtonBehavior,
        toastBuilder: (cancelFunc) => AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              title: const Text('Are you sure to end this live?'),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    cancelFunc();
                    cancel?.call();
                  },
                  highlightColor: const Color(0x55FF8A80),
                  splashColor: const Color(0x99FF8A80),
                  child: const Text(
                    'cancel',
                    style: TextStyle(color: Colors.redAccent),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    cancelFunc();
                    confirm?.call();
                  },
                  child: const Text('confirm'),
                ),
              ],
            ),
        animationDuration: Duration(milliseconds: 300));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Consumer<PHostLive>(
          builder: (__, host, ___) => Stack(
            children: <Widget>[
              _viewRows(host),
              _panel(host),
              _toolbar(host),
              _timerWithViewers(host),
              _finishBtn(host),
            ],
          ),
        ),
      ),
    );
  }
}
