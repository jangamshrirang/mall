import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class WTopUpCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.lightBlue,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              decoration: BoxDecoration(
                  color: Colors.redAccent.withOpacity(0.3),
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(10.0))),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: AutoSizeText('Mobile Top Up'),
              )),
          AutoSizeText('50.00 QAR'),
          AutoSizeText('Price: 47.50 QAR'),
          SizedBox(height: 20.0)
        ],
      ),
    );
  }
}
