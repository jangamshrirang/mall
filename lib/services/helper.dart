import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:wblue_customer/models/auth.dart';

class Helper {
  Helper() {
    fetchLoggedInHashId();
  }

  static bool isFirstLunch = true;
  static Map<String, dynamic> state = {};
  static AuthModel authInfo = new AuthModel();
  static String loggedInUserHashid;

  static String defaultAddrHashid = 'Y2vXWzeN3rJzPmxMOVK4bB0y';

  // HIVE
  static Box currentAuthUser = Hive.box('auth');
  static Box currentUserProfile = Hive.box('profile');

  static void setUserHive({String token, Map<String, dynamic> user}) async {
    await currentAuthUser.put('token', token);
    await currentAuthUser.put('user', user);
  }

  static CameraPosition newLocation;

  void fetchLoggedInHashId() async {
    Map auth = await currentAuthUser.get('user');
    // loggedInUserHashid = auth['hashid'];
  }
}

class StatesNames {
  static String token = 'token';
  static String selectedCountry = 'selected_country';
  static var firebaseNotificationToken = '';
}

class BusinessType {
  static String marketplace = 'marketplace';
  static String hotel = 'hotel';
  static String restaurant = 'restaurant';
  static String activities = 'marketplace';
}

class WMallPayment {
  static int cod = 1;
  static int bankTransfer = 2;
  static int paypal = 3;
}

enum WMallOrderType { dineIn, delivery, takeAway }
