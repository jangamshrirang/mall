import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/widgets/w_image.dart';

class WSubCategoryWidget extends StatelessWidget {
  final ImageProvider productImg;
  final String productName;

  WSubCategoryWidget({this.productImg, this.productName});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          Container(
            height: 70,
            width: 70,
            child: WImageWidget(
              placeholder: productImg,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: AutoSizeText(
              '$productName',
              maxLines: 1,
              style: TextStyle(fontSize: 14),
              minFontSize: 14,
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    );
  }
}
