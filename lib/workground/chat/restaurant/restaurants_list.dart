import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'package:wblue_customer/workground/chat/restaurant/restaurant_table.dart';

class RestaurantListPage extends StatefulWidget {
  @override
  _RestaurantListPageState createState() => _RestaurantListPageState();
}

DioClient _dioClient = DioClient();

class _RestaurantListPageState extends State<RestaurantListPage> {
  List restaurant = List();
// /restaurant/by-restaurant

  Future fetchRestaurants() async {
    APIResponse res = await _dioClient.privateGet('/restaurant/by-restaurant');
    if (res.code == 200) return res.data;
    return new List();
  }

  Widget restaurants(Size size) {
    return FutureBuilder(
      builder: (context, restaurantSnap) {
        if (restaurantSnap.connectionState == ConnectionState.none &&
            restaurantSnap.hasData == null) {
          return Container();
        }
        if (restaurantSnap.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }

        return ListView.builder(
          itemCount: restaurantSnap.data?.length,
          itemBuilder: (context, index) => Container(
            child: GestureDetector(
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RestaurantTablePage(
                            restId: restaurantSnap.data[index]['hash_id'],
                          ))),
              child: Card(
                  child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: [
                    CachedNetworkImage(
                      imageUrl: restaurantSnap.data[index]['full'] ?? '',
                      imageBuilder: (context, imageProvider) => ClipRRect(
                        borderRadius: BorderRadius.circular(0.015 * size.width),
                        child: Container(
                          height: 100,
                          width: 100,
                          child: Image.network(
                            restaurantSnap.data[index]['full'] ?? '',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => ClipRRect(
                        borderRadius: BorderRadius.circular(0.015 * size.width),
                        child: Container(
                          height: 100,
                          width: 100,
                          child: Image.network(
                            restaurantSnap.data[index]['thumbnail'] ?? '',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      errorWidget: (context, url, error) => Container(
                        height: 0.26 * size.height,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            AutoSizeText(
                              'Image Not Found',
                              style: TextStyle(
                                  fontSize: 28, fontWeight: FontWeight.w300),
                            ),
                            SizedBox(
                              height: 0.02 * size.height,
                            ),
                            Icon(
                              MdiIcons.alertCircle,
                              color: Colors.red,
                              size: 0.08 * size.height,
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 10),
                    Text('${restaurantSnap.data[index]['hash_id']}'),
                  ],
                ),
              )),
            ),
          ),
        );
      },
      future: fetchRestaurants(),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(body: restaurants(size));
  }
}
