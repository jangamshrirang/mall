class SearchModel {
    SearchModel({
        this.restaurant,
        this.food,
    });

    List<Restaurant> restaurant;
    List<Food> food;

    factory SearchModel.fromJson(Map<String, dynamic> json) => SearchModel(
        restaurant: json["Restaurant"] == null ? null : List<Restaurant>.from(json["Restaurant"].map((x) => Restaurant.fromJson(x))),
        food: json["Food"] == null ? null : List<Food>.from(json["Food"].map((x) => Food.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "Restaurant": restaurant == null ? null : List<dynamic>.from(restaurant.map((x) => x.toJson())),
        "Food": food == null ? null : List<dynamic>.from(food.map((x) => x.toJson())),
    };
}

class Food {
    Food({
        this.food,
        this.foodHashId,
        this.restaurntHashId,
        this.full,
        this.thumbnail,
    });

    String food;
    String foodHashId;
    String restaurntHashId;
    String full;
    String thumbnail;

    factory Food.fromJson(Map<String, dynamic> json) => Food(
        food: json["food"] == null ? null : json["food"],
        foodHashId: json["food_hash_id"] == null ? null : json["food_hash_id"],
        restaurntHashId: json["restaurnt_hash_id"] == null ? null : json["restaurnt_hash_id"],
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "food": food == null ? null : food,
        "food_hash_id": foodHashId == null ? null : foodHashId,
        "restaurnt_hash_id": restaurntHashId == null ? null : restaurntHashId,
        "full": full == null ? null : full,
        "thumbnail": thumbnail == null ? null : thumbnail,
    };
}

class Restaurant {
    Restaurant({
        this.restaurant,
        this.hashId,
        this.full,
        this.thumbnail,
    });

    String restaurant;
    String hashId;
    String full;
    String thumbnail;

    factory Restaurant.fromJson(Map<String, dynamic> json) => Restaurant(
        restaurant: json["restaurant"] == null ? null : json["restaurant"],
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "restaurant": restaurant == null ? null : restaurant,
        "hash_id": hashId == null ? null : hashId,
        "full": full == null ? null : full,
        "thumbnail": thumbnail == null ? null : thumbnail,
    };
}
