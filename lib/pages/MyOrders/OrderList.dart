import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/MyOrderListModel.dart';
import 'package:wblue_customer/providers/restaurant/p_my_order_list.dart';
import 'package:wblue_customer/providers/socket/orderStatus.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/workground/map.dart';
import 'dart:ui' as ui;
import 'package:flutter/services.dart';
import 'LiveTrakingMap.dart';
import 'MyOrderMapCard.dart';

class MyordeList extends StatefulWidget {
  @override
  _MyordeListState createState() => _MyordeListState();
}

class _MyordeListState extends State<MyordeList> {
  void initState() {
    print(Helper.state[StatesNames.token]);
    Future.microtask(() => context.read<PMyOrderList>().fetchrMyOrderList());
    gettingUserDetails();
    super.initState();
  }

  String updatedStatus, userHashId;
  String dt;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Config.primaryColor,
        title: Text('My Orders'),
      ),
      body: Consumer<PMyOrderList>(
        builder: (context, restaurant, child) => restaurant.myOrders.length > 0
            ? ListView(
                children: List.generate(restaurant.myOrders.length, (index) {
                MyOrderListModel myOrderListModel =
                    MyOrderListModel.fromJson(restaurant.myOrders[index]);
                double rLAt = double.parse(myOrderListModel.storeLatitude);
                double rLANG = double.parse(myOrderListModel.storeLongitude);
                double cLAT = double.parse(myOrderListModel.shippingLatitude);
                double cLANG = double.parse(myOrderListModel.shippingLongitude);
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LiveTrackingMap(
                            orderStatus:
                                updatedStatus == null || updatedStatus == ""
                                    ? myOrderListModel.orderStatus
                                    : updatedStatus,
                            rLat: rLAt,
                            //  myOrderListModel.storeLatitude,
                            rLang: rLANG,
                            // restaurant.myOrderListModel.storeLongitude,
                            cLang: cLANG,
                            // restaurant.myOrderListModel.shippingLatitude,
                            cLat: cLAT,
                            // restaurant.myOrderListModel.shippingLongitude,
                            orderHashID: myOrderListModel.orderHashId,
                            rName: myOrderListModel.restaurantName,
                            date: "${myOrderListModel.orderDate}",
                            time: myOrderListModel.orderTime,
                            //  rImage: myOrderListModel.thumbnail,
                          ),

                          // MyOrderMapCard(
                          //       rLangitude: "25.288363",
                          //       rLattitude: "51.527171",
                          //     )
                          // MapPage(
                          //       destLocation: LatLng(
                          //           25.336249101767294, 51.48123353719711),
                          //       sourceLocation:
                          //           LatLng(25.2677555, 51.54440579999999),
                          //     )
                        ));
                  },
                  child: Card(
                    child: Container(
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('${myOrderListModel.restaurantName}',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 32.sp)),
                                SizedBox(height: 0.01.sh),
                                // variations(restaurant, index),

                                Text(
                                    "Date: ${myOrderListModel.orderDate.toString().substring(0, 10)}"),
                                SizedBox(height: 0.01.sh),
                                Text("Time: ${myOrderListModel.orderTime}"),

                                // to update status
                                Consumer<OrderStatusScocket>(
                                    builder: (_, orderStatus, __) {
                                  if (orderStatus.isUpdated) {
                                    print(
                                        "itting the scoket consumer in screen");
                                    print(orderStatus
                                        .updatedstatusData['status']);
                                    updatedStatus =
                                        orderStatus.updatedstatusData['status'];
                                    return Text("");
                                  } else {
                                    return Text("");
                                  }
                                })
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 0.05.sw,
                          ),
                          Container(
                            width: 0.18.sw,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              color: _colorCode(myOrderListModel.orderStatus),
                            ),
                            padding: EdgeInsets.all(5.0),
                            child: Center(
                              //here we r integrating socket to check the status updated or not
                              child: Consumer<OrderStatusScocket>(
                                  builder: (_, orderStatus, __) {
                                print("itting the scoket consumer in screen");

                                if (orderStatus.isUpdated &&
                                    myOrderListModel
                                            .orderHashId == //order hash id
                                        orderStatus.updatedstatusData[
                                            'order_hashid'] &&
                                    userHashId == //user hash id
                                        orderStatus
                                            .updatedstatusData['user_hashid']) {
                                  print(
                                      orderStatus.updatedstatusData['status']);
                                  context
                                      .read<OrderStatusScocket>()
                                      .setOrderStatusUpdated(false);
                                  return Text(
                                      orderStatus.updatedStatusData['status'],
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 32.sp));
                                } else {
                                  print(
                                      "socket user hashId is not matching to local user hashId");
                                  print(
                                      "socket data : ${orderStatus.updatedstatusData} && local user hashID: ${Helper.loggedInUserHashid}");
                                  return Text(
                                    '${myOrderListModel.orderStatus}',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 24.sp),
                                    textAlign: TextAlign.center,
                                  );
                                }
                              }),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }))
            : ListView(
                physics: ClampingScrollPhysics(),
                children: List.generate(
                    MediaQuery.of(context).size.height ~/ 5,
                    (index) => Column(
                          children: [
                            SizedBox(
                              height: 0.01.sh,
                            ),
                            Container(
                              width: 1.sw,
                              height: 0.1.sh,
                              child: Shimmer.fromColors(
                                baseColor: Color.fromRGBO(0, 0, 0, 0.1),
                                highlightColor: Color(0x44CCCCCC),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )),
              ),
      ),
    );
  }

  Color _colorCode(String status) {
    Color color;

    if (status == 'Pending')
      color = OrderColor.pending;
    else if (status == 'Preparing')
      color = OrderColor.preparing;
    else if (status == 'Ready To Deliver')
      color = OrderColor.readyToDelivery;
    else if (status == 'Delivering')
      color = OrderColor.readyToDelivery;
    else
      color = Config.primaryColor;

    return color;
  }

  // Container variations(PMyOrderList restaurant, int index) {
  //   String items = '';

  //   List.generate(
  //       restaurant.myOrderListModel.foodDetails.length,
  //       (foodIndex) => items +=
  //           '${restaurant.myOrderListModel.foodDetails[foodIndex].name}${restaurant.myOrderListModel.foodDetails.length - 1 == foodIndex + 1 ? ', ' : ''}');
  //   return Container(
  //       width: 1.sw,
  //       child: Text(
  //         '$items',
  //         style: TextStyle(fontSize: 30.sp, color: Colors.black87),
  //         overflow: TextOverflow.ellipsis,
  //       ));
  // }

  Widget orderDate(PMyOrderList restaurant, int index) {
    try {
      DateFormat format = DateFormat("yyyy-MM-dd hh:mm");
      DateTime formattedFromApi = format.parse(
          '${restaurant.myOrders[index].orderDate} ${restaurant.myOrders[index].orderTime}');
      dt = DateFormat("dd MMM, hh:mm").format(formattedFromApi);
      print("Date $dt");
      return Text('$dt',
          style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 28.sp,
              color: Colors.black54));
    } catch (e) {
      return SizedBox();
    }
  }

  gettingUserDetails() async {
    final currentAuthUser = Hive.box('auth');
    Map user = await currentAuthUser.get('user');
    setState(() {
      userHashId = user['hashid'];
    });
  }
}
class OrderColor { static const Color pending = Colors.orange; static const Color preparing = Colors.blue; static const Color readyToDelivery = Colors.purple;}