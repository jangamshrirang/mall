import 'package:device_preview/device_preview.dart' as dp;
import 'package:auto_route/auto_route.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:camera/camera.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flare_flutter/flare_cache.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/providers/Reservations/p_reservation.dart';
import 'package:wblue_customer/providers/auth/p_auth.dart';
import 'package:wblue_customer/providers/auth/phone_auth.dart';
import 'package:wblue_customer/providers/market/live/p_create_live.dart';
import 'package:wblue_customer/providers/market/live/p_host.dart';
import 'package:wblue_customer/providers/market/p_bottom_nav.dart';
import 'package:wblue_customer/providers/market/p_carousel.dart';
import 'package:wblue_customer/providers/market/p_cart.dart';
import 'package:wblue_customer/providers/market/p_fullscreen_image.dart';
import 'package:wblue_customer/providers/market/p_home.dart';
import 'package:wblue_customer/providers/market/p_product_recognizer.dart';
import 'package:wblue_customer/providers/market/p_store.dart';
import 'package:wblue_customer/providers/market/p_view_product.dart';
import 'package:wblue_customer/providers/order/p_order.dart';
import 'package:wblue_customer/providers/p_loading.dart/loading.dart';
import 'package:wblue_customer/providers/restaurant/checkout/p_user_address.dart';
import 'package:wblue_customer/providers/restaurant/p_MyOrdersExpanion.dart';
import 'package:wblue_customer/providers/restaurant/p_my_order_list.dart';
import 'package:wblue_customer/providers/restaurant/R.CatogeryHelper.dart';
import 'package:wblue_customer/providers/restaurant/R_AddToCartHelper.dart';
import 'package:wblue_customer/providers/restaurant/R_FoodListHlper.dart';
import 'package:wblue_customer/providers/restaurant/R_GetCartHelpr.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/providers/restaurant/p_food_list_type.dart';
import 'package:wblue_customer/providers/restaurant/p_restaurant_list.dart';
import 'package:wblue_customer/providers/restaurant/R_ListByRatings.dart';
import 'package:wblue_customer/providers/restaurant/R_remove_item.dart';
import 'package:wblue_customer/providers/restaurant/R_searchHelper.dart';
import 'package:wblue_customer/providers/restaurant/RestSearchResult.dart';
import 'package:wblue_customer/providers/restaurant/SearcFoodDetailHelper.dart';
import 'package:wblue_customer/providers/restaurant/p_qr_reservation.dart';
import 'package:wblue_customer/providers/restaurant/p_restaurant_message.dart';
import 'package:wblue_customer/providers/restaurant/p_variation.dart';
import 'package:wblue_customer/providers/restaurant/pbrowse.dart';
import 'package:wblue_customer/providers/socket/client.dart';
import 'package:wblue_customer/providers/socket/driverCoordinates.dart';
import 'package:wblue_customer/providers/socket/orderStatus.dart';
import 'package:wblue_customer/providers/user/profile.dart';
import 'package:wblue_customer/routes/route_guards.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/workground/chat/ping/p_ping.dart';
import 'package:wblue_customer/workground/chat/restaurant/p_floor_plan.dart';

import 'env/config.dart';
import 'models/country.dart';
import 'providers/market/p_camera.dart';

Injector injector;

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

List<CameraDescription> cameras = [];

List _assetsToWarmup = [
  AssetFlare(
    bundle: rootBundle,
    name: 'assets/flares/w-loading.flr',
  ),
  AssetFlare(
    bundle: rootBundle,
    name: 'assets/flares/market/w-scan.flr',
  ),
  AssetFlare(
    bundle: rootBundle,
    name: 'assets/flares/w-mall.flr',
  ),
  AssetFlare(
    bundle: rootBundle,
    name: 'assets/flares/landing/blue.flr',
  ),
  AssetFlare(
    bundle: rootBundle,
    name: 'assets/flares/landing/building.flr',
  ),
  AssetFlare(
    bundle: rootBundle,
    name: 'assets/flares/landing/w-mall.flr',
  ),
  AssetFlare(
    bundle: rootBundle,
    name: 'assets/flares/landing/cooking.flr',
  ),
  AssetFlare(
    bundle: rootBundle,
    name: 'assets/flares/landing/deliveryman.flr',
  ),
  //  AssetFlare(
  //   bundle: rootBundle,
  //   name: 'assets/flares/landing/2.flr',
  // ),
  //  AssetFlare(
  //   bundle: rootBundle,
  //   name: 'assets/flares/landing/3.flr',
  // ),
  //  AssetFlare(
  //   bundle: rootBundle,
  //   name: 'assets/flares/landing/4.flr',
  // ),
  //  AssetFlare(
  //   bundle: rootBundle,
  //   name: 'assets/flares/landing/5.flr',
  // ),
  //  AssetFlare(
  //   bundle: rootBundle,
  //   name: 'assets/flares/landing/6.flr',
  // ),
  //  AssetFlare(
  //   bundle: rootBundle,
  //   name: 'assets/flares/landing/7.flr',
  // ),
  //  AssetFlare(
  //   bundle: rootBundle,
  //   name: 'assets/flares/landing/8.flr',
  // ),
];

Future<void> warmupFlare() async {
  for (final asset in _assetsToWarmup) {
    await cachedActor(asset);
  }
}

void main() async {
  try {
    Helper.state[StatesNames.selectedCountry] = CountryModel.defaultItems[0];
    WidgetsFlutterBinding.ensureInitialized();
    FlareCache.doesPrune = false;
    await Firebase.initializeApp();
    final appDoc = await getApplicationDocumentsDirectory();

    Hive.init(appDoc.path);
    await Hive.openBox('auth');
    await Hive.openBox('profile');
    await Hive.openBox('area');

    cameras = await availableCameras();
    Global.user = UserModel.createDummy();
  } on CameraException catch (e) {
    logError(e.code, e.description);
  }

  runApp(
    MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => PBottomNavigation()),
          ChangeNotifierProvider(create: (_) => PCarouselCurrentPage()),
          ChangeNotifierProvider(create: (_) => PMarketHome()),
          ChangeNotifierProvider(create: (_) => PFullscreenImage()),
          ChangeNotifierProvider(create: (_) => PViewProduct()),
          ChangeNotifierProvider(create: (_) => PMarketCart()),
          ChangeNotifierProvider(create: (_) => PProductRecognizer()),
          ChangeNotifierProvider(create: (_) => PCreateLive()),
          ChangeNotifierProvider(create: (_) => PCamera()),
          ChangeNotifierProvider(create: (_) => PhoneAuthDataProvider()),
          ChangeNotifierProvider(create: (_) => PAuth()),
          ChangeNotifierProvider(create: (_) => PSocketClient()),
          ChangeNotifierProvider(create: (_) => PUserProfile()),
          ChangeNotifierProvider(create: (_) => PRestaurantList()),
          ChangeNotifierProvider(create: (_) => R_ListRatingHelper()),
          ChangeNotifierProvider(create: (_) => RFoodListHelper()),
          ChangeNotifierProvider(create: (_) => RCategariesListHelper()),
          ChangeNotifierProvider(create: (_) => R_AddToCartHelper()),
          ChangeNotifierProvider(create: (_) => RGetCartHelper()),
          ChangeNotifierProvider(create: (_) => R_RemoveItemCartHelper()),
          ChangeNotifierProvider(create: (_) => R_SearchHelper()),
          ChangeNotifierProvider(create: (_) => PBrowseRestaurant()),
          ChangeNotifierProvider(create: (_) => PStore()),
          ChangeNotifierProvider(create: (_) => PFloorPlan()),
          ChangeNotifierProvider(create: (_) => PPingStaff()),
          ChangeNotifierProvider(create: (_) => PRestaurantMessage()),
          ChangeNotifierProvider(create: (_) => PFoodListType()),
          ChangeNotifierProvider(create: (_) => PRestaurantFoodVariation()),
          ChangeNotifierProvider(create: (_) => PRestaurantFoodCart()),
          ChangeNotifierProvider(create: (_) => PQRReserevation()),
          ChangeNotifierProvider(create: (_) => PRestaurantUserAddress()),
          ChangeNotifierProvider(create: (_) => PRestaurantOrders()),

          // sandy Add the below
          ChangeNotifierProvider(create: (_) => PReservation()),
          ChangeNotifierProvider(create: (_) => PHostLive()),
          ChangeNotifierProvider(create: (_) => PLoading()),
          ChangeNotifierProvider(create: (_) => PMyOrderList()),
          ChangeNotifierProvider(create: (_) => R_SearchResultHelper()),
          ChangeNotifierProvider(create: (_) => SearchFoodResultHelper()),
          ChangeNotifierProvider(create: (_) => OrderStatusScocket()),
          ChangeNotifierProvider(create: (_) => DriverCoordinatesScocket()), //
          ChangeNotifierProvider(create: (_) => MyOrderExpantionHelper()),
        ],
        child: dp.DevicePreview(
          builder: (context) => MyApp(),
        )),
  );
  // ], child: ChatPage()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    _portraitModeOnly();

// Wakelock.enable();

//    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//      statusBarColor: Colors.transparent,
//      statusBarBrightness: Brightness.light,
//      statusBarIconBrightness: Brightness.dark,
//      systemNavigationBarColor: Colors.white,
//      systemNavigationBarIconBrightness: Brightness.dark,
//    ));

//    return MaterialApp(
//      home: MyHomePage(),
//    );

    final botToastBuilder = BotToastInit(); //1. call BotToastInit
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Config.primaryColor,
        accentColor: Colors.green,
        textTheme: TextTheme(bodyText2: TextStyle(color: Colors.purple)),
      ),
      debugShowCheckedModeBanner: false,
      title: 'W Mall',
      builder: ExtendedNavigator.builder<AppRouter>(
          router: AppRouter(),
          guards: [AuthGuard(), LocGuard()],
          initialRoute: '/',
          observers: [BotToastNavigatorObserver()],
          builder: (context, child) {
            child = Theme(
              data: ThemeData(fontFamily: 'Montserrat'),
              child: child,
            );
            child = botToastBuilder(context, child);
            return child;
          }),
    );
  }

  /// blocks rotation; sets orientation to: portrait
  void _portraitModeOnly() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
}
