import 'dart:convert';

import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_shimmer/flutter_shimmer.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/auth.dart';
import 'package:wblue_customer/providers/auth/p_auth.dart';
import 'package:wblue_customer/providers/user/profile.dart';
import 'package:wblue_customer/services/default.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/extensions/e_auth_user.dart';

class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  AuthModel authApi = AuthModel();

  @override
  void initState() {
    super.initState();
    Future.microtask(() => context.read<PUserProfile>().fetchUserProfile());
  }

  final key = new GlobalKey();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Config.primaryColor,
          title: Text('Profile'),
          centerTitle: false,
          elevation: 0.0,
        ),
        body: Consumer<PUserProfile>(builder: (_, profile, __) {
          if (profile.user.hashid != null) {
            stateManagment.setUserMobileNumber(profile.user.mobileNumber);
            return Column(
              children: [
                Container(
                  width: size.width,
                  color: Colors.grey[100],
                  child: Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () {
                            print(Helper.loggedInUserHashid.address());
                          },
                          child: Container(
                            width: 0.12.sw,
                            height: 0.12.sw,
                            child: CachedNetworkImage(
                                imageUrl: profile.user.full,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) => Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        image: DecorationImage(
                                          image: MemoryImage(
                                              base64Decode(Default.avatar)),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    )),
                          ),
                        ),
                        SizedBox(
                          width: 8.0,
                        ),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                AutoSizeText(
                                  '${profile.user.name}',
                                  style: TextStyle(color: Colors.black87),
                                  maxLines: 1,
                                  minFontSize: 36.sp,
                                  stepGranularity: 36.sp,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                FlatButton(
                                  onPressed: () async {
                                    final res = await authApi.logout();
                                    if (res) {
                                      context.read<PAuth>().logout();
                                      ExtendedNavigator.of(context)
                                          .root
                                          .popUntilPath('/');
                                    }
                                  },
                                  child: AutoSizeText(
                                    'Logout',
                                    style: TextStyle(
                                        color: Colors.red, fontSize: 28.sp),
                                  ),
                                )
                              ],
                            ),
                            profile.user.mobileNumber == null
                                ? Row(
                                    children: [
                                      AutoSizeText(
                                        'Not Verified',
                                        style: TextStyle(
                                            color: Colors.redAccent[100]),
                                        maxLines: 1,
                                        minFontSize: 20.sp,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      SizedBox(width: 0.02.sw),
                                      Tooltip(
                                          key: key,
                                          message:
                                              'Please bind your phone to your account',
                                          child: GestureDetector(
                                            child: Icon(MdiIcons.information,
                                                size: 0.05.sw,
                                                color: Colors.black54),
                                            onTap: () {
                                              final dynamic tooltip =
                                                  key.currentState;
                                              tooltip.ensureTooltipVisible();
                                            },
                                          ))
                                    ],
                                  )
                                : SizedBox(),
                          ],
                        ))
                      ],
                    ),
                  ),
                ),
                Card(
                  child: ListTile(
                    title: AutoSizeText(
                      'Name',
                      minFontSize: 18.0,
                    ),
                    subtitle: AutoSizeText(
                      '${profile.user.name}',
                      minFontSize: 16.0,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    dense: true,
                    trailing: Icon(MdiIcons.chevronRight),
                  ),
                ),
                profile.user.email != null
                    ? Card(
                        child: ListTile(
                          title: AutoSizeText(
                            'Email',
                            minFontSize: 18.0,
                          ),
                          subtitle: AutoSizeText(
                            '${profile.user.email}',
                            minFontSize: 16.0,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          dense: true,
                          trailing: Icon(MdiIcons.chevronRight),
                        ),
                      )
                    : SizedBox(),
                profile.user.mobileNumber != null
                    ? Card(
                        child: ListTile(
                          title: AutoSizeText(
                            'Phone',
                            minFontSize: 18.0,
                          ),
                          subtitle: AutoSizeText(
                            '${Helper.state[StatesNames.selectedCountry]['dialCode']}${profile.user.mobileNumber}',
                            minFontSize: 16.0,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          dense: true,
                          trailing: Icon(MdiIcons.chevronRight),
                        ),
                      )
                    : SizedBox(),
              ],
            );
          } else {
            return Column(
              children: [
                ProfileShimmer(),
                ListTileShimmer(),
              ],
            );
          }
        }));
  }
}
