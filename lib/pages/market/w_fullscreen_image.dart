import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:provider/provider.dart';

import '../../env/config.dart';
import 'package:wblue_customer/providers/market/p_fullscreen_image.dart';
import 'package:wblue_customer/widgets/w_image.dart';

class WProductImage extends StatefulWidget {
  WProductImage({
    this.loadingBuilder,
    this.backgroundDecoration,
    this.minScale,
    this.maxScale,
    this.initialIndex: 1,
    this.scrollDirection = Axis.horizontal,
  });

  final LoadingBuilder loadingBuilder;
  final Decoration backgroundDecoration;
  final dynamic minScale;
  final dynamic maxScale;
  final int initialIndex;
  final Axis scrollDirection;

  @override
  _WProductImageState createState() => _WProductImageState();
}

class _WProductImageState extends State<WProductImage> {
  List galleryItems;
  PageController pageController;

  int currentIndex;

  void onPageChanged(int index) {
    context.read<PFullscreenImage>().selectImageIndex(index);
  }

  @override
  void initState() {
    currentIndex = context.read<PFullscreenImage>().selectedImgIndex;
    pageController = PageController(initialPage: currentIndex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Consumer<PFullscreenImage>(
          builder: (context, img, child) {
            galleryItems = img.images;
            return Container(
              decoration: widget.backgroundDecoration,
              constraints: BoxConstraints.expand(
                height: MediaQuery.of(context).size.height,
              ),
              child: Stack(
                alignment: Alignment.bottomRight,
                children: <Widget>[
                  PhotoViewGallery.builder(
                    scrollPhysics: const BouncingScrollPhysics(),
                    builder: _buildItem,
                    itemCount: img.images.length,
                    loadingBuilder: widget.loadingBuilder,
                    backgroundDecoration: widget.backgroundDecoration,
                    pageController: pageController,
                    onPageChanged: onPageChanged,
                    scrollDirection: widget.scrollDirection,
                  ),
                  Container(
                    height: 100,
                    child: ListView.builder(
                        itemCount: img.images.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) => GestureDetector(
                              onTap: () {
                                context.read<PFullscreenImage>().selectImageIndex(index);
                                pageController.jumpToPage(index);
                                print('${img.selectedImgIndex}');
                              },
                              child: Container(
                                height: 100,
                                width: 100,
                                decoration: BoxDecoration(border: Border.all(color: context.watch<PFullscreenImage>().selectedImgIndex == index ? Config.primaryColor : Colors.grey.withOpacity(0.3))),
                                margin: EdgeInsets.all(5.0),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: WImageWidget(
                                    placeholder: AssetImage(img.images[index]['small']),
                                  ),
                                ),
                              ),
                            )),
                  ),
                  Positioned(
                    top: 20,
                    right: 10,
                    child: SafeArea(
                      child: GestureDetector(
                        onTap: () => ExtendedNavigator.of(context).pop(),
                        child: Container(
                          height: 50,
                          color: Colors.transparent,
                          width: 50,
                          padding: EdgeInsets.all(8),
                          child: Icon(
                            MdiIcons.close,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  PhotoViewGalleryPageOptions _buildItem(BuildContext context, int index) {
    Map item = galleryItems[index];

    return PhotoViewGalleryPageOptions(
      imageProvider: AssetImage(item['big']),
      initialScale: PhotoViewComputedScale.contained,
      minScale: PhotoViewComputedScale.contained * (0.5 + index / 10),
      maxScale: PhotoViewComputedScale.covered * 1.1,
      heroAttributes: PhotoViewHeroAttributes(tag: item['small']),
    );
  }
}
