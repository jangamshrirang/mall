import 'package:dio/dio.dart';
import 'package:wblue_customer/models/Restaurant/R.CatogeryModel.dart';
import 'package:wblue_customer/models/Restaurant/resturentMenuModel.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';
import 'dart:collection';

import 'package:flutter/foundation.dart';

DioClient _dioClient = DioClient();

class ReservationCartListHelper with ChangeNotifier {
  List _reservationCartList = List();
  bool _isLoading = true;

  UnmodifiableListView get reservationCartList =>
      UnmodifiableListView(_reservationCartList);
  bool get isLoading => _isLoading;

  Future<void> fetchreservationCartList(String reservationId) async {
    setLoading(true);
    _reservationCartList = await fetchreservationCartLists(reservationId);
    setLoading(false);

    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future<List> fetchreservationCartLists(String reservationId) async {
  APIResponse res = await _dioClient
      .privatePost('/restaurant/table/reservation/cart-details');
  if (res.code >= 400) {
    return [];
  }
  print(res.data);
  return res.data;
}
