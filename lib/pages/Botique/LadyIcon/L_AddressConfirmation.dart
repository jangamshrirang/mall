import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';

class L_AddressConfirmation extends StatefulWidget {
  String address;
  L_AddressConfirmation({this.address});
  @override
  _L_AddressConfirmationState createState() => _L_AddressConfirmationState();
}

class _L_AddressConfirmationState extends State<L_AddressConfirmation> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBarWithOutIcon(context, "Confirmation"),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
        
          Container(
            margin: EdgeInsets.only(top: size.height * 0.2,left: size.width*0.01),
            // color: Colors.red,
            height: size.height * 0.25,
            width: size.width,
            child: Column(
               crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                    Icon(
                      Icons.check_circle,
                      color: Colors.green,
                      size: 64.0,
                    ),
                text(
                widget. address=="any"?"Our Professional Taylor Received Your Requrments" :  "Our Professional Taylor Will Come To This Location \n\n${widget.address}",
                    Colors.black,
                    Colors.grey[200],
                    size.height * 0.025,
                    true),
              
              ],
            ),
          ),
          Container(
            width: size.width * 0.4,
            child: RaisedButton(
                child: Text(
                  "Back To Home",
                  style: TextStyle(color: Colors.white),
                ),
                color: Config.primaryColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5.0))),
                onPressed: () {}),
          ),
        ],
      ),
    );
  }

  text(String text, Color color, color2, double fontSize, bool center) {
    var size = MediaQuery.of(context).size;
    return Container(
     
      height: size.height * 0.15,
      padding: EdgeInsets.only(top: size.height * 0.002,left: size.width*0.01),
      margin:
          EdgeInsets.only(left: size.width * 0.03, right: size.width * 0.03),
      width: double.infinity,
      decoration: BoxDecoration(
        color: color2,
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: center == true
          ? Center(
              child: Text(text,
                  style: TextStyle(color: color, fontSize: fontSize)))
          : Text(text, style: TextStyle(color: color, fontSize: fontSize)),
    );
  }
}
