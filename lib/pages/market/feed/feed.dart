import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dash_chat/dash_chat.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:wblue_customer/models/market/store.dart';
import 'package:wblue_customer/providers/market/p_store.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/widgets/w_card_title_button.dart';
import 'package:wblue_customer/widgets/w_items_wrapper.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

import '../../../env/config.dart';

class FeedMarketPage extends StatefulWidget {
  @override
  _FeedMarketPageState createState() => _FeedMarketPageState();
}

class _FeedMarketPageState extends State<FeedMarketPage>
    with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  TabController _tabController;
  final ScrollController controller = ScrollController();

  int page = 1;
  int limit = 5;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
          backgroundColor: Config.bodyColor,
          body: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: SafeArea(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 18.0, vertical: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            AutoSizeText('Feed',
                                style: TextStyle(
                                    fontSize: 40.sp,
                                    fontWeight: FontWeight.w400)),
                            Row(
                              children: [
                                Icon(MdiIcons.storefront,
                                    size: 0.06.sw, color: Config.primaryColor),
                                SizedBox(
                                  width: 20.0,
                                ),
                                Icon(
                                  MdiIcons.dotsVertical,
                                  size: 0.06.sw,
                                  color: Config.primaryColor,
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 0.08.sw,
                        width: 0.5.sw,
                        child: TabBar(
                          controller: _tabController,
                          labelColor: Config.primaryColor,
                          unselectedLabelColor: Colors.grey,
                          indicatorColor: Config.primaryColor,
                          indicatorSize: TabBarIndicatorSize.tab,
                          labelPadding: EdgeInsets.all(0.0),
                          indicatorPadding: EdgeInsets.all(0),
                          tabs: [
                            Tab(
                                icon: AutoSizeText(
                              'Following',
                              style: TextStyle(fontSize: 32.sp),
                            )),
                            Tab(
                                icon: AutoSizeText(
                              'Explore',
                              style: TextStyle(fontSize: 32.sp),
                            )),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    ListView(
                      physics: ClampingScrollPhysics(),
                      padding: EdgeInsets.zero,
                      children: [],
                    ),
                    _explore()
                  ],
                ),
              ),
            ],
          )),
    );
  }

  FlareControls controls = FlareControls();

  Widget loading(String name) {
    return FlareActor(
      'assets/flares/w-loading.flr',
      animation: name,
      fit: BoxFit.fitHeight,
    );
  }

  header() {
    return CustomHeader(
      builder: (context, mode) {
        Widget body;
        if (mode == RefreshStatus.idle) {
          body = loading('open');
        } else if (mode == RefreshStatus.refreshing) {
          body = loading('loop');
        } else if (mode == RefreshStatus.canRefresh) {
          body = loading('open');
        } else if (mode == RefreshStatus.completed) {
          body = loading('close');
        }
        return Container(
          height: 60,
          width: 60,
          child: body,
        );
      },
    );
  }

  footer() {
    return CustomFooter(
      builder: (BuildContext context, LoadStatus mode) {
        Widget body;
        if (mode == LoadStatus.idle) {
          body = loading('open');
        } else if (mode == LoadStatus.loading) {
          body = loading('loop');
        } else if (mode == LoadStatus.failed) {
          body = loading('normal');
        } else if (mode == LoadStatus.canLoading) {
          body = loading('loop');
        } else {
          body = AutoSizeText(
            'No more data',
            style: TextStyle(fontSize: 28.sp),
          );
        }
        print(mode);

        return Container(
          height: 60,
          width: 60,
          child: Center(child: body),
        );
      },
    );
  }

  Widget _explore() {
    return Consumer<PStore>(
      builder: (__, store, _) {
        return SmartRefresher(
            enablePullDown: true,
            enablePullUp: store.explore.length <= 0 ? false : true,
            header: header(),
            footer: footer(),
            onRefresh: () async {
              page = 1;
              store.fetchExploreStores(
                  limit: limit, page: page, isRefresh: true);
            },
            onLoading: () async {
              page++;
              store.fetchExploreStores(limit: limit, page: page);
            },
            controller: store.refreshController,
            child: store.explore.length > 0
                ? WItemList(
                    crossAxisCellCount: 4,
                    crossAxisCount: 4,
                    mainAxisSpacing: 8.0,
                    widget: (store, index) {
                      StoreListModel storeModel =
                          StoreListModel.fromJson(store);

                      return WCardTitleButton(
                          onTap: () async {
                            // bool isGranted = await context.navigator
                            //     .canNavigate('/chats/room');

                            // if (isGranted)
                            //   ExtendedNavigator.of(context).push('/chats/room',
                            //       arguments: MessageRoomPageArguments(
                            //           user: ChatUser(
                            //               name: storeModel.name,
                            //               uid: storeModel.hashId,
                            //               avatar:
                            //                   storeModel.profilePhotoFull)));
                          },
                          titleWidget: Expanded(
                            child: Row(
                              children: [
                                Container(
                                  width: 0.12.sw,
                                  height: 0.12.sw,
                                  child: CachedNetworkImage(
                                      imageUrl: storeModel.profilePhotoFull,
                                      imageBuilder: (context, imageProvider) =>
                                          Container(
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(30),
                                              image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error, color: Colors.red)),
                                ),
                                SizedBox(width: 5),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        child: AutoSizeText(
                                          '${storeModel.name}',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 28.sp,
                                          ),
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          ConstrainedBox(
                                            constraints: new BoxConstraints(
                                              maxWidth: 50.0,
                                            ),
                                            child: AutoSizeText(
                                              '0',
                                              style: TextStyle(
                                                  fontSize: 26.sp,
                                                  color: Colors.black54),
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ),
                                          AutoSizeText(
                                            ' Followers',
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: 26.sp,
                                                color: Colors.black54),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          linkWidget: Row(
                            children: [
                              SizedBox(
                                height: 0.04.sh,
                                width: 0.20.sw,
                                child: WRoundedButton(
                                  btnColor: Config.primaryColor,
                                  labelColor: Colors.white,
                                  onCustomButtonPressed: () {},
                                  child: AutoSizeText('Follow',
                                      style: TextStyle(fontSize: 26.sp)),
                                ),
                              )
                            ],
                          ),
                          bodyWidget: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CachedNetworkImage(
                                imageUrl: storeModel.coverPhotoFull ?? '',
                                imageBuilder: (context, imageProvider) =>
                                    ClipRRect(
                                  borderRadius: BorderRadius.circular(0.015.sw),
                                  child: Container(
                                    height: 0.26.sh,
                                    child: Image.network(
                                      storeModel.coverPhotoFull,
                                      fit: BoxFit.cover,
                                      height: double.infinity,
                                      width: double.infinity,
                                      alignment: Alignment.center,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) => Container(
                                  height: 0.26.sh,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      AutoSizeText(
                                        'Image Not Found',
                                        style: TextStyle(
                                            fontSize: 28.sp,
                                            fontWeight: FontWeight.w300),
                                      ),
                                      SizedBox(
                                        height: 0.02.sh,
                                      ),
                                      Icon(
                                        MdiIcons.alertCircle,
                                        color: Colors.red,
                                        size: 0.08.sh,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ));
                    },
                    items: store.explore,
                  )
                : Row(
                    children: [
                      Expanded(
                        child: Center(
                          child: AutoSizeText('No results found'),
                        ),
                      ),
                    ],
                  ));
      },
    );
  }
}
