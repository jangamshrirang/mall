import 'package:flutter/cupertino.dart';

class PLoading with ChangeNotifier {
  String _animation;
  String get animation => _animation;

  void setAnimation(String animation) {
    _animation = animation;
    notifyListeners();
  }
}
