import 'package:flutter/foundation.dart';
import 'package:wblue_customer/models/market/store.dart';
import 'package:wblue_customer/models/product.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

class PViewProduct extends ChangeNotifier {
  DioClient _dioClient = DioClient();

  Map _selectedProduct;
  Map _selectedOtherProduct;
  SingleProductModel _singleProduct;
  StoreModel _store;

  String _hashId;

  StoreModel get store => _store;
  Map get selectedProduct => _selectedProduct;
  SingleProductModel get singleProduct => _singleProduct;
  Map get selectedOtherProduct => _selectedOtherProduct;

  Future<void> fetchProduct(String hashid) async {
    Map<String, dynamic> body = {'product_hashid': hashid};

    APIResponse res =
        await _dioClient.publicPost('/product/show-in-open-api', data: body);

    if (res.code != 200) {
      return;
    }

    _singleProduct = SingleProductModel.fromJson(res.data);

    if (_singleProduct != null) {
      fetchStore(_singleProduct.storeId);

      if (_singleProduct.sku != null)
        _singleProduct.quickDetails
            .add(QuickDetail(label: 'SKU', value: _singleProduct.sku));

      if (int.parse(_singleProduct.dimensions.l) != 0 &&
          int.parse(_singleProduct.dimensions.w) != 0 &&
          int.parse(_singleProduct.dimensions.h) != 0)
        _singleProduct.quickDetails.add(QuickDetail(
            label: 'Dimensions',
            value:
                'Length=${_singleProduct.dimensions.l}, Width=${_singleProduct.dimensions.w}, Height=${_singleProduct.dimensions.h}'));
    }

    notifyListeners();
  }

  Future<void> fetchStore(String hashid) async {
    Map<String, dynamic> body = {'store_hash_id': hashid};
    APIResponse res = await _dioClient.publicPost('/store-show', data: body);

    if (res.code != 200) {
      return;
    }

    _store = StoreModel.fromJson(res.data);

    notifyListeners();
  }

  //remove
  void setProduct(Map product) {
    _selectedProduct = product;
  }

  //remove
  void setOtherProduct(Map product) {
    _selectedOtherProduct = product;
  }
}
