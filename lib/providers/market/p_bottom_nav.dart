import 'dart:ffi';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/foundation.dart';
import 'package:wblue_customer/routes/route_guards.dart';
import 'package:wblue_customer/services/helper.dart';

class PBottomNavigation with ChangeNotifier {
  int _tab = 0;
  int get tab => _tab;

  void changeTab(tab) {
    _tab = tab;

    notifyListeners();
  }
}
