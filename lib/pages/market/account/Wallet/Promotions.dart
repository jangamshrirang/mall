import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';

final List promotions = [
  {
    'image':
        "https://qa.dohabank.com/wp-content/uploads/sites/12/Al-Dana-6th-Draw-Winners-En.jpg",
    'text':
        "Balloon Blast: Cash in from Aug 29 -Sep8 for a chance to win up to Qr 9,000 CREDITS",
    'validDate': "01/05/2020"
  },
  {
    'image': "https://i.ytimg.com/vi/pNpbmjSMUwI/hqdefault.jpg",
    'text':
        "Win up to Qr 5,000 Wallet Credits using your Wmall Wallet or Wcash from Aug",
    'validDate': "02/10/2020"
  },
  {
    'image':
        "https://www.ibs-uae.com/innovate/wp-content/uploads/2015/04/MARK-1200x629.jpg",
    'text':
        "Learn how to activ,cas in,and pay with your Wmall Wallet by cliking here",
    'validDate': "03/08/2020"
  }
];

class Pramotions extends StatefulWidget {
  @override
  _PramotionsState createState() => _PramotionsState();
}

class _PramotionsState extends State<Pramotions> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.25,
      width: size.width * 4,
      child: ListView.separated(
          separatorBuilder: (BuildContext context, int index) {
            return SizedBox(
              width: 10,
            );
          },
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: promotions.length,
          itemBuilder: (BuildContext ctxt, int index) {
            return Container(
                width: size.width * 0.8,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    image: DecorationImage(
                      image: NetworkImage(promotions[index]['image']),
                      fit: BoxFit.cover,
                    )),
                child: Container(
                    //text container
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(7),
                          bottomRight: Radius.circular(7)),
                    ),
                    margin: EdgeInsets.only(
                      top: size.height * 0.15,
                    ),
                    padding: EdgeInsets.only(
                        left: size.width * 0.01,
                        right: size.width * 0.01,
                        top: size.height * 0.01),
                    height: size.height * 0.06,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          promotions[index]['text'],
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: size.height * 0.025),
                        ),
                        SizedBox(height: 8),
                        Text("Valid till:" + promotions[index]['validDate'],
                            style: TextStyle(
                              color: Colors.black38,
                            ))
                      ],
                    )));
          }),
    );
  }
}
