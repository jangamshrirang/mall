import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/widgets/w_avatar.dart';
import 'package:wblue_customer/widgets/w_drawer.dart';

class WScaffoldWidget extends StatefulWidget {
  final String title;
  final Widget body;
  final List<Widget> bottomChildren;
  final bool noScroll;

  WScaffoldWidget({
    @required this.title,
    @required this.body,
    this.bottomChildren,
    this.noScroll: false,
  });

  @override
  _WScaffoldWidgetState createState() => _WScaffoldWidgetState();
}

class _WScaffoldWidgetState extends State<WScaffoldWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.notifications),
            onPressed: () {},
          ),
          InkWell(
            onTap: () {
              ExtendedNavigator.of(context).root.push('/main/agent/profile');
            },
            child: WAvatarWidget(
              imageUrl: 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?f=y',
            ),
          ),
        ],
      ),
      drawer: WDrawerWidget('home'),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: widget.noScroll
            ? widget.body
            : SingleChildScrollView(
                child: widget.body,
              ),
      ),
      bottomNavigationBar: widget.bottomChildren == null
          ? SizedBox()
          : Container(
              color: Config.primaryColor,
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: Wrap(
                children: widget.bottomChildren,
              ),
            ),
    );
  }
}
