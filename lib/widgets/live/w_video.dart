import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WLiveVideoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(color: Color(0xff1C1A1A), image: DecorationImage(image: AssetImage('assets/images/samples/ads.jpg'), fit: BoxFit.cover)),
    );
  }
}
