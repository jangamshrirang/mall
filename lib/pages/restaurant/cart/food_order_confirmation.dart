import 'package:auto_route/auto_route.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/providers/order/p_order.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/extensions/w_money_formatter.dart';
import 'package:wblue_customer/providers/socket/orderStatus.dart';
import 'package:wblue_customer/services/helper.dart';

class RestaurantOrderConfirmation extends StatefulWidget {
  @override
  _RestaurantOrderConfirmationState createState() =>
      _RestaurantOrderConfirmationState();
}

class _RestaurantOrderConfirmationState
    extends State<RestaurantOrderConfirmation> {
  @override
  void initState() {
    super.initState();
    Future.microtask(() => context.read<PRestaurantOrders>().oneOrder());
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PRestaurantFoodCart>(
      builder: (context, order, child) => Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(0.07.sh),
          child: AppBar(
            leading: IconButton(
              onPressed: () =>
                  ExtendedNavigator.of(context).popUntilPath('/restaurants'),
              icon: Icon(MdiIcons.close),
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              iconSize: 64.sp,
            ),
            backgroundColor: Config.primaryColor,
            automaticallyImplyLeading: false,
            title: Column(
              children: [
                Text(
                  'Order Confirm',
                  style: TextStyle(fontSize: 42.sp),
                ),
                order.restaurantCart.food.length > 0
                    ? Column(
                        children: [
                          SizedBox(height: 0.01.sh),
                          Text('${order.restName}',
                              style: TextStyle(fontSize: 32.sp)),
                        ],
                      )
                    : SizedBox(),
              ],
            ),
          ),
        ),
        body: Container(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            children: [
              ///this below code will change according to orde status
              Consumer<OrderStatusScocket>(builder: (_, orderStatus, __) {
                print("hitting the scoket consumer in order confirm screen");

                if (orderStatus.isUpdated &&
                    order.orderConfirmModel.orderHashId == //order hash id
                        orderStatus.updatedstatusData['order_hashid'] &&
                    Helper.loggedInUserHashid == //user hash id
                        orderStatus.updatedstatusData['user_hashid']) {
                  print(orderStatus.updatedstatusData['status']);
                  context
                      .read<OrderStatusScocket>()
                      .setOrderStatusUpdated(false);
                  return Column(
                    children: [
                      Container(
                        width: 0.5.sw,
                        height: 0.5.sw,
                        child: FlareActor(
                          orderStatus.updatedstatusData['status'] == "Preparing"
                              ? "assets/flares/restaurant/cooking.flr"
                              : orderStatus.updatedstatusData['status'] ==
                                      "Delivering"
                                  ? "assets/flares/restaurant/delivery-man.flr"
                                  : "assets/flares/restaurant/glass-hour.flr",
                          animation: "normal-loop",
                        ),
                      ),
                      SizedBox(height: 0.1.sw),
                      //here im changing status when it is updated
                      Text(
                        orderStatus.isUpdated
                            ? orderStatus.updatedstatusData['status']
                            : 'Sending your order to',
                        style:
                            TextStyle(fontSize: 32.sp, color: Colors.black54),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        '${order.orderConfirmModel.store.name}',
                        style: TextStyle(
                            fontSize: 50.sp,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  );
                } else {
                  print(
                      "socket user hashId is not matching to local user hashId");
                  print(
                      "socket data : ${orderStatus.updatedstatusData} && local user hashID: ${Helper.loggedInUserHashid}");
                  return Column(
                    children: [
                      Container(
                        width: 0.5.sw,
                        height: 0.5.sw,
                        child: FlareActor(
                          "assets/flares/restaurant/glass-hour.flr",
                          animation: "normal-loop",
                        ),
                      ),
                      SizedBox(height: 0.1.sw),
                      Text(
                        'Sending your order to',
                        style:
                            TextStyle(fontSize: 32.sp, color: Colors.black54),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        '${order.orderConfirmModel.store.name}',
                        style: TextStyle(
                            fontSize: 50.sp,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  );
                }
              }),
              ////////////////////////////////////
              SizedBox(height: 0.05.sh),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black12),
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    )),
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${order.orderConfirmModel.user.currentAddress.area}',
                      style: TextStyle(
                          fontSize: 42.sp, fontWeight: FontWeight.w500),
                    ),
                    Divider(),
                    Text(
                        '${order.orderConfirmModel.user.currentAddress.street}',
                        style: TextStyle(
                            fontSize: 32.sp,
                            fontWeight: FontWeight.w500,
                            color: Colors.black45)),
                    Text('Mobile: ${order.orderConfirmModel.contactNumber}',
                        style: TextStyle(
                            fontSize: 32.sp,
                            fontWeight: FontWeight.w500,
                            color: Colors.black45)),
                    order.orderConfirmModel.landline != null
                        ? Text('Landline:  ${order.orderConfirmModel.landline}',
                            style: TextStyle(
                                fontSize: 32.sp,
                                fontWeight: FontWeight.w500,
                                color: Colors.black45))
                        : SizedBox(),
                  ],
                ),
              ),
              SizedBox(height: 0.05.sh),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black12),
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    )),
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${order.orderConfirmModel.store.name}',
                      style: TextStyle(
                          fontSize: 42.sp, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(height: 0.02.sh),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: List.generate(
                          order.orderConfirmModel.foodItems.length,
                          (index) => Text(
                              '${order.orderConfirmModel.foodItems[index].quantity} x ${order.orderConfirmModel.foodItems[index].name}',
                              style: TextStyle(
                                  fontSize: 32.sp,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black45)),
                        )),
                    Divider(),
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              Text('${order.orderConfirmModel.orderNumber}',
                                  style: TextStyle(
                                      fontSize: 30.sp,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black54)),
                              SizedBox(height: 0.005.sh),
                              Text('Order number',
                                  style: TextStyle(
                                      fontSize: 28.sp,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black45)),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              Text(
                                  '${order.orderConfirmModel.totalAmount.moneyFromNum()}',
                                  style: TextStyle(
                                      fontSize: 30.sp,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black54)),
                              SizedBox(height: 0.005.sh),
                              Text('Order amount',
                                  style: TextStyle(
                                      fontSize: 28.sp,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black45)),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              Text('${order.orderConfirmModel.paymentMethod}',
                                  style: TextStyle(
                                      fontSize: 30.sp,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black54)),
                              SizedBox(height: 0.005.sh),
                              Text('Payment',
                                  style: TextStyle(
                                      fontSize: 28.sp,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black45)),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
