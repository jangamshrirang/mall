import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();

class R_AddToCartHelper with ChangeNotifier {
  var _restaurants;
  bool _isLoading = true;

  UnmodifiableListView get restaurantsAddToCart =>
      UnmodifiableListView(_restaurants);
  bool get isLoading => _isLoading;

  Future<void> fetchRestaurantsAddToCart(var foodId, quantity, specialRequest,
      dineIn, tableId, reservationHashId) async {
    setLoading(true);
    await fetchRestaurantAddToCart(
        foodId, quantity, specialRequest, dineIn, tableId, reservationHashId);
    setLoading(false);
    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future<void> fetchRestaurantAddToCart(var foodId, quantity, specialRequest,
    dineIn, tableId, reservationHashId) async {
  Map<String, dynamic> _body = {
    'food_id': foodId,
    'quantity': quantity == null || quantity == "" ? 1 : quantity,
    'order_type': dineIn,
    'has_booking': tableId,
    'reservation_hash_id': reservationHashId
  };

  try {
    APIResponse res =
        await _dioClient.privatePost('/restaurant/cart/add', data: _body);

    print("Add to cart result:" + res.message);
    print("reservation HashID: " + reservationHashId);

    if (res.code >= 400) {
      print('fetchRestaurantAddToCart failed: ${res.code}');
      return;
    }
  } catch (r) {
    print('fetchRestaurantAddToCart Error: $r');
  }
}
