import 'dart:typed_data';

import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_map_polyline/google_map_polyline.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wblue_customer/pages/market/order/track-order.dart';
import 'package:wblue_customer/widgets/w_button.dart';
import 'package:wblue_customer/widgets/w_qr.dart';
import 'dart:ui' as ui;

class MarketSuccessPurchasedPage extends StatefulWidget {
  @override
  _MarketSuccessPurchasedPageState createState() =>
      _MarketSuccessPurchasedPageState();
}

class _MarketSuccessPurchasedPageState
    extends State<MarketSuccessPurchasedPage> {
  List<LatLng> routeCoords;
  GoogleMapPolyline googleMapPolyline =
      new GoogleMapPolyline(apiKey: "AIzaSyCpZybhi4ZzUBXImzkKtaggzrOSsfcH8p4");
  BitmapDescriptor pinLocationIcon;
  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AutoSizeText('Thank you for purchasing with us!',
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500)),
                SizedBox(height: 5.0),
                AutoSizeText.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                          text:
                              'you can track you order in My Account settings or click '),
                      TextSpan(
                          text: 'here',
                          style: TextStyle(color: Colors.blue),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              setState(() {
                                getaddressPoints();
                              });
                            }),
                    ],
                  ),
                  textAlign: TextAlign.center,
                ),
                FractionallySizedBox(
                  widthFactor: 0.5,
                  child: WButtonWidget(
                    title: 'Qr Code',
                    onPressed: () {
                      setState(() {
                        _showBarcode();
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }

  getaddressPoints() async {
    routeCoords = await googleMapPolyline.getCoordinatesWithLocation(
        origin: LatLng(25.254246, 51.536136),
        destination: LatLng(25.246684, 51.533330),
        mode: RouteMode.driving);

    final Uint8List markerIcon =
        await getBytesFromAsset('assets/images/van.png',80 );
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OrderTrackPage(
                  cordinates: routeCoords,
                  pinLocationIcon: BitmapDescriptor.fromBytes(markerIcon),
                )));
  }

  void _showBarcode() {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            return Container(
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  )),
              child: Wrap(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Transaction Code',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      IconButton(
                        icon: Icon(Icons.close),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                  JQRWidget(
                    size: MediaQuery.of(context).size.width * 0.8,
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
