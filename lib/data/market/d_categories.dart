class DCategoryData {
  static List categories = [
    {
      'name': 'Load, Bills & eCoupon',
      'image': 'load-bills-coupon',
      'route': '/market-categories'
    },
    {'name': 'WMall', 'image': 'wmall', 'route': null},
    {'name': 'WLive', 'image': 'wlive', 'route': null},
    {
      'name': 'Categories',
      'image': 'categories',
      'route': '/market-categories'
    },
    {'name': 'WGlobal', 'image': 'wglobal', 'route': null},
    {'name': 'Coins & Rewards', 'image': 'coins-rewards', 'route': null},
  ];
}
