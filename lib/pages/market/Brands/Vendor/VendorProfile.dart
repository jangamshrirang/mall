import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/widgets/w_rating.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'AllProductRarings.dart';
import 'AllSellerRating_Raviews.dart';

class VendorProfile extends StatefulWidget {
  @override
  _VendorProfileState createState() => _VendorProfileState();
}

class _VendorProfileState extends State<VendorProfile> {
  List<dynamic> sellerRatingHours = [
    13,
    18,
    6,
  ];
  List<dynamic> productRatingHours = [1, 2, 3];
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBarWithOutIcon(context, "Profile"),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              vendortitle(Colors.white),
              Divider(
                thickness: 5,
              ),
              //2nd bloc of Screen u can see in UI
              rowText("Main Catogery", Colors.black45,
                  "Mens Shoes and Clothing", Colors.black),
              rowText("Chat Response Time", Colors.black45, "Active in 5:min",
                  Colors.green),
              rowText(
                  "Chat Response Rate", Colors.black45, "98.21%", Colors.green),
              rowText("Shipped on Time", Colors.black45, "100%", Colors.green),
              Divider(
                thickness: 5,
              ),
              //rating bloc eg:positive.netural....
              ratingsBloc(),
              Divider(
                color: Colors.white,
                thickness: 5,
              ),
              //seller rating text
              sellerAndProductRatingText(
                  "Seller Ratings and Reviews (284)",
                  Colors.black,
                  FontWeight.bold,
                  size.height * 0.025,
                  size.width * 0.04,
                  1),
              //seller list tile
              sellerRatingtiles(sellerRatingHours),
              //product rating
              sellerAndProductRatingText(
                  "Product Rating and Reviews (3377)",
                  Colors.black,
                  FontWeight.bold,
                  size.height * 0.025,
                  size.width * 0.04,
                  2),
              Divider(
                color: Colors.white,
                thickness: 5,
              ),
              //product rating list tiles
              producReviwesListTile(productRatingHours)
            ],
          ),
        ),
      ),
    );
  }

  //vendor bloc
  vendortitle(Color color) {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: size.height * 0.03),
          color: color,
          width: size.width * 0.9,
          height: size.height * 0.12,
          child: Row(
            children: [
              Container(
                color: Colors.transparent,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.asset(
                    ('assets/images/users/1-boy.png'),
                    width: 60,
                  ),
                ),
              ),
              SizedBox(
                width: 4,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Spacer(),
                  Container(
                    margin: EdgeInsets.only(
                      top: 5,
                    ),
                    child: Text(
                      'Mandy Shop',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: size.height * 0.03,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Spacer(),
                  Row(
                    children: [
                      Icon(
                        Icons.favorite_border,
                        color: Colors.black,
                        size: 15,
                      ),
                      Text(
                        '1.1 K',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: size.height * 0.02,
                            fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                  Spacer(),
                  Text(
                    '95% Positive seller Ratings',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: size.height * 0.02,
                        fontWeight: FontWeight.normal),
                  ),
                  Spacer(),
                  Spacer(),
                ],
              ),
            ],
          ),
        ),
        Container(
            margin: EdgeInsets.only(left: size.height * 0.03),
            width: size.width * 0.9,
            height: size.height * 0.06,
            color: Colors.white,
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    textContainer(
                        "Time on Wmall",
                        Colors.black45,
                        FontWeight.normal,
                        size.height * 0.018,
                        size.width * 0.15),
                    textContainer("1 + years", Colors.black, FontWeight.bold,
                        size.height * 0.02, size.width * 0.18),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    textContainer(
                        "Seller Size",
                        Colors.black45,
                        FontWeight.normal,
                        size.height * 0.018,
                        size.width * 0.175),
                    Container(
                        height: size.height * 0.025,
                        margin: EdgeInsets.only(left: size.width * 0.2),
                        child: Image.network(
                            "https://icons-for-free.com/iconfiles/png/512/connection+mobile+signal+signal+power+icon-1320191166696755743.png",)),
                  ],
                )
              ],
            ))
      ],
    );
  }

//ratings Bloc
  ratingsBloc() {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: 10),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              textContainer2(
                  "88%",
                  Colors.black,
                  FontWeight.normal,
                  size.height * 0.05,
                  size.width * 0.0,
                  size.height * 0.05,
                  TextAlign.center),
              textContainer2(
                  "Positive Seller Ratings",
                  Colors.black,
                  FontWeight.normal,
                  size.height * 0.023,
                  size.width * 0.0,
                  size.height * 0.022,
                  TextAlign.center),
              textContainer2(
                  "7491 Customer Reviews",
                  Colors.black54,
                  FontWeight.normal,
                  size.height * 0.018,
                  size.width * 0.0,
                  size.height * 0.018,
                  TextAlign.center),
            ],
          ),
          Container(height: 60, child: VerticalDivider(color: Colors.black45)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ratingBarWithText("Positive", 0.8, 999),
              ratingBarWithText("Netural", 0.1, 549),
              ratingBarWithText("Negative", 0.1, 555),
            ],
          ),
        ],
      ),
    );
  }

  textContainer(String text, Color color, FontWeight fontWeight,
      double fontsize, double margin) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.2,
      height: size.height * 0.02,
      //color: Colors.yellow,
      margin: EdgeInsets.only(left: margin, top: 5),
      child: Text(
        text,
        style:
            TextStyle(color: color, fontWeight: fontWeight, fontSize: fontsize),
      ),
    );
  }

  sellerAndProductRatingText(String text, Color color, FontWeight fontWeight,
      double fontsize, double margin, int number) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => number == 1
                    ? AllSellerRatingRevies(
                        sellereRevies: sellerRatingHours,
                      )
                    : AllProductRatings(
                        allProductReviews: productRatingHours,
                      )));
      },
      child: Row(
        children: [
          Container(
            width: size.width * 0.9,
            height: size.height * 0.03,
            //color: Colors.yellow,
            margin: EdgeInsets.only(left: margin, top: 10),
            child: Text(
              text,
              style: TextStyle(
                  color: color, fontWeight: fontWeight, fontSize: fontsize),
            ),
          ),
          Text(
            "...",
            style: TextStyle(
                color: color, fontWeight: fontWeight, fontSize: fontsize),
          )
        ],
      ),
    );
  }

  textContainer2(
      String text,
      Color color,
      FontWeight fontWeight,
      double fontsize,
      double margin,
      double containerHeight,
      TextAlign textAlign) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.45,
      height: containerHeight,
      //color: Colors.yellow,
      margin: EdgeInsets.only(left: margin, top: 5),
      child: Text(
        text,
        textAlign: textAlign,
        style:
            TextStyle(color: color, fontWeight: fontWeight, fontSize: fontsize),
      ),
    );
  }

  rowText(String text1, Color color1, String text2, Color color) {
    var size = MediaQuery.of(context).size;
    return Row(
      children: [
        Container(
          margin: EdgeInsets.only(left: size.width * 0.1, bottom: 10),
          //color: Colors.blue,
          width: size.width * 0.4,
          child: Text(
            text1,
            style: TextStyle(color: color1, fontSize: size.height * 0.018),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 9),
          //color: Colors.yellow,
          width: size.width * 0.45,
          child: Text(
            text2,
            style: TextStyle(color: color, fontSize: size.height * 0.019),
          ),
        ),
      ],
    );
  }

  ratingBarWithText(String text, double value, int count) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      child: Row(
        children: [
          Container(
            width: size.width * 0.12,
            child: Text(
              text,
              style: TextStyle(color: Colors.black45, fontSize: 12),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(0.0),
            child: new LinearPercentIndicator(
              width: size.width * 0.3,
              animation: true,
              animationDuration: 3000,
              lineHeight: 8.0,
              //leading: new Text(""),
              // trailing: new Text(""),
              percent: value,
              center: Text(""),
              linearStrokeCap: LinearStrokeCap.butt,
              progressColor: Colors.amber,
            ),
          ),
          Text(
            "$count",
            style: TextStyle(color: Colors.black45, fontSize: 10),
          ),
        ],
      ),
    );
  }

  sellerRatingtiles(List hours) {
    return Wrap(
        children: List.generate(hours.length, (index) {
      return Column(
        children: [
          ListTile(
            // leading: Image.network(image, height: 30, color: Colors.amber),
            title: Row(
              children: [
                Text(
                  "Wmall Customer - " + "${hours[index]}" + " hours ago",
                  style: TextStyle(color: Colors.black45, fontSize: 15),
                ),
                Spacer(),
                Column(
                  children: [
                    Image.asset("assets/images/happyEmoji.png", height: 20),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Icon(
                          Icons.thumb_up,
                          size: 12,
                          color: Colors.black26,
                        ),
                        Text(
                          "  2",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black26,
                          ),
                        )
                      ],
                    ),
                  ],
                )
              ],
            ),
            onTap: () {},
          ),
          Divider()
        ],
      );
    }));
  }

  producReviwesListTile(List products) {
    var size = MediaQuery.of(context).size;
    return Wrap(
        children: List.generate(products.length, (index) {
      return Column(
        children: [
          productRatingtileImage(),
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: size.width * 0.05, top: 5),
                child: Text(
                  "Myrna J -" + "${products[index]}" + " Weeks ago",
                  style: TextStyle(color: Colors.black45, fontSize: 13),
                ),
              ),
              Spacer(),
              WRatingsWidget(3.5, size: 16.0, compact: true),
              SizedBox(
                width: 8,
              ),
            ],
          ),
          SizedBox(
            height: 6,
          ),
          Container(
              width: size.width * 0.9,
              child: Text(
                "adidas_alphabounce rc 2.0 mens running shoe270 ...are one of the best shoe i ever had and im giving 5 Star for this product",
                style: TextStyle(fontSize: 16),
              )),
          SizedBox(
            height: 10,
          ),
          Container(
            height: size.height * 0.1,
            width: size.width * 0.8,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                Image.network(
                  ('https://images-na.ssl-images-amazon.com/images/I/41Leu3gBUFL._UX395_.jpg'),
                  width: 80,
                ),
                Image.network(
                  ('https://images-na.ssl-images-amazon.com/images/I/41Leu3gBUFL._UX395_.jpg'),
                  width: 80,
                ),
              ],
            ),
          ),
          Divider(),
        ],
      );
    }));
  }

  productRatingtileImage() {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: size.height * 0.01),
          color: Colors.grey[200],
          width: size.width * 0.93,
          height: size.height * 0.1,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: size.height * 0.01),
                color: Colors.transparent,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    ('https://images-na.ssl-images-amazon.com/images/I/61Nh8BmRvoL.__AC_SY395_QL70_ML2_.jpg'),
                    width: 60,
                  ),
                ),
              ),
              SizedBox(
                width: 4,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Spacer(),
                  Container(
                    width: size.width * 0.6,
                    margin: EdgeInsets.only(top: 5, bottom: 5),
                    child: Text(
                      'adidas_alphabounce rc 2.0 mens running shoe270 ',
                      maxLines: 2,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: size.height * 0.023,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    'Color family: Purple,' + " Size:EU:36",
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: size.height * 0.018,
                        fontWeight: FontWeight.normal),
                  ),
                  Spacer(),
                  Spacer(),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
