//import 'package:flutter/cupertino.dart';
//import 'package:get_it/get_it.dart';
//import 'package:wblue_customer/pages/404.dart';
//import 'package:wblue_customer/pages/auth/login.dart';
//import 'package:wblue_customer/pages/auth/registration.dart';
//import 'package:wblue_customer/pages/booking/book_table.dart';
//import 'package:wblue_customer/pages/booking/booking_confirmation.dart';
//import 'package:wblue_customer/pages/cinema/cinema_confirmation.dart';
//import 'package:wblue_customer/pages/cinema/cinema_payment.dart';
//import 'package:wblue_customer/pages/cinema/cinemas.dart';
//import 'package:wblue_customer/pages/cinema/fullscreen_photo.dart';
//import 'package:wblue_customer/pages/cinema/movie_details.dart';
//import 'package:wblue_customer/pages/cinema/select_seat.dart';
//import 'package:wblue_customer/pages/cinema/single_cinema.dart';
//import 'package:wblue_customer/pages/cinema/ticket_details.dart';
//import 'package:wblue_customer/pages/hotel/hotel_confirmation.dart';
//import 'package:wblue_customer/pages/hotel/hotel_payment.dart';
//import 'package:wblue_customer/pages/hotel/hotels.dart';
//import 'package:wblue_customer/pages/hotel/review_booking.dart';
//import 'package:wblue_customer/pages/hotel/single_hotel.dart';
//import 'package:wblue_customer/pages/.dart';
//import 'package:wblue_customer/pages/reservation/reservation_details.dart';
//import 'package:wblue_customer/pages/reservation/reservation_timeline.dart';
//import 'package:wblue_customer/pages/reservation/reservations.dart';
//import 'package:wblue_customer/pages/restaurant/cart/cart.dart';
//import 'package:wblue_customer/pages/restaurant/cart/deliver.dart';
//import 'package:wblue_customer/pages/restaurant/cart/delivery_confirmation.dart';
//import 'package:wblue_customer/pages/restaurant/cart/pickup.dart';
//import 'package:wblue_customer/pages/restaurant/cart/pickup_confirmation.dart';
//import 'package:wblue_customer/pages/restaurant/menu.dart';
//import 'package:wblue_customer/pages/restaurant/restaurants.dart';
//import 'package:wblue_customer/pages/restaurant/single_restaurant.dart';
//
//GetIt locator = GetIt.instance;
//
//void setupLocator() {
//  locator.registerLazySingleton(() => NavigationService());
//}
//
//class Routes {
//  static Route<dynamic> generateRoute(RouteSettings settings) {
//    final args = settings.arguments;
//    switch (settings.name) {
//      case '/':
//        return _pageBuilder(LandingPage());
//      case '/register':
//        return _pageBuilder(RegistrationPage());
//      case '/login':
//        return _pageBuilder(LoginPage());
//      case '/wmall_logout':
//        return _pageBuilder(LoginPage());
//
//      case '/browse-restaurants':
//        return _pageBuilder(RestaurantsPage());
//      case '/single-restaurant':
//        return _pageBuilder(SingleRestaurantPage());
//      case '/group-menu':
//        return _pageBuilder(MenuPage());
//
//      case '/browse-hotels':
//        return _pageBuilder(HotelsPage());
//      case '/single-hotel':
//        return _pageBuilder(SingleHotelPage());
//      case '/hotel-review-booking':
//        return _pageBuilder(HotelReviewBookingPage());
//      case '/hotel-payment':
//        return _pageBuilder(HotelPaymentPage());
//      case '/hotel-confirmation':
//        return _pageBuilder(HotelConfirmationPage());
//
//      case '/browse-cinemas':
//        return _pageBuilder(CinemasPage());
//      case '/single-cinema':
//        return _pageBuilder(SingleCinemaPage());
//      case '/movie-details':
//        return _pageBuilder(MovieDetailsPage());
//      case '/movie-fullscreen-image':
//        return _pageBuilder(FullScreenPhotoPage());
//      case '/select-cinema-seat':
//        return _pageBuilder(SelectCinemaSeatPage());
//      case '/ticket-details':
//        return _pageBuilder(TicketDetailsPage());
//      case '/cinema-payment':
//        return _pageBuilder(CinemaPaymentPage());
//      case '/cinema-confirmation':
//        return _pageBuilder(CinemaConfirmationPage());
//
//      case '/book-table':
//        return _pageBuilder(BookTablePage());
//
//      case '/reservations':
//        return _pageBuilder(ReservationsPage());
//      case '/reservation-details':
//        return _pageBuilder(ReservationDetailsPage());
//      case '/reservation-timeline':
//        return _pageBuilder(ReservationTimelinePage());
//
//      case '/cart':
//        return _pageBuilder(RestaurantCartPage());
//      case '/pickup':
//        return _pageBuilder(PickupPage());
//      case '/deliver':
//        return _pageBuilder(DeliverPage());
//      case '/pickup-confirmation':
//        return _pageBuilder(PickupConfirmationPage());
//      case '/delivery-confirmation':
//        return _pageBuilder(DeliveryConfirmationPage());
//
//      case '/booking-confirmation':
//        return _pageBuilder(BookingConfirmationPage());
//
//      case '/home-products':
//        return _pageBuilder(MarketPage());
//      case '/market-cart':
//        return _pageBuilder(MarketCartPage());
//      case '/home-category':
//        return _pageBuilder(CategoryPage());
//      case '/products':
//        if (args != null) {
//          Map _args = args;
//
//          String title = _args['title'];
//          String keyword = _args['keyword'];
//
//          return _pageBuilder(ProductListPage(title: title ?? '', searchKeyword: keyword ?? ''));
//        }
//        return _pageBuilder(PageNotFound());
//      case '/product':
//        return _pageBuilder(ProductInfo());
//      default:
//        return _pageBuilder(PageNotFound());
//    }
//  }
//
//  static _pageBuilder(Widget page) {
//    return PageRouteBuilder(
//      opaque: Args.onTop == OnTop.no ? true : false,
//      pageBuilder: (BuildContext context, _, __) => page,
//      transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
//        return new FadeTransition(
//          opacity: animation,
//          child: child,
//        );
//      },
//    );
//  }
//}
//
//class NavigationService {
//  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
//
//  Future<dynamic> navigatePush(String routeName, {Object args}) {
//    return navigatorKey.currentState.pushNamed(routeName, arguments: args);
//  }
//
//  Future<dynamic> navigateRoot(String routeName, {Object args}) {
//    return navigatorKey.currentState.pushReplacementNamed(routeName, arguments: args);
//  }
//
//  Future<dynamic> navigateTransfer(String routeName, {Object args}) {
//    return navigatorKey.currentState.popAndPushNamed(routeName, arguments: args);
//  }
//
//  void navigateBack() {
//    navigatorKey.currentState.pop();
//  }
//}
//
//enum OnTop {
//  yes,
//  no,
//}
//
//class Goto {
//  static push(String routeName, {OnTop onTop: OnTop.no, Object args}) {
//    Args.onTop = onTop;
//    locator<NavigationService>().navigatePush(routeName, args: args);
//  }
//
//  static root(String routeName, {OnTop onTop: OnTop.no, Object args}) {
//    Args.onTop = onTop;
//    locator<NavigationService>().navigateRoot(routeName, args: args);
//  }
//
//  static transfer(String routeName, {OnTop onTop: OnTop.no, Object args}) {
//    Args.onTop = onTop;
//    locator<NavigationService>().navigateTransfer(routeName, args: args);
//  }
//
//  static back() {
//    locator<NavigationService>().navigateBack();
//  }
//}
//
//class Args {
//  static OnTop onTop = OnTop.no;
//}
