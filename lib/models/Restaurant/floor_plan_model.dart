class FloorPlanModel {
  FloorPlanModel({
    this.hashId,
    this.name,
    this.full,
    this.thumbnail,
    this.tables,
  });

  String hashId;
  String name;
  String full;
  String thumbnail;
  List<FloorPlanTableModel> tables;

  factory FloorPlanModel.fromJson(Map<String, dynamic> json) => FloorPlanModel(
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        name: json["name"] == null ? null : json["name"],
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        tables: json["tables"] == null
            ? null
            : List<FloorPlanTableModel>.from(
                json["tables"].map((x) => FloorPlanTableModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "hash_id": hashId == null ? null : hashId,
        "name": name == null ? null : name,
        "full": full == null ? null : full,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "tables": tables == null
            ? null
            : List<dynamic>.from(tables.map((x) => x.toJson())),
      };
}

class FloorPlanTableModel {
  FloorPlanTableModel({
    this.hashId,
    this.name,
    this.description,
    this.floor,
    this.xAxis,
    this.yAxis,
    this.angle,
    this.flrAnimation,
  });

  String hashId;
  String name;
  String description;
  int floor;
  double xAxis;
  double yAxis;
  int angle;
  String flrAnimation;

  factory FloorPlanTableModel.fromJson(Map<String, dynamic> json) =>
      FloorPlanTableModel(
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
        floor: json["floor"] == null ? null : json["floor"],
        xAxis: json["x_axis"] == null ? null : json["x_axis"].toDouble(),
        yAxis: json["y_axis"] == null ? null : json["y_axis"].toDouble(),
        angle: json["angle"] == null ? null : json["angle"],
        flrAnimation:
            json["flr_animation"] == null ? null : json["flr_animation"],
      );

  Map<String, dynamic> toJson() => {
        "hash_id": hashId == null ? null : hashId,
        "name": name == null ? null : name,
        "description": description == null ? null : description,
        "floor": floor == null ? null : floor,
        "x_axis": xAxis == null ? null : xAxis,
        "y_axis": yAxis == null ? null : yAxis,
        "angle": angle == null ? null : angle,
        "flr_animation": flrAnimation == null ? null : flrAnimation,
      };
}
