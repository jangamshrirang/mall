import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum Types {
  completed,
  pending,
  processing,
  cancelled,
  expired,
}

class WChipWidget extends StatelessWidget {
  final Types type;

  WChipWidget({this.type});

  @override
  Widget build(BuildContext context) {
    String label = 'pending';
    Color fontColor = Colors.white;
    Color backgroundColor = Colors.grey;
    if (type == Types.completed) {
      label = 'completed';
      backgroundColor = Colors.green;
    } else if (type == Types.processing) {
      label = 'processing';
      backgroundColor = Colors.blue;
    } else if (type == Types.cancelled) {
      label = 'cancelled';
      backgroundColor = Colors.orange;
    } else if (type == Types.expired) {
      label = 'expired';
      backgroundColor = Colors.red;
    }

    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
      decoration: BoxDecoration(color: backgroundColor, borderRadius: BorderRadius.circular(10.0)),
      child: Center(
        child: Text(
          label.toUpperCase(),
          style: TextStyle(fontSize: 10.0, color: fontColor, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
