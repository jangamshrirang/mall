import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/services/global.dart';

class WUserCardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    UserModel user = Global.user;

    return ListTile(
      leading: CircleAvatar(
        backgroundImage: ExactAssetImage('assets/images/user-male.png'),
      ),
      title: Text(user.name),
      subtitle: Text(user.mobileNumber),
    );
  }
}
