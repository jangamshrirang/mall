import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class WItemCarousel extends StatefulWidget {
  final List items;
  final double height;
  final DisplayType widget;
  final double viewportFraction;

  WItemCarousel({
    @required this.widget,
    this.viewportFraction: 0.2,
    this.items,
    this.height: 130.0,
  });

  @override
  _WItemCarouselState createState() => _WItemCarouselState();
}

class _WItemCarouselState extends State<WItemCarousel> {
  final _controller = new SwiperController();
  int currentIndex = 0;

  @override
  void initState() {
    currentIndex = (widget.items.length * 0.2).round();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
        child: Swiper(
          outer: false,
          itemBuilder: (c, index) {
            return Wrap(
              runSpacing: 6.0,
              children: widget.items.map((i) {
                return index == 0 && i < 5
                    ? SizedBox(
                        width: MediaQuery.of(context).size.width / 4,
                        child: new Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            new SizedBox(
                              child: Icon(
                                MdiIcons.scanHelper,
                              ),
                              height: MediaQuery.of(context).size.width * 0.12,
                              width: MediaQuery.of(context).size.width * 0.12,
                            ),
                            new Padding(
                              padding: new EdgeInsets.only(top: 6.0),
                              child: new Text("$index"),
                            )
                          ],
                        ),
                      )
                    : Container();
              }).toList(),
            );
          },
          pagination: new SwiperPagination(margin: new EdgeInsets.all(5.0)),
          itemCount: 2,
        ),
        constraints: new BoxConstraints.loose(Size(MediaQuery.of(context).size.width, 170.0)));
  }
}

typedef Widget DisplayType(Map items);
