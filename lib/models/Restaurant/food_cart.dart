import 'package:wblue_customer/models/Restaurant/food_type_model.dart';

class RestaurantFoodCartModel {
  String hashid;
  int quantity;
  String title;
  Food food;
  int originalPrice;
  int eachPrice;
  int totalDiscount;

  RestaurantFoodCartModel(
      {this.hashid, this.quantity, this.title, this.eachPrice, this.food, this.originalPrice, this.totalDiscount});

  factory RestaurantFoodCartModel.fromJson(Map<String, dynamic> json) => RestaurantFoodCartModel(
        hashid: json["hashid"] == null ? null : json["hashid"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        title: json["title"] == null ? null : json["title"],
        eachPrice: json["each_price"] == null ? null : json["each_price"],
        food: json["food"] == null ? null : Food.fromJson(json["food"]),
        originalPrice: json["original_price"] == null ? null : json["original_price"],
        totalDiscount: json["total_discount"] == null ? null : json["total_discount"],
      );
}

class CustomRestaurantFoodVariationModel {
  String hashid;
  String value;

  CustomRestaurantFoodVariationModel({this.hashid, this.value});

  factory CustomRestaurantFoodVariationModel.fromJson(Map<String, dynamic> json) => CustomRestaurantFoodVariationModel(
        hashid: json["hashid"] == null ? null : json["hashid"],
        value: json["value"] == null ? null : json["value"],
      );
}
