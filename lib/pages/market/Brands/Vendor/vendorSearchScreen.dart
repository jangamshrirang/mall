import 'package:flutter/material.dart';

class VendorSearchScreen extends StatefulWidget {
  @override
  _VendorSearchScreenState createState() => _VendorSearchScreenState();
}

class _VendorSearchScreenState extends State<VendorSearchScreen> {
  final searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.chevron_left),
            onPressed: () {
              Navigator.pop(context, false);
            }),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        title: searchBoxTextField(searchController),
        centerTitle: true,
      ),
      body: Container(
        //color: Colors.red,
        padding: EdgeInsets.only(top: 20),
        width: size.width,
        margin: EdgeInsets.only(left: size.width * 0.1),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(left: size.width * 0.0),
                child: Text(
                  "Recomondations",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
              ),
              SizedBox(
                height: size.height * 0.01,
              ),
              Wrap(spacing: 10, runSpacing: 10, children: [
                textContainer("sling bag for women"),
                textContainer("shirt"),
                textContainer("men sport shoes"),
                textContainer("dress for womer on sale"),
              ]),
            ],
          ),
        ),
      ),
    );
  }

//search box in app bar
  searchBoxTextField(controller) {
    var size = MediaQuery.of(context).size;
    return Row(
      children: [
        Container(
          height: size.height * 0.05,
          width: size.width * 0.57,
          child: TextFormField(
            cursorColor: Colors.black,
            controller: controller,
            decoration: InputDecoration(
                filled: true,
                hintStyle: TextStyle(
                    color: Colors.black12,
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.none),
                fillColor: Colors.grey[200],
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white, width: 1),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(color: Colors.white, width: 1)),
                contentPadding:
                    EdgeInsets.only(left: 15, bottom: 10, top: 0, right: 15),
                hintText: "Search here"),
          ),
        ),
        GestureDetector(
          onTap: () {
            print(controller.text);
          },
          child: Text(
            " SEARCH",
            style: TextStyle(color: Colors.red),
          ),
        )
      ],
    );
  }

//text container for recomondations
  textContainer(String text) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        print(text);
      },
      child: Container(
        padding: EdgeInsets.only(top: 2),
        height: size.height * 0.03,
        decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        child: Text(
          "  " + text + "  ",
          style: TextStyle(color: Colors.black54, fontSize: 16),
        ),
      ),
    );
  }
}
