import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/country.dart';
import 'package:wblue_customer/providers/auth/p_auth.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_input.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

class OtpPage extends StatefulWidget {
  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  final formKey = new GlobalKey<FormState>();

  String phoneNo, code = '';
  bool codeSent = false;
  Map<String, dynamic> body = {
    'phone': '',
    'otp': '',
  };

  @override
  void dispose() {
    super.dispose();
  }

  // FacebookLogin fbLogin = new FacebookLogin();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final phoneNo = context.watch<PAuth>().phoneNumber;
    CountryModel countryModel =
        CountryModel.fromJson(Helper.state[StatesNames.selectedCountry]);

    if (phoneNo != this.phoneNo) {
      this.phoneNo = phoneNo;
    }
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          width: size.width,
          height: size.height,
          decoration: BoxDecoration(
            gradient: RadialGradient(
              colors: [Config.primaryColor, Color(0xFF284d99).withOpacity(0.9)],
              stops: [0.4, 1],
              center: Alignment(0.0, 0.0),
              focal: Alignment(0.0, -0.0),
              focalRadius: 1.9,
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Center(
              child: SafeArea(
                child: SingleChildScrollView(
                  physics: ClampingScrollPhysics(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        child: Container(
                          height: size.width * 0.5,
                          width: size.width * 0.5,
                          child: Center(
                              child: WImageWidget(
                                  placeholder: AssetImage(
                                      'assets/images/logos/wmall-512.png'))),
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.05,
                      ),
                      Container(
                        width: size.width * 0.80,
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              AutoSizeText(
                                'Phone Verification',
                                style: TextStyle(
                                    color: Config.primaryColor,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              AutoSizeText.rich(
                                TextSpan(children: [
                                  TextSpan(text: 'Enter code sent to '),
                                  TextSpan(
                                      text: '${countryModel.dialCode}$phoneNo',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                ]),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              SizedBox(
                                child: Consumer<PAuth>(
                                  builder: (__, otp, ___) => WInputWidget(
                                    label: 'OTP',
                                    isNumber: true,
                                    initialValue: code,
                                    customErrorText: otp.smsCode.error,
                                    maxLength: 6,
                                    onChanged: (val) {
                                      if (val.length <= 0) otp.setSmsCode(null);
                                      code = val;
                                    },
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Expanded(
                                    child: Consumer<PAuth>(
                                      builder: (__, otp, ___) => WRoundedButton(
                                        onCustomButtonPressed: () {
                                          if (code.length == 6) {
                                            otp.setCode(code);
                                            otp.submitOTP();
                                            FocusScope.of(context)
                                                .requestFocus(new FocusNode());
                                          }
                                          if (code.length != 6)
                                            otp.setSmsCode(
                                                'OTP contain 6-digit');
                                        },
                                        btnColor: Config.primaryColor,
                                        labelColor: Colors.white,
                                        child: AutoSizeText('Continue'),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Center(
                                child: Column(
                                  children: [
                                    AutoSizeText(
                                        'Didn\'t receive the code yet?'),
                                    FlatButton(
                                      onPressed: () => print('resend'),
                                      child: AutoSizeText('Resend Code',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          boxShadow: <BoxShadow>[
                            new BoxShadow(
                              color: Colors.black26,
                              blurRadius: 10.0,
                              offset: new Offset(0.0, 0.0),
                            ),
                          ],
                        ),
                        margin: EdgeInsets.all(5.0),
                      ),
                      FlatButton(
                        onPressed: () => ExtendedNavigator.of(context).pop(),
                        child: AutoSizeText.rich(
                          TextSpan(
                              style: TextStyle(color: Colors.white),
                              children: [
                                TextSpan(text: 'Already have an account ? '),
                                TextSpan(
                                    text: 'Login',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                              ]),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
