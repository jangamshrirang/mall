import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

navigate(BuildContext context, screen) {
  return Navigator.push(
      context, MaterialPageRoute(builder: (context) => screen));
}

button(String title, Function onTap, Color color, double widthh) {
  return SizedBox(
    height: 0.1.sw,
    width: 0.5.sw,
    child: RaisedButton(
        child: Text(
          title,
          style: TextStyle(color: Colors.white, fontSize: 32.sp),
        ),
        color: color,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        onPressed: onTap),
  );
}
