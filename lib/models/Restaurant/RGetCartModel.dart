class RCartListModel {
    RCartListModel({
        this.customer,
        this.tableNo,
        this.cartNo,
        this.restaurant,
        this.food,
        this.totalInfo,
    });

    String customer;
    dynamic tableNo;
    String cartNo;
    Restaurant restaurant;
    List<Food> food;
    List<dynamic> totalInfo;

    factory RCartListModel.fromJson(Map<String, dynamic> json) => RCartListModel(
        customer: json["customer"] == null ? null : json["customer"],
        tableNo: json["table_no"],
        cartNo: json["cart_hash_id"] == null ? null : json["cart_hash_id"],
        restaurant: json["restaurant"] == null ? null : Restaurant.fromJson(json["restaurant"]),
        food: json["food"] == null ? null : List<Food>.from(json["food"].map((x) => Food.fromJson(x))),
        totalInfo: json["total_info"] == null ? null : List<dynamic>.from(json["total_info"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "customer": customer == null ? null : customer,
        "table_no": tableNo,
        "cart_hash_id": cartNo == null ? null : cartNo,
        "restaurant": restaurant == null ? null : restaurant.toJson(),
        "food": food == null ? null : List<dynamic>.from(food.map((x) => x.toJson())),
        "total_info": totalInfo == null ? null : List<dynamic>.from(totalInfo.map((x) => x)),
    };
}

class Food {
    Food({
        this.quantity,
        this.onCheckout,
        this.isOrdered,
        this.foodStatus,
        this.foodInfo,
    });

    int quantity;
    int onCheckout;
    int isOrdered;
    int foodStatus;
    FoodInfo foodInfo;

    factory Food.fromJson(Map<String, dynamic> json) => Food(
        quantity: json["quantity"] == null ? null : json["quantity"],
        onCheckout: json["on_checkout"] == null ? null : json["on_checkout"],
        isOrdered: json["is_ordered"] == null ? null : json["is_ordered"],
        foodStatus: json["food_status"] == null ? null : json["food_status"],
        foodInfo: json["food_info"] == null ? null : FoodInfo.fromJson(json["food_info"]),
    );

    Map<String, dynamic> toJson() => {
        "quantity": quantity == null ? null : quantity,
        "on_checkout": onCheckout == null ? null : onCheckout,
        "is_ordered": isOrdered == null ? null : isOrdered,
        "food_status": foodStatus == null ? null : foodStatus,
        "food_info": foodInfo == null ? null : foodInfo.toJson(),
    };
}

class FoodInfo {
    FoodInfo({
        this.hashId,
        this.name,
        this.type,
        this.price,
        this.description,
        this.full,
        this.thumbnail,
    });

    String hashId;
    String name;
    dynamic type;
    int price;
    String description;
    String full;
    String thumbnail;

    factory FoodInfo.fromJson(Map<String, dynamic> json) => FoodInfo(
        hashId: json["hash_id"] == null ? null : json["hash_id"],
        name: json["name"] == null ? null : json["name"],
        type: json["type"],
        price: json["price"] == null ? null : json["price"],
        description: json["description"] == null ? null : json["description"],
        full: json["full"] == null ? null : json["full"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
    );

    Map<String, dynamic> toJson() => {
        "hash_id": hashId == null ? null : hashId,
        "name": name == null ? null : name,
        "type": type,
        "price": price == null ? null : price,
        "description": description == null ? null : description,
        "full": full == null ? null : full,
        "thumbnail": thumbnail == null ? null : thumbnail,
    };
}

class Restaurant {
    Restaurant({
        this.name,
        this.hashid,
        this.totalFoods,
    });

    String name;
    String hashid;
    int totalFoods;

    factory Restaurant.fromJson(Map<String, dynamic> json) => Restaurant(
        name: json["name"] == null ? null : json["name"],
        hashid: json["hashid"] == null ? null : json["hashid"],
        totalFoods: json["total_foods"] == null ? null : json["total_foods"],
    );

    Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "hashid": hashid == null ? null : hashid,
        "total_foods": totalFoods == null ? null : totalFoods,
    };
}
