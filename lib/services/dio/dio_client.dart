import 'package:auto_route/auto_route.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:wblue_customer/models/country.dart';
import 'package:wblue_customer/services/dio/retry_interceptor.dart';
import 'package:wblue_customer/services/helper.dart';
import 'package:wblue_customer/widgets/w_loading.dart';

import '../../env/config.dart';
import 'dio_connectivity_request_retrier.dart';

class DioClient {
  Dio _dio = Dio();

  DioClient() {
//    print('Initiating Dio...');

    this._dio.options.baseUrl = Config.api;
    this._dio.options.connectTimeout = 30000;
    this._dio.options.receiveTimeout = 10000;

    this._dio.interceptors.add(RetryOnConnectionChangeInterceptor(
          requestRetrier: DioConnectivityRequestRetrier(
            dio: _dio,
            connectivity: Connectivity(),
          ),
        ));
//    print('Dio Initiated!');
  }

  Future<Map> randomGet(String url, {Map<String, dynamic> data}) async {
    final response = await this._dio.get(url, queryParameters: data);

    if (response.statusCode >= 400) return null;
    print(response.data);
    return response.data;
  }

  Future<APIResponse> publicGet(String uri, {Map<String, dynamic> data}) async {
    CountryModel country = CountryModel.fromJson(Helper.state[StatesNames.selectedCountry]);
    print(country.iso);
    _dio.options.headers["content-type"] = 'application/json';
    _dio.options.headers["App-Origin"] = Config.appOrigin;
    _dio.options.headers["Current-ISO"] = country.iso;
    _dio.options.headers["PubKey"] = Config.pubKey;

    final response = await this._dio.get(uri, queryParameters: data);
    return _process(response);
  }

  Future<APIResponse> publicPost(String uri, {Map<String, dynamic> data}) async {
    CountryModel country = CountryModel.fromJson(Helper.state[StatesNames.selectedCountry]);

    _dio.options.headers["content-type"] = 'application/json';
    _dio.options.headers["App-Origin"] = Config.appOrigin;
    _dio.options.headers["Current-ISO"] = country.iso;
    _dio.options.headers["PubKey"] = Config.pubKey;

    FormData formData = new FormData.fromMap(data);
    final response = await this._dio.post(
      uri,
      data: formData,
      onSendProgress: (int sent, int total) {
        if (sent == total) print('Sent to : $uri');
      },
      onReceiveProgress: (int received, int total) {
        if (received == total) print('Full Received');
      },
    ).catchError((onError) {
      print(onError.message);
    });

    return _process(response);
  }

  Future<APIResponse> privateGet(String uri, {Map<String, dynamic> data}) async {
    String token = Helper.state[StatesNames.token] ?? Helper.currentAuthUser.get('token');

    CountryModel country = CountryModel.fromJson(Helper.state[StatesNames.selectedCountry]);

    _dio.options.headers = {
      'App-Origin': Config.appOrigin,
      'Content-Type': 'application/json',
      'Current-ISO': country.iso,
      'Authorization': 'Bearer $token',
      'PubKey': Config.pubKey,
    };

    final response = await this._dio.get(uri, queryParameters: data).catchError((onError) {
      print('Error Url: ($uri) $onError');
      WLoading.type(Loading.dismiss);
      if (onError.response.statusCode == 401 || onError.response.statusCode == 302) {
        logout();
      }
    });

    return _process(response);
  }

  Future<APIResponse> privatePost(String uri, {Map<String, dynamic> data}) async {
    String token = Helper.state[StatesNames.token] ?? Helper.currentAuthUser.get('token');

    CountryModel country = CountryModel.fromJson(Helper.state[StatesNames.selectedCountry]);

    _dio.options.headers = {
      'App-Origin': Config.appOrigin,
      'Content-Type': 'application/json',
      'Current-ISO': country.iso,
      'Authorization': 'Bearer $token',
      'PubKey': Config.pubKey,
    };

    FormData formData = new FormData.fromMap(data);

    final response = await this._dio.post(
      uri,
      data: formData,
      onSendProgress: (int sent, int total) {
        if (sent == total) print('Sent to : $uri');
      },
      onReceiveProgress: (int received, int total) {
        if (received == total) print('Full Received');
      },
    ).catchError((onError) {
      WLoading.type(Loading.dismiss);

      if (onError.response.statusCode == 401 || onError.response.statusCode == 302) {
        logout();
        // BotToast.showText(text: 'Something went wrong, please try again!');
        // ExtendedNavigator.root.push('/');
      }
      print('onError: $onError');
    });

    if (response == null) {
      // Helper.currentAuthUser.put('isLogin', false);
      // BotToast.showText(text: 'Something went wrong, please try again!');
      // ExtendedNavigator.root.push('/');
    }
    return _process(response);
  }

  APIResponse _process(Response response) {
    if (response.statusCode == 200) {
      return APIResponse.fromJson(response.data);
    } else {
      var body = response.data;
      body['code'] = response.statusCode;
      return APIResponse.fromJson(body);
    }
  }

  logout() {
    WLoading.type(Loading.dismiss);
    // Helper.unauthenticated = 1;
    // Goto.root('/login');
  }
}

class APIResponse {
  final String message;
  final data;
  final int code;
  final errors;
  final List<String> errorArray;
  final String errorMessages;

  APIResponse({this.message, this.data, this.code, this.errors, this.errorArray, this.errorMessages});

  factory APIResponse.fromJson(Map<String, dynamic> json) {
    List<String> errorArray = [];
    String errorMessages = '';

    String multiLineString(List<String> arr) {
      StringBuffer sb = new StringBuffer();
      for (String line in arr) {
        sb.write(line + '\n');
      }
      return sb.toString().substring(0, sb.length - 2);
    }

    if (json['code'] == 422 || json['code'] == 412) {
      Map errors = json['code'] == 412 ? json['data'] : json['errors'];
      if (errors.length > 0) {
        errors.forEach((key, val) {
          errorArray.add(val[0]);
        });
        errorMessages = multiLineString(errorArray);
        print(errorMessages);
      }
    } else {
      if (json['code'] >= 400) {}
    }

    return APIResponse(
      message: json['message'] == null ? json['msg'] : json['message'],
      data: json['data'],
      code: json['code'],
      errors: json['errors'],
      errorArray: errorArray,
      errorMessages: errorMessages,
    );
  }
}
