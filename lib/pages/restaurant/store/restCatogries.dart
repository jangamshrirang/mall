import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/food_type_model.dart';
import 'package:wblue_customer/models/Restaurant/restaurant_cart_mode.dart';
import 'package:wblue_customer/providers/restaurant/p_food_cart.dart';
import 'package:wblue_customer/providers/restaurant/p_food_list_type.dart';
import 'package:wblue_customer/providers/socket/client.dart';
import 'package:wblue_customer/routes/router.gr.dart';
import 'package:wblue_customer/services/helper.dart';

class RestCatogries extends StatefulWidget {
  final String coverImage,
      restimage,
      restName,
      restSlogan,
      restId,
      coverThumbnail,
      reservationHashId,
      restThumbnail;
  final int restindex;
  final tableID;

  final bool isFromScan, isFromTakeAway, isDelivery, preOrder;

  const RestCatogries(
      {this.coverImage,
      this.restimage,
      this.restName,
      this.isDelivery: false,
      this.preOrder: false,
      this.isFromScan: false,
      this.restSlogan,
      this.restindex,
      this.isFromTakeAway,
      this.restId,
      this.restThumbnail,
      this.coverThumbnail,
      this.tableID,
      this.reservationHashId});
  @override
  _RestCatogriesState createState() => _RestCatogriesState();
}

class _RestCatogriesState extends State<RestCatogries>
    with AutomaticKeepAliveClientMixin<RestCatogries> {
  @override
  bool get wantKeepAlive => true;
  TextEditingController reqController = new TextEditingController();

  @override
  void initState() {
    Future.microtask(() => context
        .read<PRestaurantFoodCart>()
        .setRestaurantHashId(hashid: widget.restId, resName: widget.restName));
    stateManagment.setRashID(widget.restId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Consumer<PFoodListType>(
          builder: (context, foodType, child) => DefaultTabController(
            length: foodType.foodListType.length,
            child: Scaffold(
              bottomNavigationBar: Consumer<PRestaurantFoodCart>(
                builder: (context, checkCart, child) => Container(
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.0, 1.0), //(x,y)
                      blurRadius: 1.0,
                    ),
                  ]),
                  child: checkCart.restaurantCart.food.length > 0 &&
                          checkCart.restHashidInCart == checkCart.restHashid
                      ? GestureDetector(
                          onTap: () => ExtendedNavigator.of(context)
                              .root
                              .push('/restaurant-food-cart'),
                          child: BottomAppBar(
                            child: Consumer<PRestaurantFoodCart>(
                              builder: (context, cart, child) => Container(
                                margin: EdgeInsets.all(10.0),
                                padding: EdgeInsets.all(10.0),
                                decoration: BoxDecoration(
                                  color: cart.restaurantCart != null
                                      ? Config.primaryColor
                                      : Colors.black12,
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        SizedBox(width: 0.05.sw),
                                        Container(
                                          width: 0.09.sw,
                                          height: 0.09.sw,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                          child: Padding(
                                            padding: EdgeInsets.all(2),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                color:
                                                    cart.restaurantCart != null
                                                        ? Config.primaryColor
                                                        : Colors.black12,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(5)),
                                              ),
                                              child: totalCart(cart),
                                            ),
                                          ),
                                        ),
                                        SizedBox(width: 0.05.sw),
                                        Text(
                                          'VIEW CART',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 34.sp,
                                          ),
                                        )
                                      ],
                                    ),
                                    total(cart.restaurantCart),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      : BottomAppBar(
                          child: Consumer<PRestaurantFoodCart>(
                            builder: (context, cart, child) => Container(
                              margin: EdgeInsets.all(10.0),
                              padding: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                color: Colors.black12,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      SizedBox(width: 0.05.sw),
                                      Container(
                                        width: 0.09.sw,
                                        height: 0.09.sw,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: Padding(
                                          padding: EdgeInsets.all(2),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.black12,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5)),
                                            ),
                                            child: Center(
                                              child: Text(
                                                '0',
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 38.sp,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 0.05.sw),
                                      Text(
                                        'VIEW CART',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 34.sp,
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'QAR 0.00',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 34.sp,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(width: 0.05.sw),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                ),
              ),
              appBar: PreferredSize(
                preferredSize: Size.fromHeight(0.135.sh),
                child: Consumer<PRestaurantFoodCart>(
                  builder: (context, checkCart, child) => AppBar(
                    elevation: 0,
                    backgroundColor: Config.primaryColor,
                    centerTitle: false,
                    title: Text('Menu'),
                    actions: checkCart.restaurantCart.food.length > 0 &&
                            checkCart.restHashidInCart == checkCart.restHashid
                        ? [
                            IconButton(
                              icon:
                                  Icon(Icons.shopping_bag, color: Colors.white),
                              onPressed: () {
                                // widget.fromScan == "yes"
                                //     ? print("coming from Scan")
                                //     :
                                // navigate(
                                //     context,
                                //     RestaurantCartPage(
                                //       reservationHashID:
                                //           widget.reservationHashId,
                                //       restId: widget.restId,
                                //       fromScan: widget.fromScan,
                                //       delivery: widget.delivery,
                                //       fromTakeAway: widget.fromTakeAway,
                                //       preOrder: widget.preOrder,
                                //     ));
                                ExtendedNavigator.of(context)
                                    .root
                                    .push('/restaurant-food-cart',
                                        arguments: RestaurantFoodCartArguments(
                                          reservationHashID:
                                              widget.reservationHashId,
                                          restId: widget.restId,
                                          isFromScan: widget.isFromScan,
                                          isDelivery: widget.isDelivery,
                                          isFromTakeAway: widget.isFromTakeAway,
                                          preOrder: widget.preOrder,
                                        ));
                              },
                            )
                          ]
                        : [
                            // IconButton(
                            // icon: Icon(Icons.chat),
                            // onPressed: () {
                            //   //checking
                            //   context.read<PSocketClient>().emitter(
                            //       eventName:
                            //           WEvents.checkoutRestaurentHashId,
                            //       data: {
                            //         'order_type': "Deliery",
                            //         'restaurent_hashid': widget.restId,
                            //       });
                            // })
                          ],
                    bottom: PreferredSize(
                        child: Container(
                          height: 0.06.sh,
                          width: double.infinity,
                          color: Colors.white,
                          child: TabBar(
                            indicator: UnderlineTabIndicator(
                                borderSide: BorderSide(
                                    width: 3, color: Config.primaryColor),
                                insets: EdgeInsets.only(
                                  left: 8,
                                  right: 8,
                                )),
                            labelPadding: EdgeInsets.only(left: 8, right: 8),
                            isScrollable: true,
                            unselectedLabelColor: Colors.black.withOpacity(0.3),
                            indicatorColor: Colors.black,
                            labelColor: Config.primaryColor,
                            onTap: (index) => foodType.setCurrentTab(index),
                            tabs: List.generate(
                              foodType.foodListType.length,
                              (index) => Container(
                                child: Tab(
                                  child: Text(
                                    '${foodType.foodListType[index].foodTypeName}',
                                    style: TextStyle(fontSize: 28.sp),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        preferredSize: Size.fromHeight(50.0)),
                  ),
                ),
              ),
              body: foodType.foodListType.length > 0
                  ? IndexedStack(
                      index: foodType.currentTab,
                      children: List.generate(
                          foodType.foodListType.length,
                          (i) => GridView.builder(
                              itemCount: foodType.foodListType[i].foods?.length,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 1,
                                childAspectRatio: 3,
                                crossAxisSpacing: 8,
                              ),
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                    foodType.foodListType[i].foods[index]
                                            .isContainsVariation
                                        ? ExtendedNavigator.of(context).push(
                                            '/variation',
                                            arguments:
                                                RestaurantFoodVariationArguments(
                                              food: foodType
                                                  .foodListType[i].foods[index],
                                              restaurantHashid: widget.restId,
                                              isFromTakeAway:
                                                  widget.isFromTakeAway,
                                              isDelivery: widget.isDelivery,
                                              preorder: widget.preOrder,
                                              isFromScan: widget.isFromScan,
                                              reservationHashId:
                                                  widget.reservationHashId,
                                            ))
                                        : _addToCartBtmSheet(
                                            foodType
                                                .foodListType[i].foods[index],
                                            foodType);
                                  },
                                  child: Stack(
                                    children: [
                                      Container(
                                          margin: const EdgeInsets.symmetric(
                                              horizontal: 8.0),
                                          decoration: BoxDecoration(
                                            border: Border(
                                              bottom: BorderSide(
                                                  color: Colors.black12,
                                                  width: 0.5),
                                            ),
                                          ),
                                          child: Container(
                                            margin: EdgeInsets.symmetric(
                                              vertical: 8.0,
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Flexible(
                                                  child: Container(
                                                    padding: EdgeInsets.only(
                                                        right: 8.0),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Flexible(
                                                          child: Text(
                                                            '${foodType.foodListType[i].foods[index].name}',
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize:
                                                                    34.sp),
                                                            maxLines: 1,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                            height: 0.008.sh),
                                                        AutoSizeText(
                                                          '${foodType.foodListType[i].foods[index].description}',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              color: Colors
                                                                  .black54,
                                                              fontSize: 29.sp),
                                                          maxLines: 4,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                        SizedBox(
                                                            height: 0.008.sh),
                                                        Flexible(
                                                          child: AutoSizeText(
                                                            'QR ${foodType.foodListType[i].foods[index].price}',
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                fontSize:
                                                                    32.sp),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                CachedNetworkImage(
                                                  imageUrl: foodType
                                                          .foodListType[i]
                                                          .foods[index]
                                                          .full ??
                                                      '',
                                                  imageBuilder: (context,
                                                          imageProvider) =>
                                                      AspectRatio(
                                                    aspectRatio: 5 / 5,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                        image: DecorationImage(
                                                          image: imageProvider,
                                                          fit: BoxFit.fill,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  placeholder: (context, url) =>
                                                      AspectRatio(
                                                    aspectRatio: 5 / 5,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                        image: DecorationImage(
                                                          image: NetworkImage(foodType
                                                                  .foodListType[
                                                                      i]
                                                                  .foods[index]
                                                                  .thumbnail ??
                                                              ''),
                                                          fit: BoxFit.fill,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          SizedBox(),
                                                ),
                                              ],
                                            ),
                                          )),
                                      // Consumer<PRestaurantFoodCart>(
                                      //   builder: (context, check, child) =>
                                      //       Positioned(
                                      //     right: 0,
                                      //     child: CustomPaint(
                                      //         painter: _ShapesPainter(
                                      //             Colors.redAccent),
                                      //         child: Container(
                                      //             height: 50,
                                      //             width: 50,
                                      //             child: Center(
                                      //                 child: Padding(
                                      //                     padding:
                                      //                         EdgeInsets.only(
                                      //                             left: 20.0,
                                      //                             bottom: 16),
                                      //                     child: Text('0',
                                      //                         style: TextStyle(
                                      //                           color: Colors
                                      //                               .white,
                                      //                           fontSize: 26.sp,
                                      //                           fontWeight:
                                      //                               FontWeight
                                      //                                   .w500,
                                      //                         )))))),
                                      //   ),
                                      // )
                                    ],
                                  ),
                                );
                              })),
                    )
                  : Container(),
            ),
          ),
        ));
  }

  Widget totalCart(PRestaurantFoodCart cart) {
    int total = 0;

    cart.restaurantCart.food.forEach((item) {
      total += item.quantity;
    });

    return Center(
      child: Text(
        '$total',
        style: TextStyle(
          color: Colors.white,
          fontSize: 38.sp,
        ),
      ),
    );
  }

  Widget total(RestaurantCartModel cart) {
    return Row(
      children: [
        Text(
          'QAR ${cart.totalInfo.total}',
          style: TextStyle(
              color: Colors.white,
              fontSize: 34.sp,
              fontWeight: FontWeight.bold),
        ),
        SizedBox(width: 0.05.sw),
      ],
    );
  }

  void _addToCartBtmSheet(Food food, PFoodListType foodListTypeProvider) {
    double baseAmount = double.parse(food.price);
    double totalAmount;

    int dynamicQty = 1;
    int max = 99;
    int min = 1;
    int stepValue = 1;

    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return SingleChildScrollView(
          child: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState2) {
              totalAmount = baseAmount * dynamicQty;
              return GestureDetector(
                onTap: () => FocusScope.of(context).unfocus(),
                child: Container(
                  padding: EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0),
                      )),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          CachedNetworkImage(
                            imageUrl: food.full ?? '',
                            imageBuilder: (context, imageProvider) => Container(
                              height: 0.3.sw,
                              width: 0.3.sw,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                            ),
                            placeholder: (context, url) => Container(
                              height: 0.3.sw,
                              width: 0.3.sw,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                image: DecorationImage(
                                  image: NetworkImage(food.thumbnail ?? ''),
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                            ),
                            errorWidget: (context, url, error) => SizedBox(),
                          ),
                          SizedBox(width: 0.02.sw),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${food.name}',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 32.sp),
                                ),
                                Text(
                                  'QAR ${food.price}',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 32.sp),
                                ),
                                food.description.length > 0
                                    ? Column(
                                        children: [
                                          SizedBox(height: 0.005.sh),
                                          Text('${food.description}',
                                              style: TextStyle(
                                                  fontSize: 32.sp,
                                                  color: Colors.black54)),
                                        ],
                                      )
                                    : SizedBox(),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 0.005.sh),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              RawMaterialButton(
                                constraints: BoxConstraints.tightFor(
                                    width: 0.08.sw, height: 0.08.sw),
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    side: BorderSide(
                                        color: dynamicQty > min
                                            ? Colors.black87
                                            : Colors.black38,
                                        width: 1.5)),
                                onPressed: dynamicQty == min
                                    ? null
                                    : () {
                                        setState2(() {
                                          dynamicQty = dynamicQty == min
                                              ? min
                                              : dynamicQty -= stepValue;
                                        });
                                      },
                                fillColor: Colors.transparent,
                                child: Icon(
                                  MdiIcons.minus,
                                  color: dynamicQty > min
                                      ? Colors.black87
                                      : Colors.black38,
                                  size: 50.sp,
                                ),
                              ),
                              Container(
                                width: 0.1.sw,
                                child: Text(
                                  '$dynamicQty',
                                  style: TextStyle(
                                    fontSize: 22 * 0.8,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              RawMaterialButton(
                                constraints: BoxConstraints.tightFor(
                                    width: 0.08.sw, height: 0.08.sw),
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    side: BorderSide(
                                        color: dynamicQty < max
                                            ? Colors.black87
                                            : Colors.black38,
                                        width: 1.5)),
                                onPressed: dynamicQty == max
                                    ? null
                                    : () {
                                        setState2(() {
                                          dynamicQty = dynamicQty == max
                                              ? max
                                              : dynamicQty += stepValue;
                                        });
                                      },
                                fillColor: Colors.transparent,
                                child: Icon(
                                  MdiIcons.plus,
                                  color: dynamicQty < max
                                      ? Colors.black87
                                      : Colors.black38,
                                  size: 50.sp,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 0.005.sh),
                          Text(
                            'TOTAL: $totalAmount',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 32.sp),
                          ),
                        ],
                      ),
                      SizedBox(height: 0.01.sh),
                      Text(
                        'Add a note (Optional)',
                        style: TextStyle(fontSize: 32.sp),
                      ),
                      SizedBox(height: 0.01.sh),
                      TextField(
                        minLines: 1,
                        maxLines: null,
                        keyboardType: TextInputType.text,
                        controller: reqController,
                        onSubmitted: (value) {
                          FocusScope.of(context).unfocus();
                        },
                        decoration: InputDecoration(
                          hintText: 'Do you have any request?',
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey),
                          ),
                        ),
                      ),
                      SizedBox(height: 0.02.sh),
                      Selector<PRestaurantFoodCart, bool>(
                        selector: (_, clear) =>
                            clear.restaurantCart.food.length > 0 &&
                            clear.restHashid != clear.restHashidInCart,
                        builder: (context, isSameRestaurant, child) => Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  bottom:
                                      MediaQuery.of(context).viewInsets.bottom),
                              child: SizedBox(
                                height: 0.06.sh,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0)),
                                  onPressed: () async {
                                    bool canNav =
                                        await ExtendedNavigator.of(context)
                                            .root
                                            .canNavigate('/restaurants/cart');
                                    if (!canNav) return;

                                    try {
                                      if (isSameRestaurant) {
                                        showCartAlertDialog(
                                          BackButtonBehavior.none,
                                          cancel: () {},
                                          confirm: () {
                                            context
                                                .read<PRestaurantFoodCart>()
                                                .apiRestaurantAddToCart(
                                                    restHashid: widget.restId,
                                                    foodId: food.hashid,
                                                    quantity: dynamicQty,
                                                    orderType: widget.isFromScan
                                                        ? WMallOrderType.dineIn
                                                        : widget.isDelivery
                                                            ? WMallOrderType
                                                                .delivery
                                                            : WMallOrderType
                                                                .takeAway,
                                                    reservationHashId: widget
                                                        .reservationHashId,
                                                    specialRequest:
                                                        '${reqController.text.length > 0 ? reqController.text : 'any'}',
                                                    isReset: true);
                                            reqController.clear();
                                            Navigator.of(context).pop();
                                          },
                                          backgroundReturn: () {},
                                        );
                                      } else {
                                        context.read<PRestaurantFoodCart>().apiRestaurantAddToCart(
                                            restHashid: widget.restId,
                                            foodId: food.hashid,
                                            quantity: dynamicQty,
                                            orderType: widget.isFromScan || widget.preOrder
                                                ? WMallOrderType.dineIn
                                                : widget.isDelivery
                                                    ? WMallOrderType.delivery
                                                    : WMallOrderType.takeAway,
                                            reservationHashId:
                                                widget.reservationHashId != null ? widget.reservationHashId : "null",
                                            specialRequest:
                                                '${reqController.text.length > 0 ? reqController.text : 'any'}');
                                        reqController.clear();
                                        Navigator.of(context).pop();
                                      }
                                    } catch (e) {
                                      print(
                                          'Error in restCatogries.dart file when adding cart: Error ($e)');
                                    }

                                    // if (isSameRestaurant) {
                                    //   showCartAlertDialog(
                                    //     BackButtonBehavior.none,
                                    //     cancel: () {},
                                    //     confirm: () {
                                    //       context
                                    //           .read<PRestaurantFoodCart>()
                                    //           .setItems(
                                    //               restHashid: widget.restId,
                                    //               food: food,
                                    //               qty: dynamicQty,
                                    //               total: food.price,
                                    //               isReset: true);

                                    //       Navigator.of(context).pop();
                                    //     },
                                    //     backgroundReturn: () {},
                                    //   );
                                    // } else {
                                    //   context
                                    //       .read<PRestaurantFoodCart>()
                                    //       .setItems(
                                    //         restHashid: widget.restId,
                                    //         food: food,
                                    //         qty: dynamicQty,
                                    //         total: food.price,
                                    //       );
                                    //   Navigator.of(context).pop();
                                    // }

                                    //popup a text toast;
                                  },
                                  elevation: 0.0,
                                  color: Config.primaryColor,
                                  textColor: Colors.white,
                                  child: Text('ADD TO CART'),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }

  void showCartAlertDialog(BackButtonBehavior backButtonBehavior,
      {VoidCallback cancel,
      VoidCallback confirm,
      VoidCallback backgroundReturn}) {
    BotToast.showAnimationWidget(
        clickClose: true,
        allowClick: false,
        onlyOne: true,
        crossPage: true,
        backgroundColor: Colors.black54,
        backButtonBehavior: backButtonBehavior,
        toastBuilder: (cancelFunc) => AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              title: const Text('Clear Cart?'),
              content: Text(
                  'You have already selected different restaurant. If you continue your cart and selection will be removed.'),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    cancelFunc();
                    cancel?.call();
                  },
                  highlightColor: const Color(0x55FF8A80),
                  splashColor: const Color(0x99FF8A80),
                  child: Text(
                    'cancel',
                    style: TextStyle(color: Colors.redAccent, fontSize: 36.sp),
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    cancelFunc();
                    confirm?.call();
                  },
                  child: Text(
                    'confirm',
                    style: TextStyle(fontSize: 36.sp),
                  ),
                ),
              ],
            ),
        animationDuration: Duration(milliseconds: 300));
  }
}

class _ShapesPainter extends CustomPainter {
  final Color color;
  _ShapesPainter(this.color);
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = color;
    var path = Path();
    path.lineTo(size.width, 0);
    path.lineTo(size.height, size.width);
    path.close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
