import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/Botique/Boggers/BloggersScreen.dart';

import 'Boggers/BolggersList.dart';
import 'Followings/F_listing.dart';
import 'Followings/Following_Screen.dart';
import 'LadyIcon/L_ListingScreen.dart';
import 'LadyIcon/LadyScreen.dart';

class BotiqueHome extends StatefulWidget {
  int colorVal = 0xff84020e;
  @override
  _BotiqueHomeState createState() => _BotiqueHomeState();
}

class _BotiqueHomeState extends State<BotiqueHome>
    with TickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 4);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {
      widget.colorVal = 0xff84020e;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: DefaultTabController(
        length: 4,
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Config.primaryColor,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20)),
              ),
              child: TabBar(
                controller: _tabController,
                indicatorSize: TabBarIndicatorSize.label,
                indicatorColor: Config.primaryColor,
                unselectedLabelColor: Colors.grey,
                tabs: <Widget>[
                  Tab(
                      icon: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.redAccent,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    ),
                    child: Icon(Icons.search,
                        size: 20,
                        color: _tabController.index == 0
                            ? Colors.white
                            : Colors.white),
                  )),
                  Tab(
                      icon: _tabController.index == 1
                          ? Image.asset("assets/botique/b_white.png",
                              height: 40)
                          : Image.asset("assets/botique/b_blue.png",
                              height: 40)),
                  Tab(
                      icon: _tabController.index == 2
                          ? Image.asset("assets/botique/l_white.png",
                              height: 40)
                          : Image.asset("assets/botique/l_blue.png",
                              height: 40)),
                  Tab(
                      icon: _tabController.index == 3
                          ? Image.asset("assets/botique/not_white.png",
                              height: 40)
                          : Image.asset("assets/botique/note_blue.png",
                              height: 40)),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  BloggersList(),
                  BloggersList(),
                  L_ListingScreen(),
                  FollowingListing(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
