import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

import '../../env/config.dart';
import 'package:wblue_customer/providers/market/p_home.dart';
import 'package:wblue_customer/providers/market/p_view_product.dart';
import 'package:wblue_customer/services/view_product_app_bar.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_image_product_card.dart';
import 'package:wblue_customer/widgets/w_items_wrapper.dart';
import 'package:wblue_customer/widgets/w_rating.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

class ViewOtherProduct extends StatelessWidget {
  Widget _productCard(BuildContext context, List products) {
    return WItemList(
      crossAxisCellCount: 2,
      crossAxisCount: 4,
      widget: (product, index) => Container(
        width: MediaQuery.of(context).size.width * 0.5,
        child: WProductImageCard(
          onTap: () {
            context.read<PViewProduct>().setOtherProduct(product);
            ExtendedNavigator.of(context).root.push('/view-other-product');
          },
          isVertical: true,
          imageUrl: '${product['images'][0]['small']}',
          widget: Container(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AutoSizeText(
                '${product['name']}',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                minFontSize: 16,
              ),
              SizedBox(height: 10.0),
              AutoSizeText(
                '${product['shortDescription']}',
                style: TextStyle(fontSize: 16),
                maxLines: 4,
                overflow: TextOverflow.ellipsis,
                minFontSize: 14,
              ),
              SizedBox(height: 10.0),
              Container(
                child: Row(
                  children: <Widget>[
                    AutoSizeText(
                      'QR ${product['newPrice']}',
                      style: TextStyle(fontSize: 18, color: Colors.redAccent),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      minFontSize: 16,
                    ),
                    SizedBox(width: 8.0),
                    product['oldPrice'] != null
                        ? Expanded(
                            child: AutoSizeText(
                              'QR ${product['oldPrice']}',
                              style: TextStyle(fontSize: 18, color: Colors.grey, decoration: TextDecoration.lineThrough),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
              SizedBox(height: 10.0),
              Row(
                children: [
                  Icon(
                    MdiIcons.checkboxBlankCircle,
                    size: 16.0,
                    color: product['availibilityCount'] > 0 ? Colors.green.withOpacity(0.9) : Colors.red.withOpacity(0.9),
                  ),
                  SizedBox(width: 8.0),
                  AutoSizeText(
                    '${product['availibilityCount'] > 0 ? 'In' : 'Out'} stock, ${product['availibilityCount'] > 1 ? '${product['availibilityCount']} units' : '${product['availibilityCount']} unit'}',
                    style: TextStyle(fontSize: 16, color: product['availibilityCount'] > 0 ? Colors.green.withOpacity(0.9) : Colors.red.withOpacity(0.9)),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    minFontSize: 16,
                  ),
                ],
              ),
              SizedBox(height: 3.0),
              Row(
                children: [
                  WRatingsWidget(product['ratingsValue'] / 100),
                  SizedBox(width: 2),
                  AutoSizeText(
                    '(${product['ratingsCount']})',
                    style: TextStyle(fontSize: 16, color: Colors.grey.withOpacity(0.9)),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    minFontSize: 16,
                  ),
                ],
              ),
              SizedBox(height: 3.0),
              AutoSizeText(
                '15 Reviews',
                style: TextStyle(fontSize: 16, color: Colors.grey),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                minFontSize: 16,
              ),
            ],
          )),
        ),
      ),
      items: products,
    );
  }

  @override
  Widget build(BuildContext context) {
    print('view other product rebuild');
    double width = MediaQuery.of(context).size.width;
    double tabWidth = width / 1.5;
    final HideNavbar hiding = HideNavbar();

    return ValueListenableBuilder(
      valueListenable: hiding.visible,
      builder: (context, bool value, child) => AnnotatedRegion<SystemUiOverlayStyle>(
        value: value ? SystemUiOverlayStyle.light : SystemUiOverlayStyle.dark,
        child: Scaffold(
          backgroundColor: Config.bodyColor,
          body: Consumer<PViewProduct>(builder: (__, product, _) {
            return Stack(
              children: [
                ListView(
                  physics: ClampingScrollPhysics(),
                  padding: EdgeInsets.zero,
                  controller: hiding.controller,
                  children: [
                    Container(
                      height: 260,
                      color: Colors.white,
                      child: Stack(
                        children: [
                          SafeArea(
                            child: Swiper(
                              loop: false,
                              pagination: SwiperCustomPagination(builder: (BuildContext context, SwiperPluginConfig config) {
                                return Positioned(
                                  bottom: 16,
                                  child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: Center(
                                          child: Container(
                                              decoration: BoxDecoration(
                                                color: Colors.black.withOpacity(0.5),
                                                borderRadius: BorderRadius.circular(30),
                                              ),
                                              padding: EdgeInsets.all(5.0),
                                              child: AutoSizeText(
                                                '${config.activeIndex + 1} / ${config.itemCount}',
                                                style: TextStyle(color: Colors.white),
                                              )))),
                                );
                              }),
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  height: 300,
                                  child: WImageWidget(
                                    placeholder: AssetImage('${product.selectedOtherProduct['images'][index]['big']}'),
                                  ),
                                );
                              },
                              itemCount: product.selectedOtherProduct['images'].length,
                            ),
                          ),
                          AnimatedOpacity(
                            opacity: !value ? 1.0 : 0.0,
                            duration: Duration(milliseconds: 1000),
                            child: SafeArea(
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    GestureDetector(
                                      onTap: () => ExtendedNavigator.of(context).root.popUntilPath('/view-product'),
                                      child: Container(
                                        padding: EdgeInsets.all(5.0),
                                        decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black.withOpacity(0.3)),
                                        child: Icon(
                                          MdiIcons.arrowLeft,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                            padding: EdgeInsets.all(5.0),
                                            decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black.withOpacity(0.3)),
                                            child: Icon(
                                              MdiIcons.cartOutline,
                                              color: Colors.white,
                                            )),
                                        SizedBox(width: 20.0),
                                        Container(
                                            padding: EdgeInsets.all(5.0),
                                            decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black.withOpacity(0.3)),
                                            child: Icon(
                                              MdiIcons.dotsVertical,
                                              color: Colors.white,
                                            ))
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 16),
                    Container(
                        padding: EdgeInsets.all(10.0),
                        color: Colors.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Row(
                                children: <Widget>[
                                  AutoSizeText(
                                    'QR ${product.selectedOtherProduct['newPrice']}',
                                    style: TextStyle(fontSize: 18, color: Colors.redAccent),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    minFontSize: 16,
                                  ),
                                  SizedBox(width: 8.0),
                                  product.selectedOtherProduct['oldPrice'] != null
                                      ? Expanded(
                                          child: AutoSizeText(
                                            'QR ${product.selectedOtherProduct['oldPrice']}',
                                            style: TextStyle(fontSize: 18, color: Colors.grey, decoration: TextDecoration.lineThrough),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            minFontSize: 16,
                                          ),
                                        )
                                      : SizedBox(),
                                ],
                              ),
                            ),
                            SizedBox(height: 10.0),
                            AutoSizeText(
                              '${product.selectedOtherProduct['name']}',
                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              minFontSize: 16,
                            ),
                            SizedBox(height: 10.0),
                            AutoSizeText(
                              '${product.selectedOtherProduct['shortDescription']}',
                              style: TextStyle(fontSize: 16),
                              maxLines: 4,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 14,
                            ),
                            SizedBox(height: 10.0),
                            Row(
                              children: [
                                Icon(
                                  MdiIcons.checkboxBlankCircle,
                                  size: 16.0,
                                  color: product.selectedOtherProduct['availibilityCount'] > 0 ? Colors.green.withOpacity(0.9) : Colors.red.withOpacity(0.9),
                                ),
                                SizedBox(width: 8.0),
                                AutoSizeText(
                                  '${product.selectedOtherProduct['availibilityCount'] > 0 ? 'In' : 'Out'} stock, ${product.selectedOtherProduct['availibilityCount'] > 1 ? '${product.selectedOtherProduct['availibilityCount']} units' : '${product.selectedOtherProduct['availibilityCount']} unit'}',
                                  style: TextStyle(fontSize: 16, color: product.selectedOtherProduct['availibilityCount'] > 0 ? Colors.green.withOpacity(0.9) : Colors.red.withOpacity(0.9)),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  minFontSize: 16,
                                ),
                              ],
                            ),
                            SizedBox(height: 3.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    WRatingsWidget(product.selectedOtherProduct['ratingsValue'] / 100),
                                    SizedBox(width: 2),
                                    AutoSizeText(
                                      '(${product.selectedOtherProduct['ratingsCount']}) Reviews',
                                      style: TextStyle(fontSize: 16, color: Colors.grey.withOpacity(0.9)),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      minFontSize: 16,
                                    ),
                                  ],
                                ),
                                AutoSizeText(
                                  'Open Until : 11:59 PM',
                                  style: TextStyle(fontSize: 16, color: Colors.green.withOpacity(0.9)),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  minFontSize: 16,
                                ),
                              ],
                            ),
                            SizedBox(height: 3.0),
                          ],
                        )),
                    SizedBox(height: 10.0),
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          AutoSizeText(
                            'Promotions',
                            style: TextStyle(fontSize: 16),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            minFontSize: 16,
                          ),
                          Row(
                            children: [
                              AutoSizeText(
                                'Save more with wallet',
                                style: TextStyle(fontSize: 16),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                minFontSize: 16,
                              ),
                              Icon(MdiIcons.chevronRight)
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          AutoSizeText(
                            'Delivery',
                            style: TextStyle(fontSize: 16),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            minFontSize: 16,
                          ),
                          Row(
                            children: [
                              AutoSizeText(
                                'QR ${product.selectedOtherProduct['newPrice']}',
                                style: TextStyle(fontSize: 16),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                minFontSize: 16,
                              ),
                              Icon(MdiIcons.chevronRight)
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              AutoSizeText(
                                'Specifications',
                                style: TextStyle(fontSize: 16),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                minFontSize: 16,
                              ),
                              Icon(MdiIcons.chevronRight)
                            ],
                          ),
                          AutoSizeText(
                            'Brand, Model, Box Content',
                            style: TextStyle(fontSize: 14, color: Colors.grey),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            minFontSize: 14,
                          ),
                          Divider(color: Colors.grey),
                          AutoSizeText(
                            'Return & Warranty',
                            style: TextStyle(fontSize: 16),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            minFontSize: 16,
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                flex: 1,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      MdiIcons.subdirectoryArrowRight,
                                      size: 16,
                                    ),
                                    Expanded(
                                      child: AutoSizeText(
                                        '7 Days return to seller change of mind is not applicable',
                                        style: TextStyle(fontSize: 16),
                                        overflow: TextOverflow.ellipsis,
                                        minFontSize: 16,
                                        maxLines: 5,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Icon(
                                      MdiIcons.shieldOutline,
                                      size: 16,
                                    ),
                                    AutoSizeText(
                                      '2 years warranty',
                                      style: TextStyle(fontSize: 16),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      minFontSize: 16,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AutoSizeText(
                              'Ratings & Reviews (0)',
                              style: TextStyle(fontSize: 16),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: AutoSizeText(
                                  'This products has no reviews.',
                                  maxLines: 2,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 16, color: Colors.grey),
                                  overflow: TextOverflow.ellipsis,
                                  minFontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AutoSizeText(
                              'Questions about this Products (0)',
                              style: TextStyle(fontSize: 16),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: AutoSizeText(
                                  'There are no questions yet. Ask the seller now and their answer will show here.',
                                  maxLines: 2,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 16, color: Colors.grey),
                                  overflow: TextOverflow.ellipsis,
                                  minFontSize: 16,
                                ),
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                FlatButton(
                                  onPressed: () {},
                                  child: AutoSizeText('Ask Questions', style: TextStyle(color: Config.primaryColor)),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      color: Colors.white,
                      child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      WImageWidget(
                                        placeholder: AssetImage('assets/images/market/vendor.png'),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: AutoSizeText(
                                          'Seller seller',
                                          style: TextStyle(fontSize: 18),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          minFontSize: 18,
                                        ),
                                      ),
                                    ],
                                  ),
                                  FlatButton(
                                    onPressed: () {},
                                    child: AutoSizeText(
                                      'Follow',
                                      style: TextStyle(fontSize: 18, color: Config.primaryColor),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      minFontSize: 18,
                                    ),
                                  )
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: Column(
                                          children: [
                                            AutoSizeText(
                                              '92 %',
                                              style: TextStyle(fontSize: 16),
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              minFontSize: 16,
                                            ),
                                            AutoSizeText(
                                              'Positive Seller Ratings',
                                              style: TextStyle(fontSize: 16, color: Colors.grey),
                                              maxLines: 2,
                                              textAlign: TextAlign.center,
                                              overflow: TextOverflow.ellipsis,
                                              minFontSize: 16,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border(
                                            right: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                            left: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        child: Column(
                                          children: [
                                            AutoSizeText(
                                              '92 %',
                                              style: TextStyle(fontSize: 16),
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              minFontSize: 16,
                                            ),
                                            AutoSizeText(
                                              'Positive Seller Ratings',
                                              style: TextStyle(fontSize: 16, color: Colors.grey),
                                              textAlign: TextAlign.center,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              minFontSize: 16,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: Column(
                                          children: [
                                            AutoSizeText(
                                              '92 %',
                                              style: TextStyle(fontSize: 16),
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              minFontSize: 16,
                                            ),
                                            AutoSizeText(
                                              'Positive Seller Ratings',
                                              style: TextStyle(fontSize: 16, color: Colors.grey),
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              textAlign: TextAlign.center,
                                              minFontSize: 16,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    WRoundedButton(
                                      onCustomButtonPressed: () {},
                                      elevation: 0.0,
                                      child: AutoSizeText('Chat'),
                                    ),
                                    SizedBox(
                                      width: 8,
                                    ),
                                    WRoundedButton(
                                      onCustomButtonPressed: () {},
                                      elevation: 0.0,
                                      labelColor: Colors.white,
                                      btnColor: Config.primaryColor,
                                      child: AutoSizeText('Visit Store'),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          )),
                    ),
                    DefaultTabController(
                      length: 2,
                      child: Column(
                        children: [
                          TabBar(
                            isScrollable: true,
                            indicatorColor: Colors.transparent,
                            tabs: [
                              Tab(
                                  icon: Container(
                                      width: tabWidth,
                                      child: AutoSizeText(
                                        'Recommended for you',
                                        style: TextStyle(color: Colors.black),
                                      ))),
                              Tab(
                                  icon: AutoSizeText(
                                'Top Picks',
                                style: TextStyle(color: Colors.grey),
                              )),
                            ],
                          ),
                          Consumer<PMarketHome>(
                            builder: (context, market, child) {
                              return market.justForYouProducts.length > 0
                                  ? _productCard(context, market.justForYouProducts)
                                  : Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Center(child: CircularProgressIndicator()),
                                    );
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AutoSizeText(
                              'Highlights',
                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                            Divider(
                              thickness: 1,
                            ),
                            AutoSizeText(
                              'This is highlights',
                              style: TextStyle(fontSize: 16),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Consumer<PMarketHome>(
                      builder: (context, market, child) {
                        return market.justForYouProducts.length > 0
                            ? _productCard(context, market.justForYouProducts)
                            : Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Center(child: CircularProgressIndicator()),
                              );
                      },
                    ),
                  ],
                ),
                AnimatedOpacity(
                  opacity: value ? 1.0 : 0.0,
                  duration: Duration(milliseconds: 500),
                  child: Container(
                    height: 80,
                    color: Config.primaryColor,
                    child: SafeArea(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () => ExtendedNavigator.of(context).root.popUntilPath('/view-product'),
                              child: Container(
                                padding: EdgeInsets.all(5.0),
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black.withOpacity(0.3)),
                                child: Icon(
                                  MdiIcons.arrowLeft,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            Row(
                              children: [
                                Container(
                                    padding: EdgeInsets.all(5.0),
                                    decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black.withOpacity(0.3)),
                                    child: Icon(
                                      MdiIcons.cartOutline,
                                      color: Colors.white,
                                    )),
                                SizedBox(width: 20.0),
                                Container(
                                    padding: EdgeInsets.all(5.0),
                                    decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black.withOpacity(0.3)),
                                    child: Icon(
                                      MdiIcons.dotsVertical,
                                      color: Colors.white,
                                    ))
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            );
          }),
          bottomNavigationBar: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              height: 60,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          height: 40,
                          width: 60,
                          child: FlatButton(
                            onPressed: () {},
                            child: Center(
                              child: Icon(
                                MdiIcons.storefrontOutline,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 8.0),
                          child: Container(
                            color: Colors.grey,
                            width: 2,
                          ),
                        ),
                        SizedBox(
                          height: 40,
                          width: 60,
                          child: FlatButton(
                            onPressed: () {},
                            child: Center(
                              child: Icon(
                                MdiIcons.messageProcessingOutline,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        WRoundedButton(
                          onCustomButtonPressed: () {},
                          child: Text('Buy Now'),
                          btnColor: Config.primaryColor,
                          labelColor: Colors.white,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        WRoundedButton(
                          onCustomButtonPressed: () {},
                          child: Text('Add to Cart'),
                          btnColor: Config.primaryColor,
                          labelColor: Colors.white,
                        )
                      ],
                    ),
                  )
                ],
              )),
        ),
      ),
    );
  }
}
