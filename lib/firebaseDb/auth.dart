import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hive/hive.dart';
import 'package:wblue_customer/firebaseDb/firestoreDB.dart';

final FirebaseFirestore _db = FirebaseFirestore.instance;

Future<void> deleteMyAccount({username}) async {
  var box = await Hive.openBox('auth');

  await _db.collection(FireStoreClass.userCollection).doc(username).delete();

  await box.put('isLogin', false);
}
