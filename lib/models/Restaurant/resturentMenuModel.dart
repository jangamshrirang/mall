import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();

class Foodmenu {
  Foodmenu({
    this.main,
    this.deserts,
  });

  List<Main> main;
  List<Desert> deserts;

  factory Foodmenu.fromJson(Map<String, dynamic> json) => Foodmenu(
        main: List<Main>.from(json["Main"].map((x) => Main.fromJson(x))),
        deserts:
            List<Desert>.from(json["Deserts"].map((x) => Desert.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Main": List<dynamic>.from(main.map((x) => x.toJson())),
        "Deserts": List<dynamic>.from(deserts.map((x) => x.toJson())),
      };
}

class Desert {
  Desert({
    this.id,
    this.email,
    this.firstName,
    this.lastName,
    this.avatar,
  });

  int id;
  String email;
  String firstName;
  String lastName;
  String avatar;

  factory Desert.fromJson(Map<String, dynamic> json) => Desert(
        id: json["id"],
        email: json["email"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        avatar: json["avatar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "email": email,
        "first_name": firstName,
        "last_name": lastName,
        "avatar": avatar,
      };
}

class Main {
  Main({
    this.image,
    this.price,
    this.name,
  });

  String image;
  String price;
  String name;

  factory Main.fromJson(Map<String, dynamic> json) => Main(
        image: json["image"],
        price: json["price"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "image": image,
        "price": price,
        "name": name,
      };
}

class RestaurantsModel {
  RestaurantsModel({
    this.coverPicture,
    this.profilePicture,
    this.name,
    this.slogan,
    this.schedule,
    this.status,
  });

  String coverPicture;
  String profilePicture;
  String name;
  String slogan;
  String schedule;
  String status;

  factory RestaurantsModel.fromJson(Map<String, dynamic> json) =>
      RestaurantsModel(
        coverPicture: json["cover_picture"],
        profilePicture: json["profile_picture"],
        name: json["name"],
        slogan: json["slogan"],
        schedule: json["schedule"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "cover_picture": coverPicture,
        "profile_picture": profilePicture,
        "name": name,
        "slogan": slogan,
        "schedule": schedule,
        "status": status,
      };

  Future<List> fetchRestaurants() async {
    APIResponse res = await _dioClient.publicPost('/store-advertisement-list');

    if (res.code >= 400) {
      return [];
    }

    return res.data;
  }
}
