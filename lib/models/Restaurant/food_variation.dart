import 'package:quiver/core.dart';

class RestaurantFoodVariationModel {
  RestaurantFoodVariationModel(
      {this.hashid,
      this.name,
      this.isRequired,
      this.isMultiple,
      this.selectionMaxNumber,
      this.variationTypeSelections,
      this.originalPrice});

  String hashid;
  String name;
  int isRequired;
  int isMultiple;
  int originalPrice;
  int selectionMaxNumber;

  List<VariationTypeSelection> variationTypeSelections;

  factory RestaurantFoodVariationModel.fromJson(Map<String, dynamic> json) => RestaurantFoodVariationModel(
        hashid: json["hashid"] == null ? null : json["hashid"],
        name: json["name"] == null ? null : json["name"],
        isRequired: json["is_required"] == null ? null : json["is_required"],
        isMultiple: json["is_multiple"] == null ? null : json["is_multiple"],
        originalPrice: json["original_price"] == null ? 0 : json["original_price"],
        selectionMaxNumber: json["selection_max_number"] == null ? null : json["selection_max_number"],
        variationTypeSelections: json["variation_type_selections"] == null
            ? null
            : List<VariationTypeSelection>.from(
                json["variation_type_selections"].map((x) => VariationTypeSelection.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "hashid": hashid == null ? null : hashid,
        "name": name == null ? null : name,
        "is_required": isRequired == null ? null : isRequired,
        "is_multiple": isMultiple == null ? null : isMultiple,
        "selection_max_number": selectionMaxNumber == null ? null : selectionMaxNumber,
        "variation_type_selections":
            variationTypeSelections == null ? null : List<dynamic>.from(variationTypeSelections.map((x) => x.toJson())),
      };
}

class VariationTypeSelection {
  VariationTypeSelection({
    this.hashid,
    this.name,
    this.price,
    this.isAddedPrice,
    this.isSelected: false,
  });

  String hashid;
  String name;
  String price;
  bool isSelected;
  int isAddedPrice;

  factory VariationTypeSelection.fromJson(Map<String, dynamic> json) => VariationTypeSelection(
        hashid: json["hashid"] == null ? null : json["hashid"],
        name: json["name"] == null ? null : json["name"],
        price: json["price"] == null ? null : json["price"],
        isSelected: json["is_selected"] == null ? false : json["is_selected"],
        isAddedPrice: json["is_added_price"] == null ? null : json["is_added_price"],
      );

  Map<String, dynamic> toJson() => {
        "hashid": hashid == null ? null : hashid,
        "name": name == null ? null : name,
        "price": price == null ? null : price,
        "is_selected": isSelected == null ? null : isSelected,
        "is_added_price": isAddedPrice == null ? null : isAddedPrice,
      };
}
