import 'package:flutter/material.dart';
import 'package:wblue_customer/models/Reservations/reservation_model.dart';
import 'package:wblue_customer/services/helper.dart';

import 'p_food_cart.dart';

class PQRReserevation extends ChangeNotifier {
  PRestaurantFoodCart pRestaurantFoodCart = PRestaurantFoodCart();
  bool _isScanned = false;
  bool get isScanned => _isScanned;
  bool _canBack = false;

  RestaurentReservationModel _restaurentReservationModel;
  RestaurentReservationModel get restaurentReservationModel => _restaurentReservationModel;

  bool get canBack => _canBack;

  Future<void> whenScan(Map data) async {
    if (data['reservation_qrCode'] == _restaurentReservationModel.qrCode) _isScanned = true;
    notifyListeners();
  }

  void setScanStatus(bool status) {
    _isScanned = status;
    notifyListeners();
  }

  void setBackStatus(bool status) {
    _canBack = status;
  }

  void setReservation(RestaurentReservationModel restaurentReservationModel) {
    _restaurentReservationModel = restaurentReservationModel;
  }
}
