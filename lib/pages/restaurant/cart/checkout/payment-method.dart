import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RestaurantCheckoutPaymentPage extends StatefulWidget {
  @override
  _RestaurantCheckoutPaymentPageState createState() => _RestaurantCheckoutPaymentPageState();
}

List paymentMethods = [
  {'name': 'Credit / Debit Card', 'image': 'assets/images/payment-method/card.png', 'hashOption': true},
  {'name': 'Paypal', 'image': 'assets/images/payment-method/paypal.png', 'hashOption': false},
  {'name': 'Cash on Delivery', 'image': 'assets/images/payment-method/cash.png', 'hashOption': false}
];

int _groupValue = 2;

class _RestaurantCheckoutPaymentPageState extends State<RestaurantCheckoutPaymentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Config.primaryColor,
        title: Text('Select a payment method'),
      ),
      body: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                padding: EdgeInsets.all(16),
                color: Config.primaryColor.withAlpha(16),
                child: Text(
                  'Only cash on delivery is available',
                  style: TextStyle(color: Colors.redAccent, fontWeight: FontWeight.w500, fontSize: 32.sp),
                ),
              ),
            ],
          ),
          Container(
              padding: EdgeInsets.all(26.0),
              child: Column(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: List.generate(
                      paymentMethods.length,
                      (index) => method(
                        paymentMethods[index]['image'],
                        paymentMethods[index]['name'],
                        index,
                        hashOption: paymentMethods[index]['hashOption'],
                      ),
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }

  Widget method(String image, String title, int index, {bool hashOption: false}) {
    return hashOption
        ? ListTile(
            trailing: Icon(MdiIcons.chevronRight),
            hoverColor: Colors.red,
            onTap: null,
            title: Row(
              children: [
                SizedBox(height: 0.15.sw, width: 0.15.sw, child: Image.asset(image)),
                SizedBox(width: 0.05.sw),
                Text('$title', style: TextStyle(fontSize: 36.sp, fontWeight: FontWeight.w500, color: Colors.grey)),
              ],
            ),
          )
        : RadioListTile(
            value: index,

            //FIXME: not dynamic
            groupValue: _groupValue,
            onChanged: index != 2
                ? null
                : (int val) {
                    setState(() => _groupValue = val);
                  },
            selected: false,
            activeColor: Config.primaryColor,
            controlAffinity: ListTileControlAffinity.trailing,
            title: Row(
              children: [
                SizedBox(height: 0.15.sw, width: 0.15.sw, child: Image.asset(image)),
                SizedBox(width: 0.05.sw),
                Text('$title', style: TextStyle(fontSize: 36.sp, fontWeight: FontWeight.w500)),
              ],
            ),
          );
  }
}
