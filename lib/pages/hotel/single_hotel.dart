import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wblue_customer/widgets/w_about_property.dart';
import 'package:wblue_customer/widgets/w_checking_dates.dart';
import 'package:wblue_customer/widgets/w_hotel_profile_header.dart';
import 'package:wblue_customer/widgets/w_room_type.dart';

class SingleHotelPage extends StatefulWidget {
  @override
  _SingleHotelPageState createState() => _SingleHotelPageState();
}

class _SingleHotelPageState extends State<SingleHotelPage> {
  LatLng location = LatLng(37.43296265331129, -122.08832357078792);
  String dummyText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum facilisis commodo congue. Etiam quis feugiat metus. Suspendisse sollicitudin tincidunt nisl, vitae scelerisque magna dapibus at. Mauris ornare in odio eu efficitur. Duis tempus lorem in rutrum varius. Morbi convallis nulla at ligula ultrices luctus. Aenean vitae porta nisi. Pellentesque sed dui eget est lobortis ultrices vitae nec libero. Aliquam consectetur augue vitae lectus venenatis dapibus. Nam vel risus eget est elementum vulputate. Integer vulputate velit quis quam iaculis eleifend. Maecenas id magna dolor. Donec accumsan elit ac aliquam tincidunt. Nulla cursus risus dui, at interdum felis maximus vel.';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      extendBody: true,
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              WHotelProfileHeaderWidget(),
              WAboutPropertyWidget(latLng: location, about: dummyText),
              Divider(),
              _yourStayDates(),
              Divider(),
              _amenities(),
              Divider(),
              _serviceGuarantee(),
              Divider(),
              _availableRoomType(),
              Divider(),
              _policies(),
              SizedBox(height: 18.0),
            ],
          ),
        ),
      ),
    );
  }

  Widget _yourStayDates() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Your Stay Dates',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          SizedBox(height: 8.0),
          WCheckingDatesWidget(),
        ],
      ),
    );
  }

  Widget __icon(String assetName, String label) {
    return Column(
      children: <Widget>[
        Container(
          height: 50.0,
          width: 50.0,
          padding: EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            color: Colors.black54,
            borderRadius: BorderRadius.circular(50.0),
          ),
          child: SvgPicture.asset(
            'assets/svg/$assetName.svg',
            semanticsLabel: label,
            color: Colors.amber,
          ),
        ),
        SizedBox(height: 4.0),
        Container(
          child: Text(
            label,
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }

  Widget _amenities() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Amenities',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          SizedBox(height: 8.0),
          Container(
            height: 80.0,
            child: ListView(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemExtent: 70.0,
              children: <Widget>[
                __icon('hotel-cart', 'Cart'),
                __icon('car-parking', 'Parking'),
                __icon('reception', 'Reception'),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _serviceGuarantee() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Service Guarantee',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          SizedBox(height: 8.0),
          Container(
            height: 90.0,
            child: ListView(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemExtent: 70.0,
              children: <Widget>[
                __icon('free-wifi', 'Free WIFI'),
                __icon('tv', 'Sattelite Television'),
                __icon('water', 'Mineral Water'),
                __icon('bed', 'Clean Linen'),
                __icon('bathtub', 'Clean Washrooms'),
                __icon('toiletries', 'Toiletries'),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _availableRoomType() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                'Available Room Type',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          SizedBox(height: 8.0),
          WRoomTypeWidget(name: 'Twin Room', left: 10, type: 'Twin Bed', newPrice: 692.10, oldPrice: 769, canPayAtHotel: true, isRefundable: true),
          WRoomTypeWidget(name: 'Deluxe Room', left: 4, type: 'King Bed', newPrice: 890.10, oldPrice: 989),
          WRoomTypeWidget(name: 'Triple Room', left: 2, type: 'King Bed', newPrice: 1088.10, oldPrice: 1209),
        ],
      ),
    );
  }

  Widget _policies() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                'Policies',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          ListView(
            shrinkWrap: true,
            children: <Widget>[
              Text('• Check In from 2pm to 4pm on the start date of booking.'),
              Text('• Check Out before 12PM'),
            ],
          ),
        ],
      ),
    );
  }
}
