import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/services/global.dart';
import 'package:wblue_customer/services/helper.dart';

class FireBasePushNotification extends StatefulWidget {
  @override
  _FireBasePushNotificationState createState() =>
      _FireBasePushNotificationState();
}

class _FireBasePushNotificationState extends State<FireBasePushNotification> {
  FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final FirebaseMessaging _fcm = FirebaseMessaging();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    messagesList = List<Message>();
    _configureFirebaseListeners();
    if (Platform.isIOS) {
            _saveDeviceToken();
      _fcm.requestNotificationPermissions(IosNotificationSettings(
          sound: true, alert: true, badge: true, provisional: false));

    } else {
      _saveDeviceToken();
    }
  }

  _saveDeviceToken() async {
    // Get the current user
    // FirebaseUser user = await _auth.currentUser();

    // Get the token for this device
    String fcmToken = await _fcm.getToken();

    if (fcmToken != null) {
      var user = Helper.currentAuthUser.get('user');
      var tokens = _db
          .collection('users')
          .doc(user['hashid'])
          .collection('tokens')
          .doc(fcmToken);

      await tokens.set({
        'token': fcmToken,
        'createdAt': FieldValue.serverTimestamp(), // optional
        'platform': Platform.operatingSystem // optional
      });

      StatesNames.firebaseNotificationToken = fcmToken;
    }
  }

  void _sendNotification() async {
    final databaseReference = FirebaseFirestore.instance;
    await databaseReference.collection("Messages").add({
      'message': 'dmmy test 3',
      'receriver':
          '8eZ9aEj4GgJoYmwQ0pPr7Wv5', // mi device  yBYhDR1dO14Q9MRf25MT  // vivo device  uvZZ3MoHDsMVgRNL7lqW  //
      'sender': "dummy 3 "
    });
  }

  List<Message> messagesList;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          _sendNotification();
        },
        label: Text('send message'),
        icon: Icon(Icons.chat),
        backgroundColor: Colors.pink,
      ),
      appBar: AppBar(
        title: Text("pushNotification testing"),
      ),
      body:
          // Container()
          ListView.builder(
        itemCount: null == messagesList ? 0 : messagesList.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Text(
                messagesList[index].message,
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.black,
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  _configureFirebaseListeners() {
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('onMessage: $message');
        _setMessage(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('onLaunch: $message');
        _setMessage(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print('onResume: $message');
        _setMessage(message);
      },
    );
    firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(sound: true, badge: true, alert: true),
    );
  }

  _setMessage(Map<String, dynamic> message) {
    final notification = message['notification'];
    final data = message['data'];
    final String title = notification['title'];
    final String body = notification['body'];
    String mMessage = data['message'];
    print("nofication data Title: $title, body: $body, message: $mMessage");
    setState(() {
      Message msg = Message(title, body, mMessage);
      messagesList.add(msg);
    });
  }
}

class Message {
  String title;
  String body;
  String message;
  Message(title, body, message) {
    this.title = title;
    this.body = body;
    this.message = message;
  }
}

// import * as functions from 'firebase-functions';
// import * as admin from 'firebase-admin';

// admin.initializeApp();

// const db = admin.firestore();
// const fcm = admin.messaging();

// export const sendToDevice = functions.firestore
//   .document('Messages/{messageId}')
//   .onCreate(async snapshot => {

//     const user = snapshot.data();

//     const querySnapshot = await db
//       .collection('users')
//       .doc(user.receiver)
//       .collection('tokens')
//       .get();

//     const tokens = querySnapshot.docs.map(snap => {

//       console.log('======== from '+ user.sender +' ============= to ' + user.receiver + ' =============== : ' + snap.id);
//       return snap.id
//     });

//     if(tokens.length <= 0){
//       return false;
//     }

//     const payload= {
//       notification: {
//         title: 'WLive',
//         body: 'You have new message',
//         click_action: 'FLUTTER_NOTIFICATION_CLICK',
//         sound: 'default',
//       },
//       data: {
//         click_action: "FLUTTER_NOTIFICATION_CLICK",
//         receiver : user.receiver,
//         sender : user.sender,
//       },
//     };

//     const options = {
//       priority: 'high',
//     };

//     return fcm.sendToDevice(tokens, payload, options);
//   });

// const functions = require('firebase-functions');
// const admin = require('firebase-admin');

// admin.initializeApp(functions.config().functions);

// var newData;

// exports.myTrigger = functions.firestore.document('Messages/{messageId}').onCreate(async (snapshot, context) => {
//     if (snapshot.empty) {
//         console.log('No Devices');
//         return;
//     }
//     var tokens = [];
//     const deviceTokens = await admin
//     .firestore()
//     .collection('DeviceTokens')
//     .get();
//     newData = snapshot.data();
//     for (var token of deviceTokens.docs) {
//         tokens.push(token.data().device_token);
//     }
//     var payload = {
//         notification: { title: "W Mall", body: 'You got a notification ' }, data: { click_action: "FLUTTER_NOTIFICATION_CLICK", message: newData.message },
//     };

//     try {
//         const response = await admin.messaging().sendToDevice(tokens, payload);
//         console.log('Notification sent successfully');
//     } catch (err) {
//         console.log(err);
//     }
// });
