import 'dart:ui';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/widgets/w_image.dart';

class WMovieHeaderWidget extends StatelessWidget {
  const WMovieHeaderWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height * 0.45;

    return Stack(
      children: <Widget>[
        Container(
          height: height,
          width: double.infinity,
          child: WImageWidget(
            url: '',
            placeholder: ExactAssetImage('assets/images/samples/movie.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5.1, sigmaY: 5.1),
          child: Container(
            height: height,
            color: Colors.black.withOpacity(0.0),
          ),
        ),
        InkWell(
          onTap: () {
            ExtendedNavigator.of(context).root.push('/movie-fullscreen-image');
          },
          child: Hero(
            tag: 'movie-image',
            child: Container(
              height: height,
              width: double.infinity,
              child: WImageWidget(
                url: '',
                placeholder: ExactAssetImage('assets/images/samples/movie.jpg'),
                fit: BoxFit.scaleDown,
              ),
            ),
          ),
        ),
        SafeArea(
          child: Row(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.arrow_back),
                color: Colors.amber,
                onPressed: () {
                  ExtendedNavigator.of(context).pop();
                },
              ),
            ],
          ),
        ),
        Positioned.fill(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              color: Colors.black38,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        'Movie Title',
                        style: TextStyle(color: Colors.white, fontSize: 24.0, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {},
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      color: Colors.amber,
                      child: Text('Buy Ticket'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
