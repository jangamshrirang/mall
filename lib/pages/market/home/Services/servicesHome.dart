import 'package:flutter/material.dart';
import 'package:wblue_customer/pages/market/account/Wallet/AppbarForAllWalwtScreens.dart';
import 'package:wblue_customer/pages/market/account/Wallet/wallet.dart';
import 'package:wblue_customer/pages/market/home/Services/vendorExpantion.dart';

import 'ServicesCart.dart';

class ServicesHome extends StatefulWidget {
  @override
  _ServicesHomeState createState() => _ServicesHomeState();
}

class _ServicesHomeState extends State<ServicesHome> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: appBarWithSearchIcons(context, "Services", () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ServicesCart()));
        }),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(children: [
            Container(
              height: size.height * 0.12,
              width: size.width,
              color: Colors.white,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  categoryContainer('assets/services/massage.png',
                      Color(0xffBFDFA8), "Spa Service"),
                  categoryContainer('assets/services/watch.png',
                      Color(0xffDFCDA8), "Watch Repair"),
                  categoryContainer('assets/services/car.png',
                      Color(0xffAEA8DF), "Car Repair"),
                  categoryContainer('assets/services/massage.png',
                      Color(0xffBFDFA8), "Spa Service"),
                  categoryContainer('assets/services/watch.png',
                      Color(0xffDFCDA8), "Watch Repair"),
                  categoryContainer('assets/services/car.png',
                      Color(0xffAEA8DF), "Car Repair"),
                ],
              ),
            ),
            listItemCard(),
            listItemCard(),
            listItemCard(),
            listItemCard(),
          ]),
        ));
  }

  categoryContainer(image, Color color, String text) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(
        right: 10,
        left: 5,
      ),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.4),
            spreadRadius: 1,
            blurRadius: 3,
            offset: Offset(1, 3), // changes position of shadow
          ),
        ],
        color: color,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        children: [
          Container(
            //text container
            margin: EdgeInsets.only(top: 2),
            width: size.width * 0.3,
            height: size.height * 0.02,
            child: Text(
              text,
              maxLines: 1,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                  fontSize: size.height * 0.02),
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 2),
              height: size.height * 0.07,
              child: Image.asset(image))
        ],
      ),
    );
  }

  listItemCard() {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => ServiceVendorExpantion()));
      },
      child: Container(
          width: size.width * 0.9,
          height: size.height * 0.22,
          margin: EdgeInsets.only(bottom: 10, left: 10, top: 5, right: 5),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 1,
                blurRadius: 3,
                offset: Offset(1, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Container(
              //text container
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
      
              padding: EdgeInsets.only(
                  left: size.width * 0.02,
                  right: size.width * 0.01,
                  top: size.height * 0.01),
              height: size.height * 0.06,
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        border: Border.all(color: Colors.black26)),
                    child: Image.asset(
                      'assets/services/downloa.png',
                      height: size.height * 0.14,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [                     
                      text("Mark Spa", size.height * 0.023, Colors.black, 1),
                      text(
                          "Address: Al Kalidiy St,Bulding No: 20 ,Office No:68, Doha, Qatar.",
                          size.height * 0.020,
                          Colors.black38,
                          2),
                      text("QAR 500 - QAR 1500 ", size.height * 0.021,
                          Colors.red, 1),
                      text("OPEN |4PM - 12MIDNIGHT ", size.height * 0.021,
                          Colors.green, 1),
                      starRatingIcon()
                    ],
                  ),
                ],
              ))),
    );
  }

  text(String text, double height, Color color, int lines) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.5,
      margin: EdgeInsets.only(left: 10, top: 4),
      child: Text(
        text,
        maxLines: lines,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.normal,
            color: color,
            fontSize: height),
      ),
    );
  }

  starRatingIcon() {
    var size = MediaQuery.of(context).size;
    return Row(
      children: [
        Container(
          height: size.height * 0.023,
          width: size.width * 0.123,
          margin: EdgeInsets.only(left: 10, top: 4),
          decoration: BoxDecoration(
            color: Colors.amber,
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          child: Row(
            children: [
              Spacer(),
              Icon(Icons.star, color: Colors.white, size: 14),
              Text(
                " 3.5",
                style: TextStyle(color: Colors.white),
              ),
              Spacer(),
            ],
          ),
        ),
        Container(
            margin: EdgeInsets.only(top: 4),
            child: Text(
              "  15 Reviwes",
              style: TextStyle(color: Colors.black38),
            )),
      ],
    );
  }
}
