import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WArrivalSelectionWidget extends StatefulWidget {
  final Function(int) onSelected;

  WArrivalSelectionWidget({
    this.onSelected: functionArgs,
  });

  @override
  _WArrivalSelectionWidgetState createState() => _WArrivalSelectionWidgetState();
}

class _WArrivalSelectionWidgetState extends State<WArrivalSelectionWidget> {
  int index;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(bottom: 16.0),
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Text('SELECT ARRIVAL TIME'),
            SizedBox(height: 16.0),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: _item(0, text: 'Before 2:00 PM', onTap: () {
                    setState(() {
                      index = 0;
                    });
                  }),
                ),
                SizedBox(width: 8.0),
                Expanded(
                  flex: 5,
                  child: _item(1, text: '2:00 PM to 5:00 PM', onTap: () {
                    setState(() {
                      index = 1;
                    });
                  }),
                ),
              ],
            ),
            SizedBox(height: 8.0),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: _item(2, text: '5:00 PM to 8:00 PM', onTap: () {
                    setState(() {
                      index = 2;
                    });
                  }),
                ),
                SizedBox(width: 8.0),
                Expanded(
                  flex: 5,
                  child: _item(3, text: '8:00 PM to 10:00 PM', onTap: () {
                    setState(() {
                      index = 3;
                    });
                  }),
                ),
              ],
            ),
            SizedBox(height: 8.0),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: _item(4, text: 'After 10:00 PM', onTap: () {
                    setState(() {
                      index = 4;
                    });
                  }),
                ),
                SizedBox(width: 8.0),
                Expanded(
                  flex: 5,
                  child: SizedBox(),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _item(int $index, {String text: '', Function onTap}) {
    return InkWell(
      onTap: () {
        onTap();
        widget.onSelected($index);
      },
      child: Container(
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          border: Border.all(),
          borderRadius: BorderRadius.circular(4.0),
          color: $index == index ? Colors.amber : Colors.transparent,
        ),
        child: Text(
          text,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}

void functionArgs(dynamic) {}
