// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:stripe_payment/stripe_payment.dart';
// import '../env/config.example.dart';
// import 'package:wblue_customer/services/global.dart';

// class WPaymentWidget extends StatefulWidget {
//   final Function onConfirmTap;
//   final String itemName;
//   final double amount;

//   WPaymentWidget({
//     this.onConfirmTap: function,
//     this.itemName: '',
//     this.amount: 0.0,
//   });

//   @override
//   _WPaymentWidgetState createState() => _WPaymentWidgetState();
// }

// class _WPaymentWidgetState extends State<WPaymentWidget> {
//   Token paymentToken;

//   @override
//   void initState() {
//     super.initState();
//     StripePayment.setOptions(StripeOptions(publishableKey: Config.testStripe['key'], merchantId: 'Test', androidPayMode: 'test'));
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Card(
//       child: Container(
//         width: double.infinity,
//         padding: EdgeInsets.all(16.0),
//         child: Column(
//           children: <Widget>[
//             FlatButton(
//               child: Row(
//                 mainAxisSize: MainAxisSize.min,
//                 children: <Widget>[
//                   Icon(Icons.credit_card),
//                   SizedBox(width: 4.0),
//                   Text('Select Credit/Debit Card'),
//                 ],
//               ),
//               onPressed: () {
//                 _nativePay();
//               },
//             ),
//             SizedBox(height: 16.0),
//             paymentToken == null
//                 ? SizedBox()
//                 : Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceAround,
//                     children: <Widget>[
//                       Icon(Icons.credit_card),
//                       SizedBox(width: 8.0),
//                       Expanded(
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: <Widget>[
//                             Text(
//                               paymentToken.card.name ?? '',
//                               style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
//                             ),
//                             Text('${paymentToken.card.brand.toUpperCase()} •••• •••• •••• ${paymentToken.card.last4}'),
//                           ],
//                         ),
//                       ),
//                       Text('${paymentToken.card.expMonth}/${paymentToken.card.expYear}'),
//                     ],
//                   ),
//             SizedBox(height: 16.0),
//             Divider(),
//             FlatButton(
//               child: Row(
//                 mainAxisSize: MainAxisSize.min,
//                 children: <Widget>[
//                   Icon(Icons.check_circle),
//                   SizedBox(width: 4.0),
//                   Text('Confirm Payment'),
//                 ],
//               ),
//               color: Colors.amber,
//               onPressed: paymentToken == null
//                   ? null
//                   : () {
//                       _completeNativePay();
//                     },
//             ),
// //            CreditCardWidget(
// //              cardNumber: '4644 2122 5043 9390',
// //              expiryDate: '10/25',
// //              cvvCode: '123',
// //              cardHolderName: 'Jane Doe',
// //              showBackView: false,
// //              height: 170.0,
// //              width: 300.0,
// //            ),
//           ],
//         ),
//       ),
//     );
//   }

//   void _createSourceWithParams() {
//     StripePayment.createSourceWithParams(SourceParams(
//       type: 'ideal',
//       amount: 1099,
//       currency: 'eur',
//       returnURL: 'example://stripe-redirect',
//     )).then((source) {
//       print(source.toJson());
//     }).catchError((error) {
//       print(error);
//     });
//   }

//   void _createTokenWithCardForm() {
//     StripePayment.paymentRequestWithCardForm(CardFormPaymentRequest(requiredBillingAddressFields: 'Billing Address')).then((paymentMethod) {
//       print(paymentMethod.toJson());
//     }).catchError((error) => print(error));
//   }

//   void _createTokenWithCard() {
//     final CreditCard testCard = CreditCard(
//       number: '4000002760003184',
//       expMonth: 12,
//       expYear: 21,
//     );

//     StripePayment.createTokenWithCard(testCard).then((token) {
//       print(token.toJson());
//     }).catchError((error) => print(error));
//   }

//   void _createPaymentMethodWithCard() {
//     final CreditCard testCard = CreditCard(
//       number: '4111111111111111',
//       expMonth: 10,
//       expYear: 25,
//     );

//     StripePayment.createPaymentMethod(PaymentMethodRequest(card: testCard)).then((paymentMethod) {
//       print(paymentMethod.toJson());
//     }).catchError((error) => print(error));
//   }

//   void _nativePay() {
//     StripePayment.paymentRequestWithNativePay(
//       androidPayOptions: AndroidPayPaymentRequest(totalPrice: Global.money(widget.amount, symbol: ''), currencyCode: 'QAR'),
//       applePayOptions: ApplePayPaymentOptions(
//         countryCode: 'QA',
//         currencyCode: 'QAR',
//         items: [
//           ApplePayItem(label: widget.itemName, amount: Global.money(widget.amount, symbol: '')),
//         ],
//       ),
//     ).then((token) {
//       setState(() {
//         paymentToken = token;
//         print(token.toJson());
//       });
//     }).catchError((error) => print(error));
//   }

//   void _completeNativePay() {
//     StripePayment.completeNativePayRequest().then((_) {
//       widget.onConfirmTap();
//     }).catchError((error) => print(error));
//   }
// }

// void function() {}
