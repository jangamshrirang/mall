import 'package:clippy_flutter/trapezoid.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:wblue_customer/env/config.dart';

import 'B_Exp_TabBAr.dart';
import 'B_Expantion.dart';

class BloggersScreen extends StatefulWidget {
  @override
  _BloggersScreenState createState() => _BloggersScreenState();
}

class _BloggersScreenState extends State<BloggersScreen> {
  final controller = PageController(viewportFraction: 1.0);
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(left: size.width * 0.03, top: 5),
        width: size.width * 0.95,
        height: size.height * 0.5,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Container(
                      height: size.height * 0.18,
                      width: size.width * 0.5,
                      child: PageView(
                        controller: controller,
                        children: <Widget>[
                          bloggerImages(
                              'assets/botique/b1.png'),
                          bloggerImages(
                              'assets/botique/b2.png'),
                          bloggerImages(
                              'assets/botique/b3.png'),
                          bloggerImages(
                              'assets/botique/b1.png'),
                        ],
                        pageSnapping: true,
                      ),
                    ),
                    SizedBox(
                      height: size.height * 0.01,
                    ),
                    Container(
                      child: SmoothPageIndicator(
                        controller: controller,
                        count: 4,
                        effect: ExpandingDotsEffect(
                          activeDotColor: Colors.black38,
                          dotHeight: 05.0,
                          expansionFactor: 2,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: size.height * 0.01,
                    ),
                    text(
                      "QQQ",
                      Colors.red,
                    ),
                    text(
                      "Blogger",
                      Colors.black,
                    ),
                    richText(
                        " Doha ",
                        " Qatar",
                        'assets/botique/location.png',
                        Colors.black45,
                        Colors.black45),
                    richText(
                        " 598 ",
                        "Products",
                        'assets/botique/products.png',
                        Colors.black,
                        Colors.black45),
                    richText(
                        " 446K ",
                        "Followers",
                        'assets/botique/follwers.png',
                        Colors.black,
                        Colors.black45),
                    SizedBox(height: size.height * 0.02),
                    Row(
                      children: [
                        button("Follow", size.height * 0.03, size.width * 0.2,
                            () {}),
                        iocn(Icons.favorite_border),
                        iocn(Icons.bookmark_border),
                        iocn(Icons.bubble_chart),
                      ],
                    ),
                  ],
                ), //divider

                Container(
                  margin: EdgeInsets.only(left: 3),
                  width: 1,
                  height: size.height * 0.35,
                  color: Colors.black12,
                ), //second half
                Container(
                  width: size.width * 0.4,
                  height: size.height * 0.43,
                  //color: Colors.red,
                  child: Wrap(
                    children: [
                      bloggerImages('assets/botique/p1.png'),
                      bloggerImages('assets/botique/p2.png'),
                      bloggerImages('assets/botique/p3.png'),
                      bloggerImages('assets/botique/p4.png'),
                      bloggerImages('assets/botique/p5.png'),
                      bloggerImages('assets/botique/p6.png'),
                      wrapText(" Jewelry "),
                      wrapText("  Electronics  "),
                      wrapText(" Shoes "),
                      wrapText(" More... "),
                      Container(
                        margin: EdgeInsets.only(
                            left: size.width * 0.05, top: size.height * 0.03),
                        child: button(
                            "View All", size.height * 0.03, size.width * 0.3,
                            () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      B_Exp_TabBar())); //
                        }),
                      )
                    ],
                  ),
                )
                // Container(
                //   height: size.,
                //   child: GridView.count(
                //     childAspectRatio: 4.0,
                //     crossAxisCount: 2,

                //     padding: EdgeInsets.all(5.0),
                //     children: <Widget>[

                //     ],
                //     shrinkWrap: true,
                //     // todo comment this out and check the result
                //     physics: ClampingScrollPhysics(),
                //   ),
                // )
              ],
            ),
          ],
        ),
      ),
    );
  }

  bloggerImages(String image) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(
        left: size.width * 0.02,
        top: size.height * 0.01,
      ),
      height: size.height * 0.08,
      width: size.width * 0.18,
      decoration: BoxDecoration(
        color: Colors.transparent,
        image: DecorationImage(
            image: AssetImage(
              image,
            ),
            fit: BoxFit.fill),
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
    );
  }

  richText(
    String text1,
    text2,
    image,
    Color color1,
    color2,
  ) {
    var size = MediaQuery.of(context).size;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(top: size.height * 0.008),
          width: 10,
          child: Image.asset(
            image,
          ),
        ),
        Container(
          width: size.width * 0.4,
          // color: Colors.red,
          margin: EdgeInsets.only(top: 3),
          child: RichText(
            text: new TextSpan(
              text: "",
              style: DefaultTextStyle.of(context).style,
              children: <TextSpan>[
                new TextSpan(
                    text: text1,
                    style: new TextStyle(
                        fontWeight: FontWeight.normal, color: color1)),
                new TextSpan(
                    text: text2,
                    style: new TextStyle(
                        fontWeight: FontWeight.normal, color: color2)),
              ],
            ),
          ),
        ),
      ],
    );
  }

  text(String title, Color color) {
    var size = MediaQuery.of(context).size;
    return Container(
        margin: EdgeInsets.only(top: 3),
        width: size.width * 0.4,
        child: Text(
          title,
          textAlign: TextAlign.start,
          style: TextStyle(color: color, fontWeight: FontWeight.w600),
        ));
  }

  button(String title, double heigt, width, Function ontap) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: ontap,
      child: Container(
        margin: EdgeInsets.only(top: size.height * 0.02),
        height: heigt,
        width: width,
        decoration: const BoxDecoration(
            color: Config.primaryColor,
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        child: Center(
            child: Text(title,
                style: TextStyle(fontSize: 15, color: Colors.white))),
      ),
    );
  }

  iocn(IconData icons) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(top: size.height * 0.015),
      height: size.height * 0.03,
      width: size.width * 0.1,
      child: MaterialButton(
        onPressed: () {},
        color: Colors.grey[300],
        textColor: Colors.red,
        child: Icon(
          icons,
          size: 18,
        ),
        padding: EdgeInsets.all(0),
        shape: CircleBorder(),
      ),
    );
  }

  wrapText(String text) {
    return Container(
      margin: EdgeInsets.only(top: 5, left: 2),
      decoration: const BoxDecoration(
          color: Colors.black12,
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      child: Text(text),
    );
  }
}
