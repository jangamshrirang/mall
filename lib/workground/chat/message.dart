import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';

class MessagePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Config.bodyColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => ExtendedNavigator.of(context).root.push('/'),
        ),
        elevation: 0.0,
        backgroundColor: Config.primaryColor,
        title: Text('Chats'),
        centerTitle: true,
      ),
      body: ListView(
        padding: EdgeInsets.zero,
        children: [
          GestureDetector(
            onTap: () => context.navigator.root.push("/chats/room"),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: 40,
                          width: 40,
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(
                                'https://yt3.ggpht.com/a/AATXAJyZVJUqqUcMQJgPt6KWOmGppeaV5wg-3ofGBmrT=s900-c-k-c0xffffffff-no-rj-mo'),
                          ),
                        ),
                        SizedBox(
                          width: 8.0,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AutoSizeText(
                              'HyperX',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.black87),
                            ),
                            SizedBox(
                              height: 3.0,
                            ),
                            AutoSizeText(
                              '08/09/2020',
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 12.0),
                              minFontSize: 12,
                            ),
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        child: AutoSizeText(
                            'Get ready for the best deals this new year',
                            style: TextStyle(color: Colors.black87)))
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
