import 'package:flutter/material.dart';

TextStyle textStyle = new TextStyle(fontFamily: 'Gotham', color: Colors.white);
TextStyle textStyleBold = new TextStyle(
    fontFamily: 'Gotham', fontWeight: FontWeight.bold, color: Colors.white);
TextStyle textStyleLigthGrey =
    new TextStyle(fontFamily: 'Gotham', color: Colors.grey);
