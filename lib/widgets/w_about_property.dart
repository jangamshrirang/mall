import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:wblue_customer/services/global.dart';

import '../env/config.dart';

class WAboutPropertyWidget extends StatelessWidget {
  final LatLng latLng;
  final String about;

  WAboutPropertyWidget({
    this.latLng,
    this.about: '',
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _location(context),
        Divider(),
        _aboutProperty(context),
      ],
    );
  }

  Widget _location(BuildContext context) {
    LatLng _latLng = latLng ?? LatLng(37.43296265331129, -122.08832357078792);

    MarkerId markerId = MarkerId('test');
    final Marker marker = Marker(
      markerId: markerId,
      position: _latLng,
    );
    Map<dynamic, Marker> markers = Map<dynamic, Marker>();
    markers[markerId] = marker;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Location',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  GestureDetector(
                    onTap: () async {
                      await showLocationPicker(
                        context,
                        Config.googleMapAPI,
                        initialCenter: _latLng,
                        appBarColor: Colors.amber,
                      );
                    },
                    child: Container(
                      height: 90.0,
                      width: 90.0,
                      margin: EdgeInsets.symmetric(vertical: 8.0),
                      child: Stack(
                        children: <Widget>[
                          GoogleMap(
                            onMapCreated: (GoogleMapController controller) {},
                            initialCameraPosition: CameraPosition(
                              zoom: 10.0,
                              target: _latLng,
                            ),
                            myLocationButtonEnabled: false,
                            markers: Set<Marker>.of(markers.values),
                            zoomGesturesEnabled: false,
                            onTap: (newLatLng) async {
                              await showLocationPicker(
                                context,
                                Config.googleMapAPI,
                                initialCenter: _latLng,
                                appBarColor: Colors.amber,
                              );
                            },
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              height: 25.0,
                              width: double.infinity,
                              color: Colors.amber,
                              child: Center(
                                child: Text(
                                  'View Map',
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(width: 16.0),
              Expanded(
                child: Text('514 Stonybrook Court Canal Winchester, OH 43110'),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _aboutProperty(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'About Property',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              InkWell(
                child: Text(
                  'See All',
                  style: TextStyle(
                    fontSize: 12.0,
                    color: Colors.amber,
                    decoration: TextDecoration.underline,
                  ),
                ),
                onTap: () {
                  _seeAll(context);
                },
              ),
            ],
          ),
          SizedBox(height: 8.0),
          Text(
            about,
            textAlign: TextAlign.justify,
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }

  void _seeAll(BuildContext context) {
    Global.simpleModal(
      context,
      title: 'About Property',
      isScrollControlled: true,
      children: [
        Text(
          about,
          textAlign: TextAlign.justify,
        ),
      ],
    );
  }
}
