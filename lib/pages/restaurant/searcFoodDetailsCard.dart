import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/Restaurant/searcFoodDetailsModel.dart';
import 'package:wblue_customer/providers/restaurant/SearcFoodDetailHelper.dart';
import 'package:wblue_customer/widgets/w_loading.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/widgets/w_rating.dart';

class SearchFoodDetailsCard extends StatefulWidget {
  String restHashId, foodHashID, foodName;
  SearchFoodDetailsCard({this.foodHashID, this.restHashId, this.foodName});
  @override
  _SearchFoodDetailsCardState createState() => _SearchFoodDetailsCardState();
}

class _SearchFoodDetailsCardState extends State<SearchFoodDetailsCard> {
  int count = 1;
  @override
  void initState() {
    // TODO: implement initState
    Future.microtask(() => context
        .read<SearchFoodResultHelper>()
        .fetchSearchFoodResult(widget.restHashId, widget.foodHashID));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Config.primaryColor, title: Text(widget.foodName)),
      body: Container(
          child: Consumer<SearchFoodResultHelper>(
              builder: (context, restaurant, child) =>
                  restaurant.restaurantsSearchFoodResult.length > 0
                      ? Wrap(
                          children: List.generate(
                              restaurant.restaurantsSearchFoodResult.length,
                              (index) {
                            SearchFoodDetailsModel rCategeriesListModel =
                                SearchFoodDetailsModel.fromJson(restaurant
                                    .restaurantsSearchFoodResult[index]);
                            return imageAndDisCard(
                                rCategeriesListModel.full,
                                rCategeriesListModel.name,
                                rCategeriesListModel.description,
                                rCategeriesListModel.price,
                                "5.5");
                            // Text(rCategeriesListModel.name);
                          }),
                        )
                      : WLoadingWidget())),
    );
  }

  imageAndDisCard(String image, name, discription, price, ratings) {
    var size = MediaQuery.of(context).size;
    return Container(
      margin:
          EdgeInsets.only(left: size.width * 0.025, top: size.height * 0.01),
      height: size.height * 0.6,
      width: size.width * 0.95,
      // color: Colors.red,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CachedNetworkImage(
            height: size.height * 0.25,
            width: size.width,
            imageUrl: image,
            imageBuilder: (context, imageProvider) => Container(
              height: size.height * 0.1,
              width: size.width * 0.18,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(image),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(color: Colors.black12),
              ),
            ),
            placeholder: (context, url) => Padding(
              padding: const EdgeInsets.all(8.0),
              child: WLoadingWidget(),
            ),
            errorWidget: (context, url, error) => Container(
              margin: EdgeInsets.all(08),
              height: size.height * 0.2,
              width: size.width * 0.2,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                      "https://cdn.dribbble.com/users/1012566/screenshots/4187820/topic-2.jpg"),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                ),
              ),
            ),
          ),
          SizedBox(height: size.height * 0.01),
          text(
              " QAR $price", Colors.black, FontWeight.bold, size.height * 0.03),
          SizedBox(height: size.height * 0.005),
          Container(
              width: size.width,
              child: text(" dewfDFSFDSFASDFSDsdasdlsakdfj", Colors.black,
                  FontWeight.normal, size.height * 0.025)),
          Container(
            height: size.height * 0.05,
            // color: Colors.blue,
            child: Row(
              children: [
                WRatingsWidget(3.5, size: size.height * 0.025, compact: true),
                Spacer(),
                incrementDecriment()
              ],
            ),
          ),
          customCheckBox(),
          customCheckBox(),
          customCheckBox()
        ],
      ),
    );
  }

  text(String title, Color color, FontWeight fontWeight, double size) {
    return Text(title,
        maxLines: 2,
        style: TextStyle(
            color: color,
            fontWeight: fontWeight,
            fontSize: size,
            fontFamily: Config.fontFamily));
  }

  incrementDecriment() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.5,
      height: MediaQuery.of(context).size.height * 0.05,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  if (count >= 1) {
                    setState(() {
                      count--;
                    });
                  }
                },
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.09,
                  height: MediaQuery.of(context).size.height * 0.035,
                  child: Icon(Icons.remove,
                      size: MediaQuery.of(context).size.height * 0.04,
                      color: Colors.black),
                ),
              ),
              Text('$count',
                  style: new TextStyle(
                      fontSize: MediaQuery.of(context).size.height * 0.04,
                      color: Colors.black)),
              GestureDetector(
                onTap: () {
                  setState(() {
                    count++;
                  });
                },
                child: Container(
                  padding: EdgeInsets.only(right: 0),
                  width: MediaQuery.of(context).size.width * 0.09,
                  height: MediaQuery.of(context).size.height * 0.05,
                  // color: Colors.red,
                  child: Icon(
                    Icons.add,
                    size: MediaQuery.of(context).size.height * 0.04,
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget customCheckBox() {
    bool checked=true;
    return CheckboxListTile(
        title: const Text('Animate Slowly'),
        controlAffinity: ListTileControlAffinity.leading,
        value: checked,
        secondary: const Icon(Icons.hourglass_empty),
        activeColor: Config.primaryColor,
        checkColor: Colors.black,
        onChanged: (bool value) {
          setState(
            () {
              checked = value;
            },
          );
        });
  }
}
