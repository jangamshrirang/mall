import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WRatingsWidget extends StatelessWidget {
  final double rate;
  final double size;
  final bool compact;

  WRatingsWidget(this.rate, {this.size: 16.0, this.compact: false});

  @override
  Widget build(BuildContext context) {
    int full = rate.floor();
    bool hasHalf = (rate - rate.floor()) > 0;
    List<String> stars = [];
    for (int i = 1; i <= 5; i++) {
      if (i <= full) {
        stars.add('full');
      } else if (hasHalf) {
        if (stars.indexOf('half') < 0) {
          stars.add('half');
        } else {
          stars.add('empty');
        }
      } else {
        stars.add('empty');
      }
    }
    stars.add(rate.toString());

    if (compact) {
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(
            Icons.star,
            color: Colors.amber,
            size: size - 2,
          ),
          SizedBox(width: 2.0),
          Text(
            rate.toString(),
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: size,
                color: Colors.amber),
          )
        ],
      );
    } else {
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: stars.map<Widget>((item) {
          if (item == 'full') {
            return Icon(
              Icons.star,
              color: Colors.amber,
              size: size,
            );
          } else if (item == 'half') {
            return Icon(
              Icons.star_half,
              color: Colors.amber,
              size: size,
            );
          } else if (item == 'empty') {
            return Icon(
              Icons.star_border,
              color: Colors.amber,
              size: size,
            );
          } else {
            return Container();
          }
        }).toList(),
      );
    }
  }
}
