import 'package:wblue_customer/services/dio/dio_client.dart';
import 'dart:collection';

import 'package:flutter/foundation.dart';

DioClient _dioClient = DioClient();

class R_SearchResultHelper with ChangeNotifier {
  Map _restaurants;
  bool _isLoading = true;

  Map get restaurantsSearchResult =>
 _restaurants;
  bool get isLoading => _isLoading;

  Future<void> fetchSearchRestResult(String searchKey) async {
    print('searchKey :$searchKey');

    setLoading(true);
    _restaurants = await fetchSearchRestaurentsResult(searchKey);
    setLoading(false);

    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future fetchSearchRestaurentsResult(String searchKey) async {
  Map<String, dynamic> _body = {
    'restaurant_hash_id': searchKey,
  };
  APIResponse res =
      await _dioClient.publicPost('/restaurant/details', data: _body);
  if (res.code >= 400) {
    return [];
  }
print(res.data);
  return res.data;
}
