import 'package:email_launcher/email_launcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/market/Brands/Vendor/vendorHomePage.dart';
import 'package:wblue_customer/pages/market/Brands/Vendor/vendorSearchScreen.dart';

import '../Brands.dart';
import 'VendorProfile.dart';
import 'VendorReviews.dart';

class VendorDetails extends StatefulWidget {
  int colorVal = 0xff84020e;
  @override
  _VendorDetailsState createState() => _VendorDetailsState();
}

class _VendorDetailsState extends State<VendorDetails>
    with TickerProviderStateMixin {
  ScrollController _scrollViewController;
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(
      vsync: this,
      length: 2,
    );
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {
      widget.colorVal = 0xff84020e;
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return SafeArea(
      child: DefaultTabController(
        length: 2,
        child: new Scaffold(
          body: new NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  actions: [
                    Spacer(),
                    searchContainer(),
                    Spacer(),
                  ],

                  expandedHeight: 160.0,
                  floating: true,
                  pinned: true,
                  backgroundColor: Config.primaryColor,
                  flexibleSpace: FlexibleSpaceBar(
                      titlePadding:
                          EdgeInsetsDirectional.only(start: 20, bottom: 20),
                      centerTitle: false,
                      title: vendorAppBartitle(),
                      background: Image.asset(
                        "assets/images/account/bg.jpeg",
                        fit: BoxFit.cover,
                      )),
                  // <--- this is required if I want the application bar to show when I scroll up
                  bottom: TabBar(
                    controller: _tabController,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicatorColor: Colors.white,
                    unselectedLabelColor: Colors.grey,
                    tabs: <Widget>[
                      Tab(child: text("Al Products", 0)),
                      Tab(
                        child: text("Home Page", 1),
                      ),
                    ],
                  ),
                ),
              ];
            },
            body: new TabBarView(controller: _tabController, children: [
              VendarAllProducts(),
              VendoroHomePage(),
            ]),
          ),
        ),
      ),
    );
  }

//image and details in app bar
  vendorAppBartitle() {
    var size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(bottom: size.height * 0.02),
        color: Colors.transparent,
        width: size.width * 0.7,
        height: size.height * 0.065,
        child: Row(
          children: [
            Container(
              color: Colors.transparent,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.asset(
                  ('assets/images/users/1-boy.png'),
                  width: 40,
                ),
              ),
            ),
            SizedBox(
              width: 4,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => VendorProfile()));
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: 5,
                    ),
                    child: Text(
                      'Mandy Shop',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: size.height * 0.02,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Spacer(),
                  Row(
                    children: [
                      Icon(
                        Icons.favorite_border,
                        color: Colors.white,
                        size: 7,
                      ),
                      Text(
                        '1.1 K',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: size.height * 0.012,
                            fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                  Spacer(),
                  Text(
                    '95% Positive seller Ratings',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: size.height * 0.012,
                        fontWeight: FontWeight.normal),
                  ),
                  Spacer(),
                ],
              ),
            ),
            SizedBox(width: size.width * 0.08),
            greadintButton()
          ],
        ),
      ),
    );
  }

  greadintButton() {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        SizedBox(height: size.height * 0.01),
        Row(
          children: [
            GestureDetector(
              onTap: () {
                _launchEmail();
              },
              child: Icon(
                Icons.email,
                color: Colors.white,
                size: 12,
              ),
            ),
            SizedBox(width: size.width * 0.03),
            GestureDetector(
              onTap: () {
                launch("tel:+97450372282");
                print("call");
              },
              child: Icon(
                Icons.call,
                color: Colors.white,
                size: 12,
              ),
            ),
          ],
        ),
        GestureDetector(
          onTap: () {
            print("follow");
          },
          child: Container(
            width: size.width * 0.13,
            height: size.height * 0.022,
            margin: EdgeInsets.only(top: size.height * 0.01),
            padding: EdgeInsets.only(top: 3),
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Colors.orange,
                    Colors.amber,
                  ],
                ),
                borderRadius: BorderRadius.all(Radius.circular(2.0))),
            //padding: const EdgeInsets.fromLTRB(10, 2, 0, 0),
            child: Text('      Follow', style: TextStyle(fontSize: 10)),
          ),
        ),
      ],
    );
  }

  text(String title, int number) {
    return Text(
      title,
      style: TextStyle(
          color: _tabController.index == number ? Colors.white : Colors.white,
          fontWeight: FontWeight.bold),
      textAlign: TextAlign.start,
    );
  }

//searc box in app bar
  searchContainer() {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => VendorSearchScreen()));
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 10, top: 15, left: 20),
        height: size.height * 0.02,
        width: size.width * 0.8,
        decoration: BoxDecoration(
            color: Colors.white30,
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        child: Row(
          children: [
            SizedBox(width: 10),
            Icon(
              Icons.search,
              color: Colors.white,
              size: 18,
            ),
            SizedBox(width: 10),
            Text(
              "Search in store",
              style: TextStyle(color: Colors.white, fontSize: 15),
            ),
          ],
        ),
      ),
    );
  }
   void _launchEmail() async {
    Email email = Email(  to: ['one@gmail.com'],);
    await EmailLauncher.launch(email);
  }
}
