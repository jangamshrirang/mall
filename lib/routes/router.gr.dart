// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:dash_chat/dash_chat.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../models/Reservations/reservation_model.dart';
import '../models/Restaurant/R_ListByRatings.dart';
import '../models/Restaurant/food_type_model.dart';
import '../models/Restaurant/user_office_address.dart';
import '../models/user.dart';
import '../pages/Activities/ActivityCart.dart';
import '../pages/Activities/ActivityHome.dart';
import '../pages/Activities/ActivitySucces.dart';
import '../pages/MyOrders/OrderList.dart';
import '../pages/auth/login.dart';
import '../pages/auth/otp.dart';
import '../pages/auth/registration.dart';
import '../pages/booking/ConformatinScreen.dart';
import '../pages/booking/book_table.dart';
import '../pages/booking/booking_confirmation.dart';
import '../pages/coming_soon.dart';
import '../pages/hotel/hotel_confirmation.dart';
import '../pages/hotel/hotel_payment.dart';
import '../pages/hotel/hotels.dart';
import '../pages/hotel/review_booking.dart';
import '../pages/hotel/single_hotel.dart';
import '../pages/landing.dart';
import '../pages/locations/location.dart';
import '../pages/market/Brands/Brands.dart';
import '../pages/market/account/Wallet/TransationScreen.dart';
import '../pages/market/account/Wallet/wallet.dart';
import '../pages/market/account/acount.dart';
import '../pages/market/category/category.dart';
import '../pages/market/detector/camera.dart';
import '../pages/market/detector/detector_copy.dart';
import '../pages/market/feed/feed.dart';
import '../pages/market/home/home.dart';
import '../pages/market/home/live.dart';
import '../pages/market/home/live/host.dart';
import '../pages/market/home/live/join.dart';
import '../pages/market/home/tab/live/create_live_content.dart';
import '../pages/market/hot_deals.dart';
import '../pages/market/market_main.dart';
import '../pages/market/order/cart/cart.dart';
import '../pages/market/order/payment.dart';
import '../pages/market/order/shipping_billing.dart';
import '../pages/market/order/success_purchased.dart';
import '../pages/market/order/track-order.dart';
import '../pages/market/search.dart';
import '../pages/market/view_other_product.dart';
import '../pages/market/view_product.dart';
import '../pages/market/w_fullscreen_image.dart';
import '../pages/payment/payment.dart';
import '../pages/reservation/reservation_details.dart';
import '../pages/reservation/reservation_timeline.dart';
import '../pages/reservation/reservations.dart';
import '../pages/restaurant/cart/cart.dart';
import '../pages/restaurant/cart/checkout/change_delivery_address.dart';
import '../pages/restaurant/cart/checkout/change_delivery_time.dart';
import '../pages/restaurant/cart/checkout/main.dart';
import '../pages/restaurant/cart/checkout/map.dart';
import '../pages/restaurant/cart/checkout/payment-method.dart';
import '../pages/restaurant/cart/d8_soon_pickup_confirmation.dart';
import '../pages/restaurant/cart/delivery_confirmation.dart';
import '../pages/restaurant/cart/food_cart.dart';
import '../pages/restaurant/cart/food_order_confirmation.dart';
import '../pages/restaurant/message.dart';
import '../pages/restaurant/orders/view_order.dart';
import '../pages/restaurant/restaurant_list.dart';
import '../pages/restaurant/store/main.dart';
import '../pages/restaurant/store/restCatogries.dart';
import '../pages/restaurant/store/single_restaurant.dart';
import '../pages/restaurant/store/variation.dart';
import '../pages/unknown_route.dart';
import '../pages/user/profile.dart';
import '../workground/chat/message.dart';
import '../workground/chat/room.dart';
import 'route_guards.dart';

class Routes {
  static const String landingPage = '/';
  static const String loginPage = '/login';
  static const String registrationPage = '/registration';
  static const String otpPage = '/otp';
  static const String locationPage = '/location';
  static const String restaurantlist = '/restaurants';
  static const String restaurantViewOrderPage = '/restaurants/view/order';
  static const String restaurantsScreen = '/restaurants/single';
  static const String restCatogries = '/restaurants/menu';
  static const String restaurantFoodVariation = '/variation';
  static const String restaurantFoodCart = '/restaurant-food-cart';
  static const String restaurantMessagePage = '/restaurans/chats/room';
  static const String restaurantCartPage = '/restaurants/cart';
  static const String restaurantPlaceOrderPage = '/restaurants/place-order';
  static const String restaurantCheckoutAddressPage = '/select-address';
  static const String selectDeliveryAddressPage = '/open-map';
  static const String restaurantCheckoutDeliveryPage = '/select-delivery-time';
  static const String restaurantCheckoutPaymentPage = '/select-payment-method';
  static const String myordeList = '/restaurants/my-order';
  static const String reservationPage = '/reservations';
  static const String reservationDetailsPage = '/reservations/details';
  static const String reservationTimelinePage = '/reservations/timeline';
  static const String restaurantOrderConfirmation =
      '/restaurants/order-confirmation';
  static const String pickupConfirmationPage =
      '/restaurants/pickup-confirmation';
  static const String deliveryConfirmationPage = '/delivery-confirmation';
  static const String bookTablePage = '/restaurants/book-table';
  static const String confirmationScreen =
      '/restaurants/book/table/confirmation';
  static const String activityBookingSuccess =
      '/restaurants/book/table/complete';
  static const String bookingConfirmationPage = '/booking-confirmation';
  static const String hotelPage = '/hotels';
  static const String singleHotelPage = '/hotels/single';
  static const String hotelPaymentPage = '/hotels/payment';
  static const String hotelConfirmationPage = '/hotels/confirmation';
  static const String hotelReviewBookingPage = '/hotels/review-booking';
  static const String activityHome = '/activity';
  static const String activityCart = '/activity/cart';
  static const String mainMarketPage = '/market';
  static const String cartMarketPage = '/market/cart/items';
  static const String accountMarketPage = '/market/account';
  static const String userProfile = '/user';
  static const String messagePage = '/chats';
  static const String messageRoomPage = '/chats/room';
  static const String marketWalletPage = '/market/user/wallet';
  static const String liveMarketPage = '/market/live';
  static const String callPage = '/market/live/host';
  static const String joinPage = '/market/live/join';
  static const String createLiveContentPage = '/market/live/content';
  static const String marketSearchProduct = '/market/search-product';
  static const String marketCartPayment = '/market/payment';
  static const String shippingBillingPage = '/market/shipping-billing';
  static const String marketSuccessPurchasedPage = '/market/order-success';
  static const String orderTrackPage = '/market/order-track';
  static const String hotDealsPage = '/market/hot-deals';
  static const String marketCameraPage = '/market-camera-recognizer';
  static const String productRecognizerPage = '/market-camera';
  static const String wProductImage = '/product-images';
  static const String viewProduct = '/view-product';
  static const String viewOtherProduct = '/view-other-product';
  static const String categoryPage = '/market-categories';
  static const String wmallPayment = '/wmall-payment';
  static const String comingSoonPage = '/coming-soon';
  static const String transationScreen = '/wallet-transcations';
  static const String unknownRouteScreen = '*';
  static const all = <String>{
    landingPage,
    loginPage,
    registrationPage,
    otpPage,
    locationPage,
    restaurantlist,
    restaurantViewOrderPage,
    restaurantsScreen,
    restCatogries,
    restaurantFoodVariation,
    restaurantFoodCart,
    restaurantMessagePage,
    restaurantCartPage,
    restaurantPlaceOrderPage,
    restaurantCheckoutAddressPage,
    selectDeliveryAddressPage,
    restaurantCheckoutDeliveryPage,
    restaurantCheckoutPaymentPage,
    myordeList,
    reservationPage,
    reservationDetailsPage,
    reservationTimelinePage,
    restaurantOrderConfirmation,
    pickupConfirmationPage,
    deliveryConfirmationPage,
    bookTablePage,
    confirmationScreen,
    activityBookingSuccess,
    bookingConfirmationPage,
    hotelPage,
    singleHotelPage,
    hotelPaymentPage,
    hotelConfirmationPage,
    hotelReviewBookingPage,
    activityHome,
    activityCart,
    mainMarketPage,
    cartMarketPage,
    accountMarketPage,
    userProfile,
    messagePage,
    messageRoomPage,
    marketWalletPage,
    liveMarketPage,
    callPage,
    joinPage,
    createLiveContentPage,
    marketSearchProduct,
    marketCartPayment,
    shippingBillingPage,
    marketSuccessPurchasedPage,
    orderTrackPage,
    hotDealsPage,
    marketCameraPage,
    productRecognizerPage,
    wProductImage,
    viewProduct,
    viewOtherProduct,
    categoryPage,
    wmallPayment,
    comingSoonPage,
    transationScreen,
    unknownRouteScreen,
  };
}

class AppRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.landingPage, page: LandingPage),
    RouteDef(Routes.loginPage, page: LoginPage),
    RouteDef(Routes.registrationPage, page: RegistrationPage),
    RouteDef(Routes.otpPage, page: OtpPage),
    RouteDef(Routes.locationPage, page: LocationPage),
    RouteDef(Routes.restaurantlist, page: Restaurantlist, guards: [LocGuard]),
    RouteDef(Routes.restaurantViewOrderPage, page: RestaurantViewOrderPage),
    RouteDef(
      Routes.restaurantsScreen,
      page: SingleRestaurantPage,
      generator: RestaurantsScreenRouter(),
    ),
    RouteDef(Routes.restCatogries, page: RestCatogries),
    RouteDef(Routes.restaurantFoodVariation, page: RestaurantFoodVariation),
    RouteDef(Routes.restaurantFoodCart,
        page: RestaurantFoodCart, guards: [AuthGuard]),
    RouteDef(Routes.restaurantMessagePage,
        page: RestaurantMessagePage, guards: [AuthGuard]),
    RouteDef(Routes.restaurantCartPage,
        page: RestaurantCartPage, guards: [AuthGuard]),
    RouteDef(Routes.restaurantPlaceOrderPage, page: RestaurantPlaceOrderPage),
    RouteDef(Routes.restaurantCheckoutAddressPage,
        page: RestaurantCheckoutAddressPage),
    RouteDef(Routes.selectDeliveryAddressPage, page: SelectDeliveryAddressPage),
    RouteDef(Routes.restaurantCheckoutDeliveryPage,
        page: RestaurantCheckoutDeliveryPage),
    RouteDef(Routes.restaurantCheckoutPaymentPage,
        page: RestaurantCheckoutPaymentPage),
    RouteDef(Routes.myordeList, page: MyordeList, guards: [AuthGuard]),
    RouteDef(Routes.reservationPage,
        page: ReservationPage, guards: [AuthGuard]),
    RouteDef(Routes.reservationDetailsPage, page: ReservationDetailsPage),
    RouteDef(Routes.reservationTimelinePage, page: ReservationTimelinePage),
    RouteDef(Routes.restaurantOrderConfirmation,
        page: RestaurantOrderConfirmation),
    RouteDef(Routes.pickupConfirmationPage, page: PickupConfirmationPage),
    RouteDef(Routes.deliveryConfirmationPage, page: DeliveryConfirmationPage),
    RouteDef(Routes.bookTablePage, page: BookTablePage, guards: [AuthGuard]),
    RouteDef(Routes.confirmationScreen, page: ConfirmationScreen),
    RouteDef(Routes.activityBookingSuccess, page: ActivityBookingSuccess),
    RouteDef(Routes.bookingConfirmationPage, page: BookingConfirmationPage),
    RouteDef(Routes.hotelPage, page: HotelPage),
    RouteDef(Routes.singleHotelPage, page: SingleHotelPage),
    RouteDef(Routes.hotelPaymentPage, page: HotelPaymentPage),
    RouteDef(Routes.hotelConfirmationPage, page: HotelConfirmationPage),
    RouteDef(Routes.hotelReviewBookingPage,
        page: HotelReviewBookingPage, guards: [AuthGuard]),
    RouteDef(Routes.activityHome, page: ActivityHome),
    RouteDef(Routes.activityCart, page: ActivityCart, guards: [AuthGuard]),
    RouteDef(
      Routes.mainMarketPage,
      page: MainMarketPage,
      generator: MainMarketPageRouter(),
    ),
    RouteDef(Routes.cartMarketPage, page: CartMarketPage, guards: [AuthGuard]),
    RouteDef(Routes.accountMarketPage,
        page: AccountMarketPage, guards: [AuthGuard]),
    RouteDef(Routes.userProfile, page: UserProfile, guards: [AuthGuard]),
    RouteDef(Routes.messagePage, page: MessagePage),
    RouteDef(Routes.messageRoomPage,
        page: MessageRoomPage, guards: [AuthGuard]),
    RouteDef(Routes.marketWalletPage,
        page: MarketWalletPage, guards: [AuthGuard]),
    RouteDef(Routes.liveMarketPage, page: LiveMarketPage),
    RouteDef(Routes.callPage, page: CallPage),
    RouteDef(Routes.joinPage, page: JoinPage, guards: [AuthGuard]),
    RouteDef(Routes.createLiveContentPage,
        page: CreateLiveContentPage, guards: [AuthGuard]),
    RouteDef(Routes.marketSearchProduct, page: MarketSearchProduct),
    RouteDef(Routes.marketCartPayment, page: MarketCartPayment),
    RouteDef(Routes.shippingBillingPage, page: ShippingBillingPage),
    RouteDef(Routes.marketSuccessPurchasedPage,
        page: MarketSuccessPurchasedPage),
    RouteDef(Routes.orderTrackPage, page: OrderTrackPage),
    RouteDef(Routes.hotDealsPage, page: HotDealsPage),
    RouteDef(Routes.marketCameraPage, page: MarketCameraPage),
    RouteDef(Routes.productRecognizerPage, page: ProductRecognizerPage),
    RouteDef(Routes.wProductImage, page: WProductImage),
    RouteDef(Routes.viewProduct, page: ViewProduct),
    RouteDef(Routes.viewOtherProduct, page: ViewOtherProduct),
    RouteDef(Routes.categoryPage, page: CategoryPage),
    RouteDef(Routes.wmallPayment, page: WmallPayment),
    RouteDef(Routes.comingSoonPage, page: ComingSoonPage),
    RouteDef(Routes.transationScreen, page: TransationScreen),
    RouteDef(Routes.unknownRouteScreen, page: UnknownRouteScreen),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    LandingPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => LandingPage(),
        settings: data,
        fullscreenDialog: true,
      );
    },
    LoginPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => LoginPage(),
        settings: data,
        fullscreenDialog: true,
      );
    },
    RegistrationPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => RegistrationPage(),
        settings: data,
      );
    },
    OtpPage: (data) {
      return buildAdaptivePageRoute<bool>(
        builder: (context) => OtpPage(),
        settings: data,
        fullscreenDialog: true,
      );
    },
    LocationPage: (data) {
      return buildAdaptivePageRoute<bool>(
        builder: (context) => LocationPage(),
        settings: data,
        fullscreenDialog: true,
      );
    },
    Restaurantlist: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => Restaurantlist(),
        settings: data,
      );
    },
    RestaurantViewOrderPage: (data) {
      final args = data.getArgs<RestaurantViewOrderPageArguments>(
        orElse: () => RestaurantViewOrderPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) =>
            RestaurantViewOrderPage(orderHashid: args.orderHashid),
        settings: data,
      );
    },
    SingleRestaurantPage: (data) {
      final args = data.getArgs<SingleRestaurantPageArguments>(
        orElse: () => SingleRestaurantPageArguments(),
      );
      return PageRouteBuilder<String>(
        pageBuilder: (context, animation, secondaryAnimation) =>
            SingleRestaurantPage(
          restaurantListModel: args.restaurantListModel,
          restIndex: args.restIndex,
        ),
        settings: data,
        transitionsBuilder: TransitionsBuilders.slideBottom,
        transitionDuration: const Duration(milliseconds: 200),
      );
    },
    RestCatogries: (data) {
      final args = data.getArgs<RestCatogriesArguments>(
        orElse: () => RestCatogriesArguments(),
      );
      return PageRouteBuilder<String>(
        pageBuilder: (context, animation, secondaryAnimation) => RestCatogries(
          coverImage: args.coverImage,
          restimage: args.restimage,
          restName: args.restName,
          isDelivery: args.isDelivery,
          preOrder: args.preOrder,
          isFromScan: args.isFromScan,
          restSlogan: args.restSlogan,
          restindex: args.restindex,
          isFromTakeAway: args.isFromTakeAway,
          restId: args.restId,
          restThumbnail: args.restThumbnail,
          coverThumbnail: args.coverThumbnail,
          tableID: args.tableID,
          reservationHashId: args.reservationHashId,
        ),
        settings: data,
        transitionsBuilder: TransitionsBuilders.slideBottom,
      );
    },
    RestaurantFoodVariation: (data) {
      final args = data.getArgs<RestaurantFoodVariationArguments>(
        orElse: () => RestaurantFoodVariationArguments(),
      );
      return PageRouteBuilder<dynamic>(
        pageBuilder: (context, animation, secondaryAnimation) =>
            RestaurantFoodVariation(
          food: args.food,
          isDelivery: args.isDelivery,
          isFromTakeAway: args.isFromTakeAway,
          preorder: args.preorder,
          restaurantHashid: args.restaurantHashid,
          isFromScan: args.isFromScan,
          reservationHashId: args.reservationHashId,
        ),
        settings: data,
        transitionsBuilder: TransitionsBuilders.slideBottom,
      );
    },
    RestaurantFoodCart: (data) {
      final args = data.getArgs<RestaurantFoodCartArguments>(
        orElse: () => RestaurantFoodCartArguments(),
      );
      return PageRouteBuilder<dynamic>(
        pageBuilder: (context, animation, secondaryAnimation) =>
            RestaurantFoodCart(
          isFromScan: args.isFromScan,
          preOrder: args.preOrder,
          isDelivery: args.isDelivery,
          totlaPrice: args.totlaPrice,
          coverImage: args.coverImage,
          restimage: args.restimage,
          restName: args.restName,
          restId: args.restId,
          restSlogan: args.restSlogan,
          onlybooking: args.onlybooking,
          filterkey: args.filterkey,
          isFromTakeAway: args.isFromTakeAway,
          appBarTitle: args.appBarTitle,
          fromMenu: args.fromMenu,
          reservationHashID: args.reservationHashID,
        ),
        settings: data,
        transitionsBuilder: TransitionsBuilders.slideBottom,
        transitionDuration: const Duration(milliseconds: 200),
      );
    },
    RestaurantMessagePage: (data) {
      final args = data.getArgs<RestaurantMessagePageArguments>(
        orElse: () => RestaurantMessagePageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => RestaurantMessagePage(
          restaurantHashid: args.restaurantHashid,
          restaurantName: args.restaurantName,
        ),
        settings: data,
      );
    },
    RestaurantCartPage: (data) {
      final args = data.getArgs<RestaurantCartPageArguments>(
        orElse: () => RestaurantCartPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => RestaurantCartPage(
          fromScan: args.fromScan,
          preOrder: args.preOrder,
          delivery: args.delivery,
          totlaPrice: args.totlaPrice,
          coverImage: args.coverImage,
          restimage: args.restimage,
          restName: args.restName,
          restId: args.restId,
          restSlogan: args.restSlogan,
          onlybooking: args.onlybooking,
          filterkey: args.filterkey,
          fromTakeAway: args.fromTakeAway,
          appBarTitle: args.appBarTitle,
          fromMenu: args.fromMenu,
          reservationHashID: args.reservationHashID,
        ),
        settings: data,
      );
    },
    RestaurantPlaceOrderPage: (data) {
      final args = data.getArgs<RestaurantPlaceOrderPageArguments>(
        orElse: () => RestaurantPlaceOrderPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => RestaurantPlaceOrderPage(
          cartId: args.cartId,
          note: args.note,
        ),
        settings: data,
      );
    },
    RestaurantCheckoutAddressPage: (data) {
      final args = data.getArgs<RestaurantCheckoutAddressPageArguments>(
        orElse: () => RestaurantCheckoutAddressPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => RestaurantCheckoutAddressPage(
            userAddressModel: args.userAddressModel),
        settings: data,
      );
    },
    SelectDeliveryAddressPage: (data) {
      final args = data.getArgs<SelectDeliveryAddressPageArguments>(
        orElse: () => SelectDeliveryAddressPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => SelectDeliveryAddressPage(
            userOfficeAddressModel: args.userOfficeAddressModel),
        settings: data,
      );
    },
    RestaurantCheckoutDeliveryPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => RestaurantCheckoutDeliveryPage(),
        settings: data,
      );
    },
    RestaurantCheckoutPaymentPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => RestaurantCheckoutPaymentPage(),
        settings: data,
      );
    },
    MyordeList: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => MyordeList(),
        settings: data,
      );
    },
    ReservationPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ReservationPage(),
        settings: data,
      );
    },
    ReservationDetailsPage: (data) {
      final args = data.getArgs<ReservationDetailsPageArguments>(
        orElse: () => ReservationDetailsPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ReservationDetailsPage(
            restaurentReservationModel: args.restaurentReservationModel),
        settings: data,
      );
    },
    ReservationTimelinePage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ReservationTimelinePage(),
        settings: data,
      );
    },
    RestaurantOrderConfirmation: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => RestaurantOrderConfirmation(),
        settings: data,
        fullscreenDialog: true,
      );
    },
    PickupConfirmationPage: (data) {
      final args = data.getArgs<PickupConfirmationPageArguments>(
        orElse: () => PickupConfirmationPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => PickupConfirmationPage(
          totalprice: args.totalprice,
          rImage: args.rImage,
          rName: args.rName,
        ),
        settings: data,
      );
    },
    DeliveryConfirmationPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => DeliveryConfirmationPage(),
        settings: data,
      );
    },
    BookTablePage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => BookTablePage(),
        settings: data,
      );
    },
    ConfirmationScreen: (data) {
      final args = data.getArgs<ConfirmationScreenArguments>(
        orElse: () => ConfirmationScreenArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ConfirmationScreen(
          time: args.time,
          date: args.date,
          tableNumber: args.tableNumber,
          guest: args.guest,
          onlybooking: args.onlybooking,
        ),
        settings: data,
        fullscreenDialog: true,
      );
    },
    ActivityBookingSuccess: (data) {
      final args = data.getArgs<ActivityBookingSuccessArguments>(
        orElse: () => ActivityBookingSuccessArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ActivityBookingSuccess(from: args.from),
        settings: data,
        fullscreenDialog: true,
      );
    },
    BookingConfirmationPage: (data) {
      final args = data.getArgs<BookingConfirmationPageArguments>(
        orElse: () => BookingConfirmationPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => BookingConfirmationPage(
          time: args.time,
          date: args.date,
          tableNumber: args.tableNumber,
          guest: args.guest,
        ),
        settings: data,
      );
    },
    HotelPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => HotelPage(),
        settings: data,
      );
    },
    SingleHotelPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => SingleHotelPage(),
        settings: data,
      );
    },
    HotelPaymentPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => HotelPaymentPage(),
        settings: data,
      );
    },
    HotelConfirmationPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => HotelConfirmationPage(),
        settings: data,
      );
    },
    HotelReviewBookingPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => HotelReviewBookingPage(),
        settings: data,
      );
    },
    ActivityHome: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ActivityHome(),
        settings: data,
      );
    },
    ActivityCart: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ActivityCart(),
        settings: data,
      );
    },
    MainMarketPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => MainMarketPage(),
        settings: data,
      );
    },
    CartMarketPage: (data) {
      final args = data.getArgs<CartMarketPageArguments>(
        orElse: () => CartMarketPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => CartMarketPage(canBack: args.canBack),
        settings: data,
      );
    },
    AccountMarketPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => AccountMarketPage(),
        settings: data,
      );
    },
    UserProfile: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => UserProfile(),
        settings: data,
      );
    },
    MessagePage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => MessagePage(),
        settings: data,
      );
    },
    MessageRoomPage: (data) {
      final args = data.getArgs<MessageRoomPageArguments>(
        orElse: () => MessageRoomPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => MessageRoomPage(user: args.user),
        settings: data,
      );
    },
    MarketWalletPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => MarketWalletPage(),
        settings: data,
      );
    },
    LiveMarketPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => LiveMarketPage(),
        settings: data,
      );
    },
    CallPage: (data) {
      final args = data.getArgs<CallPageArguments>(
        orElse: () => CallPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => CallPage(
          title: args.title,
          contentThumbnail: args.contentThumbnail,
        ),
        settings: data,
        fullscreenDialog: true,
      );
    },
    JoinPage: (data) {
      final args = data.getArgs<JoinPageArguments>(
        orElse: () => JoinPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => JoinPage(
          channelId: args.channelId,
          hostId: args.hostId,
          hostImageUrl: args.hostImageUrl,
          hostName: args.hostName,
        ),
        settings: data,
        fullscreenDialog: true,
      );
    },
    CreateLiveContentPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => CreateLiveContentPage(),
        settings: data,
      );
    },
    MarketSearchProduct: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => MarketSearchProduct(),
        settings: data,
      );
    },
    MarketCartPayment: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => MarketCartPayment(),
        settings: data,
      );
    },
    ShippingBillingPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ShippingBillingPage(),
        settings: data,
      );
    },
    MarketSuccessPurchasedPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => MarketSuccessPurchasedPage(),
        settings: data,
      );
    },
    OrderTrackPage: (data) {
      final args = data.getArgs<OrderTrackPageArguments>(
        orElse: () => OrderTrackPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => OrderTrackPage(
          cordinates: args.cordinates,
          pinLocationIcon: args.pinLocationIcon,
          deliverd: args.deliverd,
        ),
        settings: data,
      );
    },
    HotDealsPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => HotDealsPage(),
        settings: data,
      );
    },
    MarketCameraPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => MarketCameraPage(),
        settings: data,
      );
    },
    ProductRecognizerPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ProductRecognizerPage(),
        settings: data,
      );
    },
    WProductImage: (data) {
      final args = data.getArgs<WProductImageArguments>(
        orElse: () => WProductImageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => WProductImage(
          loadingBuilder: args.loadingBuilder,
          backgroundDecoration: args.backgroundDecoration,
          minScale: args.minScale,
          maxScale: args.maxScale,
          initialIndex: args.initialIndex,
          scrollDirection: args.scrollDirection,
        ),
        settings: data,
      );
    },
    ViewProduct: (data) {
      final args = data.getArgs<ViewProductArguments>(nullOk: false);
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ViewProduct(args.hashid),
        settings: data,
      );
    },
    ViewOtherProduct: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ViewOtherProduct(),
        settings: data,
      );
    },
    CategoryPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => CategoryPage(),
        settings: data,
      );
    },
    WmallPayment: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => WmallPayment(),
        settings: data,
      );
    },
    ComingSoonPage: (data) {
      final args = data.getArgs<ComingSoonPageArguments>(
        orElse: () => ComingSoonPageArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => ComingSoonPage(
          page: args.page,
          showDrawer: args.showDrawer,
        ),
        settings: data,
      );
    },
    TransationScreen: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => TransationScreen(),
        settings: data,
      );
    },
    UnknownRouteScreen: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => UnknownRouteScreen(),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Navigation helper methods extension
/// *************************************************************************

extension AppRouterExtendedNavigatorStateX on ExtendedNavigatorState {
  Future<dynamic> pushLandingPage() => push<dynamic>(Routes.landingPage);

  Future<dynamic> pushLoginPage() => push<dynamic>(Routes.loginPage);

  Future<dynamic> pushRegistrationPage() =>
      push<dynamic>(Routes.registrationPage);

  Future<bool> pushOtpPage() => push<bool>(Routes.otpPage);

  Future<bool> pushLocationPage() => push<bool>(Routes.locationPage);

  Future<dynamic> pushRestaurantlist() => push<dynamic>(Routes.restaurantlist);

  Future<dynamic> pushRestaurantViewOrderPage({
    String orderHashid,
  }) =>
      push<dynamic>(
        Routes.restaurantViewOrderPage,
        arguments: RestaurantViewOrderPageArguments(orderHashid: orderHashid),
      );

  Future<String> pushRestaurantsScreen({
    RestaurantListModel restaurantListModel,
    int restIndex,
  }) =>
      push<String>(
        Routes.restaurantsScreen,
        arguments: SingleRestaurantPageArguments(
            restaurantListModel: restaurantListModel, restIndex: restIndex),
      );

  Future<String> pushRestCatogries({
    String coverImage,
    String restimage,
    String restName,
    bool isDelivery = false,
    bool preOrder = false,
    bool isFromScan = false,
    String restSlogan,
    int restindex,
    bool isFromTakeAway,
    String restId,
    String restThumbnail,
    String coverThumbnail,
    dynamic tableID,
    String reservationHashId,
  }) =>
      push<String>(
        Routes.restCatogries,
        arguments: RestCatogriesArguments(
            coverImage: coverImage,
            restimage: restimage,
            restName: restName,
            isDelivery: isDelivery,
            preOrder: preOrder,
            isFromScan: isFromScan,
            restSlogan: restSlogan,
            restindex: restindex,
            isFromTakeAway: isFromTakeAway,
            restId: restId,
            restThumbnail: restThumbnail,
            coverThumbnail: coverThumbnail,
            tableID: tableID,
            reservationHashId: reservationHashId),
      );

  Future<dynamic> pushRestaurantFoodVariation({
    Food food,
    bool isDelivery,
    bool isFromTakeAway,
    bool preorder,
    String restaurantHashid,
    bool isFromScan,
    String reservationHashId,
  }) =>
      push<dynamic>(
        Routes.restaurantFoodVariation,
        arguments: RestaurantFoodVariationArguments(
            food: food,
            isDelivery: isDelivery,
            isFromTakeAway: isFromTakeAway,
            preorder: preorder,
            restaurantHashid: restaurantHashid,
            isFromScan: isFromScan,
            reservationHashId: reservationHashId),
      );

  Future<dynamic> pushRestaurantFoodCart(
          {bool isFromScan,
          bool preOrder,
          bool isDelivery,
          dynamic totlaPrice,
          String coverImage,
          String restimage,
          String restName,
          String restId,
          String restSlogan,
          String onlybooking,
          String filterkey,
          bool isFromTakeAway,
          String appBarTitle,
          String fromMenu,
          String reservationHashID,
          OnNavigationRejected onReject}) =>
      push<dynamic>(
        Routes.restaurantFoodCart,
        arguments: RestaurantFoodCartArguments(
            isFromScan: isFromScan,
            preOrder: preOrder,
            isDelivery: isDelivery,
            totlaPrice: totlaPrice,
            coverImage: coverImage,
            restimage: restimage,
            restName: restName,
            restId: restId,
            restSlogan: restSlogan,
            onlybooking: onlybooking,
            filterkey: filterkey,
            isFromTakeAway: isFromTakeAway,
            appBarTitle: appBarTitle,
            fromMenu: fromMenu,
            reservationHashID: reservationHashID),
        onReject: onReject,
      );

  Future<dynamic> pushRestaurantMessagePage(
          {String restaurantHashid,
          String restaurantName,
          OnNavigationRejected onReject}) =>
      push<dynamic>(
        Routes.restaurantMessagePage,
        arguments: RestaurantMessagePageArguments(
            restaurantHashid: restaurantHashid, restaurantName: restaurantName),
        onReject: onReject,
      );

  Future<dynamic> pushRestaurantCartPage(
          {String fromScan,
          String preOrder,
          String delivery,
          dynamic totlaPrice,
          String coverImage,
          String restimage,
          String restName,
          String restId,
          String restSlogan,
          String onlybooking,
          String filterkey,
          String fromTakeAway,
          String appBarTitle,
          String fromMenu,
          String reservationHashID,
          OnNavigationRejected onReject}) =>
      push<dynamic>(
        Routes.restaurantCartPage,
        arguments: RestaurantCartPageArguments(
            fromScan: fromScan,
            preOrder: preOrder,
            delivery: delivery,
            totlaPrice: totlaPrice,
            coverImage: coverImage,
            restimage: restimage,
            restName: restName,
            restId: restId,
            restSlogan: restSlogan,
            onlybooking: onlybooking,
            filterkey: filterkey,
            fromTakeAway: fromTakeAway,
            appBarTitle: appBarTitle,
            fromMenu: fromMenu,
            reservationHashID: reservationHashID),
        onReject: onReject,
      );

  Future<dynamic> pushRestaurantPlaceOrderPage({
    String cartId,
    String note,
  }) =>
      push<dynamic>(
        Routes.restaurantPlaceOrderPage,
        arguments:
            RestaurantPlaceOrderPageArguments(cartId: cartId, note: note),
      );

  Future<dynamic> pushRestaurantCheckoutAddressPage({
    UserAddressModel userAddressModel,
  }) =>
      push<dynamic>(
        Routes.restaurantCheckoutAddressPage,
        arguments: RestaurantCheckoutAddressPageArguments(
            userAddressModel: userAddressModel),
      );

  Future<dynamic> pushSelectDeliveryAddressPage({
    UserOfficeAddressModel userOfficeAddressModel,
  }) =>
      push<dynamic>(
        Routes.selectDeliveryAddressPage,
        arguments: SelectDeliveryAddressPageArguments(
            userOfficeAddressModel: userOfficeAddressModel),
      );

  Future<dynamic> pushRestaurantCheckoutDeliveryPage() =>
      push<dynamic>(Routes.restaurantCheckoutDeliveryPage);

  Future<dynamic> pushRestaurantCheckoutPaymentPage() =>
      push<dynamic>(Routes.restaurantCheckoutPaymentPage);

  Future<dynamic> pushMyordeList() => push<dynamic>(Routes.myordeList);

  Future<dynamic> pushReservationPage() =>
      push<dynamic>(Routes.reservationPage);

  Future<dynamic> pushReservationDetailsPage({
    RestaurentReservationModel restaurentReservationModel,
  }) =>
      push<dynamic>(
        Routes.reservationDetailsPage,
        arguments: ReservationDetailsPageArguments(
            restaurentReservationModel: restaurentReservationModel),
      );

  Future<dynamic> pushReservationTimelinePage() =>
      push<dynamic>(Routes.reservationTimelinePage);

  Future<dynamic> pushRestaurantOrderConfirmation() =>
      push<dynamic>(Routes.restaurantOrderConfirmation);

  Future<dynamic> pushPickupConfirmationPage({
    String totalprice,
    String rImage,
    String rName,
  }) =>
      push<dynamic>(
        Routes.pickupConfirmationPage,
        arguments: PickupConfirmationPageArguments(
            totalprice: totalprice, rImage: rImage, rName: rName),
      );

  Future<dynamic> pushDeliveryConfirmationPage() =>
      push<dynamic>(Routes.deliveryConfirmationPage);

  Future<dynamic> pushBookTablePage() => push<dynamic>(Routes.bookTablePage);

  Future<dynamic> pushConfirmationScreen({
    String time,
    String date,
    String tableNumber,
    String guest,
    String onlybooking,
  }) =>
      push<dynamic>(
        Routes.confirmationScreen,
        arguments: ConfirmationScreenArguments(
            time: time,
            date: date,
            tableNumber: tableNumber,
            guest: guest,
            onlybooking: onlybooking),
      );

  Future<dynamic> pushActivityBookingSuccess({
    String from,
  }) =>
      push<dynamic>(
        Routes.activityBookingSuccess,
        arguments: ActivityBookingSuccessArguments(from: from),
      );

  Future<dynamic> pushBookingConfirmationPage({
    String time,
    String date,
    String tableNumber,
    String guest,
  }) =>
      push<dynamic>(
        Routes.bookingConfirmationPage,
        arguments: BookingConfirmationPageArguments(
            time: time, date: date, tableNumber: tableNumber, guest: guest),
      );

  Future<dynamic> pushHotelPage() => push<dynamic>(Routes.hotelPage);

  Future<dynamic> pushSingleHotelPage() =>
      push<dynamic>(Routes.singleHotelPage);

  Future<dynamic> pushHotelPaymentPage() =>
      push<dynamic>(Routes.hotelPaymentPage);

  Future<dynamic> pushHotelConfirmationPage() =>
      push<dynamic>(Routes.hotelConfirmationPage);

  Future<dynamic> pushHotelReviewBookingPage() =>
      push<dynamic>(Routes.hotelReviewBookingPage);

  Future<dynamic> pushActivityHome() => push<dynamic>(Routes.activityHome);

  Future<dynamic> pushActivityCart() => push<dynamic>(Routes.activityCart);

  Future<dynamic> pushMainMarketPage() => push<dynamic>(Routes.mainMarketPage);

  Future<dynamic> pushCartMarketPage(
          {bool canBack = false, OnNavigationRejected onReject}) =>
      push<dynamic>(
        Routes.cartMarketPage,
        arguments: CartMarketPageArguments(canBack: canBack),
        onReject: onReject,
      );

  Future<dynamic> pushAccountMarketPage() =>
      push<dynamic>(Routes.accountMarketPage);

  Future<dynamic> pushUserProfile() => push<dynamic>(Routes.userProfile);

  Future<dynamic> pushMessagePage() => push<dynamic>(Routes.messagePage);

  Future<dynamic> pushMessageRoomPage(
          {ChatUser user, OnNavigationRejected onReject}) =>
      push<dynamic>(
        Routes.messageRoomPage,
        arguments: MessageRoomPageArguments(user: user),
        onReject: onReject,
      );

  Future<dynamic> pushMarketWalletPage() =>
      push<dynamic>(Routes.marketWalletPage);

  Future<dynamic> pushLiveMarketPage() => push<dynamic>(Routes.liveMarketPage);

  Future<dynamic> pushCallPage({
    String title,
    File contentThumbnail,
  }) =>
      push<dynamic>(
        Routes.callPage,
        arguments:
            CallPageArguments(title: title, contentThumbnail: contentThumbnail),
      );

  Future<dynamic> pushJoinPage(
          {dynamic channelId,
          dynamic hostId,
          dynamic hostImageUrl,
          dynamic hostName,
          OnNavigationRejected onReject}) =>
      push<dynamic>(
        Routes.joinPage,
        arguments: JoinPageArguments(
            channelId: channelId,
            hostId: hostId,
            hostImageUrl: hostImageUrl,
            hostName: hostName),
        onReject: onReject,
      );

  Future<dynamic> pushCreateLiveContentPage() =>
      push<dynamic>(Routes.createLiveContentPage);

  Future<dynamic> pushMarketSearchProduct() =>
      push<dynamic>(Routes.marketSearchProduct);

  Future<dynamic> pushMarketCartPayment() =>
      push<dynamic>(Routes.marketCartPayment);

  Future<dynamic> pushShippingBillingPage() =>
      push<dynamic>(Routes.shippingBillingPage);

  Future<dynamic> pushMarketSuccessPurchasedPage() =>
      push<dynamic>(Routes.marketSuccessPurchasedPage);

  Future<dynamic> pushOrderTrackPage({
    List<LatLng> cordinates,
    BitmapDescriptor pinLocationIcon,
    String deliverd,
  }) =>
      push<dynamic>(
        Routes.orderTrackPage,
        arguments: OrderTrackPageArguments(
            cordinates: cordinates,
            pinLocationIcon: pinLocationIcon,
            deliverd: deliverd),
      );

  Future<dynamic> pushHotDealsPage() => push<dynamic>(Routes.hotDealsPage);

  Future<dynamic> pushMarketCameraPage() =>
      push<dynamic>(Routes.marketCameraPage);

  Future<dynamic> pushProductRecognizerPage() =>
      push<dynamic>(Routes.productRecognizerPage);

  Future<dynamic> pushWProductImage({
    Widget Function(BuildContext, ImageChunkEvent) loadingBuilder,
    Decoration backgroundDecoration,
    dynamic minScale,
    dynamic maxScale,
    int initialIndex = 1,
    Axis scrollDirection = Axis.horizontal,
  }) =>
      push<dynamic>(
        Routes.wProductImage,
        arguments: WProductImageArguments(
            loadingBuilder: loadingBuilder,
            backgroundDecoration: backgroundDecoration,
            minScale: minScale,
            maxScale: maxScale,
            initialIndex: initialIndex,
            scrollDirection: scrollDirection),
      );

  Future<dynamic> pushViewProduct({
    @required String hashid,
  }) =>
      push<dynamic>(
        Routes.viewProduct,
        arguments: ViewProductArguments(hashid: hashid),
      );

  Future<dynamic> pushViewOtherProduct() =>
      push<dynamic>(Routes.viewOtherProduct);

  Future<dynamic> pushCategoryPage() => push<dynamic>(Routes.categoryPage);

  Future<dynamic> pushWmallPayment() => push<dynamic>(Routes.wmallPayment);

  Future<dynamic> pushComingSoonPage({
    String page,
    bool showDrawer = false,
  }) =>
      push<dynamic>(
        Routes.comingSoonPage,
        arguments: ComingSoonPageArguments(page: page, showDrawer: showDrawer),
      );

  Future<dynamic> pushTransationScreen() =>
      push<dynamic>(Routes.transationScreen);

  Future<dynamic> pushUnknownRouteScreen() =>
      push<dynamic>(Routes.unknownRouteScreen);
}

class RestaurantsScreenRoutes {
  static const String singeRestaurantMain = '/';
  static const all = <String>{
    singeRestaurantMain,
  };
}

class RestaurantsScreenRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(RestaurantsScreenRoutes.singeRestaurantMain,
        page: SingeRestaurantMain),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    SingeRestaurantMain: (data) {
      final args = data.getArgs<SingeRestaurantMainArguments>(
        orElse: () => SingeRestaurantMainArguments(),
      );
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => SingeRestaurantMain(
          key: args.key,
          restaurantListModel: args.restaurantListModel,
          restIndex: args.restIndex,
        ),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Navigation helper methods extension
/// *************************************************************************

extension RestaurantsScreenRouterExtendedNavigatorStateX
    on ExtendedNavigatorState {
  Future<dynamic> pushSingeRestaurantMain({
    Key key,
    RestaurantListModel restaurantListModel,
    int restIndex,
  }) =>
      push<dynamic>(
        RestaurantsScreenRoutes.singeRestaurantMain,
        arguments: SingeRestaurantMainArguments(
            key: key,
            restaurantListModel: restaurantListModel,
            restIndex: restIndex),
      );
}

class MainMarketPageRoutes {
  static const String homeMarketPage = '/';
  static const String feedMarketPage = '/feed';
  static const String brandsPage = '/message';
  static const all = <String>{
    homeMarketPage,
    feedMarketPage,
    brandsPage,
  };
}

class MainMarketPageRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(MainMarketPageRoutes.homeMarketPage, page: HomeMarketPage),
    RouteDef(MainMarketPageRoutes.feedMarketPage, page: FeedMarketPage),
    RouteDef(MainMarketPageRoutes.brandsPage, page: BrandsPage),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    HomeMarketPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => HomeMarketPage(),
        settings: data,
      );
    },
    FeedMarketPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => FeedMarketPage(),
        settings: data,
      );
    },
    BrandsPage: (data) {
      return buildAdaptivePageRoute<dynamic>(
        builder: (context) => BrandsPage(),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Navigation helper methods extension
/// *************************************************************************

extension MainMarketPageRouterExtendedNavigatorStateX
    on ExtendedNavigatorState {
  Future<dynamic> pushHomeMarketPage() =>
      push<dynamic>(MainMarketPageRoutes.homeMarketPage);

  Future<dynamic> pushFeedMarketPage() =>
      push<dynamic>(MainMarketPageRoutes.feedMarketPage);

  Future<dynamic> pushBrandsPage() =>
      push<dynamic>(MainMarketPageRoutes.brandsPage);
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// RestaurantViewOrderPage arguments holder class
class RestaurantViewOrderPageArguments {
  final String orderHashid;
  RestaurantViewOrderPageArguments({this.orderHashid});
}

/// SingleRestaurantPage arguments holder class
class SingleRestaurantPageArguments {
  final RestaurantListModel restaurantListModel;
  final int restIndex;
  SingleRestaurantPageArguments({this.restaurantListModel, this.restIndex});
}

/// RestCatogries arguments holder class
class RestCatogriesArguments {
  final String coverImage;
  final String restimage;
  final String restName;
  final bool isDelivery;
  final bool preOrder;
  final bool isFromScan;
  final String restSlogan;
  final int restindex;
  final bool isFromTakeAway;
  final String restId;
  final String restThumbnail;
  final String coverThumbnail;
  final dynamic tableID;
  final String reservationHashId;
  RestCatogriesArguments(
      {this.coverImage,
      this.restimage,
      this.restName,
      this.isDelivery = false,
      this.preOrder = false,
      this.isFromScan = false,
      this.restSlogan,
      this.restindex,
      this.isFromTakeAway,
      this.restId,
      this.restThumbnail,
      this.coverThumbnail,
      this.tableID,
      this.reservationHashId});
}

/// RestaurantFoodVariation arguments holder class
class RestaurantFoodVariationArguments {
  final Food food;
  final bool isDelivery;
  final bool isFromTakeAway;
  final bool preorder;
  final String restaurantHashid;
  final bool isFromScan;
  final String reservationHashId;
  RestaurantFoodVariationArguments(
      {this.food,
      this.isDelivery,
      this.isFromTakeAway,
      this.preorder,
      this.restaurantHashid,
      this.isFromScan,
      this.reservationHashId});
}

/// RestaurantFoodCart arguments holder class
class RestaurantFoodCartArguments {
  final bool isFromScan;
  final bool preOrder;
  final bool isDelivery;
  final dynamic totlaPrice;
  final String coverImage;
  final String restimage;
  final String restName;
  final String restId;
  final String restSlogan;
  final String onlybooking;
  final String filterkey;
  final bool isFromTakeAway;
  final String appBarTitle;
  final String fromMenu;
  final String reservationHashID;
  RestaurantFoodCartArguments(
      {this.isFromScan,
      this.preOrder,
      this.isDelivery,
      this.totlaPrice,
      this.coverImage,
      this.restimage,
      this.restName,
      this.restId,
      this.restSlogan,
      this.onlybooking,
      this.filterkey,
      this.isFromTakeAway,
      this.appBarTitle,
      this.fromMenu,
      this.reservationHashID});
}

/// RestaurantMessagePage arguments holder class
class RestaurantMessagePageArguments {
  final String restaurantHashid;
  final String restaurantName;
  RestaurantMessagePageArguments({this.restaurantHashid, this.restaurantName});
}

/// RestaurantCartPage arguments holder class
class RestaurantCartPageArguments {
  final String fromScan;
  final String preOrder;
  final String delivery;
  final dynamic totlaPrice;
  final String coverImage;
  final String restimage;
  final String restName;
  final String restId;
  final String restSlogan;
  final String onlybooking;
  final String filterkey;
  final String fromTakeAway;
  final String appBarTitle;
  final String fromMenu;
  final String reservationHashID;
  RestaurantCartPageArguments(
      {this.fromScan,
      this.preOrder,
      this.delivery,
      this.totlaPrice,
      this.coverImage,
      this.restimage,
      this.restName,
      this.restId,
      this.restSlogan,
      this.onlybooking,
      this.filterkey,
      this.fromTakeAway,
      this.appBarTitle,
      this.fromMenu,
      this.reservationHashID});
}

/// RestaurantPlaceOrderPage arguments holder class
class RestaurantPlaceOrderPageArguments {
  final String cartId;
  final String note;
  RestaurantPlaceOrderPageArguments({this.cartId, this.note});
}

/// RestaurantCheckoutAddressPage arguments holder class
class RestaurantCheckoutAddressPageArguments {
  final UserAddressModel userAddressModel;
  RestaurantCheckoutAddressPageArguments({this.userAddressModel});
}

/// SelectDeliveryAddressPage arguments holder class
class SelectDeliveryAddressPageArguments {
  final UserOfficeAddressModel userOfficeAddressModel;
  SelectDeliveryAddressPageArguments({this.userOfficeAddressModel});
}

/// ReservationDetailsPage arguments holder class
class ReservationDetailsPageArguments {
  final RestaurentReservationModel restaurentReservationModel;
  ReservationDetailsPageArguments({this.restaurentReservationModel});
}

/// PickupConfirmationPage arguments holder class
class PickupConfirmationPageArguments {
  final String totalprice;
  final String rImage;
  final String rName;
  PickupConfirmationPageArguments({this.totalprice, this.rImage, this.rName});
}

/// ConfirmationScreen arguments holder class
class ConfirmationScreenArguments {
  final String time;
  final String date;
  final String tableNumber;
  final String guest;
  final String onlybooking;
  ConfirmationScreenArguments(
      {this.time, this.date, this.tableNumber, this.guest, this.onlybooking});
}

/// ActivityBookingSuccess arguments holder class
class ActivityBookingSuccessArguments {
  final String from;
  ActivityBookingSuccessArguments({this.from});
}

/// BookingConfirmationPage arguments holder class
class BookingConfirmationPageArguments {
  final String time;
  final String date;
  final String tableNumber;
  final String guest;
  BookingConfirmationPageArguments(
      {this.time, this.date, this.tableNumber, this.guest});
}

/// CartMarketPage arguments holder class
class CartMarketPageArguments {
  final bool canBack;
  CartMarketPageArguments({this.canBack = false});
}

/// MessageRoomPage arguments holder class
class MessageRoomPageArguments {
  final ChatUser user;
  MessageRoomPageArguments({this.user});
}

/// CallPage arguments holder class
class CallPageArguments {
  final String title;
  final File contentThumbnail;
  CallPageArguments({this.title, this.contentThumbnail});
}

/// JoinPage arguments holder class
class JoinPageArguments {
  final dynamic channelId;
  final dynamic hostId;
  final dynamic hostImageUrl;
  final dynamic hostName;
  JoinPageArguments(
      {this.channelId, this.hostId, this.hostImageUrl, this.hostName});
}

/// OrderTrackPage arguments holder class
class OrderTrackPageArguments {
  final List<LatLng> cordinates;
  final BitmapDescriptor pinLocationIcon;
  final String deliverd;
  OrderTrackPageArguments(
      {this.cordinates, this.pinLocationIcon, this.deliverd});
}

/// WProductImage arguments holder class
class WProductImageArguments {
  final Widget Function(BuildContext, ImageChunkEvent) loadingBuilder;
  final Decoration backgroundDecoration;
  final dynamic minScale;
  final dynamic maxScale;
  final int initialIndex;
  final Axis scrollDirection;
  WProductImageArguments(
      {this.loadingBuilder,
      this.backgroundDecoration,
      this.minScale,
      this.maxScale,
      this.initialIndex = 1,
      this.scrollDirection = Axis.horizontal});
}

/// ViewProduct arguments holder class
class ViewProductArguments {
  final String hashid;
  ViewProductArguments({@required this.hashid});
}

/// ComingSoonPage arguments holder class
class ComingSoonPageArguments {
  final String page;
  final bool showDrawer;
  ComingSoonPageArguments({this.page, this.showDrawer = false});
}

/// SingeRestaurantMain arguments holder class
class SingeRestaurantMainArguments {
  final Key key;
  final RestaurantListModel restaurantListModel;
  final int restIndex;
  SingeRestaurantMainArguments(
      {this.key, this.restaurantListModel, this.restIndex});
}
