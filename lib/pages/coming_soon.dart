import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wblue_customer/widgets/w_drawer.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

class ComingSoonPage extends StatelessWidget {
  final String page;
  final bool showDrawer;
  ComingSoonPage({this.page, this.showDrawer: false});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: WDrawerWidget('coming-soon'),
      appBar: AppBar(
        automaticallyImplyLeading: showDrawer,
        backgroundColor: Config.primaryColor,
        elevation: 0,
      ),
      backgroundColor: Config.primaryColor,
      body: SafeArea(
        child: Container(
          color: Config.primaryColor,
          height: 1.sh,
          width: 1.sw,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/launcher/wmall.jpg',
                height: 254,
                width: 254,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    if (page != null)
                      RichText(
                        text: TextSpan(
                          style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.black,
                          ),
                          children: <TextSpan>[
                            TextSpan(
                              text: 'OUR',
                              style: TextStyle(
                                fontSize: 24,
                                color: Colors.white,
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                            TextSpan(
                                text: ' $page',
                                style: new TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 24,
                                    color: Colors.white)),
                            TextSpan(
                              text: ' IS',
                              style: TextStyle(
                                fontSize: 24,
                                color: Colors.white,
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ],
                        ),
                      ),
                    AutoSizeText(" COMING SOON",
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.white,
                          fontWeight: FontWeight.w200,
                        )),
                    SizedBox(height: 20),
                    WRoundedButton(
                      onCustomButtonPressed: () =>
                          ExtendedNavigator.of(context).pop(),
                      borderColor: Colors.white,
                      btnColor: Config.primaryColor,
                      labelColor: Colors.white,
                      child: AutoSizeText('BACK'),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
