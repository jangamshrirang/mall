class RestaurantReviewModel {
  RestaurantReviewModel({
    this.rating,
    this.content,
    this.createdAt,
    this.updatedAt,
    this.user,
  });

  int rating;
  String content;
  DateTime createdAt;
  DateTime updatedAt;
  String user;

  factory RestaurantReviewModel.fromJson(Map<String, dynamic> json) =>
      RestaurantReviewModel(
        rating: json["rating"] == null ? null : json["rating"],
        content: json["content"] == null ? null : json["content"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        user: json["user"] == null ? null : json["user"],
      );

  Map<String, dynamic> toJson() => {
        "rating": rating == null ? null : rating,
        "content": content == null ? null : content,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "user": user == null ? null : user,
      };
}
