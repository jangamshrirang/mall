import 'package:flutter/foundation.dart';
import 'package:wblue_customer/models/order/my_order_list.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();

class PRestaurantOrders with ChangeNotifier {
  RestaurantMyOrderListModel showLatestOneOrder;
  bool _isLoading = true;
  bool get isLoading => _isLoading;

  void setOneOrderLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }

  oneOrder() async {
    showLatestOneOrder = await fetchOneOrder();
    notifyListeners();
  }

  viewOrder(String hashId) async {
    showLatestOneOrder = await fetchOneOrder();
    notifyListeners();
  }

  setFromSocket(String hashId) async {}

  Future<RestaurantMyOrderListModel> fetchOneOrder() async {
    try {
      setOneOrderLoading(true);
      APIResponse res = await _dioClient
          .privatePost('/restaurant/order/order-progress', data: {'status': null, 'limit': 1, 'all_order': 2});

      if (res.code == 200 && res.data != null) {
        print(res.data);

        return RestaurantMyOrderListModel.fromJson(res.data);
      }
      setOneOrderLoading(false);
      return null;
    } catch (e) {
      print('Method fetchOneOrder Throw an error: $e');
      return null;
    }
  }
}
