import 'package:flutter/cupertino.dart';
import 'package:transparent_image/transparent_image.dart';

class WImageWidget extends StatelessWidget {
  final String url;
  final ImageProvider placeholder;
  final BoxFit fit;

  WImageWidget({
    this.url: '',
    this.placeholder,
    this.fit: BoxFit.contain,
  });

  @override
  Widget build(BuildContext context) {
    ImageProvider _placeholder = placeholder ?? MemoryImage(kTransparentImage);

    return FadeInImage(
      image: url == '' ? _placeholder : NetworkImage(url),
      placeholder: _placeholder,
      fit: fit,
    );
  }
}
