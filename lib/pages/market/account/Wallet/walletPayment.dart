import 'package:auto_route/auto_route.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:wblue_customer/Bloc/Statemanagement.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/pages/Activities/ActivitySucces.dart';
import 'package:wblue_customer/pages/booking/booking_confirmation.dart';

import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

class WalletPayment extends StatefulWidget {
  @override
  _WalletPaymentState createState() => _WalletPaymentState();
}

class _WalletPaymentState extends State<WalletPayment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        elevation: 0.0,
        leading: IconButton(
          onPressed: () => ExtendedNavigator.of(context).pop(),
          icon: Icon(MdiIcons.arrowLeft),
          color: Colors.black,
        ),
        centerTitle: false,
        title: AutoSizeText('Select Payment Method',
            style: TextStyle(color: Colors.black)),
      ),
      body: ListView(
        physics: ClampingScrollPhysics(),
        children: [
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                      child: Card(
                    elevation: 0.0,
                    shape: RoundedRectangleBorder(
                        side: BorderSide(width: 0, color: Colors.transparent),
                        borderRadius: BorderRadius.circular(5)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Container(
                            height: 50,
                            width: 50,
                            child: WImageWidget(
                              placeholder: AssetImage(
                                  'assets/images/market/cart/visa.png'),
                            ),
                          ),
                          AutoSizeText('Credit/Debit Card'),
                        ],
                      ),
                    ),
                  )),
                 
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  field('Card Number'),
                  SizedBox(height: 10),
                  field('Name on card'),
                  SizedBox(height: 10),
                  Row(
                    children: [
                      Expanded(
                        child: field('Expiration Date', hint: 'MM/YY'),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: field('CVV'),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                children: [
                  Icon(
                    MdiIcons.checkboxMarked,
                    color: Config.primaryColor,
                  ),
                  SizedBox(width: 10.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AutoSizeText('Save Card'),
                      SizedBox(height: 5.0),
                      AutoSizeText(
                          'Information is encrypted and securely stored.',
                          style: TextStyle(color: Colors.grey))
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: WRoundedButton(
                onCustomButtonPressed: () {
                 _successDialog(context);
                },
                child: AutoSizeText('Pay Now'),
                borderColor: Config.primaryColor,
                btnColor: Config.primaryColor,
                labelColor: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget field(String type, {String hint}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        AutoSizeText(
          '* $type',
          style: TextStyle(fontSize: 16.0, color: Colors.red),
        ),
        SizedBox(
          height: 10,
        ),
        TextField(
            decoration: new InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Config.primaryColor, width: 0.5),
          ),
          contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 0.5),
          ),
          hintText: hint ?? type,
        ))
      ],
    );
  }

  _displayDialog(BuildContext context) async {
    var size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    button(" Done", () {
                      Navigator.pop(context);
                      _successDialog(context);
                    }, Color(0xff101f40)),
                  ],
                ),
              ),
            ),
          );
        });
  }

  text(
    String text,
    double height,
    Color color,
    int lines,
    FontWeight fontWeight,
    double width,
    TextDecoration underline,
  ) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: width,
      margin: EdgeInsets.only(left: 10, top: 0),
      child: Text(
        text,
        maxLines: lines,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: fontWeight,
            color: color,
            decoration: underline,
            fontSize: height),
      ),
    );
  }

  _successDialog(BuildContext context) async {
    var size = MediaQuery.of(context).size;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.check_circle,
                      color: Colors.green,
                      size: 64.0,
                    ),
                    text(
                        "Top Up done Succefully ",
                        size.height * 0.023,
                        Colors.black,
                        1,
                        FontWeight.bold,
                        size.width * 0.5,
                        TextDecoration.none),
                    Divider(),
                    button(" Done", () {
                      Navigator.pop(context);
                    }, Color(0xff101f40)),
                  ],
                ),
              ),
            ),
          );
        });
  }
}

button(String title, Function onTap, Color color) {
  return RaisedButton(
      child: Text(
        title,
        style: TextStyle(color: Colors.white),
      ),
      color: color,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      onPressed: onTap);
}
