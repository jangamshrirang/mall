import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_modal_container.dart';
import 'package:wblue_customer/widgets/w_rating.dart';

class WHotelProfileHeaderWidget extends StatefulWidget {
  @override
  _WHotelProfileHeaderWidgetState createState() => _WHotelProfileHeaderWidgetState();
}

class _WHotelProfileHeaderWidgetState extends State<WHotelProfileHeaderWidget> {
  bool loved = false;

  @override
  Widget build(BuildContext context) {
    void _reviews() {
      showModalBottomSheet(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState2) {
              Widget _row(String label, double value, double total, {double width: 150.0}) {
                double pad = 0;
                double percentage = value / total;
                double toLess = 1 - percentage;
                pad = width * toLess;
                return Container(
                  margin: EdgeInsets.only(bottom: 8.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 60.0,
                        child: Text(
                          label,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 10.0,
                          ),
                        ),
                      ),
                      Container(
                        width: width,
                        height: 12.0,
                        padding: EdgeInsets.only(right: pad),
                        child: Container(
                          color: Colors.pink,
                          child: Text(''),
                        ),
                      ),
                      Text(
                        value.toString(),
                        style: TextStyle(fontSize: 10.0),
                      ),
                    ],
                  ),
                );
              }

              String filter = 'all';
              String sort = 'default';
              return WModalContainerWidget(
                title: 'Reviews',
                isWrap: false,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Hotel Name',
                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Based on 333 Reviews',
                        style: TextStyle(fontSize: 10.0),
                      ),
                      SizedBox(height: 8.0),
                      Row(
                        children: <Widget>[
                          SizedBox(width: 16.0),
                          Column(
                            children: <Widget>[
                              Container(
                                width: 50.0,
                                height: 50.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50.0),
                                  border: Border.all(color: Colors.pink, width: 2.0),
                                ),
                                child: Center(
                                  child: Text(
                                    '5.0',
                                    style: TextStyle(
                                      color: Colors.pink,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                              Text(
                                'Excellent',
                                style: TextStyle(
                                  color: Colors.pink,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(width: 16.0),
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                _row('Excellent', 165, 333),
                                _row('Very Good', 101, 333),
                                _row('Good', 37, 333),
                                _row('Bad', 21, 333),
                                _row('Worse', 9, 333),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 8.0),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Filter Reviews',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                DropdownButton(
                                  value: filter,
                                  icon: Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.amber,
                                  ),
                                  onChanged: (String newVal) {
                                    setState2(() {
                                      filter = newVal;
                                    });
                                  },
                                  style: TextStyle(
                                    fontSize: 10.0,
                                    color: Colors.black,
                                  ),
                                  isExpanded: true,
                                  items: <String>[
                                    'all',
                                    'english',
                                    'arabic'
                                  ].map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem(
                                      value: value,
                                      child: Text(value.toUpperCase()),
                                    );
                                  }).toList(),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 16.0),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Sort by',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                DropdownButton(
                                  value: sort,
                                  icon: Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.amber,
                                  ),
                                  onChanged: (String newVal) {
                                    setState2(() {
                                      sort = newVal;
                                    });
                                  },
                                  style: TextStyle(
                                    fontSize: 10.0,
                                    color: Colors.black,
                                  ),
                                  isExpanded: true,
                                  items: <String>[
                                    'default',
                                    'latest',
                                  ].map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem(
                                      value: value,
                                      child: Text(value.toUpperCase()),
                                    );
                                  }).toList(),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Divider(),
                      Text('Guest Reviews'),
                      Divider(),
                      SingleChildScrollView(
                        child: Column(
                          children: List.generate(
                            10,
                            (index) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  ListTile(
                                    contentPadding: EdgeInsets.all(0.0),
                                    leading: CircleAvatar(
                                      child: Text('5'),
                                      backgroundColor: Colors.pink,
                                      foregroundColor: Colors.white,
                                    ),
                                    title: Text(
                                      'Guest Name',
                                      style: TextStyle(fontSize: 10.0, fontWeight: FontWeight.bold),
                                    ),
                                    subtitle: Text(
                                      '08/28/2019',
                                      style: TextStyle(fontSize: 10.0),
                                    ),
                                  ),
                                  Text(
                                    'A long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.',
                                    style: TextStyle(fontSize: 10.0),
                                    textAlign: TextAlign.justify,
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              );
            },
          );
        },
      );
    }

    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.3,
          child: WImageWidget(
            url: '',
            placeholder: ExactAssetImage('assets/images/samples/hotel.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        Positioned.fill(
          child: SafeArea(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        ExtendedNavigator.of(context).pop();
                      },
                      color: Colors.amber,
                    ),
                    IconButton(
                      icon: Icon(
                        loved ? Icons.favorite : Icons.favorite_border,
                        color: Colors.pink,
                      ),
                      onPressed: () {
                        setState(() {
                          loved = !loved;
                        });
                      },
                    ),
                  ],
                ),
                Expanded(
                  child: SizedBox(),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
                  decoration: BoxDecoration(color: Colors.black45),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Hotel Name',
                              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                              overflow: TextOverflow.ellipsis,
                            ),
                            SizedBox(height: 4.0),
                            InkWell(
                              onTap: () {
                                _reviews();
                              },
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  WRatingsWidget(5.0),
                                  Text(
                                    '• See all Reviews',
                                    style: TextStyle(color: Colors.white, fontSize: 10.0),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Text(
                        'QAR 250.00',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
