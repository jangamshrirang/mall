import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:wblue_customer/services/dio/dio_client.dart';

DioClient _dioClient = DioClient();
class RCategariesListHelper with ChangeNotifier {
  List _rCatgoriesMenuList = new List();
  bool _isLoading = true;

  UnmodifiableListView get rCatMenu =>
      UnmodifiableListView(_rCatgoriesMenuList);
  bool get isLoading => _isLoading;

  Future<void> fetchRCatogriesList(String rHashId, key) async {
    setLoading(true);
    _rCatgoriesMenuList = await fetchCategaries(rHashId, key);
    setLoading(false);

    notifyListeners();
  }

  void setLoading(bool status) {
    _isLoading = status;
    notifyListeners();
  }
}

Future<List> fetchCategaries(String rHashId, key) async {
  Map<String, dynamic> _body = {
    'restaurant_hashid': rHashId,
    'category_hashid': key,
  };

  APIResponse res = await _dioClient.publicGet(
    '/restaurant/list-food-types',
  );
  if (res.code >= 400) {
    return [];
  }
  print(res.data);
  return res.data;
}