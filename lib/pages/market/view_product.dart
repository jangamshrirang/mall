import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wblue_customer/env/config.dart';
import 'package:wblue_customer/models/market/store.dart';
import 'package:wblue_customer/models/product.dart';
import 'package:wblue_customer/providers/market/p_view_product.dart';
import 'package:wblue_customer/widgets/w_image.dart';
import 'package:wblue_customer/widgets/w_rating.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:wblue_customer/widgets/w_rounded_button.dart';

class ViewProduct extends StatefulWidget {
  final String hashid;
  ViewProduct(this.hashid);

  @override
  _ViewProductState createState() => _ViewProductState();
}

class _ViewProductState extends State<ViewProduct> {
  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      context.read<PViewProduct>().fetchProduct(widget.hashid);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<PViewProduct>(
        builder: (__, product, ___) => product.singleProduct != null
            ? Column(
                children: [
                  Container(
                    height: 0.9.sh,
                    child: CustomScrollView(
                      physics: ClampingScrollPhysics(),
                      slivers: <Widget>[
                        SliverPersistentHeader(
                          pinned: true,
                          delegate: SliverCustomHeaderDelegate(
                              title: '${product.singleProduct.name}',
                              collapsedHeight: 0.06.sh,
                              expandedHeight: 1.sw,
                              paddingTop: MediaQuery.of(context).padding.top,
                              coverImgsUrl: product.singleProduct.images),
                        ),
                        SliverList(
                          delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) =>
                                  productContent(context, product),
                              childCount: 1),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                        top: BorderSide(
                          color: Colors.black,
                          width: 0.1,
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        Expanded(
                            child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  SizedBox(
                                    height: 40,
                                    width: 60,
                                    child: FlatButton(
                                      onPressed: () {},
                                      child: Center(
                                        child: Icon(
                                          MdiIcons.storefrontOutline,
                                          color: Colors.grey,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 40,
                                    width: 60,
                                    child: FlatButton(
                                      onPressed: () {},
                                      child: Center(
                                        child: Icon(
                                          MdiIcons.messageProcessingOutline,
                                          color: Colors.grey,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  WRoundedButton(
                                    onCustomButtonPressed: () {},
                                    child: Text('Buy Now'),
                                    btnColor: Config.primaryColor,
                                    labelColor: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  WRoundedButton(
                                    onCustomButtonPressed: () {},
                                    child: Text('Add to Cart'),
                                    btnColor: Config.primaryColor,
                                    labelColor: Colors.white,
                                  ),
                                ],
                              )
                            ],
                          ),
                        )),
                      ],
                    ),
                  ),
                ],
              )
            : Center(child: CircularProgressIndicator()),
      ),
    );
  }
}

Widget productContent(BuildContext context, PViewProduct product) {
  return Container(
    color: Colors.grey[100],
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            description(product),
            SizedBox(height: 10),
            if (product.singleProduct.quickDetails.length > 0)
              specification(context, product.singleProduct),
            SizedBox(height: 10),
            if (product.singleProduct.variations.length > 0)
              variation(context, product.singleProduct.variations),
            SizedBox(height: 10),
            if (product.store != null &&
                product.singleProduct.storeId == product.store.hashid)
              store(context, product.store)
          ],
        )
      ],
    ),
  );
}

Widget store(BuildContext context, StoreModel store) {
  return GestureDetector(
    onTap: () {},
    child: Container(
      color: Colors.white,
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      CachedNetworkImage(
                          imageUrl: store.profilePhotoFull ?? '',
                          imageBuilder: (context, imageProvider) => Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                width: 50,
                                height: 50,
                                child: Image.network(
                                  store.profilePhotoFull,
                                  fit: BoxFit.fitWidth,
                                ),
                              ),
                          placeholder: (context, url) => Container(
                                color: Colors.black,
                                width: 50,
                                height: 50,
                                child: Image.network(
                                  store.profilePhotoThumbnail,
                                  fit: BoxFit.fitWidth,
                                  height: double.infinity,
                                  width: double.infinity,
                                  alignment: Alignment.center,
                                ),
                              ),
                          errorWidget: (context, url, error) =>
                              Icon(MdiIcons.alertCircle)),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: AutoSizeText(
                              store.storeName,
                              style: TextStyle(fontSize: 18),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  FlatButton(
                    onPressed: () {},
                    child: AutoSizeText(
                      'Follow',
                      style:
                          TextStyle(fontSize: 18, color: Config.primaryColor),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      minFontSize: 18,
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            AutoSizeText(
                              '0 %',
                              style: TextStyle(fontSize: 16),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                            AutoSizeText(
                              'Positive Seller Ratings',
                              style:
                                  TextStyle(fontSize: 12, color: Colors.grey),
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            right: BorderSide(
                              color: Colors.grey,
                              width: 1.5,
                            ),
                            left: BorderSide(
                              color: Colors.grey,
                              width: 1.5,
                            ),
                          ),
                        ),
                        child: Column(
                          children: [
                            AutoSizeText(
                              '0 %',
                              style: TextStyle(fontSize: 16),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                            AutoSizeText(
                              'Ship on Time',
                              style:
                                  TextStyle(fontSize: 12, color: Colors.grey),
                              textAlign: TextAlign.center,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            AutoSizeText(
                              '0 %',
                              style: TextStyle(fontSize: 16),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              minFontSize: 16,
                            ),
                            AutoSizeText(
                              'Chat Response Rate',
                              style:
                                  TextStyle(fontSize: 12, color: Colors.grey),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    WRoundedButton(
                      onCustomButtonPressed: () {},
                      elevation: 0.0,
                      child: AutoSizeText('Chat'),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    WRoundedButton(
                      onCustomButtonPressed: () {},
                      elevation: 0.0,
                      labelColor: Colors.white,
                      btnColor: Config.primaryColor,
                      child: AutoSizeText('Visit Store'),
                    )
                  ],
                ),
              ),
            ],
          )),
    ),
  );
}

Widget variation(
  BuildContext context,
  List<Variation> variations,
) {
  return GestureDetector(
    onTap: () => displayVariation(context, variations),
    child: Container(
      padding: EdgeInsets.all(10.0),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AutoSizeText(
                'Variation',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Icon(MdiIcons.chevronRight),
            ],
          ),
          Container(
            height: 0.08.sw,
            child: ListView.builder(
                physics: ClampingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemCount: variations.length,
                itemBuilder: (BuildContext ctxt, int index) => Container(
                      color: Colors.grey.withOpacity(0.2),
                      padding: EdgeInsets.all(10.0),
                      child: Center(
                        child: AutoSizeText(
                          '${variations[index].values.length} ${variations[index].name}',
                          style: TextStyle(color: Colors.black45),
                        ),
                      ),
                    )),
          ),
        ],
      ),
    ),
  );
}

Widget specification(BuildContext context, SingleProductModel product) {
  return GestureDetector(
    onTap: () => displaySpecification(context, product),
    child: Container(
      padding: EdgeInsets.all(10.0),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AutoSizeText(
                'Specification',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Icon(MdiIcons.chevronRight),
            ],
          ),
          Container(
            height: 0.06.sw,
            child: ListView.builder(
                physics: ClampingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemCount: product.quickDetails.length,
                itemBuilder: (BuildContext ctxt, int index) => Center(
                      child: AutoSizeText(
                        '${product.quickDetails[index].label}${product.quickDetails.length - 1 != index ? ', ' : ''}',
                        style: TextStyle(color: Colors.grey),
                      ),
                    )),
          ),
        ],
      ),
    ),
  );
}

void displayVariation(BuildContext context, List<Variation> variations) {
  showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (ctx) {
        int selected = 0;
        return StatefulBuilder(
            builder: (BuildContext context, setState) => Container(
                height: MediaQuery.of(context).size.height * 0.7,
                child: ListView.builder(
                  physics: ClampingScrollPhysics(),
                  itemCount: variations.length,
                  itemBuilder: (ctx, index) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              AutoSizeText(variations[index].name,
                                  style: TextStyle(fontSize: 16.0)),
                              SizedBox(height: 0.01.sw),
                              Wrap(
                                  children: List.generate(
                                      variations[index].values.length,
                                      (varIndex) {
                                return Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8.0),
                                  child: FilterChip(
                                    label: AutoSizeText(
                                      '${variations[index].values[varIndex]}',
                                      style: TextStyle(
                                          color: selected == varIndex
                                              ? Colors.white
                                              : Colors.black),
                                    ),
                                    backgroundColor: selected == varIndex
                                        ? Config.primaryColor
                                        : Colors.black12,
                                    shape: BeveledRectangleBorder(
                                      borderRadius: BorderRadius.circular(3.0),
                                    ),
                                    onSelected: (bool value) {
                                      setState(() => selected = varIndex);
                                    },
                                  ),
                                );
                              })),
                            ],
                          ),
                        ),
                        if (variations.length - 1 == index)
                          Divider(color: Colors.black12, height: 0.25),
                        if (variations.length - 1 == index)
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: AutoSizeText('Quantity',
                                style: TextStyle(fontSize: 18.0)),
                          )
                      ],
                    );
                  },
                )));
      });
}

void displaySpecification(BuildContext context, SingleProductModel product) {
  showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (ctx) {
        return Container(
            height: MediaQuery.of(context).size.height * 0.7,
            child: ListView.builder(
              physics: ClampingScrollPhysics(),
              itemCount: product.quickDetails.length,
              itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AutoSizeText(product.quickDetails[index].label,
                        style: TextStyle(fontSize: 16.0)),
                    SizedBox(height: 0.01.sw),
                    Row(
                      children: [
                        Icon(
                          MdiIcons.square,
                          color: Colors.grey,
                          size: 10,
                        ),
                        SizedBox(width: 0.02.sw),
                        AutoSizeText('${product.quickDetails[index].value}',
                            style:
                                TextStyle(fontSize: 14.0, color: Colors.grey)),
                      ],
                    ),
                  ],
                ),
              ),
            ));
      });
}

Widget description(PViewProduct product) {
  return Container(
      padding: EdgeInsets.all(10.0),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Row(
              children: <Widget>[
                AutoSizeText(
                  'QR ${product.singleProduct.price}',
                  style: TextStyle(fontSize: 18, color: Colors.redAccent),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  minFontSize: 16,
                ),
                product.singleProduct.salePrice > 0
                    ? Expanded(
                        child: AutoSizeText(
                          'QR ${product.singleProduct.price}',
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.grey,
                              decoration: TextDecoration.lineThrough),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          minFontSize: 16,
                        ),
                      )
                    : SizedBox(),
                Spacer(),
                IconButton(
                    icon: Icon(Icons.favorite_border,
                        size: 20, color: Colors.black38),
                    onPressed: () {})
              ],
            ),
          ),
          SizedBox(height: 5.0),
          AutoSizeText(
            '${product.singleProduct.name}',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            minFontSize: 16,
          ),
          SizedBox(height: 10.0),
          AutoSizeText(
            '${product.singleProduct.shortDescription}',
            style: TextStyle(fontSize: 16),
            maxLines: 4,
            overflow: TextOverflow.ellipsis,
            minFontSize: 14,
          ),
          SizedBox(height: 8.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  //FIXME: need api
                  WRatingsWidget(0),
                  SizedBox(width: 2),
                  AutoSizeText(
                    '(${product.singleProduct.totalReviews}) Reviews',
                    style: TextStyle(
                        fontSize: 16, color: Colors.grey.withOpacity(0.9)),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    minFontSize: 16,
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 3.0),
        ],
      ));
}

class SliverCustomHeaderDelegate extends SliverPersistentHeaderDelegate {
  final double collapsedHeight;
  final double expandedHeight;
  final double paddingTop;
  final List<SingleProductImageModel> coverImgsUrl;
  final String title;

  SliverCustomHeaderDelegate({
    this.collapsedHeight,
    this.expandedHeight,
    this.paddingTop,
    this.coverImgsUrl,
    this.title,
  });

  @override
  double get minExtent => this.collapsedHeight + this.paddingTop;

  @override
  double get maxExtent => this.expandedHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

  Color makeStickyHeaderBgColor(shrinkOffset) {
    final int alpha = (shrinkOffset / (this.maxExtent - this.minExtent) * 255)
        .clamp(0, 255)
        .toInt();
    return Color.fromARGB(alpha, 255, 255, 255);
  }

  Color makeStickyHeaderTextColor(shrinkOffset, isIcon) {
    if (shrinkOffset <= 50) {
      return isIcon ? Colors.white : Colors.transparent;
    } else {
      final int alpha = (shrinkOffset / (this.maxExtent - this.minExtent) * 255)
          .clamp(0, 255)
          .toInt();
      return Color.fromARGB(alpha, 0, 0, 0);
    }
  }

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      height: this.maxExtent,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          // Background image
          Swiper(
            loop: false,
            pagination: SwiperCustomPagination(
                builder: (BuildContext context, SwiperPluginConfig config) {
              return Positioned(
                bottom: 16,
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                        child: Container(
                            decoration: BoxDecoration(
                              color: Colors.black.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(30),
                            ),
                            padding: EdgeInsets.all(5.0),
                            child: AutoSizeText(
                              '${config.activeIndex + 1} / ${config.itemCount}',
                              style: TextStyle(color: Colors.white),
                            )))),
              );
            }),
            itemBuilder: (BuildContext context, int index) {
              return CachedNetworkImage(
                imageUrl: coverImgsUrl[index].full ?? '',
                imageBuilder: (context, imageProvider) => Container(
                  color: Colors.black,
                  height: 0.26.sh,
                  child: SafeArea(
                    bottom: false,
                    child: Image.network(
                      coverImgsUrl[index].full,
                      fit: BoxFit.fitWidth,
                      height: double.infinity,
                      width: double.infinity,
                      alignment: Alignment.center,
                    ),
                  ),
                ),
                placeholder: (context, url) => Container(
                  color: Colors.black,
                  height: 0.26.sh,
                  child: SafeArea(
                    bottom: false,
                    child: Image.network(
                      coverImgsUrl[index].thumbnail,
                      fit: BoxFit.fitWidth,
                      height: double.infinity,
                      width: double.infinity,
                      alignment: Alignment.center,
                    ),
                  ),
                ),
                errorWidget: (context, url, error) => Container(
                  height: 0.26.sh,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AutoSizeText(
                        'Image Not Found',
                        style: TextStyle(
                            fontSize: 28.sp, fontWeight: FontWeight.w300),
                      ),
                      SizedBox(
                        height: 0.02.sh,
                      ),
                      Icon(
                        MdiIcons.alertCircle,
                        color: Colors.red,
                        size: 0.08.sh,
                      )
                    ],
                  ),
                ),
              );
            },
            itemCount: coverImgsUrl.length,
          ),
          // Put your head back
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            child: Container(
              color: this
                  .makeStickyHeaderBgColor(shrinkOffset), // Background color
              child: SafeArea(
                bottom: false,
                child: Container(
                  height: this.collapsedHeight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: this.makeStickyHeaderTextColor(
                              shrinkOffset, true), // Return icon color
                        ),
                        onPressed: () => Navigator.pop(context),
                      ),
                      Text(
                        this.title,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: this.makeStickyHeaderTextColor(
                              shrinkOffset, false), // Title color
                        ),
                      ),
                      IconButton(
                        icon: Icon(
                          MdiIcons.cart,
                          color: this.makeStickyHeaderTextColor(
                              shrinkOffset, true), // Share icon color
                        ),
                        onPressed: () {},
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
