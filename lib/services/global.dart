import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:wblue_customer/models/Restaurant/cart.dart';
import 'package:wblue_customer/models/user.dart';
import 'package:wblue_customer/widgets/w_modal_container.dart';

class Global {
  static UserModel user;
  static String otp = '';
  static List marketCart = new List();
  static Map<String, Cart> cart = {};

  static String currentCategory;

  static Map selectedProduct;

  static String myAddress = '';
  static LatLng myLocation = LatLng(25.267389469721426, 51.54438178986311);
  static bool payAtHotel = false;
  static bool payAtCinema = false;

  static String money(double amount, {String symbol: '\QAR '}) {
    final formatter = new NumberFormat('#,##0.00', 'ar_QA');
    return '$symbol${formatter.format(amount)}';
  }

//  static alertStyle() {
//    return AlertStyle(
//      alertBorder: RoundedRectangleBorder(
//        borderRadius: BorderRadius.only(topLeft: Radius.circular(30.0)),
//        side: BorderSide(
//          color: Config.secondaryColor,
//          width: 4.0,
//        ),
//      ),
//    );
//  }

  static String capitalize(String string) {
    if (string == null) {
      throw ArgumentError("string: $string");
    }

    if (string.isEmpty) {
      return string;
    }
    return string[0].toUpperCase() + string.substring(1);
  }

  static void simpleModal(BuildContext context,
      {String title: '',
      List<Widget> children,
      bool isScrollControlled: true}) {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: isScrollControlled,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState2) {
            return WModalContainerWidget(
              title: title,
              children: children,
            );
          },
        );
      },
    );
  }
}
